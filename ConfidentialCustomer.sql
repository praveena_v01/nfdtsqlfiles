USE [master]
GO
/****** Object:  Database [ConfidentialCustomer]    Script Date: 9/18/2019 7:23:21 PM ******/
CREATE DATABASE [ConfidentialCustomer]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ConfidentialCustomer', FILENAME = N'D:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\ConfidentialCustomer.mdf' , SIZE = 35844096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 256000KB )
 LOG ON 
( NAME = N'confidential_log2', FILENAME = N'F:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\Data\confidential_log2.ldf' , SIZE = 257024KB , MAXSIZE = 2048GB , FILEGROWTH = 256000KB ), 
( NAME = N'ConfidentialCustomer_log', FILENAME = N'E:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\Data\ConfidentialCustomer_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 256000KB )
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ConfidentialCustomer].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ConfidentialCustomer] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ConfidentialCustomer] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ConfidentialCustomer] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ConfidentialCustomer] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ConfidentialCustomer] SET ARITHABORT OFF 
GO
ALTER DATABASE [ConfidentialCustomer] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ConfidentialCustomer] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [ConfidentialCustomer] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ConfidentialCustomer] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ConfidentialCustomer] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ConfidentialCustomer] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ConfidentialCustomer] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ConfidentialCustomer] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ConfidentialCustomer] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ConfidentialCustomer] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ConfidentialCustomer] SET  DISABLE_BROKER 
GO
ALTER DATABASE [ConfidentialCustomer] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ConfidentialCustomer] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ConfidentialCustomer] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ConfidentialCustomer] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ConfidentialCustomer] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ConfidentialCustomer] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ConfidentialCustomer] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ConfidentialCustomer] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [ConfidentialCustomer] SET  MULTI_USER 
GO
ALTER DATABASE [ConfidentialCustomer] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ConfidentialCustomer] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ConfidentialCustomer] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ConfidentialCustomer] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [ConfidentialCustomer]
GO
/****** Object:  User [SEARS2\delderk]    Script Date: 9/18/2019 7:23:23 PM ******/
CREATE USER [SEARS2\delderk] FOR LOGIN [SEARS2\delderk] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [os_spt]    Script Date: 9/18/2019 7:23:23 PM ******/
CREATE USER [os_spt] FOR LOGIN [os_spt] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [nfdt_dbconnect]    Script Date: 9/18/2019 7:23:23 PM ******/
CREATE USER [nfdt_dbconnect] FOR LOGIN [nfdt_dbconnect] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [nfdt_boss]    Script Date: 9/18/2019 7:23:23 PM ******/
CREATE USER [nfdt_boss] FOR LOGIN [nfdt_boss] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [KMART\njain3]    Script Date: 9/18/2019 7:23:24 PM ******/
CREATE USER [KMART\njain3] FOR LOGIN [KMART\njain3] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [KMART\HomeServices-HSNFDTTM-local]    Script Date: 9/18/2019 7:23:24 PM ******/
CREATE USER [KMART\HomeServices-HSNFDTTM-local] FOR LOGIN [KMART\HomeServices-HSNFDTTM-local]
GO
/****** Object:  User [KMART\HomeServices-HSNFDADMLocal]    Script Date: 9/18/2019 7:23:24 PM ******/
CREATE USER [KMART\HomeServices-HSNFDADMLocal] FOR LOGIN [KMART\HomeServices-HSNFDADMLocal]
GO
/****** Object:  User [KMART\emiller]    Script Date: 9/18/2019 7:23:24 PM ******/
CREATE USER [KMART\emiller] FOR LOGIN [KMART\emiller] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  DatabaseRole [db_execute]    Script Date: 9/18/2019 7:23:25 PM ******/
CREATE ROLE [db_execute]
GO
ALTER ROLE [db_execute] ADD MEMBER [SEARS2\delderk]
GO
ALTER ROLE [db_owner] ADD MEMBER [SEARS2\delderk]
GO
ALTER ROLE [db_datareader] ADD MEMBER [SEARS2\delderk]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [SEARS2\delderk]
GO
ALTER ROLE [db_execute] ADD MEMBER [os_spt]
GO
ALTER ROLE [db_owner] ADD MEMBER [os_spt]
GO
ALTER ROLE [db_datareader] ADD MEMBER [os_spt]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [os_spt]
GO
ALTER ROLE [db_execute] ADD MEMBER [nfdt_dbconnect]
GO
ALTER ROLE [db_datareader] ADD MEMBER [nfdt_dbconnect]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [nfdt_dbconnect]
GO
ALTER ROLE [db_execute] ADD MEMBER [nfdt_boss]
GO
ALTER ROLE [db_datareader] ADD MEMBER [nfdt_boss]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [nfdt_boss]
GO
ALTER ROLE [db_owner] ADD MEMBER [KMART\njain3]
GO
ALTER ROLE [db_datareader] ADD MEMBER [KMART\njain3]
GO
ALTER ROLE [db_execute] ADD MEMBER [KMART\HomeServices-HSNFDTTM-local]
GO
ALTER ROLE [db_datareader] ADD MEMBER [KMART\HomeServices-HSNFDTTM-local]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [KMART\HomeServices-HSNFDTTM-local]
GO
ALTER ROLE [db_execute] ADD MEMBER [KMART\HomeServices-HSNFDADMLocal]
GO
ALTER ROLE [db_owner] ADD MEMBER [KMART\HomeServices-HSNFDADMLocal]
GO
ALTER ROLE [db_datareader] ADD MEMBER [KMART\HomeServices-HSNFDADMLocal]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [KMART\HomeServices-HSNFDADMLocal]
GO
ALTER ROLE [db_datareader] ADD MEMBER [KMART\emiller]
GO
/****** Object:  StoredProcedure [dbo].[PROC_Actions_Insert_BillPrint]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





/*************************************************************************
	Procedure Name: PROC_Actions_Insert_BillPrint
	Returns:		Failure Code (@ErrorCode)
	Primary User:	Everyone
	Designer:		Brandi Fischer
	Company:		Sears, Roebuck and Co
	Last Updated:	Tue Sep 18 13:24:45 EST 2018 @810 /Internet Time/
	Updated by:		
	Information:	
	************************************************************************
	Version:		1.00
	Date:			Tue Sep 18 13:24:45 EST 2018 @810 /Internet Time/
	Updated By:		Brandi Fischer
	************************************************************************
	Version:		1.01
	Date:			Mon Jan 28 13:37:00 EST 2018 @810 /Internet Time/
	Updated By:		Brandi Fischer
	Update:			Add If statement to check for print week day and count.
***************************************************************************/
CREATE Procedure [dbo].[PROC_Actions_Insert_BillPrint]
	--@ErrorCode varchar(150) = NULL OUTPUT
AS
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

/*Set Print variables Weekday to Print and number of bills to print*/
DECLARE @billcount AS INTEGER = (SELECT NumericValue FROM ConfidentialCustomer.dbo.TBL_LookupCodes WITH (NOLOCK) WHERE OwnerType = 'CustomerBillPrint' AND OwnerID ='ProcedureVariable' AND Description = 'BillCount')

DECLARE @printweekday AS Integer = (SELECT NumericValue FROM ConfidentialCustomer.dbo.TBL_LookupCodes WITH (NOLOCK) WHERE OwnerType = 'CustomerBillPrint' AND OwnerID ='ProcedureVariable' AND Description = 'PrintDayofWeek')

DECLARE @todayweekday AS Integer = (SELECT DayofWeek FROM ServiceOrder.dbo.TBL_FiscalCalendar WITH (NOLOCK) WHERE CalendarDate = CAST(GETDATE() AS DATE))



--PRINT @billcount
--PRINT @printweekday
--PRINT @todayweekday

IF @printweekday = @todayweekday AND @billcount > 0
BEGIN
	BEGIN
		UPDATE CB
		SET 
			ActionID = NULL
			,ActionType = NULL
			,CB.StatusID = CB.StatusID + 1
			,CB.InvoiceNumber = (CASE WHEN CB.InvoiceNumber IN (0, NULL) THEN CB.RNInvoiceNumber ELSE CB.InvoiceNumber END) 
			,CB.InstallationCount = CB.InstallationCount + 1
			,CB.LastBillDate = CAST(GETDATE() AS DATE)
			,CB.LastModifiedEnterpriseID = 'SYSTEM'
			,CB.LastModifiedDateTime = GETDATE()
		FROM (
			SELECT TOP (@billcount) CB.*, RN.RNInvoiceNumber
			FROM 
				ConfidentialCustomer.dbo.TBL_CustomerBills CB
			INNER JOIN (
				SELECT 
					BillID
					,CustomerID
					,MAX(ServiceOrder) AS ServiceOrder
					,MIN(EntrySourceDate) AS RouteDate
					,COUNT(DISTINCT ServiceOrder) AS SOCount
					,SUM(EntryAmount) AS EntryAmount
				FROM 
					ConfidentialCustomer.dbo.TBL_CustomerLedger WITH (NOLOCK)
				WHERE 
					BillID IS NOT NULL
					AND
					EntryType = 100
--					AND EntrySourceType = 'RefuseToPay' /*Added on 7/18/2019 to only process Refuse To Pay items*/
				GROUP BY 
					BillID
					,CustomerID
				) CLE
			ON 
				CB.EntryID = CLE.BillID
			INNER JOIN (
				SELECT
					BillID
					,SUM(EntryAmount) AS EntryAmount
				FROM 
					ConfidentialCustomer.dbo.TBL_CustomerLedger WITH (NOLOCK)
				WHERE 
					BillID IS NOT NULL
					AND
					EntryType > 0
--					AND EntrySourceType = 'RefuseToPay' /*Added on 7/18/2019 to only process Refuse To Pay items*/
				GROUP BY BillID
				) CL
			ON 
				CB.EntryID = CL.BillID
			LEFT JOIN (
				SELECT [CustomerID]
					,MAX([EntrySourceDate]) AS PayMaxDate
					,SUM([EntryAmount]) AS AcctBal
				FROM 
					[ConfidentialCustomer].[dbo].[TBL_CustomerLedger] WITH (NOLOCK)
				GROUP BY 
					[CustomerID]
				) CLC
			ON 
				CB.CustomerID = CLC.[CustomerID]
			INNER JOIN 
				ServiceOrder.dbo.TBL_NPSServiceOrderCustomerLookaside LA WITH (NOLOCK)
			ON 
				LA.AddressLocation = 1 
				AND 
				CLE.CustomerID = LA.Customer
				AND
				CLE.ServiceOrder = LA.ServiceOrder
			INNER JOIN (
				SELECT 
					EntryID
					,CAST(CustomerID AS VARCHAR(20)) + CAST(ROW_NUMBER() 
					OVER(PARTITION BY CustomerID 
					ORDER BY ISNULL(LastBillDate,'2100-01-01') ASC) AS VARCHAR(10))
					AS RNInvoiceNumber
				FROM 
					ConfidentialCustomer.dbo.TBL_CustomerBills WITH (NOLOCK)
				) RN
			ON 
				CB.EntryID = RN.EntryID
			WHERE 
				CB.StatusID IN (2,4,6)
				AND (CB.ReasonID IS NULL OR CB.ReasonID <> -1)
				AND ABS(CB.CurrentBalance) > 20
				AND ABS(CB.CurrentBalance) = ABS(CL.EntryAmount)
				AND ABS(CB.CurrentBalance) = ABS(CLC.AcctBal)
				AND CLE.SOCount = 1
				AND CLC.AcctBal < 0
				AND CLE.RouteDate > '2017-02-01'
			ORDER BY
				ISNULL(CB.LastBillDate,'1900-01-01') ASC
				,CLE.RouteDate ASC
				,ABS(CB.CurrentBalance) DESC
		) CB;
	END


	BEGIN
		INSERT INTO ConfidentialCustomer.dbo.TBL_Actions
		(ActionType
		,OwnerID
		,OwnerType
		,StatusID
		,CreatedEnterpriseID
		,CreatedDateTime)
		SELECT
		'Actions'
		,CB.EntryID
		,CB.EntryType
		,CB.StatusID
		,CB.LastModifiedEnterpriseID
		,CB.LastModifiedDateTime
		FROM ConfidentialCustomer.dbo.TBL_CustomerBills CB WITH (NOLOCK)
		WHERE CB.ActionID IS NULL
		AND CB.LastBillDate = CAST(GETDATE() AS DATE);

		UPDATE CB
		SET CB.ActionID = A.ActionID
		,CB.ActionType = A.ActionType
		FROM [ConfidentialCustomer].[dbo].[TBL_CustomerBills] CB WITH (NOLOCK)
		INNER JOIN [ConfidentialCustomer].[dbo].[TBL_Actions] A WITH (NOLOCK)
		ON CB.EntryID = A.OwnerID
		AND CB.EntryType = A.OwnerType
		AND CB.StatusID = A.StatusID
		AND CB.LastModifiedEnterpriseID = A.CreatedEnterpriseID
		AND CB.LastModifiedDateTime = A.CreatedDateTime
		WHERE CB.LastBillDate =  CAST(GETDATE() AS DATE);

	END

	BEGIN
		SELECT 
			CB.EntryID
			,CB.ServiceType
			,COALESCE(LT.Description, LTRIM(RTRIM(SO.ServicingOrganization))) AS Template
			,COALESCE(CO.CustomerName
				,CASE WHEN LA.CustomerNamePrefix <> ''
				THEN LTRIM(RTRIM(LA.CustomerNamePrefix)) ELSE '' END
				+ CASE WHEN LA.CustomerFirstName <> ''
				THEN ' ' + LTRIM(RTRIM(LA.CustomerFirstName)) ELSE '' END
				+ CASE WHEN LA.CustomerMiddleName <> ''
				THEN ' ' + LTRIM(RTRIM(LA.CustomerMiddleName)) ELSE '' END
				+ CASE WHEN LA.CustomerLastName <> ''
				THEN ' ' + LTRIM(RTRIM(LA.CustomerLastName)) ELSE '' END
				+ CASE WHEN LA.CustomerLastNameAdditional <> ''
				THEN ' ' + LTRIM(RTRIM(LA.CustomerLastNameAdditional)) ELSE '' END 
				+ CASE WHEN LA.CustomerNameSuffix <> ''
				THEN ' ' + LTRIM(RTRIM(LA.CustomerNameSuffix)) ELSE '' END) 
			AS CustomerName
			,COALESCE(CO.CustomerStreetAddress, LTRIM(RTRIM(REPLACE(REPLACE(LA.CustomerStreetAddress,'# NONE -',''),'# NONE','')))) AS CustomerStreetAddress 
			,LTRIM(RTRIM(COALESCE(CO.CustomerCity, LA.CustomerCity))) 
			+', ' + LTRIM(RTRIM(COALESCE(CO.CustomerState, LA.CustomerState))) 
			+' ' + LTRIM(RTRIM(COALESCE(CO.CustomerZip, LA.CustomerZip))) AS CustomerCityStateZip
			,CB.InvoiceNumber AS PayID
			,CL.ServiceDate
			,CB.InstallationCount AS InvoiceNumber
			,CB.LastBillDate
			,CB.ClaimNumber
			,CB.BillType
			,BT.ClientName
			,(BT.ContactDisplayName + ', ' + BT.ContactStreetAddress + ', ' + BT.ContactCity + ', ' + BT.ContactState + ' ' + BT.ContactZipCode + ', ' + BT.ContactPhoneNumber) AS ClientContactDetail
			,CL.ServiceOrder
			,SO.ManufacturerBrandName AS Brand
			,UPPER(COALESCE(MC.MD_SM_CODE_DESC2, SO.MerchandiseCode)) AS ItemDesc
			,SO.SerialNumber
			,CL.ServiceUnit + CL.ServiceOrder AS ServiceNumber
			,CB.CurrentBalance AS AmountDue
			,ROUND(CB.CurrentBalance/(4 - CB.InstallationCount), 2) AS FirstInstallment
			,CB.CurrentBalance - (((4 - CB.InstallationCount) - 1) * ROUND(CB.CurrentBalance/(4 - CB.InstallationCount), 2)) AS FinalInstallment
		FROM 
			[ConfidentialCustomer].[dbo].[TBL_CustomerBills] CB WITH (NOLOCK)
		LEFT JOIN ConfidentialCustomer.dbo.TBL_LookupCodes LT WITH (NOLOCK) 
		ON LT.IsEnabled = 1 AND LT.OwnerID = 'ServiceType' 
		AND CB.EntryType = LT.OwnerType AND CB.ServiceType = LT.NumericValue
		INNER JOIN (
			SELECT 
				BillID
				,ServiceUnit
				,ServiceOrder
				,MAX(EntrySourceDate) AS ServiceDate
			FROM 
				ConfidentialCustomer.dbo.TBL_CustomerLedger WITH (NOLOCK) 
			WHERE 
				BillID IS NOT NULL
				AND
				EntryType = 100
			GROUP BY
				BillID
				,ServiceUnit
				,ServiceOrder
			) CL 
		ON 
			CB.EntryID = CL.BillID 
		INNER JOIN 
			ServiceOrder.dbo.TBL_NPSServiceOrderCustomerLookaside LA WITH (NOLOCK) 
		ON 
			LA.AddressLocation = 1 
			AND CL.ServiceUnit = LA.ServiceUnit 
			AND CL.ServiceOrder = LA.ServiceOrder 
		LEFT JOIN ConfidentialCustomer.dbo.TBL_BillType BT WITH (NOLOCK)
		ON CB.BillType = BT.EntryID
		LEFT JOIN ConfidentialCustomer.dbo.TBL_CustomerContactOverride CO WITH (NOLOCK) 
		ON CB.EntryType = CO.OwnerType AND CB.EntryID = CO.OwnerID
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrder SO  WITH (NOLOCK)
		ON CL.ServiceUnit = SO.ServiceUnit AND CL.ServiceOrder = SO.ServiceOrder
		LEFT JOIN ServiceOrder.dbo.TBL_MerchandiseCode MC WITH (NOLOCK)
		ON SO.MerchandiseCode = MC.MD_SM_CODE
		WHERE CB.LastBillDate = CAST(GETDATE() AS DATE)
	END	
END




GO
/****** Object:  StoredProcedure [dbo].[PROC_Actions_Insert_CustomerBills]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*************************************************************************
	Procedure Name: PROC_Actions_Insert_CustomerBills
	Returns:		Failure Code (@ErrorCode)
	Primary User:	Everyone
	Designer:		Brandi Fischer
	Company:		Sears, Roebuck and Co
	Last Updated:	
	Updated by:		Brandi Fischer
	Information:	Procedure for manually updating bill status
	************************************************************************
	Version:		1.00
	Date:			
	Updated By:		Brandi Fischer
	
	Version:		1.01
	Date:			Fri Oct 15 10:51:14 EST 2018 @810 /Internet Time/
	Updated By:		Brandi Fischer
	Notes:			Edit Manuel Offset proceduer execution to pass OffsetType.
	
	Version:		1.02
	Date:			Mon Oct 22 8:17:22 EST 2018 @810 /Internet Time/
	Updated By:		Brandi Fischer
	Notes:			Added Communcation table insert.
	
***************************************************************************/
CREATE Procedure [dbo].[PROC_Actions_Insert_CustomerBills]
	@ActionID int = Null OUTPUT, /* Unique ID number for the comment.  To update an existing comment, supply the comment ID.  Otherwise, an insert will be done. */
	@OwnerType varchar(30), /* Ownertype from the ownertype process managed for a database side applicaiton identification. */
	@OwnerID int, /* OwnerID number from the calling applicaiton. */
	@SecondaryType varchar(30) = NULL, /* Ownertype from the ownertype process managed for a database side applicaiton identification. */
	@SecondaryID int = NULL, /* OwnerID number from the calling application. */
	@StatusID int, /* StatusID number from the calling application. */
	@ReasonID int = NULL, /* ReasonID number from the calling application. */
	@EnterpriseID varchar(8), /* Employee LDAP ID making the insert or update.  Upper case. */
	@ServiceUnit char(7) = NULL,
	@ServiceOrder char(8) = NULL,
	@NPSID varchar(7) = NULL,
	@CustomerID int = NULL,
	@Amount decimal (18,4) = NULL,
	@ManualOffsetID int = NULL,
	@ErrorCode varchar(150) = NULL OUTPUT /* error message if any during processing */ 
AS
	SET NOCOUNT ON

	SET @EnterpriseID = LTRIM(RTRIM(UPPER(@EnterpriseID)))
	SET @OwnerType = LTRIM(RTRIM(@OwnerType))


	IF @OwnerType = '' /* set @OwnerType to null if empty string */
		SET @OwnerType = NULL
	IF @EnterpriseID = '' /* set @EnterpriseID to null if empty string */
		SET @EnterpriseID = NULL
	IF @SecondaryID = 0 /* set @CommentID to null if zero */
		SET @SecondaryID = NULL
	IF @SecondaryType = '' /* set @SecondaryType to null if empty string */
		SET @SecondaryType = NULL
	IF @OwnerID = 0 /* set @OwnerID to null if zero */
		SET @OwnerID = NULL
	IF @CustomerID = 0 /* set @OwnerID to null if zero */
		SET @CustomerID = NULL
	IF @Amount = 0 /* set @OwnerID to null if zero */
		SET @Amount = NULL
	
	/*Discuss: ManualOffset is Secondary What is Multiple Reasons*/
	--If @ReasonID IS NOT NULL
	--	SET @SecondaryType = 'ReasonID'
	--	SET @SecondaryID = @ReasonID


	IF @StatusID Is Null /* Return error code if @Comment is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for Status, it is Required.'
		Return
	END
	
	IF @OwnerType Is Null /* Return error code if @OwnerType is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for OwnerType, it is Required.'
		Return
	END	
	IF @OwnerID Is Null /* Return error code if @OwnerID is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for OwnerID, it is Required.'
		Return
	END
	IF @EnterpriseID Is Null /* Return error code if @EnterpriseID is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for EmpID, it is Required.'
		Return
	END
	
	IF @Amount IS NOT NULL AND @StatusID IN (21,22) AND @ReasonID <> 10 
		BEGIN
			DECLARE @OffsetType INT = (SELECT CASE WHEN @ReasonID = 20 THEN 201 ELSE 202 END)
			DECLARE @EntryID INT
			DECLARE @LedgerEntryID INT
			IF @CustomerID IS NULL
				SET @CustomerID = (SELECT CustomerID FROM ConfidentialCustomer.dbo.TBL_CustomerBills WHERE EntryID = @OwnerID)
			If @ServiceUnit IS NULL OR @ServiceOrder IS NULL
				BEGIN
					DECLARE @EntryIDLedgerOffset INT = (SELECT TOP 1 EntryID FROM ConfidentialCustomer.dbo.TBL_CustomerLedger WHERE BillID = @OwnerID AND EntryAmount = COALESCE((SELECT MAX(EntryAmount) FROM ConfidentialCustomer.dbo.TBL_CustomerLedger WHERE BillID = @OwnerID AND OffsetEntryID IS NULL),(SELECT MAX(EntryAmount) FROM ConfidentialCustomer.dbo.TBL_CustomerLedger WHERE BillID = @OwnerID)) ORDER BY EntryID)
					
					SET @ServiceUnit = (SELECT ServiceUnit FROM ConfidentialCustomer.dbo.TBL_CustomerLedger WHERE EntryID = @EntryIDLedgerOffset)
					SET @ServiceOrder = (SELECT ServiceOrder FROM ConfidentialCustomer.dbo.TBL_CustomerLedger WHERE EntryID = @EntryIDLedgerOffset)
				END
			EXEC [ConfidentialCustomer].[dbo].[PROC_Insert_ManualOffset] 
			   @EntryID OUTPUT
			  ,@OffsetType
			  ,@OwnerType
			  ,@OwnerID
			  ,@CustomerID
			  ,NULL
			  ,NULL
			  ,@EnterpriseID
			  ,NULL
			  ,NULL
			  ,NULL
			  ,NULL
			  ,@Amount
			  ,@ReasonID
			  ,@ServiceUnit
			  ,@ServiceOrder
			  ,@LedgerEntryID OUTPUT
			  ,@ErrorCode OUTPUT
						     			  
			SET @ManualOffsetID = @EntryID
			
			IF @ErrorCode Is NOT NULL 
				BEGIN
					RETURN
				END
			
			If @LedgerEntryID IS NOT NULL
				BEGIN
					UPDATE ConfidentialCustomer.dbo.TBL_CustomerLedger				
					SET BillType = @OwnerType
					,BillID = @OwnerID
					WHERE EntryID = @LedgerEntryID 
					
					UPDATE ConfidentialCustomer.dbo.TBL_CustomerLedger
					SET OffsetEntryID = @LedgerEntryID
					WHERE EntryID = @EntryIDLedgerOffset AND OffsetEntryID IS NULL	
					
					UPDATE CB
					SET AdjustmentAmount = CL.AdjustmentAmount 
					,PaymentAmount = CL.PaymentAmount
					,CurrentBalance = CB.OriginalBalance - (ISNULL(CL.AdjustmentAmount,0.00)+ ISNULL(CL.PaymentAmount,0.00))
					FROM ConfidentialCustomer.dbo.TBL_CustomerBills CB
					LEFT JOIN 
						(SELECT 
							BillID
							,SUM(CASE WHEN EntryType = 202 THEN EntryAmount ELSE 0.00 END) AS AdjustmentAmount
							,SUM(CASE WHEN EntryType IN (200,201) THEN EntryAmount ELSE 0.00 END) AS PaymentAmount
						FROM 
							ConfidentialCustomer.dbo.TBL_CustomerLedger 
						WHERE 
							EntryType IN (200,201,202) 
						GROUP BY 
							BillID) CL
					ON CB.EntryID = CL.BillID
					WHERE CB.EntryID = @OwnerID
								
				END
				
					
			IF @ManualOffsetID IS NOT NULL 
				SET @SecondaryType = 'ManualOffset'
				SET @SecondaryID = @ManualOffsetID
				  
		  END
		  	
	/* if a comment ID was passed in, update it. otherwise create a new row */
	IF @ActionID IS NULL /* add row */
		BEGIN
			INSERT INTO ConfidentialCustomer.dbo.TBL_Actions
			(ActionType, OwnerType, OwnerID, SecondaryType, SecondaryID, StatusID, CreatedEnterpriseID, CreatedDateTime)
			VALUES('Actions', @OwnerType, @OwnerID, @SecondaryType, @SecondaryID, @StatusID, @EnterpriseID, GETDATE())
		
			SET @ActionID = scope_identity()
		
			UPDATE ConfidentialCustomer.dbo.TBL_CustomerBills
			SET ActionID = @ActionID
				,StatusID = @StatusID
				,ReasonID = @ReasonID
				,LastModifiedEnterpriseID = @EnterpriseID
				,LastModifiedDateTime = GETDATE()
			WHERE
				EntryID = @OwnerID
		END		
	
	/*Check Bill Balance and Close if <$1*/	
	DECLARE @CurrentBalance AS DECIMAL (18,4) = (SELECT CurrentBalance FROM ConfidentialCustomer.dbo.TBL_CustomerBills WHERE EntryID = @OwnerID)
	DECLARE @AutoCloseActionID AS INT = NULL
	IF @CurrentBalance <= 1.0000 AND @StatusID NOT IN (50,100) /* add row */
		BEGIN
			SET @StatusID = (SELECT (CASE WHEN PaymentAmount > 0 THEN 50 WHEN AdjustmentAmount > 0 THEN 100 END) AS StatusID FROM ConfidentialCustomer.dbo.TBL_CustomerBills WHERE EntryID = @OwnerID)			
			SET @SecondaryType = NULL
			SET @SecondaryID = NULL
			SET @EnterpriseID = 'SYSTEM'
			
			IF @StatusID IN (50,100)
			BEGIN
				INSERT INTO ConfidentialCustomer.dbo.TBL_Actions
				(ActionType, OwnerType, OwnerID, SecondaryType, SecondaryID, StatusID, CreatedEnterpriseID, CreatedDateTime)
				VALUES('Actions', @OwnerType, @OwnerID, @SecondaryType, @SecondaryID, @StatusID, @EnterpriseID, GETDATE())
			
				DECLARE @CloseActionID AS Integer = scope_identity()
			
				UPDATE ConfidentialCustomer.dbo.TBL_CustomerBills
				SET ActionID = @CloseActionID
					,StatusID = @StatusID
					,LastModifiedEnterpriseID = @EnterpriseID
					,LastModifiedDateTime = GETDATE()
				WHERE
					EntryID = @OwnerID
					
			SET @AutoCloseActionID = scope_identity()
			END
		END	
		
IF @StatusID IN (21,22,40,50,100)
	BEGIN
	DECLARE @CommunicationType AS INT = (SELECT CASE WHEN @StatusID IN (21,22) THEN 1 WHEN @StatusID = 40 THEN 2 WHEN @StatusID IN (50, 100) THEN 3 END)		
		
	INSERT INTO [ConfidentialCustomer].[dbo].[TBL_Communications]
           ([EntryType]
           ,[OwnerID]
           ,[OwnerType]
           ,[CustomerID]
           ,[CommunicationType]
           ,[CommunicationMethod]
           ,[SentDateTime]
           ,[CreatedEnterpriseID]
           ,[CreatedDateTime])
     VALUES
           ('Communications'
           ,COALESCE(@AutoCloseActionID,@ActionID)
           ,'Actions'
           ,@CustomerID
           ,@CommunicationType
           ,NULL
           ,NULL
           ,'SYSTEM'
           ,GETDATE())	
     END

GO
/****** Object:  StoredProcedure [dbo].[PROC_Actions_Insert_NA]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





/*************************************************************************
	Procedure Name: PROC_Actions_Insert_NA
	Returns:		Failure Code (@ErrorCode)
	Primary User:	Everyone
	Designer:		Rob O'Brien
	Company:		Sears, Roebuck and Co
	Last Updated:	Fri Mar 02 12:26:28 CST 2018 @810 /Internet Time/
	Updated by:		Rob O'Brien
	Information:	Procedure for adding or updating a comment
	************************************************************************
	Version:		1.00
	Date:			Fri Mar 02 12:26:36 CST 2018 @810 /Internet Time/
	Updated By:		Rob O'Brien
***************************************************************************/
CREATE Procedure [dbo].[PROC_Actions_Insert_NA]
	@ActionID int = Null OUTPUT, /* Unique ID number for the comment.  To update an existing comment, supply the comment ID.  Otherwise, an insert will be done. */
	@OwnerType varchar(30), /* Ownertype from the ownertype process managed for a database side applicaiton identification. */
	@OwnerID int, /* OwnerID number from the calling applicaiton. */
	@SecondaryType varchar(30) = NULL, /* Ownertype from the ownertype process managed for a database side applicaiton identification. */
	@SecondaryID int = NULL, /* OwnerID number from the calling application. */
	@StatusID int, /* StatusID number from the calling application. */
	@EnterpriseID varchar(8), /* Employee LDAP ID making the insert or update.  Upper case. */
	@ServiceUnit char(7) = NULL,
	@ServiceOrder char(8) = NULL,
	@NPSID varchar(7) = NULL,
	@CustomerID int = NULL,
	@ErrorCode varchar(150) = NULL OUTPUT /* error message if any during processing */ 
AS
	SET NOCOUNT ON

	DECLARE @PMOwnerType varchar(30)
	DECLARE @PMOwnerID int

	SET @EnterpriseID = LTRIM(RTRIM(UPPER(@EnterpriseID)))
	SET @OwnerType = LTRIM(RTRIM(@OwnerType))


	IF @OwnerType = '' /* set @OwnerType to null if empty string */
		SET @OwnerType = NULL
	IF @EnterpriseID = '' /* set @EnterpriseID to null if empty string */
		SET @EnterpriseID = NULL
	IF @SecondaryID = 0 /* set @CommentID to null if zero */
		SET @SecondaryID = NULL
	IF @OwnerID = 0 /* set @OwnerID to null if zero */
		SET @OwnerID = NULL
	IF @SecondaryType = '' /* set @SecondaryType to null if empty string */
		SET @SecondaryType = NULL


	IF @StatusID Is Null /* Return error code if @Comment is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for Status, it is Required.'
		Return
	END
	IF @OwnerID Is Null /* Return error code if @OwnerID is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for OwnerID, it is Required.'
		Return
	END
	IF @EnterpriseID Is Null /* Return error code if @EnterpriseID is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for EmpID, it is Required.'
		Return
	END
	
	IF @OwnerType Is Null /* Return error code if @OwnerType is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for OwnerType, it is Required.'
		Return
	END	
	
	/* if a comment ID was passed in, update it. otherwise create a new row */
	IF @ActionID IS NULL /* add row */
		BEGIN
			INSERT INTO ConfidentialCustomer.dbo.TBL_Actions
			(OwnerType, OwnerID, SecondaryType, SecondaryID, StatusID, CreatedEnterpriseID, CreatedDateTime)
			VALUES(@OwnerType, @OwnerID, @SecondaryType, @SecondaryID, @StatusID, @EnterpriseID, GETDATE())
		
			SET @ActionID = scope_identity()
		
			UPDATE ConfidentialCustomer.dbo.TBL_PaymentMatch
			SET ActionID = @ActionID
				,LastModifiedEnterpriseID = @EnterpriseID
				,LastModifiedDateTime = GETDATE()
			WHERE
				PaymentMatchID = @OwnerID
		END		
		IF @ActionID = 16
			BEGIN
				SET @PMOwnerType = (SELECT OwnerType FROM ConfidentialCustomer.dbo.TBL_PaymentMatch WHERE PaymentMatchID = @OwnerID)
				SET @PMOwnerID = (SELECT OwnerID FROM ConfidentialCustomer.dbo.TBL_PaymentMatch WHERE PaymentMatchID = @OwnerID)
				IF @PMOwnerType = 'HSPOS'
					BEGIN
						UPDATE ConfidentialCustomer.dbo.TBL_HSPOS
						SET ServiceUnit = @ServiceUnit
							,ServiceOrder = @ServiceOrder
							,LastModifiedDateTime = GETDATE()
						WHERE 
							EntryID = @PMOwnerID

					END

				IF @PMOwnerType = 'TempusCard'
					BEGIN
						UPDATE ConfidentialCustomer.dbo.TBL_TempusCard
						SET ServiceUnit = @ServiceUnit
							,ServiceOrder = @ServiceOrder
							,LastModifiedDateTime = GETDATE()
						WHERE 
							SystemCode = @PMOwnerID
					END

				IF @PMOwnerType = 'FirstDataPayments'
					BEGIN
						UPDATE ConfidentialCustomer.dbo.TBL_FirstDataPayments
						SET ServiceUnit = @ServiceUnit
							,ServiceOrder = @ServiceOrder
							,LastModifiedDateTime = GETDATE()
						WHERE 
							PaymentID = @PMOwnerID
					END

				IF @PMOwnerType = 'Telecheck'
					BEGIN
						UPDATE ConfidentialCustomer.dbo.TBL_Telecheck
						SET ServiceUnit = @ServiceUnit
							,ServiceOrder = @ServiceOrder
							,LastModifiedDateTime = GETDATE()
						WHERE 
							EntryID = @PMOwnerID
					END
			END

			/*
			ELSE /* update row */
				BEGIN
		
					UPDATE TBL_Actions SET
						StatusID = @StatusID,
						ModifiedEnterpriseID = @EnterpriseID,
						ModifiedDateTime = GETDATE()
					WHERE 
						ActionID = @ActionID
				END
			*/
	


	









GO
/****** Object:  StoredProcedure [dbo].[PROC_Actions_Insert_PaymentMatch]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



/*************************************************************************
	Procedure Name: PROC_Actions_Insert_PaymentMatch
	Returns:		Failure Code (@ErrorCode)
	Primary User:	Everyone
	Designer:		Rob O'Brien
	Company:		Sears, Roebuck and Co
	Last Updated:	Fri Mar 02 12:26:28 CST 2018 @810 /Internet Time/
	Updated by:		Rob O'Brien
	Information:	Procedure for adding or updating a comment
	************************************************************************
	Version:		1.00
	Date:			Fri Mar 02 12:26:36 CST 2018 @810 /Internet Time/
	Updated By:		Rob O'Brien
	************************************************************************
	Version:		1.01
	Date:			Fri Jul 10 12:26:36 CST 2018 @810 /Internet Time/
	Updated By:		Rob O'Brien

	Notes:			Updated the procedure to use the ancillary status from the lookup table to determine if the base table needs to be updated
	Version:		1.02
	Date:			Fri Aug 31 12:20:00 EST 2018 @810 /Internet Time/
	Updated By:		Brandi Fischer

	Notes:			Updated the procedure update and insert when payment source is PNC Check.
***************************************************************************/
CREATE Procedure [dbo].[PROC_Actions_Insert_PaymentMatch]
	@ActionID int = Null OUTPUT, /* Unique ID number for the comment.  To update an existing comment, supply the comment ID.  Otherwise, an insert will be done. */
	@OwnerType varchar(30), /* Ownertype from the ownertype process managed for a database side applicaiton identification. */
	@OwnerID int, /* OwnerID number from the calling applicaiton. */
	@SecondaryType varchar(30) = NULL, /* Ownertype from the ownertype process managed for a database side applicaiton identification. */
	@SecondaryID int = NULL, /* OwnerID number from the calling application. */
	@StatusID int, /* StatusID number from the calling application. */
	@EnterpriseID varchar(8), /* Employee LDAP ID making the insert or update.  Upper case. */
	@ServiceUnit char(7) = NULL,
	@ServiceOrder char(8) = NULL,
	@NPSID varchar(7) = NULL,
	@CustomerID int = NULL,
	@ErrorCode varchar(150) = NULL OUTPUT /* error message if any during processing */ 
AS
	SET NOCOUNT ON

	DECLARE @PMOwnerType varchar(30)
	DECLARE @PMOwnerID int
	DECLARE @AncillaryStatus int

	SET @EnterpriseID = LTRIM(RTRIM(UPPER(@EnterpriseID)))
	SET @OwnerType = LTRIM(RTRIM(@OwnerType))


	IF @OwnerType = '' /* set @OwnerType to null if empty string */
		SET @OwnerType = NULL
	IF @EnterpriseID = '' /* set @EnterpriseID to null if empty string */
		SET @EnterpriseID = NULL
	IF @SecondaryID = 0 /* set @CommentID to null if zero */
		SET @SecondaryID = NULL
	IF @OwnerID = 0 /* set @OwnerID to null if zero */
		SET @OwnerID = NULL
	IF @CustomerID = 0 /* set @OwnerID to null if zero */
		SET @CustomerID = NULL
	IF @CustomerID = '' /* set @OwnerID to null if zero */
		SET @CustomerID = NULL
	IF @SecondaryType = '' /* set @SecondaryType to null if empty string */
		SET @SecondaryType = NULL


	IF @StatusID Is Null /* Return error code if @Comment is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for Status, it is Required.'
		Return
	END
	IF @OwnerID Is Null /* Return error code if @OwnerID is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for OwnerID, it is Required.'
		Return
	END
	IF @EnterpriseID Is Null /* Return error code if @EnterpriseID is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for EmpID, it is Required.'
		Return
	END
	
	IF @OwnerType Is Null /* Return error code if @OwnerType is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for OwnerType, it is Required.'
		Return
	END	
	
	/* if a comment ID was passed in, update it. otherwise create a new row */
	IF @ActionID IS NULL /* add row */
		BEGIN
			INSERT INTO ConfidentialCustomer.dbo.TBL_Actions
			(ActionType, OwnerType, OwnerID, SecondaryType, SecondaryID, StatusID, CreatedEnterpriseID, CreatedDateTime)
			VALUES('Actions', @OwnerType, @OwnerID, @SecondaryType, @SecondaryID, @StatusID, @EnterpriseID, GETDATE())
		
			SET @ActionID = scope_identity()
		
			UPDATE ConfidentialCustomer.dbo.TBL_PaymentMatch
			SET ActionID = @ActionID
				,StatusID = @StatusID
				,LastModifiedEnterpriseID = @EnterpriseID
				,LastModifiedDateTime = GETDATE()
			WHERE
				EntryID = @OwnerID
		END		
		SET @AncillaryStatus = (SELECT AncillaryStatus FROM ConfidentialCustomer.dbo.TBL_LookupCodes WITH (NOLOCK) WHERE OwnerType = 'PaymentMatch' AND OwnerID = 'StatusID' AND NumericValue = @StatusID) 
		IF @AncillaryStatus IN (1) 
			BEGIN
			IF NOT @StatusID = 6
				BEGIN
					SET @PMOwnerType = (SELECT OwnerType FROM ConfidentialCustomer.dbo.TBL_PaymentMatch WHERE EntryID = @OwnerID)
					SET @PMOwnerID = (SELECT OwnerID FROM ConfidentialCustomer.dbo.TBL_PaymentMatch WHERE EntryID = @OwnerID)
				--	IF @CustomerID = NULL
				--		BEGIN
						SET @CustomerID = (SELECT CustomerID FROM ServiceOrder.dbo.TBL_ServiceOrder WHERE ServiceUnit = @ServiceUnit AND ServiceOrder = @ServiceOrder)
				--		END
					IF @PMOwnerType = 'HSPOS'
						BEGIN
							UPDATE ConfidentialCustomer.dbo.TBL_HSPOS
							SET ServiceUnit = @ServiceUnit
								,ServiceOrder = @ServiceOrder
								,CustomerID = @CustomerID
								,CustomerIDUpdateSource = @OwnerType
								,CustomerIDUpdateSourceID = @OwnerID
								,LastModifiedEnterpriseID = @EnterpriseID
								,LastModifiedDateTime = GETDATE()
							WHERE 
								EntryID = @PMOwnerID

							INSERT INTO ConfidentialCustomer.dbo.TBL_CustomerLedger
								(EntryType
								,CustomerID
								,EntryAmount
								,EntryAmountParts
								,EntryAmountLabor
								,EntryAmountOther
								,EntryAmountTax
								,EntrySourceType
								,EntrySourceID
								,EntrySourceConfidence
								,EntrySourceDate
								,ServiceUnit
								,ServiceOrder
								,PurchaseOrder
								,EntrySourceCreatedBy)
							SELECT 
								200
								,h.CustomerID
								,h.total_transaction_amount AS EntryAmount
								,NULL AS EntryAmountParts
								,NULL AS EntryAmountLabor
								,NULL AS EntryAmountOther
								,NULL AS EntryAmountTax
								,'HSPOS' AS EntrySourceType
								,h.EntryID AS EntrySourceID
								,90 AS EntrySourceConfidence
								,h.transaction_date AS EntrySourceDate
								,h.ServiceUnit
								,h.ServiceOrder
								,NULL AS PurchaseOrder
								,'SYSTEM' AS EntrySourceCreatedBy
							FROM 
								ConfidentialCustomer.dbo.TBL_HSPOS h WITH (NOLOCK)
							LEFT JOIN
								(SELECT * FROM ConfidentialCustomer.dbo.TBL_CustomerLedger WITH (NOLOCK) WHERE EntrySourceType = 'HSPOS') d
								ON h.CurrentLedgerEntryID = d.EntryID
							WHERE
								h.transaction_type_cd = 'SAL'
								AND NOT h.ServiceOrder IS NULL
								AND NOT h.CustomerID IS NULL
								AND d.EntrySourceID IS NULL
								AND h.unit_no IN (SELECT RIGHT(Unit, 5) AS Unit FROM Employee.dbo.TBL_UnitCrossReference WITH (NOLOCK) WHERE UnitType IN ('In-Home District', 'HVAC District') AND ReportingRegion IN ('0000820', '0000830', '0000890', '0000900'))
								AND h.EntryID = @PMOwnerID

							UPDATE h
							SET OriginalLedgerEntryID = ISNULL(h.OriginalLedgerEntryID, l.EntryID) 
								,CurrentLedgerEntryID = l.EntryID
							FROM 
								ConfidentialCustomer.dbo.TBL_HSPOS h
							JOIN 
								ConfidentialCustomer.dbo.TBL_CustomerLedger l WITH (NOLOCK)
								ON h.EntryID = l.EntrySourceID
								AND h.EntryType = l.EntrySourceType
							WHERE
								h.EntryID = @PMOwnerID
						
							/*
							UPDATE c
							SET OffsetEntryID = p.EntryID
							--SELECT *
							FROM
								(	SELECT 
										EntryID
										,EntryType
										,CustomerID
										,EntryAmount
										,EntrySourceType
										,EntrySourceID
										,EntrySourceDate
										,ServiceUnit
										,ServiceOrder
										,OffsetEntryID
										,SUM(EntryAmount) OVER(PARTITION BY CustomerID, CAST(EntrySourceDate AS DATE)) AS CustomerTotal
									FROM 
										ConfidentialCustomer.dbo.TBL_CustomerLedger l WITH (NOLOCK)
									WHERE
										l.EntryType = 100
										AND l.OffsetEntryID IS NULL
								) c
							JOIN
								(	SELECT 
										EntryID
										,EntryType
										,CustomerID
										,EntryAmount
										,EntrySourceType
										,EntrySourceID
										,EntrySourceDate
										,ServiceUnit
										,ServiceOrder
										,OffsetEntryID
										,SUM(EntryAmount) OVER(PARTITION BY CustomerID, CAST(EntrySourceDate AS DATE)) AS CustomerTotal
									FROM 
										ConfidentialCustomer.dbo.TBL_CustomerLedger WITH (NOLOCK)
									WHERE
										EntryType = 200
								) p
								ON c.ServiceUnit = p.ServiceUnit
								AND c.ServiceOrder = p.ServiceOrder
								AND CAST(c.EntrySourceDate AS DATE) <= CAST(p.EntrySourceDate AS DATE)
								AND 
									(	ABS(c.EntryAmount) = ABS(p.EntryAmount)
										OR
										ABS(c.CustomerTotal) = ABS(p.EntryAmount)
									)
							WHERE
								c.OffsetEntryID IS NULL
							*/
						END

					IF @PMOwnerType = 'TempusCard'
						BEGIN
							UPDATE ConfidentialCustomer.dbo.TBL_TempusCard
							SET ServiceUnit = @ServiceUnit
								,ServiceOrder = @ServiceOrder
								,CustomerID = @CustomerID
								,CustomerIDUpdateSource = @OwnerType
								,CustomerIDUpdateSourceID = @OwnerID
								,LastModifiedEnterpriseID = @EnterpriseID
								,LastModifiedDateTime = GETDATE()
							WHERE 
								EntryID = @PMOwnerID

							INSERT INTO ConfidentialCustomer.dbo.TBL_CustomerLedger
								(EntryType
								,CustomerID
								,EntryAmount
								,EntryAmountParts
								,EntryAmountLabor
								,EntryAmountOther
								,EntryAmountTax
								,EntrySourceType
								,EntrySourceID
								,EntrySourceConfidence
								,EntrySourceDate
								,ServiceUnit
								,ServiceOrder
								,PurchaseOrder
								,EntrySourceCreatedBy)
							SELECT 
								200
								,s.CustomerID
								,s.tranamt AS EntryAmount
								,NULL AS EntryAmountParts
								,NULL AS EntryAmountLabor
								,NULL AS EntryAmountOther
								,NULL AS EntryAmountTax
								,'TempusCard' AS EntrySourceType
								,s.EntryID AS EntrySourceID
								,90 AS EntrySourceConfidence
								,s.authdate AS EntrySourceDate
								,s.ServiceUnit
								,s.ServiceOrder
								,NULL AS PurchaseOrder
								,COALESCE(UPPER(i.EnterpriseID), s.TechnicianID, 'SYSTEM') AS EntrySourceCreatedBy
							FROM 
								ConfidentialCustomer.dbo.TBL_TempusCard s WITH (NOLOCK)
							LEFT JOIN
								ConfidentialCustomer.dbo.TBL_CustomerLedger d WITH (NOLOCK)
								ON s.CurrentLedgerEntryID = d.EntryID
							LEFT JOIN
								Employee.dbo.TBL_NPSTechnicianPay e WITH (NOLOCK)
								ON s.ServiceUnit = e.Unit
								AND s.TechnicianID = e.NPSID
							LEFT JOIN
								Employee.dbo.TBL_Employee_New i
								ON e.EmployeeID = i.EmployeeID
							WHERE
								s.saletype IN ('SALE', 'DEBIT')
								AND s.authdate > '2017-02-05'
								AND s.authcode IS NOT NULL
								AND s.ServiceOrder IS NOT NULL
								AND s.CustomerID IS NOT NULL
								AND d.EntrySourceID IS NULL
								AND s.EntryID = @PMOwnerID

							UPDATE t
							SET OriginalLedgerEntryID = ISNULL(t.OriginalLedgerEntryID, l.EntryID) 
								,CurrentLedgerEntryID = l.EntryID
							FROM 
								ConfidentialCustomer.dbo.TBL_TempusCard t WITH (NOLOCK)
							JOIN
								ConfidentialCustomer.dbo.TBL_CustomerLedger l WITH (NOLOCK) 
								ON t.EntryID = l.EntrySourceID
								AND t.EntryType = l.EntrySourceType
							WHERE
								t.EntryID = @PMOwnerID

							/*
							UPDATE c
							SET OffsetEntryID = p.EntryID
							--SELECT *
							FROM
								(	SELECT 
										EntryID
										,EntryType
										,CustomerID
										,EntryAmount
										,EntrySourceType
										,EntrySourceID
										,EntrySourceDate
										,ServiceUnit
										,ServiceOrder
										,OffsetEntryID
										,SUM(EntryAmount) OVER(PARTITION BY CustomerID, CAST(EntrySourceDate AS DATE)) AS CustomerTotal
									FROM 
										ConfidentialCustomer.dbo.TBL_CustomerLedger l WITH (NOLOCK)
									WHERE
										l.EntryType = 100
										AND l.OffsetEntryID IS NULL
								) c
							JOIN
								(	SELECT 
										EntryID
										,EntryType
										,CustomerID
										,EntryAmount
										,EntrySourceType
										,EntrySourceID
										,EntrySourceDate
										,ServiceUnit
										,ServiceOrder
										,OffsetEntryID
										,SUM(EntryAmount) OVER(PARTITION BY CustomerID, CAST(EntrySourceDate AS DATE)) AS CustomerTotal
									FROM 
										ConfidentialCustomer.dbo.TBL_CustomerLedger WITH (NOLOCK)
									WHERE
										EntryType = 200
								) p
								ON c.ServiceUnit = p.ServiceUnit
								AND c.ServiceOrder = p.ServiceOrder
								AND CAST(c.EntrySourceDate AS DATE) <= CAST(p.EntrySourceDate AS DATE)
								AND 
									(	ABS(c.EntryAmount) = ABS(p.EntryAmount)
										OR
										ABS(c.CustomerTotal) = ABS(p.EntryAmount)
									)
							WHERE
								c.OffsetEntryID IS NULL
							*/
						END

					IF @PMOwnerType = 'FirstDataPayments'
						BEGIN
							UPDATE ConfidentialCustomer.dbo.TBL_FirstDataPayments
							SET ServiceUnit = @ServiceUnit
								,ServiceOrder = @ServiceOrder
								,CustomerID = @CustomerID
								,CustomerIDUpdateSource = @OwnerType
								,CustomerIDUpdateSourceID = @OwnerID
								,LastModifiedEnterpriseID = @EnterpriseID
								,LastModifiedDateTime = GETDATE()
							WHERE 
								EntryID = @PMOwnerID

							INSERT INTO ConfidentialCustomer.dbo.TBL_CustomerLedger
								(EntryType
								,CustomerID
								,EntryAmount
								,EntryAmountParts
								,EntryAmountLabor
								,EntryAmountOther
								,EntryAmountTax
								,EntrySourceType
								,EntrySourceID
								,EntrySourceConfidence
								,EntrySourceDate
								,ServiceUnit
								,ServiceOrder
								,PurchaseOrder
								,EntrySourceCreatedBy)
							SELECT 
								200
								,s.CustomerID
								,s.PaymentAmount AS EntryAmount
								,NULL AS EntryAmountParts
								,NULL AS EntryAmountLabor
								,NULL AS EntryAmountOther
								,NULL AS EntryAmountTax
								,'FirstDataPayments' AS EntrySourceType
								,s.EntryID AS EntrySourceID
								,90 AS EntrySourceConfidence
								,s.TransactionDate AS EntrySourceDate
								,s.ServiceUnit
								,s.ServiceOrder
								,NULL AS PurchaseOrder
								,COALESCE(UPPER(i.EnterpriseID), s.TechnicianID, 'SYSTEM') AS EntrySourceCreatedBy
							FROM 
								ConfidentialCustomer.dbo.TBL_FirstDataPayments s WITH (NOLOCK)
							LEFT JOIN
								ConfidentialCustomer.dbo.TBL_CustomerLedger d WITH (NOLOCK)
								ON s.CurrentLedgerEntryID = d.EntryID
							LEFT JOIN
								Employee.dbo.TBL_NPSTechnicianPay e WITH (NOLOCK)
								ON s.ServiceUnit = e.Unit
								AND s.TechnicianID = e.NPSID
							LEFT JOIN
								Employee.dbo.TBL_Employee_New i
								ON e.EmployeeID = i.EmployeeID
							WHERE
								s.TransactionDate > '2017-02-05'
								AND s.ServiceOrder IS NOT NULL
								AND s.CustomerID IS NOT NULL
								AND d.EntrySourceID IS NULL
								AND s.EntryID = @PMOwnerID

							UPDATE f
							SET OriginalLedgerEntryID = ISNULL(f.OriginalLedgerEntryID, l.EntryID) 
								,CurrentLedgerEntryID = l.EntryID
							FROM 
								ConfidentialCustomer.dbo.TBL_FirstDataPayments f WITH (NOLOCK)
							JOIN 
								ConfidentialCustomer.dbo.TBL_CustomerLedger l WITH (NOLOCK)
								ON f.EntryID = l.EntrySourceID
								AND f.EntryType = l.EntrySourceType
							WHERE 
								f.EntryID = @PMOwnerID

							/*
							UPDATE c
							SET OffsetEntryID = p.EntryID
							--SELECT *
							FROM
								(	SELECT 
										EntryID
										,EntryType
										,CustomerID
										,EntryAmount
										,EntrySourceType
										,EntrySourceID
										,EntrySourceDate
										,ServiceUnit
										,ServiceOrder
										,OffsetEntryID
										,SUM(EntryAmount) OVER(PARTITION BY CustomerID, CAST(EntrySourceDate AS DATE)) AS CustomerTotal
									FROM 
										ConfidentialCustomer.dbo.TBL_CustomerLedger l WITH (NOLOCK)
									WHERE
										l.EntryType = 100
										AND l.OffsetEntryID IS NULL
								) c
							JOIN
								(	SELECT 
										EntryID
										,EntryType
										,CustomerID
										,EntryAmount
										,EntrySourceType
										,EntrySourceID
										,EntrySourceDate
										,ServiceUnit
										,ServiceOrder
										,OffsetEntryID
										,SUM(EntryAmount) OVER(PARTITION BY CustomerID, CAST(EntrySourceDate AS DATE)) AS CustomerTotal
									FROM 
										ConfidentialCustomer.dbo.TBL_CustomerLedger WITH (NOLOCK)
									WHERE
										EntryType = 200
								) p
								ON c.ServiceUnit = p.ServiceUnit
								AND c.ServiceOrder = p.ServiceOrder
								AND CAST(c.EntrySourceDate AS DATE) <= CAST(p.EntrySourceDate AS DATE)
								AND 
									(	ABS(c.EntryAmount) = ABS(p.EntryAmount)
										OR
										ABS(c.CustomerTotal) = ABS(p.EntryAmount)
									)
							WHERE
								c.OffsetEntryID IS NULL
							*/
						END

					IF @PMOwnerType = 'Telecheck'
						BEGIN
							UPDATE ConfidentialCustomer.dbo.TBL_Telecheck
							SET ServiceUnit = @ServiceUnit
								,ServiceOrder = @ServiceOrder
								,CustomerID = @CustomerID
								,CustomerIDUpdateSource = @OwnerType
								,CustomerIDUpdateSourceID = @OwnerID
								,LastModifiedEnterpriseID = @EnterpriseID
								,LastModifiedDateTime = GETDATE()
							WHERE 
								EntryID = @PMOwnerID

							INSERT INTO ConfidentialCustomer.dbo.TBL_CustomerLedger
								(EntryType
								,CustomerID
								,EntryAmount
								,EntryAmountParts
								,EntryAmountLabor
								,EntryAmountOther
								,EntryAmountTax
								,EntrySourceType
								,EntrySourceID
								,EntrySourceConfidence
								,EntrySourceDate
								,ServiceUnit
								,ServiceOrder
								,PurchaseOrder
								,EntrySourceCreatedBy)
							SELECT 
								CASE 
									WHEN s.TransactionCode = 'A' THEN 100
									WHEN s.TransactionCode = 'D' THEN 100
									WHEN s.TransactionCode = 'R' THEN 300
									WHEN s.TransactionCode = 'S' THEN 100
									WHEN s.TransactionCode = 'T' THEN 200
									WHEN s.TransactionCode = 'V' THEN 100
									ELSE 0 END
								,s.CustomerID
								,s.EntryAmount AS EntryAmount
								,NULL AS EntryAmountParts
								,NULL AS EntryAmountLabor
								,NULL AS EntryAmountOther
								,NULL AS EntryAmountTax
								,'Telecheck' AS EntrySourceType
								,s.EntryID AS EntrySourceID
								,90 AS EntrySourceConfidence
								,s.CheckDate AS EntrySourceDate
								,s.ServiceUnit
								,s.ServiceOrder
								,NULL AS PurchaseOrder
								,COALESCE(UPPER(i.EnterpriseID), s.TechnicianID, 'SYSTEM') AS EntrySourceCreatedBy
							FROM 
								ConfidentialCustomer.dbo.TBL_Telecheck s WITH (NOLOCK)
							LEFT JOIN
								ConfidentialCustomer.dbo.TBL_CustomerLedger d WITH (NOLOCK)
								ON s.CurrentLedgerEntryID = d.EntryID
							LEFT JOIN
								Employee.dbo.TBL_NPSTechnicianPay e WITH (NOLOCK)
								ON s.ServiceUnit = e.Unit
								AND s.TechnicianID = e.NPSID
							LEFT JOIN
								Employee.dbo.TBL_Employee_New i
								ON e.EmployeeID = i.EmployeeID
							WHERE
								s.CheckDate > '2017-02-05'
								AND s.TransactionCode IN ('T')
								AND s.CustomerID IS NOT NULL
								AND s.ServiceOrder IS NOT NULL
								AND d.EntrySourceID IS NULL
								AND s.EntryID = @PMOwnerID

							UPDATE t
							SET OriginalLedgerEntryID = ISNULL(t.OriginalLedgerEntryID, l.EntryID)
								,CurrentLedgerEntryID = l.EntryID
							FROM 
								ConfidentialCustomer.dbo.TBL_Telecheck t WITH (NOLOCK)
							JOIN 
								ConfidentialCustomer.dbo.TBL_CustomerLedger l WITH (NOLOCK)
								ON t.EntryID = l.EntrySourceID
								AND t.EntryType = l.EntrySourceType
							WHERE
								t.EntryID = @PMOwnerID

							/*
							UPDATE c
							SET OffsetEntryID = p.EntryID
							--SELECT *
							FROM
								(	SELECT 
										EntryID
										,EntryType
										,CustomerID
										,EntryAmount
										,EntrySourceType
										,EntrySourceID
										,EntrySourceDate
										,ServiceUnit
										,ServiceOrder
										,OffsetEntryID
										,SUM(EntryAmount) OVER(PARTITION BY CustomerID, CAST(EntrySourceDate AS DATE)) AS CustomerTotal
									FROM 
										ConfidentialCustomer.dbo.TBL_CustomerLedger l WITH (NOLOCK)
									WHERE
										l.EntryType = 100
										AND l.OffsetEntryID IS NULL
								) c
							JOIN
								(	SELECT 
										EntryID
										,EntryType
										,CustomerID
										,EntryAmount
										,EntrySourceType
										,EntrySourceID
										,EntrySourceDate
										,ServiceUnit
										,ServiceOrder
										,OffsetEntryID
										,SUM(EntryAmount) OVER(PARTITION BY CustomerID, CAST(EntrySourceDate AS DATE)) AS CustomerTotal
									FROM 
										ConfidentialCustomer.dbo.TBL_CustomerLedger WITH (NOLOCK)
									WHERE
										EntryType = 200
								) p
								ON c.ServiceUnit = p.ServiceUnit
								AND c.ServiceOrder = p.ServiceOrder
								AND CAST(c.EntrySourceDate AS DATE) <= CAST(p.EntrySourceDate AS DATE)
								AND 
									(	ABS(c.EntryAmount) = ABS(p.EntryAmount)
										OR
										ABS(c.CustomerTotal) = ABS(p.EntryAmount)
									)
							WHERE
								c.OffsetEntryID IS NULL
							*/
						END
						
						IF @PMOwnerType = 'PNCCheck'
						BEGIN
							UPDATE ConfidentialCustomer.dbo.TBL_PNCCheck
							SET ServiceUnit = @ServiceUnit
								,ServiceOrder = @ServiceOrder
								,CustomerID = @CustomerID
								,CustomerIDUpdateSource = @OwnerType
								,CustomerIDUpdateSourceID = @OwnerID
								,LastModifiedEnterpriseID = @EnterpriseID
								,LastModifiedDateTime = GETDATE()
							WHERE 
								EntryID = @PMOwnerID

							INSERT INTO ConfidentialCustomer.dbo.TBL_CustomerLedger
								(EntryType
								,CustomerID
								,EntryAmount
								,EntryAmountParts
								,EntryAmountLabor
								,EntryAmountOther
								,EntryAmountTax
								,EntrySourceType
								,EntrySourceID
								,EntrySourceConfidence
								,EntrySourceDate
								,ServiceUnit
								,ServiceOrder
								,PurchaseOrder
								,EntrySourceCreatedBy)
							SELECT 
								200
								,s.CustomerID
								,s.EntryAmount AS EntryAmount
								,NULL AS EntryAmountParts
								,NULL AS EntryAmountLabor
								,NULL AS EntryAmountOther
								,NULL AS EntryAmountTax
								,s.EntryType AS EntrySourceType
								,s.EntryID AS EntrySourceID
								,90 AS EntrySourceConfidence
								,s.TransmitDateTime AS EntrySourceDate
								,s.ServiceUnit
								,s.ServiceOrder
								,NULL AS PurchaseOrder
								,COALESCE(UPPER(REPLACE(s.UserLoginID,'119600','')), 'SYSTEM') AS EntrySourceCreatedBy
							FROM 
								ConfidentialCustomer.dbo.TBL_PNCCheck s WITH (NOLOCK)
							LEFT JOIN
								ConfidentialCustomer.dbo.TBL_CustomerLedger d WITH (NOLOCK)
								ON s.CurrentLedgerEntryID = d.EntryID
							WHERE
								s.ServiceOrder IS NOT NULL
								AND s.CustomerID IS NOT NULL
								AND d.EntrySourceID IS NULL
								AND s.EntryID = @PMOwnerID

							UPDATE t
							SET OriginalLedgerEntryID = ISNULL(t.OriginalLedgerEntryID, l.EntryID) 
								,CurrentLedgerEntryID = l.EntryID
							FROM 
								ConfidentialCustomer.dbo.TBL_PNCCheck t WITH (NOLOCK)
							JOIN
								ConfidentialCustomer.dbo.TBL_CustomerLedger l WITH (NOLOCK) 
								ON t.EntryID = l.EntrySourceID
								AND t.EntryType = l.EntrySourceType
							WHERE
								t.EntryID = @PMOwnerID

						END						
					
				END

			END

			/*
			ELSE /* update row */
				BEGIN
		
					UPDATE TBL_Actions SET
						StatusID = @StatusID,
						ModifiedEnterpriseID = @EnterpriseID,
						ModifiedDateTime = GETDATE()
					WHERE 
						ActionID = @ActionID
				END
			*/
	


	

























GO
/****** Object:  StoredProcedure [dbo].[PROC_Actions_Insert_RefuseToPay]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





/*************************************************************************
	Procedure Name: PROC_Actions_Insert_RefuseToPay
	Returns:		Failure Code (@ErrorCode)
	Primary User:	Everyone
	Designer:		Rob O'Brien
	Company:		Sears, Roebuck and Co
	Last Updated:	Fri Mar 02 12:26:28 CST 2018 @810 /Internet Time/
	Updated by:		Rob O'Brien
	Information:	Procedure for adding or updating a comment
	************************************************************************
	Version:		1.00
	Date:			Fri Mar 02 12:26:36 CST 2018 @810 /Internet Time/
	Updated By:		Rob O'Brien
***************************************************************************/
CREATE Procedure [dbo].[PROC_Actions_Insert_RefuseToPay]
	@ActionID int = Null OUTPUT, /* Unique ID number for the comment.  To update an existing comment, supply the comment ID.  Otherwise, an insert will be done. */
	@OwnerType varchar(30), /* Ownertype from the ownertype process managed for a database side applicaiton identification. */
	@OwnerID int, /* OwnerID number from the calling applicaiton. */
	@SecondaryType varchar(30) = NULL, /* Ownertype from the ownertype process managed for a database side applicaiton identification. */
	@SecondaryID int = NULL, /* OwnerID number from the calling application. */
	@StatusID int, /* StatusID number from the calling application. */
	@EnterpriseID varchar(8), /* Employee LDAP ID making the insert or update.  Upper case. */
	@AdjustedPartsAmount decimal(18,4) = NULL,
	@AdjustedLaborAmount decimal(18,4) = NULL,
	@AdjustedDeductableAmount decimal(18,4) = NULL,
	@AdjustedAmount decimal(18,4) = NULL,
	@ReasonCode int = NULL,
	@ErrorCode varchar(150) = NULL OUTPUT /* error message if any during processing */ 
AS
	SET NOCOUNT ON
	DECLARE @PartsTaxRate decimal(18,4)
	DECLARE @LaborTaxRate decimal(18,4)
	DECLARE @AdjustedAmountWithTax decimal(18,4)

	SET @EnterpriseID = LTRIM(RTRIM(UPPER(@EnterpriseID)))
	SET @OwnerType = LTRIM(RTRIM(@OwnerType))


	IF @OwnerType = '' /* set @OwnerType to null if empty string */
		SET @OwnerType = NULL
	IF @EnterpriseID = '' /* set @EnterpriseID to null if empty string */
		SET @EnterpriseID = NULL
	IF @SecondaryID = 0 /* set @CommentID to null if zero */
		SET @SecondaryID = NULL
	IF @OwnerID = 0 /* set @OwnerID to null if zero */
		SET @OwnerID = NULL
	IF @SecondaryType = '' /* set @SecondaryType to null if empty string */
		SET @SecondaryType = NULL


	IF @StatusID Is Null /* Return error code if @Comment is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for Status, it is Required.'
		Return
	END
	IF @OwnerID Is Null /* Return error code if @OwnerID is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for OwnerID, it is Required.'
		Return
	END
	IF @EnterpriseID Is Null /* Return error code if @EnterpriseID is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for EmpID, it is Required.'
		Return
	END
	
	IF @OwnerType Is Null /* Return error code if @OwnerType is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for OwnerType, it is Required.'
		Return
	END	
	
	/* if a comment ID was passed in, update it. otherwise create a new row */
	IF @ActionID IS NULL /* add row */
		BEGIN
			INSERT INTO ConfidentialCustomer.dbo.TBL_Actions
			(ActionType, OwnerType, OwnerID, SecondaryType, SecondaryID, StatusID, CreatedEnterpriseID, CreatedDateTime)
			VALUES('Actions', @OwnerType, @OwnerID, @SecondaryType, @SecondaryID, @StatusID, @EnterpriseID, GETDATE())
		
			SET @ActionID = scope_identity()
		
			UPDATE ConfidentialCustomer.dbo.TBL_RefuseToPay
			SET ActionID = @ActionID
				,StatusID = @StatusID
				,ReasonCode = COALESCE(@ReasonCode, ReasonCode)
				,LastModifiedEnterpriseID = @EnterpriseID
				,LastModifiedDateTime = GETDATE()
			WHERE
				EntryID = @OwnerID
		END		
		IF @StatusID = 5
			BEGIN
				SET @PartsTaxRate = (SELECT PartsTaxRate FROM ConfidentialCustomer.dbo.TBL_RefuseToPay WITH (NOLOCK) WHERE EntryID = @OwnerID)
				SET @LaborTaxRate = (SELECT LaborTaxRate FROM ConfidentialCustomer.dbo.TBL_RefuseToPay WITH (NOLOCK) WHERE EntryID = @OwnerID)
				SET @AdjustedAmountWithTax = ROUND((@AdjustedPartsAmount + (@AdjustedPartsAmount * @PartsTaxRate) + @AdjustedLaborAmount + (@AdjustedLaborAmount * @LaborTaxRate) + @AdjustedDeductableAmount),2)

				UPDATE ConfidentialCustomer.dbo.TBL_RefuseToPay
				SET AdjustedPartsAmount = @AdjustedPartsAmount
					,AdjustedLaborAmount = @AdjustedLaborAmount
					,AdjustedDeductableAmount = @AdjustedDeductableAmount
					,AdjustedAmountWithTax = @AdjustedAmountWithTax
					,ReasonCode = COALESCE(@ReasonCode, ReasonCode)
					,LastModifiedEnterpriseID = @EnterpriseID
					,LastModifiedDateTime = GETDATE()
				WHERE
					EntryID = @OwnerID
			END

			/*
			ELSE /* update row */
				BEGIN
		
					UPDATE TBL_Actions SET
						StatusID = @StatusID,
						ModifiedEnterpriseID = @EnterpriseID,
						ModifiedDateTime = GETDATE()
					WHERE 
						ActionID = @ActionID
				END
			*/
	


	















GO
/****** Object:  StoredProcedure [dbo].[PROC_Actions_Insert_Requests]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO










/*************************************************************************
	Procedure Name: PROC_Actions_Insert_Requests
	Returns:		Failure Code (@ErrorCode)
	Primary User:	Everyone
	Designer:		Rob O'Brien
	Company:		Sears, Roebuck and Co
	Last Updated:	Fri Mar 02 12:26:28 CST 2018 @810 /Internet Time/
	Updated by:		Rob O'Brien
	Information:	Procedure for adding or updating a comment
	************************************************************************
	Version:		1.00
	Date:			Fri Mar 02 12:26:36 CST 2018 @810 /Internet Time/
	Updated By:		Rob O'Brien
***************************************************************************/
CREATE Procedure [dbo].[PROC_Actions_Insert_Requests]
	@ActionID int = Null OUTPUT, /* Unique ID number for the comment.  To update an existing comment, supply the comment ID.  Otherwise, an insert will be done. */
	@OwnerType varchar(30), /* Ownertype from the ownertype process managed for a database side applicaiton identification. */
	@OwnerID int, /* OwnerID number from the calling applicaiton. */
	@SecondaryType varchar(30) = NULL, /* Ownertype from the ownertype process managed for a database side applicaiton identification. */
	@SecondaryID int = NULL, /* OwnerID number from the calling application. */
	@StatusID int, /* StatusID number from the calling application. */
	@EnterpriseID varchar(8), /* Employee LDAP ID making the insert or update.  Upper case. */
	@PaymentMethod varchar(50), /* Transaction Number. */
	@TransactionNumber varchar(50), /* Transaction Number. */
	@ApprovedPartsAmount decimal(18,4) = NULL,
	@ApprovedLaborAmount decimal(18,4) = NULL,
	@ApprovedAmount decimal(18,4) = NULL,
	@ErrorCode varchar(150) = NULL OUTPUT /* error message if any during processing */ 
AS
	SET NOCOUNT ON

--	DECLARE @PartsTaxRate decimal(18,4)
--	DECLARE @LaborTaxRate decimal(18,4)
--	DECLARE @AdjustedAmountWithTax decimal(18,4)

	SET @EnterpriseID = LTRIM(RTRIM(UPPER(@EnterpriseID)))
	SET @OwnerType = LTRIM(RTRIM(@OwnerType))


	IF @OwnerType = '' /* set @OwnerType to null if empty string */
		SET @OwnerType = NULL
	IF @EnterpriseID = '' /* set @EnterpriseID to null if empty string */
		SET @EnterpriseID = NULL
	IF @SecondaryID = 0 /* set @CommentID to null if zero */
		SET @SecondaryID = NULL
	IF @OwnerID = 0 /* set @OwnerID to null if zero */
		SET @OwnerID = NULL
	IF @SecondaryType = '' /* set @SecondaryType to null if empty string */
		SET @SecondaryType = NULL



	IF @StatusID Is Null /* Return error code if @Comment is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for Status, it is Required.'
		Return
	END
	IF @EnterpriseID Is Null /* Return error code if @EnterpriseID is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for EmpID, it is Required.'
		Return
	END
	
	
	/* if a comment ID was passed in, update it. otherwise create a new row */
	IF @ActionID IS NULL /* add row */
		BEGIN
			INSERT INTO ConfidentialCustomer.dbo.TBL_Actions
			(ActionType, OwnerType, OwnerID, SecondaryType, SecondaryID, StatusID, CreatedEnterpriseID, CreatedDateTime)
			VALUES('Actions', @OwnerType, @OwnerID, @SecondaryType, @SecondaryID, @StatusID, @EnterpriseID, GETDATE())
		
			SET @ActionID = scope_identity()
		
			UPDATE ConfidentialCustomer.dbo.TBL_Requests
			SET ActionID = @ActionID
				,StatusID = @StatusID
				,LastModifiedEnterpriseID = @EnterpriseID
				,LastModifiedDateTime = GETDATE()
			WHERE
				EntryID = @OwnerID
		END		
		IF @StatusID = 5
			BEGIN
				
				UPDATE ConfidentialCustomer.dbo.TBL_Requests
				SET ApprovedPartsAmount = @ApprovedPartsAmount
					,ApprovedLaborAmount = @ApprovedLaborAmount
					,ApprovedAmount = @ApprovedAmount
					,PaymentMethod = @PaymentMethod
					,TransactionNumber = @TransactionNumber
					,LastModifiedEnterpriseID = @EnterpriseID
					,LastModifiedDateTime = GETDATE()
				WHERE
					EntryID = @OwnerID
			END

			/*
			ELSE /* update row */
				BEGIN
		
					UPDATE TBL_Actions SET
						StatusID = @StatusID,
						ModifiedEnterpriseID = @EnterpriseID,
						ModifiedDateTime = GETDATE()
					WHERE 
						ActionID = @ActionID
				END
			*/
	


	














GO
/****** Object:  StoredProcedure [dbo].[PROC_Actions_Insert_RMS]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*************************************************************************
	Procedure Name: PROC_Actions_Insert_RMS
	Returns:		Failure Code (@ErrorCode)
	Primary User:	Everyone
	Designer:		Brandi Fischer
	Company:		Sears, Roebuck and Co
	Last Updated:	Mon Oct 01 13:37:21 EST 2018 @810 /Internet Time/
	Updated by:		
	Information:	
	************************************************************************
	Version:		1.00
	Date:			Mon Oct 01 13:37:21 EST 2018 @810 /Internet Time/
	Updated By:		Brandi Fischer
***************************************************************************/
CREATE Procedure [dbo].[PROC_Actions_Insert_RMS]
	--@ErrorCode varchar(150) = NULL OUTPUT
AS
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

--DECLARE @billcount AS INTEGER = (SELECT NumericValue FROM ConfidentialCustomer.dbo.TBL_LookupCodes WHERE OwnerType = 'CustomerBillPrint' AND OwnerID ='ProcedureVariable' AND Description = 'BillCount')

BEGIN
	UPDATE CB
	SET 
		ActionID = NULL
		,ActionType = NULL
		,CB.StatusID = CB.StatusID + 1
		--,CB.InvoiceNumber = (CASE WHEN CB.InvoiceNumber IN (0, NULL) THEN CB.CustomerID ELSE CB.InvoiceNumber END)
		--,CB.InstallationCount = CB.InstallationCount + 1
		--,CB.LastBillDate = CAST(GETDATE() AS DATE)
		,CB.LastModifiedEnterpriseID = 'SYSTEM'
		,CB.LastModifiedDateTime = GETDATE()
	FROM (
		SELECT CB.*
		FROM 
			ConfidentialCustomer.dbo.TBL_CustomerBills CB
		INNER JOIN (
			SELECT 
				BillID
				,MIN(EntrySourceDate) AS RouteDate
				,COUNT(ServiceOrder) AS SOCount
				,SUM(EntryAmount) AS EntryAmount
			FROM 
				ConfidentialCustomer.dbo.TBL_CustomerLedger
			WHERE 
				BillID IS NOT NULL
			GROUP BY 
				BillID
			) CL
		ON 
			CB.EntryID = CL.BillID
		LEFT JOIN (
			SELECT [CustomerID]
				,MAX([EntrySourceDate]) AS PayMaxDate
				,SUM([EntryAmount]) AS AcctBal
			FROM 
				[ConfidentialCustomer].[dbo].[TBL_CustomerLedger] WITH (NOLOCK)
			GROUP BY 
				[CustomerID]
			) CLC
		ON 
			CB.[CustomerID] = CLC.[CustomerID]
		INNER JOIN 
			ServiceOrder.dbo.TBL_NPJCustomerNameAddress CN WITH (NOLOCK)
		ON 
			CB.CustomerID = CN.Customer
		WHERE 
			CB.StatusID = 8
			AND (CB.ReasonID IS NULL OR CB.ReasonID <> -1)
			AND ABS(CB.CurrentBalance) > 20
			AND ABS(CB.CurrentBalance) = ABS(CL.EntryAmount)
			AND ABS(CB.CurrentBalance) = ABS(CLC.AcctBal)
			AND CL.SOCount = 1
			AND CLC.AcctBal < 0
		--ORDER BY
		--	ISNULL(CB.LastBillDate,'1900-01-01') ASC
		--	,CL.RouteDate ASC
		--	,ABS(CB.CurrentBalance) DESC
	) CB;
END

BEGIN
	INSERT INTO ConfidentialCustomer.dbo.TBL_Actions
	(ActionType
	,OwnerID
	,OwnerType
	,StatusID
	,CreatedEnterpriseID
	,CreatedDateTime)
	SELECT
	'Actions'
	,CB.EntryID
	,CB.EntryType
	,CB.StatusID
	,CB.LastModifiedEnterpriseID
	,CB.LastModifiedDateTime
	FROM ConfidentialCustomer.dbo.TBL_CustomerBills CB
	WHERE 
	CB.StatusID = 9
	AND CB.ActionID IS NULL
	AND CAST(CB.LastModifiedDateTime AS DATE) = CAST(GETDATE() AS DATE);

	UPDATE CB
	SET CB.ActionID = A.ActionID
	,CB.ActionType = A.ActionType
	FROM [ConfidentialCustomer].[dbo].[TBL_CustomerBills] CB
	INNER JOIN [ConfidentialCustomer].[dbo].[TBL_Actions] A
	ON CB.EntryID = A.OwnerID
	AND CB.EntryType = A.OwnerType
	AND CB.StatusID = A.StatusID
	AND CB.LastModifiedEnterpriseID = A.CreatedEnterpriseID
	AND CB.LastModifiedDateTime = A.CreatedDateTime
	WHERE CAST(CB.LastModifiedDateTime AS DATE) =  CAST(GETDATE() AS DATE);

END

       
--/* Return recordset to client */
--BEGIN
	--DECLARE @RMSType AS VARCHAR(15) = 'COLLECT';
	--SELECT 
	--	CB.InvoiceNumber AS CustomerNumber
	--	,COALESCE(CO.CustomerName
	--		,CASE WHEN LA.CustomerNamePrefix <> ''
	--		THEN LTRIM(RTRIM(LA.CustomerNamePrefix)) ELSE '' END
	--		+ CASE WHEN LA.CustomerFirstName <> ''
	--		THEN ' ' + LTRIM(RTRIM(LA.CustomerFirstName)) ELSE '' END
	--		+ CASE WHEN LA.CustomerMiddleName <> ''
	--		THEN ' ' + LTRIM(RTRIM(LA.CustomerMiddleName)) ELSE '' END
	--		+ CASE WHEN LA.CustomerLastName <> ''
	--		THEN ' ' + LTRIM(RTRIM(LA.CustomerLastName)) ELSE '' END
	--		+ CASE WHEN LA.CustomerLastNameAdditional <> ''
	--		THEN ' ' + LTRIM(RTRIM(LA.CustomerLastNameAdditional)) ELSE '' END 
	--		+ CASE WHEN LA.CustomerNameSuffix <> ''
	--		THEN ' ' + LTRIM(RTRIM(LA.CustomerNameSuffix)) ELSE '' END) 
	--	AS CustomerName
	--	,NULL AS Title
	--	,NULL AS MrMrsEtc
	--	,LTRIM(RTRIM(LA.CustomerFirstName)) AS CustomerFirstName
	--	,LTRIM(RTRIM(LA.CustomerLastName)) AS CustomerLastName
	--	,COALESCE(CO.CustomerStreetAddress, LTRIM(RTRIM(REPLACE(REPLACE(LA.CustomerStreetAddress,'# NONE -',''),'# NONE','')))) AS CustomerStreetAddress1 
	--	,NULL AS CustomerStreetAddress2
	--	,LTRIM(RTRIM(COALESCE(CO.CustomerCity, LA.CustomerCity))) AS CustomerCity
	--	,LTRIM(RTRIM(COALESCE(CO.CustomerState, LA.CustomerState))) AS CustomerState
	--	,LTRIM(RTRIM(COALESCE(CO.CustomerZip, LA.CustomerZip))) AS CustomerZip
	--	,NULL AS CustomerCountry
	--	,CB.CurrentBalance AS AmountDue
	--	,CAST(AP.CreatedDateTime AS DATE) AS InvoiceDate
	--	,LTRIM(RTRIM(COALESCE(CO.CustomerPhone, LA.CustomerPhone))) AS CustomerPhone1
	--	,LTRIM(RTRIM(COALESCE(CO.CustomerAltPhone, LA.CustomerAlternatePhone))) AS CustomerPhone2
	--	,NULL AS CustomerPhone3
	--	,CL.ServiceUnit + CL.ServiceOrder AS Reference
	--	,@RMSType AS Comment
	--	,COALESCE(LT.Description, LTRIM(RTRIM(SO.ServicingOrganization))) AS BusinessIndicator
	--	, LTRIM(RTRIM(SO.ManufacturerBrandName)) + ' - ' + UPPER(COALESCE(MC.MD_SM_CODE_DESC2, SO.MerchandiseCode)) AS Detail
	--	,ABS(CL.EntryAmount) AS DetailAmount
	--	,CAST(CL.EntrySourceDate AS DATE) AS ServiceDate
	--FROM 
	--	[ConfidentialCustomer].[dbo].[TBL_CustomerBills] CB WITH (NOLOCK) 
	--LEFT JOIN ConfidentialCustomer.dbo.TBL_LookupCodes LT WITH (NOLOCK) 
	--ON LT.IsEnabled = 1 AND LT.OwnerID = 'ServiceType' 
	--AND CB.EntryType = LT.OwnerType AND CB.ServiceType = LT.NumericValue
	--INNER JOIN (
	--	SELECT 
	--		BillID
	--		,ServiceUnit
	--		,ServiceOrder
	--		,EntrySourceDate 
	--		,EntryAmount
	--	FROM 
	--		ConfidentialCustomer.dbo.TBL_CustomerLedger WITH (NOLOCK) 
	--	WHERE 
	--		BillID IS NOT NULL
	--	) CL 
	--ON 
	--	CB.EntryID = CL.BillID 
	--INNER JOIN 
	--	ServiceOrder.dbo.TBL_NPSServiceOrderCustomerLookaside LA WITH (NOLOCK) 
	--ON 
	--	LA.AddressLocation = 1 
	--	AND CL.ServiceUnit = LA.ServiceUnit 
	--	AND CL.ServiceOrder = LA.ServiceOrder 
	--LEFT JOIN ConfidentialCustomer.dbo.TBL_BillType BT WITH (NOLOCK)
	--ON CB.BillType = BT.EntryID
	--LEFT JOIN ConfidentialCustomer.dbo.TBL_CustomerContactOverride CO WITH (NOLOCK) 
	--ON CB.EntryType = CO.OwnerType AND CB.EntryID = CO.OwnerID
	--INNER JOIN ServiceOrder.dbo.TBL_ServiceOrder SO WITH (NOLOCK)
	--ON CL.ServiceUnit = SO.ServiceUnit AND CL.ServiceOrder = SO.ServiceOrder
	--LEFT JOIN ServiceOrder.dbo.TBL_MerchandiseCode MC WITH (NOLOCK)
	--ON SO.MerchandiseCode = MC.MD_SM_CODE
	--LEFT JOIN ConfidentialCustomer.dbo.TBL_Actions AP WITH (NOLOCK)
	--ON 
	--AP.StatusID = 3
	--AND	CB.EntryType = AP.OwnerType 
	--AND CB.EntryID = AP.OwnerID
	----AND AP.ActionID IN (SELECT MAX(ActionID) 
	----	FROM ConfidentialCustomer.dbo.TBL_Actions 
	----	WHERE OwnerType = 'CustomerBills' 
	----	AND StatusID = 3 
	----	GROUP BY OwnerType, OwnerID)
	--WHERE 
	--CB.StatusID = 9
	--AND CAST(CB.LastModifiedDateTime AS DATE) = CAST(GETDATE() AS DATE)
--END	

GO
/****** Object:  StoredProcedure [dbo].[PROC_Actions_PaymentReMatch]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




/*************************************************************************
	Procedure Name: PROC_Actions_PaymentReMatch
	Returns:		Failure Code (@ErrorCode)
	Primary User:	Everyone
	Designer:		Rob O'Brien
	Company:		Sears, Roebuck and Co
	Last Updated:	Fri Mar 02 12:26:28 CST 2018 @810 /Internet Time/
	Updated by:		Rob O'Brien
	Information:	Procedure for adding or updating a comment
	************************************************************************
	Version:		1.00
	Date:			Fri Mar 02 12:26:36 CST 2018 @810 /Internet Time/
	Updated By:		Rob O'Brien
	************************************************************************
	Updated By: Brandi Fischer
	Update Date: 2018-07-10
	Update Notes: 
		1. Added the creation of a Rematch entry into the Payment match queue and changed the payment table updates to use this entry as the customer id update source.
		2. Change was for the rematch creation default status id change from 1 to 101.
		
	Updated By: Brandi Fischer
	Update Date: 2018-8-06
	Update Notes: 
		1. Cleaned up each previous current ledger entry to use source table current ledger entryid value.
		2. Capture correct ledger entry id from new ledger entry as source new current ledger entry.
***************************************************************************/
CREATE Procedure [dbo].[PROC_Actions_PaymentReMatch]
	@ActionID int = NULL OUTPUT, /* Unique ID number for the comment.  To update an existing comment, supply the comment ID.  Otherwise, an insert will be done. */
	@OwnerType varchar(30), /* Ownertype from the ownertype process managed for a database side applicaiton identification. */
	@OwnerID int, /* OwnerID number from the calling application. */
	@SecondaryType varchar(30) = NULL, /* Ownertype from the ownertype process managed for a database side applicaiton identification. */
	@SecondaryID int = NULL, /* OwnerID number from the calling application. */
	
	@EnterpriseID varchar(8), /* Employee LDAP ID making the insert or update.  Upper case. */
	@NewServiceUnit char(7),
	@NewServiceOrder char(8),
	@NPSID varchar(7) = NULL,
	
	@EntryType varchar(30 )= NULL OUTPUT, /* Ownertype from the ownertype process managed for a database side applicaiton identification. */
	@EntryID int = NULL OUTPUT, /* OwnerID number from the calling applicaiton. */
	@ErrorCode varchar(150) = NULL OUTPUT /* error message if any during processing */ 
AS
	SET NOCOUNT ON
	DECLARE @StatusID int
	DECLARE @LedgerEntryID int 
	DECLARE @OldServiceUnit char(7)
	DECLARE @OldServiceOrder char(8)
	DECLARE @CustomerID INT 
	SET @EnterpriseID = LTRIM(RTRIM(UPPER(@EnterpriseID)))
	SET @EntryType = LTRIM(RTRIM(@EntryType))


	IF @OwnerType = '' /* set @EntryType to null if empty string */
		SET @OwnerType = NULL
	IF @EnterpriseID = '' /* set @EnterpriseID to null if empty string */
		SET @EnterpriseID = NULL
	IF @OwnerID = 0 /* set @EntryID to null if zero */
		SET @OwnerID = NULL
	
	--IF @StatusID Is Null /* Return error code if @Comment is null */
	-- 	Begin
	--	SET @ErrorCode = 'Please enter a value for Status, it is Required.'
	--	Return
	--END
	IF @OwnerType Is Null /* Return error code if @EntryType is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for EntryType, it is Required.'
		Return
	END	
	IF @OwnerID Is Null /* Return error code if @EntryID is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for EntryID, it is Required.'
		Return
	END
	IF @EnterpriseID Is Null /* Return error code if @EnterpriseID is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for EmpID, it is Required.'
		Return
	END
	
	--IF @OwnerType = 'ServiceOrderPayments' OR @OwnerType = 'NPS' /* Return error code if @EntryType is null */
	-- 	Begin
	--	SET @ErrorCode = 'Currently can not rematch NPS Payments.'
	--	Return
	--END	
		
	
	/*create rematch item in queue*/
	IF @EntryID IS NULL /* add row */	
		BEGIN
			SET @StatusID = CASE WHEN @OwnerType = 'ServiceOrderPayments' OR @OwnerType = 'NPS' THEN 110 ELSE 100 END
			SET @EntryType = CASE WHEN @OwnerType = 'ServiceOrderPayments' OR @OwnerType = 'NPS' THEN 'NPSInsert' ELSE 'Rematch' END
			INSERT INTO [ConfidentialCustomer].[dbo].[TBL_PaymentMatch]
			   ([EntryType]
			   ,[OwnerType]
			   ,[OwnerID]
			   ,[ActionID]
			   ,[StatusID]
			   ,[PaymentAmount]
			   ,[PaymentType]
			   ,[PaymentDate]
			   ,[CreatedEnterpriseID]
			   ,[CreatedDateTime]
			   ,[LastModifiedEnterpriseID]
			   ,[LastModifiedDateTime]
			   ,[ActionType]
			   ,[ReasonID]
			   ,[SubStatusID])
			 SELECT
				   @EntryType		
				   ,@OwnerType
				   ,@OwnerID
				   ,0
				   ,NULL
				   ,S.PaymentAmount
				   ,S.PaymentType
				   ,S.PaymentDate
				   ,@EnterpriseID
				   ,GETDATE()
				   ,@EnterpriseID
				   ,GETDATE()
				   ,'Actions'
				   ,NULL
				   ,NULL
					FROM 
					(	SELECT 
							TC.EntryType
							,TC.EntryID 
							,TC.tranamt AS PaymentAmount
							,TC.CardType AS PaymentType
							,TC.authDate AS PaymentDate
						FROM
							ConfidentialCustomer.dbo.TBL_TempusCard TC WITH (NOLOCK)
						WHERE
							TC.EntryType = @OwnerType 
							AND TC.EntryID = @OwnerID
					UNION ALL		
						SELECT
							TL.EntryType
							,TL.EntryID 
							,TL.EntryAmount AS PaymentAmount
							,'Check' AS PaymentType
							,TL.CheckDate AS PaymentDate
						FROM 
							ConfidentialCustomer.dbo.TBL_Telecheck TL WITH (NOLOCK)
						WHERE
							TL.EntryType = @OwnerType 
							AND TL.EntryID = @OwnerID 
					UNION ALL	
						SELECT
							FD.EntryType
							,FD.EntryID 
							,FD.PaymentAmount AS PaymentAmount
							,FD.CardTypeCode AS PaymentType
							,FD.TransactionDate AS PaymentDate
						FROM 
							ConfidentialCustomer.dbo.TBL_FirstDataPayments FD WITH (NOLOCK)
						WHERE
							FD.EntryType = @OwnerType 
							AND FD.EntryID = @OwnerID
					UNION ALL		
						SELECT
							P.EntryType
							,P.EntryID 
							,P.EntryAmount AS PaymentAmount
							,P.ItemType AS PaymentType
							,P.TransmitDateTime AS PaymentDate
						FROM 
							ConfidentialCustomer.dbo.TBL_PNCCheck P WITH (NOLOCK)
						WHERE
							P.EntryType = @OwnerType 
							AND P.EntryID = @OwnerID 
					UNION ALL	
						SELECT
							HS.EntryType
							,HS.EntryID 
							,HS.payment_amount AS PaymentAmount
							,COALESCE(HS.card_account_type_cd, HS.payment_method_cd) AS PaymentType
							,HS.transaction_date AS PaymentDate
						FROM 
							ConfidentialCustomer.dbo.TBL_HSPOS HS WITH (NOLOCK)
						WHERE
							HS.EntryType = @OwnerType 
							AND HS.EntryID = @OwnerID 
					UNION ALL	
						SELECT
							SOP.EntryType
							,SOP.EntryID 
							,SOP.CollectedAmount AS PaymentAmount
							,SOP.PaymentMethod AS PaymentType
							,SOP.ServiceCallDate AS PaymentDate
						FROM 
							ConfidentialCustomer.dbo.TBL_ServiceOrderPayments SOP WITH (NOLOCK)
						WHERE
							SOP.EntryType = @OwnerType 
							AND SOP.EntryID = @OwnerID	
							) AS S
				
					SET @EntryID = scope_identity()
	
	

		IF @ActionID IS NULL /* add row */
			BEGIN
				INSERT INTO ConfidentialCustomer.dbo.TBL_Actions
				(ActionType, OwnerType, OwnerID, SecondaryType, SecondaryID, StatusID, CreatedEnterpriseID, CreatedDateTime)
				VALUES('Actions', @EntryType, @EntryID, @SecondaryType, @SecondaryID, @StatusID, @EnterpriseID, GETDATE())
			
				SET @ActionID = scope_identity()
			
				UPDATE ConfidentialCustomer.dbo.TBL_PaymentMatch
				SET ActionID = @ActionID
					,StatusID = @StatusID
					,LastModifiedEnterpriseID = @EnterpriseID
					,LastModifiedDateTime = GETDATE()
				WHERE
					EntryType = @EntryType
					AND EntryID = @EntryID
			END		
				BEGIN
					IF @OwnerType = 'HSPOS'
						BEGIN
						SET @LedgerEntryID = (SELECT CurrentLedgerEntryID FROM ConfidentialCustomer.dbo.TBL_HSPOS WITH (NOLOCK) WHERE EntryID = @OwnerID)
						SET @OldServiceUnit = (SELECT ServiceUnit FROM ConfidentialCustomer.dbo.TBL_HSPOS WITH (NOLOCK) WHERE EntryID = @OwnerID)
						SET @OldServiceOrder = (SELECT ServiceOrder FROM ConfidentialCustomer.dbo.TBL_HSPOS WITH (NOLOCK) WHERE EntryID = @OwnerID)
						SET @CustomerID = (SELECT CustomerID FROM ServiceOrder.dbo.TBL_ServiceOrder WITH (NOLOCK) WHERE ServiceOrder = @NewServiceOrder AND ServiceUnit = @NewServiceUnit)

						UPDATE ConfidentialCustomer.dbo.TBL_CustomerLedger
						SET OffsetEntryID = NULL
						,LastModifiedEnterpriseID = @EnterpriseID
						,LastModifiedDateTime = GETDATE()
						WHERE OffsetEntryID = @LedgerEntryID

						UPDATE ConfidentialCustomer.dbo.TBL_CustomerLedger
						SET EntryType = -200
						,LastModifiedEnterpriseID = @EnterpriseID
						,LastModifiedDateTime = GETDATE()
						WHERE EntryID = @LedgerEntryID
						--EntrySourceID = @OwnerID
						--AND EntrySourceType = @OwnerType

						UPDATE f
						SET ServiceUnit = @NewServiceUnit
							,ServiceOrder = @NewServiceOrder
							,CustomerID = @CustomerID
							,CustomerIDUpdateSource = @EntryType
							,CustomerIDUpdateSourceID = @EntryID
							,CurrentLedgerEntryID = NULL
							,LastModifiedDateTime = GETDATE()
							,LastModifiedEnterpriseID = @EnterpriseID
						--SELECT *
						FROM
							ConfidentialCustomer.dbo.TBL_HSPOS f WITH (NOLOCK)
						WHERE
							EntryID = @OwnerID

						INSERT INTO ConfidentialCustomer.dbo.TBL_CustomerLedger
							(EntryType
							,CustomerID
							,EntryAmount
							,EntryAmountParts
							,EntryAmountLabor
							,EntryAmountOther
							,EntryAmountTax
							,EntrySourceType
							,EntrySourceID
							,EntrySourceConfidence
							,EntrySourceDate
							,ServiceUnit
							,ServiceOrder
							,PurchaseOrder
							,EntrySourceCreatedBy
							,LastModifiedDateTime
							,LastModifiedEnterpriseID)
						SELECT 
							200
							,s.CustomerID
							,s.total_transaction_amount AS EntryAmount
							,NULL AS EntryAmountParts
							,NULL AS EntryAmountLabor
							,NULL AS EntryAmountOther
							,NULL AS EntryAmountTax
							,s.EntryType AS EntrySourceType
							,s.EntryID AS EntrySourceID
							,89 AS EntrySourceConfidence
							,s.transaction_date AS EntrySourceDate
							,s.ServiceUnit
							,s.ServiceOrder
							,NULL AS PurchaseOrder
							,'SYSTEM' AS EntrySourceCreatedBy
							,GETDATE() AS LastModifiedDateTime
							,@EnterpriseID AS LastModifiedEnterpriseID
						FROM 
							ConfidentialCustomer.dbo.TBL_HSPOS s WITH (NOLOCK)
						LEFT JOIN
							ConfidentialCustomer.dbo.TBL_CustomerLedger d WITH (NOLOCK)
							ON s.CurrentLedgerEntryID = d.EntryID
						WHERE
							s.transaction_type_cd = 'SAL'
							AND NOT s.ServiceOrder IS NULL
							AND NOT s.CustomerID IS NULL
							AND d.EntrySourceID IS NULL
							AND s.unit_no IN (SELECT RIGHT(Unit, 5) AS Unit FROM Employee.dbo.TBL_UnitCrossReference WITH (NOLOCK) WHERE UnitType IN ('In-Home District', 'HVAC District') AND ReportingRegion IN ('0000820', '0000830', '0000890', '0000900'))
							AND s.EntryID = @OwnerID
							
							SET @LedgerEntryID = scope_identity()

						UPDATE f
						SET CurrentLedgerEntryID = @LedgerEntryID --l.EntryID
						FROM 
							ConfidentialCustomer.dbo.TBL_HSPOS f WITH (NOLOCK)
						--JOIN 
						--	ConfidentialCustomer.dbo.TBL_CustomerLedger l WITH (NOLOCK)
						--	ON f.EntryID = l.EntrySourceID
						--	AND f.EntryType = l.EntrySourceType
						WHERE 
							f.EntryID = @EntryID

						END

					IF @OwnerType = 'TempusCard'
						BEGIN
						SET @LedgerEntryID = (SELECT CurrentLedgerEntryID FROM ConfidentialCustomer.dbo.TBL_TempusCard WITH (NOLOCK) WHERE EntryID = @OwnerID)
						SET @OldServiceUnit = (SELECT ServiceUnit FROM ConfidentialCustomer.dbo.TBL_TempusCard WITH (NOLOCK) WHERE EntryID = @OwnerID)
						SET @OldServiceOrder = (SELECT ServiceOrder FROM ConfidentialCustomer.dbo.TBL_TempusCard WITH (NOLOCK) WHERE EntryID = @OwnerID)
						SET @CustomerID = (SELECT CustomerID FROM ServiceOrder.dbo.TBL_ServiceOrder WITH (NOLOCK) WHERE ServiceOrder = @NewServiceOrder AND ServiceUnit = @NewServiceUnit)

						UPDATE ConfidentialCustomer.dbo.TBL_CustomerLedger
						SET OffsetEntryID = NULL
						,LastModifiedEnterpriseID = @EnterpriseID
						,LastModifiedDateTime = GETDATE()
						WHERE OffsetEntryID = @LedgerEntryID

						UPDATE ConfidentialCustomer.dbo.TBL_CustomerLedger
						SET EntryType = -200
						,LastModifiedEnterpriseID = @EnterpriseID
						,LastModifiedDateTime = GETDATE()
						WHERE EntryID = @LedgerEntryID 
						--EntrySourceID = @OwnerID
						--AND EntrySourceType = @OwnerType

						UPDATE f
						SET ServiceUnit = @NewServiceUnit
							,ServiceOrder = @NewServiceOrder
							,CustomerID = @CustomerID
							,CustomerIDUpdateSource = @EntryType
							,CustomerIDUpdateSourceID = @EntryID
							,CurrentLedgerEntryID = NULL
							,LastModifiedDateTime = GETDATE()
							,LastModifiedEnterpriseID = @EnterpriseID
						--SELECT *
						FROM
							ConfidentialCustomer.dbo.TBL_TempusCard f WITH (NOLOCK)
						WHERE
							EntryID = @OwnerID

						INSERT INTO ConfidentialCustomer.dbo.TBL_CustomerLedger
							(EntryType
							,CustomerID
							,EntryAmount
							,EntryAmountParts
							,EntryAmountLabor
							,EntryAmountOther
							,EntryAmountTax
							,EntrySourceType
							,EntrySourceID
							,EntrySourceConfidence
							,EntrySourceDate
							,ServiceUnit
							,ServiceOrder
							,PurchaseOrder
							,EntrySourceCreatedBy
							,LastModifiedDateTime
							,LastModifiedEnterpriseID)
						
						SELECT 
							200
							,s.CustomerID
							,s.tranamt AS EntryAmount
							,NULL AS EntryAmountParts
							,NULL AS EntryAmountLabor
							,NULL AS EntryAmountOther
							,NULL AS EntryAmountTax
							,s.EntryType AS EntrySourceType
							,s.EntryID AS EntrySourceID
							,89 AS EntrySourceConfidence
							,s.authdate AS EntrySourceDate
							,s.ServiceUnit
							,s.ServiceOrder
							,NULL AS PurchaseOrder
							,COALESCE(UPPER(i.EnterpriseID), s.TechnicianID, 'SYSTEM') AS EntryCreatedBy
							,GETDATE() AS LastModifiedDateTime
							,@EnterpriseID AS LastModifiedEnterpriseID
						FROM 
							ConfidentialCustomer.dbo.TBL_TempusCard s WITH (NOLOCK)
						LEFT JOIN
							ConfidentialCustomer.dbo.TBL_CustomerLedger d WITH (NOLOCK)
							ON s.CurrentLedgerEntryID = d.EntryID
						LEFT JOIN
							Employee.dbo.TBL_NPSTechnicianPay e WITH (NOLOCK)
							ON s.ServiceUnit = e.Unit
							AND s.TechnicianID = e.NPSID
						LEFT JOIN
							Employee.dbo.TBL_Employee_New i
							ON e.EmployeeID = i.EmployeeID
						WHERE
							s.saletype IN ('SALE', 'DEBIT')
							AND s.authdate > '2017-02-05'
							AND s.authcode IS NOT NULL
							AND s.ServiceOrder IS NOT NULL
							AND s.CustomerID IS NOT NULL
							AND d.EntrySourceID IS NULL
							AND s.EntryID = @OwnerID
							
							SET @LedgerEntryID = scope_identity()

						UPDATE f
						SET CurrentLedgerEntryID = @LedgerEntryID --l.EntryID
						FROM 
							ConfidentialCustomer.dbo.TBL_TempusCard f WITH (NOLOCK)
						--JOIN 
						--	ConfidentialCustomer.dbo.TBL_CustomerLedger l WITH (NOLOCK)
						--	ON f.EntryID = l.EntrySourceID
						--	AND f.EntryType = l.EntrySourceType
						WHERE 
							f.EntryID = @OwnerID

						END

					IF @OwnerType = 'FirstDataPayments' OR @OwnerType = 'FirstData'
						BEGIN
						SET @LedgerEntryID = (SELECT CurrentLedgerEntryID FROM ConfidentialCustomer.dbo.TBL_FirstDataPayments WITH (NOLOCK) WHERE EntryID = @OwnerID)
						SET @OldServiceUnit = (SELECT ServiceUnit FROM ConfidentialCustomer.dbo.TBL_FirstDataPayments WITH (NOLOCK) WHERE EntryID = @OwnerID)
						SET @OldServiceOrder = (SELECT ServiceOrder FROM ConfidentialCustomer.dbo.TBL_FirstDataPayments WITH (NOLOCK) WHERE EntryID = @OwnerID)
						SET @CustomerID = (SELECT CustomerID FROM ServiceOrder.dbo.TBL_ServiceOrder WITH (NOLOCK) WHERE ServiceOrder = @NewServiceOrder AND ServiceUnit = @NewServiceUnit)

						UPDATE ConfidentialCustomer.dbo.TBL_CustomerLedger
						SET OffsetEntryID = NULL
						,LastModifiedEnterpriseID = @EnterpriseID
						,LastModifiedDateTime = GETDATE()
						WHERE OffsetEntryID = @LedgerEntryID

						UPDATE ConfidentialCustomer.dbo.TBL_CustomerLedger
						SET EntryType = -200
						,LastModifiedEnterpriseID = @EnterpriseID
						,LastModifiedDateTime = GETDATE()
						WHERE EntryID = @LedgerEntryID
						--EntrySourceID = @OwnerID
						--AND EntrySourceType = @OwnerType

						UPDATE f
						SET ServiceUnit = @NewServiceUnit
							,ServiceOrder = @NewServiceOrder
							,CustomerID = @CustomerID
							,CustomerIDUpdateSource = @EntryType
							,CustomerIDUpdateSourceID = @EntryID
							,CurrentLedgerEntryID = NULL
							,LastModifiedDateTime = GETDATE()
							,LastModifiedEnterpriseID = @EnterpriseID
						--SELECT *
						FROM
							ConfidentialCustomer.dbo.TBL_FirstDataPayments f WITH (NOLOCK)
						WHERE
							EntryID = @OwnerID

						INSERT INTO ConfidentialCustomer.dbo.TBL_CustomerLedger
							(EntryType
							,CustomerID
							,EntryAmount
							,EntryAmountParts
							,EntryAmountLabor
							,EntryAmountOther
							,EntryAmountTax
							,EntrySourceType
							,EntrySourceID
							,EntrySourceConfidence
							,EntrySourceDate
							,ServiceUnit
							,ServiceOrder
							,PurchaseOrder
							,EntrySourceCreatedBy
							,LastModifiedDateTime
							,LastModifiedEnterpriseID)
						SELECT 
							200
							,s.CustomerID
							,s.PaymentAmount AS EntryAmount
							,NULL AS EntryAmountParts
							,NULL AS EntryAmountLabor
							,NULL AS EntryAmountOther
							,NULL AS EntryAmountTax
							,s.EntryType AS EntrySourceType
							,s.EntryID AS EntrySourceID
							,89 AS EntrySourceConfidence
							,s.TransactionDate AS EntrySourceDate
							,s.ServiceUnit
							,s.ServiceOrder
							,NULL AS PurchaseOrder
							,COALESCE(UPPER(i.EnterpriseID), s.TechnicianID, 'SYSTEM') AS EntryCreatedBy
							,GETDATE() AS LastModifiedDateTime
							,@EnterpriseID AS LastModifiedEnterpriseID
						FROM 
							ConfidentialCustomer.dbo.TBL_FirstDataPayments s WITH (NOLOCK)
						LEFT JOIN
							ConfidentialCustomer.dbo.TBL_CustomerLedger d WITH (NOLOCK)
							ON s.CurrentLedgerEntryID = d.EntryID
						LEFT JOIN
							Employee.dbo.TBL_NPSTechnicianPay e WITH (NOLOCK)
							ON s.ServiceUnit = e.Unit
							AND s.TechnicianID = e.NPSID
						LEFT JOIN
							Employee.dbo.TBL_Employee_New i
							ON e.EmployeeID = i.EmployeeID
						WHERE
							s.TransactionDate > '2017-02-05'
							AND s.ServiceOrder IS NOT NULL
							AND s.CustomerID IS NOT NULL
							AND d.EntrySourceID IS NULL
							AND s.EntryID = @OwnerID
							
						SET @LedgerEntryID = scope_identity()

						UPDATE f
						SET CurrentLedgerEntryID = @LedgerEntryID --l.EntryID
						FROM 
							ConfidentialCustomer.dbo.TBL_FirstDataPayments f WITH (NOLOCK)
						--JOIN 
						--	ConfidentialCustomer.dbo.TBL_CustomerLedger l WITH (NOLOCK)
						--	ON f.EntryID = l.EntrySourceID
						--	AND f.EntryType = l.EntrySourceType
						WHERE 
							f.EntryID = @OwnerID

						END

					IF @OwnerType = 'Telecheck'
						BEGIN
						SET @LedgerEntryID = (SELECT CurrentLedgerEntryID FROM ConfidentialCustomer.dbo.TBL_Telecheck WITH (NOLOCK) WHERE EntryID = @OwnerID)
						SET @OldServiceUnit = (SELECT ServiceUnit FROM ConfidentialCustomer.dbo.TBL_Telecheck WITH (NOLOCK) WHERE EntryID = @OwnerID)
						SET @OldServiceOrder = (SELECT ServiceOrder FROM ConfidentialCustomer.dbo.TBL_Telecheck WITH (NOLOCK) WHERE EntryID = @OwnerID)
						SET @CustomerID = (SELECT CustomerID FROM ServiceOrder.dbo.TBL_ServiceOrder WITH (NOLOCK) WHERE ServiceOrder = @NewServiceOrder AND ServiceUnit = @NewServiceUnit)

						UPDATE ConfidentialCustomer.dbo.TBL_CustomerLedger
						SET OffsetEntryID = NULL
						,LastModifiedEnterpriseID = @EnterpriseID
						,LastModifiedDateTime = GETDATE()
						WHERE OffsetEntryID = @LedgerEntryID

						UPDATE ConfidentialCustomer.dbo.TBL_CustomerLedger
						SET EntryType = -200
						,LastModifiedEnterpriseID = @EnterpriseID
						,LastModifiedDateTime = GETDATE()
						WHERE EntryID = @LedgerEntryID
						--EntrySourceID = @OwnerID
						--AND EntrySourceType = @OwnerType


						UPDATE f
						SET ServiceUnit = @NewServiceUnit
							,ServiceOrder = @NewServiceOrder
							,CustomerID = @CustomerID
							,CustomerIDUpdateSource = @EntryType
							,CustomerIDUpdateSourceID = @EntryID
							,CurrentLedgerEntryID = NULL
							,LastModifiedDateTime = GETDATE()
							,LastModifiedEnterpriseID = @EnterpriseID
						--SELECT *
						FROM
							ConfidentialCustomer.dbo.TBL_Telecheck f WITH (NOLOCK)
						WHERE
							EntryID = @OwnerID

						INSERT INTO ConfidentialCustomer.dbo.TBL_CustomerLedger
							(EntryType
							,CustomerID
							,EntryAmount
							,EntryAmountParts
							,EntryAmountLabor
							,EntryAmountOther
							,EntryAmountTax
							,EntrySourceType
							,EntrySourceID
							,EntrySourceConfidence
							,EntrySourceDate
							,ServiceUnit
							,ServiceOrder
							,PurchaseOrder
							,EntrySourceCreatedBy
							,LastModifiedDateTime
							,LastModifiedEnterpriseID)
						SELECT 
							200
							,s.CustomerID
							,s.EntryAmount AS EntryAmount
							,NULL AS EntryAmountParts
							,NULL AS EntryAmountLabor
							,NULL AS EntryAmountOther
							,NULL AS EntryAmountTax
							,s.EntryType AS EntrySourceType
							,s.EntryID AS EntrySourceID
							,89 AS EntrySourceConfidence
							,s.CheckDate AS EntrySourceDate
							,s.ServiceUnit
							,s.ServiceOrder
							,NULL AS PurchaseOrder
							,COALESCE(UPPER(i.EnterpriseID), s.TechnicianID, 'SYSTEM') AS EntryCreatedBy
							,GETDATE() AS LastModifiedDateTime
							,@EnterpriseID AS LastModifiedEnterpriseID
						FROM 
							ConfidentialCustomer.dbo.TBL_Telecheck s WITH (NOLOCK)
						LEFT JOIN
							ConfidentialCustomer.dbo.TBL_CustomerLedger d WITH (NOLOCK)
							ON s.CurrentLedgerEntryID = d.EntryID
						LEFT JOIN
							Employee.dbo.TBL_NPSTechnicianPay e WITH (NOLOCK)
							ON s.ServiceUnit = e.Unit
							AND s.TechnicianID = e.NPSID
						LEFT JOIN
							Employee.dbo.TBL_Employee_New i
							ON e.EmployeeID = i.EmployeeID
						WHERE
							s.CheckDate > '2017-02-05'
							AND s.TransactionCode IN ('T')
							AND s.CustomerID IS NOT NULL
							AND s.ServiceOrder IS NOT NULL
							AND d.EntrySourceID IS NULL
							AND s.EntryID = @OwnerID
							
							SET @LedgerEntryID = scope_identity()

						UPDATE f
						SET CurrentLedgerEntryID = @LedgerEntryID --l.EntryID
						FROM 
							ConfidentialCustomer.dbo.TBL_Telecheck f WITH (NOLOCK)
						--JOIN 
						--	ConfidentialCustomer.dbo.TBL_CustomerLedger l WITH (NOLOCK)
						--	ON f.EntryID = l.EntrySourceID
						--	AND f.EntryType = l.EntrySourceType
						WHERE 
							f.EntryID = @OwnerID
						END
					IF @OwnerType = 'PNCCheck'
						BEGIN
						SET @LedgerEntryID = (SELECT CurrentLedgerEntryID FROM ConfidentialCustomer.dbo.TBL_PNCCheck WITH (NOLOCK) WHERE EntryID = @OwnerID)
						SET @OldServiceUnit = (SELECT ServiceUnit FROM ConfidentialCustomer.dbo.TBL_PNCCheck WITH (NOLOCK) WHERE EntryID = @OwnerID)
						SET @OldServiceOrder = (SELECT ServiceOrder FROM ConfidentialCustomer.dbo.TBL_PNCCheck WITH (NOLOCK) WHERE EntryID = @OwnerID)
						SET @CustomerID = (SELECT CustomerID FROM ServiceOrder.dbo.TBL_ServiceOrder WITH (NOLOCK) WHERE ServiceOrder = @NewServiceOrder AND ServiceUnit = @NewServiceUnit)

						UPDATE ConfidentialCustomer.dbo.TBL_CustomerLedger
						SET OffsetEntryID = NULL
						,LastModifiedEnterpriseID = @EnterpriseID
						,LastModifiedDateTime = GETDATE()
						WHERE OffsetEntryID = @LedgerEntryID

						UPDATE ConfidentialCustomer.dbo.TBL_CustomerLedger
						SET EntryType = -200
						,LastModifiedEnterpriseID = @EnterpriseID
						,LastModifiedDateTime = GETDATE()
						WHERE EntryID = @LedgerEntryID
						--EntrySourceID = @OwnerID
						--AND EntrySourceType = @OwnerType


						UPDATE f
						SET ServiceUnit = @NewServiceUnit
							,ServiceOrder = @NewServiceOrder
							,CustomerID = @CustomerID
							,CustomerIDUpdateSource = @EntryType
							,CustomerIDUpdateSourceID = @EntryID
							,CurrentLedgerEntryID = NULL
							,LastModifiedDateTime = GETDATE()
							,LastModifiedEnterpriseID = @EnterpriseID
						--SELECT *
						FROM
							ConfidentialCustomer.dbo.TBL_PNCCheck f WITH (NOLOCK)
						WHERE
							EntryID = @OwnerID

						INSERT INTO ConfidentialCustomer.dbo.TBL_CustomerLedger
							(EntryType
							,CustomerID
							,EntryAmount
							,EntryAmountParts
							,EntryAmountLabor
							,EntryAmountOther
							,EntryAmountTax
							,EntrySourceType
							,EntrySourceID
							,EntrySourceConfidence
							,EntrySourceDate
							,ServiceUnit
							,ServiceOrder
							,PurchaseOrder
							,EntrySourceCreatedBy
							,LastModifiedDateTime
							,LastModifiedEnterpriseID)
						SELECT 
							200
							,s.CustomerID
							,s.EntryAmount AS EntryAmount
							,NULL AS EntryAmountParts
							,NULL AS EntryAmountLabor
							,NULL AS EntryAmountOther
							,NULL AS EntryAmountTax
							,s.EntryType AS EntrySourceType
							,s.EntryID AS EntrySourceID
							,89 AS EntrySourceConfidence
							,s.TransmitDateTime AS EntrySourceDate
							,s.ServiceUnit
							,s.ServiceOrder
							,NULL AS PurchaseOrder
							,COALESCE(UPPER(REPLACE([UserLoginID],'119600','')), 'SYSTEM') AS EntryCreatedBy
							,GETDATE() AS LastModifiedDateTime
							,@EnterpriseID AS LastModifiedEnterpriseID
						FROM 
							ConfidentialCustomer.dbo.TBL_PNCCheck s WITH (NOLOCK)
						LEFT JOIN
							ConfidentialCustomer.dbo.TBL_CustomerLedger d WITH (NOLOCK)
							ON s.CurrentLedgerEntryID = d.EntryID
						--LEFT JOIN
						--	Employee.dbo.TBL_NPSTechnicianPay e WITH (NOLOCK)
						--	ON s.ServiceUnit = e.Unit
						--	AND s.TechnicianID = e.NPSID
						--LEFT JOIN
						--	Employee.dbo.TBL_Employee_New i
						--	ON e.EmployeeID = i.EmployeeID
						WHERE
							s.TransmitDateTime > '2017-02-05'
							AND s.CustomerID IS NOT NULL
							AND s.ServiceOrder IS NOT NULL
							AND d.EntrySourceID IS NULL
							AND s.EntryID = @OwnerID
							
							SET @LedgerEntryID = scope_identity()

						UPDATE f
						SET CurrentLedgerEntryID = @LedgerEntryID --l.EntryID
						FROM 
							ConfidentialCustomer.dbo.TBL_PNCCheck f WITH (NOLOCK)
						WHERE 
							f.EntryID = @OwnerID
					END
						
					IF @OwnerType = 'ServiceOrderPayments' OR @OwnerType = 'NPS'
						BEGIN
					--	SET @LedgerEntryID = (SELECT CurrentLedgerEntryID FROM ConfidentialCustomer.dbo.TBL_ServiceOrderPayments WITH (NOLOCK) WHERE EntryID = @OwnerID)
					--	SET @OldServiceUnit = (SELECT ServiceUnit FROM ConfidentialCustomer.dbo.TBL_ServiceOrderPayments WITH (NOLOCK) WHERE EntryID = @OwnerID)
					--	SET @OldServiceOrder = (SELECT ServiceOrder FROM ConfidentialCustomer.dbo.TBL_ServiceOrderPayments WITH (NOLOCK) WHERE EntryID = @OwnerID)
					--	SET @CustomerID = (SELECT CustomerID FROM ServiceOrder.dbo.TBL_ServiceOrder WITH (NOLOCK) WHERE ServiceOrder = @NewServiceOrder AND ServiceUnit = @NewServiceUnit)

					--	UPDATE ConfidentialCustomer.dbo.TBL_CustomerLedger
					--	SET OffsetEntryID = NULL
					--,LastModifiedEnterpriseID = @EnterpriseID
					--,LastModifiedDateTime = GETDATE()
					--	WHERE OffsetEntryID = @LedgerEntryID

					--	UPDATE ConfidentialCustomer.dbo.TBL_CustomerLedger
					--SET EntryType = -200
					--,LastModifiedEnterpriseID = @EnterpriseID
					--,LastModifiedDateTime = GETDATE()
					--WHERE EntryID = @LedgerEntryID
					----EntrySourceID = @OwnerID
					----AND EntrySourceType = @OwnerType


					--	UPDATE f
					--	SET ServiceUnit = @NewServiceUnit
					--		,ServiceOrder = @NewServiceOrder
					--		,CustomerID = @CustomerID
					--		--,CustomerIDUpdateSource = @EntryType
					--		--,CustomerIDUpdateSourceID = @EntryID
					--		,CurrentLedgerEntryID = NULL
					--		--,LastModifiedDateTime = GETDATE()
					--		--,LastModifiedEnterpriseID = @EnterpriseID
					--	--SELECT *
					--	FROM
					--		ConfidentialCustomer.dbo.TBL_ServiceOrderPayments f WITH (NOLOCK)
					--	WHERE
					--		EntryID = @OwnerID

						INSERT INTO ConfidentialCustomer.dbo.TBL_CustomerLedger
							(EntryType
							,CustomerID
							,EntryAmount
							,EntryAmountParts
							,EntryAmountLabor
							,EntryAmountOther
							,EntryAmountTax
							,EntrySourceType
							,EntrySourceID
							,EntrySourceConfidence
							,EntrySourceDate
							,ServiceUnit
							,ServiceOrder
							,PurchaseOrder
							,EntrySourceCreatedBy)
						SELECT 
							200
							,s.CustomerID
							,s.CollectedAmount AS EntryAmount
							,NULL AS EntryAmountParts
							,NULL AS EntryAmountLabor
							,NULL AS EntryAmountOther
							,NULL AS EntryAmountTax
							,s.EntryType AS EntrySourceType
							,s.EntryID AS EntrySourceID
							,89 AS EntrySourceConfidence
							,s.ServiceCallDate AS EntrySourceDate
							,s.ServiceUnit
							,s.ServiceOrder
							,NULL AS PurchaseOrder
							,'SYSTEM' AS EntrySourceCreatedBy
						FROM 
							ConfidentialCustomer.dbo.TBL_ServiceOrderPayments s WITH (NOLOCK)
						LEFT JOIN
							ConfidentialCustomer.dbo.TBL_CustomerLedger d WITH (NOLOCK)
							ON s.CurrentLedgerEntryID = d.EntryID
						WHERE
							s.ServiceCallDate > '2017-02-05'
							AND s.CustomerID IS NOT NULL
							AND s.ServiceOrder IS NOT NULL
							AND d.EntrySourceID IS NULL
							AND s.EntryID = @OwnerID
					SET @LedgerEntryID = scope_identity()

						UPDATE f
						SET CurrentLedgerEntryID = @LedgerEntryID --l.EntryID
						FROM 
							ConfidentialCustomer.dbo.TBL_ServiceOrderPayments f WITH (NOLOCK)
					--	JOIN 
					--		ConfidentialCustomer.dbo.TBL_CustomerLedger l WITH (NOLOCK)
					--		ON f.EntryID = l.EntrySourceID
					--		AND f.EntryType = l.EntrySourceType
						WHERE 
							f.EntryID = @OwnerID
						END
					END

				END

				/*
				ELSE /* update row */
					BEGIN
			
						UPDATE TBL_Actions SET
							StatusID = @StatusID,
							ModifiedEnterpriseID = @EnterpriseID,
							ModifiedDateTime = GETDATE()
						WHERE 
							ActionID = @ActionID
					END
				*/
		








GO
/****** Object:  StoredProcedure [dbo].[PROC_Coaching_Insert_NA]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*************************************************************************
	Procedure Name: PROC_Comments_Update_NA
	Returns:		Failure Code (@ErrorCode)
	Primary User:	Everyone
	Designer:		Rob O'Brien
	Company:		Sears, Roebuck and Co
	Last Updated:	Fri Mar 02 12:26:28 CST 2018 @810 /Internet Time/
	Updated by:		Rob O'Brien
	Information:	Procedure for adding or updating a comment
	************************************************************************
	Version:		1.00
	Date:			Fri Mar 02 12:26:36 CST 2018 @810 /Internet Time/
	Updated By:		Rob O'Brien
***************************************************************************/
CREATE Procedure [dbo].[PROC_Coaching_Insert_NA]
	@CoachingID int = Null OUTPUT, /* Unique ID number for the comment.  To update an existing comment, supply the comment ID.  Otherwise, an insert will be done. */
	@OwnerType varchar(30), /* Ownertype from the ownertype process managed for a database side applicaiton identification. */
	@OwnerID int, /* OwnerID number from the calling applicaiton. */
	@SecondaryType varchar(30) = NULL, /* Ownertype from the ownertype process managed for a database side applicaiton identification. */
	@SecondaryID int = NULL, /* OwnerID number from the calling applicaiton. */
	@StatusID int, /* OwnerID number from the calling applicaiton. */
	@EnterpriseID varchar(8), /* Employee LDAP ID making the insert or update.  Upper case. */
	@AssignedEnterpriseID varchar(8), /* Employee LDAP ID making the insert or update.  Upper case. */
	@ServiceUnit char(7) = NULL,
	@ServiceOrder char(8) = NULL,
	@CustomerID INT = NULL,
	@Comment varchar(max), /* comment text. HTML supported */
	@ErrorCode varchar(150) = NULL OUTPUT /* error message if any during processing */ 
AS
	SET NOCOUNT ON
	SET @EnterpriseID = LTRIM(RTRIM(UPPER(@EnterpriseID)))
	SET @AssignedEnterpriseID = LTRIM(RTRIM(UPPER(@AssignedEnterpriseID)))
	SET @Comment = LTRIM(RTRIM(@Comment))
	SET @OwnerType = LTRIM(RTRIM(@OwnerType))


	IF @OwnerType = '' /* set @OwnerType to null if empty string */
		SET @OwnerType = NULL
	IF @EnterpriseID = '' /* set @EnterpriseID to null if empty string */
		SET @EnterpriseID = NULL
	IF @OwnerID = 0 /* set @OwnerID to null if zero */
		SET @OwnerID = NULL
	IF @Comment = '' /* set @Comment to null if empty string */
		SET @Comment = NULL


	IF @Comment Is Null /* Return error code if @Comment is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for Comment, it is Required.'
		Return
	END
	IF @OwnerID Is Null /* Return error code if @OwnerID is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for OwnerID, it is Required.'
		Return
	END
	IF @EnterpriseID Is Null /* Return error code if @EnterpriseID is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for EmpID, it is Required.'
		Return
	END
	IF @AssignedEnterpriseID Is Null /* Return error code if @AssignedEnterpriseID is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for Assigned Enterprise ID, it is Required.'
		Return
	END
	IF @OwnerType Is Null /* Return error code if @OwnerType is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for OwnerType, it is Required.'
		Return
	END	
	
	
	/* if a comment ID was passed in, update it. otherwise create a new row */
	IF @CoachingID IS NULL /* add row */
		BEGIN
		INSERT INTO TBL_Coaching
		(AssignedEnterpriseID, StatusID, ServiceUnit, ServiceOrder, OwnerType, OwnerID, SecondaryType, SecondaryID, CustomerID, CreatedEnterpriseID, CreatedDateTime, Comment)
		VALUES(@AssignedEnterpriseID, @StatusID, @ServiceUnit, @ServiceOrder, @OwnerType, @OwnerID, @SecondaryType, @SecondaryID,@CustomerID, @EnterpriseID, GETDATE(), @Comment)
		
		SET @CoachingID = scope_identity()
	/*
		/* update the unit/so in the table if this is a known type and do any other updates */
		IF @OwnerType = 'WorkQueue'
			BEGIN
			UPDATE TBL_Comments SET UnitNumber = WQ_UnitNumber ,ServiceOrderNumber = RIGHT('00000000' + cast(WQ_OrderID as varchar(10)),8) FROM TBL_Comments INNER JOIN dbo.VIEW_WorkQueue_All ON C_OwnerID = WQ_WorkQueueID WHERE C_OwnerType = 'WorkQueue' AND WQ_WorkQueueID = @OwnerID AND CommentID = @CommentID 
			UPDATE TBL_WorkQueue SET WQ_CommentRecorded = 1 WHERE WQ_WorkQueueID = @OwnerID
			END
	*/
		END		
	/*
	ELSE /* update row */
		BEGIN
		
		INSERT INTO TBL_CommentLog (CommentID, OriginalDate, UpdatedDate, OriginalText, OriginalUserID, UpdatedByID)
		SELECT CommentID, CreatedDateTime, GETDATE(), Comment, CreatedEnterpriseID, @EnterpriseID FROM TBL_Comments WHERE CommentID = @CommentID
  		
		UPDATE TBL_Comments SET
			Comment = @Comment,
			IsPrivate = @IsPrivate,
			ModifiedEnterpriseID = @EnterpriseID,
			ModifiedDateTime = GETDATE()
			WHERE CommentID = @CommentID
	END
	*/














GO
/****** Object:  StoredProcedure [dbo].[PROC_Comments_Update_NA]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





/*************************************************************************
	Procedure Name: PROC_Comments_Update_NA
	Returns:		Failure Code (@ErrorCode)
	Primary User:	Everyone
	Designer:		Rob O'Brien
	Company:		Sears, Roebuck and Co
	Last Updated:	Fri Mar 02 12:26:28 CST 2018 @810 /Internet Time/
	Updated by:		Rob O'Brien
	Information:	Procedure for adding or updating a comment
	************************************************************************
	Version:		1.00
	Date:			Fri Mar 02 12:26:36 CST 2018 @810 /Internet Time/
	Updated By:		Rob O'Brien
	************************************************************************
	Version:		1.01
	Date:			Mon Mar 05 12:26:36 CST 2018 @810 /Internet Time/
	Updated By:		Rob O'Brien
	Comments:		Added the SubOwnerType and SubOwnerID fields into the procedure
***************************************************************************/
CREATE PROCEDURE [dbo].[PROC_Comments_Update_NA]
	@CommentID INT = Null OUTPUT, /* Unique ID number for the comment.  To update an existing comment, supply the comment ID.  Otherwise, an insert will be done. */
	@OwnerType VARCHAR(30), /* Ownertype from the ownertype process managed for a database side applicaiton identification. */
	@OwnerID INT, /* OwnerID number from the calling applicaiton. */
	@SecondaryType VARCHAR(30) = NULL, /* Ownertype from the ownertype process managed for a database side applicaiton identification. */
	@SecondaryID INT = NULL, /* OwnerID number from the calling applicaiton. */
	@Comment VARCHAR(MAX), /* comment text. HTML supported */
	@EnterpriseID VARCHAR(10) = NULL, /* Employee LDAP ID making the insert or update.  Upper case. */
	@ServiceUnit CHAR(7) = NULL, /*  */
	@ServiceOrder CHAR(8) = NULL, /*  */
	@CustomerID INT = NULL, /*  */
	@IsPrivate BIT = 0 OUTPUT, /* bit denoting if this comment is private.  Private comments are encrypted at the database layer.  This value can be changed during processing */
	@ErrorCode VARCHAR(150) = NULL OUTPUT /* error message if any during processing */ 
AS
	SET NOCOUNT ON
	SET @EnterpriseID = LTRIM(RTRIM(UPPER(@EnterpriseID)))
	SET @Comment = LTRIM(RTRIM(@Comment))
	SET @OwnerType = LTRIM(RTRIM(@OwnerType))


	IF @OwnerType = '' /* set @OwnerType to null if empty string */
		SET @OwnerType = NULL
	IF @EnterpriseID = '' /* set @EnterpriseID to null if empty string */
		SET @EnterpriseID = NULL
	IF @CommentID = 0 /* set @CommentID to null if zero */
		SET @CommentID = NULL
	IF @OwnerID = 0 /* set @OwnerID to null if zero */
		SET @OwnerID = NULL
	IF @IsPrivate IS NULL /* set @IsPrivate to 0 if Null */
		SET @IsPrivate = 0
	IF @Comment = '' /* set @Comment to null if empty string */
		SET @Comment = NULL


	IF @Comment IS NULL /* Return error code if @Comment is null */
	 	BEGIN
		SET @ErrorCode = 'Please enter a value for Comment, it is Required.'
		RETURN
	END
	IF @OwnerID IS NULL /* Return error code if @OwnerID is null */
	 	BEGIN
		SET @ErrorCode = 'Please enter a value for OwnerID, it is Required.'
		RETURN
	END
	IF @EnterpriseID IS NULL /* Return error code if @EnterpriseID is null */
	 	BEGIN
		SET @ErrorCode = 'Please enter a value for EmpID, it is Required.'
		RETURN
	END
	
	IF @OwnerType IS NULL /* Return error code if @OwnerType is null */
	 	BEGIN
		SET @ErrorCode = 'Please enter a value for OwnerType, it is Required.'
		RETURN
	END	
	
	/*SET @OwnerType = (SELECT LC_Description FROM TBL_LookupCodes with (nolock) WHERE LC_OwnerType = 'Owner Type' AND LC_Description = @OwnerType) /* verify valid @OwnerType and fix case if required. */
	IF @OwnerType = '' /* set @OwnerType to null if empty string */
		SET @OwnerType = NULL
	
	/* check that everything is passed in */
	IF @OwnerType Is Null /* Return error code if @OwnerType is null */
	 	Begin
		SET @ErrorCode = 'Owner Type is not configured for comments'
		Return
	END
	
		
	/* encrypt if necessary */
	IF (@Comment LIKE '% ss %' OR @Comment LIKE '%ss#%' OR @Comment LIKE '%social security%' OR @Comment LIKE '%password%') AND @IsPrivate = 0
		BEGIN
		SET @IsPrivate = 1
		IF NOT LEFT(ltrim(rtrim(@Comment)), len('Note: Automatically encrypted due to message content filter.')) = 'Note: Automatically encrypted due to message content filter.'
			SET @Comment = 'Note: Automatically encrypted due to message content filter.<br /><br />' + @Comment
		END
	IF @IsPrivate = 1 
		SET @Comment = EncryptByPassPhrase((SELECT convert(varchar(100),DECRYPTByPassPhrase('#nfdt13', P_Password)) FROM  nfdt..TBL_Password WHERE P_Name = 'Comment'), left(@Comment,4000))
	*/
	/* if a comment ID was passed in, update it. otherwise create a new row */
	IF @CommentID IS NULL /* add row */
		BEGIN
		INSERT INTO TBL_Comments 
		(OwnerType, OwnerID, SecondaryType, SecondaryID, IsPrivate, Comment, ServiceUnit, ServiceOrder, CustomerID, CreatedEnterpriseID, CreatedDateTime, LastModifiedEnterpriseID, LastModifiedDateTime)
		VALUES(@OwnerType, @OwnerID, @SecondaryType, @SecondaryID, @IsPrivate, @Comment, @ServiceUnit, @ServiceOrder, @CustomerID, @EnterpriseID, GETDATE(), @EnterpriseID, GETDATE())
		
		SET @CommentID = scope_identity()
	/*
		/* update the unit/so in the table if this is a known type and do any other updates */
		IF @OwnerType = 'WorkQueue'
			BEGIN
			UPDATE TBL_Comments SET UnitNumber = WQ_UnitNumber ,ServiceOrderNumber = RIGHT('00000000' + cast(WQ_OrderID as varchar(10)),8) FROM TBL_Comments INNER JOIN dbo.VIEW_WorkQueue_All ON C_OwnerID = WQ_WorkQueueID WHERE C_OwnerType = 'WorkQueue' AND WQ_WorkQueueID = @OwnerID AND CommentID = @CommentID 
			UPDATE TBL_WorkQueue SET WQ_CommentRecorded = 1 WHERE WQ_WorkQueueID = @OwnerID
			END
	*/
		END		
	
	ELSE /* update row */
		BEGIN
		
		INSERT INTO TBL_CommentLog 
		(CommentID, OriginalDate, UpdatedDate, OriginalText, OriginalUserID, UpdatedByID)
		SELECT CommentID, CreatedDateTime, GETDATE(), Comment, CreatedEnterpriseID, @EnterpriseID 
		FROM TBL_Comments 
		WHERE CommentID = @CommentID
  		
		UPDATE TBL_Comments SET
			Comment = @Comment,
			IsPrivate = @IsPrivate,
			LastModifiedEnterpriseID = @EnterpriseID,
			LastModifiedDateTime = GETDATE()
			WHERE CommentID = @CommentID
	END
	/*
	EXECUTE PROC_HashTag_ProcessText_NA @InputText = @Comment, @SourceOwnerType = 'Comment', @SourceOwnerID = @CommentID, @LDAPID = @EnterpriseID, @UpdateSourceData = 1
	*/












GO
/****** Object:  StoredProcedure [dbo].[PROC_CustomerContactOveride_Insert]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*************************************************************************
	Procedure Name: PROC_CustomerContactOveride_Insert
	Returns:		Failure Code (@ErrorCode)
	Primary User:	Everyone
	Designer:		Rob O'Brien
	Company:		Sears, Roebuck and Co
	Last Updated:	Fri Mar 02 12:26:28 CST 2018 @810 /Internet Time/
	Updated by:		Rob O'Brien
	Information:	Procedure for adding or updating a comment
	************************************************************************
	Version:		1.00
	Date:			Fri Mar 02 12:26:36 CST 2018 @810 /Internet Time/
	Updated By:		Rob O'Brien
***************************************************************************/
CREATE Procedure [dbo].[PROC_CustomerContactOveride_Insert]
	@EntryID int = Null OUTPUT, /* Unique ID number for the comment.  To update an existing comment, supply the comment ID.  Otherwise, an insert will be done. */
	@OwnerType varchar(30), /* Ownertype from the ownertype process managed for a database side applicaiton identification. */
	@OwnerID int, /* OwnerID number from the calling applicaiton. */
	@PreferenceID int = NULL, /* OwnerID number from the calling applicaiton. */
	@CustomerID int, /* OwnerID number from the calling applicaiton. */
	@CustomerName varchar(50), /* Employee LDAP ID making the insert or update.  Upper case. */
	@CustomerEmail varchar(50), /* Employee LDAP ID making the insert or update.  Upper case. */
	@CustomerPhone varchar(50), /* Employee LDAP ID making the insert or update.  Upper case. */
	@CustomerAltPhone varchar(50), /* Employee LDAP ID making the insert or update.  Upper case. */
	@CustomerStreetAddress varchar(50), /* Employee LDAP ID making the insert or update.  Upper case. */
	@CustomerCity varchar(50), /* Employee LDAP ID making the insert or update.  Upper case. */
	@CustomerState varchar(50), /* Employee LDAP ID making the insert or update.  Upper case. */
	@CustomerZip varchar(50), /* Employee LDAP ID making the insert or update.  Upper case. */
	@EnterpriseID varchar(8), /* Employee LDAP ID making the insert or update.  Upper case. */
	@ErrorCode varchar(150) = NULL OUTPUT /* error message if any during processing */ 
AS
	SET NOCOUNT ON
	SET @EnterpriseID = LTRIM(RTRIM(UPPER(@EnterpriseID)))
	SET @OwnerType = LTRIM(RTRIM(@OwnerType))


	IF @OwnerType = '' /* set @OwnerType to null if empty string */
		SET @OwnerType = NULL
	IF @EnterpriseID = '' /* set @EnterpriseID to null if empty string */
		SET @EnterpriseID = NULL
	IF @OwnerID = 0 /* set @OwnerID to null if zero */
		SET @OwnerID = NULL
	IF @PreferenceID = '' /* set @Comment to null if empty string */
		SET @PreferenceID = NULL
	IF @CustomerID = '' /* set @Comment to null if empty string */
		SET @CustomerID = NULL
	IF @CustomerName = '' /* set @Comment to null if empty string */
		SET @CustomerName = NULL
	IF @CustomerEmail = '' /* set @Comment to null if empty string */
		SET @CustomerEmail = NULL
	IF @CustomerPhone = '' /* set @Comment to null if empty string */
		SET @CustomerPhone = NULL
	IF @CustomerAltPhone = '' /* set @Comment to null if empty string */
		SET @CustomerAltPhone = NULL
	IF @CustomerStreetAddress = '' /* set @Comment to null if empty string */
		SET @CustomerStreetAddress = NULL
	IF @CustomerCity = '' /* set @Comment to null if empty string */
		SET @CustomerCity = NULL
	IF @CustomerState = '' /* set @Comment to null if empty string */
		SET @CustomerState = NULL
	IF @CustomerZip = '' /* set @Comment to null if empty string */
		SET @CustomerZip = NULL


	IF @OwnerID Is Null /* Return error code if @OwnerID is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for OwnerID, it is Required.'
		Return
	END
	IF @EnterpriseID Is Null /* Return error code if @EnterpriseID is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for EmpID, it is Required.'
		Return
	END
	IF @OwnerType Is Null /* Return error code if @OwnerType is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for OwnerType, it is Required.'
		Return
	END	
	IF @CustomerName Is Null /* Return error code if @CustomerName is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for Customer Name, it is Required.'
		Return
	END	
	IF @CustomerStreetAddress Is Null /* Return error code if @CustomerStreetAddress is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for Customer Street Address, it is Required.'
		Return
	END	
	IF @CustomerCity Is Null /* Return error code if @CustomerCity is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for Customer City, it is Required.'
		Return
	END	
	IF @CustomerState Is Null /* Return error code if @CustomerState is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for Customer State, it is Required.'
		Return
	END	
	IF @CustomerZip Is Null /* Return error code if @CustomerZip is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for Customer Zip, it is Required.'
		Return
	END	
	IF @CustomerID Is Null /* Return error code if @CustomerID is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for Customer ID, it is Required.'
		Return
	END	

	
	
	/* if a comment ID was passed in, update it. otherwise create a new row */
	IF @EntryID IS NULL /* add row */
		BEGIN
		INSERT INTO ConfidentialCustomer.dbo.TBL_CustomerContactOverride
			(	EntryType
				,OwnerID
				,OwnerType
				,PreferenceID
				,CustomerID
				,CustomerName
				,CustomerEmail
				,CustomerPhone
				,CustomerAltPhone
				,CustomerStreetAddress
				,CustomerCity
				,CustomerState
				,CustomerZip
				,CreatedEnterpriseID
				,CreatedDateTime)
		VALUES
			(	'CustomerContactOverride'
				,@OwnerID
				,@OwnerType
				,@PreferenceID
				,@CustomerID
				,@CustomerName
				,@CustomerEmail
				,@CustomerPhone
				,@CustomerAltPhone
				,@CustomerStreetAddress
				,@CustomerCity
				,@CustomerState
				,@CustomerZip
				,@EnterpriseID
				,GETDATE()
			)
		
		SET @EntryID = scope_identity()

		END		
	

GO
/****** Object:  StoredProcedure [dbo].[PROC_Insert_ManualOffset]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*************************************************************************
	Procedure Name: PROC_Insert_ManualOffset
	Returns:		Failure Code (@ErrorCode)
	Primary User:	Everyone
	Designer:		Brandi Fischer
	Company:		Sears, Roebuck and Co
	Last Updated:	Fri Jul 06 13:00:00 EST 2018 @810 /Internet Time/
	Updated by:		Brandi Fischer
	Information:	Procedure for adding a manual offset
	************************************************************************
	Version:		1.00
	Date:			Fri Jul 06 13:00:00 EST 2018 @810 /Internet Time/
	Updated By:		Brandi Fischer
	Version:		1.00
	Date:			Wed Oct 17 14:28:07 EST 2018 @810 /Internet Time/
	Updated By:		Brandi Fischer
	Notes:			Enabled being able to pass and update labor and parts amount, also removed last step making incorrect updates. Add OffsetType logic if not passed through. and Error code if reason id is not passed in.
	
***************************************************************************/
CREATE Procedure [dbo].[PROC_Insert_ManualOffset]
	@EntryID int = NULL OUTPUT,  /*Unique ID number for the offset entry.  To update an existing etnry, supply the offset Entry ID.  Otherwise, an insert will be done. */
	@OffsetType int = NULL, /* OffsetType  */	
	@OwnerType varchar(30), /* Ownertype from the ownertype process managed for a database side applicaiton identification. */
	@OwnerID int = NULL, /* OwnerID number from the calling applicaiton. */
	@CustomerID int, 
	@SecondaryType varchar(30) = NULL, /* Ownertype from the ownertype process managed for a database side applicaiton identification. */
	@SecondaryID int = NULL, /* OwnerID number from the calling application. */
	@EnterpriseID varchar(8), /* Employee LDAP ID making the insert or update.  Upper case. */
	@PaymentMethod char(2) = NULL, /* Transaction Method (Card Type or CK). */
	@Coverage char(2) = NULL,
	@PartsTotal decimal(18,4) = NULL,
	@LaborTotal decimal(18,4) = NULL,
	@EntryAmount decimal(18,4),
	@ReasonID int = NULL,
	@ServiceUnit char (7) = NULL,
	@ServiceOrder char (8) = NULL,
	@LedgerEntryID int = NULL OUTPUT,
	@ErrorCode varchar(150) = NULL OUTPUT /* error message if any during processing */ 
AS
	SET NOCOUNT ON

	SET @EnterpriseID = LTRIM(RTRIM(UPPER(@EnterpriseID)))
	SET @OwnerType = LTRIM(RTRIM(@OwnerType))



	IF @OwnerType = '' /* set @OwnerType to null if empty string */
		SET @OwnerType = NULL
	IF @OwnerID = 0 /* set @OwnerID to null if zero */
		SET @OwnerID = NULL
	IF @EnterpriseID = '' /* set @EnterpriseID to null if empty string */
		SET @EnterpriseID = NULL
	IF @SecondaryID = 0 /* set @CommentID to null if zero */
		SET @SecondaryID = NULL
	IF @SecondaryType = '' /* set @SecondaryType to null if empty string */
		SET @SecondaryType = NULL
	IF @EntryAmount = 0 
		SET @EntryAmount = NULL


	IF @EnterpriseID Is Null /* Return error code if @EnterpriseID is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for EmpID, it is Required.'
		Return
	END
	--IF @OwnerID Is Null /* Return error code if @OwnerID is null */
	-- 	Begin
	--	SET @ErrorCode = 'Please enter a value for OwnerID, it is Required.'
	--	Return
	--END	
	IF @OwnerType Is Null /* Return error code if @OwnerType is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for OwnerType, it is Required.'
		Return
	END	
	
	IF @ReasonID Is Null /* Return error code if @OwnerType is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for reason, it is Required.'
		Return
	END	
	
	IF @OffsetType IS NULL
		SET @OffsetType = (SELECT CASE WHEN AncillaryStatus = 1 THEN 201 ELSE 202 END 
			FROM ConfidentialCustomer.dbo.TBL_LookupCodes 
			WHERE OwnerType = @OwnerType AND OwnerID = 'ReasonID' AND NumericValue = @ReasonID)
	
	/* if a manual offset Entry ID was passed in, update it. otherwise create a new row */
	IF @EntryID IS NULL /* add row */
		BEGIN
			INSERT INTO [ConfidentialCustomer].[dbo].[TBL_ManualOffset]
			   ([EntryType]
			   ,[CustomerID]
			   --,[PaymentMethod]
			   --,[Coverage]
			   ,[PartsTotal]
			   ,[LaborTotal]
			   ,[EntryAmount]
			   --,[OriginalLedgerEntryID]
			   --,[CurrentLedgerEntryID]
			   ,[CreatedEnterpriseID]
			   ,[CreatedDateTime]
			   ,[OwnerType]
			   ,[OwnerID]
			   ,[ReasonID]
			   ,[OffsetType]
			   ,ServiceUnit
			   ,ServiceOrder)
			VALUES
			   ('ManualOffset'
			   ,@CustomerID
			   --,<PaymentMethod, char(2),>
			   --,<Coverage, char(2),>
			   ,@PartsTotal
			   ,@LaborTotal
			   ,@EntryAmount
			   --,<OriginalLedgerEntryID, int,>
			   --,<CurrentLedgerEntryID, int,>
			   ,@EnterpriseID 
			   ,GETDATE()
			   ,@OwnerType
			   ,@OwnerID
			   ,@ReasonID
			   ,@OffsetType
			   ,@ServiceUnit
			   ,@ServiceOrder)	
           
			SET @EntryID = scope_identity()
			
			INSERT INTO ConfidentialCustomer.dbo.TBL_CustomerLedger
				(EntryType
				,CustomerID
				,EntryAmount
				,EntryAmountParts
				,EntryAmountLabor
				,EntryAmountOther
				,EntryAmountTax
				,EntrySourceType
				,EntrySourceID
				,EntrySourceConfidence
				,EntrySourceDate
				,ServiceUnit
				,ServiceOrder
				,PurchaseOrder
				,EntrySourceCreatedBy
				--,BillType
				--,BillID
				,CreatedEnterpriseID
				,CreatedDateTime
				,LastModifiedEnterpriseID
				,LastModifiedDateTime
				)
			SELECT 
				OffsetType AS EntryType
				,CustomerID
				,EntryAmount
				,PartsTotal
				,LaborTotal
				,NULL AS EntryAmountOther
				,NULL AS EntryAmountTax
				,EntryType AS EntrySourceType
				,EntryID AS EntrySourceID
				,NULL AS EntrySourceConfidence
				,CreatedDateTime AS EntrySourceDate
				,ServiceUnit
				,ServiceOrder
				,NULL AS PurchaseOrder
				,CreatedEnterpriseID AS EntrySourceCreatedBy
				--,OwnerType AS BillType
				--,OwnerID AS BillID
				,@EnterpriseID AS CreatedEnterpriseID
				,GETDATE() AS CreatedDateTime
				,@EnterpriseID AS LastModifiedEnterpriseID
				,GETDATE() AS LastModifiedDateTime
			FROM 
				[ConfidentialCustomer].[dbo].[TBL_ManualOffset] 
			WHERE
				EntryID = @EntryID
				
			SET @LedgerEntryID = scope_identity()
			
			/* Update Current Ledger Entry ID in Manual Offset table*/
			UPDATE [ConfidentialCustomer].[dbo].[TBL_ManualOffset]
			SET OriginalLedgerEntryID = ISNULL(OriginalLedgerEntryID, @LedgerEntryID) 
				,CurrentLedgerEntryID = @LedgerEntryID 
			WHERE
				EntryID = @EntryID
				
				
			/*Reverse bad updates*/	
			--/* If Offset Type = 201 then update OffsetEntry ID for corresponding 100 Ledger Entry */
			--IF @OffsetType = 201 
	 	--	BEGIN
			--	UPDATE ConfidentialCustomer.dbo.TBL_CustomerLedger
			--	SET OffsetEntryID = @LedgerEntryID
			--	WHERE
			--		EntryID = @EntryID
			--END
		
		
		END		
GO
/****** Object:  StoredProcedure [dbo].[PROC_Requests_Insert_NA]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*************************************************************************
	Procedure Name: PROC_Requests_Insert
	Returns:		Failure Code (@ErrorCode)
	Primary User:	Everyone
	Designer:		Rob O'Brien
	Company:		Sears, Roebuck and Co
	Last Updated:	Fri Apr 17 16:26:28 CST 2018 @810 /Internet Time/
	Updated by:		Rob O'Brien
	Information:	Procedure for adding or updating a comment
	************************************************************************
	Version:		1.00
	Date:			Fri Apr 17 16:26:36 CST 2018 @810 /Internet Time/
	Updated By:		Rob O'Brien
	
	Version:		1.01
	Date:			Mon Oct 26 11:06:52 EST 2018 @810 /Internet Time/
	Updated By:		Brandi Fischer
	Notes:			Added pending request and amount minimum validators.
***************************************************************************/
CREATE Procedure [dbo].[PROC_Requests_Insert_NA]
	@EntryID int = Null OUTPUT, /* Unique ID number for the comment.  To update an existing comment, supply the comment ID.  Otherwise, an insert will be done. */
	@ActionID int = Null OUTPUT, /* Unique ID number for the comment.  To update an existing comment, supply the comment ID.  Otherwise, an insert will be done. */
	@RequestType varchar(30), /* Ownertype from the ownertype process managed for a database side applicaiton identification. */
	@ReasonID int, /* Ownertype from the ownertype process managed for a database side applicaiton identification. */
	@RequestPartsAmount decimal(18,4) = NULL, /* OwnerID number from the calling application. */
	@RequestLaborAmount decimal(18,4) = NULL, /* OwnerID number from the calling application. */
	@RequestAmount decimal(18,4) = NULL, /* OwnerID number from the calling application. */
	@CustomerID int = NULL, 
	@ServiceUnit char(7) = NULL,
	@ServiceOrder char(8) = NULL,
	@SecondaryType varchar(30) = NULL, /* Ownertype from the ownertype process managed for a database side applicaiton identification. */
	@SecondaryID int = NULL, /* OwnerID number from the calling application. */
	@StatusID int, /* StatusID number from the calling application. */
	@EnterpriseID varchar(8), /* Employee LDAP ID making the insert or update.  Upper case. */
	@ErrorCode varchar(150) = NULL OUTPUT /* error message if any during processing */ 
AS
	SET NOCOUNT ON

	
	SET @EnterpriseID = LTRIM(RTRIM(UPPER(@EnterpriseID)))
	SET @RequestType = LTRIM(RTRIM(@RequestType))
	
	DECLARE @RequestCount AS INT = (SELECT COUNT(EntryID) FROM ConfidentialCustomer.dbo.TBL_Requests WHERE ServiceUnit = @ServiceUnit AND ServiceOrder = @ServiceOrder AND RequestType = @RequestType AND StatusID IN (1,4))
	/*SELECT * FROM ConfidentialCustomer.dbo.TBL_LookupCodes WHERE OwnerType = 'Requests' AND OwnerID = 'StatusID' ORDER BY SortOrder*/

	IF @RequestType = '' /* set @OwnerType to null if empty string */
		SET @RequestType = NULL
	IF @EnterpriseID = '' /* set @EnterpriseID to null if empty string */
		SET @EnterpriseID = NULL
	IF @SecondaryID = 0 /* set @CommentID to null if zero */
		SET @SecondaryID = NULL
	IF @StatusID = 0 /* set @OwnerID to null if zero */
		SET @StatusID = NULL
	IF @SecondaryType = '' /* set @SecondaryType to null if empty string */
		SET @SecondaryType = NULL
		
	IF @RequestCount > 0 /* Return error code if @RequestCount is more than 0 */
	 	Begin
		SET @ErrorCode = 'Unable to submit request due to open pending request.'
		Return
	END

	IF @RequestType Is Null /* Return error code if @RequestType is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for Request Type, it is Required.'
		Return
	END
	IF @EnterpriseID Is Null /* Return error code if @EnterpriseID is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for EmpID, it is Required.'
		Return
	END
	IF @RequestAmount <=0
		Begin
		SET @ErrorCode = 'Please enter a request amount, it is Required.'
		Return
	END

	
	/* if a comment ID was passed in, update it. otherwise create a new row */
	IF @EntryID IS NULL /* add row */
	BEGIN
		INSERT INTO ConfidentialCustomer.dbo.TBL_Requests
			(	EntryType
				,RequestType
				,ReasonID
				,ActionID
				,ActionType
				,StatusID
				,RequestPartsAmount
				,RequestLaborAmount
				,RequestAmount
				,CustomerID
				,ServiceUnit
				,ServiceOrder
				,CreatedEnterpriseID
				,CreatedDateTime
				,LastModifiedEnterpriseID
				,LastModifiedDateTime
			)
		VALUES
			(	'Requests'
				,@RequestType
				,@ReasonID
				,0
				,'Actions'
				,0
				,@RequestPartsAmount
				,@RequestLaborAmount
				,@RequestAmount
				,@CustomerID
				,@ServiceUnit
				,@ServiceOrder
				,@EnterpriseID
				,GETDATE()
				,@EnterpriseID
				,GETDATE()
			)
		
		SET @EntryID = scope_identity()

		IF @ActionID IS NULL /* add row */
		BEGIN
			INSERT INTO ConfidentialCustomer.dbo.TBL_Actions
			(ActionType, OwnerType, OwnerID, SecondaryType, SecondaryID, StatusID, CreatedEnterpriseID, CreatedDateTime)
			VALUES('Actions', 'Requests', @EntryID, @SecondaryType, @SecondaryID, @StatusID, @EnterpriseID, GETDATE())
				
			SET @ActionID = scope_identity()
		
			UPDATE ConfidentialCustomer.dbo.TBL_Requests
			SET ActionID = @ActionID
				,StatusID = @StatusID
				,LastModifiedEnterpriseID = @EnterpriseID
				,LastModifiedDateTime = GETDATE()
			WHERE
				EntryID = @EntryID
		END	
	ELSE
		BEGIN
		INSERT INTO ConfidentialCustomer.dbo.TBL_Actions
			(ActionType, OwnerType, OwnerID, SecondaryType, SecondaryID, StatusID, CreatedEnterpriseID, CreatedDateTime)
			VALUES('Actions', 'Requests', @EntryID, @SecondaryType, @SecondaryID, @StatusID, @EnterpriseID, GETDATE())
				
			SET @ActionID = scope_identity()

		UPDATE ConfidentialCustomer.dbo.TBL_Requests
		SET ActionID = @ActionID
			,StatusID = @StatusID
			,RequestPartsAmount = @RequestPartsAmount
			,RequestLaborAmount = @RequestLaborAmount
			,RequestAmount = @RequestAmount
			,LastModifiedEnterpriseID = @EnterpriseID
			,LastModifiedDateTime = GETDATE()
		WHERE
			EntryID = @EntryID
		END
	END
GO
/****** Object:  Table [dbo].[BILL_wty_PRINT]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BILL_wty_PRINT](
	[ReqID] [int] NOT NULL,
	[SVC_UN_NO] [nvarchar](50) NULL,
	[SO_NO] [nvarchar](50) NULL,
	[RUN_DATE] [date] NULL,
	[PARTS_DUE] [decimal](10, 2) NULL,
	[LABOR_DUE] [decimal](10, 2) NULL,
	[TAX_DUE] [decimal](10, 2) NULL,
	[TTL_DUE] [decimal](12, 2) NULL,
	[SOURCE] [nvarchar](50) NULL,
	[ETL_DATE] [date] NULL,
	[ITEM_DSC] [nvarchar](50) NULL,
	[DUNS_1] [date] NULL,
	[DUNS_2] [date] NULL,
	[DUNS_3] [date] NULL,
	[DUNS_COLLECT] [date] NULL,
	[DUNS_STAGE] [int] NULL,
	[C_NAME] [nvarchar](50) NULL,
	[C_STREET] [nvarchar](100) NULL,
	[C_CITY] [nvarchar](302) NULL,
	[C_STATE] [varchar](41) NULL,
	[C_ZIP] [decimal](10, 2) NULL,
	[C_PHONE] [nvarchar](50) NULL,
	[XMIT_COLLECTOIN_ON] [date] NULL,
	[SVC_ORG] [varchar](19) NULL,
	[MODEL] [nvarchar](50) NULL,
	[SERIAL NUMBER] [nvarchar](50) NULL,
	[CLAIM_NO] [varchar](87) NULL,
	[Reference] [nvarchar](50) NULL,
	[IMG_DATA] [image] NULL,
	[FD_IMG] [image] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CreditChargebacks]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CreditChargebacks](
	[ServiceUnit] [varchar](50) NULL,
	[AccountType] [varchar](50) NULL,
	[Amount] [varchar](50) NULL,
	[TransDate] [varchar](50) NULL,
	[Salecheck] [varchar](50) NULL,
	[Reason] [varchar](50) NULL,
	[ReasonForLoss] [varchar](50) NULL,
	[ServiceOrder] [varchar](50) NULL,
	[WeekEndingDate] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EssbaseAccounts]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EssbaseAccounts](
	[Acct Per] [varchar](50) NULL,
	[Acct Wk] [varchar](50) NULL,
	[Region] [varchar](50) NULL,
	[District] [varchar](50) NULL,
	[Unit] [varchar](50) NULL,
	[Locn] [varchar](50) NULL,
	[Locn Description] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](50) NULL,
	[Acct] [varchar](50) NULL,
	[Acct Description] [varchar](50) NULL,
	[GL Description] [varchar](50) NULL,
	[Div] [varchar](50) NULL,
	[Cost] [varchar](50) NULL,
	[Sell Value] [varchar](50) NULL,
	[Stat Amt] [varchar](50) NULL,
	[MU %] [varchar](50) NULL,
	[Entry Src] [varchar](50) NULL,
	[Entry Src Description] [varchar](50) NULL,
	[Atm Code] [varchar](50) NULL,
	[Opid] [varchar](50) NULL,
	[Approver Id] [varchar](50) NULL,
	[Jvnum] [varchar](50) NULL,
	[Doc Nbr] [varchar](50) NULL,
	[Misc 1] [varchar](50) NULL,
	[Misc 2] [varchar](50) NULL,
	[Misc 3] [varchar](50) NULL,
	[Journal Date] [varchar](50) NULL,
	[Processing Date] [varchar](50) NULL,
	[Detail Tran Date] [varchar](50) NULL,
	[Refer Nbr 1] [varchar](50) NULL,
	[Refer Nbr 2] [varchar](50) NULL,
	[To From] [varchar](50) NULL,
	[Timestamp File] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EVA_SHORTAGE_QUEUE]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EVA_SHORTAGE_QUEUE](
	[FRID] [int] NOT NULL,
	[RGN_NO] [nvarchar](50) NULL,
	[SVC_UN_NO] [nvarchar](50) NULL,
	[SO_NO] [nvarchar](50) NULL,
	[SVC_CAL_DT] [date] NULL,
	[NPS_TECH_ID] [nvarchar](50) NULL,
	[TOTAL_DUE] [decimal](10, 2) NULL,
	[LOAD_DATE] [date] NULL,
	[TECH_RACF] [nvarchar](50) NULL,
	[TECH_NAME] [nvarchar](50) NULL,
	[NAME_3] [nvarchar](50) NULL,
	[NAME_2] [nvarchar](50) NULL,
	[NAME_1] [nvarchar](50) NULL,
	[CLOSE_USER] [nvarchar](50) NULL,
	[CLOSE_DATE] [date] NULL,
	[REASON] [nvarchar](50) NULL,
	[DO_NOT_BILL] [nvarchar](50) NULL,
	[NOTES] [nvarchar](70) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EVA_tbl_PAYMENT_MASTER]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EVA_tbl_PAYMENT_MASTER](
	[PMT_ID] [int] NOT NULL,
	[CREDIT_DEBT] [nvarchar](50) NULL,
	[PAYMENT_TYPE] [nvarchar](50) NULL,
	[PAYMENT_MTH] [nvarchar](50) NULL,
	[AMT] [decimal](10, 2) NULL,
	[DIST_NO] [nvarchar](50) NULL,
	[SO_NO] [nvarchar](50) NULL,
	[PMT_DATE] [date] NULL,
	[PMT_SOURCE] [nvarchar](50) NULL,
	[ETL_DATE] [date] NULL,
	[PMID_1] [nvarchar](50) NULL,
	[PMID_2] [nvarchar](50) NULL,
	[PMID_3] [nvarchar](50) NULL,
	[PMID_4] [nvarchar](50) NULL,
	[TKN] [nvarchar](50) NULL,
	[EXP_DATE] [nvarchar](50) NULL,
	[NOTE] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EVA_tbl_PriceOverride]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EVA_tbl_PriceOverride](
	[OID] [int] NOT NULL,
	[SVC_UN_NO] [nvarchar](50) NULL,
	[SO_NO] [nvarchar](50) NULL,
	[SVC_CAL_DT] [nvarchar](50) NULL,
	[SET_AMT] [numeric](10, 2) NULL,
	[REQ_USER] [nvarchar](50) NULL,
	[REQ_DATE] [nvarchar](50) NULL,
	[REASON] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EVA_tbl_RECON_MASTER]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EVA_tbl_RECON_MASTER](
	[RID] [int] NOT NULL,
	[RGN_NO] [nvarchar](50) NULL,
	[SVC_UN_NO] [nvarchar](50) NULL,
	[SO_NO] [nvarchar](50) NULL,
	[SVC_CAL_DT] [date] NULL,
	[FISCAL_YEAR] [int] NULL,
	[FISCAL_QTR] [int] NULL,
	[FISCAL_MTH] [int] NULL,
	[FISCAL_WK] [int] NULL,
	[NPS_TECH_ID] [nvarchar](50) NULL,
	[TOTAL_DUE] [decimal](10, 2) NULL,
	[LAST_BAL_UPDATE] [date] NULL,
	[STAT] [nvarchar](50) NULL,
	[ETL_DATE] [date] NULL,
	[ENT_ID] [nvarchar](50) NULL,
	[TECH_NAME] [nvarchar](50) NULL,
	[PROC_ID] [nvarchar](50) NULL,
	[CLOSE_CODE] [nvarchar](50) NULL,
	[ITEM_DSC] [nvarchar](50) NULL,
	[DUNS_1] [date] NULL,
	[DUNS_2] [date] NULL,
	[DUNS_3] [date] NULL,
	[DUNS_COLLECT] [date] NULL,
	[DUNS_STAGE] [int] NULL,
	[ALLOW_REFUND] [nvarchar](50) NULL,
	[NPS_CUST_ID] [nvarchar](50) NULL,
	[C_NAME] [nvarchar](50) NULL,
	[C_STREET] [nvarchar](100) NULL,
	[C_CITYSTATEZIP] [nvarchar](100) NULL,
	[C_PHONE] [nvarchar](50) NULL,
	[XMIT_COLLECTOIN_ON] [date] NULL,
	[SVC_ORG] [nvarchar](50) NULL,
	[FLAGS] [nvarchar](300) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EVA_tbl_REFUND_MASTER]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EVA_tbl_REFUND_MASTER](
	[REF_ID] [int] IDENTITY(1,1) NOT NULL,
	[SVC_UN_NO] [nvarchar](50) NULL,
	[SO_NO] [nvarchar](50) NULL,
	[REQ_DATE] [date] NULL,
	[REQ_USER] [nvarchar](50) NULL,
	[REQ_AMT] [decimal](10, 2) NULL,
	[PAID_AMT] [decimal](10, 2) NULL,
	[REQ_REASON] [nvarchar](50) NULL,
	[REFUND_METHOD] [nvarchar](50) NULL,
	[CHK_NM] [nvarchar](50) NULL,
	[CHK_STREET_ADDR] [nvarchar](50) NULL,
	[CHK_CITY] [nvarchar](50) NULL,
	[CHK_STATE] [nvarchar](50) NULL,
	[CHK_ZIP] [nvarchar](50) NULL,
	[REFUND_STATUS] [nchar](10) NULL,
	[APPRV_USER] [nvarchar](50) NULL,
	[APPRV_DATE] [date] NULL,
	[COE_USER] [nvarchar](50) NULL,
	[COE_DATE] [date] NULL,
	[COE_REG] [nvarchar](50) NULL,
	[COE_TRANS] [nvarchar](50) NULL,
 CONSTRAINT [PK_EVA_tbl_REFUND_MASTER] PRIMARY KEY CLUSTERED 
(
	[REF_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EVA_TBL_REFUND_NOTES]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EVA_TBL_REFUND_NOTES](
	[NOTEID] [int] NOT NULL,
	[DIST] [nvarchar](50) NULL,
	[SO] [nvarchar](50) NULL,
	[NOTE_DATE] [date] NULL,
	[USER_NM] [nvarchar](50) NULL,
	[NOTE_FLD] [varbinary](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EVA_tbl_WRNTY_BILL]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EVA_tbl_WRNTY_BILL](
	[ReqID] [int] NOT NULL,
	[SVC_UN_NO] [nvarchar](50) NULL,
	[SO_NO] [nvarchar](50) NULL,
	[RUN_DATE] [date] NULL,
	[PARTS_DUE] [numeric](10, 2) NULL,
	[LABOR_DUE] [numeric](10, 2) NULL,
	[TAX_DUE] [numeric](10, 2) NULL,
	[SOURCE] [nvarchar](50) NULL,
	[ETL_DATE] [date] NULL,
	[ITEM_DSC] [nvarchar](50) NULL,
	[DUNS_1] [date] NULL,
	[DUNS_2] [date] NULL,
	[DUNS_3] [date] NULL,
	[DUNS_COLLECT] [date] NULL,
	[DUNS_STAGE] [int] NULL,
	[C_NAME] [nvarchar](50) NULL,
	[C_STREET] [nvarchar](100) NULL,
	[C_CITY] [nvarchar](100) NULL,
	[C_STATE] [nvarchar](100) NULL,
	[C_ZIP] [nvarchar](100) NULL,
	[C_PHONE] [nvarchar](50) NULL,
	[XMIT_COLLECTOIN_ON] [date] NULL,
	[SVC_ORG] [nvarchar](50) NULL,
	[MODEL] [nvarchar](50) NULL,
	[SERIAL NUMBER] [nvarchar](50) NULL,
	[CLAIM_NO] [nvarchar](50) NULL,
	[Reference] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HLD_TempusCard]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HLD_TempusCard](
	[EntryType] [varchar](30) NOT NULL,
	[SystemCode] [bigint] NOT NULL,
	[Storecode] [int] NULL,
	[authdate] [datetime] NULL,
	[saletype] [varchar](20) NULL,
	[tranamt] [decimal](15, 4) NULL,
	[authcode] [char](6) NULL,
	[nonauthresp] [varchar](20) NULL,
	[batchdate] [date] NULL,
	[batchid] [bigint] NULL,
	[cardtype] [varchar](20) NULL,
	[cardacct] [varchar](20) NULL,
	[TranEntryMode] [varchar](50) NULL,
	[ProcessorToken] [varchar](20) NULL,
	[TokenType] [varchar](20) NULL,
	[TokenProviderID] [int] NULL,
	[tranident] [varchar](25) NULL,
	[CustIdent] [varchar](25) NULL,
	[stationident] [varchar](25) NULL,
	[storesystemcode] [int] NULL,
	[name] [varchar](50) NULL,
	[city] [varchar](50) NULL,
	[state] [varchar](2) NULL,
	[zip] [varchar](5) NULL,
	[ChainCode] [varchar](50) NULL,
	[TranEmployeeIdent] [varchar](25) NULL,
	[MID] [bigint] NULL,
	[CCServiceName] [varchar](50) NULL,
	[NAIUnit] [varchar](7) NULL,
	[ServiceUnit] [char](7) NULL,
	[ServiceOrder] [char](8) NULL,
	[TechnicianID] [char](7) NULL,
	[CustomerID] [int] NULL,
	[CustomerIDUpdateSource] [varchar](30) NULL,
	[CustomerIDUpdateSourceID] [int] NULL,
	[OriginalLedgerEntryID] [int] NULL,
	[CurrentLedgerEntryID] [int] NULL,
	[CreatedEnterpriseID] [varchar](8) NOT NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
	[LastModifiedEnterpriseID] [varchar](8) NULL,
	[LastModifiedDateTime] [datetime2](7) NULL,
	[TechHubTechnician] [int] NULL,
	[CustomerIDUpdateConfidence] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HLD_TempusCards]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HLD_TempusCards](
	[EntryType] [varchar](30) NOT NULL,
	[SystemCode] [bigint] IDENTITY(-1,-1) NOT NULL,
	[Storecode] [int] NULL,
	[authdate] [datetime] NULL,
	[saletype] [varchar](20) NULL,
	[tranamt] [decimal](15, 4) NULL,
	[authcode] [char](6) NULL,
	[nonauthresp] [varchar](20) NULL,
	[batchdate] [date] NULL,
	[batchid] [bigint] NULL,
	[cardtype] [varchar](20) NULL,
	[cardacct] [varchar](20) NULL,
	[TranEntryMode] [varchar](50) NULL,
	[ProcessorToken] [varchar](20) NULL,
	[TokenType] [varchar](20) NULL,
	[TokenProviderID] [int] NULL,
	[tranident] [varchar](25) NULL,
	[CustIdent] [varchar](25) NULL,
	[stationident] [varchar](25) NULL,
	[storesystemcode] [int] NULL,
	[name] [varchar](50) NULL,
	[city] [varchar](50) NULL,
	[state] [varchar](2) NULL,
	[zip] [varchar](5) NULL,
	[ChainCode] [varchar](50) NULL,
	[TranEmployeeIdent] [varchar](25) NULL,
	[MID] [bigint] NULL,
	[CCServiceName] [varchar](50) NULL,
	[NAIUnit] [varchar](7) NULL,
	[ServiceUnit] [char](7) NULL,
	[ServiceOrder] [char](8) NULL,
	[TechnicianID] [char](7) NULL,
	[CustomerID] [int] NULL,
	[CustomerIDUpdateSource] [varchar](30) NULL,
	[CustomerIDUpdateSourceID] [int] NULL,
	[OriginalLedgerEntryID] [int] NULL,
	[CurrentLedgerEntryID] [int] NULL,
	[CreatedEnterpriseID] [varchar](8) NOT NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
	[LastModifiedEnterpriseID] [varchar](8) NULL,
	[LastModifiedDateTime] [datetime2](7) NULL,
	[TechHubTechnician] [int] NULL,
	[CustomerIDUpdateConfidence] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PendingOrders]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PendingOrders](
	[SVC_UN_NO] [varchar](7) NOT NULL,
	[SO_NO] [varchar](8) NOT NULL,
	[SO_PY_MET_CD] [varchar](2) NULL,
	[SVC_SCH_DT] [date] NULL,
	[SVC_SCH_FRM_TM] [varchar](8) NULL,
	[SVC_SCH_TO_TM] [varchar](8) NULL,
	[SVC_ORI_SCH_DT] [date] NULL,
	[SVC_ORI_SCH_FRM_TM] [varchar](8) NULL,
	[SVC_ORI_SCH_TO_TM] [varchar](8) NULL,
	[SO_CRT_DT] [date] NULL,
	[SO_CRT_TM] [time](0) NULL,
	[SO_CRT_UN_NO] [varchar](7) NULL,
	[SO_CRT_EMP_NO] [varchar](7) NULL,
	[SO_STS_CD] [varchar](2) NULL,
	[SO_TYP_CD] [varchar](4) NULL,
	[SO_PRI_CD] [varchar](1) NULL,
	[SO_HPR_NED_FL] [varchar](1) NULL,
	[MOD_EXT_DT] [datetime2](7) NULL,
	[MOD_ID] [varchar](7) NULL,
	[MDS_SP_CD] [varchar](10) NULL,
	[LST_ACT_CD] [varchar](1) NULL,
	[LST_ACT_TS] [datetime2](7) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[POSII_PO_PROCESSED]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[POSII_PO_PROCESSED](
	[PO_NO] [varchar](50) NULL,
	[SVC_UN_NO] [varchar](52) NULL,
	[POS_REG] [varchar](50) NULL,
	[TRANS_NO] [varchar](50) NULL,
	[AMT] [money] NULL,
	[TRAN_DATE] [date] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RNIDs]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RNIDs](
	[RNID] [varchar](50) NULL,
	[StoreName] [varchar](50) NULL,
	[UnitNumber] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[STG_TempusCard]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[STG_TempusCard](
	[EntryType] [varchar](30) NOT NULL,
	[SystemCode] [bigint] IDENTITY(-1,-1) NOT NULL,
	[Storecode] [int] NULL,
	[authdate] [datetime] NULL,
	[saletype] [varchar](20) NULL,
	[tranamt] [decimal](15, 4) NULL,
	[authcode] [char](6) NULL,
	[nonauthresp] [varchar](20) NULL,
	[batchdate] [date] NULL,
	[batchid] [bigint] NULL,
	[cardtype] [varchar](20) NULL,
	[cardacct] [varchar](20) NULL,
	[TranEntryMode] [varchar](50) NULL,
	[ProcessorToken] [varchar](20) NULL,
	[TokenType] [varchar](20) NULL,
	[TokenProviderID] [int] NULL,
	[tranident] [varchar](25) NULL,
	[CustIdent] [varchar](25) NULL,
	[stationident] [varchar](25) NULL,
	[storesystemcode] [int] NULL,
	[name] [varchar](50) NULL,
	[city] [varchar](50) NULL,
	[state] [varchar](2) NULL,
	[zip] [varchar](5) NULL,
	[ChainCode] [varchar](50) NULL,
	[TranEmployeeIdent] [varchar](25) NULL,
	[MID] [bigint] NULL,
	[CCServiceName] [varchar](50) NULL,
	[NAIUnit] [varchar](7) NULL,
	[ServiceUnit] [char](7) NULL,
	[ServiceOrder] [char](8) NULL,
	[TechnicianID] [char](7) NULL,
	[CustomerID] [int] NULL,
	[CustomerIDUpdateSource] [varchar](30) NULL,
	[CustomerIDUpdateSourceID] [int] NULL,
	[OriginalLedgerEntryID] [int] NULL,
	[CurrentLedgerEntryID] [int] NULL,
	[CreatedEnterpriseID] [varchar](8) NOT NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
	[LastModifiedEnterpriseID] [varchar](8) NULL,
	[LastModifiedDateTime] [datetime2](7) NULL,
	[TechHubTechnician] [int] NULL,
	[CustomerIDUpdateConfidence] [int] NULL,
 CONSTRAINT [PK_STG_TempusCard] PRIMARY KEY CLUSTERED 
(
	[SystemCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_Actions]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Actions](
	[ActionID] [int] IDENTITY(1,1) NOT NULL,
	[ActionType] [varchar](30) NOT NULL,
	[OwnerID] [int] NOT NULL,
	[OwnerType] [varchar](30) NOT NULL,
	[SecondaryID] [int] NULL,
	[SecondaryType] [varchar](30) NULL,
	[StatusID] [int] NOT NULL,
	[CreatedEnterpriseID] [varchar](8) NOT NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_TBL_Actions_V2] PRIMARY KEY CLUSTERED 
(
	[ActionID] ASC,
	[ActionType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_BillType]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_BillType](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[EntryType] [varchar](30) NOT NULL,
	[ClientName] [varchar](50) NOT NULL,
	[ContactDisplayName] [varchar](50) NULL,
	[ContactStreetAddress] [varchar](50) NULL,
	[ContactCity] [varchar](50) NULL,
	[ContactState] [char](2) NULL,
	[ContactZipCode] [varchar](10) NULL,
	[ContactPhoneNumber] [char](12) NULL,
	[ContactURL] [varchar](255) NULL,
	[ContactEmail] [varchar](50) NULL,
	[ClientSupportEnterpriseID] [varchar](8) NULL,
	[CreatedEnterpriseID] [varchar](8) NOT NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
	[LastModifiedEnterpriseID] [varchar](8) NOT NULL,
	[LastModifiedDateTime] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_TBL_BillType] PRIMARY KEY CLUSTERED 
(
	[EntryID] ASC,
	[EntryType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_Coaching]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Coaching](
	[CoachingID] [int] IDENTITY(1,1) NOT NULL,
	[EntryType] [varchar](30) NOT NULL,
	[AssignedEnterpriseID] [varchar](8) NOT NULL,
	[StatusID] [int] NOT NULL,
	[ServiceUnit] [char](7) NULL,
	[ServiceOrder] [char](8) NULL,
	[CustomerID] [int] NULL,
	[OwnerType] [varchar](30) NOT NULL,
	[OwnerID] [int] NOT NULL,
	[SecondaryType] [varchar](30) NULL,
	[SecondaryID] [int] NULL,
	[CreatedEnterpriseID] [varchar](8) NOT NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
	[Comment] [varchar](max) NULL,
 CONSTRAINT [PK_TBL_Coaching_V2] PRIMARY KEY CLUSTERED 
(
	[CoachingID] ASC,
	[EntryType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_CommentLog]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_CommentLog](
	[ChangeID] [int] IDENTITY(1,1) NOT NULL,
	[CommentID] [int] NOT NULL,
	[OriginalDate] [datetime2](7) NOT NULL,
	[UpdatedDate] [datetime2](7) NOT NULL,
	[OriginalText] [text] NOT NULL,
	[OriginalUserID] [varchar](10) NOT NULL,
	[UpdatedByID] [varchar](10) NOT NULL,
 CONSTRAINT [PK_TBL_CommentLog] PRIMARY KEY NONCLUSTERED 
(
	[ChangeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_Comments]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Comments](
	[CommentID] [int] IDENTITY(1,1) NOT NULL,
	[OwnerType] [varchar](30) NULL,
	[OwnerID] [int] NULL,
	[SecondaryType] [varchar](30) NULL,
	[SecondaryID] [int] NULL,
	[IsPrivate] [bit] NOT NULL,
	[Comment] [varchar](max) NOT NULL,
	[ServiceUnit] [char](7) NULL,
	[ServiceOrder] [char](8) NULL,
	[CustomerID] [int] NULL,
	[CreatedEnterpriseID] [varchar](8) NOT NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
	[LastModifiedEnterpriseID] [varchar](8) NULL,
	[LastModifiedDateTime] [datetime2](7) NULL,
 CONSTRAINT [PK_TBL_Comments] PRIMARY KEY CLUSTERED 
(
	[CommentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_Communications]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Communications](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[EntryType] [varchar](30) NOT NULL,
	[OwnerID] [int] NOT NULL,
	[OwnerType] [varchar](30) NOT NULL,
	[CustomerID] [int] NULL,
	[CommunicationType] [int] NOT NULL,
	[CommunicationMethod] [int] NULL,
	[SentDateTime] [datetime2](7) NULL,
	[CreatedEnterpriseID] [varchar](8) NOT NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_TBL_Communications2] PRIMARY KEY CLUSTERED 
(
	[EntryID] ASC,
	[EntryType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_CommunicationType]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_CommunicationType](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[EntryType] [varchar](30) NOT NULL,
	[CommunicationTypeName] [varchar](30) NOT NULL,
	[CommunicationHTML] [nvarchar](max) NOT NULL,
	[CreatedEnterpriseID] [varchar](8) NOT NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
	[LastModifiedEnterpriseID] [varchar](8) NULL,
	[LastModifiedDateTime] [datetime2](7) NULL,
 CONSTRAINT [PK_TBL_CommunicationType] PRIMARY KEY CLUSTERED 
(
	[EntryID] ASC,
	[EntryType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_CreditChargebacks]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_CreditChargebacks](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[EntryType] [varchar](30) NOT NULL,
	[Unit] [char](7) NOT NULL,
	[AccountType] [varchar](30) NULL,
	[EntryAmount] [decimal](15, 4) NULL,
	[TransDate] [date] NULL,
	[Salecheck] [varchar](30) NULL,
	[Reason] [varchar](50) NULL,
	[ReasonForLoss] [varchar](50) NULL,
	[OrderNumber] [varchar](50) NULL,
	[WeekEndingDate] [varchar](50) NULL,
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NULL,
	[PurchaseOrder] [char](8) NULL,
	[CustomerID] [int] NULL,
	[CustomerIDUpdateSource] [varchar](30) NULL,
	[CustomerIDUpdateSourceID] [int] NULL,
	[OriginalLedgerEntryID] [int] NULL,
	[CurrentLedgerEntryID] [int] NULL,
	[CreatedEnterpriseID] [varchar](8) NOT NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
	[LastModifiedEnterpriseID] [varchar](8) NULL,
	[LastModifiedDateTime] [datetime2](7) NULL,
 CONSTRAINT [PK_TBL_CreditChargebacks] PRIMARY KEY CLUSTERED 
(
	[EntryID] ASC,
	[EntryType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_CustomerBills]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_CustomerBills](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[EntryType] [varchar](30) NOT NULL,
	[ActionID] [int] NULL,
	[ActionType] [varchar](30) NULL,
	[StatusID] [int] NULL,
	[ReasonID] [int] NULL,
	[CustomerID] [int] NOT NULL,
	[OriginalBalance] [decimal](18, 4) NOT NULL,
	[CurrentBalance] [decimal](18, 4) NOT NULL,
	[PaidInFull] [bit] NOT NULL,
	[ServiceType] [int] NOT NULL,
	[BillType] [int] NOT NULL,
	[ClaimNumber] [varchar](50) NULL,
	[PaymentCount] [int] NOT NULL,
	[InvoiceNumber] [bigint] NOT NULL,
	[InstallationCount] [int] NOT NULL,
	[LastBillDate] [date] NULL,
	[LastPaymentDate] [date] NULL,
	[CreatedEnterpriseID] [varchar](8) NOT NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
	[LastModifiedEnterpriseID] [varchar](8) NULL,
	[LastModifiedDateTime] [datetime2](7) NULL,
	[AdjustmentAmount] [decimal](18, 6) NULL,
	[PaymentAmount] [decimal](18, 6) NULL,
	[LedgerEntryID] [int] NULL,
 CONSTRAINT [PK_TBL_CustomerBills] PRIMARY KEY CLUSTERED 
(
	[EntryID] ASC,
	[EntryType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_CustomerBillsEntryDetail]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_CustomerBillsEntryDetail](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[EntryType] [varchar](30) NOT NULL,
	[EntrySourceType] [varchar](30) NOT NULL,
	[EntrySourceID] [int] NOT NULL,
	[EntrySourceDate] [datetime2](7) NOT NULL,
	[LedgerEntrySourceType] [varchar](30) NOT NULL,
	[LedgerEntrySourceID] [int] NOT NULL,
	[EntryAmount] [decimal](18, 4) NOT NULL,
	[AdjustmentAmount] [decimal](18, 6) NULL,
	[PaymentAmount] [decimal](18, 6) NULL,
	[BillType] [int] NULL,
	[BillID] [int] NULL,
	[StatusID] [int] NULL,
	[ReasonID] [int] NULL,
	[CustomerID] [int] NOT NULL,
	[ServiceUnit] [char](7) NULL,
	[ServiceOrder] [char](8) NULL,
	[ClaimNumber] [varchar](50) NULL,
	[CreatedEnterpriseID] [varchar](8) NOT NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
	[LastModifiedEnterpriseID] [varchar](8) NULL,
	[LastModifiedDateTime] [datetime2](7) NULL,
 CONSTRAINT [PK_TBL_CustomerBillsEntryDetail] PRIMARY KEY CLUSTERED 
(
	[EntryID] ASC,
	[EntryType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_CustomerBillsNotSent]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_CustomerBillsNotSent](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[EntryType] [varchar](30) NOT NULL,
	[ActionID] [int] NULL,
	[ActionType] [varchar](30) NULL,
	[StatusID] [int] NULL,
	[ReasonID] [int] NULL,
	[CustomerID] [int] NOT NULL,
	[OriginalBalance] [decimal](18, 4) NOT NULL,
	[CurrentBalance] [decimal](18, 4) NOT NULL,
	[PaidInFull] [bit] NOT NULL,
	[ServiceType] [int] NOT NULL,
	[BillType] [int] NOT NULL,
	[ClaimNumber] [varchar](50) NULL,
	[PaymentCount] [int] NOT NULL,
	[InvoiceNumber] [int] NOT NULL,
	[InstallationCount] [int] NOT NULL,
	[LastBillDate] [date] NULL,
	[LastPaymentDate] [date] NULL,
	[CreatedEnterpriseID] [varchar](8) NOT NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
	[LastModifiedEnterpriseID] [varchar](8) NULL,
	[LastModifiedDateTime] [datetime2](7) NULL,
	[AdjustmentAmount] [decimal](18, 6) NULL,
	[PaymentAmount] [decimal](18, 6) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_CustomerContactOverride]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_CustomerContactOverride](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[EntryType] [varchar](30) NOT NULL,
	[OwnerID] [int] NOT NULL,
	[OwnerType] [varchar](30) NOT NULL,
	[PreferenceID] [int] NOT NULL,
	[CustomerID] [int] NULL,
	[CustomerName] [varchar](50) NULL,
	[CustomerEmail] [varchar](250) NULL,
	[CustomerPhone] [varchar](50) NULL,
	[CustomerAltPhone] [varchar](50) NULL,
	[CustomerStreetAddress] [varchar](50) NULL,
	[CustomerCity] [varchar](50) NULL,
	[CustomerState] [varchar](50) NULL,
	[CustomerZip] [varchar](50) NULL,
	[CreatedEnterpriseID] [varchar](8) NOT NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_TBL_CustomerContactOverride] PRIMARY KEY CLUSTERED 
(
	[EntryID] ASC,
	[EntryType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_CustomerDoNotCall]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_CustomerDoNotCall](
	[CustomerPhone] [char](10) NOT NULL,
	[DoNotPhoneIndicator] [char](1) NOT NULL,
	[CreateDateTime] [datetime] NOT NULL,
	[ModifiedDateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_TBL_CustomerDoNotCall] PRIMARY KEY CLUSTERED 
(
	[CustomerPhone] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_CustomerLedger]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_CustomerLedger](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[EntryType] [int] NOT NULL,
	[CustomerID] [int] NOT NULL,
	[EntryAmount] [decimal](18, 4) NOT NULL,
	[EntryAmountParts] [decimal](18, 4) NULL,
	[EntryAmountLabor] [decimal](18, 4) NULL,
	[EntryAmountOther] [decimal](18, 4) NULL,
	[EntryAmountTax] [decimal](18, 4) NULL,
	[EntrySourceType] [varchar](30) NOT NULL,
	[EntrySourceID] [int] NULL,
	[EntrySourceConfidence] [int] NULL,
	[EntrySourceDate] [datetime2](0) NULL,
	[ServiceUnit] [char](7) NULL,
	[ServiceOrder] [char](8) NULL,
	[PurchaseOrder] [varchar](10) NULL,
	[EntrySourceCreatedBy] [varchar](10) NULL,
	[OffsetEntryID] [int] NULL,
	[BillType] [varchar](30) NULL,
	[BillID] [int] NULL,
	[CreatedEnterpriseID] [varchar](8) NOT NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
	[LastModifiedEnterpriseID] [varchar](8) NULL,
	[LastModifiedDateTime] [datetime2](7) NULL,
 CONSTRAINT [PK_CustomerLedger] PRIMARY KEY CLUSTERED 
(
	[EntryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_CustomerLedgerBillID]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_CustomerLedgerBillID](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[EntryType] [int] NOT NULL,
	[CustomerID] [int] NOT NULL,
	[EntryAmount] [decimal](18, 4) NOT NULL,
	[EntryAmountParts] [decimal](18, 4) NULL,
	[EntryAmountLabor] [decimal](18, 4) NULL,
	[EntryAmountOther] [decimal](18, 4) NULL,
	[EntryAmountTax] [decimal](18, 4) NULL,
	[EntrySourceType] [varchar](30) NOT NULL,
	[EntrySourceID] [int] NULL,
	[EntrySourceConfidence] [int] NULL,
	[EntrySourceDate] [datetime2](0) NULL,
	[ServiceUnit] [char](7) NULL,
	[ServiceOrder] [char](8) NULL,
	[PurchaseOrder] [varchar](10) NULL,
	[EntrySourceCreatedBy] [varchar](10) NULL,
	[OffsetEntryID] [int] NULL,
	[BillType] [varchar](30) NULL,
	[BillID] [int] NULL,
	[CreatedEnterpriseID] [varchar](8) NOT NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
	[LastModifiedEnterpriseID] [varchar](8) NULL,
	[LastModifiedDateTime] [datetime2](7) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_CustomerLedgerOffsets]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_CustomerLedgerOffsets](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[EntryType] [varchar](30) NOT NULL,
	[LedgerEntryType] [int] NULL,
	[LedgerEntryID] [int] NULL,
	[LedgerEntrySourceType] [varchar](30) NULL,
	[LedgerEntrySourceID] [int] NULL,
	[LedgerEntrySourceConfidence] [int] NULL,
	[LedgerEntrySourceCreatedBy] [varchar](10) NULL,
	[LedgerEntryAmount] [decimal](18, 4) NULL,
	[LedgerEntrySourceDate] [datetime2](0) NULL,
	[LedgerOffsetEntryType] [int] NULL,
	[LedgerOffsetEntryID] [int] NULL,
	[LedgerOffsetEntrySourceType] [varchar](30) NULL,
	[LedgerOffsetEntrySourceID] [int] NULL,
	[LedgerOffsetEntrySourceConfidence] [int] NULL,
	[LedgerOffsetEntrySourceCreatedBy] [varchar](10) NULL,
	[LedgerOffsetAmount] [decimal](18, 4) NULL,
	[LedgerOffsetEntrySourceDate] [datetime2](0) NULL,
	[CustomerID] [int] NOT NULL,
	[LedgerServiceUnit] [char](7) NULL,
	[LedgerServiceOrder] [char](8) NULL,
	[LedgerOffsetServiceUnit] [char](7) NULL,
	[LedgerOffsetServiceOrder] [char](8) NULL,
	[BillType] [varchar](30) NULL,
	[BillID] [int] NULL,
	[EntryStatusID] [int] NULL,
	[CreatedEnterpriseID] [varchar](8) NOT NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
	[LastModifiedEnterpriseID] [varchar](8) NULL,
	[LastModifiedDateTime] [datetime2](7) NULL,
 CONSTRAINT [PK_CustomerLedgerOffsets] PRIMARY KEY CLUSTERED 
(
	[EntryID] ASC,
	[EntryType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_CustomerLedgerOffsetsHold]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_CustomerLedgerOffsetsHold](
	[EntryID] [int] NOT NULL,
	[EntryType] [varchar](30) NOT NULL,
	[LedgerEntryType] [int] NULL,
	[LedgerEntryID] [int] NULL,
	[LedgerEntrySourceType] [varchar](30) NULL,
	[LedgerEntrySourceID] [int] NULL,
	[LedgerEntrySourceConfidence] [int] NULL,
	[LedgerEntrySourceCreatedBy] [varchar](10) NULL,
	[LedgerEntryAmount] [decimal](18, 4) NULL,
	[LedgerEntrySourceDate] [datetime2](0) NULL,
	[LedgerOffsetEntryType] [int] NULL,
	[LedgerOffsetEntryID] [int] NULL,
	[LedgerOffsetEntrySourceType] [varchar](30) NULL,
	[LedgerOffsetEntrySourceID] [int] NULL,
	[LedgerOffsetEntrySourceConfidence] [int] NULL,
	[LedgerOffsetEntrySourceCreatedBy] [varchar](10) NULL,
	[LedgerOffsetAmount] [decimal](18, 4) NULL,
	[LedgerOffsetEntrySourceDate] [datetime2](0) NULL,
	[CustomerID] [int] NOT NULL,
	[LedgerServiceUnit] [char](7) NULL,
	[LedgerServiceOrder] [char](8) NULL,
	[LedgerOffsetServiceUnit] [char](7) NULL,
	[LedgerOffsetServiceOrder] [char](8) NULL,
	[BillType] [varchar](30) NULL,
	[BillID] [int] NULL,
	[EntryStatusID] [int] NULL,
	[CreatedEnterpriseID] [varchar](8) NOT NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
	[LastModifiedEnterpriseID] [varchar](8) NULL,
	[LastModifiedDateTime] [datetime2](7) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_Discounts_comp_calls]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Discounts_comp_calls](
	[svc_un_no] [char](7) NOT NULL,
	[so_no] [char](8) NOT NULL,
	[cvg_cd] [char](2) NULL,
	[svc_cal_dt] [date] NULL,
	[so_sts_cd] [char](2) NULL,
	[pos_trs_no] [char](4) NULL,
	[pos_reg_no] [char](3) NULL,
	[mod_ext_dt] [date] NULL,
	[mod_ext_tm] [time](7) NULL,
	[tax_exm_fl] [char](1) NULL,
	[prt_tot_am] [decimal](38, 2) NULL,
	[prt_dnt_am] [decimal](38, 2) NULL,
	[prt_net_am] [decimal](38, 2) NULL,
	[prt_tax_am] [decimal](38, 2) NULL,
	[prt_grs_am] [decimal](38, 2) NULL,
	[lab_tot_am] [decimal](38, 2) NULL,
	[lab_dnt_am] [decimal](38, 2) NULL,
	[lab_net_am] [decimal](38, 2) NULL,
	[lab_tax_am] [decimal](38, 2) NULL,
	[lab_grs_am] [decimal](38, 2) NULL,
	[bas_crg_am] [decimal](38, 2) NULL,
	[lab_pls_bas_crg_am] [decimal](38, 2) NULL,
	[prt_dnt_cd_57_am] [decimal](38, 2) NULL,
	[prt_dnt_cd_58_am] [decimal](38, 2) NULL,
	[lab_dnt_cd_57_am] [decimal](38, 2) NULL,
	[lab_dnt_cd_58_am] [decimal](38, 2) NULL,
	[dsr_alw_am] [decimal](38, 2) NULL,
	[total_charges] [decimal](38, 2) NULL,
	[btc_billtocust] [varchar](1) NULL,
	[btc_comments] [varchar](max) NULL,
	[btc_lst_upd] [datetime] NULL,
	[btc_userid] [varchar](8) NULL,
	[btc_http_raw] [varchar](max) NULL,
	[btc_bill_am] [varchar](10) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EssbaseAccounts]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EssbaseAccounts](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[EntryType] [varchar](30) NOT NULL,
	[AccountingPeriod] [int] NULL,
	[AccountingWeek] [int] NULL,
	[Region] [char](7) NULL,
	[District] [char](7) NULL,
	[Unit] [char](7) NULL,
	[Location] [varchar](5) NULL,
	[LocationDescription] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](2) NULL,
	[Account] [char](5) NULL,
	[AccountDescription] [varchar](50) NULL,
	[GLDescription] [varchar](50) NULL,
	[Division] [char](4) NULL,
	[Cost] [decimal](9, 2) NULL,
	[SellValue] [decimal](9, 2) NULL,
	[StatAmount] [decimal](9, 2) NULL,
	[MUPercent] [varchar](50) NULL,
	[EntrySource] [char](3) NULL,
	[EntrySourceDescription] [varchar](50) NULL,
	[AtmCode] [varchar](50) NULL,
	[OpID] [char](7) NULL,
	[ApproverId] [char](7) NULL,
	[Jvnum] [varchar](50) NULL,
	[DocNumber] [varchar](50) NULL,
	[Misc1] [varchar](50) NULL,
	[Misc2] [varchar](50) NULL,
	[Misc3] [varchar](50) NULL,
	[JournalDate] [date] NULL,
	[ProcessingDate] [date] NULL,
	[DetailTranDate] [date] NULL,
	[ReferNbr1] [varchar](50) NULL,
	[ReferNbr2] [varchar](50) NULL,
	[ToFrom] [varchar](50) NULL,
	[TimestampFile] [datetime2](7) NULL,
 CONSTRAINT [PK_TBL_EssbaseAccounts] PRIMARY KEY CLUSTERED 
(
	[EntryID] ASC,
	[EntryType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_FirstDataPayments]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_FirstDataPayments](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[EntryType] [varchar](30) NOT NULL,
	[FirstDataAgencyID] [int] NULL,
	[FirstDataSiteID] [int] NULL,
	[FirstDataApplicationID] [int] NULL,
	[PaymentMethod] [int] NULL,
	[PaymentID] [int] NOT NULL,
	[TransactionDate] [datetime2](0) NULL,
	[PaymentCode] [int] NULL,
	[PaymentCommandCode] [int] NULL,
	[PaymentAmount] [decimal](18, 4) NULL,
	[ConfirmationNumber] [varchar](14) NULL,
	[RegisteredAccountID] [int] NULL,
	[RecurringID] [int] NULL,
	[CardTypeCode] [char](1) NULL,
	[RoutingNumber] [varchar](9) NULL,
	[AccountNumber] [varchar](9) NULL,
	[FirstName] [varchar](25) NULL,
	[MiddleInitial] [char](1) NULL,
	[LastName] [varchar](50) NULL,
	[AccountHolder] [varchar](100) NULL,
	[EmailAddress] [varchar](75) NULL,
	[StreetName] [varchar](50) NULL,
	[StreetName2] [varchar](50) NULL,
	[CityName] [varchar](50) NULL,
	[StateCode] [varchar](2) NULL,
	[ZipCode] [varchar](10) NULL,
	[PaymentChannel] [int] NULL,
	[CustomReference] [varchar](254) NULL,
	[ReturnCode] [char](3) NULL,
	[NAIUnitNumber] [char](7) NULL,
	[CRSplit] [varchar](254) NULL,
	[ServiceUnit] [char](7) NULL,
	[ServiceOrder] [char](8) NULL,
	[TechnicianID] [char](7) NULL,
	[CustomerID] [int] NULL,
	[CustomerIDUpdateSource] [varchar](30) NULL,
	[CustomerIDUpdateSourceID] [int] NULL,
	[OriginalLedgerEntryID] [int] NULL,
	[CurrentLedgerEntryID] [int] NULL,
	[CreatedEnterpriseID] [varchar](8) NULL,
	[CreatedDateTime] [datetime2](7) NULL,
	[LastModifiedEnterpriseID] [varchar](8) NULL,
	[LastModifiedDateTime] [datetime2](7) NULL,
	[CustomerIDUpdateConfidence] [int] NULL,
	[OriginalGLUnit] [char](5) NULL,
	[CurrentGLUnit] [char](5) NULL,
	[LastGLUpdateDateTime] [datetime2](7) NULL,
 CONSTRAINT [PK_TBL_FirstDataPayments] PRIMARY KEY CLUSTERED 
(
	[PaymentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_HouseholdConsumer]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_HouseholdConsumer](
	[SourceApplicationCode] [char](3) NOT NULL,
	[SourcePrimaryKey1] [varchar](50) NOT NULL,
	[SourcePrimaryKey2] [varchar](50) NULL,
	[ConsumerID] [bigint] NULL,
	[PreviousConsumerID] [bigint] NULL,
	[BusinessID] [bigint] NULL,
	[PreviousBusinessID] [bigint] NULL,
	[HouseholdID] [bigint] NULL,
	[PreviousHouseholdID] [bigint] NULL,
	[PrefixText] [varchar](15) NULL,
	[FirstName] [varchar](30) NULL,
	[MiddleName] [varchar](30) NULL,
	[LastName] [varchar](30) NULL,
	[SuffixText] [varchar](15) NULL,
	[GenderCode] [char](1) NULL,
	[SSADeceasedIndicator] [char](1) NULL,
	[PrimaryBusinessName] [varchar](50) NULL,
	[BusinessContactID] [bigint] NULL,
	[PreviousBusinessContactID] [bigint] NULL,
	[PrimaryEmailAddress] [varchar](129) NULL,
	[SecondaryEmailAddress] [varchar](129) NULL,
	[PrimaryEmailDMADoNotEmailIndicator] [char](1) NULL,
	[PrimaryPhoneNumber] [char](10) NULL,
	[SecondaryPhoneNumber] [char](10) NULL,
	[AreaCode] [char](3) NULL,
	[ExchangeCode] [char](3) NULL,
	[SubscriberNumber] [char](4) NULL,
	[WirelessIndicator] [char](1) NULL,
	[DMADoNotCallIndicator] [char](1) NULL,
	[StateAttorneyGeneralDoNotCallIndicator] [char](1) NULL,
	[FTCDoNotCallIndicator] [char](1) NULL,
	[AddressID] [bigint] NULL,
	[PreviousAddressID] [bigint] NULL,
	[AddressLine1Text] [varchar](50) NULL,
	[AddressLine2Text] [varchar](50) NULL,
	[CityName] [varchar](28) NULL,
	[StateCode] [char](2) NULL,
	[ZipCode] [char](6) NULL,
	[ZipPlug4Code] [char](4) NULL,
	[CanadaProvenceCode] [char](2) NULL,
	[CanadaPostalCode] [char](6) NULL,
	[UrbanName] [varchar](28) NULL,
	[CarrierRouteTypeCode] [char](1) NULL,
	[CarrierRouteID] [smallint] NULL,
	[DeliveryPtBarCode] [char](3) NULL,
	[DeliveryPtDropCode] [char](1) NULL,
	[DeliveryPtDropUnitsServicedQuantity] [smallint] NULL,
	[DeliveryPtZipPlus4Indicator] [char](1) NULL,
	[DeliverabilityAssignmentCode] [char](1) NULL,
	[AddressDeliveryTypeCode] [char](1) NULL,
	[AddressTypeCode] [char](1) NULL,
	[SRRiversAppendCode] [char](3) NULL,
	[SRRiversAppendConfidentialCode] [char](1) NULL,
	[NCOAMitchCode] [char](1) NULL,
	[VacancyIdentifer] [char](1) NULL,
	[SeasonalIndentier] [char](1) NULL,
	[POBoxDeliveryIndicator] [char](1) NULL,
	[LocationAddressCnvrsnSystemIdentifier] [char](1) NULL,
	[ConsumerIDOrBusinessIdentifier] [char](1) NULL,
	[LargePrisonIdenfitier] [char](1) NULL,
	[MilitaryAddressIdentifier] [char](1) NULL,
	[ScheduledMoveYearMonth] [char](6) NULL,
	[DMADoNotMailIndicator] [char](1) NULL,
	[PreferredConsumerIndicator] [char](1) NULL,
	[PreviousPreferredConsumerIndicator] [nchar](10) NULL,
	[ConsumerIDOrDerivedCode] [char](1) NULL,
	[AddressIDOrDerivedCode] [char](1) NULL,
	[BusinessIDOrDerivedCode] [char](1) NULL,
	[SelectedAddressSourceCode] [char](1) NULL,
	[ChangeOfAddressMoveTypeCode] [char](1) NULL,
	[AcxiomProcessDate] [date] NULL,
	[SourceConsumerLastInteractionDate] [date] NULL,
	[SourceConsumerLastInteractionTime] [datetime] NULL,
	[NameCheckCode] [char](3) NULL,
	[ISRCode] [char](3) NULL,
	[ISRConferenceCode] [char](1) NULL,
	[TeradataRowCreateTimestamp] [datetime] NULL,
	[TeradataRowModTimestamp] [datetime] NULL,
	[RowCreateTimestamp] [datetime] NULL,
	[RowModTimestamp] [datetime] NULL,
 CONSTRAINT [PK_TBL_HouseholdConsumer] PRIMARY KEY CLUSTERED 
(
	[SourcePrimaryKey1] ASC,
	[SourceApplicationCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_HouseholdConsumerDoNotCall]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_HouseholdConsumerDoNotCall](
	[SourceApplicationCode] [char](3) NOT NULL,
	[SourcePrimaryKey1] [varchar](50) NOT NULL,
	[SourcePrimaryKey2] [varchar](50) NULL,
	[PrimaryEmailAddress] [varchar](129) NULL,
	[SecondaryEmailAddress] [varchar](129) NULL,
	[PrimaryEmailDMADoNotEmailIndicator] [char](1) NULL,
	[PrimaryPhoneNumber] [char](10) NULL,
	[SecondaryPhoneNumber] [char](10) NULL,
	[WirelessIndicator] [char](1) NULL,
	[DMADoNotCallIndicator] [char](1) NULL,
	[StateAttorneyGeneralDoNotCallIndicator] [char](1) NULL,
	[FTCDoNotCallIndicator] [char](1) NULL,
	[VacancyIdentifer] [char](1) NULL,
	[SeasonalIndentier] [char](1) NULL,
	[POBoxDeliveryIndicator] [char](1) NULL,
	[DMADoNotMailIndicator] [char](1) NULL,
	[TeradataRowCreateTimestamp] [datetime] NULL,
	[TeradataRowModTimestamp] [datetime] NULL,
	[RowCreateTimestamp] [datetime] NULL,
	[RowModTimestamp] [datetime] NULL,
 CONSTRAINT [PK_TBL_HouseholdConsumerDoNotCall] PRIMARY KEY CLUSTERED 
(
	[SourcePrimaryKey1] ASC,
	[SourceApplicationCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_HSPOS]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_HSPOS](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[EntryType] [varchar](30) NOT NULL,
	[unit_no] [char](5) NULL,
	[register_no] [char](3) NULL,
	[transaction_no] [int] NULL,
	[salescheck_no] [varchar](20) NULL,
	[transaction_date] [date] NULL,
	[transaction_type_cd] [char](3) NULL,
	[service_unit_no] [char](7) NULL,
	[service_order_no] [char](8) NULL,
	[purchase_order_no] [varchar](17) NULL,
	[total_transaction_amount] [decimal](11, 2) NULL,
	[total_tax_amount] [decimal](11, 2) NULL,
	[total_shipping_tax_amount] [decimal](11, 2) NULL,
	[total_discount_amount] [decimal](11, 2) NULL,
	[total_vendor_shipping_amount] [decimal](11, 2) NULL,
	[reward_member_id] [char](16) NULL,
	[npn_part_order_no] [char](7) NULL,
	[part_order_no] [char](7) NULL,
	[line_seq_no] [smallint] NULL,
	[line_type_cd] [varchar](5) NULL,
	[part_coverage_cd] [char](4) NULL,
	[item_id] [char](30) NULL,
	[item_ds] [varchar](254) NULL,
	[line_qty] [smallint] NULL,
	[item_type_cd] [char](4) NULL,
	[price_amount] [decimal](7, 2) NULL,
	[cost_amount] [decimal](7, 2) NULL,
	[tax_amount] [decimal](7, 2) NULL,
	[adjustment_amount] [decimal](7, 2) NULL,
	[cardpayment_card_no] [varchar](16) NULL,
	[payment_method_cd] [char](2) NULL,
	[payment_amount] [decimal](11, 2) NULL,
	[card_account_type_cd] [varchar](300) NULL,
	[card_account_mask_no] [varchar](16) NULL,
	[card_swip_payment] [varchar](4) NULL,
	[discount_seq_no] [smallint] NULL,
	[discount_type_cd] [char](3) NULL,
	[discount_amount] [decimal](7, 2) NULL,
	[discount_ref_no] [varchar](20) NULL,
	[discount_level_cd] [varchar](4) NULL,
	[reason_cd] [varchar](4) NULL,
	[comments] [varchar](60) NULL,
	[created_by] [varchar](20) NULL,
	[transactionTime] [datetime2](7) NULL,
	[customerName] [varchar](50) NULL,
	[customerEmail] [varchar](50) NULL,
	[customerPhone] [varchar](50) NULL,
	[customerAddress] [varchar](500) NULL,
	[ServiceUnit] [char](7) NULL,
	[ServiceOrder] [char](8) NULL,
	[CustomerID] [int] NULL,
	[CustomerIDUpdateSource] [varchar](30) NULL,
	[CustomerIDUpdateSourceID] [int] NULL,
	[OriginalLedgerEntryID] [int] NULL,
	[CurrentLedgerEntryID] [int] NULL,
	[CreatedEnterpriseID] [varchar](8) NOT NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
	[LastModifiedEnterpriseID] [varchar](8) NULL,
	[LastModifiedDateTime] [datetime2](7) NULL,
	[CustomerIDUpdateConfidence] [int] NULL,
 CONSTRAINT [PK_TBL_HSPOS] PRIMARY KEY CLUSTERED 
(
	[EntryID] ASC,
	[EntryType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_LookupCodes]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_LookupCodes](
	[LookupID] [int] IDENTITY(1,1) NOT NULL,
	[OwnerType] [varchar](30) NOT NULL,
	[OwnerID] [varchar](30) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[LongDescription] [varchar](500) NULL,
	[NumericValue] [int] NULL,
	[SortOrder] [int] NOT NULL,
	[IsEnabled] [bit] NOT NULL,
	[IsUserAllowed] [bit] NOT NULL,
	[LastModified] [datetime] NOT NULL,
	[LastModifiedBy] [varchar](10) NULL,
	[AncillaryStatus] [bit] NULL,
 CONSTRAINT [PK_TBL_LookupCodes] PRIMARY KEY NONCLUSTERED 
(
	[LookupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ManualOffset]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ManualOffset](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[EntryType] [varchar](30) NOT NULL,
	[CustomerID] [int] NULL,
	[PaymentMethod] [char](2) NULL,
	[Coverage] [char](2) NULL,
	[PartsTotal] [decimal](18, 4) NULL,
	[LaborTotal] [decimal](18, 4) NULL,
	[OriginalLedgerEntryID] [int] NULL,
	[CurrentLedgerEntryID] [int] NULL,
	[OwnerType] [varchar](30) NULL,
	[OwnerID] [int] NULL,
	[ReasonID] [int] NULL,
	[ActionType] [varchar](30) NULL,
	[ActionID] [int] NULL,
	[OffsetType] [int] NOT NULL,
	[EntryAmount] [decimal](18, 4) NOT NULL,
	[ServiceUnit] [char](7) NULL,
	[ServiceOrder] [char](8) NULL,
	[CreatedEnterpriseID] [varchar](8) NULL,
	[CreatedDateTime] [datetime] NULL,
 CONSTRAINT [PK_TBL_ManualPayments] PRIMARY KEY CLUSTERED 
(
	[EntryID] DESC,
	[EntryType] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_NDJPayments]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_NDJPayments](
	[DocumentNumber] [varchar](15) NOT NULL,
	[DocumentDate] [date] NOT NULL,
	[LocationNumber] [varchar](5) NOT NULL,
	[AccountNumber] [varchar](5) NOT NULL,
	[InvoiceID] [varchar](9) NOT NULL,
	[InvoiceLine] [varchar](4) NOT NULL,
	[DUNSNumber] [varchar](11) NULL,
	[VendorName] [varchar](37) NULL,
	[VendorState] [varchar](2) NULL,
	[BankCode] [varchar](3) NULL,
	[PayDate] [varchar](3) NULL,
	[VendorCodes] [varchar](7) NULL,
	[LocationState] [varchar](2) NULL,
	[RegionCode] [varchar](1) NULL,
	[DivisionCode] [varchar](1) NULL,
	[DocumentType] [varchar](3) NULL,
	[EntryType] [varchar](2) NULL,
	[KIHReferenceNumber] [varchar](9) NULL,
	[PurchaseOrder] [varchar](10) NULL,
	[DepartmentNumber] [varchar](3) NULL,
	[SplitInvoice] [varchar](2) NULL,
	[DuplicateCheckOverride] [bit] NULL,
	[CashDiscountOverride] [bit] NULL,
	[FreightDiscountOverride] [bit] NULL,
	[TradeDiscountOverride] [bit] NULL,
	[CommissionPaySplit] [bit] NULL,
	[Commission1099IndividualTIN] [bit] NULL,
	[Commission1099TIN] [varchar](9) NULL,
	[Commission1099WTaxAmount] [decimal](18, 4) NULL,
	[Commission1099WTaxRecieved] [bit] NULL,
	[Commission1099IndividualPRTax] [bit] NULL,
	[GoodsRecievedDate] [date] NULL,
	[DeferredPriorYearDate] [date] NULL,
	[EntryDate] [date] NULL,
	[PaymentDueDate] [date] NULL,
	[CostAmount] [decimal](18, 4) NULL,
	[SellingValue] [decimal](18, 4) NULL,
	[FreightDiscount] [decimal](18, 4) NULL,
	[CashDiscount] [decimal](18, 4) NULL,
	[TradeDiscount] [decimal](18, 4) NULL,
	[OtherDiscount] [decimal](18, 4) NULL,
	[FreightAmount] [decimal](18, 4) NULL,
	[CashDiscountAmount] [decimal](18, 4) NULL,
	[TaxAmount] [decimal](18, 4) NULL,
	[CheckDate] [date] NULL,
	[CheckAmount] [decimal](18, 4) NULL,
	[CheckNumber] [varchar](9) NULL,
	[FactorDUNS] [varchar](11) NULL,
	[ManagementReportType] [varchar](10) NULL,
	[SendingSubsystem] [varchar](2) NULL,
	[InquiryReasonCode] [varchar](2) NULL,
	[KeyIndicator] [varchar](1) NULL,
	[KeyData] [varchar](5) NULL,
	[EditOverrides] [varchar](5) NULL,
	[TerminalStore] [varchar](1) NULL,
	[RemitFilmNumber] [varchar](11) NULL,
	[ConversionCode] [varchar](2) NULL,
	[CIAApproveCode] [varchar](1) NULL,
	[HDRCode] [varchar](1) NULL,
	[PayeeStreet] [varchar](36) NULL,
	[PayeeCity] [varchar](32) NULL,
	[PayeeStateCode] [varchar](2) NULL,
	[PayeezipCode] [varchar](10) NULL,
	[EmployeeNumber] [varchar](7) NULL,
	[JANumber] [varchar](7) NULL,
	[DepositTicket] [varchar](4) NULL,
	[InputUnit] [varchar](6) NULL,
	[ExpCode] [varchar](3) NULL,
	[SequenceID] [varchar](8) NULL,
	[Address2] [varchar](36) NULL,
	[Address3] [varchar](36) NULL,
	[NDJDepartment] [varchar](4) NULL,
	[CheckSerialNumber] [varchar](5) NULL,
	[SeperateCheck] [bit] NULL,
	[StatusCode] [varchar](3) NULL,
	[VendorName2] [varchar](90) NULL,
 CONSTRAINT [PK_NDJPayments] PRIMARY KEY CLUSTERED 
(
	[DocumentNumber] ASC,
	[DocumentDate] ASC,
	[LocationNumber] ASC,
	[AccountNumber] ASC,
	[InvoiceID] ASC,
	[InvoiceLine] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_PaymentMatch]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_PaymentMatch](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[EntryType] [varchar](30) NOT NULL,
	[OwnerType] [varchar](30) NOT NULL,
	[OwnerID] [int] NOT NULL,
	[ActionID] [int] NOT NULL,
	[StatusID] [int] NULL,
	[PaymentAmount] [decimal](18, 4) NOT NULL,
	[PaymentType] [varchar](20) NOT NULL,
	[PaymentDate] [date] NOT NULL,
	[CreatedEnterpriseID] [varchar](8) NOT NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
	[LastModifiedEnterpriseID] [varchar](8) NOT NULL,
	[LastModifiedDateTime] [datetime2](7) NOT NULL,
	[ActionType] [varchar](30) NULL,
	[ReasonID] [int] NULL,
	[SubStatusID] [int] NULL,
 CONSTRAINT [PK_TBL_PaymentMatch_V2] PRIMARY KEY CLUSTERED 
(
	[EntryID] ASC,
	[EntryType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_PNCCheck]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_PNCCheck](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[EntryType] [varchar](30) NOT NULL,
	[CreateDepositDateTime] [datetime2](7) NULL,
	[TransmitDateTime] [datetime2](7) NULL,
	[PostingDateTime] [datetime2](7) NULL,
	[Location] [varchar](10) NULL,
	[AccountName] [varchar](50) NULL,
	[UserLoginID] [varchar](50) NULL,
	[ItemType] [varchar](50) NULL,
	[CheckNumber] [varchar](50) NULL,
	[DepositNumber] [int] NOT NULL,
	[SequenceNumber] [int] NOT NULL,
	[EntryAmount] [decimal](18, 4) NULL,
	[ServiceUnit] [char](7) NULL,
	[ServiceOrder] [char](8) NULL,
	[ServiceOrder2] [char](8) NULL,
	[ServiceOrder3] [char](8) NULL,
	[ServiceOrder4] [char](8) NULL,
	[CustomerID] [int] NULL,
	[CustomerIDUpdateSource] [varchar](30) NULL,
	[CustomerIDUpdateSourceID] [int] NULL,
	[OriginalLedgerEntryID] [int] NULL,
	[CurrentLedgerEntryID] [int] NULL,
	[CreatedEnterpriseID] [varchar](8) NOT NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
	[LastModifiedEnterpriseID] [varchar](8) NULL,
	[LastModifiedDateTime] [datetime2](7) NULL,
	[CustomerIDUpdateConfidence] [int] NULL,
 CONSTRAINT [PK_TBL_PNCCheck] PRIMARY KEY CLUSTERED 
(
	[DepositNumber] ASC,
	[SequenceNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_PNCCheck_Staging]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_PNCCheck_Staging](
	[ReportDate] [varchar](50) NULL,
	[SubmitDate] [varchar](50) NULL,
	[PostDate] [varchar](50) NULL,
	[DepositNumber] [varchar](50) NULL,
	[ItemType] [varchar](50) NULL,
	[ItemStatus] [varchar](50) NULL,
	[SequenceNumber] [varchar](50) NULL,
	[UserLoginID] [varchar](50) NULL,
	[AccountNumber] [varchar](50) NULL,
	[PostAmount] [varchar](50) NULL,
	[CheckNumber] [varchar](50) NULL,
	[ItemID] [varchar](50) NULL,
	[Unit] [varchar](50) NULL,
	[ServiceOrderNumber01] [varchar](50) NULL,
	[ServiceOrderNumber02] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_RefuseToPay]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_RefuseToPay](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[EntryType] [varchar](30) NOT NULL,
	[ActionID] [int] NOT NULL,
	[ActionType] [varchar](30) NULL,
	[StatusID] [int] NULL,
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[ServiceCallDate] [date] NOT NULL,
	[CustomerID] [int] NOT NULL,
	[PartsDiscountReason] [varchar](50) NULL,
	[LaborDiscountReason] [varchar](50) NULL,
	[OriginalPaidAmount] [decimal](18, 4) NULL,
	[OriginalPartsAmount] [decimal](18, 4) NULL,
	[OriginalLaborAmount] [decimal](18, 4) NULL,
	[OriginalAmountWithTax] [decimal](18, 4) NULL,
	[AdjustedPartsAmount] [decimal](18, 4) NULL,
	[AdjustedLaborAmount] [decimal](18, 4) NULL,
	[AdjustedAmountWithTax] [decimal](18, 4) NULL,
	[PartsTaxRate] [decimal](18, 4) NULL,
	[LaborTaxRate] [decimal](18, 4) NULL,
	[ReasonCode] [int] NULL,
	[OriginalLedgerEntryID] [int] NULL,
	[CurrentLedgerEntryID] [int] NULL,
	[CreatedEnterpriseID] [varchar](8) NOT NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
	[LastModifiedEnterpriseID] [varchar](8) NULL,
	[LastModifiedDateTime] [datetime2](7) NULL,
	[AdjustedDeductableAmount] [decimal](18, 4) NULL,
 CONSTRAINT [PK_TBL_RefuseToPay_V2] PRIMARY KEY CLUSTERED 
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC,
	[ServiceCallDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_Requests]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Requests](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[EntryType] [varchar](30) NOT NULL,
	[RequestType] [varchar](30) NOT NULL,
	[ActionID] [int] NOT NULL,
	[ActionType] [varchar](30) NOT NULL,
	[ApprovedID] [int] NULL,
	[ApprovedType] [varchar](30) NULL,
	[ReasonID] [int] NOT NULL,
	[StatusID] [int] NOT NULL,
	[RequestEnterpriseID] [varchar](8) NULL,
	[RequestPartsAmount] [decimal](18, 4) NULL,
	[RequestLaborAmount] [decimal](18, 4) NULL,
	[RequestAmount] [decimal](18, 4) NULL,
	[ApprovedPartsAmount] [decimal](18, 4) NULL,
	[ApprovedLaborAmount] [decimal](18, 4) NULL,
	[ApprovedAmount] [decimal](18, 4) NULL,
	[CustomerID] [int] NULL,
	[ServiceUnit] [char](7) NULL,
	[ServiceOrder] [char](8) NULL,
	[PaymentMethod] [varchar](50) NULL,
	[TransactionNumber] [varchar](50) NULL,
	[OriginalLedgerEntryID] [int] NULL,
	[CurrentLedgerEntryID] [int] NULL,
	[CreatedEnterpriseID] [varchar](8) NOT NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
	[LastModifiedEnterpriseID] [varchar](8) NOT NULL,
	[LastModifiedDateTime] [datetime2](7) NOT NULL,
	[BillType] [int] NULL,
	[ClaimNumber] [varchar](30) NULL,
	[ServiceCallDate] [date] NULL,
 CONSTRAINT [PK_TBL_Requests] PRIMARY KEY CLUSTERED 
(
	[EntryID] ASC,
	[EntryType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ServiceOrderPayments]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ServiceOrderPayments](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[EntryType] [varchar](30) NULL,
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[ServiceCallDate] [date] NOT NULL,
	[PaymentMethod] [char](2) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[ServiceTechEmployee] [char](7) NOT NULL,
	[EmployeeID] [varchar](11) NULL,
	[CallCode] [char](2) NOT NULL,
	[CollectedAmount] [decimal](8, 2) NULL,
	[TotalCollectedAmount] [decimal](38, 2) NULL,
	[Coverage] [char](2) NOT NULL,
	[PartsTotal] [decimal](9, 2) NOT NULL,
	[PartsDiscount] [decimal](9, 2) NOT NULL,
	[PartsNet] [decimal](9, 2) NOT NULL,
	[PartsTax] [decimal](9, 2) NOT NULL,
	[PartsGross] [decimal](9, 2) NOT NULL,
	[LaborTotal] [decimal](9, 2) NOT NULL,
	[LaborDiscount] [decimal](9, 2) NOT NULL,
	[LaborNet] [decimal](9, 2) NOT NULL,
	[LaborTax] [decimal](9, 2) NOT NULL,
	[LaborGross] [decimal](9, 2) NOT NULL,
	[BasicCharge] [decimal](7, 2) NOT NULL,
	[LaborPlusBasicCharge] [decimal](9, 2) NOT NULL,
	[PointofSaleTransaction] [varchar](4) NOT NULL,
	[PointofSaleRegister] [varchar](3) NOT NULL,
	[PartsDiscountCode57] [decimal](9, 2) NOT NULL,
	[PartsDiscountCode58] [decimal](9, 2) NOT NULL,
	[LaborDiscountCode57] [decimal](9, 2) NOT NULL,
	[LaborDiscountCode58] [decimal](9, 2) NOT NULL,
	[DiscrepancyAllowance] [decimal](9, 2) NOT NULL,
	[TaxExemption] [char](1) NOT NULL,
	[LaborDiscountReason] [varchar](45) NULL,
	[PartsDiscountReason] [varchar](45) NULL,
	[CC1CONFIRMATION] [varchar](120) NULL,
	[CC2CONFIRMATION] [varchar](120) NULL,
	[PaymentConfirmationTpye] [varchar](25) NULL,
	[PaymentConfirmationNumber] [varchar](100) NULL,
	[OriginalLedgerEntryID] [int] NULL,
	[CurrentLedgerEntryID] [int] NULL,
	[InsertDate] [datetime2](7) NULL,
	[CreditOriginalLedgerEntryID] [int] NULL,
	[CreditCurrentLedgerEntryID] [int] NULL,
	[CreditApprovalNumber] [char](16) NULL,
	[TechHubTechnician] [int] NULL,
	[RecordStatus] [int] NULL,
 CONSTRAINT [PK_TBL_ServiceOrderPayments] PRIMARY KEY CLUSTERED 
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC,
	[ServiceCallDate] ASC,
	[PaymentMethod] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_TechnicianOrderUpload]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_TechnicianOrderUpload](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[EntryType] [varchar](30) NOT NULL,
	[OrderUploadID] [bigint] NOT NULL,
	[EnterpriseID] [varchar](8) NOT NULL,
	[NPSID] [varchar](7) NOT NULL,
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[ServiceCallDate] [date] NOT NULL,
	[ServiceCallCode] [char](2) NULL,
	[TechArrivalTime] [char](5) NULL,
	[TechEndTime] [char](5) NULL,
	[CustomerID] [int] NULL,
	[GrandTotal] [decimal](9, 2) NULL,
	[PrePaidAmount] [decimal](9, 2) NULL,
	[AmountCollected] [decimal](9, 2) NULL,
	[PartsTaxPercent] [decimal](9, 3) NULL,
	[PartsTax] [decimal](9, 2) NULL,
	[PartsNetAmount] [decimal](9, 2) NULL,
	[LaborTaxPercent] [decimal](9, 3) NULL,
	[LaborTaxCollected] [decimal](9, 2) NULL,
	[LaborTotal] [decimal](9, 2) NULL,
	[BasicHomeCallCharge] [decimal](9, 3) NULL,
	[ServiceProductDollars] [decimal](9, 2) NULL,
	[ServiceProductTaxDollars] [decimal](9, 2) NULL,
	[SpTotal] [decimal](9, 3) NULL,
	[AcceptIndicator] [varchar](5) NULL,
	[TaxExemptNum] [varchar](50) NULL,
	[CustPayTotal] [decimal](9, 2) NULL,
	[DeductibleApplied] [decimal](9, 2) NULL,
	[NewWaiveBasicCharge] [char](1) NULL,
	[PartsAssociateDiscount] [decimal](9, 2) NULL,
	[PartsPromotionDiscount] [decimal](9, 2) NULL,
	[PartsCouponDiscount] [decimal](9, 2) NULL,
	[LaborAssociateDiscount] [decimal](9, 2) NULL,
	[LaborPromotionDiscount] [decimal](9, 2) NULL,
	[LaborCouponDiscount] [decimal](9, 2) NULL,
	[SpAssociateDiscount] [decimal](9, 2) NULL,
	[CouponNumber] [varchar](50) NULL,
	[DiscountReasonCode1] [char](4) NULL,
	[DiscountReasonCode2] [char](4) NULL,
	[PrimaryPaymentMethod] [char](2) NULL,
	[PrimaryAmountCollected] [decimal](9, 2) NULL,
	[PrimaryExpirationDate] [varchar](10) NULL,
	[SecondaryPaymentMethod] [char](2) NULL,
	[SecondaryAmountCollected] [decimal](9, 2) NULL,
	[SecondaryExpirationDate] [varchar](10) NULL,
	[PayrollTransferStore] [char](7) NULL,
	[PayrollTransferSubAccount] [varchar](3) NULL,
	[PrimaryToken] [varchar](50) NULL,
	[PrimarySettlementKey] [varchar](50) NULL,
	[SecondaryToken] [varchar](50) NULL,
	[SecondarySettlementKey] [varchar](50) NULL,
	[OriginalLedgerEntryID] [int] NULL,
	[CurrentLedgerEntryID] [int] NULL,
	[SecondaryOriginalLedgerEntryID] [int] NULL,
	[SecondaryCurrentLedgerEntryID] [int] NULL,
	[CreatedEnterpriseID] [varchar](8) NOT NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
	[LastModifiedEnterpriseID] [varchar](8) NULL,
	[LastModifiedDateTime] [datetime2](7) NULL,
 CONSTRAINT [PK_TBL_TechnicianOrderUpload_V2] PRIMARY KEY CLUSTERED 
(
	[OrderUploadID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_Telecheck]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Telecheck](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[EntryType] [varchar](30) NOT NULL,
	[SubscriberNumber] [int] NULL,
	[CustomerStoreNumber] [varchar](16) NULL,
	[TraceNumber] [char](22) NOT NULL,
	[MerchantCrossReference] [varchar](50) NULL,
	[CheckWriterName] [varchar](30) NULL,
	[ProductCode] [varchar](5) NULL,
	[TransactionCode] [char](1) NULL,
	[CheckDate] [date] NULL,
	[ProcessedDate] [date] NULL,
	[EffectiveDate] [date] NULL,
	[TerminalNumber] [varchar](8) NULL,
	[BatchNumber] [int] NULL,
	[CheckNumber] [int] NULL,
	[EntryAmount] [decimal](18, 4) NULL,
	[ReasonDescription] [varchar](50) NULL,
	[FundingAccountNumber] [varchar](17) NULL,
	[FundingTraceNumber] [varchar](22) NULL,
	[TransactionType] [varchar](10) NULL,
	[CompanyName] [varchar](50) NULL,
	[AdjustDate] [date] NULL,
	[ReturnDate] [date] NULL,
	[ODFIDate] [date] NULL,
	[Descriptor] [varchar](30) NULL,
	[ReturnReasonCode] [varchar](3) NULL,
	[NAIUnitNumber] [char](7) NULL,
	[ServiceUnit] [char](7) NULL,
	[ServiceOrder] [char](8) NULL,
	[TechnicianID] [char](7) NULL,
	[CustomerID] [int] NULL,
	[CustomerIDUpdateSource] [varchar](30) NULL,
	[CustomerIDUpdateSourceID] [int] NULL,
	[OriginalLedgerEntryID] [int] NULL,
	[CurrentLedgerEntryID] [int] NULL,
	[CreatedEnterpriseID] [varchar](8) NOT NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
	[LastModifiedEnterpriseID] [varchar](8) NULL,
	[LastModifiedDateTime] [datetime2](7) NULL,
	[CustomerIDUpdateConfidence] [int] NULL,
 CONSTRAINT [PK_TBL_Telecheck] PRIMARY KEY CLUSTERED 
(
	[TraceNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_TempusCard]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_TempusCard](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[EntryType] [varchar](30) NOT NULL,
	[SystemCode] [bigint] NOT NULL,
	[Storecode] [int] NULL,
	[authdate] [datetime2](7) NULL,
	[saletype] [varchar](20) NULL,
	[tranamt] [decimal](15, 4) NULL,
	[authcode] [char](6) NULL,
	[nonauthresp] [varchar](20) NULL,
	[batchdate] [date] NULL,
	[batchid] [bigint] NULL,
	[cardtype] [varchar](20) NULL,
	[cardacct] [varchar](20) NULL,
	[TranEntryMode] [int] NULL,
	[ProcessorToken] [varchar](20) NULL,
	[TokenType] [varchar](20) NULL,
	[TokenProviderID] [int] NULL,
	[tranident] [varchar](25) NULL,
	[CustIdent] [varchar](25) NULL,
	[stationident] [varchar](25) NULL,
	[storesystemcode] [int] NULL,
	[name] [varchar](50) NULL,
	[city] [varchar](50) NULL,
	[state] [varchar](2) NULL,
	[zip] [varchar](5) NULL,
	[ChainCode] [varchar](50) NULL,
	[TranEmployeeIdent] [varchar](25) NULL,
	[MID] [bigint] NULL,
	[CCServiceName] [varchar](50) NULL,
	[NAIUnit] [varchar](7) NULL,
	[ServiceUnit] [char](7) NULL,
	[ServiceOrder] [char](8) NULL,
	[TechnicianID] [char](7) NULL,
	[CustomerID] [int] NULL,
	[CustomerIDUpdateSource] [varchar](30) NULL,
	[CustomerIDUpdateSourceID] [int] NULL,
	[OriginalLedgerEntryID] [int] NULL,
	[CurrentLedgerEntryID] [int] NULL,
	[CreatedEnterpriseID] [varchar](8) NOT NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
	[LastModifiedEnterpriseID] [varchar](8) NULL,
	[LastModifiedDateTime] [datetime2](7) NULL,
	[TechHubTechnician] [int] NULL,
	[CustomerIDUpdateConfidence] [int] NULL,
 CONSTRAINT [PK_TBL_TempusCard] PRIMARY KEY CLUSTERED 
(
	[SystemCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_TempusCheck]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_TempusCheck](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[EntryType] [varchar](30) NOT NULL,
	[SystemCode] [bigint] NOT NULL,
	[storecode] [int] NULL,
	[authdate] [datetime2](7) NULL,
	[storesystemcode] [varchar](40) NULL,
	[BatchSerial] [int] NULL,
	[DisplayText] [varchar](20) NULL,
	[tranamount] [decimal](15, 4) NULL,
	[voided] [varchar](5) NULL,
	[checktype] [varchar](20) NULL,
	[ResponseTransitNumber] [varchar](40) NULL,
	[ResponseCheckNumber] [varchar](40) NULL,
	[ApprovalCode] [varchar](40) NULL,
	[TraceID] [varchar](40) NULL,
	[ECATranStatus] [varchar](1) NULL,
	[ReturnCheckFee] [decimal](15, 4) NULL,
	[denialrecordnumber] [varchar](10) NULL,
	[TranIdent] [varchar](25) NULL,
	[ApplicationType] [varchar](50) NULL,
	[ECAAccepted] [varchar](5) NULL,
	[ECAAcceptDate] [datetime2](7) NULL,
	[ECADeclineDate] [datetime2](7) NULL,
	[ImageSize] [int] NULL,
	[StationIdent] [varchar](25) NULL,
	[TCMID] [varchar](50) NULL,
	[TCServiceName] [varchar](50) NULL,
	[Submitdate] [datetime2](7) NULL,
	[EffectiveDate] [datetime2](7) NULL,
	[AdjustDate] [datetime2](7) NULL,
	[ReturnDate] [datetime2](7) NULL,
	[storeID] [varchar](50) NULL,
	[name] [varchar](50) NULL,
	[city] [varchar](50) NULL,
	[state] [varchar](2) NULL,
	[zip] [varchar](5) NULL,
	[ChainCode] [varchar](50) NULL,
	[NAIUnit] [varchar](7) NULL,
	[TranEmployeeIdent] [varchar](25) NULL,
	[ServiceUnit] [char](7) NULL,
	[ServiceOrder] [char](8) NULL,
	[TechnicianID] [char](7) NULL,
	[CustomerID] [int] NULL,
	[CustomerIDUpdateSource] [varchar](30) NULL,
	[CustomerIDUpdateSourceID] [int] NULL,
	[CreatedEnterpriseID] [varchar](8) NOT NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
	[LastModifiedEnterpriseID] [varchar](8) NULL,
	[LastModifiedDateTime] [datetime2](7) NULL,
 CONSTRAINT [PK_TBL_TempusCheck] PRIMARY KEY CLUSTERED 
(
	[SystemCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_TempusUnitReference]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_TempusUnitReference](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[EntryType] [varchar](30) NOT NULL,
	[StoreCode] [int] NOT NULL,
	[StoreName] [varchar](50) NULL,
	[UnitNumber] [varchar](7) NULL,
	[ServiceUnit] [varchar](7) NULL,
	[CreatedEnterpriseID] [varchar](8) NOT NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
	[LastModifiedEnterpriseID] [varchar](8) NULL,
	[LastModifiedDateTime] [datetime2](7) NULL,
 CONSTRAINT [PK_TBL_TempusUnitReference] PRIMARY KEY CLUSTERED 
(
	[StoreCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[VIEW_LedgerDetail]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[VIEW_LedgerDetail]
AS
SELECT 
	l.EntryID
	,l.EntryType
	,l.CustomerID
	,l.EntryAmount
	,l.EntrySourceType
	,l.EntrySourceID
	,l.EntrySourceDate
	,l.EntrySourceCreatedBy AS EntryCreatedBy
	,l.ServiceUnit
	,l.ServiceOrder
	,l.OffsetEntryID
	,NULL AS PaymentSourceType
	,l.BillID
	,p.TechHubTechnician AS StatusID
	,u.ReportingRegion
	,s.ServiceOrderStatusCode
	,s.ServiceOrderStatusDate
	,'CREDIT' AS TransactionType
	,CASE 
		WHEN l.OffsetEntryID IS NULL AND l.BillID IS NULL THEN 'Nothing'
		WHEN l.OffsetEntryID IS NULL AND NOT l.BillID IS NULL THEN 'Billed'
		WHEN NOT l.OffsetEntryID IS NULL AND l.BillID IS NULL THEN 'Paid'
		WHEN NOT l.OffsetEntryID IS NULL AND NOT l.BillID IS NULL THEN 'Billed/Paid'
		ELSE NULL END AS ExpectationStaus
	,f.AccountingYear
	,f.AccountingMonth
	,f.AccountingWeek
	,f.AccountingDate
	,f.DayofWeekNameShort
FROM 
	ConfidentialCustomer.dbo.TBL_CustomerLedger l WITH (NOLOCK)
JOIN
	ServiceOrder.dbo.TBL_FiscalCalendar f WITH (NOLOCK)
	ON CAST(l.EntrySourceDate AS DATE) = f.AccountingDate
JOIN
	(	SELECT
			*
		FROM
			Employee.dbo.TBL_UnitCrossReference WITH (NOLOCK)
		WHERE
			ReportingRegion IN ('0000820', '0000830', '0000890', '0000900')
	) u
	ON l.ServiceUnit = u.Unit
JOIN
	(	SELECT
			ServiceUnit
			,ServiceOrder
			,ServiceOrderStatusCode
			,ServiceOrderStatusDate
		FROM
			ServiceOrder.dbo.TBL_ServiceOrder WITH (NOLOCK)
	) s
	ON l.ServiceUnit = s.ServiceUnit
	AND l.ServiceOrder = s.ServiceOrder
LEFT JOIN
	ConfidentialCustomer.dbo.TBL_CustomerBills b WITH (NOLOCK)
	ON l.BillID = b.EntryID
LEFT JOIN
	(	SELECT
			EntryType
			,EntryID
			,TechHubTechnician	
		FROM
			ConfidentialCustomer.dbo.TBL_ServiceOrderPayments WITH (NOLOCK)
	) p
	ON l.EntrySourceType = p.EntryType
	AND l.EntrySourceID = p.EntryID
WHERE
--	f.AccountingYearMonth = 201803
	f.AccountingYear >= 2019
	AND l.EntryType = 100
	AND NOT ISNULL(l.EntrySourceConfidence, 0) = 92
UNION ALL
SELECT 
	l.EntryID
	,l.EntryType
	,l.CustomerID
	,l.EntryAmount
	,l.EntrySourceType
	,l.EntrySourceID
	,l.EntrySourceDate
	,l.EntrySourceCreatedBy
	,l.ServiceUnit
	,l.ServiceOrder
	,l.OffsetEntryID
	,l.EntrySourceType AS PaymentSourceType
	,l.BillID
	,p.TechHubTechnician AS StatusID
	,u.ReportingRegion
	,s.ServiceOrderStatusCode
	,s.ServiceOrderStatusDate
	,'DEBIT' AS TransactionType
	,'NoOffset' AS ExpectationStaus
	,f.AccountingYear
	,f.AccountingMonth
	,f.AccountingWeek
	,f.AccountingDate
	,f.DayofWeekNameShort
FROM 
	ConfidentialCustomer.dbo.TBL_CustomerLedger l WITH (NOLOCK)
JOIN
	ServiceOrder.dbo.TBL_FiscalCalendar f WITH (NOLOCK)
	ON CAST(l.EntrySourceDate AS DATE) = f.AccountingDate
JOIN
	(	SELECT
			*
		FROM
			Employee.dbo.TBL_UnitCrossReference WITH (NOLOCK)
		WHERE
			ReportingRegion IN ('0000820', '0000830', '0000890', '0000900')
	) u
	ON l.ServiceUnit = u.Unit
JOIN
	(	SELECT
			ServiceUnit
			,ServiceOrder
			,ServiceOrderStatusCode
			,ServiceOrderStatusDate
		FROM
			ServiceOrder.dbo.TBL_ServiceOrder WITH (NOLOCK)
	) s
	ON l.ServiceUnit = s.ServiceUnit
	AND l.ServiceOrder = s.ServiceOrder
LEFT JOIN
	(	SELECT
			ServiceUnit
			,ServiceOrder
			,ServiceCallDate
			,EntryType
			,EntryID
			,PaymentConfirmationTpye
			,PaymentConfirmationNumber
			,TechHubTechnician	
		FROM
			ConfidentialCustomer.dbo.TBL_ServiceOrderPayments WITH (NOLOCK)
		WHERE
			CollectedAmount > 0
	) p
	ON l.ServiceUnit = p.ServiceUnit
	AND l.ServiceOrder = p.ServiceOrder
	AND CAST(l.EntrySourceDate AS DATE) = p.ServiceCallDate
WHERE
--	f.AccountingYearMonth = 201803
	f.AccountingYear >= 2019
	AND l.EntryType IN (200, 201)
	AND l.OffsetEntryID IS NULL
--	AND l.EntryID NOT IN (SELECT OffsetEntryID FROM ConfidentialCustomer.dbo.TBL_CustomerLedger WITH (NOLOCK) WHERE EntryType = 100 AND NOT OffsetEntryID IS NULL GROUP BY OffsetEntryID)
UNION ALL
SELECT 
	l.EntryID
	,'200' AS EntryType
	,l.CustomerID
	,l.EntryAmount
	,l.EntrySourceType
	,l.EntrySourceID
	,l.EntrySourceDate
	,l.EntrySourceCreatedBy
	,l.ServiceUnit
	,l.ServiceOrder
	,l.OffsetEntryID
	,l.EntrySourceType AS PaymentSourceType
	,l.BillID
	,p.TechHubTechnician AS StatusID
	,u.ReportingRegion
	,s.ServiceOrderStatusCode
	,s.ServiceOrderStatusDate
	,'DEBIT' AS TransactionType
	,'Matched' AS ExpectationStaus
	,f.AccountingYear
	,f.AccountingMonth
	,f.AccountingWeek
	,f.AccountingDate
	,f.DayofWeekNameShort
FROM 
	ConfidentialCustomer.dbo.TBL_CustomerLedger l WITH (NOLOCK)
JOIN
	ServiceOrder.dbo.TBL_FiscalCalendar f WITH (NOLOCK)
	ON CAST(l.EntrySourceDate AS DATE) = f.AccountingDate
JOIN
	(	SELECT
			*
		FROM
			Employee.dbo.TBL_UnitCrossReference WITH (NOLOCK)
		WHERE
			ReportingRegion IN ('0000820', '0000830', '0000890', '0000900')
	) u
	ON l.ServiceUnit = u.Unit
JOIN
	(	SELECT
			ServiceUnit
			,ServiceOrder
			,ServiceOrderStatusCode
			,ServiceOrderStatusDate
		FROM
			ServiceOrder.dbo.TBL_ServiceOrder WITH (NOLOCK)
	) s
	ON l.ServiceUnit = s.ServiceUnit
	AND l.ServiceOrder = s.ServiceOrder
LEFT JOIN
	(	SELECT
			ServiceUnit
			,ServiceOrder
			,ServiceCallDate
			,EntryType
			,EntryID
			,PaymentConfirmationTpye
			,PaymentConfirmationNumber
			,TechHubTechnician	
		FROM
			ConfidentialCustomer.dbo.TBL_ServiceOrderPayments WITH (NOLOCK)
		WHERE
			CollectedAmount > 0
	) p
	ON l.ServiceUnit = p.ServiceUnit
	AND l.ServiceOrder = p.ServiceOrder
	AND CAST(l.EntrySourceDate AS DATE) = p.ServiceCallDate
WHERE
--	f.AccountingYearMonth = 201803
	f.AccountingYear >= 2019
	AND l.EntryType IN (200, 201)
	AND l.OffsetEntryID IS NOT NULL
--	AND l.EntryID IN (SELECT OffsetEntryID FROM ConfidentialCustomer.dbo.TBL_CustomerLedger WITH (NOLOCK) WHERE EntryType = 100 AND NOT OffsetEntryID IS NULL GROUP BY OffsetEntryID)
UNION ALL
SELECT 
	l.EntryID
	,l.EntryType
	,l.CustomerID
	,l.EntryAmount
	,l.EntrySourceType
	,l.EntrySourceID
	,l.EntrySourceDate
	,l.EntrySourceCreatedBy
	,l.ServiceUnit
	,l.ServiceOrder
	,l.OffsetEntryID
	,NULL AS PaymentSourceType
	,l.BillID
	,NULL AS StatusID
	,u.ReportingRegion
	,s.ServiceOrderStatusCode
	,s.ServiceOrderStatusDate
	,'REFUND' AS TransactionType
	,NULL AS ExpectationStaus
	,f.AccountingYear
	,f.AccountingMonth
	,f.AccountingWeek
	,f.AccountingDate
	,f.DayofWeekNameShort
FROM 
	ConfidentialCustomer.dbo.TBL_CustomerLedger l WITH (NOLOCK)
JOIN
	ServiceOrder.dbo.TBL_FiscalCalendar f WITH (NOLOCK)
	ON CAST(l.EntrySourceDate AS DATE) = f.AccountingDate
JOIN
	(	SELECT
			*
		FROM
			Employee.dbo.TBL_UnitCrossReference WITH (NOLOCK)
		WHERE
			ReportingRegion IN ('0000820', '0000830', '0000890', '0000900')
	) u
	ON l.ServiceUnit = u.Unit
JOIN
	(	SELECT
			ServiceUnit
			,ServiceOrder
			,ServiceOrderStatusCode
			,ServiceOrderStatusDate
		FROM
			ServiceOrder.dbo.TBL_ServiceOrder WITH (NOLOCK)
	) s
	ON l.ServiceUnit = s.ServiceUnit
	AND l.ServiceOrder = s.ServiceOrder
WHERE
--	f.AccountingYearMonth = 201803
	f.AccountingYear >= 2019
	AND l.EntryType = 400
UNION ALL
SELECT 
	l.EntryID
	,'200' AS EntryType
	,NULL AS CustomerID
	,l.PaymentAmount
	,l.EntryType AS OwnerType
	,l.EntryID AS OwnerID
	,l.TransactionDate AS PaymentDate
	,COALESCE(UPPER(i.LDAP_ID), l.CreatedEnterpriseID) AS EntrySourceCreatedBy
	,l.ServiceUnit AS ServiceUnit
	,NULL AS ServiceOrder
	,NULL AS OffsetEntryID
	,NULL AS PaymentSourceType
	,NULL AS BillID
	,NULL AS StatusID
	,u.ReportingRegion AS ReportingRegion
	,NULL AS ServiceOrderStatusCode
	,NULL AS ServiceOrderStatusDate
	,'DEBIT' AS TransactionType
	,'Unmatched' AS ExpectationStaus
	,f.AccountingYear
	,f.AccountingMonth
	,f.AccountingWeek
	,f.AccountingDate
	,f.DayofWeekNameShort
--SELECT l.*
FROM 
	ConfidentialCustomer.dbo.TBL_FirstDataPayments l WITH (NOLOCK)
JOIN
	ServiceOrder.dbo.TBL_FiscalCalendar f WITH (NOLOCK)
	ON CAST(l.TransactionDate AS DATE) = f.AccountingDate
JOIN
	(	SELECT
			*
		FROM
			Employee.dbo.TBL_UnitCrossReference WITH (NOLOCK)
		WHERE
			ReportingRegion IN ('0000820', '0000830', '0000890', '0000900')
	) u
	ON l.ServiceUnit = u.Unit
LEFT JOIN
	Employee.dbo.TBL_NPSTechnicianPay e WITH (NOLOCK)
	ON l.ServiceUnit = e.Unit
	AND l.TechnicianID = e.NPSID
LEFT JOIN
	Employee.dbo.Employee_Info i WITH (NOLOCK)
	ON e.EmployeeID = i.empl_id
WHERE
	l.CustomerID IS NULL
	AND l.PaymentAmount > 0
	AND f.AccountingYear >= 2019
--	AND f.AccountingMonth < 6
UNION ALL
SELECT 
	l.EntryID
	,'200' AS EntryType
	,NULL AS CustomerID
	,l.tranamt AS PaymentAmount
	,l.EntryType AS OwnerType
	,l.EntryID AS OwnerID
	,l.authdate AS PaymentDate
	,COALESCE(UPPER(i.LDAP_ID), l.CreatedEnterpriseID) AS EntrySourceCreatedBy
	,l.ServiceUnit AS ServiceUnit
	,NULL AS ServiceOrder
	,NULL AS OffsetEntryID
	,NULL AS PaymentSourceType
	,NULL AS BillID
	,NULL AS StatusID
	,u.ReportingRegion AS ReportingRegion
	,NULL AS ServiceOrderStatusCode
	,NULL AS ServiceOrderStatusDate
	,'DEBIT' AS TransactionType
	,'Unmatched' AS ExpectationStaus
	,f.AccountingYear
	,f.AccountingMonth
	,f.AccountingWeek
	,f.AccountingDate
	,f.DayofWeekNameShort
--SELECT l.*
FROM 
	ConfidentialCustomer.dbo.TBL_TempusCard l WITH (NOLOCK)
JOIN
	ServiceOrder.dbo.TBL_FiscalCalendar f WITH (NOLOCK)
	ON CAST(l.authdate AS DATE) = f.AccountingDate
JOIN
	(	SELECT
			*
		FROM
			Employee.dbo.TBL_UnitCrossReference WITH (NOLOCK)
		WHERE
			ReportingRegion IN ('0000820', '0000830', '0000890', '0000900')
	) u
	ON l.ServiceUnit = u.Unit
LEFT JOIN
	Employee.dbo.TBL_NPSTechnicianPay e WITH (NOLOCK)
	ON l.ServiceUnit = e.Unit
	AND l.TechnicianID = e.NPSID
LEFT JOIN
	Employee.dbo.Employee_Info i WITH (NOLOCK)
	ON e.EmployeeID = i.empl_id
WHERE
	l.CustomerID IS NULL
	AND l.saletype IN ('SALE', 'DEBIT')
	AND l.authcode IS NOT NULL
	AND l.tranamt > 0
	AND f.AccountingYear >= 2019
--	AND f.AccountingMonth < 6
UNION ALL
SELECT 
	l.EntryID
	,'200' AS EntryType
	,NULL AS CustomerID
	,l.EntryAmount AS PaymentAmount
	,l.EntryType AS OwnerType
	,l.EntryID AS OwnerID
	,l.CheckDate AS PaymentDate
	,COALESCE(UPPER(i.LDAP_ID), l.CreatedEnterpriseID) AS EntrySourceCreatedBy
	,l.ServiceUnit AS ServiceUnit
	,NULL AS ServiceOrder
	,NULL AS OffsetEntryID
	,NULL AS PaymentSourceType
	,NULL AS BillID
	,NULL AS StatusID
	,u.ReportingRegion AS ReportingRegion
	,NULL AS ServiceOrderStatusCode
	,NULL AS ServiceOrderStatusDate
	,'DEBIT' AS TransactionType
	,'Unmatched' AS ExpectationStaus
	,f.AccountingYear
	,f.AccountingMonth
	,f.AccountingWeek
	,f.AccountingDate
	,f.DayofWeekNameShort
--SELECT l.*
FROM 
	ConfidentialCustomer.dbo.TBL_Telecheck l WITH (NOLOCK)
JOIN
	ServiceOrder.dbo.TBL_FiscalCalendar f WITH (NOLOCK)
	ON CAST(l.CheckDate AS DATE) = f.AccountingDate
JOIN
	(	SELECT
			*
		FROM
			Employee.dbo.TBL_UnitCrossReference WITH (NOLOCK)
		WHERE
			ReportingRegion IN ('0000820', '0000830', '0000890', '0000900')
	) u
	ON l.ServiceUnit = u.Unit
LEFT JOIN
	Employee.dbo.TBL_NPSTechnicianPay e WITH (NOLOCK)
	ON l.ServiceUnit = e.Unit
	AND l.TechnicianID = e.NPSID
LEFT JOIN
	Employee.dbo.Employee_Info i WITH (NOLOCK)
	ON e.EmployeeID = i.empl_id
WHERE
	l.CustomerID IS NULL
	AND l.TransactionCode IN ('T')
	AND f.AccountingYear >= 2019
--	AND f.AccountingMonth < 6
	AND l.EntryAmount > 0
UNION ALL
SELECT 
	l.EntryID
	,'200' AS EntryType
	,NULL AS CustomerID
	,l.total_transaction_amount AS PaymentAmount
	,l.EntryType AS OwnerType
	,l.EntryID AS OwnerID
	,l.transaction_date AS PaymentDate
	,l.CreatedEnterpriseID AS EntrySourceCreatedBy
	,l.ServiceUnit AS ServiceUnit
	,NULL AS ServiceOrder
	,NULL AS OffsetEntryID
	,NULL AS PaymentSourceType
	,NULL AS BillID
	,NULL AS StatusID
	,u.ReportingRegion AS ReportingRegion
	,NULL AS ServiceOrderStatusCode
	,NULL AS ServiceOrderStatusDate
	,'DEBIT' AS TransactionType
	,'Unmatched' AS ExpectationStaus
	,f.AccountingYear
	,f.AccountingMonth
	,f.AccountingWeek
	,f.AccountingDate
	,f.DayofWeekNameShort
--SELECT l.*
FROM 
	ConfidentialCustomer.dbo.TBL_HSPOS l WITH (NOLOCK)
JOIN
	ServiceOrder.dbo.TBL_FiscalCalendar f WITH (NOLOCK)
	ON CAST(l.transaction_date AS DATE) = f.AccountingDate
JOIN
	(	SELECT
			*
		FROM
			Employee.dbo.TBL_UnitCrossReference WITH (NOLOCK)
		WHERE
			ReportingRegion IN ('0000820', '0000830', '0000890', '0000900')
	) u
	ON l.ServiceUnit = u.Unit
WHERE
	l.CustomerID IS NULL
	AND l.transaction_type_cd = 'SAL'
	AND l.unit_no IN (SELECT RIGHT(Unit, 5) AS Unit FROM Employee.dbo.TBL_UnitCrossReference WITH (NOLOCK) WHERE UnitType IN ('In-Home District', 'HVAC District') AND ReportingRegion IN ('0000820', '0000830', '0000890', '0000900'))
	AND f.AccountingYear >= 2019
--	AND f.AccountingMonth < 6
	AND l.total_transaction_amount > 0 
UNION ALL
SELECT 
	r.EntryID
	,'100' AS EntryType
	,r.CustomerID
	,ABS(r.OriginalAmountWithTax) * -1.0 AS EntryAmount
	,r.EntryType AS EntrySourceType
	,r.EntryID AS EntrySourceID
	,r.ServiceCallDate AS EntrySourceDate
	,r.CreatedEnterpriseID AS EntryCreatedBy
	,r.ServiceUnit
	,r.ServiceOrder
	,NULL AS OffsetEntryID
	,NULL AS PaymentSourceType
	,NULL AS BillID
	,NULL AS StatusID
	,u.ReportingRegion
	,s.ServiceOrderStatusCode
	,s.ServiceOrderStatusDate
	,'CREDIT' AS TransactionType
	,'Nothing' AS ExpectationStaus
	,f.AccountingYear
	,f.AccountingMonth
	,f.AccountingWeek
	,f.AccountingDate
	,f.DayofWeekNameShort
FROM 
	ConfidentialCustomer.dbo.TBL_RefuseToPay r WITH (NOLOCK)
JOIN
	ServiceOrder.dbo.TBL_FiscalCalendar f WITH (NOLOCK)
	ON CAST(r.ServiceCallDate AS DATE) = f.AccountingDate
JOIN
	(	SELECT
			*
		FROM
			Employee.dbo.TBL_UnitCrossReference WITH (NOLOCK)
		WHERE
			ReportingRegion IN ('0000820', '0000830', '0000890', '0000900')
	) u
	ON r.ServiceUnit = u.Unit
JOIN
	(	SELECT
			ServiceUnit
			,ServiceOrder
			,ServiceOrderStatusCode
			,ServiceOrderStatusDate
		FROM
			ServiceOrder.dbo.TBL_ServiceOrder WITH (NOLOCK)
	) s
	ON r.ServiceUnit = s.ServiceUnit
	AND r.ServiceOrder = s.ServiceOrder
--LEFT JOIN
--	ConfidentialCustomer.dbo.TBL_CustomerBills b WITH (NOLOCK)
--	ON l.BillID = b.EntryID
--LEFT JOIN
--	ConfidentialCustomer.dbo.TBL_CustomerLedger o WITH (NOLOCK)
--	ON l.OffsetEntryID = o.EntryID
WHERE
--	f.AccountingYearMonth = 201803
	f.AccountingYear >= 2019
	AND (r.StatusID < 5 OR r.StatusID = 10000)
UNION ALL
SELECT 
	r.EntryID
	,'400' AS EntryType
	,r.CustomerID
	,ABS(r.RequestAmount) * -1.0 AS EntryAmount
	,r.EntryType AS EntrySourceType
	,r.EntryID AS EntrySourceID
	,r.CreatedDateTime AS EntrySourceDate
	,r.CreatedEnterpriseID AS EntryCreatedBy
	,r.ServiceUnit
	,r.ServiceOrder
	,NULL AS OffsetEntryID
	,NULL AS PaymentSourceType
	,NULL AS BillID
	,NULL AS StatusID
	,u.ReportingRegion
	,s.ServiceOrderStatusCode
	,s.ServiceOrderStatusDate
	,'REFUND' AS TransactionType
	,NULL AS ExpectationStaus
	,f.AccountingYear
	,f.AccountingMonth
	,f.AccountingWeek
	,f.AccountingDate
	,f.DayofWeekNameShort
FROM 
	ConfidentialCustomer.dbo.TBL_Requests r WITH (NOLOCK)
JOIN
	ServiceOrder.dbo.TBL_FiscalCalendar f WITH (NOLOCK)
	ON CAST(r.CreatedDateTime AS DATE) = f.AccountingDate
JOIN
	(	SELECT
			*
		FROM
			Employee.dbo.TBL_UnitCrossReference WITH (NOLOCK)
		WHERE
			ReportingRegion IN ('0000820', '0000830', '0000890', '0000900')
	) u
	ON r.ServiceUnit = u.Unit
JOIN
	(	SELECT
			ServiceUnit
			,ServiceOrder
			,ServiceOrderStatusCode
			,ServiceOrderStatusDate
		FROM
			ServiceOrder.dbo.TBL_ServiceOrder WITH (NOLOCK)
	) s
	ON r.ServiceUnit = s.ServiceUnit
	AND r.ServiceOrder = s.ServiceOrder
--LEFT JOIN
--	ConfidentialCustomer.dbo.TBL_CustomerBills b WITH (NOLOCK)
--	ON l.BillID = b.EntryID
--LEFT JOIN
--	ConfidentialCustomer.dbo.TBL_CustomerLedger o WITH (NOLOCK)
--	ON l.OffsetEntryID = o.EntryID
WHERE
--	f.AccountingYearMonth = 201803
	f.AccountingYear >= 2019
	AND r.StatusID < 3
	AND r.RequestType = 'Refund'
	



















GO
/****** Object:  View [dbo].[VIEW_TechnicianReporting]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[VIEW_TechnicianReporting]
AS
SELECT 
	u.ReportingRegion AS Region
	,u.ReportingDistrict AS District
	,UPPER(u.OwnerLDAPID) AS GMEnterpriseID
	,UPPER(u.GMFullName) AS GMFullName  --l.StatusID AS GMFullName
	,UPPER(e.TMDTMEnterpriseID) AS TechManagerEnterpriseID
	,UPPER(e.TMDTMFullName) AS TechManagerName
	,UPPER(l.EntryCreatedBy) AS EnterpriseID
	,UPPER(n.FullName) AS TechnicianName
	,l.AccountingYear
	,l.AccountingMonth
	,l.AccountingWeek
	,l.AccountingDate
	,SUM(CASE WHEN l.TransactionType = 'CREDIT' AND l.EntrySourceType = 'ServiceOrderPayments' THEN 1 ELSE 0 END) AS Credits
	,SUM(CASE WHEN l.TransactionType = 'CREDIT' AND l.EntrySourceType = 'ServiceOrderPayments' THEN l.EntryAmount ELSE 0 END) AS CreditAmount
	,SUM(CASE WHEN l.TransactionType = 'CREDIT' AND l.EntrySourceType = 'ServiceOrderPayments' AND l.OffsetEntryID IS NULL THEN 1 ELSE 0 END) AS CreditNoOffset
	,SUM(CASE WHEN l.TransactionType = 'CREDIT' AND l.EntrySourceType = 'ServiceOrderPayments' AND l.OffsetEntryID IS NULL THEN l.EntryAmount ELSE 0 END) AS CreditNoOffsetAmount
	,SUM(CASE WHEN l.TransactionType = 'CREDIT' AND l.EntrySourceType = 'ServiceOrderPayments' AND l.BillID IS NOT NULL THEN 1 ELSE 0 END) AS CreditBilled
	,SUM(CASE WHEN l.TransactionType = 'CREDIT' AND l.EntrySourceType = 'ServiceOrderPayments' AND l.BillID IS NOT NULL THEN l.EntryAmount ELSE 0 END) AS CreditBilledAmount
	,SUM(CASE WHEN l.TransactionType = 'CREDIT' AND l.EntrySourceType = 'ServiceOrderPayments' AND l.OffsetEntryID IS NOT NULL THEN 1 ELSE 0 END) AS CreditPaid
	,SUM(CASE WHEN l.TransactionType = 'CREDIT' AND l.EntrySourceType = 'ServiceOrderPayments' AND l.OffsetEntryID IS NOT NULL THEN l.EntryAmount ELSE 0 END) AS CreditPaidAmount
	,SUM(CASE WHEN l.TransactionType = 'CREDIT' AND l.EntrySourceType = 'RefuseToPay' THEN 1 ELSE 0 END) AS CreditDiscount
	,SUM(CASE WHEN l.TransactionType = 'CREDIT' AND l.EntrySourceType = 'RefuseToPay' THEN l.EntryAmount ELSE 0 END) AS CreditDiscountAmount
	,SUM(CASE WHEN l.TransactionType = 'DEBIT' THEN 1 ELSE 0 END) AS Debits
	,SUM(CASE WHEN l.TransactionType = 'DEBIT' THEN l.EntryAmount ELSE 0 END) AS DebitAmount
	,SUM(CASE WHEN l.TransactionType = 'DEBIT' AND l.ExpectationStaus = 'NoOffset' THEN 1 ELSE 0 END) AS DebitsNoOffset
	,SUM(CASE WHEN l.TransactionType = 'DEBIT' AND l.ExpectationStaus = 'NoOffset' THEN l.EntryAmount ELSE 0 END) AS DebitNoOffsetAmount
	,SUM(CASE WHEN l.TransactionType = 'DEBIT' AND l.ExpectationStaus = 'Unmatched' THEN 1 ELSE 0 END) AS DebitsUnmatched
	,SUM(CASE WHEN l.TransactionType = 'DEBIT' AND l.ExpectationStaus = 'Unmatched' THEN l.EntryAmount ELSE 0 END) AS DebitUnmatchedAmount
	,SUM(CASE WHEN l.TransactionType = 'DEBIT' AND l.EntrySourceType = 'FirstDataPayments' THEN 1 ELSE 0 END) AS DebitsIVR
	,SUM(CASE WHEN l.TransactionType = 'DEBIT' AND l.EntrySourceType = 'FirstDataPayments' THEN l.EntryAmount ELSE 0 END) AS DebitIVRAmount
	,SUM(CASE WHEN l.TransactionType = 'REFUND' THEN 1 ELSE 0 END) AS Refunds
	,SUM(CASE WHEN l.TransactionType = 'REFUND' THEN l.EntryAmount ELSE 0 END) AS RefundAmount
FROM 
	ConfidentialCustomer.dbo.VIEW_LedgerDetail l
JOIN
	(	SELECT 
			u.Unit
			,u.ReportingDistrict
			,d.ReportingRegion
			,d.OwnerLDAPID
			,n.FullName AS GMFullName
		FROM 
			Employee.dbo.TBL_UnitCrossReference u WITH (NOLOCK)
		JOIN
			Employee.dbo.TBL_UnitCrossReference d WITH (NOLOCK)
			ON u.ReportingDistrict = d.Unit
		LEFT JOIN
			Employee.dbo.TBL_Employee_New n with (NOLOCK)
			ON d.OwnerLDAPID = n.EnterpriseID
		WHERE
			u.UnitType IN ('In-Home District', 'HVAC District')
	) u
	ON l.ServiceUnit = u.Unit
LEFT JOIN
	Employee.dbo.TBL_EmployeeHierarchyIHHVACTechRegion e WITH (NOLOCK)
	ON l.EntryCreatedBy = e.EnterpriseID
LEFT JOIN
	Employee.dbo.TBL_Employee_New n with (NOLOCK)
	ON l.EntryCreatedBy = n.EnterpriseID
GROUP BY
	u.ReportingRegion
	,u.ReportingDistrict
	,UPPER(u.OwnerLDAPID)
	,UPPER(u.GMFullName)	--l.StatusID
	,UPPER(e.TMDTMEnterpriseID)
	,UPPER(e.TMDTMFullName)
	,UPPER(l.EntryCreatedBy)
	,UPPER(n.FullName)
	,l.AccountingYear
	,l.AccountingMonth
	,l.AccountingWeek
	,l.AccountingDate



GO
/****** Object:  View [dbo].[VIEW_Monetary]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[VIEW_Monetary]
AS
SELECT
	d.AccountingYear
	,d.AccountingMonth
	,d.AccountingWeek
	,u.ReportingRegion
	,u.ReportingDistrict
	,a.ServiceUnit
	,a.ServiceOrder
	,s.NPSSpecialty
	,s.PartDivision AS Division
	,a.ServiceCallDate
	,s.PaymentMethod
	,a.TechnicianType
	,a.ServiceTechEnterpriseID
	,s.ServiceTypeCode
	,s.PrimaryJobCode
	,a.CallCode
	,CASE
		WHEN a.CallCode = '38' THEN 'Declined Estimate'
		WHEN ISNULL(s.PrimaryJobCode, '0') IN ('66100', '66101', '66102', '66103', '66104', '66105', '66110', '66111', '66112', '66113', '66114', '66115') THEN 'Clean Maintain'
		WHEN ISNULL(s.PrimaryJobCode, '0') IN ('95100', '95101', '95102', '95103', '95104', '95105') THEN 'PM Check'
		WHEN ISNULL(s.PrimaryJobCode, '0') IN ('95700') THEN 'Unable To Verify'
		WHEN ISNULL(s.PrimaryJobCode, '0') IN ('95310') THEN 'Detailed Estimate'
		WHEN ISNULL(s.ServiceTypeCode, 'S') IN ('Q', 'R') THEN 'Recall'
		WHEN ISNULL(s.SubAccountCode, '1') IN ('912') THEN 'Recall'
		ELSE 'Repair'
		END AS RepairType
	,ISNULL(m.CCPartsGross, 0) AS CCPartsGross
	,ISNULL(m.CCLaborGross, 0) AS CCLaborGross
	,ISNULL(m.DiscrepancyAllowance, 0) AS DiscrepancyAllowance
	,ISNULL(p.CollectedAmount, 0) AS CollectedAmount
	,CASE WHEN ISNULL(m.CCLaborTotal, 0) = 0.0 THEN 1 ELSE 0 END AS NoLabor
	,CASE WHEN t.TechnicianEnterpriseID IS NULL THEN 0 ELSE 1 END AS TechHub
	,CASE WHEN LEFT(LTRIM(s.SpecialInstructionsDescOne), 3) = 'SOC' THEN 1 ELSE 0 END AS SOC
	,CASE WHEN ISNULL(s.PrimaryJobCode, '0') IN ('66100', '66101', '66102', '66103', '66104', '66105', '66110', '66111', '66112', '66113', '66114', '66115') THEN 1 ELSE 0 END AS CM
	,CASE WHEN ISNULL(s.PrimaryJobCode, '0') IN ('95100', '95101', '95102', '95103', '95104', '95105') THEN 1 ELSE 0 END AS PM
	,CASE WHEN ISNULL(s.PrimaryJobCode, '0') IN ('95700') THEN 1 ELSE 0 END AS UVC
	,CASE WHEN ISNULL(s.PrimaryJobCode, '0') IN ('95310') THEN 1 ELSE 0 END AS DE
	,CASE WHEN ISNULL(s.ServiceTypeCode, 'S') IN ('Q', 'R') THEN 1 ELSE 0 END AS RecallType
	,CASE WHEN ISNULL(s.SubAccountCode, '1') IN ('912') AND NOT ISNULL(s.ServiceTypeCode, 'S') IN ('Q', 'R') THEN 1 ELSE 0 END AS RecallAccount
	,1 AS CountRecords
FROM
	ServiceOrder.dbo.TBL_NPSServiceOrderAttempt a WITH (NOLOCK)
JOIN
	ServiceOrder.dbo.TBL_FiscalCalendar d WITH (NOLOCK)
	ON a.ServiceCallDate = d.AccountingDate
JOIN
	(	SELECT
			ServiceUnit
			,ServiceOrder
			,PaymentMethod
			,ServiceTypeCode
			,PrimaryJobCode
			,PartDivision
			,NPSSpecialty
			,SubAccountCode
			,SpecialInstructionsDescOne
		FROM
			ServiceOrder.dbo.TBL_ServiceOrder WITH (NOLOCK)		
	) s
	ON a.ServiceUnit = s.ServiceUnit
	AND a.ServiceOrder = s.ServiceOrder
LEFT JOIN
	(	SELECT
			*
		FROM
			Employee.dbo.TBL_EmployeeTechHubTimepunches WITH (NOLOCK)
		WHERE
			EntryType = 'End Order'
	) t
	ON a.ServiceCallDate = t.RouteDate
	AND a.ServiceTechEnterpriseID = t.TechnicianEnterpriseID
	AND a.ServiceOrder = t.EntryNumber
LEFT JOIN
	(	SELECT
			ServiceUnit
			,ServiceOrder
			,SUM(CASE WHEN Coverage = 'SP' THEN PartsGross ELSE 0 END) AS SPPartsGross
			,SUM(CASE WHEN Coverage = 'SP' THEN LaborGross ELSE 0 END) AS SPLaborGross
			,SUM(CASE WHEN Coverage = 'PT' THEN PartsGross ELSE 0 END) AS PTPartsGross
			,SUM(CASE WHEN Coverage = 'PT' THEN LaborGross ELSE 0 END) AS PTLaborGross
			,SUM(CASE WHEN Coverage = 'IW' THEN PartsGross ELSE 0 END) AS IWPartsGross
			,SUM(CASE WHEN Coverage = 'IW' THEN LaborGross ELSE 0 END) AS IWLaborGross
			,SUM(CASE WHEN Coverage IN ('CC', 'PP') THEN PartsGross ELSE 0 END) AS CCPartsGross
			,SUM(CASE WHEN Coverage IN ('CC', 'PP') THEN LaborGross ELSE 0 END) AS CCLaborGross
			,SUM(CASE WHEN Coverage IN ('CC') THEN LaborTotal ELSE 0 END) AS CCLaborTotal
			,SUM(PartsDiscountCode57) AS PartsDiscountCode57
			,SUM(PartsDiscountCode58) AS PartsDiscountCode58
			,SUM(LaborDiscountCode57) AS LaborDiscountCode57
			,SUM(LaborDiscountCode58) AS LaborDiscountCode58
			,SUM(DiscrepancyAllowance) AS DiscrepancyAllowance
		FROM 	
			ServiceOrder.dbo.TBL_NPSServiceOrderMonetaryCharges WITH (NOLOCK)
		GROUP BY	
			ServiceUnit
			,ServiceOrder
	) m
	ON a.ServiceUnit = m.ServiceUnit
	AND a.ServiceOrder = m.ServiceOrder
--	AND a.ServiceCallDate = m.ServiceCallDate
LEFT JOIN
	(	SELECT
			ServiceUnit
			,ServiceOrder
			,SUM(CollectedAmount) AS CollectedAmount
		FROM
			ServiceOrder.dbo.TBL_NPSServiceOrderPaymentMethod WITH (NOLOCK)
		WHERE
			NOT ISNULL(RecordStatus, '111') = '999'
		GROUP BY
			ServiceUnit
			,ServiceOrder
	) p
	ON a.ServiceUnit = p.ServiceUnit
	AND a.ServiceOrder = p.ServiceOrder
JOIN
	Employee.dbo.TBL_UnitCrossReference u WITH (NOLOCK)
	ON a.ServiceUnit = u.Unit
WHERE
	a.CallCode IN ('30', '38')
--	AND ISNULL(m.CCLaborTotal, 0) = 0
--	AND NOT ISNULL(s.PrimaryJobCode, '0') IN ('66100', '66101', '66102', '66103', '66104', '66110', '66111', '66112', '66113', '66114')
--	AND NOT ISNULL(s.ServiceTypeCode, 'S') IN ('Q', 'R')
--	AND NOT ISNULL(s.SubAccountCode, '1') IN ('912')
	AND d.AccountingYear = 2018
--	AND d.AccountingMonth IN (6, 7, 8, 9)
--	AND m.ServiceOrder IS NULL
--ORDER BY
--	a.ServiceCallDate





GO
/****** Object:  View [dbo].[VIEW_MonetaryUpdated]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








CREATE VIEW [dbo].[VIEW_MonetaryUpdated]
AS
SELECT
	d.AccountingYear
	,d.AccountingMonth
	,d.AccountingWeek
	,u.ReportingRegion
	,u.ReportingDistrict
	,a.ServiceUnit
	,a.ServiceOrder
	,s.ClientCoverage AS ThirdPartyIDCoverage
	,s.ThirdPartyID
	,s.NPSSpecialty
	,s.PartDivision AS Division
	,a.ServiceCallDate
	,s.PaymentMethod
	,a.TechnicianType
	,a.ServiceTechEnterpriseID
	,a.TechnicianCommentsOne
	,a.TechnicianCommentsTwo
	,s.ServiceTypeCode
	,s.PrimaryJobCode
	,h.JobCodeDescription
	,h.DifficultyLevel
	,a.CallCode
	,s.SubAccountCode
	,CASE
		WHEN a.CallCode = '38' THEN 'Declined Estimate'
		WHEN ISNULL(s.PrimaryJobCode, '0') IN ('90001') THEN 'Trip Charge'
		WHEN ISNULL(s.PrimaryJobCode, '0') IN ('90000') THEN 'Deductible'
		WHEN ISNULL(s.ServiceTypeCode, 'S') IN ('Q', 'R') THEN 'Recall'
		WHEN ISNULL(s.SubAccountCode, '1') IN ('912') THEN 'Recall'
		WHEN ISNULL(s.PrimaryJobCode, '0') IN ('66100', '66101', '66102', '66103', '66104', '66105', '66110', '66111', '66112', '66113', '66114', '66115') THEN 'Clean Maintain'
		WHEN ISNULL(s.PrimaryJobCode, '0') IN ('95100', '95101', '95102', '95103', '95104', '95105','28000','86000') THEN 'PM Check'
--		WHEN ISNULL(s.PrimaryJobCode, '0') IN ('95700') THEN 'Unable To Verify'
--		WHEN ISNULL(s.PrimaryJobCode, '0') IN ('95310') THEN 'Detailed Estimate'
		ELSE 'Repair'
		END AS RepairType
	,ISNULL(m.PrePayAmount, 0) AS PrePayAmount
	,ISNULL(j.DiagnosticFee, 0) AS DiagnosticFee
	,ISNULL(j.DiagnosticFeeNoCharge, 0) AS DiagnosticFeeNoCharge
	,ISNULL(j.DiagnosticFeeNoCC, 0) AS DiagnosticFeeNoCC
	,ISNULL(m.CCPartsTotal, 0) AS CCPartsTotal
	,ISNULL(m.CCLaborTotal, 0) AS CCLaborTotal
	,ISNULL(m.CCPartsGross, 0) AS CCPartsGross
	,ISNULL(m.CCLaborGross, 0) AS CCLaborGross
	,ISNULL(m.DiscrepancyAllowance, 0) AS DiscrepancyAllowance
	,ISNULL(p.CollectedAmount, 0) AS CollectedAmount
	,ISNULL(j.JobCodes, 0) AS JobCodes
	,ISNULL(j.JobCodesWithAmount, 0) AS JobCodesWithAmount
	,ISNULL(j.CCJobCodes, 0) AS CCJobCodes
	,ISNULL(j.CCJobCodesWithAmount, 0) AS CCJobCodesWithAmount
	,ISNULL(j.IWJobCodes, 0) AS IWJobCodes
	,ISNULL(j.IWJobCodesWithAmount, 0) AS IWJobCodesWithAmount
	,ISNULL(j.PTJobCodes, 0) AS PTJobCodes
	,ISNULL(j.PTJobCodesWithAmount, 0) AS PTJobCodesWithAmount
	,CASE WHEN ISNULL(m.CCLaborTotal, 0) = 0.0 THEN 1 ELSE 0 END AS NoLabor
	,CASE WHEN LEFT(LTRIM(s.SpecialInstructionsDescOne), 3) = 'SOC' THEN 1 ELSE 0 END AS SOC
	,CASE WHEN ISNULL(s.PrimaryJobCode, '0') IN ('66100', '66101', '66102', '66103', '66104', '66105', '66110', '66111', '66112', '66113', '66114', '66115') THEN 1 ELSE 0 END AS CM
	,CASE WHEN ISNULL(s.PrimaryJobCode, '0') IN ('95100', '95101', '95102', '95103', '95104', '95105') THEN 1 ELSE 0 END AS PM
	,CASE WHEN ISNULL(s.PrimaryJobCode, '0') IN ('95700') THEN 1 ELSE 0 END AS UVC
	,CASE WHEN ISNULL(s.PrimaryJobCode, '0') IN ('95310') THEN 1 ELSE 0 END AS DE
	,CASE WHEN ISNULL(s.ServiceTypeCode, 'S') IN ('Q', 'R') THEN 1 ELSE 0 END AS RecallType
	,CASE WHEN ISNULL(s.SubAccountCode, '1') IN ('912') AND NOT ISNULL(s.ServiceTypeCode, 'S') IN ('Q', 'R') THEN 1 ELSE 0 END AS RecallAccount
	,1 AS CountRecords
	,CASE WHEN ISNULL(s.PrimaryJobCode, '0') = '0' THEN 0 ELSE 1 END AS JobCode
FROM
	ServiceOrder.dbo.TBL_NPSServiceOrderAttempt a WITH (NOLOCK)
JOIN
	ServiceOrder.dbo.TBL_FiscalCalendar d WITH (NOLOCK)
	ON a.ServiceCallDate = d.AccountingDate
JOIN
	Employee.dbo.TBL_UnitCrossReference u WITH (NOLOCK)
	ON a.ServiceUnit = u.Unit
JOIN
	(	SELECT
			ServiceUnit
			,ServiceOrder
			,PaymentMethod
			,ServiceTypeCode
			,CombinedThirdPartyID AS ThirdPartyID
			,ClientCoverage
			,PrimaryJobCode
			,PartDivision
			,NPSSpecialty
			,SubAccountCode
			,SpecialInstructionsDescOne
		FROM
			ServiceOrder.dbo.TBL_ServiceOrder WITH (NOLOCK)		
	) s
	ON a.ServiceUnit = s.ServiceUnit
	AND a.ServiceOrder = s.ServiceOrder
LEFT JOIN
	(	SELECT
			ServiceUnit
			,ServiceOrder
			,SUM(CASE WHEN Coverage = 'SP' THEN PartsGross ELSE 0 END) AS SPPartsGross
			,SUM(CASE WHEN Coverage = 'SP' THEN LaborGross ELSE 0 END) AS SPLaborGross
			,SUM(CASE WHEN Coverage = 'PT' THEN PartsGross ELSE 0 END) AS PTPartsGross
			,SUM(CASE WHEN Coverage = 'PT' THEN LaborGross ELSE 0 END) AS PTLaborGross
			,SUM(CASE WHEN Coverage = 'IW' THEN PartsGross ELSE 0 END) AS IWPartsGross
			,SUM(CASE WHEN Coverage = 'IW' THEN LaborGross ELSE 0 END) AS IWLaborGross
			,SUM(CASE WHEN Coverage IN ('CC') THEN PartsGross ELSE 0 END) AS CCPartsGross
			,SUM(CASE WHEN Coverage IN ('CC') THEN LaborGross ELSE 0 END) AS CCLaborGross
			,SUM(CASE WHEN Coverage IN ('PP') THEN LaborGross ELSE 0 END) AS PrePayAmount
			,SUM(CASE WHEN Coverage IN ('CC') THEN PartsTotal ELSE 0 END) AS CCPartsTotal
			,SUM(CASE WHEN Coverage IN ('CC') THEN LaborTotal ELSE 0 END) AS CCLaborTotal
			,SUM(PartsDiscountCode57) AS PartsDiscountCode57
			,SUM(PartsDiscountCode58) AS PartsDiscountCode58
			,SUM(LaborDiscountCode57) AS LaborDiscountCode57
			,SUM(LaborDiscountCode58) AS LaborDiscountCode58
			,SUM(DiscrepancyAllowance) AS DiscrepancyAllowance
		FROM 	
			ServiceOrder.dbo.TBL_NPSServiceOrderMonetaryCharges WITH (NOLOCK)
		GROUP BY	
			ServiceUnit
			,ServiceOrder
	) m
	ON a.ServiceUnit = m.ServiceUnit
	AND a.ServiceOrder = m.ServiceOrder
--	AND a.ServiceCallDate = m.ServiceCallDate
LEFT JOIN
	(	SELECT
			ServiceUnit
			,ServiceOrder
			,SUM(CollectedAmount) AS CollectedAmount
		FROM
			ServiceOrder.dbo.TBL_NPSServiceOrderPaymentMethod WITH (NOLOCK)
		WHERE
			NOT ISNULL(RecordStatus, '111') = '999'
		GROUP BY
			ServiceUnit
			,ServiceOrder
	) p
	ON a.ServiceUnit = p.ServiceUnit
	AND a.ServiceOrder = p.ServiceOrder
LEFT JOIN
	(	SELECT
			ServiceUnit
			,ServiceOrder
			,COUNT(*) AS JobCodes
			,SUM(CASE WHEN JobCode = '90001' AND CoverageCode = 'CC' AND LaborEnteredAmount > 0 THEN 1 ELSE 0 END) AS DiagnosticFee
			,SUM(CASE WHEN JobCode = '90001' AND CoverageCode = 'CC' AND LaborEnteredAmount = 0 THEN 1 ELSE 0 END) AS DiagnosticFeeNoCharge
			,SUM(CASE WHEN JobCode = '90001' AND NOT CoverageCode = 'CC' AND LaborEnteredAmount > 0 THEN 1 ELSE 0 END) AS DiagnosticFeeNoCC
			,SUM(CASE WHEN LaborEnteredAmount > 0 THEN 1 ELSE 0 END) AS JobCodesWithAmount
			,SUM(CASE WHEN CoverageCode = 'CC' THEN 1 ELSE 0 END) AS CCJobCodes
			,SUM(CASE WHEN CoverageCode = 'PT' THEN 1 ELSE 0 END) AS PTJobCodes
			,SUM(CASE WHEN CoverageCode = 'IW' THEN 1 ELSE 0 END) AS IWJobCodes
			,SUM(CASE WHEN CoverageCode = 'SP' THEN 1 ELSE 0 END) AS SPJobCodes
			,SUM(CASE WHEN CoverageCode = 'CC' AND LaborEnteredAmount > 0 THEN 1 ELSE 0 END) AS CCJobCodesWithAmount
			,SUM(CASE WHEN CoverageCode = 'PT' AND LaborEnteredAmount > 0 THEN 1 ELSE 0 END) AS PTJobCodesWithAmount
			,SUM(CASE WHEN CoverageCode = 'IW' AND LaborEnteredAmount > 0 THEN 1 ELSE 0 END) AS IWJobCodesWithAmount
			,SUM(CASE WHEN CoverageCode = 'SP' AND LaborEnteredAmount > 0 THEN 1 ELSE 0 END) AS SPJobCodesWithAmount
		FROM 
			ServiceOrder.dbo.TBL_NPSServiceOrderAttemptJobCode WITH (NOLOCK)
		GROUP BY
			ServiceUnit
			,ServiceOrder
	) j
	ON a.ServiceUnit = j.ServiceUnit
	AND a.ServiceOrder = j.ServiceOrder
LEFT JOIN
	(	SELECT DISTINCT
			Specialty
			,JobCode
			,JobCodeDescription
			,DifficultyLevel
		FROM
			ServiceOrder.dbo.TBL_TechHubJobCodes WITH (NOLOCK)
	) h
	ON s.PrimaryJobCode = h.JobCode
	AND s.NPSSpecialty = h.Specialty
WHERE
	a.CallCode IN ('30', '38')
--	AND ISNULL(m.CCLaborTotal, 0) = 0
--	AND a.TechnicianType = '15'
--	AND NOT ISNULL(s.PrimaryJobCode, '0') IN ('66100', '66101', '66102', '66103', '66104', '66110', '66111', '66112', '66113', '66114', '66115', '95100', '95101', '95102', '95103', '95104', '95105','28000','86000')
--	AND NOT ISNULL(s.ServiceTypeCode, 'S') IN ('Q', 'R')
--	AND NOT ISNULL(s.SubAccountCode, '1') IN ('912')
	AND d.AccountingYear >= 2018
--	AND d.AccountingMonth IN (6, 7, 8, 9)
--	AND m.ServiceOrder IS NULL
--ORDER BY
--	a.ServiceCallDate











GO
/****** Object:  View [dbo].[VIEW_ServiceOrderCredits]    Script Date: 9/18/2019 7:23:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







CREATE VIEW [dbo].[VIEW_ServiceOrderCredits]
AS


WITH x as (
SELECT 
	l.CustomerID
	,l.ServiceUnit
	,l.ServiceOrder
	,l.EntrySourceDate AS ServiceCallDate
	,CASE 
		WHEN l.EntrySourceType = 'ServiceOrderPayments' THEN 'NPSXTPM'
		ELSE 'RefuseToPay'
		END AS ChargeType
	,lm.ServiceOrderChargedAmount
	,pd.ServiceOrderPaidAmount
	,o.ServiceOrderAmount as ServiceOrderNetAmount
	,c.CustomerAmount as CustomerNetAmount
	,CASE 
		WHEN o.ServiceOrderAmount < 0.0 THEN 'Shortage'
		WHEN o.ServiceOrderAmount = 0.0 THEN 'Balanced'
		ELSE 'Overage'
		END AS ServiceOrderBalance
	,CASE 
		WHEN c.CustomerAmount < 0.0 THEN 'Shortage'
		WHEN c.CustomerAmount = 0.0 THEN 'Balanced'
		ELSE 'Overage'
		END AS CustomerBalance
	,s.ServiceOrderStatusCode
	,s.ServiceOrderStatusDate
	,u.ReportingRegion AS Region
	,u.ReportingDistrict AS District
	,UPPER(u.OwnerLDAPID) AS GMEnterpriseID
	,UPPER(u.GMFullName) AS GMFullName
	,UPPER(e.TMDTMEnterpriseID) AS TechManagerEnterpriseID
	,UPPER(e.TMDTMFullName) AS TechManagerName
	,UPPER(l.EntrySourceCreatedBy) AS EnterpriseID
	,UPPER(n.FullName) AS TechnicianName
	,f.AccountingYear
	,f.AccountingMonth
	,f.AccountingWeek
	,f.AccountingDate
	/*,CASE
		WHEN p.EntrySourceType = 'ServiceOrderPayments' THEN 'NPS'
		WHEN p.EntrySourceType = 'FirstDataPayments' THEN 'IVR'
		ELSE p.EntrySourceType
		END AS PaymentType
	,p.EntrySourceDate AS PaymentDate
	,p.EntrySourceConfidence AS PaymentConfidenceLevel
	
	,p.EntryAmount AS PaymentAmount
	*/
	,l.EntryID 
FROM (
		SELECT
			SUM(EntryAmount) as ServiceOrderChargedAmount
			,MAX(EntryID) as EntryID
			,CustomerID
			,ServiceUnit
			,ServiceOrder
		FROM
			ConfidentialCustomer.dbo.TBL_CustomerLedger l WITH (NOLOCK)
		WHERE 
			EntryType = 100
			AND NOT ISNULL(EntrySourceConfidence, 0) = 92
		GROUP BY
			CustomerID
			,ServiceUnit
			,ServiceOrder
	) as lm
	JOIN ConfidentialCustomer.dbo.TBL_CustomerLedger l WITH (NOLOCK) ON l.EntryID = lm.EntryID
	JOIN ServiceOrder.dbo.TBL_FiscalCalendar f WITH (NOLOCK) ON CAST(l.EntrySourceDate AS DATE) = f.AccountingDate
	JOIN
	(	SELECT
			ServiceUnit
			,ServiceOrder
			,ServiceOrderStatusCode
			,ServiceOrderStatusDate
		FROM
			ServiceOrder.dbo.TBL_ServiceOrder WITH (NOLOCK)
	) s
	ON l.ServiceUnit = s.ServiceUnit
	AND l.ServiceOrder = s.ServiceOrder
JOIN
	(	SELECT
			CustomerID
			,SUM(EntryAmount) AS CustomerAmount
		FROM
			ConfidentialCustomer.dbo.TBL_CustomerLedger WITH (NOLOCK)
		WHERE
			EntryType IN (100, 200)
			AND NOT ISNULL(EntrySourceConfidence, 0) = 92
		GROUP BY
			CustomerID
	) c
	ON l.CustomerID = c.CustomerID
JOIN
	(	SELECT
			ServiceUnit
			,ServiceOrder
			--,CAST(EntrySourceDate as date) as EntrySourceDate
			,SUM(EntryAmount) AS ServiceOrderAmount
		FROM
			ConfidentialCustomer.dbo.TBL_CustomerLedger WITH (NOLOCK)
		WHERE
			EntryType IN (100, 200)
			AND NOT ISNULL(EntrySourceConfidence, 0) = 92
		GROUP BY
			ServiceUnit
			,ServiceOrder
			--,CAST(EntrySourceDate as date)
	) o
	ON l.ServiceUnit = o.ServiceUnit
	AND l.ServiceOrder = o.ServiceOrder
	--AND l.EntrySourceDate = o.EntrySourceDate
LEFT JOIN
	(	SELECT
			ServiceUnit
			,ServiceOrder
			--,CAST(EntrySourceDate as date) as EntrySourceDate
			,SUM(EntryAmount) AS ServiceOrderPaidAmount
		FROM
			ConfidentialCustomer.dbo.TBL_CustomerLedger WITH (NOLOCK)
		WHERE
			EntryType IN (200)
		GROUP BY
			ServiceUnit
			,ServiceOrder
			--,CAST(EntrySourceDate as date)
	) pd
	ON l.ServiceUnit = pd.ServiceUnit
	AND l.ServiceOrder = pd.ServiceOrder
	--AND l.EntrySourceDate = pd.EntrySourceDate
JOIN
	(	SELECT 
			u.Unit
			,u.ReportingDistrict
			,d.ReportingRegion
			,d.OwnerLDAPID
			,n.FullName AS GMFullName
		FROM 
			Employee.dbo.TBL_UnitCrossReference u WITH (NOLOCK)
		JOIN
			Employee.dbo.TBL_UnitCrossReference d WITH (NOLOCK)
			ON u.ReportingDistrict = d.Unit
		LEFT JOIN
			Employee.dbo.TBL_Employee_New n with (NOLOCK)
			ON d.OwnerLDAPID = n.EnterpriseID
		WHERE
			u.UnitType IN ('In-Home District', 'HVAC District')
	) u
	ON l.ServiceUnit = u.Unit
LEFT JOIN
	Employee.dbo.TBL_EmployeeHierarchyIHHVACTechRegion e WITH (NOLOCK)
	ON l.EntrySourceCreatedBy = e.EnterpriseID
LEFT JOIN
	Employee.dbo.TBL_Employee_New n with (NOLOCK)
	ON l.EntrySourceCreatedBy = n.EnterpriseID
WHERE
	--l.EntryType = 100 AND 
	f.AccountingYear >= 2018
--	AND f.AccountingMonth = 5
	--AND ABS(ISNULL(l.EntryAmount, 0.0)) <> ABS(ISNULL(pd.EntryAmount, 0.0))
	AND NOT o.ServiceOrderAmount = 0.0
	AND NOT c.CustomerAmount = 0.0
)

SELECT * FROM x
	
	   
/*
SELECT 
	--ServiceUnit,	ServiceOrder,
	CustomerID,
	COUNT(ServiceOrder) as total
FROM 
	x
GROUP BY
	--ServiceUnit,	ServiceOrder
	CustomerID
HAVING 
	COUNT(ServiceOrder) > 1        
	*/
	--AND l.serviceunit = '0009650'
	--and l.serviceorder = '40170368'

	--68,183 records on original report



/*
SELECT 
	u.ReportingRegion AS Region
	,u.ReportingDistrict AS District
	,UPPER(u.OwnerLDAPID) AS GMEnterpriseID
	,UPPER(u.GMFullName) AS GMFullName
	,UPPER(e.TMDTMEnterpriseID) AS TechManagerEnterpriseID
	,UPPER(e.TMDTMFullName) AS TechManagerName
	,UPPER(l.EntrySourceCreatedBy) AS EnterpriseID
	,UPPER(n.FullName) AS TechnicianName
	,f.AccountingYear
	,f.AccountingMonth
	,f.AccountingWeek
	,f.AccountingDate
	,l.CustomerID
	,l.ServiceUnit
	,l.ServiceOrder
	,l.EntrySourceDate AS ServiceCallDate
	,CASE 
		WHEN l.EntrySourceType = 'ServiceOrderPayments' THEN 'NPSXTPM'
		ELSE 'RefuseToPay'
		END AS ChargeType
	,CASE
		WHEN p.EntrySourceType = 'ServiceOrderPayments' THEN 'NPS'
		WHEN p.EntrySourceType = 'FirstDataPayments' THEN 'IVR'
		ELSE p.EntrySourceType
		END AS PaymentType
	,p.EntrySourceDate AS PaymentDate
	,p.EntrySourceConfidence AS PaymentConfidenceLevel
	,l.EntryAmount AS ChargeAmount
	,p.EntryAmount AS PaymentAmount
	,CASE 
		WHEN o.ServiceOrderAmount < 0.0 THEN 'Shortage'
		WHEN o.ServiceOrderAmount = 0.0 THEN 'Balanced'
		ELSE 'Overage'
		END AS ServiceOrderBalance
	,CASE 
		WHEN c.CustomerAmount < 0.0 THEN 'Shortage'
		WHEN c.CustomerAmount = 0.0 THEN 'Balanced'
		ELSE 'Overage'
		END AS CustomerBalance
	,s.ServiceOrderStatusCode
	,s.ServiceOrderStatusDate
	,l.EntryID 
--	,l.EntrySourceID
--	,l.OffsetEntryID
--	,l.BillID
--	,b.BillStatus
--	,b.LastBillDate
--	,r.RefundAmount AS ServiceOrderRefundAmount
FROM 
	ConfidentialCustomer.dbo.TBL_CustomerLedger l WITH (NOLOCK)
JOIN
	ServiceOrder.dbo.TBL_FiscalCalendar f WITH (NOLOCK)
	ON CAST(l.EntrySourceDate AS DATE) = f.AccountingDate
JOIN
	(	SELECT
			ServiceUnit
			,ServiceOrder
			,ServiceOrderStatusCode
			,ServiceOrderStatusDate
		FROM
			ServiceOrder.dbo.TBL_ServiceOrder WITH (NOLOCK)
	) s
	ON l.ServiceUnit = s.ServiceUnit
	AND l.ServiceOrder = s.ServiceOrder
JOIN
	(	SELECT
			CustomerID
			,SUM(EntryAmount) AS CustomerAmount
		FROM
			ConfidentialCustomer.dbo.TBL_CustomerLedger WITH (NOLOCK)
		WHERE
			EntryType IN (100, 200)
		GROUP BY
			CustomerID
	) c
	ON l.CustomerID = c.CustomerID
JOIN
	(	SELECT
			ServiceUnit
			,ServiceOrder
			,SUM(EntryAmount) AS ServiceOrderAmount
		FROM
			ConfidentialCustomer.dbo.TBL_CustomerLedger WITH (NOLOCK)
		WHERE
			EntryType IN (100, 200)
		GROUP BY
			ServiceUnit
			,ServiceOrder
	) o
	ON l.ServiceUnit = o.ServiceUnit
	AND l.ServiceOrder = o.ServiceOrder
LEFT JOIN
	ConfidentialCustomer.dbo.TBL_CustomerLedger p WITH (NOLOCK)
	ON l.OffsetEntryID = p.EntryID
/*
LEFT JOIN
	(	SELECT
			b.*
			,l.Description AS BillStatus
		FROM	
			ConfidentialCustomer.dbo.TBL_CustomerBills b WITH (NOLOCK)
		JOIN
			(	SELECT
					*
				FROM
					ConfidentialCustomer.dbo.TBL_LookupCodes WITH (NOLOCK)
				WHERE
					OwnerType = 'CustomerBills'
					AND OwnerID = 'StatusID'
			) l
			ON b.StatusID = l.NumericValue
	) b
	ON l.BillID = b.EntryID
LEFT JOIN
	(	SELECT
			ServiceUnit
			,ServiceOrder
			,SUM(EntryAmount) AS RefundAmount
		FROM
			ConfidentialCustomer.dbo.TBL_CustomerLedger WITH (NOLOCK)
		WHERE
			EntryType IN (400)
		GROUP BY
			ServiceUnit
			,ServiceOrder
	) r
	ON l.ServiceUnit = r.ServiceUnit
	AND l.ServiceOrder = r.ServiceOrder
*/
JOIN
	(	SELECT 
			u.Unit
			,u.ReportingDistrict
			,d.ReportingRegion
			,d.OwnerLDAPID
			,n.FullName AS GMFullName
		FROM 
			Employee.dbo.TBL_UnitCrossReference u WITH (NOLOCK)
		JOIN
			Employee.dbo.TBL_UnitCrossReference d WITH (NOLOCK)
			ON u.ReportingDistrict = d.Unit
		LEFT JOIN
			Employee.dbo.TBL_Employee_New n with (NOLOCK)
			ON d.OwnerLDAPID = n.EnterpriseID
		WHERE
			u.UnitType IN ('In-Home District', 'HVAC District')
	) u
	ON l.ServiceUnit = u.Unit
LEFT JOIN
	Employee.dbo.TBL_EmployeeHierarchyIHHVACTechRegion e WITH (NOLOCK)
	ON l.EntrySourceCreatedBy = e.EnterpriseID
LEFT JOIN
	Employee.dbo.TBL_Employee_New n with (NOLOCK)
	ON l.EntrySourceCreatedBy = n.EnterpriseID
WHERE
	l.EntryType = 100
	AND f.AccountingYear = 2018
--	AND f.AccountingMonth = 5
	AND ABS(ISNULL(l.EntryAmount, 0.0)) <> ABS(ISNULL(p.EntryAmount, 0.0))
	AND NOT o.ServiceOrderAmount = 0.0
	AND NOT c.CustomerAmount = 0.0

*/




GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_LookupCodes_OwnerID_OwnerType]    Script Date: 9/18/2019 7:23:26 PM ******/
CREATE CLUSTERED INDEX [IX_TBL_LookupCodes_OwnerID_OwnerType] ON [dbo].[TBL_LookupCodes]
(
	[OwnerID] ASC,
	[OwnerType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_Actions_ActionID_ActionType_V2]    Script Date: 9/18/2019 7:23:26 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_TBL_Actions_ActionID_ActionType_V2] ON [dbo].[TBL_Actions]
(
	[ActionID] ASC,
	[ActionType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_Actions_OwnerID_OwnerType_CreateDateTime_V2]    Script Date: 9/18/2019 7:23:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_Actions_OwnerID_OwnerType_CreateDateTime_V2] ON [dbo].[TBL_Actions]
(
	[OwnerID] ASC,
	[OwnerType] ASC,
	[CreatedDateTime] ASC
)
INCLUDE ( 	[ActionID],
	[ActionType],
	[SecondaryID],
	[SecondaryType],
	[StatusID],
	[CreatedEnterpriseID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_Coaching_V2_OwnerID_OwnerType]    Script Date: 9/18/2019 7:23:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_Coaching_V2_OwnerID_OwnerType] ON [dbo].[TBL_Coaching]
(
	[OwnerID] ASC,
	[OwnerType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_Comments_OwnerID_OwnerType]    Script Date: 9/18/2019 7:23:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_Comments_OwnerID_OwnerType] ON [dbo].[TBL_Comments]
(
	[OwnerID] ASC,
	[OwnerType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_Comments_SecondaryID_SecondaryType]    Script Date: 9/18/2019 7:23:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_Comments_SecondaryID_SecondaryType] ON [dbo].[TBL_Comments]
(
	[SecondaryType] ASC,
	[SecondaryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_CustomerContactOverride_OwnerID_OwnerType]    Script Date: 9/18/2019 7:23:26 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_TBL_CustomerContactOverride_OwnerID_OwnerType] ON [dbo].[TBL_CustomerContactOverride]
(
	[OwnerID] ASC,
	[OwnerType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TBL_CustomerLedger_CustomerID]    Script Date: 9/18/2019 7:23:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_CustomerLedger_CustomerID] ON [dbo].[TBL_CustomerLedger]
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_CustomerLedger_ServiceUnitServiceOrder]    Script Date: 9/18/2019 7:23:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_CustomerLedger_ServiceUnitServiceOrder] ON [dbo].[TBL_CustomerLedger]
(
	[ServiceOrder] ASC,
	[ServiceUnit] ASC
)
INCLUDE ( 	[BillType],
	[BillID],
	[EntryAmount],
	[EntryID],
	[EntryType]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_FirstDataPayments_EntryID_EntryType]    Script Date: 9/18/2019 7:23:26 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_TBL_FirstDataPayments_EntryID_EntryType] ON [dbo].[TBL_FirstDataPayments]
(
	[EntryID] ASC,
	[EntryType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_FirstDataPayments_ServiceUnitServiceOrder]    Script Date: 9/18/2019 7:23:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_FirstDataPayments_ServiceUnitServiceOrder] ON [dbo].[TBL_FirstDataPayments]
(
	[ServiceOrder] ASC,
	[ServiceUnit] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_HSPOS_ServiceUnitServiceOrder]    Script Date: 9/18/2019 7:23:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_HSPOS_ServiceUnitServiceOrder] ON [dbo].[TBL_HSPOS]
(
	[ServiceOrder] ASC,
	[ServiceUnit] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_PaymentMatch_OwnerType_OwnerID]    Script Date: 9/18/2019 7:23:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_PaymentMatch_OwnerType_OwnerID] ON [dbo].[TBL_PaymentMatch]
(
	[OwnerType] ASC,
	[OwnerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_PaymentMatch_V2_ActionID_ActionType]    Script Date: 9/18/2019 7:23:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_PaymentMatch_V2_ActionID_ActionType] ON [dbo].[TBL_PaymentMatch]
(
	[ActionID] ASC,
	[ActionType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_PNCCheck_EntryID_EntryType]    Script Date: 9/18/2019 7:23:26 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_TBL_PNCCheck_EntryID_EntryType] ON [dbo].[TBL_PNCCheck]
(
	[EntryID] ASC,
	[EntryType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_RefuseToPay_ServiceUnitServiceOrder]    Script Date: 9/18/2019 7:23:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_RefuseToPay_ServiceUnitServiceOrder] ON [dbo].[TBL_RefuseToPay]
(
	[ServiceOrder] ASC,
	[ServiceUnit] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_RefuseToPay_V2_ActionID_ActionType]    Script Date: 9/18/2019 7:23:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_RefuseToPay_V2_ActionID_ActionType] ON [dbo].[TBL_RefuseToPay]
(
	[ActionID] ASC,
	[ActionType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_RefuseToPay_V2_EntryID_EntryType]    Script Date: 9/18/2019 7:23:26 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_TBL_RefuseToPay_V2_EntryID_EntryType] ON [dbo].[TBL_RefuseToPay]
(
	[EntryID] ASC,
	[EntryType] ASC
)
INCLUDE ( 	[ServiceUnit],
	[ServiceOrder],
	[ServiceCallDate],
	[ActionID],
	[ActionType],
	[StatusID],
	[CustomerID],
	[PartsDiscountReason],
	[LaborDiscountReason],
	[OriginalPaidAmount],
	[OriginalPartsAmount],
	[OriginalLaborAmount],
	[OriginalAmountWithTax],
	[AdjustedPartsAmount],
	[AdjustedLaborAmount],
	[AdjustedAmountWithTax],
	[PartsTaxRate],
	[LaborTaxRate],
	[ReasonCode],
	[OriginalLedgerEntryID],
	[CurrentLedgerEntryID],
	[CreatedEnterpriseID],
	[CreatedDateTime],
	[LastModifiedEnterpriseID],
	[LastModifiedDateTime]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_Requests_RequestType_StatusID]    Script Date: 9/18/2019 7:23:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_Requests_RequestType_StatusID] ON [dbo].[TBL_Requests]
(
	[RequestType] ASC,
	[ServiceUnit] ASC,
	[ServiceOrder] ASC,
	[CurrentLedgerEntryID] ASC,
	[StatusID] ASC
)
INCLUDE ( 	[RequestPartsAmount],
	[RequestLaborAmount],
	[RequestAmount]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_ServiceOrderPayments_EntryID_EntryType]    Script Date: 9/18/2019 7:23:26 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_TBL_ServiceOrderPayments_EntryID_EntryType] ON [dbo].[TBL_ServiceOrderPayments]
(
	[EntryID] ASC,
	[EntryType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_ServiceOrderPayments_ServiceUnitServiceOrder]    Script Date: 9/18/2019 7:23:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_ServiceOrderPayments_ServiceUnitServiceOrder] ON [dbo].[TBL_ServiceOrderPayments]
(
	[ServiceOrder] ASC,
	[ServiceUnit] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_TechnicianOrderUpload_V2_EntryID_EntryType]    Script Date: 9/18/2019 7:23:26 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_TBL_TechnicianOrderUpload_V2_EntryID_EntryType] ON [dbo].[TBL_TechnicianOrderUpload]
(
	[EntryID] ASC,
	[EntryType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_Telecheck_EntryID_EntryType]    Script Date: 9/18/2019 7:23:26 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_TBL_Telecheck_EntryID_EntryType] ON [dbo].[TBL_Telecheck]
(
	[EntryID] ASC,
	[EntryType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_Telecheck_ServiceUnitServiceOrder]    Script Date: 9/18/2019 7:23:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_Telecheck_ServiceUnitServiceOrder] ON [dbo].[TBL_Telecheck]
(
	[ServiceOrder] ASC,
	[ServiceUnit] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_TempusCard_ServiceUnitServiceOrder]    Script Date: 9/18/2019 7:23:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_TempusCard_ServiceUnitServiceOrder] ON [dbo].[TBL_TempusCard]
(
	[ServiceOrder] ASC,
	[ServiceUnit] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UIX_TBL_TempusCard_EntryID_EntryType]    Script Date: 9/18/2019 7:23:26 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UIX_TBL_TempusCard_EntryID_EntryType] ON [dbo].[TBL_TempusCard]
(
	[EntryID] ASC,
	[EntryType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_TempusCheck_EntryID_EntryType]    Script Date: 9/18/2019 7:23:26 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_TBL_TempusCheck_EntryID_EntryType] ON [dbo].[TBL_TempusCheck]
(
	[EntryID] ASC,
	[EntryType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_TempusCheck_ServiceUnitServiceOrder]    Script Date: 9/18/2019 7:23:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_TempusCheck_ServiceUnitServiceOrder] ON [dbo].[TBL_TempusCheck]
(
	[ServiceOrder] ASC,
	[ServiceUnit] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[STG_TempusCard] ADD  CONSTRAINT [DF_STG_TempusCard_EntryType]  DEFAULT ('TempusCard') FOR [EntryType]
GO
ALTER TABLE [dbo].[STG_TempusCard] ADD  CONSTRAINT [DF_STG_TempusCard_CreatedEnterpriseID]  DEFAULT ('SYSTEM') FOR [CreatedEnterpriseID]
GO
ALTER TABLE [dbo].[STG_TempusCard] ADD  CONSTRAINT [DF_STG_TempusCard_CreatedDateTime]  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[STG_TempusCard] ADD  CONSTRAINT [DF_STG_TempusCard_LastModifiedEnterpriseID]  DEFAULT ('SYSTEM') FOR [LastModifiedEnterpriseID]
GO
ALTER TABLE [dbo].[STG_TempusCard] ADD  CONSTRAINT [DF_STG_TempusCard_LastModifiedDateTime]  DEFAULT (getdate()) FOR [LastModifiedDateTime]
GO
ALTER TABLE [dbo].[TBL_Actions] ADD  CONSTRAINT [DF_TBL_Actions_ActionType_V2]  DEFAULT ('Actions') FOR [ActionType]
GO
ALTER TABLE [dbo].[TBL_Actions] ADD  CONSTRAINT [DF_TBL_Actions_CreatedEnterpriseID_V2]  DEFAULT ('SYSTEM') FOR [CreatedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_Actions] ADD  CONSTRAINT [DF_TBL_Actions_CreatedDateTime_V2]  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[TBL_BillType] ADD  CONSTRAINT [DF_TBL_BillType_EntryType]  DEFAULT ('BillType') FOR [EntryType]
GO
ALTER TABLE [dbo].[TBL_BillType] ADD  CONSTRAINT [DF_TBL_BillType_CreatedEnterpriseID]  DEFAULT ('SYSTEM') FOR [CreatedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_BillType] ADD  CONSTRAINT [DF_TBL_BillType_CreatedDateTime]  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[TBL_BillType] ADD  CONSTRAINT [DF_TBL_BillType_LastModifiedEnterpriseID]  DEFAULT ('SYSTEM') FOR [LastModifiedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_BillType] ADD  CONSTRAINT [DF_TBL_BillType_LastModifiedDateTime]  DEFAULT (getdate()) FOR [LastModifiedDateTime]
GO
ALTER TABLE [dbo].[TBL_Coaching] ADD  CONSTRAINT [DF_TBL_Coaching_V2_EntryType]  DEFAULT ('Coaching') FOR [EntryType]
GO
ALTER TABLE [dbo].[TBL_Coaching] ADD  CONSTRAINT [DF_TBL_Coaching_V2_CreatedEnterpriseID]  DEFAULT ('SYSTEM') FOR [CreatedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_Coaching] ADD  CONSTRAINT [DF_TBL_Coaching_V2_CreatedDateTime]  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[TBL_Comments] ADD  CONSTRAINT [DF_TBL_Comments_IsPrivate]  DEFAULT ((0)) FOR [IsPrivate]
GO
ALTER TABLE [dbo].[TBL_Comments] ADD  CONSTRAINT [DF_TBL_Comments_CreatedEnterpriseID]  DEFAULT ('SYSTEM') FOR [CreatedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_Comments] ADD  CONSTRAINT [DF_TBL_Comments_CreatedDateTime]  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[TBL_Comments] ADD  CONSTRAINT [DF_TBL_Comments_LastModifiedEnterpriseID]  DEFAULT ('SYSTEM') FOR [LastModifiedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_Comments] ADD  CONSTRAINT [DF_TBL_Comments_LastModifiedDateTime]  DEFAULT (getdate()) FOR [LastModifiedDateTime]
GO
ALTER TABLE [dbo].[TBL_CreditChargebacks] ADD  CONSTRAINT [DF_TBL_CreditChargebacks_EntryType]  DEFAULT ('CreditChargebacks') FOR [EntryType]
GO
ALTER TABLE [dbo].[TBL_CreditChargebacks] ADD  CONSTRAINT [DF_TBL_CreditChargebacks_CreatedEnterpriseID]  DEFAULT ('SYSTEM') FOR [CreatedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_CreditChargebacks] ADD  CONSTRAINT [DF_TBL_CreditChargebacks_CreatedDateTime]  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[TBL_CreditChargebacks] ADD  CONSTRAINT [DF_TBL_CreditChargebacks_LastModifiedEnterpriseID]  DEFAULT ('SYSTEM') FOR [LastModifiedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_CreditChargebacks] ADD  CONSTRAINT [DF_TBL_CreditChargebacks_LastModifiedDateTime]  DEFAULT (getdate()) FOR [LastModifiedDateTime]
GO
ALTER TABLE [dbo].[TBL_CustomerBills] ADD  CONSTRAINT [DF_TBL_CustomerBills_EntryType]  DEFAULT ('CustomerBills') FOR [EntryType]
GO
ALTER TABLE [dbo].[TBL_CustomerBills] ADD  CONSTRAINT [DF_TBL_CustomerBills_ActionType]  DEFAULT ('Actions') FOR [ActionType]
GO
ALTER TABLE [dbo].[TBL_CustomerBills] ADD  CONSTRAINT [DF_TBL_CustomerBills_CreatedEnterpriseID]  DEFAULT ('SYSTEM') FOR [CreatedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_CustomerBills] ADD  CONSTRAINT [DF_TBL_CustomerBills_CreatedDateTime]  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[TBL_CustomerBills] ADD  CONSTRAINT [DF_TBL_CustomerBills_LastModifiedEnterpriseID]  DEFAULT ('SYSTEM') FOR [LastModifiedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_CustomerBills] ADD  CONSTRAINT [DF_TBL_CustomerBills_LastModifiedDateTime]  DEFAULT (getdate()) FOR [LastModifiedDateTime]
GO
ALTER TABLE [dbo].[TBL_CustomerBillsEntryDetail] ADD  CONSTRAINT [DF_TBL_CustomerBillsEntryDetail_EntryType]  DEFAULT ('CustomerBillsEntryDetail') FOR [EntryType]
GO
ALTER TABLE [dbo].[TBL_CustomerBillsEntryDetail] ADD  CONSTRAINT [DF_TBL_CustomerBillsEntryDetail_CreatedEnterpriseID]  DEFAULT ('SYSTEM') FOR [CreatedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_CustomerBillsEntryDetail] ADD  CONSTRAINT [DF_TBL_CustomerBillsEntryDetail_CreatedDateTime]  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[TBL_CustomerBillsEntryDetail] ADD  CONSTRAINT [DF_TBL_CustomerBillsEntryDetail_LastModifiedEnterpriseID]  DEFAULT ('SYSTEM') FOR [LastModifiedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_CustomerBillsEntryDetail] ADD  CONSTRAINT [DF_TBL_CustomerBillsEntryDetail_LastModifiedDateTime]  DEFAULT (getdate()) FOR [LastModifiedDateTime]
GO
ALTER TABLE [dbo].[TBL_CustomerContactOverride] ADD  CONSTRAINT [DF_TBL_CustomerContactOverride_EntryType]  DEFAULT ('CustomerContactOverride') FOR [EntryType]
GO
ALTER TABLE [dbo].[TBL_CustomerContactOverride] ADD  CONSTRAINT [DF_TBL_CustomerContactOverride_PreferenceID]  DEFAULT ((0)) FOR [PreferenceID]
GO
ALTER TABLE [dbo].[TBL_CustomerContactOverride] ADD  CONSTRAINT [DF_TBL_CustomerContactOverride_CreatedEnterpriseID]  DEFAULT ('SYSTEM') FOR [CreatedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_CustomerContactOverride] ADD  CONSTRAINT [DF_TBL_CustomerContactOverride_CreatedDateTime]  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[TBL_CustomerLedger] ADD  CONSTRAINT [DF_TBL_CustomerLedger_CreatedEnterpriseID]  DEFAULT ('SYSTEM') FOR [CreatedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_CustomerLedger] ADD  CONSTRAINT [DF_TBL_CustomerLedger_CreatedDateTime]  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[TBL_CustomerLedger] ADD  CONSTRAINT [DF_TBL_CustomerLedger_LastModifiedEnterpriseID]  DEFAULT ('SYSTEM') FOR [LastModifiedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_CustomerLedger] ADD  CONSTRAINT [DF_TBL_CustomerLedger_LastModifiedDateTime]  DEFAULT (getdate()) FOR [LastModifiedDateTime]
GO
ALTER TABLE [dbo].[TBL_CustomerLedgerOffsets] ADD  CONSTRAINT [DF_TBL_CustomerLedgerOffsets_CreatedEnterpriseID]  DEFAULT ('SYSTEM') FOR [CreatedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_CustomerLedgerOffsets] ADD  CONSTRAINT [DF_TBL_CustomerLedgerOffsets_CreatedDateTime]  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[TBL_CustomerLedgerOffsets] ADD  CONSTRAINT [DF_TBL_CustomerLedgerOffsets_LastModifiedEnterpriseID]  DEFAULT ('SYSTEM') FOR [LastModifiedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_CustomerLedgerOffsets] ADD  CONSTRAINT [DF_TBL_CustomerLedgerOffsets_LastModifiedDateTime]  DEFAULT (getdate()) FOR [LastModifiedDateTime]
GO
ALTER TABLE [dbo].[TBL_EssbaseAccounts] ADD  CONSTRAINT [DF_TBL_EssbaseAccounts_EntryType]  DEFAULT ('EssbaseAccounts') FOR [EntryType]
GO
ALTER TABLE [dbo].[TBL_FirstDataPayments] ADD  CONSTRAINT [DF_TBL_FirstDataPayments_EntryType]  DEFAULT ('FirstDataPayments') FOR [EntryType]
GO
ALTER TABLE [dbo].[TBL_FirstDataPayments] ADD  CONSTRAINT [DF_TBL_FirstDataPayments_CreatedEnterpriseID]  DEFAULT ('SYSTEM') FOR [CreatedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_FirstDataPayments] ADD  CONSTRAINT [DF_TBL_FirstDataPayments_CreatedDateTime]  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[TBL_FirstDataPayments] ADD  CONSTRAINT [DF_TBL_FirstDataPayments_LastModifiedEnterpriseID]  DEFAULT ('SYSTEM') FOR [LastModifiedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_FirstDataPayments] ADD  CONSTRAINT [DF_TBL_FirstDataPayments_LastModifiedDateTime]  DEFAULT (getdate()) FOR [LastModifiedDateTime]
GO
ALTER TABLE [dbo].[TBL_HSPOS] ADD  CONSTRAINT [DF_TBL_HSPOS_EntryType]  DEFAULT ('HSPOS') FOR [EntryType]
GO
ALTER TABLE [dbo].[TBL_HSPOS] ADD  CONSTRAINT [DF_TBL_HSPOS_CreatedEnterpriseID]  DEFAULT ('SYSTEM') FOR [CreatedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_HSPOS] ADD  CONSTRAINT [DF_TBL_HSPOS_CreatedDateTime]  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[TBL_HSPOS] ADD  CONSTRAINT [DF_TBL_HSPOS_LastModifiedEnterpriseID]  DEFAULT ('SYSTEM') FOR [LastModifiedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_HSPOS] ADD  CONSTRAINT [DF_TBL_HSPOS_LastModifiedDateTime]  DEFAULT (getdate()) FOR [LastModifiedDateTime]
GO
ALTER TABLE [dbo].[TBL_PaymentMatch] ADD  CONSTRAINT [DF_TBL_PaymentMatch_V2_EntryType]  DEFAULT ('PaymentMatch') FOR [EntryType]
GO
ALTER TABLE [dbo].[TBL_PaymentMatch] ADD  CONSTRAINT [DF_TBL_PaymentMatch_V2_CreatedEnterpriseID]  DEFAULT ('SYSTEM') FOR [CreatedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_PaymentMatch] ADD  CONSTRAINT [DF_TBL_PaymentMatch_V2_CreatedDateTime]  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[TBL_PaymentMatch] ADD  CONSTRAINT [DF_TBL_PaymentMatch_V2_LastModifiedEnterpriseID]  DEFAULT ('SYSTEM') FOR [LastModifiedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_PaymentMatch] ADD  CONSTRAINT [DF_TBL_PaymentMatch_V2_LastModifiedDateTime]  DEFAULT (getdate()) FOR [LastModifiedDateTime]
GO
ALTER TABLE [dbo].[TBL_PaymentMatch] ADD  CONSTRAINT [DF_TBL_PaymentMatch_V2_ActionType]  DEFAULT ('Actions') FOR [ActionType]
GO
ALTER TABLE [dbo].[TBL_PNCCheck] ADD  CONSTRAINT [DF_TBL_PNCCheck_EntryOwner]  DEFAULT ('PNCCheck') FOR [EntryType]
GO
ALTER TABLE [dbo].[TBL_PNCCheck] ADD  CONSTRAINT [DF_TBL_PNCCheck_CreatedEnterpriseID]  DEFAULT ('SYSTEM') FOR [CreatedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_PNCCheck] ADD  CONSTRAINT [DF_TBL_PNCCheck_CreatedDateTime]  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[TBL_PNCCheck] ADD  CONSTRAINT [DF_TBL_PNCCheck_LastModifiedEnterpriseID]  DEFAULT ('SYSTEM') FOR [LastModifiedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_PNCCheck] ADD  CONSTRAINT [DF_TBL_PNCCheck_LastModifiedDateTime]  DEFAULT (getdate()) FOR [LastModifiedDateTime]
GO
ALTER TABLE [dbo].[TBL_RefuseToPay] ADD  CONSTRAINT [DF_TBL_RefuseToPay_V2_EntryType]  DEFAULT ('RefuseToPay') FOR [EntryType]
GO
ALTER TABLE [dbo].[TBL_RefuseToPay] ADD  CONSTRAINT [DF_TBL_RefuseToPay_V2_ActionType]  DEFAULT ('Actions') FOR [ActionType]
GO
ALTER TABLE [dbo].[TBL_RefuseToPay] ADD  CONSTRAINT [DF_TBL_RefuseToPay_V2_OriginalPaidAmount]  DEFAULT ((0.00)) FOR [OriginalPaidAmount]
GO
ALTER TABLE [dbo].[TBL_RefuseToPay] ADD  CONSTRAINT [DF_TBL_RefuseToPay_V2_OriginalPartsAmount]  DEFAULT ((0.00)) FOR [OriginalPartsAmount]
GO
ALTER TABLE [dbo].[TBL_RefuseToPay] ADD  CONSTRAINT [DF_TBL_RefuseToPay_V2_OriginalLaborAmount]  DEFAULT ((0.00)) FOR [OriginalLaborAmount]
GO
ALTER TABLE [dbo].[TBL_RefuseToPay] ADD  CONSTRAINT [DF_TBL_RefuseToPay_V2_OriginalAmountWithTax]  DEFAULT ((0.00)) FOR [OriginalAmountWithTax]
GO
ALTER TABLE [dbo].[TBL_RefuseToPay] ADD  CONSTRAINT [DF_TBL_RefuseToPay_V2_AdjustedPartsAmount]  DEFAULT ((0.00)) FOR [AdjustedPartsAmount]
GO
ALTER TABLE [dbo].[TBL_RefuseToPay] ADD  CONSTRAINT [DF_TBL_RefuseToPay_V2_AdjustedLaborAmount]  DEFAULT ((0.00)) FOR [AdjustedLaborAmount]
GO
ALTER TABLE [dbo].[TBL_RefuseToPay] ADD  CONSTRAINT [DF_TBL_RefuseToPay_V2_AdjustedAmountWithTax]  DEFAULT ((0.00)) FOR [AdjustedAmountWithTax]
GO
ALTER TABLE [dbo].[TBL_RefuseToPay] ADD  CONSTRAINT [DF_TBL_RefuseToPay_V2_CreatedEnterpriseID]  DEFAULT ('SYSTEM') FOR [CreatedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_RefuseToPay] ADD  CONSTRAINT [DF_TBL_RefuseToPay_V2_CreatedDateTime]  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[TBL_RefuseToPay] ADD  CONSTRAINT [DF_TBL_RefuseToPay_V2_LastModifiedEnterpriseID]  DEFAULT ('SYSTEM') FOR [LastModifiedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_RefuseToPay] ADD  CONSTRAINT [DF_TBL_RefuseToPay_V2_LastModifiedDateTime]  DEFAULT (getdate()) FOR [LastModifiedDateTime]
GO
ALTER TABLE [dbo].[TBL_RefuseToPay] ADD  CONSTRAINT [DF_TBL_RefuseToPay_AdjustedDeductableAmount]  DEFAULT ((0.0000)) FOR [AdjustedDeductableAmount]
GO
ALTER TABLE [dbo].[TBL_Requests] ADD  CONSTRAINT [DF_TBL_RequestsActionType]  DEFAULT ('Actions') FOR [ActionType]
GO
ALTER TABLE [dbo].[TBL_Requests] ADD  CONSTRAINT [DF_TBL_RequestsRequestPartsAmount]  DEFAULT ((0.00)) FOR [RequestPartsAmount]
GO
ALTER TABLE [dbo].[TBL_Requests] ADD  CONSTRAINT [DF_TBL_RequestsRequestLaborAmount]  DEFAULT ((0.00)) FOR [RequestLaborAmount]
GO
ALTER TABLE [dbo].[TBL_Requests] ADD  CONSTRAINT [DF_TBL_RequestsRequestAmount]  DEFAULT ((0.00)) FOR [RequestAmount]
GO
ALTER TABLE [dbo].[TBL_Requests] ADD  CONSTRAINT [DF_TBL_RequestsApprovedPartsAmount]  DEFAULT ((0.00)) FOR [ApprovedPartsAmount]
GO
ALTER TABLE [dbo].[TBL_Requests] ADD  CONSTRAINT [DF_TBL_RequestsApprovedLaborAmount]  DEFAULT ((0.00)) FOR [ApprovedLaborAmount]
GO
ALTER TABLE [dbo].[TBL_Requests] ADD  CONSTRAINT [DF_TBL_RequestsApprovedAmount]  DEFAULT ((0.00)) FOR [ApprovedAmount]
GO
ALTER TABLE [dbo].[TBL_Requests] ADD  CONSTRAINT [DF_TBL_RequestsCreatedEnterpriseID]  DEFAULT ('SYSTEM') FOR [CreatedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_Requests] ADD  CONSTRAINT [DF_TBL_RequestsCreatedDateTime]  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[TBL_Requests] ADD  CONSTRAINT [DF_TBL_RequestsLastModifiedEnterpriseID]  DEFAULT ('SYSTEM') FOR [LastModifiedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_Requests] ADD  CONSTRAINT [DF_TBL_RequestsLastModifiedDateTime]  DEFAULT (getdate()) FOR [LastModifiedDateTime]
GO
ALTER TABLE [dbo].[TBL_ServiceOrderPayments] ADD  CONSTRAINT [DF_TBL_ServiceOrderPayments_EntryType]  DEFAULT ('ServiceOrderPayments') FOR [EntryType]
GO
ALTER TABLE [dbo].[TBL_ServiceOrderPayments] ADD  CONSTRAINT [DF_TBL_ServiceOrderPayments_InsertDate]  DEFAULT (getdate()) FOR [InsertDate]
GO
ALTER TABLE [dbo].[TBL_TechnicianOrderUpload] ADD  CONSTRAINT [DF_TBL_TBL_TechnicianOrderUpload_V2_EntryType]  DEFAULT ('SST') FOR [EntryType]
GO
ALTER TABLE [dbo].[TBL_TechnicianOrderUpload] ADD  CONSTRAINT [DF_TBL_TechnicianOrderUpload_V2_CreatedEnterpriseID]  DEFAULT ('SYSTEM') FOR [CreatedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_TechnicianOrderUpload] ADD  CONSTRAINT [DF_TBL_TechnicianOrderUpload_V2_CreatedDateTime]  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[TBL_TechnicianOrderUpload] ADD  CONSTRAINT [DF_TBL_TechnicianOrderUpload_V2_LastModifiedEnterpriseID]  DEFAULT ('SYSTEM') FOR [LastModifiedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_TechnicianOrderUpload] ADD  CONSTRAINT [DF_TBL_TechnicianOrderUpload_V2_LastModifiedDateTime]  DEFAULT (getdate()) FOR [LastModifiedDateTime]
GO
ALTER TABLE [dbo].[TBL_Telecheck] ADD  CONSTRAINT [DF_TBL_Telecheck_EntryOwner]  DEFAULT ('Telecheck') FOR [EntryType]
GO
ALTER TABLE [dbo].[TBL_Telecheck] ADD  CONSTRAINT [DF_TBL_Telecheck_CreatedEnterpriseID]  DEFAULT ('SYSTEM') FOR [CreatedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_Telecheck] ADD  CONSTRAINT [DF_TBL_Telecheck_CreatedDateTime]  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[TBL_Telecheck] ADD  CONSTRAINT [DF_TBL_Telecheck_LastModifiedEnterpriseID]  DEFAULT ('SYSTEM') FOR [LastModifiedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_Telecheck] ADD  CONSTRAINT [DF_TBL_Telecheck_LastModifiedDateTime]  DEFAULT (getdate()) FOR [LastModifiedDateTime]
GO
ALTER TABLE [dbo].[TBL_TempusCard] ADD  CONSTRAINT [DF_TBL_TempusCard_EntryType]  DEFAULT ('TempusCard') FOR [EntryType]
GO
ALTER TABLE [dbo].[TBL_TempusCard] ADD  CONSTRAINT [DF_TBL_TempusCard_CreatedEnterpriseID]  DEFAULT ('SYSTEM') FOR [CreatedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_TempusCard] ADD  CONSTRAINT [DF_TBL_TempusCard_CreatedDateTime]  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[TBL_TempusCard] ADD  CONSTRAINT [DF_TBL_TempusCard_LastModifiedEnterpriseID]  DEFAULT ('SYSTEM') FOR [LastModifiedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_TempusCard] ADD  CONSTRAINT [DF_TBL_TempusCard_LastModifiedDateTime]  DEFAULT (getdate()) FOR [LastModifiedDateTime]
GO
ALTER TABLE [dbo].[TBL_TempusCheck] ADD  CONSTRAINT [DF_TBL_TempusCheck_EntryType]  DEFAULT ('TempusCheck') FOR [EntryType]
GO
ALTER TABLE [dbo].[TBL_TempusCheck] ADD  CONSTRAINT [DF_TBL_TempusCheck_CreatedEnterpriseID]  DEFAULT ('SYSTEM') FOR [CreatedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_TempusCheck] ADD  CONSTRAINT [DF_TBL_TempusCheck_CreatedDateTime]  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[TBL_TempusCheck] ADD  CONSTRAINT [DF_TBL_TempusCheck_LastModifiedEnterpriseID]  DEFAULT ('SYSTEM') FOR [LastModifiedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_TempusCheck] ADD  CONSTRAINT [DF_TBL_TempusCheck_LastModifiedDateTime]  DEFAULT (getdate()) FOR [LastModifiedDateTime]
GO
ALTER TABLE [dbo].[TBL_TempusUnitReference] ADD  CONSTRAINT [DF_TBL_TempusUnitReference_EntryType]  DEFAULT ('TempusUnitReference') FOR [EntryType]
GO
ALTER TABLE [dbo].[TBL_TempusUnitReference] ADD  CONSTRAINT [DF_TBL_TempusUnitReference_CreatedEnterpriseID]  DEFAULT ('SYSTEM') FOR [CreatedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_TempusUnitReference] ADD  CONSTRAINT [DF_TBL_TempusUnitReference_CreatedDateTime]  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[TBL_TempusUnitReference] ADD  CONSTRAINT [DF_TBL_TempusUnitReference_LastModifiedEnterpriseID]  DEFAULT ('SYSTEM') FOR [LastModifiedEnterpriseID]
GO
ALTER TABLE [dbo].[TBL_TempusUnitReference] ADD  CONSTRAINT [DF_TBL_TempusUnitReference_LastModifiedDateTime]  DEFAULT (getdate()) FOR [LastModifiedDateTime]
GO
USE [master]
GO
ALTER DATABASE [ConfidentialCustomer] SET  READ_WRITE 
GO
