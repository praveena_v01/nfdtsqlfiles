USE [master]
GO
/****** Object:  Database [Employee]    Script Date: 7/15/2019 10:32:20 AM ******/
CREATE DATABASE [Employee]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Employee', FILENAME = N'D:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\Employee.mdf' , SIZE = 66867200KB , MAXSIZE = UNLIMITED, FILEGROWTH = 256000KB )
 LOG ON 
( NAME = N'emplopyee_log2', FILENAME = N'F:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\Data\emplopyee_log2.ldf' , SIZE = 512000KB , MAXSIZE = 2048GB , FILEGROWTH = 256000KB ), 
( NAME = N'Employee_log', FILENAME = N'E:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\Data\Employee_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 524288KB )
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Employee].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Employee] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Employee] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Employee] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Employee] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Employee] SET ARITHABORT OFF 
GO
ALTER DATABASE [Employee] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Employee] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [Employee] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Employee] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Employee] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Employee] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Employee] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Employee] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Employee] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Employee] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Employee] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Employee] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Employee] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Employee] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Employee] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Employee] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Employee] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Employee] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Employee] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Employee] SET  MULTI_USER 
GO
ALTER DATABASE [Employee] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Employee] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Employee] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Employee] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [Employee]
GO
/****** Object:  User [SEARS2\delderk]    Script Date: 7/15/2019 10:32:22 AM ******/
CREATE USER [SEARS2\delderk] FOR LOGIN [SEARS2\delderk] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [os_spt]    Script Date: 7/15/2019 10:32:22 AM ******/
CREATE USER [os_spt] FOR LOGIN [os_spt] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [nfdt_wirem]    Script Date: 7/15/2019 10:32:22 AM ******/
CREATE USER [nfdt_wirem] FOR LOGIN [nfdt_wirem] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [nfdt_web]    Script Date: 7/15/2019 10:32:22 AM ******/
CREATE USER [nfdt_web] FOR LOGIN [nfdt_web] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [nfdt_soprodev]    Script Date: 7/15/2019 10:32:22 AM ******/
CREATE USER [nfdt_soprodev] FOR LOGIN [nfdt_soprodev] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [NFDT_reports]    Script Date: 7/15/2019 10:32:23 AM ******/
CREATE USER [NFDT_reports] FOR LOGIN [NFDT_reports] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [nfdt_Partspro]    Script Date: 7/15/2019 10:32:23 AM ******/
CREATE USER [nfdt_Partspro] FOR LOGIN [nfdt_Partspro] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [nfdt_dbconnect]    Script Date: 7/15/2019 10:32:23 AM ******/
CREATE USER [nfdt_dbconnect] FOR LOGIN [nfdt_dbconnect] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [nfdt_boss]    Script Date: 7/15/2019 10:32:23 AM ******/
CREATE USER [nfdt_boss] FOR LOGIN [nfdt_boss] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [KMART\njain3]    Script Date: 7/15/2019 10:32:23 AM ******/
CREATE USER [KMART\njain3] FOR LOGIN [KMART\njain3] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [KMART\HomeServices-HSNFDTTM-local]    Script Date: 7/15/2019 10:32:23 AM ******/
CREATE USER [KMART\HomeServices-HSNFDTTM-local] FOR LOGIN [KMART\HomeServices-HSNFDTTM-local]
GO
/****** Object:  User [KMART\HomeServices-HSNFDADMLocal]    Script Date: 7/15/2019 10:32:24 AM ******/
CREATE USER [KMART\HomeServices-HSNFDADMLocal] FOR LOGIN [KMART\HomeServices-HSNFDADMLocal]
GO
/****** Object:  DatabaseRole [db_execute]    Script Date: 7/15/2019 10:32:24 AM ******/
CREATE ROLE [db_execute]
GO
ALTER ROLE [db_execute] ADD MEMBER [SEARS2\delderk]
GO
ALTER ROLE [db_owner] ADD MEMBER [SEARS2\delderk]
GO
ALTER ROLE [db_datareader] ADD MEMBER [SEARS2\delderk]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [SEARS2\delderk]
GO
ALTER ROLE [db_execute] ADD MEMBER [os_spt]
GO
ALTER ROLE [db_datareader] ADD MEMBER [os_spt]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [os_spt]
GO
ALTER ROLE [db_datareader] ADD MEMBER [nfdt_web]
GO
ALTER ROLE [db_execute] ADD MEMBER [nfdt_soprodev]
GO
ALTER ROLE [db_datareader] ADD MEMBER [nfdt_soprodev]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [nfdt_soprodev]
GO
ALTER ROLE [db_execute] ADD MEMBER [nfdt_dbconnect]
GO
ALTER ROLE [db_datareader] ADD MEMBER [nfdt_dbconnect]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [nfdt_dbconnect]
GO
ALTER ROLE [db_execute] ADD MEMBER [nfdt_boss]
GO
ALTER ROLE [db_datareader] ADD MEMBER [nfdt_boss]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [nfdt_boss]
GO
ALTER ROLE [db_datareader] ADD MEMBER [KMART\njain3]
GO
ALTER ROLE [db_execute] ADD MEMBER [KMART\HomeServices-HSNFDTTM-local]
GO
ALTER ROLE [db_datareader] ADD MEMBER [KMART\HomeServices-HSNFDTTM-local]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [KMART\HomeServices-HSNFDTTM-local]
GO
ALTER ROLE [db_execute] ADD MEMBER [KMART\HomeServices-HSNFDADMLocal]
GO
ALTER ROLE [db_owner] ADD MEMBER [KMART\HomeServices-HSNFDADMLocal]
GO
ALTER ROLE [db_datareader] ADD MEMBER [KMART\HomeServices-HSNFDADMLocal]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [KMART\HomeServices-HSNFDADMLocal]
GO
/****** Object:  StoredProcedure [dbo].[PROC_EmployeeHierarchyUpdate]    Script Date: 7/15/2019 10:32:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Aaron Cote
-- Create date: 2015-10-19
-- Description:	Updates the employee hierarchy tables
-- =============================================
CREATE PROCEDURE [dbo].[PROC_EmployeeHierarchyUpdate]

AS
	BEGIN
		
		-- UPDATE GENERIC HIERARCHY
		IF object_id('tempdb..#TBL_EmployeeHierarchyTemp') IS NOT NULL
		BEGIN
			DROP TABLE TBL_EmployeeHierarchyTemp;
		END
		
		CREATE TABLE 
			#TBL_EmployeeHierarchyTemp
			([EnterpriseID] [varchar](28) NOT NULL,
			[FullName] [varchar](250) NULL,
			[ManagerEnterpriseID] [varchar](28) NULL,
			[ManagerFullName] [varchar](250) NULL,
			[ManagerJobCode] [varchar](10) NULL,
			[ManagerJobTitle] [varchar](70) NULL,
			[ManagerTwoEnterpriseID] [varchar](28) NULL,
			[ManagerTwoFullName] [varchar](250) NULL,
			[ManagerTwoJobCode] [varchar](10) NULL,
			[ManagerTwoJobTitle] [varchar](70) NULL,
			[ManagerThreeEnterpriseID] [varchar](28) NULL,
			[ManagerThreeFullName] [varchar](250) NULL,
			[ManagerThreeJobCode] [varchar](10) NULL,
			[ManagerThreeJobTitle] [varchar](70) NULL,
			[ManagerFourEnterpriseID] [varchar](28) NULL,
			[ManagerFourFullName] [varchar](250) NULL,
			[ManagerFourJobCode] [varchar](10) NULL,
			[ManagerFourJobTitle] [varchar](70) NULL,
			[ManagerFiveEnterpriseID] [varchar](28) NULL,
			[ManagerFiveFullName] [varchar](250) NULL,
			[ManagerFiveJobCode] [varchar](10) NULL,
			[ManagerFiveJobTitle] [varchar](70) NULL);
		
		INSERT INTO
			#TBL_EmployeeHierarchyTemp
			(EnterpriseID
			,FullName
			,ManagerEnterpriseID)
		SELECT
			EnterpriseID
			,FullName
			,ManagerEnterpriseID
		FROM
			Employee.dbo.TBL_Employee WITH (NOLOCK)
		WHERE
			RowEndDate IS NULL
			AND
			EnterpriseID <> '';
			
		-- UPDATE MANAGER LEVEL 1
		UPDATE 
			EMPHIER
		SET
			ManagerFullName = EMP.FullName
			,ManagerJobCode = EMP.JobCode
			,ManagerJobTitle = EMP.JobTitle
			,ManagerTwoEnterpriseID = EMP.ManagerEnterpriseID
		FROM 
			#TBL_EmployeeHierarchyTemp EMPHIER
		INNER JOIN
			(SELECT
				EnterpriseID
				,FullName
				,JobCode
				,JobTitle
				,ManagerEnterpriseID
			FROM
				Employee.dbo.TBL_Employee WITH (NOLOCK)
			WHERE
				RowEndDate IS NULL
				AND
				EnterpriseID <> '') EMP
		ON
			EMPHIER.ManagerEnterpriseID = EMP.EnterpriseID
		WHERE
			EMPHIER.ManagerEnterpriseID <> ''
			AND
			EMPHIER.ManagerEnterpriseID IS NOT NULL;
			
		-- UPDATE MANAGER LEVEL 2
		UPDATE 
			EMPHIER
		SET
			ManagerTwoFullName = EMP.FullName
			,ManagerTwoJobCode = EMP.JobCode
			,ManagerTwoJobTitle = EMP.JobTitle
			,ManagerThreeEnterpriseID = EMP.ManagerEnterpriseID
		FROM 
			#TBL_EmployeeHierarchyTemp EMPHIER
		INNER JOIN
			(SELECT
				EnterpriseID
				,FullName
				,JobCode
				,JobTitle
				,ManagerEnterpriseID
			FROM
				Employee.dbo.TBL_Employee WITH (NOLOCK)
			WHERE
				RowEndDate IS NULL
				AND
				EnterpriseID <> '') EMP
		ON
			EMPHIER.ManagerTwoEnterpriseID = EMP.EnterpriseID
		WHERE
			EMPHIER.ManagerTwoEnterpriseID <> ''
			AND
			EMPHIER.ManagerTwoEnterpriseID IS NOT NULL;
			
		-- UPDATE MANAGER LEVEL 3
		UPDATE 
			EMPHIER
		SET
			ManagerThreeFullName = EMP.FullName
			,ManagerThreeJobCode = EMP.JobCode
			,ManagerThreeJobTitle = EMP.JobTitle
			,ManagerFourEnterpriseID = EMP.ManagerEnterpriseID
		FROM 
			#TBL_EmployeeHierarchyTemp EMPHIER
		INNER JOIN
			(SELECT
				EnterpriseID
				,FullName
				,JobCode
				,JobTitle
				,ManagerEnterpriseID
			FROM
				Employee.dbo.TBL_Employee WITH (NOLOCK)
			WHERE
				RowEndDate IS NULL
				AND
				EnterpriseID <> '') EMP
		ON
			EMPHIER.ManagerThreeEnterpriseID = EMP.EnterpriseID
		WHERE
			EMPHIER.ManagerThreeEnterpriseID <> ''
			AND
			EMPHIER.ManagerThreeEnterpriseID IS NOT NULL;
		
		-- UPDATE MANAGER LEVEL 4
		UPDATE 
			EMPHIER
		SET
			ManagerFourFullName = EMP.FullName
			,ManagerFourJobCode = EMP.JobCode
			,ManagerFourJobTitle = EMP.JobTitle
			,ManagerFiveEnterpriseID = EMP.ManagerEnterpriseID
		FROM 
			#TBL_EmployeeHierarchyTemp EMPHIER
		INNER JOIN
			(SELECT
				EnterpriseID
				,FullName
				,JobCode
				,JobTitle
				,ManagerEnterpriseID
			FROM
				Employee.dbo.TBL_Employee WITH (NOLOCK)
			WHERE
				RowEndDate IS NULL
				AND
				EnterpriseID <> '') EMP
		ON
			EMPHIER.ManagerFourEnterpriseID = EMP.EnterpriseID
		WHERE
			EMPHIER.ManagerFourEnterpriseID <> ''
			AND
			EMPHIER.ManagerFourEnterpriseID IS NOT NULL;
			
		-- UPDATE MANAGER LEVEL 5
		UPDATE 
			EMPHIER
		SET
			ManagerFiveFullName = EMP.FullName
			,ManagerFiveJobCode = EMP.JobCode
			,ManagerFiveJobTitle = EMP.JobTitle
		FROM 
			#TBL_EmployeeHierarchyTemp EMPHIER
		INNER JOIN
			(SELECT
				EnterpriseID
				,FullName
				,JobCode
				,JobTitle
			FROM
				Employee.dbo.TBL_Employee WITH (NOLOCK)
			WHERE
				RowEndDate IS NULL
				AND
				EnterpriseID <> '') EMP
		ON
			EMPHIER.ManagerFiveEnterpriseID = EMP.EnterpriseID
		WHERE
			EMPHIER.ManagerFiveEnterpriseID <> ''
			AND
			EMPHIER.ManagerFiveEnterpriseID IS NOT NULL;
		
		-- MERGE DATA
		MERGE 
			Employee.dbo.TBL_EmployeeHierarchy AS T
		USING 
			(SELECT
				EnterpriseID
				,FullName
				,ManagerEnterpriseID
				,ManagerFullName
				,ManagerJobCode
				,ManagerJobTitle
				,ManagerTwoEnterpriseID
				,ManagerTwoFullName
				,ManagerTwoJobCode
				,ManagerTwoJobTitle
				,ManagerThreeEnterpriseID
				,ManagerThreeFullName
				,ManagerThreeJobCode
				,ManagerThreeJobTitle
				,ManagerFourEnterpriseID
				,ManagerFourFullName
				,ManagerFourJobCode
				,ManagerFourJobTitle
				,ManagerFiveEnterpriseID
				,ManagerFiveFullName
				,ManagerFiveJobCode
				,ManagerFiveJobTitle
			FROM 
				#TBL_EmployeeHierarchyTemp) AS S
		ON 
			(T.EnterpriseID = S.EnterpriseID)
		WHEN MATCHED THEN
			UPDATE 
			SET 
				 T.FullName = S.FullName
				,T.ManagerEnterpriseID = S.ManagerEnterpriseID
				,T.ManagerFullName = S.ManagerFullName
				,T.ManagerJobCode = S.ManagerJobCode
				,T.ManagerJobTitle = S.ManagerJobTitle
				,T.ManagerTwoEnterpriseID = S.ManagerTwoEnterpriseID
				,T.ManagerTwoFullName = S.ManagerTwoFullName
				,T.ManagerTwoJobCode = S.ManagerTwoJobCode
				,T.ManagerTwoJobTitle = S.ManagerTwoJobTitle
				,T.ManagerThreeEnterpriseID = S.ManagerThreeEnterpriseID
				,T.ManagerThreeFullName = S.ManagerThreeFullName
				,T.ManagerThreeJobCode = S.ManagerThreeJobCode
				,T.ManagerThreeJobTitle = S.ManagerThreeJobTitle
				,T.ManagerFourEnterpriseID = S.ManagerFourEnterpriseID
				,T.ManagerFourFullName = S.ManagerFourFullName
				,T.ManagerFourJobCode = S.ManagerFourJobCode
				,T.ManagerFourJobTitle = S.ManagerFourJobTitle	
				,T.ManagerFiveEnterpriseID = S.ManagerFiveEnterpriseID
				,T.ManagerFiveFullName = S.ManagerFiveFullName
				,T.ManagerFiveJobCode = S.ManagerFiveJobCode
				,T.ManagerFiveJobTitle = S.ManagerFiveJobTitle
		WHEN NOT MATCHED BY TARGET THEN
			INSERT 
				(EnterpriseID
				,FullName
				,ManagerEnterpriseID
				,ManagerFullName
				,ManagerJobCode
				,ManagerJobTitle
				,ManagerTwoEnterpriseID
				,ManagerTwoFullName
				,ManagerTwoJobCode
				,ManagerTwoJobTitle
				,ManagerThreeEnterpriseID
				,ManagerThreeFullName
				,ManagerThreeJobCode
				,ManagerThreeJobTitle
				,ManagerFourEnterpriseID
				,ManagerFourFullName
				,ManagerFourJobCode
				,ManagerFourJobTitle
				,ManagerFiveEnterpriseID
				,ManagerFiveFullName
				,ManagerFiveJobCode
				,ManagerFiveJobTitle)
			VALUES
				(S.EnterpriseID
				,S.FullName
				,S.ManagerEnterpriseID
				,S.ManagerFullName
				,S.ManagerJobCode
				,S.ManagerJobTitle
				,S.ManagerTwoEnterpriseID
				,S.ManagerTwoFullName
				,S.ManagerTwoJobCode
				,S.ManagerTwoJobTitle
				,S.ManagerThreeEnterpriseID
				,S.ManagerThreeFullName
				,S.ManagerThreeJobCode
				,S.ManagerThreeJobTitle
				,S.ManagerFourEnterpriseID
				,S.ManagerFourFullName
				,S.ManagerFourJobCode
				,S.ManagerFourJobTitle
				,S.ManagerFiveEnterpriseID
				,S.ManagerFiveFullName
				,S.ManagerFiveJobCode
				,S.ManagerFiveJobTitle)
		WHEN NOT MATCHED BY SOURCE THEN
			DELETE;
		
		IF object_id('tempdb..#TBL_EmployeeHierarchyTemp') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_EmployeeHierarchyTemp;
		END
		
		
		---- UPDATE GENERIC HIERARCHY INVERSE
		--IF object_id('tempdb..#TBL_EmployeeHierarchyInverseTemp') IS NOT NULL
		--BEGIN
		--	DROP TABLE TBL_EmployeeHierarchyInverseTemp;
		--END
		
		--CREATE TABLE 
		--	#TBL_EmployeeHierarchyInverseTemp
		--	([ManagerEnterpriseID] [varchar](28) NOT NULL,
		--	[ManagerFullName] [varchar](250) NULL,
		--	[EnterpriseID] [varchar](28) NOT NULL,
		--	[FullName] [varchar](250) NULL,
		--	[LevelDown] [int] NULL);
		
		--INSERT INTO
		--	#TBL_EmployeeHierarchyInverseTemp
		--	(ManagerEnterpriseID
		--	,ManagerFullName
		--	,EnterpriseID
		--	,FullName
		--	,LevelDown)
		--SELECT 
		--	EMPHIER.EnterpriseID AS ManagerEnterpriseID
		--	,EMPHIER.FullName As ManagerFullName
		--	,EMP.EnterpriseID
		--	,EMP.FullName
		--	,1 AS LevelDown
		--FROM
		--	Employee.dbo.TBL_EmployeeHierarchy EMPHIER WITH (NOLOCK)
		--INNER JOIN
		--	(SELECT
		--		EnterpriseID
		--		,FullName
		--		,ManagerEnterpriseID
		--	FROM
		--		Employee.dbo.TBL_Employee
		--	WHERE
		--		RowEndDate IS NULL
		--		AND
		--		EnterpriseID <> '') EMP
		--ON
		--	EMPHIER.EnterpriseID = EMP.ManagerEnterpriseID;
			
		--DECLARE @leveldown INT

		--SET @leveldown = 2

		--WHILE (@leveldown < 12)

		--BEGIN
			
		--	INSERT INTO
		--		#TBL_EmployeeHierarchyInverseTemp
		--		(ManagerEnterpriseID
		--		,ManagerFullName
		--		,EnterpriseID
		--		,FullName
		--		,LevelDown)
		--	SELECT 
		--		EMPHIERINV.ManagerEnterpriseID
		--		,ManagerFullName
		--		,EMP.EnterpriseID
		--		,EMP.FullName
		--		,@leveldown AS LevelDown
		--	FROM
		--		#TBL_EmployeeHierarchyInverseTemp EMPHIERINV WITH (NOLOCK)
		--	INNER JOIN
		--		(SELECT
		--			EnterpriseID
		--			,FullName
		--			,ManagerEnterpriseID
		--		FROM
		--			Employee.dbo.TBL_Employee EMP
		--		WHERE
		--			RowEndDate IS NULL
		--			AND
		--			EnterpriseID <> '') EMP
		--	ON
		--		EMPHIERINV.EnterpriseID = EMP.ManagerEnterpriseID
		--	LEFT JOIN
		--		(SELECT
		--			ManagerEnterpriseID
		--			,EnterpriseID
		--		FROM
		--			#TBL_EmployeeHierarchyInverseTemp) EMPHIERINVPREV
		--	ON
		--		EMPHIERINV.ManagerEnterpriseID = EMPHIERINVPREV.ManagerEnterpriseID
		--		AND
		--		EMP.EnterpriseID = EMPHIERINVPREV.EnterpriseID
		--	WHERE
		--		EMPHIERINVPREV.EnterpriseID IS NOT NULL
		--		AND
		--		EMP.EnterpriseID IS NOT NULL
		--		AND
		--		LevelDown = @leveldown - 1
			
		--	SET
		--		@leveldown = (@leveldown + 1)
		--END;
		
		---- MERGE DATA
		--MERGE 
		--	Employee.dbo.TBL_EmployeeHierarchyInverse AS T
		--USING 
		--	(SELECT
		--		ManagerEnterpriseID
		--		,ManagerFullName
		--		,EnterpriseID
		--		,FullName
		--		,LevelDown
		--	FROM 
		--		#TBL_EmployeeHierarchyInverseTemp) AS S
		--ON 
		--	(T.ManagerEnterpriseID = S.ManagerEnterpriseID
		--	AND
		--	T.EnterpriseID = S.EnterpriseID)
		--WHEN MATCHED THEN
		--	UPDATE 
		--	SET 
		--		 T.ManagerEnterpriseID = S.ManagerEnterpriseID
		--		,T.ManagerFullName = S.ManagerFullName
		--		,T.EnterpriseID = S.EnterpriseID
		--		,T.FullName = S.FullName
		--		,T.LevelDown = S.LevelDown
		--WHEN NOT MATCHED BY TARGET THEN
		--	INSERT 
		--		(ManagerEnterpriseID
		--		,ManagerFullName
		--		,EnterpriseID
		--		,FullName
		--		,LevelDown)
		--	VALUES
		--		(S.ManagerEnterpriseID
		--		,S.ManagerFullName
		--		,S.EnterpriseID
		--		,S.FullName
		--		,S.LevelDown)
		--WHEN NOT MATCHED BY SOURCE THEN
		--	DELETE;
		
		--IF object_id('tempdb..#TBL_EmployeeHierarchyInverseTemp') IS NOT NULL
		--BEGIN
		--	DROP TABLE #TBL_EmployeeHierarchyInverseTemp;
		--END
		
		-- UPDATE IN-HOME FIELD HIERARCHY
		IF object_id('tempdb..#TBL_EmployeeHierarchyIHTechRegionTemp') IS NOT NULL
		BEGIN
			DROP TABLE TBL_EmployeeHierarchyIHTechRegionTemp;
		END
		
		CREATE TABLE 
			#TBL_EmployeeHierarchyIHTechRegionTemp
			([EnterpriseID] [varchar](10) NOT NULL,
			[NPSID] [varchar](7) NULL,
			[FullName] [varchar](250) NULL,
			[TMEnterpriseID] [varchar](10) NULL,
			[TMFullName] [varchar](250) NULL,
			[DSMEnterpriseID] [varchar](10) NULL,
			[DSMFullName] [varchar](250) NULL,
			[Unit] [char](7) NULL,
			[UnitName] [varchar](250) NULL,
			[TFDEnterpriseID] [varchar](10) NULL,
			[TFDFullName] [varchar](250) NULL,
			[TerritoryName] [varchar](250) NULL,
			[Region] [char](7) NULL,
			[RegionName] [varchar](250) NULL,
			[RVPEnterpriseID] [varchar](10) NULL,
			[RVPFullName] [varchar](250) NULL);
		
		-- GL Department numbers listed below should be for all In-Home Techs
		INSERT INTO
			#TBL_EmployeeHierarchyIHTechRegionTemp
			(EnterpriseID
			,NPSID
			,FullName
			,Unit
			,UnitName
			,Region
			,RegionName)
		SELECT
			EnterpriseID
			,CASE WHEN NPSID = '' THEN SalesID ELSE NPSID END AS NPSID
			,FullName
			,EMP.Unit
			,UnitName
			,EMP.Region
			,RegionName
		FROM
			Employee.dbo.TBL_Employee EMP WITH (NOLOCK)
		LEFT JOIN
			(SELECT
				Unit
				,UnitName
			FROM
				Employee.dbo.TBL_NPSBaseUnit WITH (NOLOCK)
			WHERE
				Unit <> '') UNIT
		ON
			EMP.Unit = UNIT.Unit
		LEFT JOIN
			(SELECT
				Unit AS Region
				,UnitName AS RegionName
			FROM
				Employee.dbo.TBL_NPSBaseUnit WITH (NOLOCK)
			WHERE
				Unit <> '') REG
		ON
			EMP.Region = REG.Region
		WHERE
			RowEndDate IS NULL
			AND
			EnterpriseID <> ''
			AND
			GLDepartment IN ('3925','3927','3935')
			AND
			EMP.Region IN ('0000820', '0000830', '0000890');  -- DO NOT INCLUDE HVAC OR STAC AS IT DOESNT FOLLOW INHOME HIERARCHY
		
		-- UPDATE RVP
		UPDATE
			#TBL_EmployeeHierarchyIHTechRegionTemp
		SET
			RVPEnterpriseID = CASE 
								WHEN Region = '0000820' THEN 'jmorg07'
								WHEN Region = '0000830' THEN 'cgeog'
								WHEN Region = '0000890' THEN 'mirby2'
							END
			,RVPFullName = CASE 
								WHEN Region = '0000820' THEN 'Jeff E Morgan'
								WHEN Region = '0000830' THEN 'Clinton Geog'
								WHEN Region = '0000890' THEN 'Mike D Irby'
							END;
		
		-- UPDATE TM
		UPDATE 
			EMPIHFHIER
		SET
			TMEnterpriseID = EMP.TMEnterpriseID
			,TMFullName = EMP.TMFullName
		FROM 
			#TBL_EmployeeHierarchyIHTechRegionTemp EMPIHFHIER
		INNER JOIN
			(SELECT
				EnterpriseID
				,CASE
					WHEN ManagerJobCode = 'HS1202' THEN ManagerEnterpriseID
					WHEN ManagerTwoJobCode = 'HS1202' THEN ManagerTwoEnterpriseID
					WHEN ManagerThreeJobCode = 'HS1202' THEN ManagerThreeEnterpriseID
					WHEN ManagerFourJobCode = 'HS1202' THEN ManagerFourEnterpriseID
					WHEN ManagerFiveJobCode = 'HS1202' THEN ManagerFiveEnterpriseID
				END AS TMEnterpriseID
				,CASE
					WHEN ManagerJobCode = 'HS1202' THEN ManagerFullName
					WHEN ManagerTwoJobCode = 'HS1202' THEN ManagerTwoFullName
					WHEN ManagerThreeJobCode = 'HS1202' THEN ManagerThreeFullName
					WHEN ManagerFourJobCode = 'HS1202' THEN ManagerFourFullName
					WHEN ManagerFiveJobCode = 'HS1202' THEN ManagerFiveFullName
				END AS TMFullName
			FROM
				Employee.dbo.TBL_EmployeeHierarchy WITH (NOLOCK)
			WHERE
				ManagerJobCode = 'HS1202'
				OR
				ManagerTwoJobCode = 'HS1202'
				OR
				ManagerThreeJobCode = 'HS1202'
				OR
				ManagerFourJobCode = 'HS1202'
				OR
				ManagerFiveJobCode = 'HS1202') EMP
		ON
			EMPIHFHIER.EnterpriseID = EMP.EnterpriseID
		WHERE
			EMPIHFHIER.EnterpriseID <> ''
			AND
			EMPIHFHIER.EnterpriseID IS NOT NULL;
			
		-- UPDATE DSM
		UPDATE 
			EMPIHFHIER
		SET
			DSMEnterpriseID = EMP.DSMEnterpriseID
			,DSMFullName = EMP.DSMFullName
		FROM 
			#TBL_EmployeeHierarchyIHTechRegionTemp EMPIHFHIER
		INNER JOIN
			(SELECT
				EnterpriseID
				,CASE
					WHEN ManagerJobCode IN ('HS1201', 'HS9678', 'HS9802') THEN ManagerEnterpriseID
					WHEN ManagerTwoJobCode IN ('HS1201', 'HS9678', 'HS9802') THEN ManagerTwoEnterpriseID
					WHEN ManagerThreeJobCode IN ('HS1201', 'HS9678', 'HS9802') THEN ManagerThreeEnterpriseID
					WHEN ManagerFourJobCode IN ('HS1201', 'HS9678', 'HS9802') THEN ManagerFourEnterpriseID
					WHEN ManagerFiveJobCode IN ('HS1201', 'HS9678', 'HS9802') THEN ManagerFiveEnterpriseID
				END AS DSMEnterpriseID
				,CASE
					WHEN ManagerJobCode IN ('HS1201', 'HS9678', 'HS9802') THEN ManagerFullName
					WHEN ManagerTwoJobCode IN ('HS1201', 'HS9678', 'HS9802') THEN ManagerTwoFullName
					WHEN ManagerThreeJobCode IN ('HS1201', 'HS9678', 'HS9802') THEN ManagerThreeFullName
					WHEN ManagerFourJobCode IN ('HS1201', 'HS9678', 'HS9802') THEN ManagerFourFullName
					WHEN ManagerFiveJobCode IN ('HS1201', 'HS9678', 'HS9802') THEN ManagerFiveFullName
				END AS DSMFullName
			FROM
				Employee.dbo.TBL_EmployeeHierarchy WITH (NOLOCK)
			WHERE
				ManagerJobCode IN ('HS1201', 'HS9678', 'HS9802')
				OR
				ManagerTwoJobCode IN ('HS1201', 'HS9678', 'HS9802')
				OR
				ManagerThreeJobCode IN ('HS1201', 'HS9678', 'HS9802')
				OR
				ManagerFourJobCode IN ('HS1201', 'HS9678', 'HS9802')
				OR
				ManagerFiveJobCode IN ('HS1201', 'HS9678', 'HS9802')) EMP
		ON
			EMPIHFHIER.EnterpriseID = EMP.EnterpriseID
		WHERE
			EMPIHFHIER.EnterpriseID <> ''
			AND
			EMPIHFHIER.EnterpriseID IS NOT NULL;
		
		-- UPDATE DSM FOR UNITS WITH ONE DSM
		UPDATE 
			EMPIHFHIER
		SET
			DSMEnterpriseID = EMP.DSMEnterpriseID
			,DSMFullName = EMP.DSMFullName
		FROM 
			#TBL_EmployeeHierarchyIHTechRegionTemp EMPIHFHIER
		INNER JOIN
			(SELECT DISTINCT
				DSMEnterpriseID
				,DSMFullName
				,DSMEMP.Unit
			FROM
				#TBL_EmployeeHierarchyIHTechRegionTemp DSMEMP
			INNER JOIN
				(SELECT
					Unit
					,COUNT(DSMEnterpriseID) AS DSMCount
				FROM
					(SELECT DISTINCT
						Unit
						,DSMEnterpriseID
					FROM
						#TBL_EmployeeHierarchyIHTechRegionTemp
					WHERE
						DSMEnterpriseID IS NOT NULL) DSM
				GROUP BY
					Unit
				HAVING 
					COUNT(DSMEnterpriseID) = 1) DSMCOUNT
			ON
				DSMEMP.Unit = DSMCOUNT.Unit
			WHERE
				DSMEnterpriseID IS NOT NULL) EMP
		ON
			EMPIHFHIER.Unit = EMP.Unit
		WHERE
			EMPIHFHIER.DSMEnterpriseID IS NULL;
		
		-- UPDATE TFD
		UPDATE 
			EMPIHFHIER
		SET
			TFDEnterpriseID = EMP.TFDEnterpriseID
			,TFDFullName = EMP.TFDFullName
		FROM 
			#TBL_EmployeeHierarchyIHTechRegionTemp EMPIHFHIER
		INNER JOIN
			(SELECT
				EnterpriseID
				,CASE
					WHEN ManagerJobCode = 'HS9666' THEN ManagerEnterpriseID
					WHEN ManagerTwoJobCode = 'HS9666' THEN ManagerTwoEnterpriseID
					WHEN ManagerThreeJobCode = 'HS9666' THEN ManagerThreeEnterpriseID
					WHEN ManagerFourJobCode = 'HS9666' THEN ManagerFourEnterpriseID
					WHEN ManagerFiveJobCode = 'HS9666' THEN ManagerFiveEnterpriseID
				END AS TFDEnterpriseID
				,CASE
					WHEN ManagerJobCode = 'HS9666' THEN ManagerFullName
					WHEN ManagerTwoJobCode = 'HS9666' THEN ManagerTwoFullName
					WHEN ManagerThreeJobCode = 'HS9666' THEN ManagerThreeFullName
					WHEN ManagerFourJobCode = 'HS9666' THEN ManagerFourFullName
					WHEN ManagerFiveJobCode = 'HS9666' THEN ManagerFiveFullName
				END AS TFDFullName
			FROM
				Employee.dbo.TBL_EmployeeHierarchy WITH (NOLOCK)
			WHERE
				ManagerJobCode = 'HS9666'
				OR
				ManagerTwoJobCode = 'HS9666'
				OR
				ManagerThreeJobCode = 'HS9666'
				OR
				ManagerFourJobCode = 'HS9666'
				OR
				ManagerFiveJobCode = 'HS9666') EMP
		ON
			EMPIHFHIER.EnterpriseID = EMP.EnterpriseID
		WHERE
			EMPIHFHIER.EnterpriseID <> ''
			AND
			EMPIHFHIER.EnterpriseID IS NOT NULL;		
		
		-- UPDATE TFD TO MAKE SURE ALL EMPLOYEES IN UNIT
		-- HAVE SAME TFD
		UPDATE 
			EMPIHFHIER
		SET
			TFDEnterpriseID = EMP.TFDEnterpriseID
			,TFDFullName = EMP.TFDFullName
		FROM 
			#TBL_EmployeeHierarchyIHTechRegionTemp EMPIHFHIER
		INNER JOIN
			(SELECT DISTINCT
				TFDEnterpriseID
				,TFDFullName
				,TFDEMP.Unit
			FROM
				#TBL_EmployeeHierarchyIHTechRegionTemp TFDEMP
			INNER JOIN
				(SELECT
					Unit
					,COUNT(TFDEnterpriseID) AS TFDCount
				FROM
					(SELECT DISTINCT
						Unit
						,TFDEnterpriseID
					FROM
						#TBL_EmployeeHierarchyIHTechRegionTemp
					WHERE
						TFDEnterpriseID IS NOT NULL) TFD
				GROUP BY
					Unit
				HAVING 
					COUNT(TFDEnterpriseID) = 1) TFDCOUNT
			ON
				TFDEMP.Unit = TFDCOUNT.Unit
			WHERE
				TFDEnterpriseID IS NOT NULL) EMP
		ON
			EMPIHFHIER.Unit = EMP.Unit
		WHERE
			EMPIHFHIER.TFDEnterpriseID IS NULL;
		
		-- UPDATE TERRITORY NAME
		UPDATE 
			EMPIHFHIER
		SET
			TerritoryName = UNIT.TerritoryName
		FROM 
			#TBL_EmployeeHierarchyIHTechRegionTemp EMPIHFHIER
		INNER JOIN
			(SELECT
				Unit
				,TerritoryName
			FROM
				Employee.dbo.TBL_InHomeUnitTerritory WITH (NOLOCK)) UNIT
		ON
			EMPIHFHIER.Unit = UNIT.Unit;
		
		-- MERGE DATA
		MERGE 
			Employee.dbo.TBL_EmployeeHierarchyIHTechRegion AS T
		USING 
			(SELECT
				EnterpriseID
				,NPSID
				,FullName
				,TMEnterpriseID
				,TMFullName
				,DSMEnterpriseID
				,DSMFullName
				,Unit
				,UnitName
				,TFDEnterpriseID
				,TFDFullName
				,TerritoryName
				,Region
				,RegionName
				,RVPEnterpriseID
				,RVPFullName
			FROM 
				#TBL_EmployeeHierarchyIHTechRegionTemp) AS S
		ON 
			(T.EnterpriseID = S.EnterpriseID)
		WHEN MATCHED THEN
			UPDATE 
			SET 
				 T.NPSID = S.NPSID
				,T.FullName = S.FullName
				,T.TMEnterpriseID = S.TMEnterpriseID
				,T.TMFullName = S.TMFullName
				,T.DSMEnterpriseID = S.DSMEnterpriseID
				,T.DSMFullName = S.DSMFullName
				,T.Unit = S.Unit
				,T.UnitName = S.UnitName
				,T.TFDEnterpriseID = S.TFDEnterpriseID
				,T.TFDFullName = S.TFDFullName
				,T.TerritoryName = S.TerritoryName
				,T.Region = S.Region
				,T.RegionName = S.RegionName
				,T.RVPEnterpriseID = S.RVPEnterpriseID
				,T.RVPFullName = S.RVPFullName
		WHEN NOT MATCHED BY TARGET THEN
			INSERT 
				(EnterpriseID
				,NPSID
				,FullName
				,TMEnterpriseID
				,TMFullName
				,DSMEnterpriseID
				,DSMFullName
				,Unit
				,UnitName
				,TFDEnterpriseID
				,TFDFullName
				,TerritoryName
				,Region
				,RegionName
				,RVPEnterpriseID
				,RVPFullName)
			VALUES
				(S.EnterpriseID
				,S.NPSID
				,S.FullName
				,S.TMEnterpriseID
				,S.TMFullName
				,S.DSMEnterpriseID
				,S.DSMFullName
				,S.Unit
				,S.UnitName
				,S.TFDEnterpriseID
				,S.TFDFullName
				,S.TerritoryName
				,S.Region
				,S.RegionName
				,S.RVPEnterpriseID
				,S.RVPFullName)
		WHEN NOT MATCHED BY SOURCE THEN
			DELETE;
		
		-- MERGE DATA FOR TBL_EmployeeHierarchyIHUnitRegion
		MERGE 
			Employee.dbo.TBL_EmployeeHierarchyIHUnitRegion AS T
		USING 
			(SELECT DISTINCT
				Unit
				,UnitName
				,TFDEnterpriseID
				,TFDFullName
				,TerritoryName
				,Region
				,RegionName
				,RVPEnterpriseID
				,RVPFullName
			FROM 
				#TBL_EmployeeHierarchyIHTechRegionTemp) AS S
		ON 
			(T.Unit = S.Unit)
		WHEN MATCHED THEN
			UPDATE 
			SET 
				 T.Unit = S.Unit
				,T.UnitName = S.UnitName
				,T.TFDEnterpriseID = S.TFDEnterpriseID
				,T.TFDFullName = S.TFDFullName
				,T.TerritoryName = S.TerritoryName
				,T.Region = S.Region
				,T.RegionName = S.RegionName
				,T.RVPEnterpriseID = S.RVPEnterpriseID
				,T.RVPFullName = S.RVPFullName
		WHEN NOT MATCHED BY TARGET THEN
			INSERT 
				(Unit
				,UnitName
				,TFDEnterpriseID
				,TFDFullName
				,TerritoryName
				,Region
				,RegionName
				,RVPEnterpriseID
				,RVPFullName)
			VALUES
				(S.Unit
				,S.UnitName
				,S.TFDEnterpriseID
				,S.TFDFullName
				,S.TerritoryName
				,S.Region
				,S.RegionName
				,S.RVPEnterpriseID
				,S.RVPFullName)
		WHEN NOT MATCHED BY SOURCE THEN
			DELETE;
			
			
		IF object_id('tempdb..#TBL_EmployeeHierarchyIHTechRegionTemp') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_EmployeeHierarchyIHTechRegionTemp;
		END
		
	END








GO
/****** Object:  StoredProcedure [dbo].[PROC_EmployeeUpdate]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Aaron Cote
-- Create date: 2015-10-14
-- Description:	Updates the final employee table
-- =============================================
CREATE PROCEDURE [dbo].[PROC_EmployeeUpdate]

AS
	BEGIN

		IF object_id('tempdb..#TBL_EmployeeTemp') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_EmployeeTemp;
		END
		
		CREATE TABLE 
			#TBL_EmployeeTemp
			([BusinessUnit] [varchar](4) NOT NULL,
			[FinancialRegion] [varchar](5) NOT NULL,
			[FinancialUnit] [varchar](5) NOT NULL,
			[Region] [varchar](7) NOT NULL,
			[Unit] [varchar](30) NOT NULL,
			[Department] [varchar](31) NOT NULL,
			[SearsDepartment] [varchar](31) NOT NULL,
			[SearsDepartmentName] [varchar](30) NOT NULL,
			[GLDepartment] [varchar](50) NOT NULL,
			[Location] [varchar](5) NOT NULL,
			[LocationDescription] [varchar](30) NOT NULL,
			[PayType] [varchar](3) NOT NULL,
			[PayGroup] [varchar](6) NOT NULL,
			[JobGrade] [varchar](5) NOT NULL,
			[EnterpriseID] [varchar](28) NOT NULL,
			[EmployeeID] [varchar](14) NOT NULL,
			[NPSID] [varchar](7) NOT NULL,
			[SalesID] [varchar](7) NOT NULL,
			[EmployeeStatus] [varchar](1) NOT NULL,
			[EmployeeType] [varchar](1) NOT NULL,
			[LastName] [varchar](100) NOT NULL,
			[FirstName] [varchar](100) NOT NULL,
			[MiddleName] [varchar](50) NOT NULL,
			[FullName] [varchar](250) NOT NULL,
			[DisplayName] [varchar](250) NOT NULL,
			[Email] [varchar](250) NOT NULL,
			[OfficePhone] [varchar](70) NOT NULL,
			[MobilePhone] [varchar](70) NOT NULL,
			[MobilePhoneProvider] [varchar](10) NOT NULL,
			[JobCode] [varchar](10) NOT NULL,
			[JobTitle] [varchar](70) NOT NULL,
			[StartDate] [varchar](10) NOT NULL,
			[JobStartDate] [varchar](10) NOT NULL,
			[TermDate] [varchar](10) NOT NULL,
			[ManagerEnterpriseID] [varchar](28) NOT NULL,
			[ManagerEmployeeID] [varchar](14) NOT NULL,
			[ManagerStartDate] [date] NOT NULL,
			[RowStartDate] [date] NOT NULL);
		
		INSERT INTO
			#TBL_EmployeeTemp
			(BusinessUnit
			,FinancialRegion
			,FinancialUnit
			,Region
			,Unit
			,Department
			,SearsDepartment
			,SearsDepartmentName
			,GLDepartment
			,Location
			,LocationDescription
			,PayType
			,PayGroup
			,JobGrade
			,EnterpriseID
			,EmployeeID
			,NPSID
			,SalesID
			,EmployeeStatus
			,EmployeeType
			,LastName
			,FirstName
			,MiddleName
			,FullName
			,DisplayName
			,Email
			,OfficePhone
			,MobilePhone
			,MobilePhoneProvider
			,JobCode
			,JobTitle
			,StartDate
			,JobStartDate
			,TermDate
			,ManagerEnterpriseID
			,ManagerEmployeeID
			,ManagerStartDate
			,RowStartDate)
		SELECT
			BusinessUnit
			,'' AS FinancialRegion
			,FinancialUnit
			,'' AS Region
			,'00' + FinancialUnit AS Unit
			,Department
			,SearsDepartment
			,SearsDepartmentName
			,'' AS GLDepartment
			,Location
			,LocationDescription
			,PayType
			,PayGroup
			,JobGrade
			,EnterpriseID
			,EmployeeID
			,NPSID
			,SalesID
			,EmployeeStatus
			,EmployeeType
			,LastName
			,FirstName
			,MiddleName
			,FullName
			,DisplayName
			,Email
			,OfficePhone
			,MobilePhone
			,'' AS MobilePhoneProvider
			,JobCode
			,JobTitle
			,StartDate
			,JobStartDate
			,TermDate
			,ManagerEnterpriseID
			,'' AS ManagerEmployeeID
			,CAST(GETDATE() AS DATE) AS ManagerStartDate
			,CAST(GETDATE() AS DATE) AS RowStartDate
		FROM
			Employee.dbo.TBL_LDAPEmployee;
		
		-- DELETE FAKE EMPLOYEE IDS - ADDED ON 10/6/2014
		DELETE FROM 
			#TBL_EmployeeTemp
		WHERE 
			EmployeeID IN ('00000000000', '00000000001')
			OR
			EmployeeID = ''
			OR
			LEN(EmployeeID) < 11;
	
		-- UPDATE FINANCIAL REGION, GLDEPARTMENT FROM PEOPLESOFT
		UPDATE 
			EMPSTAGE
		SET
			FinancialRegion = HREI.FinancialRegion
			,GLDepartment = HREI.GLDepartment
		FROM 
			#TBL_EmployeeTemp EMPSTAGE
		INNER JOIN
			(SELECT 
				EmployeeID
				,ISNULL(FinancialRegion, '') AS FinancialRegion
				,GLDepartment
			FROM 
				Employee.dbo.TBL_PeoplesoftEmployee) HREI
		ON
			EMPSTAGE.EmployeeID = HREI.EmployeeID;
	
		-- UPDATE USING TBL_SSTTechnician(SSTXTPT) DATA
		UPDATE 
			EMPSTAGE
		SET
			Region = SSTPTBU.Region,
			Unit = SSTPTBU.Unit,
			NPSID = SSTPTBU.NPSID
		FROM 
			#TBL_EmployeeTemp EMPSTAGE
		INNER JOIN
			(SELECT
				EnterpriseID,
				ISNULL(BU.Region, '') AS Region,
				ISNULL(SSTPT.Unit, '') AS Unit,
				ISNULL(NPSID, '') AS NPSID
			FROM 
				Employee.dbo.TBL_SSTTechnician SSTPT
			INNER JOIN
				(SELECT DISTINCT 
					Unit,
					Region
				FROM 
					Employee.dbo.TBL_NPSBaseUnit) BU
			ON 
				SSTPT.Unit = BU.Unit
			WHERE
				LEN(PendingSoftwareVersion) > 2) SSTPTBU
		ON
			EMPSTAGE.EnterpriseID = SSTPTBU.EnterpriseID;

		-- UPDATE USING TBL_NPSTechnicianPay(NPSXTPY) DATA THAT ONLY HAS EMPLOYEE ID LISTED ONCE
		UPDATE 
			EMPSTAGE
		SET
			Region = PYEIBU.Region,
			Unit = PYEIBU.Unit,
			NPSID = PYEIBU.EmployeeID
		FROM 
			#TBL_EmployeeTemp EMPSTAGE
		INNER JOIN
			(SELECT DISTINCT 
				Region,
				PY.Unit,
				NPSID,
				PY.EmployeeID
			  FROM 
				Employee.dbo.TBL_NPSTechnicianPay PY
			INNER JOIN
				(SELECT DISTINCT 
					Unit,
					Region
				FROM 
					Employee.dbo.TBL_NPSBaseUnit) BU
			ON 
				PY.Unit = BU.Unit
			INNER JOIN
				(SELECT 
					EmployeeID
				FROM 
					Employee.dbo.TBL_NPSTechnicianPay
				GROUP BY 
					EmployeeID
				HAVING 
					COUNT(EmployeeID) = 1) PYTWO
			ON 
				PY.EmployeeID = PYTWO.EmployeeID) PYEIBU
		ON
			EMPSTAGE.EmployeeID = PYEIBU.EmployeeID
		WHERE
			EMPSTAGE.Region = '';

		-- UPDATE EMPSTAGE DATA BASED ON EI DATA THAT ONLY HAS ENTERPRISE ID LISTED ONCE

		UPDATE 
			EMPSTAGE
		SET
			Region = EIBU.Region
			,Unit = EIBU.Unit
			,NPSID = EIBU.NPSID
		FROM 
			#TBL_EmployeeTemp EMPSTAGE
		INNER JOIN
			(SELECT DISTINCT 
				Region
				,EI.Unit
				,NPSID
				,EI.EnterpriseID
			  FROM 
				Employee.dbo.TBL_NPSEmployee EI
			INNER JOIN 
				(SELECT 
					EnterpriseID
			FROM 
				Employee.dbo.TBL_NPSEmployee
			WHERE 
				EnterpriseID <> ''
				AND
				TermDate = '1111-11-11'
			GROUP BY
				EnterpriseID
			HAVING
				COUNT(EnterpriseID) = 1
				AND
				LEN(EnterpriseID) > 3) EITWO
			ON
				EI.EnterpriseID = EITWO.EnterpriseID
			INNER JOIN
				(SELECT DISTINCT 
					Unit
					,Region
				FROM 
					Employee.dbo.TBL_NPSBaseUnit) BU
			ON 
				EI.Unit = BU.Unit
			WHERE 
				TermDate = '1111-11-11') EIBU
		ON
			EMPSTAGE.EnterpriseID = EIBU.EnterpriseID
		WHERE
			EMPSTAGE.Region = '';

		-- UPDATE EMPSTAGE DATA USING FINANCIAL UNIT CROSS REFERENCE
		--UPDATE 
		--	EMPSTAGE
		--SET
		--	Region = rgn_2013,
		--	Unit = ctr_svc_un_no
		--FROM 
		--	#TBL_EmployeeTemp EMPSTAGE
		--INNER JOIN
		--	(SELECT 
		--		ctr_svc_un_no,
		--		fin_un_no,
		--		CASE WHEN rgn_2013 IS NULL THEN '' ELSE rgn_2013 END AS rgn_2013
		--	FROM 
		--		TRYDVSQL1.shared_tbls.nfdt.Fin_Unit_Xref) XREF
		--ON
		--	EMPSTAGE.PayGroup = XREF.fin_un_no
		--WHERE
		--	EMPSTAGE.Region = '';

		-- UPDATE EMPSTAGE DATA USING FINANCIAL REGION FROM PEOPLESOFT
		UPDATE 
			EMPSTAGE
		SET
			Region = HREI.FinancialRegion
		FROM 
			#TBL_EmployeeTemp EMPSTAGE
		INNER JOIN
			(SELECT 
				'00' + RIGHT(FinancialRegion, 5) AS FinancialRegion,
				EmployeeID
			FROM 
				Employee.dbo.TBL_PeoplesoftEmployee
			WHERE 
				FinancialRegion <> ''
				AND
				FinancialUnit <> '') HREI
		ON
			EMPSTAGE.EmployeeID = HREI.EmployeeID
		WHERE
			EMPSTAGE.Region = '';

		-- UPDATE EMPSTAGE DATA USING TBL_NPSTechnicianPay(NPSXTPY) ON RECORDS THAT HAVE EMPLOYEE LISTED ONLY ONCE PER UNIT
		UPDATE 
			EMPSTAGE
		SET
			Region = PYEIBU.Region,
			NPSID = PYEIBU.NPSID
		FROM 
			#TBL_EmployeeTemp EMPSTAGE
		INNER JOIN
			(SELECT DISTINCT 
				ISNULL(Region, '') AS Region,
				PY.Unit,
				ISNULL(EI.NPSID, '') AS NPSID,
				PY.EmployeeID
			  FROM 
				Employee.dbo.TBL_NPSTechnicianPay PY
			INNER JOIN 
				(SELECT 
					Unit,
					NPSID
			  FROM 
				Employee.dbo.TBL_NPSEmployee
			  WHERE 
				EnterpriseID <> ''
				AND
				TermDate = '1111-11-11') EI
			ON
				PY.Unit = EI.Unit
				AND
				PY.NPSID = EI.NPSID
			INNER JOIN
				(SELECT DISTINCT 
					Unit,
					Region
				FROM 
					Employee.dbo.TBL_NPSBaseUnit) BU
			ON 
				PY.Unit = BU.Unit
			INNER JOIN
				(SELECT
					Unit,
					EmployeeID
				FROM 
					Employee.dbo.TBL_NPSTechnicianPay
				GROUP BY 
					Unit,
					EmployeeID
				HAVING 
					COUNT(EmployeeID) = 1) PYTWO
			ON 
				PY.EmployeeID = PYTWO.EmployeeID) PYEIBU
		ON
			EMPSTAGE.EmployeeID = PYEIBU.EmployeeID
			AND
			EMPSTAGE.Unit = PYEIBU.Unit
		WHERE
			EMPSTAGE.Region = '';
			
		-- UPDATE EMPSTAGE UNIT BASED ON EMPSTAGE LOCATIONS 2 TABLE
		UPDATE 
			EMPSTAGE
		SET
			Unit = EMPSTAGELOCTWO.Unit
		FROM 
			#TBL_EmployeeTemp EMPSTAGE
		INNER JOIN
			(SELECT 
				Number,
				'00' + ReportsTo AS Unit
			FROM 
				Employee.dbo.TBL_LDAPLocationsTwo
			  WHERE 
				ReportsTo <> ''
				AND
				ISNUMERIC(ReportsTo) = 1) EMPSTAGELOCTWO
		ON
			EMPSTAGE.PayGroup = EMPSTAGELOCTWO.Number
		WHERE
			EMPSTAGE.Region = '';

		-- UPDATE USING IH TBL_NPSBaseUnit(NPSXTBU) DS Data
		UPDATE 
			EMPSTAGE
		SET
			Region = BU.Region,
			Unit = CentralServiceUnit
		FROM 
			#TBL_EmployeeTemp EMPSTAGE
		INNER JOIN
			(SELECT 
				Unit,
				ISNULL(Region, '') AS Region,
				CentralServiceUnit
			FROM 
				Employee.dbo.TBL_NPSBaseUnit T1
			INNER JOIN
				(SELECT 
					UnitName
					,MAX(BatchUpdateDate) AS BatchUpdateDate
				FROM 
					Employee.dbo.TBL_NPSBaseUnit
				GROUP BY
					UnitName) T2
			ON
				T1.UnitName = T2.UnitName
				AND
				T1.BatchUpdateDate = T2.BatchUpdateDate
			WHERE
				UnitTypeCode = 'DS') BU
		ON
			'00' + EMPSTAGE.PayGroup = BU.Unit
			AND
			EMPSTAGE.Unit <> BU.CentralServiceUnit;

		-- CLEAN UP IN-HOME REGION IDS
		UPDATE 
			#TBL_EmployeeTemp 
		SET
			Region = '0000' + RIGHT(Region, 3)
		WHERE
			Region LIKE '00088%';

		-- UPDATE REMAINING NPS IDS
		UPDATE 
			EMPSTAGE
		SET
			NPSID = EI.NPSID
		  FROM #TBL_EmployeeTemp EMPSTAGE
		INNER JOIN
			(SELECT DISTINCT
				Unit,
				NPSID,
				EnterpriseID
			FROM 
				Employee.dbo.TBL_NPSEmployee
			WHERE 
				EnterpriseID <> '') EI
		ON 
			EMPSTAGE.Unit = EI.Unit
			AND
			EMPSTAGE.EnterpriseID = EI.EnterpriseID
		WHERE
			EMPSTAGE.NPSID = '';

		-- FORMAT DATES
		UPDATE
			#TBL_EmployeeTemp
		SET
			StartDate = SUBSTRING(StartDate, 1, 4) + '-' + SUBSTRING(StartDate, 5, 2) + '-' + SUBSTRING(StartDate, 7, 2)
		WHERE
			LEN(StartDate) = 8;

		UPDATE
			#TBL_EmployeeTemp
		SET
			JobStartDate = SUBSTRING(JobStartDate, 1, 4) + '-' + SUBSTRING(JobStartDate, 5, 2) + '-' + SUBSTRING(JobStartDate, 7, 2)
		WHERE
			LEN(JobStartDate) = 8;
			
		UPDATE
			#TBL_EmployeeTemp
		SET
			TermDate = SUBSTRING(TermDate, 1, 4) + '-' + SUBSTRING(TermDate, 5, 2) + '-' + SUBSTRING(TermDate, 7, 2)
		WHERE
			LEN(TermDate) = 8;

		-- INSERT FAKE EMPLOYEES FROM THE EI TABLE
		INSERT INTO 
			#TBL_EmployeeTemp
			(BusinessUnit
			,FinancialRegion
			,FinancialUnit
			,Region
			,Unit
			,Department
			,SearsDepartment
			,SearsDepartmentName
			,GLDepartment
			,Location
			,LocationDescription
			,PayType
			,PayGroup
			,JobGrade
			,EnterpriseID
			,EmployeeID
			,NPSID
			,SalesID
			,EmployeeStatus
			,EmployeeType
			,LastName
			,FirstName
			,MiddleName
			,FullName
			,DisplayName
			,Email
			,OfficePhone
			,MobilePhone
			,MobilePhoneProvider
			,JobCode
			,JobTitle
			,StartDate
			,JobStartDate
			,TermDate
			,ManagerEnterpriseID
			,ManagerEmployeeID
			,ManagerStartDate
			,RowStartDate)
		SELECT DISTINCT
			'' AS BusinessUnit
			,'' AS FinancialRegion
			,'' AS FinancialUnit
			,ISNULL(Region, '') AS Region
			,ISNULL(EI.Unit, '') AS Unit
			,'' AS Department
			,'' AS SearsDepartment
			,'' AS SearsDepartmentName
			,'' AS GLDepartment
			,'' AS Location
			,'' AS LocationDescription
			,'' AS PayType
			,'' AS PayGroup
			,'' AS JobGrade
			,'' AS EnterpriseID
			,ISNULL(EI.Unit, '') + ISNULL(NPSID, '') AS EmployeeID
			,ISNULL(NPSID, '')
			,'' AS SalesID
			,'' AS EmployeeStatus
			,'' AS EmployeeType
			,LTRIM(RTRIM(LastName)) AS LastName
			,LTRIM(RTRIM(FirstName)) AS FirstName
			,LTRIM(RTRIM(MiddleName)) AS MiddleName
			,LTRIM(RTRIM(FirstName)) + ' ' + LTRIM(RTRIM(MiddleName)) + ' ' + LTRIM(RTRIM(LastName)) AS FullName
			,LTRIM(RTRIM(LastName)) + ', ' + LTRIM(RTRIM(FirstName)) AS DisplayName
			,'' AS Email
			,'' AS OfficePhone
			,'' AS MobilePhone
			,'' AS MobilePhoneProvider
			,'' AS JobCode
			,'' AS JobTitle
			,ServiceDate AS StartDate
			,ServiceDate AS JobStartDate
			,'' AS TermDate
			,CASE WHEN SUPEI.ManagerEnterpriseID IS NULL THEN '' ELSE LTRIM(RTRIM(SUPEI.ManagerEnterpriseID)) END AS ManagerEnterpriseID
			,'' AS ManagerEmployeeID
			,CAST(GETDATE() AS DATE) AS ManagerStartDate
			,CAST(GETDATE() AS DATE) AS RowStartDate
		  FROM 
			Employee.dbo.TBL_NPSEmployee EI
		LEFT JOIN
			(SELECT
				Unit AS ManagerUnit,
				NPSID AS ManagerEmployeeID,
				EI.EnterpriseID AS ManagerEnterpriseID
			FROM 
				Employee.dbo.TBL_NPSEmployee EI
			INNER JOIN
				(SELECT
					EnterpriseID
				FROM 
					#TBL_EmployeeTemp) EMPSTAGE
			ON 
				EMPSTAGE.EnterpriseID = EI.EnterpriseID
			WHERE
				TermDate = '1111-11-11'
				AND
				EI.EnterpriseID <> ''
				AND
				LEN(EI.EnterpriseID) > 3) SUPEI
		ON
			EI.Unit = SUPEI.ManagerUnit
			AND
			EI.ManagerEmployeeID = SUPEI.ManagerEmployeeID
		LEFT JOIN 
			(SELECT 
				Unit,
				Region
			FROM 
				Employee.dbo.TBL_NPSBaseUnit
			WHERE Region <> '') BU
		ON 
			EI.Unit = BU.Unit
		WHERE 
			TermDate = '1111-11-11'
			AND
			EnterpriseID NOT IN (SELECT EnterpriseID FROM #TBL_EmployeeTemp WHERE EnterpriseID <> '')
			AND
			EI.Unit + NPSID NOT IN (SELECT Unit + NPSID FROM #TBL_EmployeeTemp WHERE Unit <> '' AND NPSID <> '');

		-- UPDATE REMAING RECORDS WITH THE REGION
		UPDATE 
			EMPSTAGE 
		SET
			Region = BU.Region
		FROM 
			#TBL_EmployeeTemp EMPSTAGE
		INNER JOIN 
			(SELECT 
				Unit,
				Region
			FROM 
				Employee.dbo.TBL_NPSBaseUnit
			WHERE Region <> '') BU
		ON 
			EMPSTAGE.Unit = BU.Unit;
		--WHERE
		--	EMPSTAGE.Region = ''
		--	OR
		--	(SUBSTRING(Region,4,1) <> '8'
		--	AND
		--	SUBSTRING(Region,4,1) <> '0');

		-- Clean up routine for regions and units using the
		-- region and unit that show up the maximum number
		-- of times for a specific pay group.
		UPDATE 
			EMPSTAGE
		SET 
			Region = STAGE.Region
			,Unit = STAGE.Unit
		FROM 
			#TBL_EmployeeTemp EMPSTAGE
		INNER JOIN 
			(SELECT 
				Region
				,Unit
				,EMPFINAL.PayGroup
			FROM 
				#TBL_EmployeeTemp EMPFINAL
			INNER JOIN
				(SELECT 
					EmployeeStaging.PayGroup
					,MAX(PayGroupCountSource) AS PayGroupCount
				FROM 
					#TBL_EmployeeTemp EmployeeStaging
				INNER JOIN
					(SELECT 
						Region
						,Unit
						,PayGroup
						,COUNT(PayGroup) AS PayGroupCountSource
					FROM 
						#TBL_EmployeeTemp
					WHERE 
						PayGroup IS NOT NULL 
						AND 
						PayGroup <> ''
					GROUP BY 
						Region
						,Unit
						,PayGroup) EmployeeStagingTWO
				ON 
					EmployeeStaging.Region = EmployeeStagingTWO.Region
					AND
					EmployeeStaging.Unit = EmployeeStagingTWO.Unit
					AND
					EmployeeStaging.PayGroup = EmployeeStagingTWO.PayGroup
				GROUP BY
					EmployeeStaging.PayGroup) EmployeeStaging
			ON EMPFINAL.PayGroup = EmployeeStaging.PayGroup
			GROUP BY 
				Region
				,Unit
				,EMPFINAL.PayGroup
				,PayGroupCount
			HAVING
				COUNT(EMPFINAL.PayGroup) = PayGroupCount) STAGE
		ON EMPSTAGE.PayGroup = STAGE.PayGroup

		-- UPDATE REGION EMPSTAGE DATA USING FINANCIAL REGION NUMBER INFORMATION FROM PEOPLESOFT
		UPDATE 
			EMPSTAGE
		SET
			Region = FINANCE.FinancialRegion
		FROM 
			#TBL_EmployeeTemp EMPSTAGE
		INNER JOIN
			(SELECT 
				'0000' + RIGHT(FinancialRegion, 3) AS FinancialRegion
				,'000' + RIGHT(FinancialUnit, 4) AS FinancialUnit
				,EmployeeID
			FROM 
				Employee.dbo.TBL_PeoplesoftEmployee
			WHERE 
				FinancialRegion <> ''
				AND
				FinancialRegion IN ('58820', '58830', '58890', '58900')) FINANCE
		ON
			EMPSTAGE.EmployeeID = FINANCE.EmployeeID;

		-- UPDATE ROUTING EMPLOYEES
		UPDATE
			#TBL_EmployeeTemp
		SET
			Unit = '00' + PayGroup,
			NPSID = ''
		WHERE
			PayGroup IN ('08251', '08261', '08253', '09302');
			
		-- UPDATE ROUTING EMPLOYEES PART 2
		UPDATE
			#TBL_EmployeeTemp
		SET
			Unit = '0008253',
			NPSID = ''
		WHERE
			PayGroup = '04265';

		-- UPDATE CARRY-IN EMPLOYEES
		UPDATE
			#TBL_EmployeeTemp
		SET
			Region = '0000490',
			NPSID = ''
		WHERE
			PayGroup = '24409';
			
		-- UPDATE CORPORATE EMPLOYEES
		--UPDATE
		--	#TBL_EmployeeTemp
		--SET
		--	Region = '0000490'
		--WHERE
		--	Paygroup = '58490';


		---- ADD MANAGER ENTERPRISE IDS FROM EI FOR EMPLOYEES THAT ARE MISSING THE INFORMATION

		--UPDATE
		--	LDAP 
		--SET
		--	ManagerEnterpriseID = mgr_rac_id_no
		--FROM 
		--	#TBL_EmployeeTemp EMPSTAGE
		--INNER JOIN
		--	(SELECT 
		--		emp_rac_id_no,
		--		mgr_rac_id_no
		--	FROM 
		--		shared_tbls.td.npsxtei EI
		--	INNER JOIN
		--		(SELECT 
		--		svc_un_no,
		--		emp_id_no,
		--		LOWER(emp_rac_id_no) AS mgr_rac_id_no
		--	FROM 
		--		shared_tbls.td.npsxtei
		--	WHERE 
		--		emp_rac_id_no IN (SELECT EnterpriseID FROM #TBL_EmployeeTemp WHERE EnterpriseID <> '')) MGREI
		--	ON
		--		EI.svc_un_no = MGREI.svc_un_no
		--		AND
		--		EI.spr_emp_id_1_no = MGREI.emp_id_no
		--	WHERE
		--		emp_rac_id_no <> '') EISOURCE
		--ON 
		--	EMPSTAGE.EnterpriseID = EISOURCE.emp_rac_id_no
		--WHERE 
		--	ManagerEnterpriseID = ''
		--	AND
		--	Unit <> ''
		--	AND
		--	PayGroup <> '';

		-- ADD MANAGER EMPLOYEE IDS
		UPDATE
			EMPSTAGE 
		SET
			ManagerEmployeeID = MGREmployeeID
		FROM 
			#TBL_EmployeeTemp EMPSTAGE
		INNER JOIN
			(SELECT
				EnterpriseID AS MGREnterpriseID,
				EmployeeID AS MGREmployeeID
			FROM
				#TBL_EmployeeTemp
			WHERE 
				EmployeeID <> '') MGREMPSTAGE
		ON 
			EMPSTAGE.ManagerEnterpriseID = MGREMPSTAGE.MGREnterpriseID
		WHERE 
			ManagerEnterpriseID <> '';
			
		---- UPDATE JOB START DATE TO MAKE SURE IT STAYS
		---- CONSISTENT WITH THE JOB CODE, EVEN IF THERE'S A CHANGE IN THE UNIT
		---- THAT WOULD CAUSE THE START DATE TO CHANGE

		--UPDATE
		--	LDAP 
		--SET
		--	JobStartDate = JC.JobStartDate
		--FROM 
		--	#TBL_EmployeeTemp EMPSTAGE
		--INNER JOIN
		--	(SELECT 
		--		EmployeeID, 
		--		JobCode, 
		--		JobStartDate 
		--	FROM 
		--		shared_tbls.dbo.view_Employee) JC
		--ON 
		--	EMPSTAGE.EmployeeID = JC.EmployeeID
		--	AND
		--	EMPSTAGE.JobCode = JC.JobCode
		--WHERE 
		--	EMPSTAGE.JobCode <> '';
			
		-- UPDATE TECH CELL PHONE NUMBERS AND PROVIDERS FROM TPMS
		UPDATE
			EMPSTAGE 
		SET
			MobilePhone = TPMSPHONE.MobilePhone
			,MobilePhoneProvider = TPMSPHONE.MobilePhoneProvider
		FROM 
			#TBL_EmployeeTemp EMPSTAGE
		INNER JOIN
			(SELECT
				EnterpriseID
				,MobilePhoneNumber AS MobilePhone
				,CASE 
					WHEN EmailAddress LIKE '%sprint%' THEN 'SPRINT'
					WHEN EmailAddress LIKE '%vzw%' OR EmailAddress LIKE '%vtext%' THEN 'VERIZON'
					WHEN EmailAddress LIKE '%att%' THEN 'ATT'
					WHEN EmailAddress LIKE '%tmo%' THEN 'TMOBILE'
					WHEN (EmailAddress NOT LIKE '%sprint%' AND EmailAddress NOT LIKE '%att%' AND EmailAddress NOT LIKE '%vtext%' AND EmailAddress NOT LIKE '%vzw%' AND EmailAddress NOT LIKE '%tmo%') OR EmailAddress IS NULL THEN ''
				END AS MobilePhoneProvider
			FROM
				Employee.dbo.TBL_AIMEmployee
			WHERE 
				MobilePhoneNumber <> ''
				AND
				MobilePhoneNumber IS NOT NULL) TPMSPHONE
		ON 
			EMPSTAGE.EnterpriseID = TPMSPHONE.EnterpriseID;
			
		-- UPDATE OFFICE PHONE NUMBER FORMAT
		UPDATE
			EMPSTAGE 
		SET
			OfficePhone = ISNULL(U.clean, '')
		FROM 
			#TBL_EmployeeTemp EMPSTAGE
		CROSS APPLY
		(
			SELECT 
				SUBSTRING(OfficePhone, v.number,1)
			FROM 
				master..spt_values v
			WHERE 
				v.type='P' 
				AND 
				v.number BETWEEN 1 AND LEN(OfficePhone)
				AND 
				SUBSTRING(OfficePhone, v.number,1) LIKE '[0-9]'
			ORDER BY 
				v.number
			FOR XML PATH ('')
		) U(clean)
		WHERE 
			OfficePhone <> '';

		-- UPDATE MOBILE PHONE NUMBER FORMAT
		UPDATE
			EMPSTAGE 
		SET
			MobilePhone = ISNULL(U.clean, '')
		FROM 
			#TBL_EmployeeTemp EMPSTAGE
		CROSS APPLY
		(
			SELECT 
				SUBSTRING(MobilePhone, v.number,1)
			FROM 
				master..spt_values v
			WHERE 
				v.type='P' 
				AND 
				v.number BETWEEN 1 AND LEN(MobilePhone)
				AND 
				SUBSTRING(MobilePhone, v.number,1) LIKE '[0-9]'
			ORDER BY 
				v.number
			FOR XML PATH ('')
		) U(clean)
		WHERE 
			MobilePhone <> '';
		
		-- UPDATE START DATES
		UPDATE
			EMPSTAGE 
		SET
			ManagerStartDate = EMP.ManagerStartDate
			,RowStartDate = EMP.RowStartDate
		FROM 
			#TBL_EmployeeTemp EMPSTAGE
		INNER JOIN
			(SELECT
				EmployeeID
				,ManagerStartDate
				,RowStartDate
			FROM
				Employee.dbo.TBL_Employee) EMP
		ON 
			EMPSTAGE.EmployeeID = EMP.EmployeeID;
		
		-- MERGE DATA
		MERGE 
			Employee.dbo.TBL_Employee AS T
		USING 
			(SELECT
				BusinessUnit
				,FinancialRegion
				,FinancialUnit
				,Region
				,Unit
				,Department
				,SearsDepartment
				,SearsDepartmentName
				,GLDepartment
				,Location
				,LocationDescription
				,PayType
				,PayGroup
				,JobGrade
				,EnterpriseID
				,EmployeeID
				,NPSID
				,SalesID
				,EmployeeStatus
				,EmployeeType
				,LastName
				,FirstName
				,MiddleName
				,FullName
				,DisplayName
				,Email
				,OfficePhone
				,MobilePhone
				,MobilePhoneProvider
				,JobCode
				,JobTitle
				,StartDate
				,JobStartDate
				,TermDate
				,ManagerEnterpriseID
				,ManagerEmployeeID
				,ManagerStartDate
				,RowStartDate
			FROM 
				#TBL_EmployeeTemp) AS S
		ON 
			(T.BusinessUnit = S.BusinessUnit
			AND
			T.FinancialRegion = S.FinancialRegion
			AND
			T.FinancialUnit = S.FinancialUnit
			AND
			T.Region = S.Region
			AND
			T.Unit = S.Unit
			AND
			T.Department = S.Department
			AND
			T.SearsDepartment = S.SearsDepartment
			AND
			T.SearsDepartmentName = S.SearsDepartmentName
			AND
			T.GLDepartment = S.GLDepartment
			AND
			T.Location = S.Location
			AND
			T.LocationDescription = S.LocationDescription
			AND
			T.PayType = S.PayType
			AND
			T.PayGroup = S.PayGroup
			AND
			T.JobGrade = S.JobGrade
			AND
			T.EnterpriseID = S.EnterpriseID
			AND
			T.EmployeeID = S.EmployeeID
			AND
			T.NPSID = S.NPSID
			AND
			T.SalesID = S.SalesID
			AND
			T.EmployeeStatus = S.EmployeeStatus
			AND
			T.EmployeeType = S.EmployeeType
			AND
			T.LastName = S.LastName
			AND
			T.FirstName = S.FirstName
			AND
			T.MiddleName = S.MiddleName
			AND
			T.FullName = S.FullName
			AND
			T.DisplayName = S.DisplayName
			AND
			T.Email = S.Email
			AND
			T.OfficePhone = S.OfficePhone
			AND
			T.MobilePhone = S.MobilePhone
			AND
			T.MobilePhoneProvider = S.MobilePhoneProvider
			AND
			T.JobCode = S.JobCode
			AND
			T.JobTitle = S.JobTitle
			AND
			T.StartDate = S.StartDate
			AND
			T.JobStartDate = S.JobStartDate
			AND
			T.TermDate = S.TermDate
			AND
			T.ManagerEnterpriseID = S.ManagerEnterpriseID
			AND
			T.ManagerEmployeeID = S.ManagerEmployeeID)
		WHEN NOT MATCHED BY TARGET THEN
			INSERT 
				(UniqueID
				,BusinessUnit
				,FinancialRegion
				,FinancialUnit
				,Region
				,Unit
				,Department
				,SearsDepartment
				,SearsDepartmentName
				,GLDepartment
				,Location
				,LocationDescription
				,PayType
				,PayGroup
				,JobGrade
				,EnterpriseID
				,EmployeeID
				,NPSID
				,SalesID
				,EmployeeStatus
				,EmployeeType
				,LastName
				,FirstName
				,MiddleName
				,FullName
				,DisplayName
				,Email
				,OfficePhone
				,MobilePhone
				,MobilePhoneProvider
				,JobCode
				,JobTitle
				,StartDate
				,JobStartDate
				,TermDate
				,ManagerEnterpriseID
				,ManagerEmployeeID
				,ManagerStartDate
				,RowStartDate)
			VALUES
				(NEWID()
				,S.BusinessUnit
				,S.FinancialRegion
				,S.FinancialUnit
				,S.Region
				,S.Unit
				,S.Department
				,S.SearsDepartment
				,S.SearsDepartmentName
				,S.GLDepartment
				,S.Location
				,S.LocationDescription
				,S.PayType
				,S.PayGroup
				,S.JobGrade
				,S.EnterpriseID
				,S.EmployeeID
				,S.NPSID
				,S.SalesID
				,S.EmployeeStatus
				,S.EmployeeType
				,S.LastName
				,S.FirstName
				,S.MiddleName
				,S.FullName
				,S.DisplayName
				,S.Email
				,S.OfficePhone
				,S.MobilePhone
				,S.MobilePhoneProvider
				,S.JobCode
				,S.JobTitle
				,S.StartDate
				,S.JobStartDate
				,S.TermDate
				,S.ManagerEnterpriseID
				,S.ManagerEmployeeID
				,S.ManagerStartDate
				,S.RowStartDate)
		WHEN NOT MATCHED BY SOURCE THEN
			UPDATE 
			SET 
				T.RowEndDate = GETDATE() - 1;
		
		IF object_id('tempdb..#TBL_EmployeeTemp') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_EmployeeTemp;
		END
		
	END



GO
/****** Object:  StoredProcedure [dbo].[PROC_NPSBaseUnitImport]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- ==========================================================================================
-- Author:		Aaron Cote
-- Create date: 08/30/2015
-- Description:	Imports unit data from HS_DAILY_VIEWS.NPSXTBU
-- ==========================================================================================
CREATE PROCEDURE [dbo].[PROC_NPSBaseUnitImport]

AS
	BEGIN
	
		DECLARE @querystring NVARCHAR(MAX) = NULL;
		
		IF object_id('tempdb..#TBL_NPSBaseUnitTemp') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_NPSBaseUnitTemp;
		END
		
		CREATE TABLE 
			#TBL_NPSBaseUnitTemp
			(Unit [char](7) NOT NULL,
			 UnitTypeCode [char](3) NOT NULL,
			 UnitName [varchar](27) NOT NULL,
			 UnitStreetAddressLine1 [varchar](30) NOT NULL,
			 UnitStreetAddressLine2 [varchar](30) NOT NULL,
			 UnitCityName [varchar](20) NOT NULL,
			 UnitStateCode [char](2) NOT NULL,
			 ZIPCode [char](5) NOT NULL,
			 ZIPPlus4Code [char](4) NOT NULL,
			 PartDepot [char](4) NOT NULL,
			 Field [char](7) NOT NULL,
			 Region [char](7) NULL,
			 CentralServiceUnit [char](7) NOT NULL,
			 AlternateRepairUnit [char](7) NOT NULL,
		     PATelemarketingPriceLevelCode [decimal](2, 1) NOT NULL,
			 MarketingUnitInboundTelephone [char](10) NOT NULL,
			 TaxingGeoCode [char](9) NOT NULL,
			 BatchUpdateDate [date] NOT NULL,
			 TaxingCountyCode [char](3) NOT NULL);
		
		
		SET @querystring = 'INSERT INTO #TBL_NPSBaseUnitTemp (Unit, UnitTypeCode, UnitName, UnitStreetAddressLine1, UnitStreetAddressLine2, UnitCityName, UnitStateCode, ZIPCode, ZIPPlus4Code, PartDepot, Field, Region,CentralServiceUnit, AlternateRepairUnit, PATelemarketingPriceLevelCode, MarketingUnitInboundTelephone, TaxingGeoCode, BatchUpdateDate, TaxingCountyCode)'

		SET @querystring = @querystring + ' SELECT UN_NO, UN_TYP_CD, UN_NM, UN_LN1_AD, UN_LN2_AD, UN_CTY_NM, UN_STE_CD, ZIP_CD, ZIP_SUF_CD, PRT_DPT_NO, FLD_NO, RGN_NO, CTR_SVC_UN_NO, ALT_RPR_UN_NO, MKT_PRC_LVL_CD,MKT_UN_INB_PHN_NO, TAX_GEO_CD, BCH_UPD_DT, TAX_CNY_CD FROM OPENQUERY(TERADATAPROD, '

		SET @querystring = @querystring + ' ''SELECT UN_NO, UN_TYP_CD, UN_NM, UN_LN1_AD, UN_LN2_AD, UN_CTY_NM, UN_STE_CD, ZIP_CD, ZIP_SUF_CD, PRT_DPT_NO, FLD_NO, RGN_NO, CTR_SVC_UN_NO, ALT_RPR_UN_NO, MKT_PRC_LVL_CD,MKT_UN_INB_PHN_NO, TAX_GEO_CD, BCH_UPD_DT, TAX_CNY_CD FROM HS_DAILY_VIEWS.NPSXTBU;'''

		SET @querystring = @querystring + ')'
		
		-- Populate temp table.
		EXEC(@querystring);
		
		-- Clean up temp table
		UPDATE 
			#TBL_NPSBaseUnitTemp
		SET
			BatchUpdateDate = '1900-01-01'
		WHERE
			BatchUpdateDate = '0001-01-01';
		
		-- Merge into primary table
		MERGE 
			Employee.dbo.TBL_NPSBaseUnit AS T
		USING 
			(SELECT 
				Unit
				,UnitTypeCode
				,UnitName
				,UnitStreetAddressLine1
				,UnitStreetAddressLine2
				,UnitCityName
				,UnitStateCode
				,ZIPCode
				,ZIPPlus4Code
				,PartDepot
				,Field
				,Region
				,CentralServiceUnit
				,AlternateRepairUnit
				,PATelemarketingPriceLevelCode
				,MarketingUnitInboundTelephone
				,TaxingGeoCode
				,BatchUpdateDate
				,TaxingCountyCode
				,GETDATE() AS LastUpdateTimestamp
			FROM 
				#TBL_NPSBaseUnitTemp) AS S
		ON 
			(T.Unit = S.Unit)
		WHEN MATCHED THEN
			UPDATE 
			SET 
				 T.UnitTypeCode = S.UnitTypeCode
				,T.UnitName = S.UnitName
				,T.UnitStreetAddressLine1 = S.UnitStreetAddressLine1
				,T.UnitStreetAddressLine2 = S.UnitStreetAddressLine2
				,T.UnitCityName = S.UnitCityName
				,T.UnitStateCode = S.UnitStateCode
				,T.ZIPCode = S.ZIPCode
				,T.ZIPPlus4Code = S.ZIPPlus4Code
				,T.PartDepot = S.PartDepot
				,T.Field = S.Field
				,T.CentralServiceUnit = S.CentralServiceUnit
				,T.AlternateRepairUnit = S.AlternateRepairUnit
				,T.PATelemarketingPriceLevelCode = S.PATelemarketingPriceLevelCode
				,T.MarketingUnitInboundTelephone = S.MarketingUnitInboundTelephone
				,T.TaxingGeoCode = S.TaxingGeoCode
				,T.BatchUpdateDate = S.BatchUpdateDate
				,T.TaxingCountyCode = S.TaxingCountyCode
				,T.LastUpdateTimestamp = S.LastUpdateTimestamp			
		WHEN NOT MATCHED BY TARGET THEN
			INSERT 
				(Unit
				,UnitTypeCode
				,UnitName
				,UnitStreetAddressLine1
				,UnitStreetAddressLine2
				,UnitCityName
				,UnitStateCode
				,ZIPCode
				,ZIPPlus4Code
				,PartDepot
				,Field
				,CentralServiceUnit
				,AlternateRepairUnit
				,PATelemarketingPriceLevelCode
				,MarketingUnitInboundTelephone
				,TaxingGeoCode
				,BatchUpdateDate
				,TaxingCountyCode
				,LastUpdateTimestamp)
			VALUES 
				(S.Unit
				,S.UnitTypeCode
				,S.UnitName
				,S.UnitStreetAddressLine1
				,S.UnitStreetAddressLine2
				,S.UnitCityName
				,S.UnitStateCode
				,S.ZIPCode
				,S.ZIPPlus4Code
				,S.PartDepot
				,S.Field
				,S.CentralServiceUnit
				,S.AlternateRepairUnit
				,S.PATelemarketingPriceLevelCode
				,S.MarketingUnitInboundTelephone
				,S.TaxingGeoCode
				,S.BatchUpdateDate
				,S.TaxingCountyCode
				,S.LastUpdateTimestamp)
		WHEN NOT MATCHED BY SOURCE THEN
			DELETE;
				
		IF object_id('tempdb..#TBL_NPSBaseUnitTemp') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_NPSBaseUnitTemp;
		END
END
	
	












GO
/****** Object:  StoredProcedure [dbo].[PROC_NPSEmployeeImport]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Aaron Cote
-- Create date: 8/30/2015
-- Description: Imports the NPS Employee data
-- =============================================
CREATE PROCEDURE [dbo].[PROC_NPSEmployeeImport]

AS
	BEGIN
	
		DECLARE @querystring NVARCHAR(MAX) = NULL;
		
		IF object_id('tempdb..#TBL_NPSEmployeeTemp') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_NPSEmployeeTemp;
		END
		
		CREATE TABLE 
			#TBL_NPSEmployeeTemp
			(Unit [varchar] (7) NOT NULL,
			NPSID [varchar](7) NOT NULL,
			SocialSecurityID [varchar](10) NULL,
			EnterpriseID [varchar](28) NULL,
			LastName [varchar](100) NULL,
			FirstName [varchar](100) NULL,
			MiddleName [varchar](50) NULL,
			ClassificationWorkCode [varchar](1) NULL,
			ServiceDate [varchar](30) NULL,
			TermDate [varchar](50) NULL,
			WorkShiftCode [varchar](2) NULL,
			AddDeleteCode [varchar](2) NULL,
			ManagerEnterpriseID [varchar](28) NULL,
			ManagerEmployeeID [varchar](14) NULL,
			JobCode [varchar](10) NULL,
			HomePhone [varchar](10) NULL,
			PagerPhone [varchar](10) NULL,
			SecurityCode [varchar](10) NULL,
			BranchUnit [varchar](7) NULL);
		
		
		SET @querystring = 'INSERT INTO #TBL_NPSEmployeeTemp ([Unit],[NPSID],[SocialSecurityID],[EnterpriseID],[LastName],[FirstName],[MiddleName],[ClassificationWorkCode],[ServiceDate],[TermDate],[WorkShiftCode],[AddDeleteCode],[ManagerEnterpriseID],[ManagerEmployeeID],[JobCode],[HomePhone],[PagerPhone],[SecurityCode],[BranchUnit])'

		SET @querystring = @querystring + ' SELECT svc_un_no, emp_id_no ,emp_fic_no ,emp_rac_id_no ,emp_lst_nm ,emp_fst_nm ,emp_mid_inl_nm ,emp_clf_wrk_cd ,emp_svc_dt ,emp_trm_dt ,wrk_shf_cd ,emp_add_del_cd ,spr_emp_id_1_no ,spr_emp_id_2_no ,job_skl_cd ,emp_hme_phn_no ,emp_pgr_phn_no ,emp_sec_cd ,br_svc_un_no FROM OPENQUERY(TERADATAPROD, '

		SET @querystring = @querystring + ' ''SELECT svc_un_no, emp_id_no ,emp_fic_no ,emp_rac_id_no ,emp_lst_nm ,emp_fst_nm ,emp_mid_inl_nm ,emp_clf_wrk_cd ,emp_svc_dt ,emp_trm_dt ,wrk_shf_cd ,emp_add_del_cd ,spr_emp_id_1_no ,spr_emp_id_2_no ,job_skl_cd ,emp_hme_phn_no ,emp_pgr_phn_no ,emp_sec_cd ,br_svc_un_no FROM HS_DAILY_VIEWS.NPSXTEI;'''

		SET @querystring = @querystring + ')'
		
		-- Populate temp table.
		EXEC(@querystring);
		
		
		
		-- Merge into primary table
		MERGE 
			Employee.dbo.TBL_NPSEmployee AS T
		USING 
			(SELECT 
			   Unit
			  ,NPSID
			  ,SocialSecurityID
			  ,EnterpriseID
			  ,LastName
			  ,FirstName
			  ,MiddleName
			  ,ClassificationWorkCode
			  ,ServiceDate
			  ,TermDate
			  ,WorkShiftCode
			  ,AddDeleteCode
			  ,ManagerEnterpriseID
			  ,ManagerEmployeeID
			  ,JobCode
			  ,HomePhone
			  ,PagerPhone
			  ,SecurityCode
			  ,BranchUnit
			FROM 
				#TBL_NPSEmployeeTemp) AS S
		ON 
			(T.Unit = S.Unit 
			AND
			T.NPSID = S.NPSID)
			
		WHEN MATCHED THEN
			UPDATE 
			SET
				T.SocialSecurityID = S.SocialSecurityID
				,T.EnterpriseID = S.EnterpriseID
				,T.LastName = S.LastName
				,T.FirstName = S.FirstName
				,T.MiddleName = S.MiddleName
				,T.ClassificationWorkCode = S.ClassificationWorkCode
				,T.ServiceDate = S.ServiceDate
				,T.TermDate = S.TermDate
				,T.WorkShiftCode = S.WorkShiftCode
				,T.AddDeleteCode = S.AddDeleteCode
				,T.ManagerEnterpriseID = S.ManagerEnterpriseID
				,T.ManagerEmployeeID = S.ManagerEmployeeID
				,T.JobCode = S.JobCode
				,T.HomePhone = S.HomePhone
				,T.PagerPhone = S.PagerPhone
				,T.SecurityCode = S.SecurityCode
				,T.BranchUnit = S.BranchUnit
		WHEN NOT MATCHED BY TARGET THEN
			INSERT 
				(Unit
				,NPSID
				,SocialSecurityID
				,EnterpriseID
				,LastName
				,FirstName
				,MiddleName
				,ClassificationWorkCode
				,ServiceDate
				,TermDate
				,WorkShiftCode
				,AddDeleteCode
				,ManagerEnterpriseID
				,ManagerEmployeeID
				,JobCode
				,HomePhone
				,PagerPhone
				,SecurityCode
				,BranchUnit)
			VALUES 
				(S.Unit
				,S.NPSID
				,S.SocialSecurityID
				,S.EnterpriseID
				,S.LastName
				,S.FirstName
				,S.MiddleName
				,S.ClassificationWorkCode
				,S.ServiceDate
				,S.TermDate
				,S.WorkShiftCode
				,S.AddDeleteCode
				,S.ManagerEnterpriseID
				,S.ManagerEmployeeID
				,S.JobCode
				,S.HomePhone
				,S.PagerPhone
				,S.SecurityCode
				,S.BranchUnit);
				
		IF object_id('tempdb..#TBL_NPSEmployeeTemp') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_NPSEmployeeTemp;
		END
END
	
	








GO
/****** Object:  StoredProcedure [dbo].[PROC_NPSTechnicianPayImport]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PROC_NPSTechnicianPayImport]

AS
	BEGIN
	
		DECLARE @querystring NVARCHAR(MAX) = NULL;
		
		IF object_id('tempdb..#TBL_NPSTechnicianPay') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_NPSTechnicianPayTemp;
		END
		
		CREATE TABLE 
			#TBL_NPSTechnicianPayTemp
			(Unit [varchar] (7) NOT NULL,
			NPSID [varchar](7) NOT NULL,
			EmployeeID [varchar](11) NULL,
			EmployeePay [decimal](7, 3) NULL);
		
		
		SET @querystring = 'INSERT INTO #TBL_NPSTechnicianPayTemp (Unit, NPSID, EmployeeID, EmployeePay)'

		SET @querystring = @querystring + ' SELECT svc_un_no, svc_tec_emp_id_no, crp_emp_id_no, tec_hry_py_rt FROM OPENQUERY(TERADATAPROD, '

		SET @querystring = @querystring + ' ''SELECT svc_un_no, svc_tec_emp_id_no, crp_emp_id_no, tec_hry_py_rt FROM HS_DAILY_VIEWS.NPSXTPY;'''

		SET @querystring = @querystring + ')'
		
		-- Populate temp table.
		EXEC(@querystring);
		
		
		
		-- Merge into primary table
		MERGE 
			Employee.dbo.TBL_NPSTechnicianPay AS T
		USING 
			(SELECT
			   Unit
			  ,NPSID			  
			  ,EmployeeID
			  ,EmployeePay			 
			FROM 
				#TBL_NPSTechnicianPayTemp) AS S
		ON 
			(T.Unit = S.Unit 
			AND
			T.EmployeeID = S.NPSID)
			
		WHEN MATCHED THEN
			UPDATE 
			SET
				T.Unit = S.Unit
				,T.NPSID = S.NPSID
				,T.EmployeeID = S.EmployeeID
				,T.EmployeePay = S.EmployeePay				
		WHEN NOT MATCHED BY TARGET THEN
			INSERT 
				(Unit
				,NPSID
				,EmployeeID				
				,EmployeePay)
			VALUES 
				(S.Unit
				,S.NPSID
				,S.EmployeeID
				,S.EmployeePay);
				
		IF object_id('tempdb..#TBL_NPSTechnicianPayTemp') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_NPSTechnicianPayTemp;
		END
END
	
	











GO
/****** Object:  StoredProcedure [dbo].[PROC_SSTTechnicianImport]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Aaron Cote
-- Create date: 2015-10-14
-- Description:	Imports the SST technician data
-- =============================================
CREATE PROCEDURE [dbo].[PROC_SSTTechnicianImport]

AS
	BEGIN
	
	-- Merge into primary table
	MERGE 
		Employee.dbo.TBL_SSTTechnician AS T
	USING 
		(SELECT 
			USR_ID
			,PT_IP_AD_TX
			,CUR_SFW_VRS_ID_NO
			,PND_SFW_VRS_ID_NO
			,SVC_UN_NO
			,EMP_ID_NO
			,SSN_NO
			,SU_DAT_DWD_FL
			,TM_ZN_FAC_CD
			,SND_AUT_SYN_FL
			,WLS_SYN_FL
			,HAL_FL
			,ULO_LOG_DT
			,CSAT_FL
			,SYWR_FL
			,LMC_FL
			,EMR_ORD_FL
			,AIM_FL
			,PR_LOTTO_FL
			,EMPLID
			,LOCN
			,LMCDMZ_FL
			,LMCTCP_FL
			,NEWSST_FL
			,TI_FL
		FROM
			TRYDVSQL1.shared_tbls.sst.SSTXTPT) AS S
	ON 
		(T.EnterpriseID = S.USR_ID)
		
	WHEN MATCHED THEN
		UPDATE 
		SET
			T.Unit = S.SVC_UN_NO
			,T.Location = S.LOCN
			,T.EnterpriseID = S.USR_ID
			,T.EmployeeID = S.EMPLID
			,T.NPSID = S.EMP_ID_NO
			,T.IPAddress = S.PT_IP_AD_TX
			,T.CurrentSoftwareVersion = S.CUR_SFW_VRS_ID_NO
			,T.PendingSoftwareVersion = S.PND_SFW_VRS_ID_NO
			,T.SocialSecurityNumber = S.SSN_NO
			,T.SupportDataDownloaded = S.SU_DAT_DWD_FL
			,T.TimeZoneFactor = S.TM_ZN_FAC_CD
			,T.SendAutoSyn = S.SND_AUT_SYN_FL
			,T.WLSSyn = S.WLS_SYN_FL
			,T.HAL = S.HAL_FL
			,T.ULOLogDate = S.ULO_LOG_DT
			,T.CSAT = S.CSAT_FL
			,T.SYWR = S.SYWR_FL
			,T.LMC = S.LMC_FL
			,T.EMROORD = S.EMR_ORD_FL
			,T.AIM = S.AIM_FL
			,T.PRLotto = S.PR_LOTTO_FL
			,T.LMCDMZ = S.LMCDMZ_FL
			,T.LMCTCP = S.LMCTCP_FL
			,T.NewSST = S.NEWSST_FL
			,T.TI = S.TI_FL	
	WHEN NOT MATCHED BY TARGET THEN
		INSERT 
			(Unit
			,Location
			,EnterpriseID
			,EmployeeID
			,NPSID
			,IPAddress
			,CurrentSoftwareVersion
			,PendingSoftwareVersion
			,SocialSecurityNumber
			,SupportDataDownloaded
			,TimeZoneFactor
			,SendAutoSyn
			,WLSSyn
			,HAL
			,ULOLogDate
			,CSAT
			,SYWR
			,LMC
			,EMROORD
			,AIM
			,PRLotto
			,LMCDMZ
			,LMCTCP
			,NewSST
			,TI)
		VALUES 
			(S.SVC_UN_NO
			,S.LOCN
			,S.USR_ID
			,S.EMPLID
			,S.EMP_ID_NO
			,S.PT_IP_AD_TX
			,S.CUR_SFW_VRS_ID_NO
			,S.PND_SFW_VRS_ID_NO
			,S.SSN_NO
			,S.SU_DAT_DWD_FL
			,S.TM_ZN_FAC_CD
			,S.SND_AUT_SYN_FL
			,S.WLS_SYN_FL
			,S.HAL_FL
			,S.ULO_LOG_DT
			,S.CSAT_FL
			,S.SYWR_FL
			,S.LMC_FL
			,S.EMR_ORD_FL
			,S.AIM_FL
			,S.PR_LOTTO_FL
			,S.LMCDMZ_FL
			,S.LMCTCP_FL
			,S.NEWSST_FL
			,S.TI_FL);
				
END
	
	









GO
/****** Object:  StoredProcedure [dbo].[PROC_TechRoutesImport]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Aaron Cote
-- Create date: 2015-12-10
-- Description:	Imports the current tech routes
-- =============================================
CREATE PROCEDURE [dbo].[PROC_TechRoutesImport]

AS
	BEGIN
	
	-- Merge into primary table
	MERGE 
		Employee.dbo.TBL_TechRoutes AS T
	USING 
		(SELECT
			SISCHED.UnitNumber AS Unit
			,NPSID AS TechID
			,SISCHED.EnterpriseID
			,SISCHED.ServiceOrderNumber AS ServiceOrder
			,SCHEDULE_STATUS AS StatusCode
			,CONTRACT_START AS ScheduleFromTime
			,CONTRACT_COMPLETION AS ScheduleToTime
			,Expected_Start AS EstimatedStart
			,LastSeenDate AS LastUpdate
		FROM 
			TRYDVSQL1.shared_tbls.views.si_schedule_backup SISCHED
		-- Service order may be listed more than once, so make sure latest one
		-- is being selected.
		INNER JOIN
			(SELECT
				UnitNumber
				,ServiceOrderNumber
				,MAX(JOB_NO) AS MaxJob
			FROM
				TRYDVSQL1.shared_tbls.views.si_schedule_backup
			WHERE
				AttemptType = 'REG'
			GROUP BY
				UnitNumber
				,ServiceOrderNumber) SISCHEDMAX
		ON
			SISCHED.UnitNumber = SISCHEDMAX.UnitNumber
			AND
			SISCHED.ServiceOrderNumber = SISCHEDMAX.ServiceOrderNumber
			AND
			SISCHED.JOB_NO = SISCHEDMAX.MaxJob
		INNER JOIN
			(SELECT 
				EnterpriseID
				,CASE 
					WHEN NPSID IS NULL THEN SalesID
					ELSE NPSID
				END AS NPSID
			FROM
				Employee.dbo.view_Employee
			WHERE 
				EnterpriseID <> '') EMP
		ON
			SISCHED.EnterpriseID = EMP.EnterpriseID
		WHERE
			Contract_Start BETWEEN CAST(GETDATE() AS DATE) AND CAST(GETDATE() + 30 AS DATE)
			AND
			SISCHED.EnterpriseID IS NOT NULL
			AND
			AttemptType = 'REG'
			AND
			SISCHED.UnitNumber NOT IN ('0008505', '0008206')) AS S
	ON 
		(T.EnterpriseID = S.EnterpriseID
		AND
		T.ServiceOrder = S.ServiceOrder)
		
	WHEN MATCHED THEN
		UPDATE 
		SET
			T.Unit = S.Unit
			,T.TechID = S.TechID
			,T.StatusCode = S.StatusCode
			,T.ScheduleFromTime = S.ScheduleFromTime
			,T.ScheduleToTime = S.ScheduleToTime
			,T.EstimatedStart = S.EstimatedStart
			,T.LastUpdate = S.LastUpdate
	WHEN NOT MATCHED BY TARGET THEN
		INSERT 
			(Unit
			,TechID
			,EnterpriseID
			,ServiceOrder
			,StatusCode
			,ScheduleFromTime
			,ScheduleToTime
			,EstimatedStart
			,LastUpdate)
		VALUES 
			(S.Unit
			,S.TechID
			,S.EnterpriseID
			,S.ServiceOrder
			,S.StatusCode
			,S.ScheduleFromTime
			,S.ScheduleToTime
			,S.EstimatedStart
			,S.LastUpdate)
	WHEN NOT MATCHED BY SOURCE THEN
		DELETE;
				
END
	
	










GO
/****** Object:  StoredProcedure [dbo].[PROC_UnitCrossReferenceGeocodingInsert]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







-- =============================================
-- Author:		Aaron Cote
-- Create date: 01/26/2017
-- Description:	For saving the geocoordinates of a unit address
-- =============================================
CREATE PROCEDURE [dbo].[PROC_UnitCrossReferenceGeocodingInsert]
	@Unit char(7) = NULL,
	@InputAddress varchar(500) = NULL,
	@PlaceID varchar(300) = NULL,
	@StreetNumber varchar(300) = NULL,
	@StreetNumberLong varchar(300) = NULL,
	@Route varchar(300) = NULL,
	@RouteLong varchar(300) = NULL,
	@SubPremise varchar(300) = NULL,
	@SubPremiseLong varchar(300) = NULL,
	@Neighborhood varchar(300) = NULL,
	@NeighborhoodLong varchar(300) = NULL,
	@Locality varchar(300) = NULL,
	@LocalityLong varchar(300) = NULL,
	@AdministrativeAreaLevelThree varchar(300) = NULL,
	@AdministrativeAreaLevelThreeLong varchar(300) = NULL,
	@AdministrativeAreaLevelTwo varchar(300) = NULL,
	@AdministrativeAreaLevelTwoLong varchar(300) = NULL,
	@AdministrativeAreaLevelOne varchar(300) = NULL,
	@AdministrativeAreaLevelOneLong varchar(300) = NULL,
	@Country varchar(300) = NULL,
	@CountryLong varchar(300) = NULL,
	@PostalCode varchar(300) = NULL,
	@PostalCodeLong varchar(300) = NULL,
	@PostalCodeSuffix varchar(300) = NULL,
	@PostalCodeSuffixLong varchar(300) = NULL,
	@FormattedAddress varchar(300) = NULL,
	@LocationType varchar(300) = NULL,
	@Latitude decimal(17,13) = NULL,
	@Longitude decimal(17,13) = NULL,
	@NorthEastLatitude decimal(17,13) = NULL,
	@NorthEastLongitude decimal(17,13) = NULL,
	@SouthWestLatitude decimal(17,13) = NULL,
	@SouthWestLongitude decimal(17,13) = NULL,

	@errorcode VARCHAR(150) = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- Interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- Merge into primary table
	INSERT INTO
		Employee.dbo.TBL_UnitCrossReferenceGeocoding
		(Unit
		,InputAddress
		,PlaceID
		,StreetNumber
		,StreetNumberLong
		,[Route]
		,RouteLong
		,SubPremise
		,SubPremiseLong
		,Neighborhood
		,NeighborhoodLong
		,Locality
		,LocalityLong
		,AdministrativeAreaLevelThree
		,AdministrativeAreaLevelThreeLong
		,AdministrativeAreaLevelTwo
		,AdministrativeAreaLevelTwoLong
		,AdministrativeAreaLevelOne
		,AdministrativeAreaLevelOneLong
		,Country
		,CountryLong
		,PostalCode
		,PostalCodeLong
		,PostalCodeSuffix
		,PostalCodeSuffixLong
		,FormattedAddress
		,LocationType
		,Latitude
		,Longitude
		,Location
		,NorthEastLatitude
		,NorthEastLongitude
		,SouthWestLatitude
		,SouthWestLongitude
		,InsertDate)
	VALUES
		(@Unit
		,@InputAddress
		,@PlaceID
		,@StreetNumber
		,@StreetNumberLong
		,@Route
		,@RouteLong
		,@SubPremise
		,@SubPremiseLong
		,@Neighborhood
		,@NeighborhoodLong
		,@Locality
		,@LocalityLong
		,@AdministrativeAreaLevelThree
		,@AdministrativeAreaLevelThreeLong
		,@AdministrativeAreaLevelTwo
		,@AdministrativeAreaLevelTwoLong
		,@AdministrativeAreaLevelOne
		,@AdministrativeAreaLevelOneLong
		,@Country
		,@CountryLong
		,@PostalCode
		,@PostalCodeLong
		,@PostalCodeSuffix
		,@PostalCodeSuffixLong
		,@FormattedAddress
		,@LocationType
		,@Latitude
		,@Longitude
		,geography::Point(@Latitude, @Longitude, 4326)
		,@NorthEastLatitude
		,@NorthEastLongitude
		,@SouthWestLatitude
		,@SouthWestLongitude
		,GETDATE())
END






GO
/****** Object:  Table [dbo].[_DeleteOn20181101_TBL_VehicleLocation]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[_DeleteOn20181101_TBL_VehicleLocation](
	[VehicleDongleMacAddress] [varchar](12) NOT NULL,
	[PhoneNumber] [varchar](11) NULL,
	[LocationTime] [datetime] NOT NULL,
	[EnterpriseIDKey] [int] NULL,
	[EnterpriseID] [varchar](11) NULL,
	[ActiveTrip] [bit] NULL,
	[Longitude] [decimal](10, 5) NULL,
	[Latitude] [decimal](10, 5) NULL,
	[Heading] [decimal](10, 5) NULL,
	[Speed] [decimal](10, 5) NULL,
	[InsertDateTime] [datetime] NULL,
	[ModifiedDateTime] [datetime] NULL,
 CONSTRAINT [PK_TBL_VehicleLocation] PRIMARY KEY CLUSTERED 
(
	[VehicleDongleMacAddress] ASC,
	[LocationTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[a_Associate_History]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[a_Associate_History](
	[ldap_ID] [char](7) NOT NULL,
	[fname] [char](15) NULL,
	[lname] [char](30) NULL,
	[lead_name] [char](15) NULL,
	[rec_RFS] [bit] NULL,
	[rec_CWO] [bit] NULL,
	[ord_CWO] [bit] NULL,
	[ord_TCH] [bit] NULL,
	[default_dept] [char](10) NULL,
	[section] [char](10) NULL,
	[date] [datetime] NOT NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_Associate_History] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[a_Associate_Master]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[a_Associate_Master](
	[LDAP_ID] [char](8) NOT NULL,
	[fname] [char](20) NULL,
	[lname] [char](35) NULL,
	[lead_name] [char](15) NULL,
	[default_dept] [char](10) NULL,
	[REC_Default] [char](10) NULL,
	[PAW_Default] [char](10) NULL,
	[ORD_Default] [char](10) NULL,
	[section] [char](10) NULL,
	[REC_CWO] [numeric](5, 2) NULL,
	[REC_RFS] [numeric](5, 2) NULL,
	[REC_VEN] [numeric](5, 2) NULL,
	[PAW_IRC] [numeric](5, 2) NULL,
	[PAW_PDC] [numeric](5, 2) NULL,
	[ORD_TECH] [numeric](5, 2) NULL,
	[ORD_CWO] [numeric](5, 2) NULL,
	[PACKING_TRANS] [numeric](5, 2) NULL,
	[SHIPPING] [numeric](5, 2) NULL,
	[PDC] [char](5) NULL,
	[is_id_in_directory] [char](1) NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_Associate_Master] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[a_Badge_ID_LDAP_ID_Pay_Rates]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[a_Badge_ID_LDAP_ID_Pay_Rates](
	[PDC] [char](5) NULL,
	[Empl_ID] [char](15) NULL,
	[LDAP_ID] [char](8) NULL,
	[Pay_rate] [numeric](10, 2) NULL,
	[Pay_rate_save] [numeric](10, 2) NULL,
	[Last_update] [datetime] NULL,
	[last_pay_raise] [datetime] NULL,
	[Head_Cnt_Code] [char](6) NULL,
	[Type_Emp] [char](20) NULL,
	[fname] [varchar](50) NULL,
	[lname] [varchar](50) NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_Badge_ID_LDAP_ID_Pay_Rates] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[a_Data_Paths]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[a_Data_Paths](
	[PDC] [char](4) NULL,
	[rpt_path_daily] [char](100) NULL,
	[rpt_path_arhive] [char](100) NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_Data_Paths] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[a_Default_Dept]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[a_Default_Dept](
	[Default_Dept] [char](10) NULL,
	[Sub_Dept] [char](10) NULL,
	[PDC] [char](5) NULL,
	[Goal_SubDept] [int] NULL,
	[Goal_Dept] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_Default_Dept] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[a_Exception_Dept_Insert_Info]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[a_Exception_Dept_Insert_Info](
	[pdc] [char](5) NULL,
	[ldap_id] [char](8) NULL,
	[department] [char](10) NULL,
	[avg_hrs] [numeric](8, 2) NULL,
	[Table_name] [char](50) NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [nchar](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_Exception_Dept_Insert_Info] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[a_ForeCast_Actual_Factors]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[a_ForeCast_Actual_Factors](
	[PK] [int] IDENTITY(1,1) NOT NULL,
	[PDC] [char](5) NULL,
	[Mth] [char](3) NULL,
	[month_nmbr] [int] NULL,
	[Year] [int] NULL,
	[Forecast_PR_Benefit_Cost] [numeric](18, 2) NULL,
	[Actual_PR_Benefit_Cost] [numeric](18, 2) NULL,
	[Forecast_Support_PR_Cost] [numeric](18, 2) NULL,
	[Actual_Support_PR_Cost] [numeric](18, 2) NULL,
	[ForeCast_Tot_PR_Cost] [numeric](18, 2) NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
 CONSTRAINT [PK_a_ForeCast_Actual_Factors] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[a_Head_Cnt_Code_Forecast]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[a_Head_Cnt_Code_Forecast](
	[head_cnt_code] [char](6) NULL,
	[_8961_Goal] [int] NULL,
	[_8963_Goal] [int] NULL,
	[_8966_Goal] [int] NULL,
	[_8976_Goal] [int] NULL,
	[_8842_Goal] [int] NULL,
	[_8961] [int] NULL,
	[_8963] [int] NULL,
	[_8966] [int] NULL,
	[_8976] [int] NULL,
	[_8942] [int] NULL,
	[_8961_cell] [char](4) NULL,
	[_8963_cell] [char](4) NULL,
	[_8966_cell] [char](4) NULL,
	[_8976_cell] [char](4) NULL,
	[_8842_cell] [char](4) NULL,
	[date] [char](10) NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_Head_Cnt_Code_Forecast] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[a_Head_Cnt_Codes]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[a_Head_Cnt_Codes](
	[Head_Cnt_code] [char](6) NULL,
	[Description] [char](32) NULL,
	[HCC_Goal] [numeric](4, 0) NULL,
	[InOutBoundSupt] [char](8) NULL,
	[found] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_Head_Cnt_Codes] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[a_HR_Data]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[a_HR_Data](
	[PDC] [char](5) NULL,
	[Pay_group] [char](5) NULL,
	[empl_ID] [char](11) NULL,
	[LDAP_ID] [char](8) NULL,
	[Emp_Full_nm] [char](100) NULL,
	[status] [char](1) NULL,
	[job_code] [char](6) NULL,
	[job_title] [char](50) NULL,
	[assigment_date] [char](10) NULL,
	[dept_id] [char](10) NULL,
	[empl_type] [char](1) NULL,
	[full_part_time] [char](1) NULL,
	[salary_plan] [char](7) NULL,
	[grade] [char](7) NULL,
	[pay_rate] [numeric](11, 2) NULL,
	[pay_rate_save] [numeric](11, 2) NULL,
	[last_update] [datetime] NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_HR_Data] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[a_Lead_Master]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[a_Lead_Master](
	[LEAD_NAME] [char](15) NOT NULL,
	[PDC] [char](5) NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_Lead_Master] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[a_RP_Delete_Names]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[a_RP_Delete_Names](
	[FullName] [char](75) NULL,
	[PDC] [char](4) NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_RP_Delete_Names] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[a_RP_Emp_Lists]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[a_RP_Emp_Lists](
	[name] [char](75) NULL,
	[badge_number] [char](15) NULL,
	[Phone] [char](12) NULL,
	[Emp_Type] [char](15) NULL,
	[Hire_Date] [char](12) NULL,
	[Department] [char](35) NULL,
	[Primary_Job] [char](35) NULL,
	[shift_Strategy] [char](15) NULL,
	[PDC] [char](5) NULL,
	[date] [char](10) NULL,
	[LDAP_ID] [char](8) NULL,
	[fname] [char](15) NULL,
	[lname] [char](20) NULL,
	[head_Cnt_Code] [char](7) NULL,
	[type_emp] [char](20) NULL,
	[shift] [char](6) NULL,
	[Head_Count_Code] [char](4) NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_RP_Emp_Lists] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[a_RP_Pay_Summary]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[a_RP_Pay_Summary](
	[weekendingdate] [datetime] NULL,
	[rptruntimestamp] [datetime] NULL,
	[date] [datetime] NULL,
	[pdc] [char](5) NULL,
	[pay_id] [char](15) NULL,
	[ldap_id] [char](8) NULL,
	[fname] [char](20) NULL,
	[lname] [char](20) NULL,
	[head_cnt_code] [char](6) NULL,
	[type_emp] [char](20) NULL,
	[tot_hrs] [numeric](10, 2) NULL,
	[reg_hrs] [numeric](10, 2) NULL,
	[tmp_hrs] [numeric](10, 2) NULL,
	[ovt_hrs] [numeric](10, 2) NULL,
	[dbl_OT_hrs] [numeric](10, 2) NULL,
	[ben_hrs] [numeric](10, 2) NULL,
	[tot_pay] [numeric](10, 2) NULL,
	[reg_pay] [numeric](10, 2) NULL,
	[tmp_pay] [numeric](10, 2) NULL,
	[ovt_pay] [numeric](10, 2) NULL,
	[dbl_OT_Pay] [numeric](10, 2) NULL,
	[ben_pay] [numeric](10, 2) NULL,
	[category_def] [char](20) NULL,
	[pay_rule] [char](25) NULL,
	[pay_rate] [numeric](10, 2) NULL,
	[Head_Count_Code] [char](4) NULL,
	[shift] [char](6) NULL,
	[changed_HCC] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_RP_Pay_Summary] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[a_RP_Pay_Summary_Import_Log]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[a_RP_Pay_Summary_Import_Log](
	[WeekEndingDate] [datetime] NULL,
	[_8842_RptRuntimeStamp] [datetime] NULL,
	[_8940_RptRuntimeStamp] [datetime] NULL,
	[_8961_RptRuntimeStamp] [datetime] NULL,
	[_8963_RptRuntimeStamp] [datetime] NULL,
	[_8976_RptRuntimeStamp] [datetime] NULL,
	[_8966_RptRuntimeStamp] [datetime] NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_RP_Pay_Summary_Import_Log] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[a_RP_PW_Access]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[a_RP_PW_Access](
	[LDAP_ID] [char](8) NULL,
	[Password] [char](25) NULL,
	[pdc] [char](5) NULL,
	[Detail_Access] [bit] NULL,
	[email] [char](35) NULL,
	[send_email] [bit] NULL,
	[JOB] [char](3) NULL,
	[Admin_Rights] [bit] NULL,
	[Missing_FROM_CC_Email] [bit] NULL,
	[Missing_TO_Email] [bit] NULL,
	[Missing_CC_Email] [bit] NULL,
	[Missing_Default_Unit] [char](5) NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_RP_PW_Access] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[a_RP_Temp_Agency_Pay_Rates]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[a_RP_Temp_Agency_Pay_Rates](
	[PAY_RATE] [numeric](10, 2) NULL,
	[Effective_Date] [datetime] NULL,
	[TYPE_EMP] [char](20) NULL,
	[PDC] [char](4) NULL,
	[Previous_Rate] [numeric](10, 2) NULL,
	[Previous_Eff_Date] [datetime] NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_RP_Temp_Agency_Pay_Rates] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[a_Support_Insert_Info]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[a_Support_Insert_Info](
	[pdc] [char](5) NULL,
	[ldap_id] [char](8) NULL,
	[department] [char](10) NULL,
	[avg_hrs] [numeric](8, 2) NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_Support_Insert_Info] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[a_Temp_Pay_Rate_Sears_Benefit_Percent]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[a_Temp_Pay_Rate_Sears_Benefit_Percent](
	[PDC] [char](5) NULL,
	[temp_pay_rate] [decimal](10, 2) NULL,
	[sears_benefit_percent] [decimal](10, 1) NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_Temp_Pay_Rate_Sears_Benefit_Percent] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[a_Temp_Payroll_Tax_Rates]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[a_Temp_Payroll_Tax_Rates](
	[PK] [int] IDENTITY(1,1) NOT NULL,
	[PDC] [char](5) NULL,
	[Type_Emp] [char](20) NULL,
	[tax_percent] [numeric](6, 2) NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
 CONSTRAINT [PK_a_Temp_Payroll_Tax_Rates] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Employee_Info]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Employee_Info](
	[Company] [varchar](3) NULL,
	[Fin_Rgn_Un_No] [varchar](5) NULL,
	[Fin_Ctr_Svc_Un_No] [varchar](5) NULL,
	[Pay_Group] [varchar](5) NULL,
	[NID] [varchar](9) NULL,
	[Empl_ID] [varchar](11) NOT NULL,
	[Emp_Full_Nm] [varchar](100) NULL,
	[Status] [varchar](1) NULL,
	[LDAP_ID] [varchar](8) NULL,
	[Hire_Date] [date] NULL,
	[Job_Code] [varchar](6) NULL,
	[Job_Title] [varchar](50) NULL,
	[Assignment_Date] [date] NULL,
	[Address] [varchar](255) NULL,
	[Apt] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[St] [varchar](2) NULL,
	[Zip] [varchar](50) NULL,
	[Gender] [varchar](1) NULL,
	[Dept_ID] [varchar](10) NULL,
	[Empl_Type] [varchar](1) NULL,
	[Full_Part_Time] [varchar](1) NULL,
	[Salary_Plan] [varchar](7) NULL,
	[Grade] [varchar](7) NULL,
	[GL_Pay_Type] [varchar](50) NULL,
	[K_GL_Dept] [varchar](50) NULL,
	[Pay_Rate] [varbinary](256) NULL,
	[Last_Update] [datetime] NULL,
 CONSTRAINT [PK_Employee_Info] PRIMARY KEY NONCLUSTERED 
(
	[Empl_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EmployeeTechHubTimepunches]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EmployeeTechHubTimepunches](
	[RouteDate] [varchar](50) NULL,
	[RouteTime] [varchar](50) NULL,
	[UserEnterpriseID] [varchar](50) NULL,
	[TechnicianEnterpriseID] [varchar](50) NULL,
	[ServiceUnit] [varchar](50) NULL,
	[EntryType] [varchar](50) NULL,
	[EntryNumber] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[profit_v2]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[profit_v2](
	[acctg_yr] [int] NULL,
	[acctg_yr_mth] [int] NULL,
	[acctg_mth] [int] NULL,
	[acctg_wk] [int] NULL,
	[acctg_yr_wk] [int] NULL,
	[svc_cal_dt] [datetime] NOT NULL,
	[rgn_no] [char](7) NULL,
	[terr_mgr_emp_id] [char](11) NULL,
	[terr_mgr] [varchar](100) NULL,
	[svc_un_fty_id_no] [char](7) NOT NULL,
	[ctr_svc_un_no] [char](7) NULL,
	[ctr_svc_un_no2] [char](7) NULL,
	[iru_id] [char](7) NULL,
	[zip_cd] [char](5) NULL,
	[asa_un_no] [char](7) NULL,
	[dtr_mgr_emp_id] [char](11) NULL,
	[dtr_mgr] [varchar](100) NULL,
	[tech_mgr_emp_id] [char](11) NULL,
	[tech_mgr] [varchar](100) NULL,
	[svc_tec_emp_id_no] [char](7) NULL,
	[svc_tec] [varchar](100) NULL,
	[svc_ord_no] [char](8) NOT NULL,
	[itm_suf_no] [int] NULL,
	[servicer] [char](10) NULL,
	[svc_ogp_cd] [char](1) NULL,
	[nai_cvg_cd] [char](2) NULL,
	[nai_svc_loc] [char](4) NULL,
	[psv_div_ogp_no] [char](3) NULL,
	[psv_mds_cd] [char](12) NULL,
	[mds_sp_cd] [char](10) NULL,
	[tto_thd_pty_fl] [char](1) NULL,
	[tto_thd_pty_id] [char](6) NULL,
	[tto_client_nm] [char](40) NULL,
	[thd_pty_typ] [char](10) NULL,
	[mfg_bnd_nm] [char](25) NULL,
	[paid_calls] [decimal](9, 2) NULL,
	[pa_calls] [decimal](9, 2) NULL,
	[iw_calls] [decimal](9, 2) NULL,
	[interco_calls] [decimal](9, 2) NULL,
	[w2_calls] [decimal](9, 2) NULL,
	[ovfl_1099_calls] [decimal](9, 2) NULL,
	[asa_calls] [decimal](9, 2) NULL,
	[d2c_calls] [decimal](9, 2) NULL,
	[b2b_calls] [decimal](9, 2) NULL,
	[bndiw_calls] [decimal](9, 2) NULL,
	[paid_labor_adj] [decimal](12, 2) NULL,
	[paid_parts_adj] [decimal](12, 2) NULL,
	[b2b_paid_labor_adj] [decimal](12, 2) NULL,
	[b2b_paid_parts_adj] [decimal](12, 2) NULL,
	[iwbnd_paid_labor_adj] [decimal](12, 2) NULL,
	[iwbnd_paid_parts_adj] [decimal](12, 2) NULL,
	[tech_pa_sales_adj] [decimal](12, 2) NULL,
	[iw_trans_cred] [decimal](12, 2) NULL,
	[pa_svc_reimb] [decimal](12, 2) NULL,
	[prod_prot_rev_hw] [decimal](12, 2) NULL,
	[de_cpn_comm] [decimal](12, 2) NULL,
	[misc_inc_a] [decimal](12, 2) NULL,
	[tot_revenue] [decimal](12, 2) NULL,
	[prt_adj_cc_p] [decimal](12, 2) NULL,
	[prt_adj_iw_p] [decimal](12, 2) NULL,
	[prt_adj_pa_p] [decimal](12, 2) NULL,
	[prt_depr_a] [decimal](12, 2) NULL,
	[mrgn_adj_a] [decimal](12, 2) NULL,
	[tot_prts_cost] [decimal](12, 2) NULL,
	[contr_exp_c] [decimal](12, 2) NULL,
	[outside_shop_exp_c] [decimal](12, 2) NULL,
	[comm_pay_exp_a] [decimal](12, 2) NULL,
	[reg_pay_exp_a] [decimal](12, 2) NULL,
	[ovrtm_pay_exp_a] [decimal](12, 2) NULL,
	[training_exp_a] [decimal](12, 2) NULL,
	[supp_pay_a] [decimal](12, 2) NULL,
	[sell_pay_a] [decimal](12, 2) NULL,
	[oth_pay_exp_a] [decimal](12, 2) NULL,
	[assoc_ben_exp_a] [decimal](12, 2) NULL,
	[fuel_exp_a] [decimal](12, 2) NULL,
	[maint_rep_exp_a] [decimal](12, 2) NULL,
	[ins_exp_a] [decimal](12, 2) NULL,
	[auth_repl_r] [decimal](12, 2) NULL,
	[fd_ls_adj_f] [decimal](12, 2) NULL,
	[equip_rent_a] [decimal](12, 2) NULL,
	[policy_adj_a] [decimal](12, 2) NULL,
	[ovrs_shrts_bd_chks_c] [decimal](12, 2) NULL,
	[oth_trans_exp_cst_c] [decimal](12, 2) NULL,
	[deliv_cart_a] [decimal](12, 2) NULL,
	[var_ins_a] [decimal](12, 2) NULL,
	[tvl_meet_mls_ent_a] [decimal](12, 2) NULL,
	[occ_exp_a] [decimal](12, 2) NULL,
	[supls_a] [decimal](12, 2) NULL,
	[comm_exp_a] [decimal](12, 2) NULL,
	[cont_award_a] [decimal](12, 2) NULL,
	[inv_rel_svc_exp_a] [decimal](12, 2) NULL,
	[recruit_hire_exp_a] [decimal](12, 2) NULL,
	[non_cap_tools_eq_a] [decimal](12, 2) NULL,
	[misc_exp_a] [decimal](12, 2) NULL,
	[fxd_ins_a] [decimal](12, 2) NULL,
	[data_proc_exp_c] [decimal](12, 2) NULL,
	[corp_legal_exp_c] [decimal](12, 2) NULL,
	[taxes_a] [decimal](12, 2) NULL,
	[auto_fix_chrg_exp_c] [decimal](12, 2) NULL,
	[non_cap_fix_eq_a] [decimal](12, 2) NULL,
	[losses_interco_ship_exp_c] [decimal](12, 2) NULL,
	[adv_exp_c] [decimal](12, 2) NULL,
	[cc_ccn_chrg_cst_a] [decimal](12, 2) NULL,
	[iw_ccn_chrg_cst_a] [decimal](12, 2) NULL,
	[pa_ccn_chrg_cst_a] [decimal](12, 2) NULL,
	[tp_ccn_chrg_cst_a] [decimal](12, 2) NULL,
	[parts_distr_cst_a] [decimal](12, 2) NULL,
	[serv_prov_cst_a] [decimal](12, 2) NULL,
	[lease_cst_a] [decimal](12, 2) NULL,
	[cent_pay_exp_a] [decimal](12, 2) NULL,
	[mitt_exp] [decimal](15, 5) NULL,
	[pa_rev_ovhd_pa] [decimal](12, 2) NULL,
	[iw_trans_cred_ovhd_iw] [decimal](12, 2) NULL,
	[pa_svc_reimb_ovhd_pa] [decimal](12, 2) NULL,
	[un_rep_ovhd_iw] [decimal](12, 2) NULL,
	[routing_exp_a] [decimal](12, 2) NULL,
	[reg_terr_ovhd_a] [decimal](12, 2) NULL,
	[nat_supp_exp_a] [decimal](12, 2) NULL,
	[node_adj_c_fix] [decimal](12, 2) NULL,
	[node_adj_c] [decimal](12, 2) NULL,
 CONSTRAINT [pk_profit_v2] PRIMARY KEY CLUSTERED 
(
	[svc_un_fty_id_no] ASC,
	[svc_ord_no] ASC,
	[svc_cal_dt] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Roster]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Roster](
	[SequenceNumber] [varchar](50) NULL,
	[EmployeeName] [varchar](50) NULL,
	[EmployeeID] [varchar](50) NULL,
	[EnterpriseID] [varchar](50) NULL,
	[JOBCODE] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[STG_CellControlDeviceInformation]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[STG_CellControlDeviceInformation](
	[PhoneNumber] [varchar](11) NOT NULL,
	[UserID] [int] NULL,
	[UserName] [varchar](100) NULL,
	[Company] [varchar](30) NULL,
	[DeviceType] [varchar](20) NULL,
	[DeviceVersion] [varchar](30) NULL,
	[DeviceModel] [varchar](20) NULL,
	[LocationPerm] [int] NULL,
	[Provider] [varchar](20) NULL,
	[LastSeenDateTime] [datetime] NULL,
	[LastBlockDateTime] [datetime] NULL,
	[LastSeenUTCDateTime] [datetime] NULL,
	[LastBlockUTCDateTime] [datetime] NULL,
	[TimeZone] [varchar](10) NULL,
	[WifiSetting] [int] NULL,
	[NotificationSetting] [int] NULL,
	[TechFirstName] [varchar](50) NULL,
	[TechLastName] [varchar](50) NULL,
	[TechEmail] [varchar](100) NULL,
	[EntityID] [int] NULL,
	[TechEnterpriseID] [varchar](250) NULL,
	[TechManagerGroupID] [int] NULL,
	[TechManagerEnterpriseID] [varchar](50) NULL,
	[TechManagerCity] [varchar](30) NULL,
	[TechManagerState] [varchar](30) NULL,
	[ExternalCreateDateTime] [datetime] NULL,
	[InsertDateTime] [datetime] NULL,
	[ModifiedDateTime] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[STG_CellControlVehicleInformation]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[STG_CellControlVehicleInformation](
	[VehicleDongleMacAddress] [varchar](12) NOT NULL,
	[VIN] [varchar](20) NULL,
	[CustomID] [varchar](30) NULL,
	[ActivationID] [varchar](20) NULL,
	[Firmware] [varchar](20) NULL,
	[Voltage] [decimal](18, 4) NULL,
	[LastSeenDateTimeUTC] [datetime] NULL,
	[OrientationX] [decimal](18, 4) NULL,
	[OrientationY] [decimal](18, 4) NULL,
	[OrientationZ] [decimal](18, 4) NULL,
	[LastSeenDateTime] [datetime] NULL,
	[LastPhoneNumber] [varchar](20) NULL,
	[Company] [varchar](30) NULL,
	[EntityID] [int] NULL,
	[TechManagerGroupID] [int] NULL,
	[TechManagerEnterpriseID] [varchar](50) NULL,
	[InsertDateTime] [datetime] NULL,
	[ModifiedDateTime] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[STG_CellControlVehicleLocation]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[STG_CellControlVehicleLocation](
	[VehicleDongleMacAddress] [varchar](12) NOT NULL,
	[PhoneNumber] [varchar](15) NULL,
	[LocationTime] [datetime] NOT NULL,
	[ManagerEnterpriseIDKey] [int] NULL,
	[ManagerEnterpriseID] [varchar](11) NULL,
	[ActiveTrip] [bit] NULL,
	[Longitude] [decimal](10, 5) NULL,
	[Latitude] [decimal](10, 5) NULL,
	[Heading] [decimal](10, 5) NULL,
	[Speed] [decimal](10, 5) NULL,
	[InsertDateTime] [datetime] NULL,
	[ModifiedDateTime] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[STG_EmployeeTechHubTimepunches]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[STG_EmployeeTechHubTimepunches](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[RouteDate] [date] NULL,
	[RouteDateTime] [datetime2](7) NULL,
	[TimeZone] [varchar](5) NULL,
	[UserEnterpriseID] [varchar](28) NULL,
	[TechnicianEnterpriseID] [varchar](28) NULL,
	[ServiceUnit] [varchar](7) NULL,
	[EntryType] [varchar](50) NULL,
	[EntryNumber] [varchar](50) NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_STG_EmployeeTechHubTimepunches] PRIMARY KEY CLUSTERED 
(
	[EntryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_AIMEmployee]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_AIMEmployee](
	[District] [varchar](7) NOT NULL,
	[Tech] [varchar](7) NOT NULL,
	[EnterpriseID] [varchar](10) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[FirstName] [varchar](50) NOT NULL,
	[MobilePhoneNumber] [varchar](10) NULL,
	[DeminF1] [varchar](1) NOT NULL,
	[EmployeeStatus] [varchar](1) NOT NULL,
	[PrimaryAddressPartOne] [varchar](50) NULL,
	[PrimaryAddressPartTwo] [varchar](50) NULL,
	[PrimaryCity] [varchar](50) NULL,
	[PrimaryState] [varchar](2) NULL,
	[PrimaryZip] [varchar](9) NULL,
	[PrimaryLatitude] [decimal](12, 9) NULL,
	[PrimaryLongitude] [decimal](12, 9) NULL,
	[ReassortmentAddressPartOne] [varchar](50) NULL,
	[ReassortmentAddressPartTwo] [varchar](50) NULL,
	[ReassortmentCity] [varchar](50) NULL,
	[ReassortmentState] [varchar](2) NULL,
	[ReassortmentZip] [varchar](9) NULL,
	[ReturnAddressPartOne] [varchar](50) NULL,
	[ReturnAddressPartTwo] [varchar](50) NULL,
	[ReturnCity] [varchar](50) NULL,
	[ReturnState] [varchar](2) NULL,
	[ReturnZip] [varchar](9) NULL,
	[AlternateAddressPartOne] [varchar](50) NULL,
	[AlternateAddressPartTwo] [varchar](50) NULL,
	[AlternateCity] [varchar](50) NULL,
	[AlternateState] [varchar](2) NULL,
	[AlternateZip] [varchar](9) NULL,
	[ShippingSchedule] [varchar](7) NULL,
	[PDCNumber] [varchar](7) NULL,
	[ManagerEntepriseID] [varchar](10) NULL,
	[ManagerName] [varchar](100) NULL,
	[EmailAddress] [varchar](250) NULL,
 CONSTRAINT [PK_TBL_AIMEmployee] PRIMARY KEY CLUSTERED 
(
	[EnterpriseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_CellControlDeviceInformation]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_CellControlDeviceInformation](
	[PhoneNumber] [varchar](11) NOT NULL,
	[UserID] [int] NULL,
	[UserName] [varchar](100) NULL,
	[Company] [varchar](30) NULL,
	[DeviceType] [varchar](20) NULL,
	[DeviceVersion] [varchar](30) NULL,
	[DeviceModel] [varchar](20) NULL,
	[LocationPerm] [int] NULL,
	[Provider] [varchar](20) NULL,
	[LastSeenDateTime] [datetime] NULL,
	[LastBlockDateTime] [datetime] NULL,
	[LastSeenUTCDateTime] [datetime] NULL,
	[LastBlockUTCDateTime] [datetime] NULL,
	[TimeZone] [varchar](10) NULL,
	[WifiSetting] [int] NULL,
	[NotificationSetting] [int] NULL,
	[TechFirstName] [varchar](50) NULL,
	[TechLastName] [varchar](50) NULL,
	[TechEmail] [varchar](100) NULL,
	[EntityID] [int] NULL,
	[TechEnterpriseID] [varchar](25) NULL,
	[TechManagerGroupID] [int] NULL,
	[TechManagerEnterpriseID] [varchar](50) NULL,
	[TechManagerCity] [varchar](30) NULL,
	[TechManagerState] [varchar](30) NULL,
	[ExternalCreateDateTime] [datetime] NULL,
	[InsertDateTime] [datetime] NULL,
	[ModifiedDateTime] [datetime] NULL,
 CONSTRAINT [PK_TBL_CellControlDeviceInformation] PRIMARY KEY CLUSTERED 
(
	[PhoneNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_CellControlDeviceInformationd]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_CellControlDeviceInformationd](
	[PhoneNumber] [varchar](11) NOT NULL,
	[UserID] [int] NULL,
	[UserName] [varchar](100) NULL,
	[Company] [varchar](30) NULL,
	[DeviceType] [varchar](20) NULL,
	[DeviceVersion] [varchar](10) NULL,
	[DeviceModel] [varchar](20) NULL,
	[LocationPerm] [int] NULL,
	[Provider] [varchar](20) NULL,
	[LastSeenDateTime] [datetime] NULL,
	[LastBlockDateTime] [datetime] NULL,
	[LastSeenUTCDateTime] [datetime] NULL,
	[LastBlockUTCDateTime] [datetime] NULL,
	[TimeZone] [varchar](10) NULL,
	[WifiSetting] [int] NULL,
	[NotificationSetting] [int] NULL,
	[TechFirstName] [varchar](50) NULL,
	[TechLastName] [varchar](50) NULL,
	[TechEmail] [varchar](100) NULL,
	[EntityID] [int] NULL,
	[TechEnterpriseID] [varchar](11) NULL,
	[TechManagerGroupID] [int] NULL,
	[TechManagerEnterpriseID] [varchar](50) NULL,
	[TechManagerCity] [varchar](30) NULL,
	[TechManagerState] [varchar](30) NULL,
	[ExternalCreateDateTime] [datetime] NULL,
	[InsertDateTime] [datetime] NULL,
	[ModifiedDateTime] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_CellControlVehicleInformation]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_CellControlVehicleInformation](
	[VehicleDongleMacAddress] [varchar](12) NOT NULL,
	[VIN] [varchar](20) NULL,
	[CustomID] [varchar](30) NULL,
	[ActivationID] [varchar](20) NULL,
	[Firmware] [varchar](20) NULL,
	[Voltage] [decimal](18, 4) NULL,
	[LastSeenDateTimeUTC] [datetime] NULL,
	[OrientationX] [decimal](18, 4) NULL,
	[OrientationY] [decimal](18, 4) NULL,
	[OrientationZ] [decimal](18, 4) NULL,
	[LastSeenDateTime] [datetime] NULL,
	[LastPhoneNumber] [varchar](20) NULL,
	[Company] [varchar](30) NULL,
	[EntityID] [int] NULL,
	[TechManagerGroupID] [int] NULL,
	[TechManagerEnterpriseID] [varchar](50) NULL,
	[InsertDateTime] [datetime] NULL,
	[ModifiedDateTime] [datetime] NULL,
 CONSTRAINT [PK_TBL_CellControlVehicleInformation] PRIMARY KEY CLUSTERED 
(
	[VehicleDongleMacAddress] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_CellControlVehicleLocation]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_CellControlVehicleLocation](
	[VehicleDongleMacAddress] [varchar](12) NOT NULL,
	[PhoneNumber] [varchar](15) NULL,
	[LocationTime] [datetime] NOT NULL,
	[ManagerEnterpriseIDKey] [int] NULL,
	[ManagerEnterpriseID] [varchar](11) NULL,
	[ActiveTrip] [bit] NULL,
	[Longitude] [decimal](10, 5) NULL,
	[Latitude] [decimal](10, 5) NULL,
	[Heading] [decimal](10, 5) NULL,
	[Speed] [decimal](10, 5) NULL,
	[InsertDateTime] [datetime] NULL,
	[ModifiedDateTime] [datetime] NULL,
 CONSTRAINT [PK_TBL_CellControlVehicleLocation] PRIMARY KEY CLUSTERED 
(
	[VehicleDongleMacAddress] ASC,
	[LocationTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ClickRoutingSchedule]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ClickRoutingSchedule](
	[TaskKey] [bigint] NULL,
	[AssignmentEventKey] [int] NULL,
	[ExternalReferenceID] [varchar](64) NULL,
	[CallID] [varchar](64) NOT NULL,
	[CallNumber] [int] NOT NULL,
	[TaskStatusCode] [int] NULL,
	[TaskStatus] [varchar](50) NULL,
	[AttemptType] [varchar](50) NULL,
	[TaskEmployeeID] [varchar](20) NULL,
	[AssignEmployeeID] [varchar](20) NULL,
	[TaskType] [varchar](25) NULL,
	[ServiceUnit] [char](7) NULL,
	[ServiceOrder] [char](8) NULL,
	[VisitNumber] [int] NULL,
	[ScheduleDate] [date] NULL,
	[CustomerRequestedStartDate] [datetimeoffset](0) NULL,
	[CustomerRequestedEndDate] [datetimeoffset](0) NULL,
	[ScheduledStartDate] [datetimeoffset](0) NULL,
	[ScheduledEndDate] [datetimeoffset](0) NULL,
	[ActualStartDate] [datetimeoffset](0) NULL,
	[ActualEndDate] [datetimeoffset](0) NULL,
	[CustomerName] [varchar](64) NULL,
	[CustomerEmail] [varchar](254) NULL,
	[CustomerPhone] [char](10) NULL,
	[EventAddress] [varchar](100) NULL,
	[EventCity] [varchar](30) NULL,
	[EventState] [char](2) NULL,
	[EventZipode] [varchar](9) NULL,
	[EventLocation] [geography] NULL,
	[EventLatitude]  AS ([EventLocation].[Lat]),
	[EventLongitude]  AS ([EventLocation].[Long]),
	[RestrictedLocation] [int] NULL,
	[Market] [int] NULL,
	[SubMarket] [int] NULL,
	[AssignmentTypeCode] [int] NOT NULL,
	[EventDescription] [varchar](max) NULL,
	[PureDuration] [int] NULL,
	[AdditionalTravelDuration] [int] NULL,
	[AdditionalUpsellDuration] [int] NULL,
	[PlannedDuration] [int] NULL,
	[ActualDuration] [int] NULL,
	[PlannedTravelDuration] [int] NULL,
	[ActualTravelDuration] [int] NULL,
	[Calendar] [int] NULL,
	[UnionCode] [int] NULL,
	[ApprovedByEmployeeID] [varchar](20) NULL,
	[ApprovedDate] [datetimeoffset](0) NULL,
	[TaskCreateDate] [datetimeoffset](0) NULL,
	[TaskExternalLastModifiedDate] [datetimeoffset](0) NULL,
	[TaskInternalLastModifiedDate] [datetimeoffset](0) NULL,
	[AssignmentCreateDate] [datetimeoffset](0) NULL,
	[AssignmentExternalLastModifiedDate] [datetimeoffset](0) NULL,
	[AssignmentInternalLastModifiedDate] [datetimeoffset](0) NULL,
	[AssignmentStatus] [int] NULL,
 CONSTRAINT [PK__TBL_ClickRoutingSchedule] PRIMARY KEY CLUSTERED 
(
	[CallID] ASC,
	[CallNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_Employee]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Employee](
	[UniqueID] [uniqueidentifier] NOT NULL,
	[BusinessUnit] [varchar](4) NOT NULL,
	[FinancialRegion] [varchar](5) NOT NULL,
	[FinancialUnit] [varchar](5) NOT NULL,
	[Region] [varchar](7) NOT NULL,
	[Unit] [varchar](30) NOT NULL,
	[Department] [varchar](50) NULL,
	[SearsDepartment] [varchar](50) NULL,
	[SearsDepartmentName] [varchar](30) NOT NULL,
	[GLDepartment] [varchar](50) NOT NULL,
	[Location] [varchar](5) NOT NULL,
	[LocationDescription] [varchar](30) NOT NULL,
	[PayType] [varchar](3) NOT NULL,
	[PayGroup] [varchar](7) NULL,
	[JobGrade] [varchar](5) NOT NULL,
	[EnterpriseID] [varchar](28) NOT NULL,
	[EmployeeID] [varchar](14) NOT NULL,
	[NPSID] [varchar](7) NOT NULL,
	[SalesID] [varchar](7) NOT NULL,
	[EmployeeStatus] [varchar](1) NOT NULL,
	[EmployeeType] [varchar](1) NOT NULL,
	[LastName] [varchar](100) NOT NULL,
	[FirstName] [varchar](100) NOT NULL,
	[MiddleName] [varchar](50) NOT NULL,
	[FullName] [varchar](250) NOT NULL,
	[DisplayName] [varchar](250) NOT NULL,
	[Email] [varchar](250) NOT NULL,
	[OfficePhone] [varchar](70) NOT NULL,
	[MobilePhone] [varchar](70) NOT NULL,
	[MobilePhoneProvider] [varchar](10) NOT NULL,
	[JobCode] [varchar](10) NOT NULL,
	[JobTitle] [varchar](71) NOT NULL,
	[StartDate] [varchar](10) NOT NULL,
	[JobStartDate] [varchar](10) NOT NULL,
	[TermDate] [varchar](10) NOT NULL,
	[ManagerEnterpriseID] [varchar](28) NOT NULL,
	[ManagerEmployeeID] [varchar](14) NOT NULL,
	[ManagerStartDate] [date] NOT NULL,
	[RowStartDate] [date] NOT NULL,
	[RowEndDate] [date] NULL,
 CONSTRAINT [PK__TBL_Empl__1881D4EF65370702] PRIMARY KEY CLUSTERED 
(
	[EmployeeID] ASC,
	[RowStartDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_Employee_New]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Employee_New](
	[UniqueID] [uniqueidentifier] NOT NULL,
	[BusinessUnit] [varchar](4) NOT NULL,
	[FinancialRegion] [varchar](5) NOT NULL,
	[FinancialUnit] [varchar](5) NOT NULL,
	[Region] [varchar](7) NOT NULL,
	[Unit] [varchar](30) NOT NULL,
	[Department] [varchar](50) NULL,
	[SearsDepartment] [varchar](50) NULL,
	[SearsDepartmentName] [varchar](30) NOT NULL,
	[GLDepartment] [varchar](50) NOT NULL,
	[Location] [varchar](5) NOT NULL,
	[LocationDescription] [varchar](30) NOT NULL,
	[PayType] [varchar](3) NOT NULL,
	[PayGroup] [varchar](7) NULL,
	[JobGrade] [varchar](5) NOT NULL,
	[EnterpriseID] [varchar](28) NOT NULL,
	[EmployeeID] [varchar](14) NOT NULL,
	[NPSID] [varchar](7) NOT NULL,
	[SalesID] [varchar](7) NOT NULL,
	[EmployeeStatus] [varchar](1) NOT NULL,
	[EmployeeType] [varchar](1) NOT NULL,
	[LastName] [varchar](100) NOT NULL,
	[FirstName] [varchar](100) NOT NULL,
	[MiddleName] [varchar](50) NOT NULL,
	[FullName] [varchar](250) NOT NULL,
	[DisplayName] [varchar](250) NOT NULL,
	[Email] [varchar](250) NOT NULL,
	[OfficePhone] [varchar](70) NOT NULL,
	[MobilePhone] [varchar](70) NOT NULL,
	[MobilePhoneProvider] [varchar](10) NOT NULL,
	[JobCode] [varchar](10) NOT NULL,
	[JobTitle] [varchar](71) NOT NULL,
	[StartDate] [varchar](10) NOT NULL,
	[JobStartDate] [varchar](10) NOT NULL,
	[TermDate] [varchar](10) NOT NULL,
	[ManagerEnterpriseID] [varchar](28) NOT NULL,
	[ManagerEmployeeID] [varchar](14) NOT NULL,
	[ManagerStartDate] [date] NOT NULL,
	[RowStartDate] [date] NOT NULL,
	[RowEndDate] [date] NULL,
 CONSTRAINT [PK__TBL_Empl__308F3E580D44F85C] PRIMARY KEY CLUSTERED 
(
	[UniqueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EmployeeHierarchy]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EmployeeHierarchy](
	[EnterpriseID] [varchar](28) NOT NULL,
	[FullName] [varchar](250) NULL,
	[ManagerEnterpriseID] [varchar](28) NULL,
	[ManagerFullName] [varchar](250) NULL,
	[ManagerJobCode] [varchar](10) NULL,
	[ManagerJobTitle] [varchar](70) NULL,
	[ManagerTwoEnterpriseID] [varchar](28) NULL,
	[ManagerTwoFullName] [varchar](250) NULL,
	[ManagerTwoJobCode] [varchar](10) NULL,
	[ManagerTwoJobTitle] [varchar](70) NULL,
	[ManagerThreeEnterpriseID] [varchar](28) NULL,
	[ManagerThreeFullName] [varchar](250) NULL,
	[ManagerThreeJobCode] [varchar](10) NULL,
	[ManagerThreeJobTitle] [varchar](70) NULL,
	[ManagerFourEnterpriseID] [varchar](28) NULL,
	[ManagerFourFullName] [varchar](250) NULL,
	[ManagerFourJobCode] [varchar](10) NULL,
	[ManagerFourJobTitle] [varchar](70) NULL,
	[ManagerFiveEnterpriseID] [varchar](28) NULL,
	[ManagerFiveFullName] [varchar](250) NULL,
	[ManagerFiveJobCode] [varchar](10) NULL,
	[ManagerFiveJobTitle] [varchar](70) NULL,
 CONSTRAINT [PK_TBL_EmployeeHierarchy] PRIMARY KEY CLUSTERED 
(
	[EnterpriseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EmployeeHierarchyIHHVACTechRegion]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EmployeeHierarchyIHHVACTechRegion](
	[EnterpriseID] [varchar](10) NOT NULL,
	[NPSID] [varchar](7) NULL,
	[FullName] [varchar](250) NULL,
	[TMDTMEnterpriseID] [varchar](10) NULL,
	[TMDTMFullName] [varchar](250) NULL,
	[DSMRTMEnterpriseID] [varchar](10) NULL,
	[DSMRTMFullName] [varchar](250) NULL,
	[Unit] [char](7) NULL,
	[UnitName] [varchar](250) NULL,
	[TFDEnterpriseID] [varchar](10) NULL,
	[TFDFullName] [varchar](250) NULL,
	[Territory] [char](7) NULL,
	[TerritoryName] [varchar](250) NULL,
	[Region] [char](7) NULL,
	[RegionName] [varchar](250) NULL,
	[RVPDirEnterpriseID] [varchar](10) NULL,
	[RVPDirFullName] [varchar](250) NULL,
 CONSTRAINT [PK_TBL_EmployeeHierarchyIHHVACTechRegion] PRIMARY KEY CLUSTERED 
(
	[EnterpriseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EmployeeHierarchyIHHVACUnitRegion]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EmployeeHierarchyIHHVACUnitRegion](
	[Unit] [char](7) NOT NULL,
	[UnitName] [varchar](250) NULL,
	[TFDEnterpriseID] [varchar](10) NULL,
	[TFDFullName] [varchar](250) NULL,
	[Territory] [char](7) NULL,
	[TerritoryName] [varchar](250) NULL,
	[Region] [char](7) NULL,
	[RegionName] [varchar](250) NULL,
	[RVPDirEnterpriseID] [varchar](10) NULL,
	[RVPDirFullName] [varchar](250) NULL,
 CONSTRAINT [PK_TBL_EmployeeHierarchyIHHVACUnitRegion] PRIMARY KEY CLUSTERED 
(
	[Unit] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EmployeeHierarchyIHTechRegion]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EmployeeHierarchyIHTechRegion](
	[EnterpriseID] [varchar](10) NOT NULL,
	[NPSID] [varchar](7) NULL,
	[FullName] [varchar](250) NULL,
	[TMEnterpriseID] [varchar](10) NULL,
	[TMFullName] [varchar](250) NULL,
	[DSMEnterpriseID] [varchar](10) NULL,
	[DSMFullName] [varchar](250) NULL,
	[Unit] [char](7) NULL,
	[UnitName] [varchar](250) NULL,
	[TFDEnterpriseID] [varchar](10) NULL,
	[TFDFullName] [varchar](250) NULL,
	[Territory] [char](7) NULL,
	[TerritoryName] [varchar](250) NULL,
	[Region] [char](7) NULL,
	[RegionName] [varchar](250) NULL,
	[RVPEnterpriseID] [varchar](10) NULL,
	[RVPFullName] [varchar](250) NULL,
 CONSTRAINT [PK_TBL_EmployeeHierarchyIHTechRegion] PRIMARY KEY CLUSTERED 
(
	[EnterpriseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EmployeeHierarchyIHUnitRegion]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EmployeeHierarchyIHUnitRegion](
	[Unit] [char](7) NOT NULL,
	[UnitName] [varchar](250) NULL,
	[TFDEnterpriseID] [varchar](10) NULL,
	[TFDFullName] [varchar](250) NULL,
	[Territory] [char](7) NULL,
	[TerritoryName] [varchar](250) NULL,
	[Region] [char](7) NULL,
	[RegionName] [varchar](250) NULL,
	[RVPEnterpriseID] [varchar](10) NULL,
	[RVPFullName] [varchar](250) NULL,
 CONSTRAINT [PK_TBL_EmployeeHierarchyIHUnitRegion] PRIMARY KEY CLUSTERED 
(
	[Unit] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EmployeeHierarchyInverse]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EmployeeHierarchyInverse](
	[ManagerEnterpriseID] [varchar](28) NOT NULL,
	[ManagerFullName] [varchar](250) NULL,
	[EnterpriseID] [varchar](28) NOT NULL,
	[FullName] [varchar](250) NULL,
	[LevelDown] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EmployeeHistoricalAssignmentDate]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_EmployeeHistoricalAssignmentDate](
	[UniqueID] [int] NOT NULL,
	[StartDate] [datetime2](0) NOT NULL,
	[AssignmentDate] [date] NOT NULL,
	[EndDate] [datetime2](0) NULL,
 CONSTRAINT [PK_TBL_EmployeeHistoricalAssignmentDate] PRIMARY KEY CLUSTERED 
(
	[UniqueID] ASC,
	[StartDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_EmployeeHistoricalBusinessUnit]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EmployeeHistoricalBusinessUnit](
	[UniqueID] [int] NOT NULL,
	[StartDate] [datetime2](0) NOT NULL,
	[BusinessUnitNumber] [char](7) NOT NULL,
	[EndDate] [datetime2](0) NULL,
 CONSTRAINT [PK_TBL_EmployeeHistoricalBusinessUnit] PRIMARY KEY CLUSTERED 
(
	[UniqueID] ASC,
	[StartDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EmployeeHistoricalCompanyCode]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EmployeeHistoricalCompanyCode](
	[UniqueID] [int] NOT NULL,
	[StartDate] [datetime2](0) NOT NULL,
	[CompanyCode] [varchar](3) NOT NULL,
	[EndDate] [datetime2](0) NULL,
 CONSTRAINT [PK_TBL_EmployeeHistoricalCompanyCode] PRIMARY KEY CLUSTERED 
(
	[UniqueID] ASC,
	[StartDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EmployeeHistoricalDepartmentCode]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EmployeeHistoricalDepartmentCode](
	[UniqueID] [int] NOT NULL,
	[StartDate] [datetime2](0) NOT NULL,
	[DepartmentCode] [varchar](10) NOT NULL,
	[EndDate] [datetime2](0) NULL,
 CONSTRAINT [PK_TBL_EmployeeHistoricalDepartmentCode] PRIMARY KEY CLUSTERED 
(
	[UniqueID] ASC,
	[StartDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EmployeeHistoricalDepartmentID]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EmployeeHistoricalDepartmentID](
	[UniqueID] [int] NOT NULL,
	[StartDate] [datetime2](0) NOT NULL,
	[DepartmentID] [varchar](10) NOT NULL,
	[EndDate] [datetime2](0) NULL,
 CONSTRAINT [PK_TBL_EmployeeHistoricalDepartmentID] PRIMARY KEY CLUSTERED 
(
	[UniqueID] ASC,
	[StartDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EmployeeHistoricalEmployeeType]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EmployeeHistoricalEmployeeType](
	[UniqueID] [int] NOT NULL,
	[StartDate] [datetime2](0) NOT NULL,
	[EmployeeType] [char](1) NOT NULL,
	[EndDate] [datetime2](0) NULL,
 CONSTRAINT [PK_TBL_EmployeeHistoricalEmployeeType] PRIMARY KEY CLUSTERED 
(
	[UniqueID] ASC,
	[StartDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EmployeeHistoricalFinancialUnit]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EmployeeHistoricalFinancialUnit](
	[UniqueID] [int] NOT NULL,
	[StartDate] [datetime2](0) NOT NULL,
	[FinancialUnitNumber] [char](7) NOT NULL,
	[EndDate] [datetime2](0) NULL,
 CONSTRAINT [PK_TBL_EmployeeHistoricalFinancialUnit] PRIMARY KEY CLUSTERED 
(
	[UniqueID] ASC,
	[StartDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EmployeeHistoricalJobCode]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EmployeeHistoricalJobCode](
	[UniqueID] [int] NOT NULL,
	[StartDate] [datetime2](0) NOT NULL,
	[JobCode] [varchar](6) NOT NULL,
	[EndDate] [datetime2](0) NULL,
 CONSTRAINT [PK_TBL_EmployeeHistoricalJobCode] PRIMARY KEY CLUSTERED 
(
	[UniqueID] ASC,
	[StartDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EmployeeHistoricalJobGrade]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EmployeeHistoricalJobGrade](
	[UniqueID] [int] NOT NULL,
	[StartDate] [datetime2](0) NOT NULL,
	[JobGrade] [varchar](7) NOT NULL,
	[EndDate] [datetime2](0) NULL,
 CONSTRAINT [PK_TBL_EmployeeHistoricalJobGrade] PRIMARY KEY CLUSTERED 
(
	[UniqueID] ASC,
	[StartDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EmployeeHistoricalManager]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EmployeeHistoricalManager](
	[UniqueID] [int] NOT NULL,
	[StartDate] [datetime2](0) NOT NULL,
	[ManagerEmployeeID] [char](11) NOT NULL,
	[EndDate] [datetime2](0) NULL,
 CONSTRAINT [PK_TBL_EmployeeHistoricalManager] PRIMARY KEY CLUSTERED 
(
	[UniqueID] ASC,
	[StartDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EmployeeHistoricalPayGroup]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EmployeeHistoricalPayGroup](
	[UniqueID] [int] NOT NULL,
	[StartDate] [datetime2](0) NOT NULL,
	[PayGroup] [varchar](5) NOT NULL,
	[EndDate] [datetime2](0) NULL,
 CONSTRAINT [PK_TBL_EmployeeHistoricalPayGroup] PRIMARY KEY CLUSTERED 
(
	[UniqueID] ASC,
	[StartDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EmployeeHistoricalPayRate]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_EmployeeHistoricalPayRate](
	[UniqueID] [int] NOT NULL,
	[StartDate] [datetime2](0) NOT NULL,
	[PayRate] [money] NOT NULL,
	[EndDate] [datetime2](0) NULL,
 CONSTRAINT [PK_TBL_EmployeeHistoricalPayRate] PRIMARY KEY CLUSTERED 
(
	[UniqueID] ASC,
	[StartDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_EmployeeHistoricalPayType]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EmployeeHistoricalPayType](
	[UniqueID] [int] NOT NULL,
	[StartDate] [datetime2](0) NOT NULL,
	[PayType] [varchar](3) NOT NULL,
	[EndDate] [datetime2](0) NULL,
 CONSTRAINT [PK_TBL_EmployeeHistoricalPayType] PRIMARY KEY CLUSTERED 
(
	[UniqueID] ASC,
	[StartDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EmployeeHistoricalSalaryPlan]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EmployeeHistoricalSalaryPlan](
	[UniqueID] [int] NOT NULL,
	[StartDate] [datetime2](0) NOT NULL,
	[SalaryPlan] [varchar](7) NOT NULL,
	[EndDate] [datetime2](0) NULL,
 CONSTRAINT [PK_TBL_EmployeeHistoricalSalaryPlan] PRIMARY KEY CLUSTERED 
(
	[UniqueID] ASC,
	[StartDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EmployeeHistoricalStatusCode]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EmployeeHistoricalStatusCode](
	[UniqueID] [int] NOT NULL,
	[StartDate] [datetime2](0) NOT NULL,
	[StatusCode] [char](1) NOT NULL,
	[EndDate] [datetime2](0) NULL,
 CONSTRAINT [PK_TBL_EmployeeHistoricalStatusCode] PRIMARY KEY CLUSTERED 
(
	[UniqueID] ASC,
	[StartDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EmployeeID]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EmployeeID](
	[UniqueID] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeID] [char](11) NOT NULL,
	[ManagerEmployeeID] [char](11) NULL,
	[PayRate] [money] NULL,
	[ExternalUniqueID]  AS (CONVERT([varchar](32),hashbytes('MD5',ltrim(rtrim(upper([EmployeeID])))),(2))),
 CONSTRAINT [PK_TBL_EmployeeID] PRIMARY KEY CLUSTERED 
(
	[UniqueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EmployeeName]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EmployeeName](
	[UniqueID] [int] NOT NULL,
	[CompanyCode] [varchar](3) NULL,
	[FinancialRegion] [char](7) NULL,
	[FinancialUnitNumber] [char](7) NULL,
	[BusinessUnitNumber] [char](7) NULL,
	[PayGroup] [varchar](5) NULL,
	[NID] [varchar](9) NULL,
	[StatusCode] [char](1) NULL,
	[EnterpriseID] [varchar](10) NULL,
	[JobCode] [varchar](6) NULL,
	[JobTitle] [varchar](70) NULL,
	[AssignmentDate] [date] NULL,
	[HireDate] [date] NULL,
	[TermDate] [date] NULL,
	[EmployeeFullName] [varchar](250) NULL,
	[EmployeeFirstName] [varchar](100) NULL,
	[EmployeeLastName] [varchar](100) NULL,
	[EmployeeDisplayName] [varchar](250) NULL,
	[ManagerEnterpriseID] [varchar](10) NULL,
	[AddressLine1] [varchar](250) NULL,
	[AddressLine2] [varchar](250) NULL,
	[City] [varchar](100) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](9) NULL,
	[Gender] [char](1) NULL,
	[DepartmentID] [varchar](10) NULL,
	[EmployeeType] [char](1) NULL,
	[FullTime] [int] NULL,
	[SalaryPlan] [varchar](7) NULL,
	[JobGrade] [varchar](7) NULL,
	[PayType] [varchar](3) NULL,
	[DepartmentCode] [varchar](10) NULL,
	[WorkPhone] [char](10) NULL,
	[HomePhone] [char](10) NULL,
	[MobilePhone] [char](10) NULL,
	[EmailAddress] [varchar](254) NULL,
	[LastUpdate] [datetime2](0) NULL,
 CONSTRAINT [PK_TBL_EmployeeName] PRIMARY KEY CLUSTERED 
(
	[UniqueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EmployeeTechHub]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EmployeeTechHub](
	[EmployeeID] [varchar](14) NOT NULL,
	[EnterpriseID] [varchar](28) NULL,
	[Region] [varchar](7) NOT NULL,
	[FinancialUnit] [varchar](5) NOT NULL,
	[Unit] [varchar](30) NOT NULL,
	[NPSID] [varchar](7) NOT NULL,
	[FullName] [varchar](250) NOT NULL,
	[JobCode] [varchar](10) NOT NULL,
	[TechHubStartDate] [date] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EmployeeTechHubDates]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EmployeeTechHubDates](
	[EnterpriseID] [varchar](28) NOT NULL,
	[TechHubDate] [date] NOT NULL,
 CONSTRAINT [PK_TBL_EmployeeTechHubDates] PRIMARY KEY NONCLUSTERED 
(
	[EnterpriseID] ASC,
	[TechHubDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EmployeeTechHubTimepunches]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EmployeeTechHubTimepunches](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[RouteDate] [date] NULL,
	[RouteDateTime] [datetime2](7) NULL,
	[TimeZone] [varchar](5) NULL,
	[UserEnterpriseID] [varchar](28) NULL,
	[TechnicianEnterpriseID] [varchar](28) NULL,
	[ServiceUnit] [varchar](7) NULL,
	[EntryType] [varchar](50) NULL,
	[EntryNumber] [varchar](50) NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_TBL_EmployeeTechHubTimepunches] PRIMARY KEY CLUSTERED 
(
	[EntryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EmployeeTechHubTimepunchReasons]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EmployeeTechHubTimepunchReasons](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[EntryType] [varchar](50) NULL,
	[GroupType] [varchar](50) NULL,
	[IncludeEntry] [int] NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
	[EventCategory] [varchar](50) NULL,
 CONSTRAINT [PK_TBL_EmployeeTechHubTimepunchReasons] PRIMARY KEY CLUSTERED 
(
	[EntryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_Enumerations]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Enumerations](
	[LineID] [int] IDENTITY(1,1) NOT NULL,
	[ParentLineID] [int] NULL,
	[Business] [varchar](20) NOT NULL,
	[OwnerType] [varchar](20) NOT NULL,
	[OwnerID] [int] NULL,
	[ShortDescription] [varchar](50) NULL,
	[LongDescription] [varchar](500) NULL,
	[NumericValue] [int] NULL,
	[SortOrder] [int] NOT NULL,
	[IsEnabled] [bit] NOT NULL,
	[IsUserAllowed] [bit] NOT NULL,
	[AncillaryShort] [varchar](50) NULL,
	[AncillaryNumeric] [int] NULL,
	[AncillaryLong] [varchar](max) NULL,
	[BusinessNumericValue] [int] NULL,
	[LastModified] [datetime2](0) NOT NULL,
	[LastModifiedBy] [varchar](10) NULL,
 CONSTRAINT [PK_TBL_Enumerations] PRIMARY KEY NONCLUSTERED 
(
	[LineID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_LDAPEmployee]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_LDAPEmployee](
	[BusinessUnit] [varchar](4) NULL,
	[FinancialUnit] [varchar](5) NULL,
	[Department] [varchar](50) NULL,
	[SearsDepartment] [varchar](50) NULL,
	[SearsDepartmentName] [varchar](30) NULL,
	[Location] [varchar](5) NULL,
	[LocationDescription] [varchar](30) NULL,
	[PayType] [varchar](3) NULL,
	[PayGroup] [varchar](7) NULL,
	[JobGrade] [varchar](5) NULL,
	[EnterpriseID] [varchar](28) NOT NULL,
	[EmployeeID] [varchar](14) NULL,
	[NPSID] [varchar](7) NULL,
	[SalesID] [varchar](7) NULL,
	[EmployeeStatus] [varchar](1) NULL,
	[EmployeeType] [varchar](1) NULL,
	[LastName] [varchar](100) NULL,
	[FirstName] [varchar](100) NULL,
	[MiddleName] [varchar](50) NULL,
	[FullName] [varchar](250) NULL,
	[DisplayName] [varchar](250) NULL,
	[Email] [varchar](250) NULL,
	[OfficePhone] [varchar](70) NULL,
	[MobilePhone] [varchar](70) NULL,
	[JobCode] [varchar](10) NULL,
	[JobTitle] [varchar](71) NULL,
	[StartDate] [varchar](10) NULL,
	[JobStartDate] [varchar](10) NULL,
	[TermDate] [varchar](10) NULL,
	[ManagerEnterpriseID] [varchar](28) NULL,
 CONSTRAINT [PK_TBL_LDAPEmployee] PRIMARY KEY CLUSTERED 
(
	[EnterpriseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_LDAPLocations]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_LDAPLocations](
	[Address] [varchar](max) NULL,
	[City] [varchar](max) NULL,
	[CloseDate] [varchar](max) NULL,
	[ClosePlanDate] [varchar](max) NULL,
	[County] [varchar](max) NULL,
	[DC] [varchar](max) NULL,
	[District] [varchar](max) NULL,
	[DivCode] [varchar](max) NULL,
	[Division] [varchar](max) NULL,
	[EffDate] [varchar](max) NULL,
	[Manager] [varchar](max) NULL,
	[Number] [varchar](max) NULL,
	[OpenDate] [varchar](max) NULL,
	[OrigFacNumber] [varchar](max) NULL,
	[OrigKMNumber] [varchar](max) NULL,
	[OrigNumber] [varchar](max) NULL,
	[OverheadDesc] [varchar](max) NULL,
	[PhoneNumber] [varchar](max) NULL,
	[Region] [varchar](max) NULL,
	[RegionCode] [varchar](max) NULL,
	[ReOpenDate] [varchar](max) NULL,
	[ReportsTo] [varchar](max) NULL,
	[SCName] [varchar](max) NULL,
	[State] [varchar](max) NULL,
	[SubType] [varchar](max) NULL,
	[TempCloseDate] [varchar](max) NULL,
	[TimeZone] [varchar](max) NULL,
	[Type] [varchar](max) NULL,
	[TypeCode] [varchar](max) NULL,
	[Zip] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_LDAPLocationsTwo]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_LDAPLocationsTwo](
	[Number] [varchar](max) NULL,
	[AddressOne] [varchar](max) NULL,
	[AddressTwo] [varchar](max) NULL,
	[City] [varchar](max) NULL,
	[Contact] [varchar](max) NULL,
	[CRC] [varchar](max) NULL,
	[DDC] [varchar](max) NULL,
	[District] [varchar](max) NULL,
	[FaxNumber] [varchar](max) NULL,
	[FC] [varchar](max) NULL,
	[MDO] [varchar](max) NULL,
	[Name] [varchar](max) NULL,
	[PartsPhoneNumber] [varchar](max) NULL,
	[PhoneNumber] [varchar](max) NULL,
	[Region] [varchar](max) NULL,
	[ReportsTo] [varchar](max) NULL,
	[RimCycle] [varchar](max) NULL,
	[RRC] [varchar](max) NULL,
	[State] [varchar](max) NULL,
	[SubType] [varchar](max) NULL,
	[SVC] [varchar](max) NULL,
	[SVCPhoneNumber] [varchar](max) NULL,
	[Type] [varchar](max) NULL,
	[Zip] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_NPSBaseUnit]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_NPSBaseUnit](
	[Unit] [char](7) NOT NULL,
	[UnitTypeCode] [char](3) NOT NULL,
	[UnitName] [varchar](27) NOT NULL,
	[UnitStreetAddressLine1] [varchar](30) NOT NULL,
	[UnitStreetAddressLine2] [varchar](30) NOT NULL,
	[UnitCityName] [varchar](20) NOT NULL,
	[UnitStateCode] [char](2) NOT NULL,
	[ZIPCode] [char](5) NOT NULL,
	[ZIPPlus4Code] [char](4) NOT NULL,
	[PartDepot] [char](4) NOT NULL,
	[Field] [char](7) NOT NULL,
	[Region] [char](7) NULL,
	[CentralServiceUnit] [char](7) NOT NULL,
	[AlternateRepairUnit] [char](7) NOT NULL,
	[PATelemarketingPriceLevelCode] [decimal](2, 1) NOT NULL,
	[MarketingUnitInboundTelephone] [char](10) NOT NULL,
	[TaxingGeoCode] [char](9) NOT NULL,
	[BatchUpdateDate] [date] NOT NULL,
	[TaxingCountyCode] [char](3) NOT NULL,
 CONSTRAINT [PK_TBL_NPSBaseUnit] PRIMARY KEY CLUSTERED 
(
	[Unit] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_NPSBaseUnitGeocode]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_NPSBaseUnitGeocode](
	[Unit] [char](7) NOT NULL,
	[Latitude] [decimal](12, 9) NULL,
	[Longitude] [decimal](12, 9) NULL,
 CONSTRAINT [PK_TBL_NPSBaseUnitGeocode] PRIMARY KEY CLUSTERED 
(
	[Unit] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_NPSEmployee]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_NPSEmployee](
	[Unit] [varchar](7) NOT NULL,
	[NPSID] [varchar](7) NOT NULL,
	[SocialSecurityID] [varchar](10) NULL,
	[EnterpriseID] [varchar](28) NULL,
	[LastName] [varchar](100) NULL,
	[FirstName] [varchar](100) NULL,
	[MiddleName] [varchar](50) NULL,
	[ClassificationWorkCode] [varchar](1) NULL,
	[ServiceDate] [varchar](30) NULL,
	[TermDate] [varchar](50) NULL,
	[WorkShiftCode] [varchar](2) NULL,
	[AddDeleteCode] [varchar](2) NULL,
	[ManagerEnterpriseID] [varchar](28) NULL,
	[ManagerEmployeeID] [varchar](14) NULL,
	[JobCode] [varchar](10) NULL,
	[HomePhone] [varchar](10) NULL,
	[PagerPhone] [varchar](10) NULL,
	[SecurityCode] [varchar](10) NULL,
	[BranchUnit] [varchar](7) NULL,
 CONSTRAINT [PK_TBL_NPSEmployee] PRIMARY KEY CLUSTERED 
(
	[Unit] ASC,
	[NPSID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_NPSSITechRouteSchedule]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_NPSSITechRouteSchedule](
	[JobNumber] [bigint] NOT NULL,
	[JobReference] [varchar](50) NOT NULL,
	[AttemptNumber] [int] NULL,
	[AttemptType] [varchar](3) NULL,
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[ScheduleFromTime] [datetime] NULL,
	[ScheduleToTime] [datetime] NULL,
	[AppointmentEarliestDate] [datetime] NULL,
	[AppointmentLatestDate] [datetime] NULL,
	[ExpectedStartDate] [datetime] NULL,
	[CategoryDescription] [varchar](50) NULL,
	[EnterpriseID] [varchar](28) NULL,
	[TechID] [varchar](7) NULL,
	[EmployeeName] [varchar](50) NULL,
	[ActualStartDate] [datetime] NULL,
	[FRUID] [char](2) NULL,
	[FRULocation] [varchar](50) NULL,
	[FRUDescription] [varchar](50) NULL,
	[IRUDescription] [varchar](50) NULL,
	[TeamNumber] [int] NULL,
	[TeamName] [varchar](50) NULL,
	[CustomerName] [varchar](50) NULL,
	[Address1] [varchar](50) NULL,
	[Address2] [varchar](50) NULL,
	[Address3] [varchar](50) NULL,
	[Address4] [varchar](50) NULL,
	[PhoneNumber] [varchar](50) NULL,
	[PostCode] [varchar](9) NULL,
	[StatusCode] [varchar](50) NULL,
	[JobType] [varchar](50) NULL,
	[FRUTimeZone] [varchar](3) NULL,
	[TechTimeZone] [varchar](3) NULL,
	[CustomerTimeZone] [varchar](3) NULL,
	[EstimatedTravelTime] [int] NULL,
	[ActualTravelTime] [int] NULL,
	[OriginalEnterpriseID] [varchar](28) NULL,
	[EstimatedStartDate] [datetime] NULL,
	[FirstSeenDate] [datetime] NOT NULL,
	[LastUpdate] [datetime] NULL,
 CONSTRAINT [PK_TBL_NPSSITechRouteSchedule] PRIMARY KEY CLUSTERED 
(
	[JobNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_NPSTechnicianPay]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_NPSTechnicianPay](
	[Unit] [varchar](7) NOT NULL,
	[NPSID] [varchar](7) NOT NULL,
	[EmployeeID] [varchar](11) NOT NULL,
	[EmployeePay] [decimal](7, 3) NOT NULL,
 CONSTRAINT [PK_TBL_NPSTechnicianPay] PRIMARY KEY CLUSTERED 
(
	[Unit] ASC,
	[NPSID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_PaySummaryEndOfWeek]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_PaySummaryEndOfWeek](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeID] [char](11) NOT NULL,
	[PayGroup] [char](5) NOT NULL,
	[WeekEndDate] [date] NULL,
	[Department] [char](10) NULL,
	[JobCode] [char](6) NULL,
	[JobTitle] [varchar](50) NULL,
	[Description] [varchar](50) NULL,
	[PayrollHours] [decimal](9, 4) NULL,
	[EarnDateTime] [datetime2](7) NULL,
	[EarnDate] [date] NULL,
	[EarnCode] [char](3) NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
	[LastModifiedDateTime] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_TBL_PaySummaryEndOfWeek] PRIMARY KEY CLUSTERED 
(
	[EntryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_PeopleSoftEarningsEntry]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_PeopleSoftEarningsEntry](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[Region] [char](5) NULL,
	[Unit] [char](5) NULL,
	[Location] [char](5) NULL,
	[ManagerName] [varchar](30) NULL,
	[EmployeeID] [char](11) NOT NULL,
	[EmployeeName] [varchar](30) NULL,
	[EnterpriseID] [varchar](8) NULL,
	[JobCode] [char](6) NULL,
	[JobTitle] [varchar](30) NULL,
	[DepartmentID] [char](10) NULL,
	[Division] [char](4) NULL,
	[Endweekdate] [date] NOT NULL,
	[EarnCode] [char](3) NOT NULL,
	[EarnDescription] [varchar](30) NULL,
	[GLExpense] [varchar](11) NULL,
	[MultFactor] [decimal](8, 5) NULL,
	[CompRate] [decimal](16, 4) NULL,
	[Hours] [decimal](16, 4) NULL,
	[OthPy] [decimal](16, 4) NULL,
	[Dollars] [decimal](16, 4) NULL,
 CONSTRAINT [PK_TBL_PeopleSoftEarningsEntry] PRIMARY KEY CLUSTERED 
(
	[EntryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_PeoplesoftEmployee]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_PeoplesoftEmployee](
	[BusinessUnit] [varchar](3) NULL,
	[FinancialRegion] [varchar](5) NULL,
	[FinancialUnit] [varchar](5) NULL,
	[PayGroup] [varchar](5) NULL,
	[NID] [varchar](9) NULL,
	[EmployeeID] [varchar](11) NOT NULL,
	[FullName] [varchar](100) NULL,
	[EmployeeStatus] [varchar](1) NULL,
	[EnterpriseID] [varchar](8) NULL,
	[StartDate] [date] NULL,
	[JobCode] [varchar](6) NULL,
	[JobTitle] [varchar](50) NULL,
	[JobStartDate] [date] NULL,
	[Address] [varchar](255) NULL,
	[Apartment] [varchar](60) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](2) NULL,
	[Zip] [varchar](50) NULL,
	[Gender] [varchar](1) NULL,
	[Department] [varchar](10) NULL,
	[EmployeeType] [varchar](1) NULL,
	[FullPartTime] [varchar](1) NULL,
	[SalaryPlan] [varchar](7) NULL,
	[JobGrade] [varchar](7) NULL,
	[PayType] [varchar](50) NULL,
	[GLDepartment] [varchar](50) NULL,
	[LastUpdate] [datetime] NULL,
 CONSTRAINT [PK_TBL_PeoplesoftEmployee] PRIMARY KEY NONCLUSTERED 
(
	[EmployeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_PeoplesoftEmployeeTermed]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_PeoplesoftEmployeeTermed](
	[BusinessUnit] [varchar](3) NULL,
	[FinancialRegion] [varchar](5) NULL,
	[FinancialUnit] [varchar](5) NULL,
	[PayGroup] [varchar](5) NULL,
	[EmployeeID] [varchar](11) NOT NULL,
	[FullName] [varchar](100) NULL,
	[EnterpriseID] [varchar](8) NULL,
	[JobCode] [varchar](6) NULL,
	[JobTitle] [varchar](50) NULL,
	[StartDate] [date] NULL,
	[EmployeeStatus] [varchar](1) NULL,
	[TermedEffectiveDate] [date] NOT NULL,
	[ActionDate] [date] NULL,
	[ActionType] [varchar](3) NULL,
	[ActionReason] [varchar](3) NULL,
	[ActionDescription] [varchar](50) NULL,
	[LastUpdate] [datetime] NOT NULL,
 CONSTRAINT [PK_TBL_PeoplesoftEmployeeTermed] PRIMARY KEY CLUSTERED 
(
	[EmployeeID] ASC,
	[TermedEffectiveDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_PeopleSoftPaycheckData]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_PeopleSoftPaycheckData](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[AccountingYearWeek] [int] NOT NULL,
	[Region] [char](7) NULL,
	[Unit] [char](7) NULL,
	[FinancialRegion] [char](5) NULL,
	[FinancialUnit] [char](7) NULL,
	[PayGroup] [varchar](7) NULL,
	[DepartmentID] [varchar](12) NULL,
	[EmployeeID] [varchar](12) NULL,
	[EmployeeName] [varchar](50) NULL,
	[EarnCode] [varchar](3) NULL,
	[Earnings] [decimal](16, 4) NULL,
	[Hours] [decimal](16, 4) NULL,
	[JobCode] [varchar](8) NULL,
	[EarnEndDate] [datetime2](7) NULL,
	[PayEndDate] [datetime2](7) NULL,
	[FinancialPayAccount] [varchar](7) NULL,
	[GLDepartment] [varchar](5) NULL,
 CONSTRAINT [PK_TBL_PeopleSoft_PaycheckData] PRIMARY KEY CLUSTERED 
(
	[EntryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_RedPrairiePunchDetail]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_RedPrairiePunchDetail](
	[PeopleSoftID] [char](11) NOT NULL,
	[WorkDate] [date] NOT NULL,
	[StartDay] [datetime2](7) NULL,
	[StartPay] [datetime2](7) NULL,
	[LunchStart] [datetime2](7) NULL,
	[LunchEnd] [datetime2](7) NULL,
	[LunchStart2] [datetime2](7) NULL,
	[LunchEnd2] [datetime2](7) NULL,
	[EndPay] [datetime2](7) NULL,
	[EndDay] [datetime2](7) NULL,
	[LastUpdateDate] [datetime2](0) NOT NULL,
 CONSTRAINT [PK_TBL_RedPrairiePunchDetail] PRIMARY KEY NONCLUSTERED 
(
	[PeopleSoftID] ASC,
	[WorkDate] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_SSTTechnician]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_SSTTechnician](
	[Unit] [varchar](7) NULL,
	[Location] [varchar](5) NULL,
	[EnterpriseID] [varchar](8) NOT NULL,
	[EmployeeID] [varchar](11) NULL,
	[NPSID] [varchar](7) NULL,
	[IPAddress] [varchar](20) NULL,
	[CurrentSoftwareVersion] [int] NULL,
	[PendingSoftwareVersion] [int] NULL,
	[SocialSecurityNumber] [varchar](9) NULL,
	[SupportDataDownloaded] [varchar](1) NULL,
	[TimeZoneFactor] [smallint] NULL,
	[SendAutoSyn] [varchar](1) NULL,
	[WLSSyn] [varchar](1) NULL,
	[HAL] [varchar](1) NULL,
	[ULOLogDate] [char](10) NULL,
	[CSAT] [varchar](1) NULL,
	[SYWR] [varchar](1) NULL,
	[LMC] [varchar](1) NULL,
	[EMROORD] [varchar](1) NULL,
	[AIM] [varchar](1) NULL,
	[PRLotto] [varchar](1) NULL,
	[LMCDMZ] [varchar](1) NULL,
	[LMCTCP] [varchar](1) NULL,
	[NewSST] [varchar](1) NULL,
	[TI] [varchar](1) NULL,
 CONSTRAINT [PK_TBL_SSTTechnician] PRIMARY KEY CLUSTERED 
(
	[EnterpriseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_TechRoutes]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_TechRoutes](
	[Unit] [char](7) NOT NULL,
	[TechID] [char](7) NOT NULL,
	[EnterpriseID] [varchar](10) NOT NULL,
	[RouteDate] [date] NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[StatusCode] [varchar](15) NULL,
	[ScheduleFromTime] [datetime] NULL,
	[ScheduleToTime] [datetime] NULL,
	[EstimatedStart] [datetime] NULL,
	[LastUpdate] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_UnitCrossReference]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_UnitCrossReference](
	[Unit] [char](7) NOT NULL,
	[UnitType] [varchar](40) NULL,
	[UnitFormat] [varchar](15) NULL,
	[ReportingUnit] [char](7) NULL,
	[ReportingDistrict] [char](7) NULL,
	[ReportingTerritory] [char](7) NULL,
	[ReportingRegion] [char](7) NULL,
	[OwnerLDAPID] [varchar](28) NULL,
	[InactiveDate] [date] NULL,
	[DisplayName] [varchar](50) NULL,
	[NPSName] [varchar](30) NULL,
	[DirectoryName] [varchar](50) NULL,
	[PeopleSoftUnit] [char](7) NULL,
	[NAIName] [varchar](50) NULL,
	[NAIUnit] [varchar](7) NULL,
	[NPSTypeCode] [varchar](3) NULL,
	[DirectoryType] [varchar](3) NULL,
	[DirectorySubType] [varchar](2) NULL,
	[DateOpen] [date] NULL,
	[Address] [varchar](100) NULL,
	[City] [varchar](100) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](9) NULL,
	[LocnNumber] [varchar](7) NULL,
	[RetailRegion] [char](7) NULL,
	[RetailDistrict] [char](7) NULL,
	[RetailDDC] [char](7) NULL,
	[RetailRRC] [char](7) NULL,
	[InhomeUnit] [char](7) NULL,
	[InhomeRRC] [char](7) NULL,
	[InhomePDC] [char](7) NULL,
	[ASAUnitNumber] [char](7) NULL,
	[TimeZone] [int] NULL,
	[DOSUnit] [varchar](7) NULL,
	[DateClosed] [date] NULL,
	[IsMother] [bit] NOT NULL,
	[Note] [varchar](1000) NULL,
	[LastUpdate] [datetime2](0) NOT NULL,
	[LastUpdateBy] [varchar](28) NOT NULL,
 CONSTRAINT [PK_TBL_UnitCrossReference] PRIMARY KEY NONCLUSTERED 
(
	[Unit] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_UnitCrossReferenceGeocoding]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_UnitCrossReferenceGeocoding](
	[Unit] [char](7) NOT NULL,
	[InputAddress] [varchar](500) NOT NULL,
	[PlaceID] [varchar](300) NULL,
	[StreetNumber] [varchar](300) NULL,
	[StreetNumberLong] [varchar](300) NULL,
	[Route] [varchar](300) NULL,
	[RouteLong] [varchar](300) NULL,
	[SubPremise] [varchar](300) NULL,
	[SubPremiseLong] [varchar](300) NULL,
	[Neighborhood] [varchar](300) NULL,
	[NeighborhoodLong] [varchar](300) NULL,
	[Locality] [varchar](300) NULL,
	[LocalityLong] [varchar](300) NULL,
	[AdministrativeAreaLevelThree] [varchar](300) NULL,
	[AdministrativeAreaLevelThreeLong] [varchar](300) NULL,
	[AdministrativeAreaLevelTwo] [varchar](300) NULL,
	[AdministrativeAreaLevelTwoLong] [varchar](300) NULL,
	[AdministrativeAreaLevelOne] [varchar](300) NULL,
	[AdministrativeAreaLevelOneLong] [varchar](300) NULL,
	[Country] [varchar](300) NULL,
	[CountryLong] [varchar](300) NULL,
	[PostalCode] [varchar](300) NULL,
	[PostalCodeLong] [varchar](300) NULL,
	[PostalCodeSuffix] [varchar](300) NULL,
	[PostalCodeSuffixLong] [varchar](300) NULL,
	[FormattedAddress] [varchar](300) NULL,
	[LocationType] [varchar](300) NULL,
	[Latitude] [decimal](17, 13) NOT NULL,
	[Longitude] [decimal](17, 13) NOT NULL,
	[Location] [geography] NOT NULL,
	[NorthEastLatitude] [decimal](17, 13) NULL,
	[NorthEastLongitude] [decimal](17, 13) NULL,
	[SouthWestLatitude] [decimal](17, 13) NULL,
	[SouthWestLongitude] [decimal](17, 13) NULL,
	[InsertDate] [datetime] NOT NULL,
 CONSTRAINT [PK_TBL_UnitCrossReferenceGeocoding] PRIMARY KEY CLUSTERED 
(
	[Unit] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[wfm_Punch_Audits_20180930]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[wfm_Punch_Audits_20180930](
	[Column 0] [varchar](50) NULL,
	[Column 1] [varchar](50) NULL,
	[Column 2] [varchar](50) NULL,
	[Column 3] [varchar](50) NULL,
	[Column 4] [varchar](50) NULL,
	[Column 5] [varchar](50) NULL,
	[Column 6] [varchar](50) NULL,
	[Column 7] [varchar](50) NULL,
	[Column 8] [varchar](50) NULL,
	[Column 9] [varchar](50) NULL,
	[Column 10] [varchar](50) NULL,
	[Column 11] [varchar](50) NULL,
	[Column 12] [varchar](50) NULL,
	[Column 13] [varchar](50) NULL,
	[Column 14] [varchar](50) NULL,
	[Column 15] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[WFM_SST_EOW_pay_summary_20181002]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WFM_SST_EOW_pay_summary_20181002](
	[Column 0] [varchar](50) NULL,
	[Column 1] [varchar](50) NULL,
	[Column 2] [varchar](50) NULL,
	[Column 3] [varchar](50) NULL,
	[Column 4] [varchar](50) NULL,
	[Column 5] [varchar](50) NULL,
	[Column 6] [varchar](50) NULL,
	[Column 7] [varchar](50) NULL,
	[Column 8] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[WFM_SST_EOW_punch_detail_20180929]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WFM_SST_EOW_punch_detail_20180929](
	[Column 0] [varchar](50) NULL,
	[Column 1] [varchar](50) NULL,
	[Column 2] [varchar](50) NULL,
	[Column 3] [varchar](50) NULL,
	[Column 4] [varchar](50) NULL,
	[Column 5] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[WFM_SST_WTD_pay_summary_20181005]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WFM_SST_WTD_pay_summary_20181005](
	[Column 0] [varchar](50) NULL,
	[Column 1] [varchar](50) NULL,
	[Column 2] [varchar](50) NULL,
	[Column 3] [varchar](50) NULL,
	[Column 4] [varchar](50) NULL,
	[Column 5] [varchar](50) NULL,
	[Column 6] [varchar](50) NULL,
	[Column 7] [varchar](50) NULL,
	[Column 8] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[WFM_SST_WTD_punch_detail_20181005]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WFM_SST_WTD_punch_detail_20181005](
	[Column 0] [varchar](50) NULL,
	[Column 1] [varchar](50) NULL,
	[Column 2] [varchar](50) NULL,
	[Column 3] [varchar](50) NULL,
	[Column 4] [varchar](50) NULL,
	[Column 5] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[VIEW_Employee]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








CREATE VIEW [dbo].[VIEW_Employee]
AS

SELECT 
	UniqueID
	,BusinessUnit
	,FinancialRegion
	,FinancialUnit
	,Region
	,Unit
	,Department
	,SearsDepartment
	,SearsDepartmentName
	,GLDepartment
	,Location
	,LocationDescription
	,PayType
	,PayGroup
	,JobGrade
	,EnterpriseID
	,EmployeeID
	,NPSID
	,SalesID
	,EmployeeStatus
	,EmployeeType
	,LastName
	,FirstName
	,MiddleName
	,FullName
	,DisplayName
	,Email
	,OfficePhone
	,MobilePhone
	,MobilePhoneProvider
	,CASE WHEN JobTitle IN ('Contractor', 'TSR', 'Supervisor/Team Manager') THEN 'VNDRGRP' ELSE JobCode END AS JobCode
	,JobTitle
	,StartDate
	,JobStartDate
	,TermDate
	,ManagerEnterpriseID
	,ManagerEmployeeID
	,ManagerStartDate
	,RowStartDate
	,RowEndDate
FROM 
	Employee.dbo.TBL_Employee_New WITH (NOLOCK)








GO
/****** Object:  View [dbo].[VIEW_CellControlVehicleLocationLatest]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[VIEW_CellControlVehicleLocationLatest]
AS
	SELECT 
		e.Unit
		,d.TechEnterpriseID
		,vl.VehicleDongleMacAddress
		,vl.PhoneNumber
		,vl.LocationTime 
		--,DATEADD(hour, DATEDIFF (hour, GETUTCDATE(), GETDATE()), LocationTime) AS LocationTime
		,vl.ManagerEnterpriseIDKey
		,vl.ManagerEnterpriseID
		,vl.ActiveTrip
		,vl.Longitude
		,vl.Latitude
		,vl.Heading
		,vl.Speed
		,vl.InsertDateTime
		,vl.ModifiedDateTime
	FROM 
		Employee.dbo.TBL_CellControlVehicleLocation vl with (nolock) 
	INNER JOIN
		(SELECT
			PhoneNumber
			,MAX(LocationTime) AS MaxLocationTime
		FROM 
			Employee.dbo.TBL_CellControlVehicleLocation with (nolock) 
		GROUP BY
			PhoneNumber) vlmax
	ON
		vl.PhoneNumber = vlmax.PhoneNumber
		AND	vl.LocationTime = vlmax.MaxLocationTime
JOIN 
	(	SELECT
			d.*
		FROM
			Employee.dbo.TBL_CellControlDeviceInformation as d with (nolock) 
		JOIN
			(	SELECT
					UserName
					,MAX(LastSeenDateTime) AS LST
				FROM 
					Employee.dbo.TBL_CellControlDeviceInformation
				GROUP BY
					UserName
			) l
			ON d.UserName = l.UserName
			AND d.LastSeenDateTime = l.LST
	) d
	on d.PhoneNumber = vl.PhoneNumber
JOIN 
	Employee.dbo.VIEW_Employee as e with (nolock) 
	ON e.EnterpriseID = d.TechEnterpriseID	
	WHERE 
		d.TechEnterpriseID <> ''
		AND vl.LocationTime >= CAST(getdate() as date)
		





GO
/****** Object:  View [dbo].[VIEW_EmployeeHierarchyIHHVACTechRegion]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE VIEW [dbo].[VIEW_EmployeeHierarchyIHHVACTechRegion]
AS

	SELECT
		EnterpriseID
		,NPSID
		,FullName
		,TMDTMEnterpriseID
		,TMDTMFullName
		,DSMRTMEnterpriseID
		,DSMRTMFullName
		,Unit
		,UnitName
		,TFDEnterpriseID
		,TFDFullName
		,Territory
		,TerritoryName
		,Region
		,RegionName
		,RVPDirEnterpriseID
		,RVPDirFullName
	FROM
		Employee.dbo.TBL_EmployeeHierarchyIHHVACTechRegion WITH (NOLOCK)





GO
/****** Object:  View [dbo].[VIEW_EmployeeHierarchyIHHVACUnitRegion]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [dbo].[VIEW_EmployeeHierarchyIHHVACUnitRegion]
AS

SELECT 
	Unit
	,UnitName
	,TFDEnterpriseID
	,TFDFullName
	,Territory
	,TerritoryName
	,Region
	,RegionName
	,RVPDirEnterpriseID
	,RVPDirFullName
FROM
	Employee.dbo.TBL_EmployeeHierarchyIHHVACUnitRegion WITH (NOLOCK)




GO
/****** Object:  View [dbo].[VIEW_EmployeeHierarchyIHTechRegion]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[VIEW_EmployeeHierarchyIHTechRegion]
AS

	SELECT
            EnterpriseID
            ,NPSID
            ,FullName
            ,TMEnterpriseID
            ,TMFullName
            ,DSMEnterpriseID
            ,DSMFullName
            ,Unit
            ,UnitName
            ,TFDEnterpriseID
            ,TFDFullName
            ,TerritoryName
            ,Region
            ,RegionName
            ,RVPEnterpriseID
            ,RVPFullName
      FROM
            Employee.dbo.TBL_EmployeeHierarchyIHTechRegion WITH (NOLOCK)



GO
/****** Object:  View [dbo].[VIEW_EmployeeHierarchyIHUnitRegion]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[VIEW_EmployeeHierarchyIHUnitRegion]
AS

SELECT 
	Unit
	,UnitName
	,TFDEnterpriseID
	,TFDFullName
	,TerritoryName
	,Region
	,RegionName
	,RVPEnterpriseID
	,RVPFullName
FROM
	Employee.dbo.TBL_EmployeeHierarchyIHUnitRegion WITH (NOLOCK)


GO
/****** Object:  View [dbo].[VIEW_EmployeeTechHubPunchSummary]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VIEW_EmployeeTechHubPunchSummary]
AS
SELECT
	s.ServiceUnit AS un_no
	,e.NPSID AS emp_id
	,s.TechnicianEnterpriseID AS emp_racf
	,s.RouteDate AS rte_dt
	,s.StartDay AS pi_tm
	,s.StartPay AS sp_tm
	,NULL AS br_tm -- s.StartTruck AS br_tm
	,s.FirstSOStart AS fso_tm
	,s.StartLunch AS ln_sta_tm
	,s.EndLunch AS ln_end_tm
	,s.LastSOStart AS lso_tm
	,s.EndRoute AS er_tm
	,s.EndPay AS ep_tm
	,s.EndDay AS po_tm
	,ISNULL(d.bk_min, 0) AS bk_min
	,ISNULL(d.mt_min, 0) AS mt_min
	,ISNULL(d.tr_min, 0) AS tr_min
	,ISNULL(d.mc_min, 0) AS mc_min
	,s.LunchCount AS num_ln
	,GETDATE() AS insert_dt_tm
FROM
	(	SELECT
			p.RouteDate
			,p.ServiceUnit
			,p.TechnicianEnterpriseID
			,MIN(CASE WHEN p.EntryType = 'Start Day' THEN CAST(p.RouteDateTime AS TIME(0)) END) AS StartDay
			,MIN(CASE WHEN p.EntryType = 'Start Pay' THEN CAST(p.RouteDateTime AS TIME(0)) END) AS StartPay
			,MIN(CASE WHEN p.EntryType = 'Start Truck' THEN CAST(p.RouteDateTime AS TIME(0)) END) AS StartTruck
			,MIN(CASE WHEN p.EntryType = 'Start Order' THEN CAST(p.RouteDateTime AS TIME(0)) END) AS FirstSOStart
			,MIN(CASE WHEN p.EntryType = 'Start Lunch' THEN CAST(p.RouteDateTime AS TIME(0)) END) AS StartLunch
			,MIN(CASE WHEN p.EntryType = 'End Lunch' THEN CAST(p.RouteDateTime AS TIME(0)) END) AS EndLunch
			,MAX(CASE WHEN p.EntryType = 'End Order' THEN CAST(p.RouteDateTime AS TIME(0)) END) AS LastSOStart
			,MIN(CASE WHEN p.EntryType = 'End Route' THEN CAST(p.RouteDateTime AS TIME(0)) END) AS EndRoute
			,MIN(CASE WHEN p.EntryType = 'End Pay' THEN CAST(p.RouteDateTime AS TIME(0)) END) AS EndPay
			,MIN(CASE WHEN p.EntryType = 'End Day' THEN CAST(p.RouteDateTime AS TIME(0)) END) AS EndDay
			,MAX(CASE WHEN p.EntryType = 'Start Lunch' THEN p.EntryNumber ELSE 0 END) AS LunchCount
		FROM 
			Employee.dbo.TBL_EmployeeTechHubTimepunches p WITH (NOLOCK)
		WHERE
			p.RouteDate > '2018-12-29'
		GROUP BY
			p.RouteDate
			,p.ServiceUnit
			,p.TechnicianEnterpriseID	
	) s
LEFT JOIN
	Employee.dbo.TBL_Employee_New e WITH (NOLOCK)
	ON s.TechnicianEnterpriseID = e.EnterpriseID
LEFT JOIN
	(	SELECT
			d.ServiceUnit
			,d.TechnicianEnterpriseID
			,d.RouteDate
			,SUM(CASE WHEN d.EventCategory = 'Break' THEN d.Duration ELSE 0 END) AS bk_min
			,SUM(CASE WHEN d.EventCategory = 'Meeting' THEN d.Duration ELSE 0 END) AS mt_min
			,SUM(CASE WHEN d.EventCategory = 'Training' THEN d.Duration ELSE 0 END) AS tr_min
			,SUM(CASE WHEN d.EventCategory = 'Misc' THEN d.Duration ELSE 0 END) AS mc_min

			,SUM(CASE WHEN d.EventCategory = 'Lunch' THEN d.Duration ELSE 0 END) AS LunchTime
			,SUM(CASE WHEN d.EventCategory = 'Lunch' THEN 1 ELSE 0 END) AS LunchCount
			,SUM(CASE WHEN d.EventCategory = 'Meeting' THEN 1 ELSE 0 END) AS MeetingCount
			,SUM(CASE WHEN d.EventCategory = 'Training' THEN 1 ELSE 0 END) AS TrainingCount
			,SUM(CASE WHEN d.EventCategory = 'Order' THEN d.Duration ELSE 0 END) AS OrderTime
			,SUM(CASE WHEN d.EventCategory = 'Order' THEN 1 ELSE 0 END) AS OrderCount
			,SUM(CASE WHEN d.EventCategory = 'Misc' THEN 1 ELSE 0 END) AS MiscCount
			,SUM(CASE WHEN d.EventCategory = 'Break' THEN 1 ELSE 0 END) AS BreakCount
			,COUNT(*) AS Events
			,SUM(d.Duration) AS Duration
		FROM
			(	SELECT
					d.*
					,ISNULL(DATEDIFF(MINUTE, d.StartTime, d.EndTime), 0) AS Duration
				FROM
					(	SELECT
							p.RouteDate
							,p.ServiceUnit
							,p.TechnicianEnterpriseID
							,r.GroupType
							,r.EventCategory
							,p.EntryNumber
							,MIN(CASE WHEN LEFT(p.EntryType, 3) = 'Sta' THEN CAST(p.RouteDateTime AS TIME(0)) END) AS StartTime
							,MAX(CASE WHEN LEFT(p.EntryType, 3) = 'End' THEN CAST(p.RouteDateTime AS TIME(0)) END) AS EndTime
						FROM 
							Employee.dbo.TBL_EmployeeTechHubTimepunches p WITH (NOLOCK)
						JOIN
							(	SELECT 
									*
								FROM
									Employee.dbo.TBL_EmployeeTechHubTimepunchReasons WITH (NOLOCK)
								WHERE
									IncludeEntry = 2
							) r
							ON p.EntryType = r.EntryType
						WHERE
							p.RouteDate > '2018-12-29'
						GROUP BY
							p.RouteDate
							,p.ServiceUnit
							,p.TechnicianEnterpriseID
							,r.GroupType
							,r.EventCategory
							,p.EntryNumber
					) d
			) d
		GROUP BY
			d.RouteDate
			,d.ServiceUnit
			,d.TechnicianEnterpriseID
	) d
	ON d.TechnicianEnterpriseID = s.TechnicianEnterpriseID
	AND d.RouteDate = s.RouteDate
	AND d.ServiceUnit = s.ServiceUnit

GO
/****** Object:  View [dbo].[VIEW_Profit]    Script Date: 7/15/2019 10:32:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








CREATE VIEW [dbo].[VIEW_Profit]
AS

SELECT 
	*
FROM 
	Employee.dbo.profit_v2 WITH (NOLOCK)
WHERE
	acctg_yr = 2018
	AND acctg_mth > 6








GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_VehicleLocation_LocationTime_EnterpriseID]    Script Date: 7/15/2019 10:32:27 AM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_VehicleLocation_LocationTime_EnterpriseID] ON [dbo].[_DeleteOn20181101_TBL_VehicleLocation]
(
	[LocationTime] ASC,
	[EnterpriseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IDX_TBL_ClickRoutingSchedule_UnitServiceOrder]    Script Date: 7/15/2019 10:32:27 AM ******/
CREATE NONCLUSTERED INDEX [IDX_TBL_ClickRoutingSchedule_UnitServiceOrder] ON [dbo].[TBL_ClickRoutingSchedule]
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_MissingIndex_enterpriseid]    Script Date: 7/15/2019 10:32:27 AM ******/
CREATE NONCLUSTERED INDEX [IX_MissingIndex_enterpriseid] ON [dbo].[TBL_Employee]
(
	[EnterpriseID] ASC
)
INCLUDE ( 	[Email]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_Employee_New_EnterpriseID]    Script Date: 7/15/2019 10:32:27 AM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_Employee_New_EnterpriseID] ON [dbo].[TBL_Employee_New]
(
	[EnterpriseID] ASC
)
INCLUDE ( 	[FullName],
	[Email],
	[OfficePhone],
	[MobilePhone]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_Employee_New_NPSID_Unit]    Script Date: 7/15/2019 10:32:27 AM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_Employee_New_NPSID_Unit] ON [dbo].[TBL_Employee_New]
(
	[NPSID] ASC,
	[Unit] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_EmployeeHierarchyIHTechRegion_Unit]    Script Date: 7/15/2019 10:32:27 AM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_EmployeeHierarchyIHTechRegion_Unit] ON [dbo].[TBL_EmployeeHierarchyIHTechRegion]
(
	[NPSID] ASC,
	[Unit] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_EmployeeID_EmpID]    Script Date: 7/15/2019 10:32:27 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_TBL_EmployeeID_EmpID] ON [dbo].[TBL_EmployeeID]
(
	[EmployeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_EmployeeName_EnterpriseID]    Script Date: 7/15/2019 10:32:27 AM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_EmployeeName_EnterpriseID] ON [dbo].[TBL_EmployeeName]
(
	[EnterpriseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_EmployeeTechHubTimepunches_TechnicianEnterpriseID_RouteDateTime_EntryType]    Script Date: 7/15/2019 10:32:27 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_TBL_EmployeeTechHubTimepunches_TechnicianEnterpriseID_RouteDateTime_EntryType] ON [dbo].[TBL_EmployeeTechHubTimepunches]
(
	[TechnicianEnterpriseID] ASC,
	[RouteDateTime] ASC,
	[EntryType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_TechRoutes_EnterpriseIDRouteDate]    Script Date: 7/15/2019 10:32:27 AM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_TechRoutes_EnterpriseIDRouteDate] ON [dbo].[TBL_TechRoutes]
(
	[EnterpriseID] ASC,
	[RouteDate] ASC
)
INCLUDE ( 	[Unit],
	[TechID],
	[ServiceOrder],
	[StatusCode],
	[ScheduleFromTime],
	[ScheduleToTime],
	[EstimatedStart],
	[LastUpdate]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_TechRoutes_UnitServiceOrder]    Script Date: 7/15/2019 10:32:27 AM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_TechRoutes_UnitServiceOrder] ON [dbo].[TBL_TechRoutes]
(
	[Unit] ASC,
	[ServiceOrder] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[_DeleteOn20181101_TBL_VehicleLocation] ADD  CONSTRAINT [DF_TBL_VehicleLocation_InsertDateTime]  DEFAULT (getdate()) FOR [InsertDateTime]
GO
ALTER TABLE [dbo].[a_Associate_History] ADD  CONSTRAINT [DF__a_Associa__LastU__436BFEE3]  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [dbo].[a_Associate_Master] ADD  CONSTRAINT [DF__a_Associa__LastU__45544755]  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [dbo].[a_Badge_ID_LDAP_ID_Pay_Rates] ADD  CONSTRAINT [DF__a_Badge_I__LastU__473C8FC7]  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [dbo].[a_Data_Paths] ADD  CONSTRAINT [DF__a_Data_Pa__LastU__4959E263]  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [dbo].[a_Default_Dept] ADD  CONSTRAINT [DF__a_Default__LastU__4F12BBB9]  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [dbo].[a_Exception_Dept_Insert_Info] ADD  CONSTRAINT [DF__a_Exception__LastU__11D4A34F]  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [dbo].[a_ForeCast_Actual_Factors] ADD  CONSTRAINT [DF_a_ForeCast_Actual_Factors_LastUpdated]  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [dbo].[a_Head_Cnt_Code_Forecast] ADD  CONSTRAINT [DF__a_Head_Cn__LastU__589C25F3]  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [dbo].[a_Head_Cnt_Codes] ADD  CONSTRAINT [DF__a_Head_Cn__LastU__5E54FF49]  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [dbo].[a_HR_Data] ADD  CONSTRAINT [DF__a_HR_Data__LastU__640DD89F]  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [dbo].[a_Lead_Master] ADD  CONSTRAINT [DF__a_Lead_Ma__LastU__69C6B1F5]  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [dbo].[a_RP_Delete_Names] ADD  CONSTRAINT [DF__a_RP_Dele__LastU__6F7F8B4B]  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [dbo].[a_RP_Emp_Lists] ADD  CONSTRAINT [DF__a_RP_Emp___LastU__753864A1]  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [dbo].[a_RP_Pay_Summary] ADD  CONSTRAINT [DF__a_RP_Pay___LastU__7AF13DF7]  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [dbo].[a_RP_Pay_Summary_Import_Log] ADD  CONSTRAINT [DF__a_RP_Pay___LastU__00AA174D]  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [dbo].[a_RP_PW_Access] ADD  CONSTRAINT [DF__a_RP_PW_A__LastU__0662F0A3]  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [dbo].[a_RP_Temp_Agency_Pay_Rates] ADD  CONSTRAINT [DF__a_RP_Temp__LastU__0C1BC9F9]  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [dbo].[a_Support_Insert_Info] ADD  CONSTRAINT [DF__a_Support__LastU__11D4A34F]  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [dbo].[a_Temp_Pay_Rate_Sears_Benefit_Percent] ADD  CONSTRAINT [DF__a_Temp_Pa__LastU__178D7CA5]  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [dbo].[a_Temp_Payroll_Tax_Rates] ADD  CONSTRAINT [DF_a_Temp_Payroll_Tax_Rates_LastUpdated]  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [dbo].[STG_CellControlDeviceInformation] ADD  CONSTRAINT [DF_STG_CellControlDeviceInformation_InsertDateTime]  DEFAULT (getdate()) FOR [InsertDateTime]
GO
ALTER TABLE [dbo].[STG_CellControlVehicleInformation] ADD  CONSTRAINT [DF_STG_CellControlVehicleInformation_InsertDateTime]  DEFAULT (getdate()) FOR [InsertDateTime]
GO
ALTER TABLE [dbo].[STG_CellControlVehicleLocation] ADD  CONSTRAINT [DF_STG_CellControlVehicleLocation_InsertDateTime]  DEFAULT (getdate()) FOR [InsertDateTime]
GO
ALTER TABLE [dbo].[STG_EmployeeTechHubTimepunches] ADD  CONSTRAINT [DF_STG_EmployeeTechHubTimepunches_CreatedDateTime]  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[TBL_CellControlDeviceInformation] ADD  CONSTRAINT [DF_TBL_CellControlDeviceInformation_InsertDateTime]  DEFAULT (getdate()) FOR [InsertDateTime]
GO
ALTER TABLE [dbo].[TBL_CellControlVehicleInformation] ADD  CONSTRAINT [DF_TBL_CellControlVehicleInformation_InsertDateTime]  DEFAULT (getdate()) FOR [InsertDateTime]
GO
ALTER TABLE [dbo].[TBL_CellControlVehicleLocation] ADD  CONSTRAINT [DF_TBL_CellControlVehicleLocation_InsertDateTime]  DEFAULT (getdate()) FOR [InsertDateTime]
GO
ALTER TABLE [dbo].[TBL_ClickRoutingSchedule] ADD  DEFAULT ((-1)) FOR [CallNumber]
GO
ALTER TABLE [dbo].[TBL_ClickRoutingSchedule] ADD  DEFAULT ((-1)) FOR [AssignmentTypeCode]
GO
ALTER TABLE [dbo].[TBL_EmployeeHistoricalAssignmentDate] ADD  DEFAULT (getdate()) FOR [StartDate]
GO
ALTER TABLE [dbo].[TBL_EmployeeHistoricalBusinessUnit] ADD  DEFAULT (getdate()) FOR [StartDate]
GO
ALTER TABLE [dbo].[TBL_EmployeeHistoricalCompanyCode] ADD  DEFAULT (getdate()) FOR [StartDate]
GO
ALTER TABLE [dbo].[TBL_EmployeeHistoricalDepartmentCode] ADD  DEFAULT (getdate()) FOR [StartDate]
GO
ALTER TABLE [dbo].[TBL_EmployeeHistoricalDepartmentID] ADD  DEFAULT (getdate()) FOR [StartDate]
GO
ALTER TABLE [dbo].[TBL_EmployeeHistoricalEmployeeType] ADD  DEFAULT (getdate()) FOR [StartDate]
GO
ALTER TABLE [dbo].[TBL_EmployeeHistoricalFinancialUnit] ADD  DEFAULT (getdate()) FOR [StartDate]
GO
ALTER TABLE [dbo].[TBL_EmployeeHistoricalJobCode] ADD  DEFAULT (getdate()) FOR [StartDate]
GO
ALTER TABLE [dbo].[TBL_EmployeeHistoricalJobGrade] ADD  DEFAULT (getdate()) FOR [StartDate]
GO
ALTER TABLE [dbo].[TBL_EmployeeHistoricalManager] ADD  CONSTRAINT [DF_TBL_EmployeeHistoricalManager_StartDate]  DEFAULT (getdate()) FOR [StartDate]
GO
ALTER TABLE [dbo].[TBL_EmployeeHistoricalPayGroup] ADD  DEFAULT (getdate()) FOR [StartDate]
GO
ALTER TABLE [dbo].[TBL_EmployeeHistoricalPayRate] ADD  DEFAULT (getdate()) FOR [StartDate]
GO
ALTER TABLE [dbo].[TBL_EmployeeHistoricalPayType] ADD  DEFAULT (getdate()) FOR [StartDate]
GO
ALTER TABLE [dbo].[TBL_EmployeeHistoricalSalaryPlan] ADD  DEFAULT (getdate()) FOR [StartDate]
GO
ALTER TABLE [dbo].[TBL_EmployeeHistoricalStatusCode] ADD  DEFAULT (getdate()) FOR [StartDate]
GO
ALTER TABLE [dbo].[TBL_EmployeeTechHubTimepunches] ADD  CONSTRAINT [DF_TBL_EmployeeTechHubTimepunches_CreatedDateTime]  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[TBL_EmployeeTechHubTimepunchReasons] ADD  CONSTRAINT [DF_TBL_EmployeeTechHubTimepunchReasons]  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[TBL_Enumerations] ADD  DEFAULT ((0)) FOR [SortOrder]
GO
ALTER TABLE [dbo].[TBL_Enumerations] ADD  DEFAULT ((1)) FOR [IsEnabled]
GO
ALTER TABLE [dbo].[TBL_Enumerations] ADD  DEFAULT ((1)) FOR [IsUserAllowed]
GO
ALTER TABLE [dbo].[TBL_Enumerations] ADD  DEFAULT (getdate()) FOR [LastModified]
GO
ALTER TABLE [dbo].[TBL_NPSSITechRouteSchedule] ADD  CONSTRAINT [DF_TBL_SITechRouteSchedule_FirstSeenDate]  DEFAULT (getdate()) FOR [FirstSeenDate]
GO
ALTER TABLE [dbo].[TBL_PaySummaryEndOfWeek] ADD  CONSTRAINT [DF_TBL_PaySummaryEndOfWeek_CreatedDateTime]  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[TBL_PaySummaryEndOfWeek] ADD  CONSTRAINT [DF_TBL_PaySummaryEndOfWeek_LastModifiedDateTime]  DEFAULT (getdate()) FOR [LastModifiedDateTime]
GO
ALTER TABLE [dbo].[TBL_RedPrairiePunchDetail] ADD  DEFAULT (getdate()) FOR [LastUpdateDate]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "TBL_Employee_New (dbo)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 249
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VIEW_Employee'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VIEW_Employee'
GO
USE [master]
GO
ALTER DATABASE [Employee] SET  READ_WRITE 
GO
