USE [master]
GO
/****** Object:  Database [ServiceOrder]    Script Date: 9/18/2019 6:58:20 PM ******/
CREATE DATABASE [ServiceOrder]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ServiceOrder', FILENAME = N'D:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\ServiceOrder.mdf' , SIZE = 291587072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 256000KB )
 LOG ON 
( NAME = N'ServiceOrder_log', FILENAME = N'E:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\Data\ServiceOrder_log.ldf' , SIZE = 7168KB , MAXSIZE = 2048GB , FILEGROWTH = 256000KB ), 
( NAME = N'ServiceOrder_log2', FILENAME = N'F:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\Data\ServiceOrder_log2.ldf' , SIZE = 512000KB , MAXSIZE = 2048GB , FILEGROWTH = 256000KB )
GO
ALTER DATABASE [ServiceOrder] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ServiceOrder].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ServiceOrder] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ServiceOrder] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ServiceOrder] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ServiceOrder] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ServiceOrder] SET ARITHABORT OFF 
GO
ALTER DATABASE [ServiceOrder] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ServiceOrder] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [ServiceOrder] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ServiceOrder] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ServiceOrder] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ServiceOrder] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ServiceOrder] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ServiceOrder] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ServiceOrder] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ServiceOrder] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ServiceOrder] SET  DISABLE_BROKER 
GO
ALTER DATABASE [ServiceOrder] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ServiceOrder] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ServiceOrder] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ServiceOrder] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ServiceOrder] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ServiceOrder] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ServiceOrder] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ServiceOrder] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [ServiceOrder] SET  MULTI_USER 
GO
ALTER DATABASE [ServiceOrder] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ServiceOrder] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ServiceOrder] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ServiceOrder] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [ServiceOrder]
GO
/****** Object:  User [os_spt]    Script Date: 9/18/2019 6:58:21 PM ******/
CREATE USER [os_spt] FOR LOGIN [os_spt] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [nfdt_soprodev]    Script Date: 9/18/2019 6:58:22 PM ******/
CREATE USER [nfdt_soprodev] FOR LOGIN [nfdt_soprodev] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [NFDT_reports]    Script Date: 9/18/2019 6:58:22 PM ******/
CREATE USER [NFDT_reports] FOR LOGIN [NFDT_reports] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [nfdt_Partspro]    Script Date: 9/18/2019 6:58:22 PM ******/
CREATE USER [nfdt_Partspro] FOR LOGIN [nfdt_Partspro] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [NFDT_EmpPro]    Script Date: 9/18/2019 6:58:22 PM ******/
CREATE USER [NFDT_EmpPro] FOR LOGIN [NFDT_EmpPro] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [nfdt_dbconnect]    Script Date: 9/18/2019 6:58:22 PM ******/
CREATE USER [nfdt_dbconnect] FOR LOGIN [nfdt_dbconnect] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [nfdt_boss]    Script Date: 9/18/2019 6:58:23 PM ******/
CREATE USER [nfdt_boss] FOR LOGIN [nfdt_boss] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [KMART\njain3]    Script Date: 9/18/2019 6:58:23 PM ******/
CREATE USER [KMART\njain3] FOR LOGIN [KMART\njain3] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [KMART\nfdt_AppMetrics]    Script Date: 9/18/2019 6:58:23 PM ******/
CREATE USER [KMART\nfdt_AppMetrics] FOR LOGIN [KMART\nfdt_AppMetrics] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [KMART\HomeServices-HSNFDTTM-local]    Script Date: 9/18/2019 6:58:23 PM ******/
CREATE USER [KMART\HomeServices-HSNFDTTM-local] FOR LOGIN [KMART\HomeServices-HSNFDTTM-local]
GO
/****** Object:  User [KMART\HomeServices-HSNFDADMLocal]    Script Date: 9/18/2019 6:58:23 PM ******/
CREATE USER [KMART\HomeServices-HSNFDADMLocal] FOR LOGIN [KMART\HomeServices-HSNFDADMLocal] WITH DEFAULT_SCHEMA=[KMART\HomeServices-HSNFDADMLocal]
GO
/****** Object:  User [aws_rr]    Script Date: 9/18/2019 6:58:24 PM ******/
CREATE USER [aws_rr] FOR LOGIN [aws_rr] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  DatabaseRole [db_execute]    Script Date: 9/18/2019 6:58:24 PM ******/
CREATE ROLE [db_execute]
GO
ALTER ROLE [db_execute] ADD MEMBER [os_spt]
GO
ALTER ROLE [db_owner] ADD MEMBER [os_spt]
GO
ALTER ROLE [db_datareader] ADD MEMBER [os_spt]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [os_spt]
GO
ALTER ROLE [db_execute] ADD MEMBER [nfdt_soprodev]
GO
ALTER ROLE [db_datareader] ADD MEMBER [nfdt_soprodev]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [nfdt_soprodev]
GO
ALTER ROLE [db_execute] ADD MEMBER [nfdt_Partspro]
GO
ALTER ROLE [db_datareader] ADD MEMBER [nfdt_Partspro]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [nfdt_Partspro]
GO
ALTER ROLE [db_execute] ADD MEMBER [nfdt_dbconnect]
GO
ALTER ROLE [db_datareader] ADD MEMBER [nfdt_dbconnect]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [nfdt_dbconnect]
GO
ALTER ROLE [db_execute] ADD MEMBER [nfdt_boss]
GO
ALTER ROLE [db_datareader] ADD MEMBER [nfdt_boss]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [nfdt_boss]
GO
ALTER ROLE [db_owner] ADD MEMBER [KMART\njain3]
GO
ALTER ROLE [db_datareader] ADD MEMBER [KMART\njain3]
GO
ALTER ROLE [db_execute] ADD MEMBER [KMART\HomeServices-HSNFDTTM-local]
GO
ALTER ROLE [db_datareader] ADD MEMBER [KMART\HomeServices-HSNFDTTM-local]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [KMART\HomeServices-HSNFDTTM-local]
GO
ALTER ROLE [db_owner] ADD MEMBER [KMART\HomeServices-HSNFDADMLocal]
GO
ALTER ROLE [db_datareader] ADD MEMBER [aws_rr]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [aws_rr]
GO
/****** Object:  Schema [KMART\HomeServices-HSNFDADMLocal]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE SCHEMA [KMART\HomeServices-HSNFDADMLocal]
GO
/****** Object:  Schema [td]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE SCHEMA [td]
GO
/****** Object:  StoredProcedure [dbo].[PROC_CustomerAddressGeocodingFinalUpdate]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		Aaron Cote
-- Create date: 01/05/2017
-- Description:	For saving the geocoordinates of a customer address
-- =============================================
CREATE PROCEDURE [dbo].[PROC_CustomerAddressGeocodingFinalUpdate]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- Interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- Merge into primary table
	MERGE 
		ServiceOrder.dbo.TBL_CustomerAddressGeocoding AS T
	USING 
		(SELECT
			ServiceUnit
			,ServiceOrder
			,CustomerID
			,LocationSuffix
			,GEOSTAGE.InputAddress
			,PlaceID
			,StreetNumber
			,StreetNumberLong
			,[Route]
			,RouteLong
			,SubPremise
			,SubPremiseLong
			,Neighborhood
			,NeighborhoodLong
			,Locality
			,LocalityLong
			,AdministrativeAreaLevelThree
			,AdministrativeAreaLevelThreeLong
			,AdministrativeAreaLevelTwo
			,AdministrativeAreaLevelTwoLong
			,AdministrativeAreaLevelOne
			,AdministrativeAreaLevelOneLong
			,Country
			,CountryLong
			,PostalCode
			,PostalCodeLong
			,PostalCodeSuffix
			,PostalCodeSuffixLong
			,FormattedAddress
			,LocationType
			,Latitude
			,Longitude
			,Location
			,NorthEastLatitude
			,NorthEastLongitude
			,SouthWestLatitude
			,SouthWestLongitude
			,InsertDate
		FROM
			ServiceOrder.dbo.TBL_CustomerAddressGeocodingStaging GEOSTAGE
		INNER JOIN
			(SELECT
				ServiceUnit
				,ServiceOrder
				,Customer AS CustomerID
				,AddressLocation AS LocationSuffix
				,InputAddress
			FROM
				ServiceOrder.dbo.VIEW_CustomerAddressBaseForGeocoding) GEOBASE
		ON
			GEOSTAGE.InputAddress = GEOBASE.InputAddress) AS S
	ON 
		(T.ServiceUnit = S.ServiceUnit
		AND
		T.ServiceOrder = S.ServiceOrder
		AND
		T.LocationSuffix = S.LocationSuffix)
	WHEN MATCHED THEN
		UPDATE 
		SET 
			T.ServiceUnit = S.ServiceUnit
			,T.ServiceOrder = S.ServiceOrder
			,T.CustomerID = S.CustomerID
			,T.LocationSuffix = S.LocationSuffix
			,T.InputAddress = S.InputAddress
			,T.PlaceID = S.PlaceID
			,T.StreetNumber = S.StreetNumber
			,T.StreetNumberLong = S.StreetNumberLong
			,T.[Route] = S.[Route]
			,T.RouteLong = S.RouteLong
			,T.SubPremise = S.SubPremise
			,T.SubPremiseLong = S.SubPremiseLong
			,T.Neighborhood = S.Neighborhood
			,T.NeighborhoodLong = S.NeighborhoodLong
			,T.Locality = S.Locality
			,T.LocalityLong = S.LocalityLong
			,T.AdministrativeAreaLevelThree = S.AdministrativeAreaLevelThree
			,T.AdministrativeAreaLevelThreeLong = S.AdministrativeAreaLevelThreeLong
			,T.AdministrativeAreaLevelTwo = S.AdministrativeAreaLevelTwo
			,T.AdministrativeAreaLevelTwoLong = S.AdministrativeAreaLevelTwoLong
			,T.AdministrativeAreaLevelOne = S.AdministrativeAreaLevelOne
			,T.AdministrativeAreaLevelOneLong = S.AdministrativeAreaLevelOneLong
			,T.Country = S.Country
			,T.CountryLong = S.CountryLong
			,T.PostalCode = S.PostalCode
			,T.PostalCodeLong = S.PostalCodeLong
			,T.PostalCodeSuffix = S.PostalCodeSuffix
			,T.PostalCodeSuffixLong = S.PostalCodeSuffixLong
			,T.FormattedAddress = S.FormattedAddress
			,T.LocationType = S.LocationType
			,T.Latitude = S.Latitude
			,T.Longitude = S.Longitude
			,T.Location = S.Location
			,T.NorthEastLatitude = S.NorthEastLatitude
			,T.NorthEastLongitude = S.NorthEastLongitude
			,T.SouthWestLatitude = S.SouthWestLatitude
			,T.SouthWestLongitude = S.SouthWestLongitude
			,T.InsertDate = S.InsertDate
	WHEN NOT MATCHED BY TARGET THEN
		INSERT 
			(ServiceUnit
			,ServiceOrder
			,CustomerID
			,LocationSuffix
			,InputAddress
			,PlaceID
			,StreetNumber
			,StreetNumberLong
			,[Route]
			,RouteLong
			,SubPremise
			,SubPremiseLong
			,Neighborhood
			,NeighborhoodLong
			,Locality
			,LocalityLong
			,AdministrativeAreaLevelThree
			,AdministrativeAreaLevelThreeLong
			,AdministrativeAreaLevelTwo
			,AdministrativeAreaLevelTwoLong
			,AdministrativeAreaLevelOne
			,AdministrativeAreaLevelOneLong
			,Country
			,CountryLong
			,PostalCode
			,PostalCodeLong
			,PostalCodeSuffix
			,PostalCodeSuffixLong
			,FormattedAddress
			,LocationType
			,Latitude
			,Longitude
			,Location
			,NorthEastLatitude
			,NorthEastLongitude
			,SouthWestLatitude
			,SouthWestLongitude
			,InsertDate)
		VALUES 
			(S.ServiceUnit
			,S.ServiceOrder
			,S.CustomerID
			,S.LocationSuffix
			,S.InputAddress
			,S.PlaceID
			,S.StreetNumber
			,S.StreetNumberLong
			,S.[Route]
			,S.RouteLong
			,S.SubPremise
			,S.SubPremiseLong
			,S.Neighborhood
			,S.NeighborhoodLong
			,S.Locality
			,S.LocalityLong
			,S.AdministrativeAreaLevelThree
			,S.AdministrativeAreaLevelThreeLong
			,S.AdministrativeAreaLevelTwo
			,S.AdministrativeAreaLevelTwoLong
			,S.AdministrativeAreaLevelOne
			,S.AdministrativeAreaLevelOneLong
			,S.Country
			,S.CountryLong
			,S.PostalCode
			,S.PostalCodeLong
			,S.PostalCodeSuffix
			,S.PostalCodeSuffixLong
			,S.FormattedAddress
			,S.LocationType
			,S.Latitude
			,S.Longitude
			,S.Location
			,S.NorthEastLatitude
			,S.NorthEastLongitude
			,S.SouthWestLatitude
			,S.SouthWestLongitude
			,S.InsertDate);
	
END





GO
/****** Object:  StoredProcedure [dbo].[PROC_CustomerAddressGeocodingInsert]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		Aaron Cote
-- Create date: 01/05/2017
-- Description:	For saving the geocoordinates of a customer address
-- =============================================
CREATE PROCEDURE [dbo].[PROC_CustomerAddressGeocodingInsert]
	@InputAddress varchar(500) = NULL,
	@PlaceID varchar(300) = NULL,
	@StreetNumber varchar(300) = NULL,
	@StreetNumberLong varchar(300) = NULL,
	@Route varchar(300) = NULL,
	@RouteLong varchar(300) = NULL,
	@SubPremise varchar(300) = NULL,
	@SubPremiseLong varchar(300) = NULL,
	@Neighborhood varchar(300) = NULL,
	@NeighborhoodLong varchar(300) = NULL,
	@Locality varchar(300) = NULL,
	@LocalityLong varchar(300) = NULL,
	@AdministrativeAreaLevelThree varchar(300) = NULL,
	@AdministrativeAreaLevelThreeLong varchar(300) = NULL,
	@AdministrativeAreaLevelTwo varchar(300) = NULL,
	@AdministrativeAreaLevelTwoLong varchar(300) = NULL,
	@AdministrativeAreaLevelOne varchar(300) = NULL,
	@AdministrativeAreaLevelOneLong varchar(300) = NULL,
	@Country varchar(300) = NULL,
	@CountryLong varchar(300) = NULL,
	@PostalCode varchar(300) = NULL,
	@PostalCodeLong varchar(300) = NULL,
	@PostalCodeSuffix varchar(300) = NULL,
	@PostalCodeSuffixLong varchar(300) = NULL,
	@FormattedAddress varchar(300) = NULL,
	@LocationType varchar(300) = NULL,
	@Latitude decimal(17,13) = NULL,
	@Longitude decimal(17,13) = NULL,
	@NorthEastLatitude decimal(17,13) = NULL,
	@NorthEastLongitude decimal(17,13) = NULL,
	@SouthWestLatitude decimal(17,13) = NULL,
	@SouthWestLongitude decimal(17,13) = NULL,

	@errorcode VARCHAR(150) = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- Interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- Merge into primary table
	INSERT INTO
		ServiceOrder.dbo.TBL_CustomerAddressGeocodingStaging
		(InputAddress
		,PlaceID
		,StreetNumber
		,StreetNumberLong
		,[Route]
		,RouteLong
		,SubPremise
		,SubPremiseLong
		,Neighborhood
		,NeighborhoodLong
		,Locality
		,LocalityLong
		,AdministrativeAreaLevelThree
		,AdministrativeAreaLevelThreeLong
		,AdministrativeAreaLevelTwo
		,AdministrativeAreaLevelTwoLong
		,AdministrativeAreaLevelOne
		,AdministrativeAreaLevelOneLong
		,Country
		,CountryLong
		,PostalCode
		,PostalCodeLong
		,PostalCodeSuffix
		,PostalCodeSuffixLong
		,FormattedAddress
		,LocationType
		,Latitude
		,Longitude
		,Location
		,NorthEastLatitude
		,NorthEastLongitude
		,SouthWestLatitude
		,SouthWestLongitude
		,InsertDate)
	VALUES
		(@InputAddress
		,@PlaceID
		,@StreetNumber
		,@StreetNumberLong
		,@Route
		,@RouteLong
		,@SubPremise
		,@SubPremiseLong
		,@Neighborhood
		,@NeighborhoodLong
		,@Locality
		,@LocalityLong
		,@AdministrativeAreaLevelThree
		,@AdministrativeAreaLevelThreeLong
		,@AdministrativeAreaLevelTwo
		,@AdministrativeAreaLevelTwoLong
		,@AdministrativeAreaLevelOne
		,@AdministrativeAreaLevelOneLong
		,@Country
		,@CountryLong
		,@PostalCode
		,@PostalCodeLong
		,@PostalCodeSuffix
		,@PostalCodeSuffixLong
		,@FormattedAddress
		,@LocationType
		,@Latitude
		,@Longitude
		,geography::Point(@Latitude, @Longitude, 4326)
		,@NorthEastLatitude
		,@NorthEastLongitude
		,@SouthWestLatitude
		,@SouthWestLongitude
		,GETDATE())
END





GO
/****** Object:  StoredProcedure [dbo].[PROC_EmailTaskSystemGenerated]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Preston Galvanek
-- Create date: 4/19/2018
-- Modified Date: 4/19/2018
-- =============================================
CREATE PROCEDURE [dbo].[PROC_EmailTaskSystemGenerated]
@QueueID int = NULL,
@ReportID int = NULL,
@GroupID int = NULL,
@ErrorCode varchar(150) = NULL OUTPUT
AS
BEGIN  
	DECLARE @EmailBodyHTML as varchar(max) ;
	DECLARE @Today as varchar(120) = convert(varchar, getdate(), 120);
	DECLARE @ReportName as varchar(max);
	DECLARE @GroupName as varchar(max);
	DECLARE @ReportColor as varchar(max);
	DECLARE @QueueName as varchar(max);
	/*
	--Declared below for convinience
	DECLARE @ServiceUnit as varchar(7);
	DECLARE @ServiceOrder as varchar(8);
	DECLARE @BodyTextServiceOrders as varchar(MAX) = ''
	*/
	;
	DECLARE @Email as varchar(max);/*was 250*/

	/* Assign email recipients based off queue, report and group*/
	SELECT
		@Email = 
				STUFF((		
					SELECT ';' + (SELECT DISTINCT e.Email)
					FROM (
						SELECT
							a.EnterpriseID,
							r.ReportID
						FROM 
							ServiceOrder.dbo.TBL_ServiceOrderAssignment as a with (nolock) 
							INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderReports as r with (nolock) on r.ReportID = a.ReportID
							INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderQueue as q with (nolock) on q.QueueID = a.QueueID
							INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderGroup as g with (nolock) on g.GroupID = a.GroupID
							INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderTasks as t with (nolock) on  a.QueueID = t.QueueID AND a.ReportID = t.ReportID  AND a.GroupID = t.GroupID  
							INNER JOIN ServiceOrder.dbo.TBL_Schedule as s with (nolock) on s.OwnerID = a.AssignmentID 
						WHERE 
							t.QueueID = @QueueID
							AND t.ReportID = @ReportID
							AND t.GroupID = @GroupID
							AND s.OwnerType = 'AssignmentID'
							AND s.Status = 1
							AND s.Frequency = 'SS'
						GROUP BY
							a.EnterpriseID,
							r.ReportID
						) as allin
						INNER JOIN Employee.dbo.TBL_Employee as e with (nolock) ON allin.EnterpriseID = e.EnterpriseID
					FOR XML PATH('')			
					)			
				, 1, 1, ''	)
				

	/*Identify Queue, Report and Group descriptive details.*/
	SELECT DISTINCT
		@QueueName = q.QueueName
	FROM 
		ServiceOrder.dbo.TBL_ServiceOrderQueue as q with (nolock)
	WHERE 
		q.QueueID = @QueueID
	
	SELECT DISTINCT
		@ReportName = r.ReportName,
		@ReportColor = COALESCE(r.ReportColor,'#2C2C2C')
	FROM 
		ServiceOrder.dbo.TBL_ServiceOrderReports as r with (nolock)
	WHERE 
		r.ReportID = @ReportID
		
	SELECT DISTINCT
		@GroupName = g.GroupName
	FROM 
		ServiceOrder.dbo.TBL_ServiceOrderGroup as g with (nolock)
	WHERE 
		g.GroupID = @GroupID	

		

    IF @Email is null --Email will never be NULL, it is ''. However, '' emails will not be sent and we want to update the notification table below instead of returning.
	BEGIN 
		SET @ErrorCode += 'Email is not defined. '
	END
      IF @QueueName is null OR @QueueName = ''
	BEGIN 
		SET @ErrorCode += 'QueueName is not defined. '
	END
	IF @ReportName is null  OR @ReportName = ''
	BEGIN 
		SET @ErrorCode += 'ReportName is not defined. '
	END
    IF @GroupName is null  OR @GroupName = ''
	BEGIN 
		SET @ErrorCode += 'GroupName is not defined. '
	END
  
    IF @ErrorCode IS NOT NULL 
	BEGIN 
		return;
	END


	/*Declare variables*/
	DECLARE @ServiceUnit as varchar(7);
	DECLARE @ServiceOrder as varchar(8);
	DECLARE @BodyTextServiceOrders as varchar(MAX) = '';

	DECLARE QueryCursor CURSOR for

	/*Assign service Unit and Service Order values*/
	SELECT DISTINCT
		t.ServiceUnit,
		t.ServiceOrder
	FROM 
		ServiceOrder.dbo.TBL_ServiceOrderTasks as t with (nolock)
		LEFT OUTER JOIN ServiceOrder.dbo.TBL_Notifications as s with (nolock) on t.TaskID = s.SourceID and s.SourceType = 'Tasks'
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderQueue as q with ( nolock) on t.QueueID = q.QueueID   
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderReports as r with ( nolock) on t.ReportID = r.ReportID  
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderGroup as g with (nolock) on t.GroupID = g.GroupID  
	WHERE 
		t.QueueID = @QueueID
		AND t.ReportID = @ReportID
		AND t.GroupID = @GroupID
		AND t.CreatedDate >= CAST(GETDATE() as date)
		AND s.NotificationID is null
		AND t.CreateEnterpriseID = 'SYSTEM'
	
	/*Open query with cusor identifier to loop through results*/
	OPEN QueryCursor
	FETCH NEXT FROM QueryCursor INTO  @ServiceUnit, @ServiceOrder
	
	WHILE @@FETCH_STATUS = 0
		BEGIN
			SELECT @BodyTextServiceOrders += '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a href="http://inhome.searshc.com/serviceorder/detail.aspx?su='+ @ServiceUnit+ '&so='+@ServiceOrder+'">'+ @ServiceUnit+@ServiceOrder+'</a> <br />' 

		FETCH NEXT FROM QueryCursor INTO  @ServiceUnit, @ServiceOrder
	END
	CLOSE QueryCursor
	DEALLOCATE QueryCursor;
	--SELECT @BodyTextServiceOrders


	DECLARE @EmailSubject as varchar(MAX)='ServicePro New Tasks - '+@QueueName+' (' + @Today + ')'
	DECLARE @HeaderText as varchar(MAX) = 'ServicePro New Tasks'
	DECLARE @BodyText as varchar(MAX) = 'The below queue has been populated with new tasks. <br /> <br /> <b>Queue</b>: '+@QueueName+' <br />  
										<b>Report</b>: '+@ReportName+' <br />  <b>Group</b>: '+@GroupName +' <br /><b>Tasks:</b><br />'
	DECLARE @RecipientList as varchar(MAX) = COALESCE(@Email,'') --+';PGALVAN@searshc.com;'


	/*Create the email body */
	SET @EmailBodyHTML = '

	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
	<html>

	<head>
				<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
	</head>

	<body, #body_style style="width:94%; padding:2%; border-style:solid; border-width:20px; border-color:#ffffff;">
				   <div style="text-align:center; min-width:95.4%; max-width:1000px; font-size:16px; color:#ffffff; border-width:15px; background-color:'+@ReportColor+'; border-color:'+@ReportColor+'; border-radius:3px; border-style:solid; font-family:century gothic, arial;">
						<b style="letter-spacing:1px; font-size:28;">'+ @HeaderText + '</b>
							<br/>
							as reported on ' + @today +
                                                
				'</div>

				<div style="border-width:1px; border-color:#ffffff; border-style:solid;">
							<div style="border-width:1px; border-color:#e8e8e8; border-style:solid;">
										<div style="width:95.4%; font-size:16px; color:#505050; border-width:15px; background-color:#ffffff; border-color:#ffffff; border-style:solid; font-family:calibri, arial;">'

										+ @BodyText + @BodyTextServiceOrders +

										'</div>
							</div>
				</div>
				<div style="width:95.4%; text-align:center; font-size:16px; color:#505050; border-width:15px; background-color:#ecf0f1; border-color:#ecf0f1; border-style:solid; font-family:calibri, arial;">

				To report a bug/defect click <a style="color:#2980b9; text-decoration:none;" href="http://inhome.searshc.com/nfd/requests/add.asp">HERE</a>, to request an enhancement or modification click <a style="color:#2980b9; text-decoration:none;" href="https://form.jotform.com/80183906278160">HERE</a>.

				</div>

	</body>
	</html>'
     
	IF @BodyTextServiceOrders <> '' AND @Email <> ''
		BEGIN                   
			EXEC msdb.dbo.sp_send_dbmail
				@profile_name = 'ServicePro',
				@recipients =  @RecipientList, --Was @Email
				--@recipients =  'PGALVAN@searshc.com',
				@blind_copy_recipients  = 'PGALVAN@searshc.com',
				@body = @EmailBodyHTML,
				@body_format = 'HTML',
				@subject = @EmailSubject;

		END

	/*Insert entry into Notification table to identify these orders as having been successfully notified*/
	/*Assign service Unit and Service Order values*/
	INSERT INTO ServiceOrder.dbo.TBL_Notifications (SourceID, SourceType, CreateDateTime, CreateEnterpriseID)
	SELECT 
		t.TaskID,
		'Tasks' as SourceType,
		@Today as CreateDateTime,
		'SYSTEM' as CreateEnterpriseID
	FROM 
		ServiceOrder.dbo.TBL_ServiceOrderTasks as t with (nolock)
		LEFT OUTER JOIN ServiceOrder.dbo.TBL_Notifications as s with (nolock) on t.TaskID = s.SourceID and s.SourceType = 'Tasks'
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderQueue as q with ( nolock) on t.QueueID = q.QueueID   
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderReports as r with ( nolock) on t.ReportID = r.ReportID  
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderGroup as g with (nolock) on t.GroupID = g.GroupID  
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderTasksLog as l with (nolock)   on t.LogID = l.LogID 
	WHERE 
		t.QueueID = @QueueID
		AND t.ReportID = @ReportID
		AND t.GroupID = @GroupID
		AND t.CreatedDate >= CAST(GETDATE() as date)
		AND s.NotificationID is null
		AND t.CreateEnterpriseID = 'SYSTEM'

END





GO
/****** Object:  StoredProcedure [dbo].[PROC_EmailTaskSystemGeneratedIteration]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



 
/*
Developed by: 
	Preston Galvanek
Create Date: 
	2018-04-19
Description:
	Iterate through queues and email systematically created tasks, created today.
Modification Date: 
	2018-06-08
Modification By:
	PGALVAN
Description:
	Entered into QEE; added executionorder and executionset.
*/

CREATE PROCEDURE [dbo].[PROC_EmailTaskSystemGeneratedIteration]
	@Frequency as varchar(4) = NULL, 
	@FrequencyModifier as int = NULL,
	@ErrorCode varchar(150) = NULL OUTPUT
AS
BEGIN 

	/*Reassign values when blank to NULL */
	IF @FrequencyModifier = 0
		SET @FrequencyModifier = NULL
	IF @Frequency = ''
		SET @Frequency = NULL	

	/*Error Handling*/
	IF @Frequency Is Null /* Return error code if @Frequency is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for Frequency, it is Required.'
		Return
	END
	IF @FrequencyModifier Is Null /* Return error code if @ExecutionGroup is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for FrequencyModifier, it is Required.'
		Return
	END

	/*Declare variables*/
	DECLARE @SQL as varchar(max)

	/*Iterate through query results*/
	DECLARE QueryCursorOuter CURSOR for
	SELECT DISTINCT 
		'
			DECLARE @ErrorCode varchar(150);
			EXEC ServiceOrder.dbo.PROC_EmailTaskSystemGenerated
				@QueueID = '+ CAST(t.QueueID as varchar)+'
				,@ReportID = '+CAST(t.ReportID as varchar)+'
				,@GroupID= '+ CAST(t.GroupID as varchar)+'
				,@ErrorCode = @ErrorCode OUTPUT;
		' as Query
	FROM
		ServiceOrder.dbo.TBL_ServiceOrderTasks as t with (nolock) 
		LEFT OUTER JOIN ServiceOrder.dbo.TBL_Notifications as n with (nolock) on t.TaskID = n.SourceID and n.SourceType = 'Tasks'
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderReports as r with (nolock) on r.ReportID = t.ReportID
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderQueue as q with (nolock) on q.QueueID = t.QueueID
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderGroup as g with (nolock) on g.GroupID = t.GroupID
	WHERE 
		q.IsEnabled = 1
		AND g.IsEnabled = 1
		AND r.IsEnabled = 1
		AND r.Frequency = @Frequency
		AND r.FrequencyModifier = @FrequencyModifier
		AND n.NotificationID is null
		AND t.CreateEnterpriseID = 'SYSTEM'
		AND t.CreatedDate >= CAST(GETDATE() as date)

	/*Open query with cusor identifier to loop through results*/
	OPEN QueryCursorOuter
	FETCH NEXT FROM QueryCursorOuter INTO  @SQL
	
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXEC (@SQL)
		FETCH NEXT FROM QueryCursorOuter INTO  @SQL
	END
	CLOSE QueryCursorOuter
	DEALLOCATE QueryCursorOuter;
END





GO
/****** Object:  StoredProcedure [dbo].[PROC_NPJMerchandiseCodeImport]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






-- ==========================================================================================
-- Author:		Aaron Cote
-- Create date: 06/02/2015
-- Description:	Imports latest NPJXTMD data from mysql server instance.
-- ==========================================================================================
CREATE PROCEDURE [dbo].[PROC_NPJMerchandiseCodeImport]

AS
	BEGIN
	
		DECLARE @querystring NVARCHAR(MAX) = NULL;
		
		IF object_id('tempdb..#TBL_NPJMerchandiseCodeTemp') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_NPJMerchandiseCodeTemp;
		END
		
		CREATE TABLE 
			#TBL_NPJMerchandiseCodeTemp
			([Merchandise] [varchar](12) NOT NULL,
			[MerchandiseDivision] [char](3) NOT NULL,
			[MerchandiseLine] [char](2) NOT NULL,
			[MerchandiseSpecialty] [varchar](10) NOT NULL,
			[MerchandiseDescription] [varchar](25) NOT NULL,
			[MerchandiseDescriptionTwo] [varchar](25) NOT NULL,
			[TSRMerchandiseDescription] [varchar](25) NOT NULL,
			[TSRMerchandiseDescriptionTwo] [varchar](25) NOT NULL,
			[MerchandiseBuiltIn] [char](1) NOT NULL,
			[UsageType] [char](1) NOT NULL,
			[LaborWarrantyLength] [int] NOT NULL,
			[LaborWarrantyLengthType] [char](1) NOT NULL,
			[PartsWarrantyLength] [int] NOT NULL,
			[PartsWarrantyLengthType] [char](1) NOT NULL,
			[ExceptionPartsWarrantyLength] [int] NOT NULL,
			[ExceptionPartsWarrantyLengthType] [char](1) NOT NULL,
			[ServiceLocation] [char](1) NOT NULL,
			[MerchandiseNewCode] [varchar](12) NOT NULL,
			[ChangeStatus] [char](1) NOT NULL,
			[MerchandiseCodeLastUpdateDate] [datetime] NOT NULL,
			[MajorBrandService] [char](1) NOT NULL,
			[MerchandisePromoteAgeLimit] [int] NOT NULL,
			[PreventativeMaintenanceInstruc] [varchar](300) NOT NULL,
			[MerchandisePriceTier] [char](1) NOT NULL,
			[MerchandisePropertyType] [char](1) NOT NULL,
			[CarryInRouting] [int] NOT NULL,
			[LastActionTimestamp] [datetime] NOT NULL);
		
		SET @querystring = 'INSERT INTO #TBL_NPJMerchandiseCodeTemp (Merchandise, MerchandiseDivision, MerchandiseLine, MerchandiseSpecialty, MerchandiseDescription, MerchandiseDescriptionTwo, TSRMerchandiseDescription, TSRMerchandiseDescriptionTwo, MerchandiseBuiltIn, UsageType, LaborWarrantyLength, LaborWarrantyLengthType, PartsWarrantyLength, PartsWarrantyLengthType, ExceptionPartsWarrantyLength, ExceptionPartsWarrantyLengthType, ServiceLocation, MerchandiseNewCode, ChangeStatus, MerchandiseCodeLastUpdateDate, MajorBrandService, MerchandisePromoteAgeLimit, PreventativeMaintenanceInstruc, MerchandisePriceTier, MerchandisePropertyType, CarryInRouting, LastActionTimestamp)'

		SET @querystring = @querystring + ' SELECT MD_SM_CODE, MD_DIV_NO_SM, MD_LINE_NO_SM, MD_MDSE_SPECIALTY, MD_SM_CODE_DESC, MD_SM_CODE_DESC2, MD_SM_CODE_TSR_D, MD_SM_CODE_TSR_D2, MD_SM_BUILT_IN_IND, MD_SM_USAGE_TYPE, MD_SM_L_WRNTY_NO, MD_SM_L_WRNTY_DMY, MD_SM_P_WRNTY_NO, MD_SM_P_WRNTY_DMY, MD_SM_XP_WRNTY_NO, MD_SM_XP_WRNTY_DMY, MD_IW_SVCE_LOCN, MD_SM_CODE_NEW, MD_CHANGE_STATUS, MD_CHANGE_TSTAMP, MDS_MAJ_BND_SVC_FL, MDS_PRO_AGE_LMT_NO, MTC_PNT_FTR_TX, MDS_PRC_TIR_CD, PPT_TYP_CD, CRY_IN_RTE_CD, LST_ACT_TS FROM OPENQUERY(TRPRHSSFOMYSQL1, '

		SET @querystring = @querystring + ' ''SELECT MD_SM_CODE, MD_DIV_NO_SM, MD_LINE_NO_SM, MD_MDSE_SPECIALTY, MD_SM_CODE_DESC, MD_SM_CODE_DESC2, MD_SM_CODE_TSR_D, MD_SM_CODE_TSR_D2, MD_SM_BUILT_IN_IND, MD_SM_USAGE_TYPE, MD_SM_L_WRNTY_NO, MD_SM_L_WRNTY_DMY, MD_SM_P_WRNTY_NO, MD_SM_P_WRNTY_DMY, MD_SM_XP_WRNTY_NO, MD_SM_XP_WRNTY_DMY, MD_IW_SVCE_LOCN, MD_SM_CODE_NEW, MD_CHANGE_STATUS, MD_CHANGE_TSTAMP, MDS_MAJ_BND_SVC_FL, MDS_PRO_AGE_LMT_NO, MTC_PNT_FTR_TX, MDS_PRC_TIR_CD, PPT_TYP_CD, CRY_IN_RTE_CD, LST_ACT_TS FROM npjxtmd WHERE LST_ACT_TS >= ''''' + LEFT(CONVERT(VARCHAR, GETDATE() - .25, 21), 19) + ''''' ORDER BY LST_ACT_TS ASC LIMIT 20000;;'''

		SET @querystring = @querystring + ')'
		
		-- Populate temp table.
		EXEC(@querystring);
		
		-- Merge into primary table
		MERGE 
			ServiceOrder.dbo.TBL_NPJMerchandiseCode AS T
		USING 
			(SELECT 
				Merchandise
				,MerchandiseDivision
				,MerchandiseLine
				,MerchandiseSpecialty
				,MerchandiseDescription
				,MerchandiseDescriptionTwo
				,TSRMerchandiseDescription
				,TSRMerchandiseDescriptionTwo
				,MerchandiseBuiltIn
				,UsageType
				,LaborWarrantyLength
				,LaborWarrantyLengthType
				,PartsWarrantyLength
				,PartsWarrantyLengthType
				,ExceptionPartsWarrantyLength
				,ExceptionPartsWarrantyLengthType
				,ServiceLocation
				,MerchandiseNewCode
				,ChangeStatus
				,MerchandiseCodeLastUpdateDate
				,MajorBrandService
				,MerchandisePromoteAgeLimit
				,PreventativeMaintenanceInstruc
				,MerchandisePriceTier
				,MerchandisePropertyType
				,CarryInRouting
				,LastActionTimestamp
			FROM 
				#TBL_NPJMerchandiseCodeTemp) AS S
		ON 
			(T.Merchandise = S.Merchandise)
		WHEN MATCHED THEN
			UPDATE 
			SET 
				T.MerchandiseDivision = S.MerchandiseDivision
				,T.MerchandiseLine = S.MerchandiseLine
				,T.MerchandiseSpecialty = S.MerchandiseSpecialty
				,T.MerchandiseDescription = S.MerchandiseDescription
				,T.MerchandiseDescriptionTwo = S.MerchandiseDescriptionTwo
				,T.TSRMerchandiseDescription = S.TSRMerchandiseDescription
				,T.TSRMerchandiseDescriptionTwo = S.TSRMerchandiseDescriptionTwo
				,T.MerchandiseBuiltIn = S.MerchandiseBuiltIn
				,T.UsageType = S.UsageType
				,T.LaborWarrantyLength = S.LaborWarrantyLength
				,T.LaborWarrantyLengthType = S.LaborWarrantyLengthType
				,T.PartsWarrantyLength = S.PartsWarrantyLength
				,T.PartsWarrantyLengthType = S.PartsWarrantyLengthType
				,T.ExceptionPartsWarrantyLength = S.ExceptionPartsWarrantyLength
				,T.ExceptionPartsWarrantyLengthType = S.ExceptionPartsWarrantyLengthType
				,T.ServiceLocation = S.ServiceLocation
				,T.MerchandiseNewCode = S.MerchandiseNewCode
				,T.ChangeStatus = S.ChangeStatus
				,T.MerchandiseCodeLastUpdateDate = S.MerchandiseCodeLastUpdateDate
				,T.MajorBrandService = S.MajorBrandService
				,T.MerchandisePromoteAgeLimit = S.MerchandisePromoteAgeLimit
				,T.PreventativeMaintenanceInstruc = S.PreventativeMaintenanceInstruc
				,T.MerchandisePriceTier = S.MerchandisePriceTier
				,T.MerchandisePropertyType = S.MerchandisePropertyType
				,T.CarryInRouting = S.CarryInRouting
				,T.LastActionTimestamp = S.LastActionTimestamp
		WHEN NOT MATCHED BY TARGET THEN
			INSERT 
				(Merchandise
				,MerchandiseDivision
				,MerchandiseLine
				,MerchandiseSpecialty
				,MerchandiseDescription
				,MerchandiseDescriptionTwo
				,TSRMerchandiseDescription
				,TSRMerchandiseDescriptionTwo
				,MerchandiseBuiltIn
				,UsageType
				,LaborWarrantyLength
				,LaborWarrantyLengthType
				,PartsWarrantyLength
				,PartsWarrantyLengthType
				,ExceptionPartsWarrantyLength
				,ExceptionPartsWarrantyLengthType
				,ServiceLocation
				,MerchandiseNewCode
				,ChangeStatus
				,MerchandiseCodeLastUpdateDate
				,MajorBrandService
				,MerchandisePromoteAgeLimit
				,PreventativeMaintenanceInstruc
				,MerchandisePriceTier
				,MerchandisePropertyType
				,CarryInRouting
				,LastActionTimestamp)
			VALUES 
				(S.Merchandise
				,S.MerchandiseDivision
				,S.MerchandiseLine
				,S.MerchandiseSpecialty
				,S.MerchandiseDescription
				,S.MerchandiseDescriptionTwo
				,S.TSRMerchandiseDescription
				,S.TSRMerchandiseDescriptionTwo
				,S.MerchandiseBuiltIn
				,S.UsageType
				,S.LaborWarrantyLength
				,S.LaborWarrantyLengthType
				,S.PartsWarrantyLength
				,S.PartsWarrantyLengthType
				,S.ExceptionPartsWarrantyLength
				,S.ExceptionPartsWarrantyLengthType
				,S.ServiceLocation
				,S.MerchandiseNewCode
				,S.ChangeStatus
				,S.MerchandiseCodeLastUpdateDate
				,S.MajorBrandService
				,S.MerchandisePromoteAgeLimit
				,S.PreventativeMaintenanceInstruc
				,S.MerchandisePriceTier
				,S.MerchandisePropertyType
				,S.CarryInRouting
				,S.LastActionTimestamp);
				
		IF object_id('tempdb..#TBL_NPJMerchandiseCodeTemp') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_NPJMerchandiseCodeTemp;
		END
END
	
	






GO
/****** Object:  StoredProcedure [dbo].[PROC_NPSServiceOrderCustomerLookupImport]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO









-- ==========================================================================================
-- Author:		Aaron Cote
-- Create date: 06/02/2015
-- Description:	Imports latest NPSXTCL data from mysql server instance.
-- ==========================================================================================
CREATE PROCEDURE [dbo].[PROC_NPSServiceOrderCustomerLookupImport]

AS
	BEGIN
	
		DECLARE @starttime DATETIME = NULL;
		DECLARE @endtime DATETIME = NULL;
		DECLARE @querystring NVARCHAR(MAX) = NULL;
		
		IF object_id('tempdb..#TBL_NPSServiceOrderCustomerLookupTemp') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_NPSServiceOrderCustomerLookupTemp;
		END
		
		CREATE TABLE 
			#TBL_NPSServiceOrderCustomerLookupTemp
			([ServiceUnit] [char](7) NOT NULL,
			[ServiceOrder] [char](8) NOT NULL,
			[Customer] [int] NOT NULL,
			[AddressLocation] [smallint] NOT NULL,
			[ItemSuffix] [int] NOT NULL,
			[CustomerType] [char](1) NOT NULL,
			[CustomerName] [varchar](50) NOT NULL,
			[CustomerStreetAddress] [varchar](50) NOT NULL,
			[CustomerCity] [varchar](12) NOT NULL,
			[CustomerState] [char](2) NOT NULL,
			[CustomerZip] [char](5) NOT NULL,
			[CustomerZipPlusFour] [char](4) NOT NULL,
			[CustomerPhone] [varchar](10) NOT NULL,
			[CustomerAlternatePhone] [varchar](10) NOT NULL,
			[Division] [char](3) NOT NULL,
			[Brand] [varchar](12) NOT NULL,
			[Model] [varchar](24) NOT NULL,
			[SerialNumber] [varchar](20) NOT NULL,
			[SearsSold] [char](1) NOT NULL,
			[SearsStore] [char](7) NOT NULL,
			[PurchaseDate] [date] NOT NULL,
			[DeliveryInstallDate] [date] NOT NULL,
			[PartWarrantyExpireDate] [date] NOT NULL,
			[LaborWarrantyExpireDate] [date] NOT NULL,
			[ExceptPartWarrantyExpireDate] [date] NOT NULL,
			[LastActionTimestamp] [datetime] NOT NULL);
		
		-- Get max time that records were pulled to determine the start time
		-- of the next pull.  Make sure it's less than the current time to ensure
		-- bad data doesn't affect the pull.
		--SET @starttime = (SELECT MAX(LastActionTimestamp) AS StartTime FROM ServiceOrder.dbo.TBL_NPSServiceOrderCustomerLookup WHERE LastActionTimestamp < GETDATE())
		
		SET @querystring = 'INSERT INTO #TBL_NPSServiceOrderCustomerLookupTemp (ServiceUnit, ServiceOrder, Customer, AddressLocation, ItemSuffix, CustomerType, CustomerName, CustomerStreetAddress, CustomerCity, CustomerState, CustomerZip, CustomerZipPlusFour, CustomerPhone, CustomerAlternatePhone, Division, Brand, Model, SerialNumber, SearsSold, SearsStore, PurchaseDate, DeliveryInstallDate, PartWarrantyExpireDate, LaborWarrantyExpireDate, ExceptPartWarrantyExpireDate, LastActionTimestamp)'

		SET @querystring = @querystring + ' SELECT SVC_UN_NO, so_no, cus_no, LOC_SUF_NO, itm_suf_no, CUS_TYP_CD, CUS_NM, CUS_AD, CUS_CTY_NM, CUS_ST_CD, zip_cd, zip_suf_cd, CUS_PHN_NO, CUS_ALT_PHN_NO, div_no, BND_NM, MDL_NO, SRL_NO, SRS_SLD_CD, SRS_STR_NO, CUS_PUR_DT, CUS_DVR_DT, PRT_WAR_EPR_DT, LAB_WAR_EPR_DT, EXP_PRT_WAR_EPR_DT, LST_ACT_TS FROM OPENQUERY(TRPRHSSFOMYSQL1, '

		SET @querystring = @querystring + ' ''SELECT SVC_UN_NO, so_no, cus_no, LOC_SUF_NO, itm_suf_no, CUS_TYP_CD, CUS_NM, CUS_AD, CUS_CTY_NM, CUS_ST_CD, zip_cd, zip_suf_cd, CUS_PHN_NO, CUS_ALT_PHN_NO, div_no, BND_NM, MDL_NO, SRL_NO, SRS_SLD_CD, SRS_STR_NO, CUS_PUR_DT, CUS_DVR_DT, PRT_WAR_EPR_DT, LAB_WAR_EPR_DT, EXP_PRT_WAR_EPR_DT, LST_ACT_TS FROM npsxtcl WHERE LST_ACT_TS >= ''''' + LEFT(CONVERT(VARCHAR, GETDATE() - .25, 21), 19) + ''''' AND LOC_SUF_NO = 1 ORDER BY LST_ACT_TS ASC LIMIT 20000;'''

		SET @querystring = @querystring + ')'
		
		-- Populate temp table.
		EXEC(@querystring);
		
		-- Clean up temp table
		UPDATE 
			#TBL_NPSServiceOrderCustomerLookupTemp
		SET
			PurchaseDate = '1900-01-01'
		WHERE
			PurchaseDate = '1111-11-11';
			
		UPDATE 
			#TBL_NPSServiceOrderCustomerLookupTemp
		SET
			DeliveryInstallDate = '1900-01-01'
		WHERE
			DeliveryInstallDate = '1111-11-11';
			
		UPDATE 
			#TBL_NPSServiceOrderCustomerLookupTemp
		SET
			PartWarrantyExpireDate = '1900-01-01'
		WHERE
			PartWarrantyExpireDate = '1111-11-11';
			
		UPDATE 
			#TBL_NPSServiceOrderCustomerLookupTemp
		SET
			LaborWarrantyExpireDate = '1900-01-01'
		WHERE
			LaborWarrantyExpireDate = '1111-11-11';
			
		UPDATE 
			#TBL_NPSServiceOrderCustomerLookupTemp
		SET
			ExceptPartWarrantyExpireDate = '1900-01-01'
		WHERE
			ExceptPartWarrantyExpireDate = '1111-11-11';
		
		-- Merge into primary table
		MERGE 
			ServiceOrder.dbo.TBL_NPSServiceOrderCustomerLookup AS T
		USING 
			(SELECT 
				ServiceUnit
				,ServiceOrder
				,Customer
				,AddressLocation
				,ItemSuffix
				,CustomerType
				,CustomerName
				,CustomerStreetAddress
				,CustomerCity
				,CustomerState
				,CustomerZip
				,CustomerZipPlusFour
				,CustomerPhone
				,CustomerAlternatePhone
				,Division
				,Brand
				,Model
				,SerialNumber
				,SearsSold
				,SearsStore
				,PurchaseDate
				,DeliveryInstallDate
				,PartWarrantyExpireDate
				,LaborWarrantyExpireDate
				,ExceptPartWarrantyExpireDate
				,LastActionTimestamp
			FROM 
				#TBL_NPSServiceOrderCustomerLookupTemp) AS S
		ON 
			(T.ServiceUnit = S.ServiceUnit
			AND
			T.ServiceOrder = S.ServiceOrder
			AND
			T.ItemSuffix = S.ItemSuffix)
		WHEN MATCHED THEN
			UPDATE 
			SET 
				T.Customer = S.Customer
				,T.AddressLocation = S.AddressLocation
				,T.CustomerType = S.CustomerType
				,T.CustomerName = S.CustomerName
				,T.CustomerStreetAddress = S.CustomerStreetAddress
				,T.CustomerCity = S.CustomerCity
				,T.CustomerState = S.CustomerState
				,T.CustomerZip = S.CustomerZip
				,T.CustomerZipPlusFour = S.CustomerZipPlusFour
				,T.CustomerPhone = S.CustomerPhone
				,T.CustomerAlternatePhone = S.CustomerAlternatePhone
				,T.Division = S.Division
				,T.Brand = S.Brand
				,T.Model = S.Model
				,T.SerialNumber = S.SerialNumber
				,T.SearsSold = S.SearsSold
				,T.SearsStore = S.SearsStore
				,T.PurchaseDate = S.PurchaseDate
				,T.DeliveryInstallDate = S.DeliveryInstallDate
				,T.PartWarrantyExpireDate = S.PartWarrantyExpireDate
				,T.LaborWarrantyExpireDate = S.LaborWarrantyExpireDate
				,T.ExceptPartWarrantyExpireDate = S.ExceptPartWarrantyExpireDate
				,T.LastActionTimestamp = S.LastActionTimestamp
		WHEN NOT MATCHED BY TARGET THEN
			INSERT 
				(ServiceUnit
				,ServiceOrder
				,Customer
				,AddressLocation
				,ItemSuffix
				,CustomerType
				,CustomerName
				,CustomerStreetAddress
				,CustomerCity
				,CustomerState
				,CustomerZip
				,CustomerZipPlusFour
				,CustomerPhone
				,CustomerAlternatePhone
				,Division
				,Brand
				,Model
				,SerialNumber
				,SearsSold
				,SearsStore
				,PurchaseDate
				,DeliveryInstallDate
				,PartWarrantyExpireDate
				,LaborWarrantyExpireDate
				,ExceptPartWarrantyExpireDate
				,LastActionTimestamp)
			VALUES 
				(S.ServiceUnit
				,S.ServiceOrder
				,S.Customer
				,S.AddressLocation
				,S.ItemSuffix
				,S.CustomerType
				,S.CustomerName
				,S.CustomerStreetAddress
				,S.CustomerCity
				,S.CustomerState
				,S.CustomerZip
				,S.CustomerZipPlusFour
				,S.CustomerPhone
				,S.CustomerAlternatePhone
				,S.Division
				,S.Brand
				,S.Model
				,S.SerialNumber
				,S.SearsSold
				,S.SearsStore
				,S.PurchaseDate
				,S.DeliveryInstallDate
				,S.PartWarrantyExpireDate
				,S.LaborWarrantyExpireDate
				,S.ExceptPartWarrantyExpireDate
				,S.LastActionTimestamp);
		
		DELETE FROM #TBL_NPSServiceOrderCustomerLookupTemp;
		
		-- End time is needed to make sure records aren't pulled ahead of time, and then records for the primary address are missed.
		--SET @endtime = (SELECT MAX(LastActionTimestamp) AS StartTime FROM ServiceOrder.dbo.TBL_NPSServiceOrderCustomerLookup WHERE LastActionTimestamp < GETDATE())
		
		-- Get customers with alternate addresses.
		-- This has to be done in 2 steps because of duplicate
		-- primary key issues.	
		SET @querystring = 'INSERT INTO #TBL_NPSServiceOrderCustomerLookupTemp (ServiceUnit, ServiceOrder, Customer, AddressLocation, ItemSuffix, CustomerType, CustomerName, CustomerStreetAddress, CustomerCity, CustomerState, CustomerZip, CustomerZipPlusFour, CustomerPhone, CustomerAlternatePhone, Division, Brand, Model, SerialNumber, SearsSold, SearsStore, PurchaseDate, DeliveryInstallDate, PartWarrantyExpireDate, LaborWarrantyExpireDate, ExceptPartWarrantyExpireDate, LastActionTimestamp)'

		SET @querystring = @querystring + ' SELECT SVC_UN_NO, so_no, cus_no, LOC_SUF_NO, itm_suf_no, CUS_TYP_CD, CUS_NM, CUS_AD, CUS_CTY_NM, CUS_ST_CD, zip_cd, zip_suf_cd, CUS_PHN_NO, CUS_ALT_PHN_NO, div_no, BND_NM, MDL_NO, SRL_NO, SRS_SLD_CD, SRS_STR_NO, CUS_PUR_DT, CUS_DVR_DT, PRT_WAR_EPR_DT, LAB_WAR_EPR_DT, EXP_PRT_WAR_EPR_DT, LST_ACT_TS FROM OPENQUERY(TRPRHSSFOMYSQL1, '

		SET @querystring = @querystring + ' ''SELECT SVC_UN_NO, so_no, cus_no, LOC_SUF_NO, itm_suf_no, CUS_TYP_CD, CUS_NM, CUS_AD, CUS_CTY_NM, CUS_ST_CD, zip_cd, zip_suf_cd, CUS_PHN_NO, CUS_ALT_PHN_NO, div_no, BND_NM, MDL_NO, SRL_NO, SRS_SLD_CD, SRS_STR_NO, CUS_PUR_DT, CUS_DVR_DT, PRT_WAR_EPR_DT, LAB_WAR_EPR_DT, EXP_PRT_WAR_EPR_DT, LST_ACT_TS FROM npsxtcl WHERE LST_ACT_TS >= ''''' + LEFT(CONVERT(VARCHAR, GETDATE() - .25, 21), 19) + ''''' AND LST_ACT_TS < ''''' + LEFT(CONVERT(VARCHAR, GETDATE() + 1, 21), 19) + ''''' AND LOC_SUF_NO <> 1 ORDER BY LST_ACT_TS ASC LIMIT 20000;'''

		SET @querystring = @querystring + ')'
		
		-- Populate temp table.
		EXEC(@querystring);
		
		-- Clean up temp table
		UPDATE 
			#TBL_NPSServiceOrderCustomerLookupTemp
		SET
			PurchaseDate = '1900-01-01'
		WHERE
			PurchaseDate = '1111-11-11';
			
		UPDATE 
			#TBL_NPSServiceOrderCustomerLookupTemp
		SET
			DeliveryInstallDate = '1900-01-01'
		WHERE
			DeliveryInstallDate = '1111-11-11';
			
		UPDATE 
			#TBL_NPSServiceOrderCustomerLookupTemp
		SET
			PartWarrantyExpireDate = '1900-01-01'
		WHERE
			PartWarrantyExpireDate = '1111-11-11';
			
		UPDATE 
			#TBL_NPSServiceOrderCustomerLookupTemp
		SET
			LaborWarrantyExpireDate = '1900-01-01'
		WHERE
			LaborWarrantyExpireDate = '1111-11-11';
			
		UPDATE 
			#TBL_NPSServiceOrderCustomerLookupTemp
		SET
			ExceptPartWarrantyExpireDate = '1900-01-01'
		WHERE
			ExceptPartWarrantyExpireDate = '1111-11-11';
		
		-- Merge into primary table
		MERGE 
			ServiceOrder.dbo.TBL_NPSServiceOrderCustomerLookup AS T
		USING 
			(SELECT 
				ServiceUnit
				,ServiceOrder
				,Customer
				,AddressLocation
				,ItemSuffix
				,CustomerType
				,CustomerName
				,CustomerStreetAddress
				,CustomerCity
				,CustomerState
				,CustomerZip
				,CustomerZipPlusFour
				,CustomerPhone
				,CustomerAlternatePhone
				,Division
				,Brand
				,Model
				,SerialNumber
				,SearsSold
				,SearsStore
				,PurchaseDate
				,DeliveryInstallDate
				,PartWarrantyExpireDate
				,LaborWarrantyExpireDate
				,ExceptPartWarrantyExpireDate
				,LastActionTimestamp
			FROM 
				#TBL_NPSServiceOrderCustomerLookupTemp) AS S
		ON 
			(T.ServiceUnit = S.ServiceUnit
			AND
			T.ServiceOrder = S.ServiceOrder
			AND
			T.ItemSuffix = S.ItemSuffix)
		WHEN MATCHED THEN
			UPDATE 
			SET 
				T.Customer = S.Customer
				,T.AddressLocation = S.AddressLocation
				,T.CustomerType = S.CustomerType
				,T.CustomerName = S.CustomerName
				,T.CustomerStreetAddress = S.CustomerStreetAddress
				,T.CustomerCity = S.CustomerCity
				,T.CustomerState = S.CustomerState
				,T.CustomerZip = S.CustomerZip
				,T.CustomerZipPlusFour = S.CustomerZipPlusFour
				,T.CustomerPhone = S.CustomerPhone
				,T.CustomerAlternatePhone = S.CustomerAlternatePhone
				,T.Division = S.Division
				,T.Brand = S.Brand
				,T.Model = S.Model
				,T.SerialNumber = S.SerialNumber
				,T.SearsSold = S.SearsSold
				,T.SearsStore = S.SearsStore
				,T.PurchaseDate = S.PurchaseDate
				,T.DeliveryInstallDate = S.DeliveryInstallDate
				,T.PartWarrantyExpireDate = S.PartWarrantyExpireDate
				,T.LaborWarrantyExpireDate = S.LaborWarrantyExpireDate
				,T.ExceptPartWarrantyExpireDate = S.ExceptPartWarrantyExpireDate
				,T.LastActionTimestamp = S.LastActionTimestamp
		WHEN NOT MATCHED BY TARGET THEN
			INSERT 
				(ServiceUnit
				,ServiceOrder
				,Customer
				,AddressLocation
				,ItemSuffix
				,CustomerType
				,CustomerName
				,CustomerStreetAddress
				,CustomerCity
				,CustomerState
				,CustomerZip
				,CustomerZipPlusFour
				,CustomerPhone
				,CustomerAlternatePhone
				,Division
				,Brand
				,Model
				,SerialNumber
				,SearsSold
				,SearsStore
				,PurchaseDate
				,DeliveryInstallDate
				,PartWarrantyExpireDate
				,LaborWarrantyExpireDate
				,ExceptPartWarrantyExpireDate
				,LastActionTimestamp)
			VALUES 
				(S.ServiceUnit
				,S.ServiceOrder
				,S.Customer
				,S.AddressLocation
				,S.ItemSuffix
				,S.CustomerType
				,S.CustomerName
				,S.CustomerStreetAddress
				,S.CustomerCity
				,S.CustomerState
				,S.CustomerZip
				,S.CustomerZipPlusFour
				,S.CustomerPhone
				,S.CustomerAlternatePhone
				,S.Division
				,S.Brand
				,S.Model
				,S.SerialNumber
				,S.SearsSold
				,S.SearsStore
				,S.PurchaseDate
				,S.DeliveryInstallDate
				,S.PartWarrantyExpireDate
				,S.LaborWarrantyExpireDate
				,S.ExceptPartWarrantyExpireDate
				,S.LastActionTimestamp);
				
		IF object_id('tempdb..#TBL_NPSServiceOrderCustomerLookupTemp') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_NPSServiceOrderCustomerLookupTemp;
		END
END
	
	









GO
/****** Object:  StoredProcedure [dbo].[PROC_NPSServiceOrderIDImport]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- ==========================================================================================
-- Author:		Aaron Cote
-- Create date: 05/20/2015
-- Description:	Imports latest NPSXTID data from mysql server instance.
-- ==========================================================================================
CREATE PROCEDURE [dbo].[PROC_NPSServiceOrderIDImport]

AS
	BEGIN
	
		DECLARE @starttime DATETIME = NULL;
		DECLARE @querystring NVARCHAR(MAX) = NULL;
		
		IF object_id('tempdb..#TBL_NPSServiceOrderIDTemp') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_NPSServiceOrderIDTemp;
		END
		
		CREATE TABLE 
			#TBL_NPSServiceOrderIDTemp
			(ServiceUnit [char](7) NOT NULL,
			ServiceOrder [char](8) NOT NULL,
			RepairTagBarCode [varchar](7) NOT NULL,
			ShopRetailUnit [char](7) NOT NULL,
			HomeServicesCustomer [int] NOT NULL,
			AddressLocation [int] NOT NULL,
			ItemSuffix [int] NOT NULL,
			SystemItemSuffix [int] NOT NULL,
			AgreementSuffix [int] NOT NULL,
			StoreStockItem [varchar](10) NOT NULL,
			MerchandiseStorageBinLoc [varchar](10) NOT NULL,
			ServiceType [char](1) NOT NULL,
			IndustryCode [char](2) NOT NULL,
			MerchandiseCode [varchar](12) NOT NULL,
			BranchUnit [char](7) NOT NULL,
			LocalNationalPromotion [char](1) NOT NULL,
			TierPricingSchedule [char](2) NOT NULL,
			ComplaintTaken [char](1) NOT NULL,
			ServicingOrganizationGroup [char](1) NOT NULL,
			Origination [varchar](4) NOT NULL,
			OriginalThirdPartyID [char](6) NOT NULL,
			FirstAvailableDate [varchar](10) NOT NULL,
			PreventiveMaintenanceCheck [char](1) NOT NULL,
			LastActionTimestamp [datetime] NOT NULL);
		
		-- Get max time that records were pulled to determine the start time
		-- of the next pull.  Make sure it's less than the current time to ensure
		-- bad data doesn't affect the pull.
		--SET @starttime = (SELECT MAX(LastActionTimestamp) AS StartTime FROM ServiceOrder.dbo.TBL_NPSServiceOrderID WHERE LastActionTimestamp < GETDATE())
		
		SET @querystring = 'INSERT INTO #TBL_NPSServiceOrderIDTemp (ServiceUnit, ServiceOrder, RepairTagBarCode, ShopRetailUnit, HomeServicesCustomer, AddressLocation, ItemSuffix, SystemItemSuffix, AgreementSuffix, StoreStockItem, MerchandiseStorageBinLoc, ServiceType, IndustryCode, MerchandiseCode, BranchUnit, LocalNationalPromotion, TierPricingSchedule, ComplaintTaken, ServicingOrganizationGroup, Origination, OriginalThirdPartyID, FirstAvailableDate, PreventiveMaintenanceCheck, LastActionTimestamp)'

		SET @querystring = @querystring + ' SELECT SVC_UN_NO, SO_NO, RPR_TAG_BCD_NO, SVC_RTL_UN_NO, SVC_CUS_ID_NO, LOC_SUF_NO, ITM_SUF_NO, SYS_ITM_SUF_NO, AGR_SUF_NO, SMG_STR_STK_NO, SVC_MDS_BIN_LOC_CD, SVC_TYP_CD, IDY_CD, MDS_CD, BR_SVC_UN_NO, LCL_NTL_PRO_CD, TIR_PRC_SCH_NO, INQ_TAK_FL, SVC_OGP_CD, ORI_CD, ORI_THD_PTY_ID, FST_AVL_DT, PM_CHK_CD, LST_ACT_TS FROM OPENQUERY(TRPRHSSFOMYSQL1, '

		SET @querystring = @querystring + ' ''SELECT SVC_UN_NO, SO_NO, RPR_TAG_BCD_NO, SVC_RTL_UN_NO, SVC_CUS_ID_NO, LOC_SUF_NO, ITM_SUF_NO, SYS_ITM_SUF_NO, AGR_SUF_NO, SMG_STR_STK_NO, SVC_MDS_BIN_LOC_CD, SVC_TYP_CD, IDY_CD, MDS_CD, BR_SVC_UN_NO, LCL_NTL_PRO_CD, TIR_PRC_SCH_NO, INQ_TAK_FL, SVC_OGP_CD, ORI_CD, ORI_THD_PTY_ID, DATE_FORMAT(FST_AVL_DT,''''%Y-%m-%d'''') FST_AVL_DT, PM_CHK_CD, LST_ACT_CD, LST_ACT_TS FROM npsxtid WHERE LST_ACT_TS >= ''''' + LEFT(CONVERT(VARCHAR, GETDATE() - .25, 21), 19) + ''''' ORDER BY LST_ACT_TS ASC LIMIT 20000;'''

		SET @querystring = @querystring + ')'
		
		-- Populate temp table.
		EXEC(@querystring);
		
		-- Clean up temp table
		UPDATE 
			#TBL_NPSServiceOrderIDTemp
		SET
			FirstAvailableDate = '1900-01-01'
		WHERE
			FirstAvailableDate = '1111-11-11';
		
		-- Merge into primary table
		MERGE 
			ServiceOrder.dbo.TBL_NPSServiceOrderID AS T
		USING 
			(SELECT 
				ServiceUnit,
				ServiceOrder,
				RepairTagBarCode,
				ShopRetailUnit,
				HomeServicesCustomer,
				AddressLocation,
				ItemSuffix,
				SystemItemSuffix,
				AgreementSuffix,
				StoreStockItem,
				MerchandiseStorageBinLoc,
				ServiceType,
				IndustryCode,
				MerchandiseCode,
				BranchUnit,
				LocalNationalPromotion,
				TierPricingSchedule,
				ComplaintTaken,
				ServicingOrganizationGroup,
				Origination,
				OriginalThirdPartyID,
				FirstAvailableDate,
				PreventiveMaintenanceCheck,
				LastActionTimestamp
			FROM 
				#TBL_NPSServiceOrderIDTemp) AS S
		ON 
			(T.ServiceUnit = S.ServiceUnit
			AND
			T.ServiceOrder = S.ServiceOrder)
		WHEN MATCHED THEN
			UPDATE 
			SET 
				T.RepairTagBarCode = S.RepairTagBarCode,
				T.ShopRetailUnit = S.ShopRetailUnit,
				T.HomeServicesCustomer = S.HomeServicesCustomer,
				T.AddressLocation = S.AddressLocation,
				T.ItemSuffix = S.ItemSuffix,
				T.SystemItemSuffix = S.SystemItemSuffix,
				T.AgreementSuffix = S.AgreementSuffix,
				T.StoreStockItem = S.StoreStockItem,
				T.MerchandiseStorageBinLoc = S.MerchandiseStorageBinLoc,
				T.ServiceType = S.ServiceType,
				T.IndustryCode = S.IndustryCode,
				T.MerchandiseCode = S.MerchandiseCode,
				T.BranchUnit = S.BranchUnit,
				T.LocalNationalPromotion = S.LocalNationalPromotion,
				T.TierPricingSchedule = S.TierPricingSchedule,
				T.ComplaintTaken = S.ComplaintTaken,
				T.ServicingOrganizationGroup = S.ServicingOrganizationGroup,
				T.Origination = S.Origination,
				T.OriginalThirdPartyID = S.OriginalThirdPartyID,
				T.FirstAvailableDate = S.FirstAvailableDate,
				T.PreventiveMaintenanceCheck = S.PreventiveMaintenanceCheck,
				T.LastActionTimestamp = S.LastActionTimestamp
		WHEN NOT MATCHED BY TARGET THEN
			INSERT 
				(ServiceUnit,
				ServiceOrder,
				RepairTagBarCode,
				ShopRetailUnit,
				HomeServicesCustomer,
				AddressLocation,
				ItemSuffix,
				SystemItemSuffix,
				AgreementSuffix,
				StoreStockItem,
				MerchandiseStorageBinLoc,
				ServiceType,
				IndustryCode,
				MerchandiseCode,
				BranchUnit,
				LocalNationalPromotion,
				TierPricingSchedule,
				ComplaintTaken,
				ServicingOrganizationGroup,
				Origination,
				OriginalThirdPartyID,
				FirstAvailableDate,
				PreventiveMaintenanceCheck,
				LastActionTimestamp)
			VALUES 
				(S.ServiceUnit,
				S.ServiceOrder,
				S.RepairTagBarCode,
				S.ShopRetailUnit,
				S.HomeServicesCustomer,
				S.AddressLocation,
				S.ItemSuffix,
				S.SystemItemSuffix,
				S.AgreementSuffix,
				S.StoreStockItem,
				S.MerchandiseStorageBinLoc,
				S.ServiceType,
				S.IndustryCode,
				S.MerchandiseCode,
				S.BranchUnit,
				S.LocalNationalPromotion,
				S.TierPricingSchedule,
				S.ComplaintTaken,
				S.ServicingOrganizationGroup,
				S.Origination,
				S.OriginalThirdPartyID,
				S.FirstAvailableDate,
				S.PreventiveMaintenanceCheck,
				S.LastActionTimestamp);
				
		IF object_id('tempdb..#TBL_NPSServiceOrderIDTemp') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_NPSServiceOrderIDTemp;
		END
END
	
	





GO
/****** Object:  StoredProcedure [dbo].[PROC_NPSServiceOrderServiceRequestImport]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






-- ==========================================================================================
-- Author:		Aaron Cote
-- Create date: 05/29/2015
-- Description:	Imports latest NPSXTRS data from mysql server instance.
-- ==========================================================================================
CREATE PROCEDURE [dbo].[PROC_NPSServiceOrderServiceRequestImport]

AS
	BEGIN
	
		DECLARE @starttime DATETIME = NULL;
		DECLARE @querystring NVARCHAR(MAX) = NULL;
		
		IF object_id('tempdb..#TBL_NPSServiceOrderServiceRequestTemp') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_NPSServiceOrderServiceRequestTemp;
		END
		
		CREATE TABLE 
			#TBL_NPSServiceOrderServiceRequestTemp
			(ServiceUnit [char](7) NOT NULL,
			ServiceOrder [char](8) NOT NULL,
			WorkArea [varchar](3) NOT NULL,
			ServiceOrderPaymentMethod [varchar](2) NOT NULL,
			CustomerChargeAccount [varchar](16) NOT NULL,
			CustomerChargeAccountExp [date] NOT NULL,
			RepairCoverageType [char](1) NOT NULL,
			RepairCoverageFlatRateAmount [decimal](9,2) NOT NULL,
			ServiceScheduleDate [date] NOT NULL,
			ServiceScheduleFromTime [char](8) NOT NULL,
			ServiceScheduleToTime [char](8) NOT NULL,
			ServiceOrigScheduleDate [date] NOT NULL,
			ServiceOrigScheduleFromTime [char](8) NOT NULL,
			ServiceOrigScheduleToTime [char](8) NOT NULL,
			ServiceWaitToAverage [decimal](2,1) NOT NULL,
			ServiceOrderCreateDate [date] NOT NULL,
			ServiceOrderCreateTime [char](8) NOT NULL,
			ServiceOrderCreateUnit [char](7) NOT NULL,
			ServiceOrderCreateEmployee [char](7) NOT NULL,
			ServiceOrderCostLimit [decimal](7,2) NOT NULL,
			ServiceRequestDescription [varchar](66) NOT NULL,
			SpecialInstructionsDescOne [varchar](60) NOT NULL,
			SpecialInstructionsDescTwo [varchar](60) NOT NULL,
			ContactName [varchar](30) NOT NULL,
			ContactPhoneNumber [varchar](10) NOT NULL,
			EstimateRequestCode [char](1) NOT NULL,
			EstimatedMinimumAmount [decimal](7,2) NOT NULL,
			EstimatedMaximumAmount [decimal](7,2) NOT NULL,
			StatusCode [char](2) NOT NULL,
			ServiceOrderType [varchar](4) NOT NULL,
			ServiceOrderPriority [char](1) NOT NULL,
			HelperNeeded [char](1) NOT NULL,
			ServiceOrderChargePerCall [decimal](9,2) NOT NULL,
			LastModifiedDate [datetime] NOT NULL,
			ModificationID [varchar](7) NOT NULL,
			ServiceProductSellingPrice [decimal](9,2) NOT NULL,
			ServiceOrderTotal [decimal](9,2) NOT NULL,
			EstimatedApprovalDate [date] NOT NULL,
			QuoteCallChargeAmount [decimal](9,2) NOT NULL,
			CallLoadType [int] NOT NULL,
			MerchandiseSpecialty [varchar](10) NOT NULL,
			TechnicianSpecialPriceAmount [decimal](7,2) NOT NULL,
			ServiceOrderCloseDate [date] NOT NULL,
			LastActionTimestamp [datetime] NOT NULL);
		
		-- Get max time that records were pulled to determine the start time
		-- of the next pull.  Make sure it's less than the current time to ensure
		-- bad data doesn't affect the pull.
		SET @starttime = (SELECT MAX(LastActionTimestamp) AS StartTime FROM ServiceOrder.dbo.TBL_NPSServiceOrderServiceRequest WHERE LastActionTimestamp < GETDATE())
		
		SET @querystring = 'INSERT INTO #TBL_NPSServiceOrderServiceRequestTemp (ServiceUnit, ServiceOrder, WorkArea, ServiceOrderPaymentMethod, CustomerChargeAccount, CustomerChargeAccountExp, RepairCoverageType, RepairCoverageFlatRateAmount, ServiceScheduleDate, ServiceScheduleFromTime, ServiceScheduleToTime, ServiceOrigScheduleDate, ServiceOrigScheduleFromTime, ServiceOrigScheduleToTime, ServiceWaitToAverage, ServiceOrderCreateDate, ServiceOrderCreateTime, ServiceOrderCreateUnit, ServiceOrderCreateEmployee, ServiceOrderCostLimit, ServiceRequestDescription, SpecialInstructionsDescOne, SpecialInstructionsDescTwo, ContactName, ContactPhoneNumber, EstimateRequestCode, EstimatedMinimumAmount, EstimatedMaximumAmount, StatusCode, ServiceOrderType, ServiceOrderPriority, HelperNeeded, ServiceOrderChargePerCall, LastModifiedDate, ModificationID, ServiceProductSellingPrice, ServiceOrderTotal, EstimatedApprovalDate, QuoteCallChargeAmount, CallLoadType, MerchandiseSpecialty, TechnicianSpecialPriceAmount, ServiceOrderCloseDate, LastActionTimestamp)'

		SET @querystring = @querystring + ' SELECT SVC_UN_NO,SO_NO,WRK_ARE_CD,SO_PY_MET_CD,cus_crg_acn_no,cus_crg_acn_epr_dt,rpr_crg_typ_cd,rpr_crg_flt_rt_am,SVC_SCH_DT,SVC_SCH_FRM_TM,SVC_SCH_TO_TM,SVC_ORI_SCH_DT,SVC_ORI_SCH_FRM_TM,SVC_ORI_SCH_TO_TM,svc_wt_to_avg_no,SO_CRT_DT,SO_CRT_TM,SO_CRT_UN_NO,SO_CRT_EMP_NO,SO_CUS_MAX_AM,SVC_RQ_DS,SO_INS_1_DS,SO_INS_2_DS,CNT_NM,CNT_PHN_NO,svc_est_rq_cd,SVC_EST_MIN_AM,SVC_EST_MAX_AM,SO_STS_CD,SO_TYP_CD,SO_PRI_CD,SO_HPR_NED_FL,SO_CRG_PER_CAL_AM,MOD_EXT_DT,MOD_ID,SPD_SLL_PRC_AM,SO_TOT_AM,EST_APV_DT,SO_QTE_CRG_AM,CAL_LD_TYP_CD,MDS_SP_CD,TEC_SP_PRC_AM,SVC_ORD_CLO_DT,LST_ACT_TS FROM OPENQUERY(TRPRHSSFOMYSQL1, '

		SET @querystring = @querystring + ' ''SELECT SVC_UN_NO,SO_NO,WRK_ARE_CD,SO_PY_MET_CD,cus_crg_acn_no,DATE_FORMAT(cus_crg_acn_epr_dt,''''%Y-%m-%d'''') cus_crg_acn_epr_dt,rpr_crg_typ_cd,rpr_crg_flt_rt_am,DATE_FORMAT(SVC_SCH_DT,''''%Y-%m-%d'''') SVC_SCH_DT,SVC_SCH_FRM_TM,SVC_SCH_TO_TM,DATE_FORMAT(SVC_ORI_SCH_DT,''''%Y-%m-%d'''') SVC_ORI_SCH_DT,SVC_ORI_SCH_FRM_TM,SVC_ORI_SCH_TO_TM,svc_wt_to_avg_no,DATE_FORMAT(SO_CRT_DT,''''%Y-%m-%d'''') SO_CRT_DT,SO_CRT_TM,SO_CRT_UN_NO,SO_CRT_EMP_NO,SO_CUS_MAX_AM,SVC_RQ_DS,SO_INS_1_DS,SO_INS_2_DS,CNT_NM,CNT_PHN_NO,svc_est_rq_cd,SVC_EST_MIN_AM,SVC_EST_MAX_AM,SO_STS_CD,SO_TYP_CD,SO_PRI_CD,SO_HPR_NED_FL,SO_CRG_PER_CAL_AM,MOD_EXT_DT,MOD_ID,SPD_SLL_PRC_AM,SO_TOT_AM,DATE_FORMAT(EST_APV_DT,''''%Y-%m-%d'''') EST_APV_DT,SO_QTE_CRG_AM,CAL_LD_TYP_CD,MDS_SP_CD,TEC_SP_PRC_AM,DATE_FORMAT(SVC_ORD_CLO_DT,''''%Y-%m-%d'''') SVC_ORD_CLO_DT,LST_ACT_TS FROM svcflxdb.npsxtsr WHERE LST_ACT_TS >= ''''' + LEFT(CONVERT(VARCHAR, @starttime, 21), 19) + ''''' ORDER BY LST_ACT_TS ASC LIMIT 20000;'''

		SET @querystring = @querystring + ')'
		
		-- Populate temp table.
		EXEC(@querystring);
		
		-- Clean up temp table
		UPDATE 
			#TBL_NPSServiceOrderServiceRequestTemp
		SET
			CustomerChargeAccountExp = '1900-01-01'
		WHERE
			CustomerChargeAccountExp = '1111-11-11';
			
		UPDATE 
			#TBL_NPSServiceOrderServiceRequestTemp
		SET
			ServiceScheduleDate = '1900-01-01'
		WHERE
			ServiceScheduleDate = '1111-11-11';
			
		UPDATE 
			#TBL_NPSServiceOrderServiceRequestTemp
		SET
			ServiceOrigScheduleDate = '1900-01-01'
		WHERE
			ServiceOrigScheduleDate = '1111-11-11';
			
		UPDATE 
			#TBL_NPSServiceOrderServiceRequestTemp
		SET
			ServiceOrderCreateDate = '1900-01-01'
		WHERE
			ServiceOrderCreateDate = '1111-11-11';
		
		UPDATE 
			#TBL_NPSServiceOrderServiceRequestTemp
		SET
			EstimatedApprovalDate = '1900-01-01'
		WHERE
			EstimatedApprovalDate = '1111-11-11';
			
		UPDATE 
			#TBL_NPSServiceOrderServiceRequestTemp
		SET
			ServiceOrderCloseDate = '1900-01-01'
		WHERE
			ServiceOrderCloseDate = '1111-11-11';
		
		-- Merge into primary table
		MERGE 
			ServiceOrder.dbo.TBL_NPSServiceOrderServiceRequest AS T
		USING 
			(SELECT 
				ServiceUnit
				,ServiceOrder
				,WorkArea
				,ServiceOrderPaymentMethod
				,CustomerChargeAccount
				,CustomerChargeAccountExp
				,RepairCoverageType
				,RepairCoverageFlatRateAmount
				,ServiceScheduleDate
				,ServiceScheduleFromTime
				,ServiceScheduleToTime
				,ServiceOrigScheduleDate
				,ServiceOrigScheduleFromTime
				,ServiceOrigScheduleToTime
				,ServiceWaitToAverage
				,ServiceOrderCreateDate
				,ServiceOrderCreateTime
				,ServiceOrderCreateUnit
				,ServiceOrderCreateEmployee
				,ServiceOrderCostLimit
				,ServiceRequestDescription
				,SpecialInstructionsDescOne
				,SpecialInstructionsDescTwo
				,ContactName
				,ContactPhoneNumber
				,EstimateRequestCode
				,EstimatedMinimumAmount
				,EstimatedMaximumAmount
				,StatusCode
				,ServiceOrderType
				,ServiceOrderPriority
				,HelperNeeded
				,ServiceOrderChargePerCall
				,LastModifiedDate
				,ModificationID
				,ServiceProductSellingPrice
				,ServiceOrderTotal
				,EstimatedApprovalDate
				,QuoteCallChargeAmount
				,CallLoadType
				,MerchandiseSpecialty
				,TechnicianSpecialPriceAmount
				,ServiceOrderCloseDate
				,LastActionTimestamp
			FROM 
				#TBL_NPSServiceOrderServiceRequestTemp) AS S
		ON 
			(T.ServiceUnit = S.ServiceUnit
			AND
			T.ServiceOrder = S.ServiceOrder)
		WHEN MATCHED THEN
			UPDATE 
			SET 
				T.WorkArea = S.WorkArea
				,T.ServiceOrderPaymentMethod = S.ServiceOrderPaymentMethod
				,T.CustomerChargeAccount = S.CustomerChargeAccount
				,T.CustomerChargeAccountExp = S.CustomerChargeAccountExp
				,T.RepairCoverageType = S.RepairCoverageType
				,T.RepairCoverageFlatRateAmount = S.RepairCoverageFlatRateAmount
				,T.ServiceScheduleDate = S.ServiceScheduleDate
				,T.ServiceScheduleFromTime = S.ServiceScheduleFromTime
				,T.ServiceScheduleToTime = S.ServiceScheduleToTime
				,T.ServiceOrigScheduleDate = S.ServiceOrigScheduleDate
				,T.ServiceOrigScheduleFromTime = S.ServiceOrigScheduleFromTime
				,T.ServiceOrigScheduleToTime = S.ServiceOrigScheduleToTime
				,T.ServiceWaitToAverage = S.ServiceWaitToAverage
				,T.ServiceOrderCreateDate = S.ServiceOrderCreateDate
				,T.ServiceOrderCreateTime = S.ServiceOrderCreateTime
				,T.ServiceOrderCreateUnit = S.ServiceOrderCreateUnit
				,T.ServiceOrderCreateEmployee = S.ServiceOrderCreateEmployee
				,T.ServiceOrderCostLimit = S.ServiceOrderCostLimit
				,T.ServiceRequestDescription = S.ServiceRequestDescription
				,T.SpecialInstructionsDescOne = S.SpecialInstructionsDescOne
				,T.SpecialInstructionsDescTwo = S.SpecialInstructionsDescTwo
				,T.ContactName = S.ContactName
				,T.ContactPhoneNumber = S.ContactPhoneNumber
				,T.EstimateRequestCode = S.EstimateRequestCode
				,T.EstimatedMinimumAmount = S.EstimatedMinimumAmount
				,T.EstimatedMaximumAmount = S.EstimatedMaximumAmount
				,T.StatusCode = S.StatusCode
				,T.ServiceOrderType = S.ServiceOrderType
				,T.ServiceOrderPriority = S.ServiceOrderPriority
				,T.HelperNeeded = S.HelperNeeded
				,T.ServiceOrderChargePerCall = S.ServiceOrderChargePerCall
				,T.LastModifiedDate = S.LastModifiedDate
				,T.ModificationID = S.ModificationID
				,T.ServiceProductSellingPrice = S.ServiceProductSellingPrice
				,T.ServiceOrderTotal = S.ServiceOrderTotal
				,T.EstimatedApprovalDate = S.EstimatedApprovalDate
				,T.QuoteCallChargeAmount = S.QuoteCallChargeAmount
				,T.CallLoadType = S.CallLoadType
				,T.MerchandiseSpecialty = S.MerchandiseSpecialty
				,T.TechnicianSpecialPriceAmount = S.TechnicianSpecialPriceAmount
				,T.ServiceOrderCloseDate = S.ServiceOrderCloseDate
				,T.LastActionTimestamp = S.LastActionTimestamp
		WHEN NOT MATCHED BY TARGET THEN
			INSERT 
				(ServiceUnit
				,ServiceOrder
				,WorkArea
				,ServiceOrderPaymentMethod
				,CustomerChargeAccount
				,CustomerChargeAccountExp
				,RepairCoverageType
				,RepairCoverageFlatRateAmount
				,ServiceScheduleDate
				,ServiceScheduleFromTime
				,ServiceScheduleToTime
				,ServiceOrigScheduleDate
				,ServiceOrigScheduleFromTime
				,ServiceOrigScheduleToTime
				,ServiceWaitToAverage
				,ServiceOrderCreateDate
				,ServiceOrderCreateTime
				,ServiceOrderCreateUnit
				,ServiceOrderCreateEmployee
				,ServiceOrderCostLimit
				,ServiceRequestDescription
				,SpecialInstructionsDescOne
				,SpecialInstructionsDescTwo
				,ContactName
				,ContactPhoneNumber
				,EstimateRequestCode
				,EstimatedMinimumAmount
				,EstimatedMaximumAmount
				,StatusCode
				,ServiceOrderType
				,ServiceOrderPriority
				,HelperNeeded
				,ServiceOrderChargePerCall
				,LastModifiedDate
				,ModificationID
				,ServiceProductSellingPrice
				,ServiceOrderTotal
				,EstimatedApprovalDate
				,QuoteCallChargeAmount
				,CallLoadType
				,MerchandiseSpecialty
				,TechnicianSpecialPriceAmount
				,ServiceOrderCloseDate
				,LastActionTimestamp)
			VALUES 
				(S.ServiceUnit
				,S.ServiceOrder
				,S.WorkArea
				,S.ServiceOrderPaymentMethod
				,S.CustomerChargeAccount
				,S.CustomerChargeAccountExp
				,S.RepairCoverageType
				,S.RepairCoverageFlatRateAmount
				,S.ServiceScheduleDate
				,S.ServiceScheduleFromTime
				,S.ServiceScheduleToTime
				,S.ServiceOrigScheduleDate
				,S.ServiceOrigScheduleFromTime
				,S.ServiceOrigScheduleToTime
				,S.ServiceWaitToAverage
				,S.ServiceOrderCreateDate
				,S.ServiceOrderCreateTime
				,S.ServiceOrderCreateUnit
				,S.ServiceOrderCreateEmployee
				,S.ServiceOrderCostLimit
				,S.ServiceRequestDescription
				,S.SpecialInstructionsDescOne
				,S.SpecialInstructionsDescTwo
				,S.ContactName
				,S.ContactPhoneNumber
				,S.EstimateRequestCode
				,S.EstimatedMinimumAmount
				,S.EstimatedMaximumAmount
				,S.StatusCode
				,S.ServiceOrderType
				,S.ServiceOrderPriority
				,S.HelperNeeded
				,S.ServiceOrderChargePerCall
				,S.LastModifiedDate
				,S.ModificationID
				,S.ServiceProductSellingPrice
				,S.ServiceOrderTotal
				,S.EstimatedApprovalDate
				,S.QuoteCallChargeAmount
				,S.CallLoadType
				,S.MerchandiseSpecialty
				,S.TechnicianSpecialPriceAmount
				,S.ServiceOrderCloseDate
				,S.LastActionTimestamp);
				
		IF object_id('tempdb..#TBL_NPSServiceOrderServiceRequestTemp') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_NPSServiceOrderServiceRequestTemp;
		END
END
	
	






GO
/****** Object:  StoredProcedure [dbo].[PROC_ReportsCountUpdate]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


 
/*
Developed by: 
	Preston Galvanek
Create Date: 
	2016-06-22
Description:
	Populate ServicePro with updated report count for all reports
Modification Date:
	2018-06-08
Modified By:
	PGALVAN
Modification:
	Entered into QEE; added executionorder and executionset.
*/

CREATE PROCEDURE [dbo].[PROC_ReportsCountUpdate]
	@Frequency as varchar(4) = NULL, 
	@FrequencyModifier as int = NULL,
	@ErrorCode varchar(150) = NULL OUTPUT
AS
BEGIN 

	/*Reassign values when blank to NULL */
	IF @FrequencyModifier = 0
		SET @FrequencyModifier = NULL
	IF @Frequency = ''
		SET @Frequency = NULL	

	/*Error Handling*/
	IF @Frequency Is Null /* Return error code if @Frequency is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for Frequency, it is Required.'
		Return
	END
	IF @FrequencyModifier Is Null /* Return error code if @ExecutionGroup is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for FrequencyModifier, it is Required.'
		Return
	END
	
	/*Declare variables*/
	DECLARE @SQL as varchar(max)

	/*Iterate through query results*/
	DECLARE QueryCursor CURSOR for
	SELECT
			'UPDATE 
				ServiceOrder.dbo.TBL_ServiceOrderReports
			SET 
				ReportCount = ( SELECT COUNT(*) as Records FROM (
				SELECT ' + CAST(r.QuerySelect as varchar(max)) + 
				' FROM  ' + CAST(r.QueryFrom as varchar(max)) + 
				CASE 
					WHEN r.QueryWhere <> '' 
					THEN ' WHERE  ' + CAST(r.QueryWhere as varchar(max)) 
					ELSE '' 
				END +
				CASE 
					WHEN r.QueryGroupBy <> '' 
					THEN ' GROUP BY ' + CAST(r.QueryGroupBy as varchar(max))
					ELSE '' 
				END
				 + ') as cr ),
				LastUpdateDateTime = GETDATE()
			WHERE
				ReportID = ' + CAST(r.ReportID as varchar) + ' 
			
				' as QueryToExecute
		FROM 
			ServiceOrder.dbo.TBL_ServiceOrderReports as r with (nolock)
		WHERE
			r.ReportID <> -1
			AND r.IsEnabled = 1
			AND r.Frequency = @Frequency
			AND r.FrequencyModifier = @FrequencyModifier

	/*Open query with cusor identifier to loop through results*/
	OPEN QueryCursor
	FETCH NEXT FROM QueryCursor INTO  @SQL
	
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXEC (@SQL)
		FETCH NEXT FROM QueryCursor INTO  @SQL
	END
	CLOSE QueryCursor
	DEALLOCATE QueryCursor;
END




GO
/****** Object:  StoredProcedure [dbo].[PROC_ReportsEmailIteration]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


 
/*
Developed by: 
	Preston Galvanek
Create Date: 
	2016-05-11
Description:
	Iterate through reports and email them to creation user.
Modification Date:
	2018-06-08
Modified By:
	PGALVAN
Modification:
	Entered into QEE; added executionorder and executionset.
*/

CREATE PROCEDURE [dbo].[PROC_ReportsEmailIteration]
	@Frequency as varchar(4) = NULL,
	@FrequencyModifier as int = NULL,
	@EmailFrequency as varchar(4) = NULL, /*currently only DD*/
	@ErrorCode varchar(150) = NULL OUTPUT
AS
BEGIN 

	/*Reassign values when blank to NULL */
	IF @FrequencyModifier = 0
		SET @FrequencyModifier = NULL
	IF @Frequency = ''
		SET @Frequency = NULL	

	/*Error Handling*/
	IF @Frequency Is Null /* Return error code if @Frequency is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for Frequency, it is Required.'
		Return
	END
	IF @EmailFrequency Is Null /* Return error code if @EmailFrequency is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for EmailFrequency, it is Required.'
		Return
	END
	IF @FrequencyModifier Is Null /* Return error code if @ExecutionGroup is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for FrequencyModifier, it is Required.'
		Return
	END
	
	/*Declare variables*/
	DECLARE @SQL as varchar(max)

	/*Iterate through query results*/
	DECLARE QueryCursor CURSOR for
	SELECT DISTINCT
		'
			EXEC ServiceOrder.dbo.PROC_ServiceProEmail 
				@ReportID = '+CAST(r.ReportID as varchar)+'
				,@QueueID = '+ CAST(a.QueueID as varchar)+'
				,@GroupID= '+ CAST(a.GroupID as varchar)+'
				,@Frequency = '''+ CAST(s.Frequency as varchar)+'''
		' as Query
	FROM
		ServiceOrder.dbo.TBL_ServiceOrderAssignment as a with (nolock) 
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderReports as r with (nolock) on r.ReportID = a.ReportID
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderQueue as q with (nolock) on q.QueueID = a.QueueID
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderGroup as g with (nolock) on g.GroupID = a.GroupID
		INNER JOIN ServiceOrder.dbo.TBL_Schedule as s with (nolock) on s.OwnerID = a.AssignmentID 
	WHERE 

		s.OwnerType = 'AssignmentID'
		AND s.Status = 1
		AND s.Frequency = @EmailFrequency
		AND r.Frequency = @Frequency
		AND r.FrequencyModifier = @FrequencyModifier
		AND q.IsEnabled = 1
		AND g.IsEnabled = 1
		AND r.IsEnabled = 1
		AND (
			s.LastExecutionDate < CAST(Getdate() as date)
			OR s.LastExecutionDate is null
		)

	/*Open query with cusor identifier to loop through results*/
	OPEN QueryCursor
	FETCH NEXT FROM QueryCursor INTO  @SQL
	
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXEC (@SQL)
		FETCH NEXT FROM QueryCursor INTO  @SQL
	END
	CLOSE QueryCursor
	DEALLOCATE QueryCursor;
END




GO
/****** Object:  StoredProcedure [dbo].[PROC_ServiceOrderClickRoutingUpdate]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



/*
	Developed By: PGALVAN
	Created: 2018-06-08
	Description: Moved into Proc from QEE
*/

CREATE PROCEDURE [dbo].[PROC_ServiceOrderClickRoutingUpdate]
	@ErrorCode varchar(150) = NULL OUTPUT
AS
BEGIN 
	IF object_id('tempdb..#TBL_ClickRoutingScheduleTemp') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_ClickRoutingScheduleTemp;
		END
		
	CREATE TABLE 
		#TBL_ClickRoutingScheduleTemp
		([TaskKey] [bigint] NULL,
		[AssignmentEventKey] [int] NULL,
		[ExternalReferenceID] [varchar](64) NULL,
		[CallID] [varchar](64) NOT NULL,
		[CallNumber] [int] NOT NULL,
		[TaskStatusCode] [int] NULL,
		[TaskStatus] [varchar](50) NULL,
		[AttemptType] [varchar](50) NULL,
		[TaskEmployeeID] [varchar](20) NULL,
		[AssignEmployeeID] [varchar](20) NULL,
		[TaskType] [varchar](25) NULL,
		[ServiceUnit] [char](7) NULL,
		[ServiceOrder] [char](8) NULL,
		[VisitNumber] [int] NULL,
		[ScheduleDate] [date] NULL,
		[CustomerRequestedStartDate] [datetimeoffset](0) NULL,
		[CustomerRequestedEndDate] [datetimeoffset](0) NULL,
		[ScheduledStartDate] [datetimeoffset](0) NULL,
		[ScheduledEndDate] [datetimeoffset](0) NULL,
		[ActualStartDate] [datetimeoffset](0) NULL,
		[ActualEndDate] [datetimeoffset](0) NULL,
		[CustomerName] [varchar](64) NULL,
		[CustomerEmail] [varchar](254) NULL,
		[CustomerPhone] [char](10) NULL,
		[EventAddress] [varchar](100) NULL,
		[EventCity] [varchar](30) NULL,
		[EventState] [char](2) NULL,
		[EventZipode] [varchar](9) NULL,
		[EventLocation] [geography] NULL,
		[EventLatitude]  AS ([EventLocation].[Lat]),
		[EventLongitude]  AS ([EventLocation].[Long]),
		[RestrictedLocation] [int] NULL,
		[Market] [int] NULL,
		[SubMarket] [int] NULL,
		[AssignmentTypeCode] [int] NOT NULL,
		[EventDescription] [varchar](max) NULL,
		[PureDuration] [int] NULL,
		[AdditionalTravelDuration] [int] NULL,
		[AdditionalUpsellDuration] [int] NULL,
		[PlannedDuration] [int] NULL,
		[ActualDuration] [int] NULL,
		[PlannedTravelDuration] [int] NULL,
		[ActualTravelDuration] [int] NULL,
		[Calendar] [int] NULL,
		[UnionCode] [int] NULL,
		[ApprovedByEmployeeID] [varchar](20) NULL,
		[ApprovedDate] [datetimeoffset](0) NULL,
		[TaskCreateDate] [datetimeoffset](0) NULL,
		[TaskExternalLastModifiedDate] [datetimeoffset](0) NULL,
		[TaskInternalLastModifiedDate] [datetimeoffset](0) NULL,
		[AssignmentCreateDate] [datetimeoffset](0) NULL,
		[AssignmentExternalLastModifiedDate] [datetimeoffset](0) NULL,
		[AssignmentInternalLastModifiedDate] [datetimeoffset](0) NULL,
		[AssignmentStatus] [int] NULL);
		
	INSERT INTO
		#TBL_ClickRoutingScheduleTemp
		(TaskKey
		,AssignmentEventKey
		,ExternalReferenceID
		,CallID
		,CallNumber
		,TaskStatusCode
		,TaskStatus
		,AttemptType
		,TaskEmployeeID
		,AssignEmployeeID
		,TaskType
		,ServiceUnit
		,ServiceOrder
		,VisitNumber
		,ScheduleDate
		,CustomerRequestedStartDate
		,CustomerRequestedEndDate
		,ScheduledStartDate
		,ScheduledEndDate
		,ActualStartDate
		,ActualEndDate
		,CustomerName
	--	,CustomerEmail
		,CustomerPhone
		,EventAddress
		,EventCity
		,EventState
		,EventZipode
		,EventLocation
		,RestrictedLocation
		,Market
		,SubMarket
		,AssignmentTypeCode
		,EventDescription
		,PureDuration
		,AdditionalTravelDuration
		,AdditionalUpsellDuration
		,PlannedDuration
		,ActualDuration
		,PlannedTravelDuration
		,ActualTravelDuration
		,Calendar
		,UnionCode
		,ApprovedByEmployeeID
		,ApprovedDate
		,TaskCreateDate
		,TaskExternalLastModifiedDate
		,TaskInternalLastModifiedDate
		,AssignmentCreateDate
		,AssignmentExternalLastModifiedDate
		,AssignmentInternalLastModifiedDate
		,AssignmentStatus)
	SELECT 
		TaskKey
		,AssignmentEventKey
		,ExternalReferenceID
		,CallID
		,CallNumber
		,TaskStatusCode
		,TaskStatus
		,AttemptType
		,TaskEmployeeID
		,AssignEmployeeID
		,TaskType
		,ServiceUnit
		,ServiceOrder
		,VisitNumber
		,ScheduleDate
		,CustomerRequestedStartDate
		,CustomerRequestedEndDate
		,ScheduledStartDate
		,ScheduledEndDate
		,ActualStartDate
		,ActualEndDate
		,CustomerName
		,CustomerPhone
		,EventAddress
		,EventCity
		,EventState
		,EventZipode
		,EventLocation
		,RestrictedLocation
		,Market
		,SubMarket
		,AssignmentTypeCode
		,EventDescription
		,PureDuration
		,AdditionalTravelDuration
		,AdditionalUpsellDuration
		,PlannedDuration
		,ActualDuration
		,PlannedTravelDuration
		,ActualTravelDuration
		,Calendar
		,UnionCode
		,ApprovedByEmployeeID
		,ApprovedDate
		,TaskCreateDate
		,TaskExternalLastModifiedDate
		,TaskInternalLastModifiedDate
		,AssignmentCreateDate
		,AssignmentExternalLastModifiedDate
		,AssignmentInternalLastModifiedDate
		,AssignmentStatus
	FROM OPENQUERY(TRYDVSQL1, '
		SELECT 
			t.TaskKey
			,t.AssignmentEventKey
			,t.ExternalReferenceID
			,t.CallID
			,t.CallNumber
			,t.StatusCode AS TaskStatusCode
			,ts.ShortDescription AS TaskStatus
			,CASE 
				WHEN t.CallNumber = 1 THEN ''TEC'' 
				WHEN t.CallNumber = 2 THEN ''PPU''
				ELSE ''REG'' 
				END AS AttemptType
			,t.TechnicianEmployeeID AS TaskEmployeeID
			,t.TechnicianEmployeeID AS AssignEmployeeID
			,CASE 
				WHEN t.TaskType = ''613056512'' THEN ''Repair''
				WHEN t.TaskType = ''613056513'' THEN ''PM Check''
				ELSE CAST(t.TaskType AS VARCHAR(25))
				END AS TaskType
			,t.UnitNumber AS ServiceUnit
			,t.ServiceOrderNumber AS ServiceOrder
			,t.VisitNumber
			,CAST(t.CustomerRequestedStartDate AS DATE) AS ScheduleDate
			,t.CustomerRequestedStartDate
			,t.CustomerRequestedEndDate
			,CASE WHEN t.ScheduledStartDate = ''1899-12-30 00:00:00 -06:00'' THEN NULL ELSE t.ScheduledStartDate END AS ScheduledStartDate
			,CASE WHEN t.ScheduledEndDate = ''1899-12-30 00:00:00 -06:00'' THEN NULL ELSE t.ScheduledEndDate END AS ScheduledEndDate
			,CASE WHEN t.ActualStartDate = ''1899-12-30 00:00:00 -06:00'' THEN NULL ELSE t.ActualStartDate END AS ActualStartDate
			,CASE WHEN t.ActualEndDate = ''1899-12-30 00:00:00 -06:00'' THEN NULL ELSE t.ActualEndDate END AS ActualEndDate
			,t.CustomerName
			,t.CustomerPhone
			,t.EventAddress
			,t.EventCity
			,t.EventState
			,t.EventZipode
			,t.EventLocation
			,t.RestrictedLocation
			,t.Market
			,t.SubMarket
			,t.AssignmentTypeCode
			,t.EventDescription
			,t.PureDuration
			,t.AdditionalTravelDuration
			,t.AdditionalUpsellDuration
			,t.PlannedDuration
			,t.ActualDuration
			,t.PlannedTravelDuration
			,t.ActualTravelDuration
			,t.Calendar
			,t.UnionCode
			,t.ApprovedByEmployeeID
			,t.ApprovedDate
			,t.TaskCreateDate
			,t.TaskExternalLastModifiedDate
			,t.TaskInternalLastModifiedDate
			,t.AssignmentCreateDate
			,t.AssignmentExternalLastModifiedDate
			,t.AssignmentInternalLastModifiedDate
			,t.AssignmentStatus
		FROM 
			DataMaster.dbo.TBL_ClickRoutingSchedule t WITH (NOLOCK)
		LEFT JOIN 
			(SELECT * FROM DataMaster.dbo.TBL_Enumerations WITH (NOLOCK) WHERE Business = ''Click'' AND OwnerType IN (''TaskStatus'')) ts
			ON t.StatusCode = ts.NumericValue
		WHERE
			CAST(t.CustomerRequestedStartDate AS DATE) >= CAST(GETDATE() AS DATE)
		')

	-- Merge into primary table
	MERGE 
		Employee.dbo.TBL_ClickRoutingSchedule AS T
	USING 
		(SELECT 
			TaskKey
			,AssignmentEventKey
			,ExternalReferenceID
			,CallID
			,CallNumber
			,TaskStatusCode
			,TaskStatus
			,AttemptType
			,TaskEmployeeID
			,AssignEmployeeID
			,TaskType
			,ServiceUnit
			,ServiceOrder
			,VisitNumber
			,ScheduleDate
			,CustomerRequestedStartDate
			,CustomerRequestedEndDate
			,ScheduledStartDate
			,ScheduledEndDate
			,ActualStartDate
			,ActualEndDate
			,CustomerName
			,CustomerPhone
			,EventAddress
			,EventCity
			,EventState
			,EventZipode
			,EventLocation
			,RestrictedLocation
			,Market
			,SubMarket
			,AssignmentTypeCode
			,EventDescription
			,PureDuration
			,AdditionalTravelDuration
			,AdditionalUpsellDuration
			,PlannedDuration
			,ActualDuration
			,PlannedTravelDuration
			,ActualTravelDuration
			,Calendar
			,UnionCode
			,ApprovedByEmployeeID
			,ApprovedDate
			,TaskCreateDate
			,TaskExternalLastModifiedDate
			,TaskInternalLastModifiedDate
			,AssignmentCreateDate
			,AssignmentExternalLastModifiedDate
			,AssignmentInternalLastModifiedDate
			,AssignmentStatus
		FROM 
			#TBL_ClickRoutingScheduleTemp) AS S
		ON T.CallID = S.CallID
		AND T.CallNumber = S.CallNumber
	WHEN MATCHED THEN
		UPDATE 
		SET 
			T.TaskKey = S.TaskKey
			,T.AssignmentEventKey = S.AssignmentEventKey
			,T.ExternalReferenceID = S.ExternalReferenceID
			,T.TaskStatusCode = S.TaskStatusCode
			,T.TaskStatus = S.TaskStatus
			,T.AttemptType = S.AttemptType
			,T.TaskEmployeeID = S.TaskEmployeeID
			,T.AssignEmployeeID = S.AssignEmployeeID
			,T.TaskType = S.TaskType
			,T.ServiceUnit = S.ServiceUnit
			,T.ServiceOrder = S.ServiceOrder
			,T.VisitNumber = S.VisitNumber
			,T.ScheduleDate = S.ScheduleDate
			,T.CustomerRequestedStartDate = S.CustomerRequestedStartDate
			,T.CustomerRequestedEndDate = S.CustomerRequestedEndDate
			,T.ScheduledStartDate = S.ScheduledStartDate
			,T.ScheduledEndDate = S.ScheduledEndDate
			,T.ActualStartDate = S.ActualStartDate
			,T.ActualEndDate = S.ActualEndDate
			,T.CustomerName = S.CustomerName
			,T.CustomerPhone = S.CustomerPhone
			,T.EventAddress = S.EventAddress
			,T.EventCity = S.EventCity
			,T.EventState = S.EventState
			,T.EventZipode = S.EventZipode
			,T.EventLocation = S.EventLocation
			,T.RestrictedLocation = S.RestrictedLocation
			,T.Market = S.Market
			,T.SubMarket = S.SubMarket
			,T.AssignmentTypeCode = S.AssignmentTypeCode
			,T.EventDescription = S.EventDescription
			,T.PureDuration = S.PureDuration
			,T.AdditionalTravelDuration = S.AdditionalTravelDuration
			,T.AdditionalUpsellDuration = S.AdditionalUpsellDuration
			,T.PlannedDuration = S.PlannedDuration
			,T.ActualDuration = S.ActualDuration
			,T.PlannedTravelDuration = S.PlannedTravelDuration
			,T.ActualTravelDuration = S.ActualTravelDuration
			,T.Calendar = S.Calendar
			,T.UnionCode = S.UnionCode
			,T.ApprovedByEmployeeID = S.ApprovedByEmployeeID
			,T.ApprovedDate = S.ApprovedDate
			,T.TaskCreateDate = S.TaskCreateDate
			,T.TaskExternalLastModifiedDate = S.TaskExternalLastModifiedDate
			,T.TaskInternalLastModifiedDate = S.TaskInternalLastModifiedDate
			,T.AssignmentCreateDate = S.AssignmentCreateDate
			,T.AssignmentExternalLastModifiedDate = S.AssignmentExternalLastModifiedDate
			,T.AssignmentInternalLastModifiedDate = S.AssignmentInternalLastModifiedDate
			,T.AssignmentStatus = S.AssignmentStatus
	WHEN NOT MATCHED BY TARGET THEN
		INSERT 
			(TaskKey
			,AssignmentEventKey
			,ExternalReferenceID
			,CallID
			,CallNumber
			,TaskStatusCode
			,TaskStatus
			,AttemptType
			,TaskEmployeeID
			,AssignEmployeeID
			,TaskType
			,ServiceUnit
			,ServiceOrder
			,VisitNumber
			,ScheduleDate
			,CustomerRequestedStartDate
			,CustomerRequestedEndDate
			,ScheduledStartDate
			,ScheduledEndDate
			,ActualStartDate
			,ActualEndDate
			,CustomerName
			,CustomerPhone
			,EventAddress
			,EventCity
			,EventState
			,EventZipode
			,EventLocation
			,RestrictedLocation
			,Market
			,SubMarket
			,AssignmentTypeCode
			,EventDescription
			,PureDuration
			,AdditionalTravelDuration
			,AdditionalUpsellDuration
			,PlannedDuration
			,ActualDuration
			,PlannedTravelDuration
			,ActualTravelDuration
			,Calendar
			,UnionCode
			,ApprovedByEmployeeID
			,ApprovedDate
			,TaskCreateDate
			,TaskExternalLastModifiedDate
			,TaskInternalLastModifiedDate
			,AssignmentCreateDate
			,AssignmentExternalLastModifiedDate
			,AssignmentInternalLastModifiedDate
			,AssignmentStatus)
		VALUES
			(S.TaskKey
			,S.AssignmentEventKey
			,S.ExternalReferenceID
			,S.CallID
			,S.CallNumber
			,S.TaskStatusCode
			,S.TaskStatus
			,S.AttemptType
			,S.TaskEmployeeID
			,S.AssignEmployeeID
			,S.TaskType
			,S.ServiceUnit
			,S.ServiceOrder
			,S.VisitNumber
			,S.ScheduleDate
			,S.CustomerRequestedStartDate
			,S.CustomerRequestedEndDate
			,S.ScheduledStartDate
			,S.ScheduledEndDate
			,S.ActualStartDate
			,S.ActualEndDate
			,S.CustomerName
			,S.CustomerPhone
			,S.EventAddress
			,S.EventCity
			,S.EventState
			,S.EventZipode
			,S.EventLocation
			,S.RestrictedLocation
			,S.Market
			,S.SubMarket
			,S.AssignmentTypeCode
			,S.EventDescription
			,S.PureDuration
			,S.AdditionalTravelDuration
			,S.AdditionalUpsellDuration
			,S.PlannedDuration
			,S.ActualDuration
			,S.PlannedTravelDuration
			,S.ActualTravelDuration
			,S.Calendar
			,S.UnionCode
			,S.ApprovedByEmployeeID
			,S.ApprovedDate
			,S.TaskCreateDate
			,S.TaskExternalLastModifiedDate
			,S.TaskInternalLastModifiedDate
			,S.AssignmentCreateDate
			,S.AssignmentExternalLastModifiedDate
			,S.AssignmentInternalLastModifiedDate
			,S.AssignmentStatus);
				
	IF object_id('tempdb..#TBL_ClickRoutingScheduleTemp') IS NOT NULL
	BEGIN
		DROP TABLE #TBL_ClickRoutingScheduleTemp;
	END


	UPDATE ServiceOrder.dbo.TBL_ServiceOrder
	SET RoutingStatus = r.TaskStatus
	,RoutingStatusCode = r.TaskStatusCode
	,RoutingStatusDateTime = r.TaskInternalLastModifiedDate
	,EventLocation = r.EventLocation
	--SELECT r.*
	FROM 
		ServiceOrder.dbo.TBL_ServiceOrder s WITH (NOLOCK)
	JOIN
		(	SELECT 
				ServiceUnit
				, ServiceOrder
				, TaskStatus
				, TaskStatusCode
				, TaskInternalLastModifiedDate
				, EventLocation
				, EventLatitude
				, EventLongitude 
			FROM 
				Employee.dbo.TBL_ClickRoutingSchedule WITH (NOLOCK) 
			WHERE 
				CallNumber = 0
		) r
		ON s.ServiceUnit = r.ServiceUnit
		AND s.ServiceOrder = r.ServiceOrder

END






GO
/****** Object:  StoredProcedure [dbo].[PROC_ServiceOrderProductSalesRequestUpdate]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
	Developed By: PGALVAN
	Created: 2018-06-08
	Description: Moved into Proc from QEE
	
	Updated: 2018-10-11
	UPdated By: PGALVAN
	Update Reason: Update to ProductSalesRequest table top 1 to max row for customer service order
*/

CREATE PROCEDURE [dbo].[PROC_ServiceOrderProductSalesRequestUpdate]
	@ErrorCode varchar(150) = NULL OUTPUT
AS
BEGIN 
	UPDATE ServiceOrder.dbo.TBL_ProductSalesRequest
	SET ServiceUnit = '000' + ServiceUnit
	WHERE LEN(ServiceUnit) = 4

	INSERT INTO ServiceOrder.dbo.TBL_ServiceOrderProductSalesRequest
		(ProductSalesRequestID
		,CustomerID
		,ServiceUnit
		,ServiceOrder
		,CustomerFirstName
		,CustomerLastName
		,CustomerPhoneNumber
		,CustomerPhoneNumberSecondary
		,CustomerEmail
		,CustomerEmailSeconday
		,ItemSuffixNumber
		,CurrentApplianceDetails
		,CallBackDate
		,CallBackDateTimeFrom
		,CallBackDateTimeTo
		,CallBackRequested
		,CouponInfo
		)
	SELECT
		ProductSalesRequestID
		,CustomerID
		,ServiceUnit
		,ServiceOrder
		,CustomerFirstName
		,CustomerLastName
		,CustomerPhoneNumber
		,CustomerPhoneNumberSecondary
		,CustomerEmail
		,CustomerEmailSeconday
		,ItemSuffixNumber
		,CurrentApplianceDetails
		,CallBackDate
		,CAST(CallBackDate AS DATETIME) + CAST(LEFT(RIGHT('000' + LTRIM(RTRIM(CallBackDateTimeFrom)),4),2) + ':' + RIGHT('000' + LTRIM(RTRIM(CallBackDateTimeFrom)),2) as datetime) AS CallBackDateTimeFrom
		,CAST(CallBackDate AS DATETIME) + CAST(LEFT(RIGHT('000' + LTRIM(RTRIM(CallBackDateTimeTo)),4),2) + ':' + RIGHT('000' + LTRIM(RTRIM(CallBackDateTimeTo)),2) as datetime) AS CallBackDateTimeTo
		/*
		,CAST(CAST(CallBackDate AS DATETIME) + CAST(STUFF(CallBackDateTimeFrom,3,0,':') AS DATETIME) AS DATETIME) AS CallBackDateTimeFrom
		,CAST(CAST(CallBackDate AS DATETIME) + CAST(STUFF(CallBackDateTimeTo,3,0,':') AS DATETIME) AS DATETIME) AS CallBackDateTimeTo
		*/
		,CallBackRequested
		,CouponInfo
	FROM
		ServiceOrder.dbo.TBL_ProductSalesRequest
	WHERE
		ProcessDateTime IS NULL

	INSERT INTO ServiceOrder.dbo.TBL_ServiceOrderPSRGoodBetterBest
		(ProductSalesRequestID
		,GoodBetterBestRowNumber
		,GoodBetterBest
		)
	SELECT 
		ProductSalesRequestID
		,ROW_NUMBER() OVER(ORDER BY ProductSalesRequestID) AS GoodBetterBestRowNumber
		,valuefound AS GoodBetterBest
	FROM
		ServiceOrder.dbo.TBL_ProductSalesRequest p WITH (NOLOCK)
	CROSS APPLY
		NFDT.dbo.Split('|', GoodBetterBest) 
	WHERE
		p.ProcessDateTime IS NULL

	INSERT INTO ServiceOrder.dbo.TBL_ServiceOrderPSRCustomerNotes
		(ProductSalesRequestID
		,CustomerNotesRowNumber
		,CustomerNotes
		)
	SELECT 
		ProductSalesRequestID
		,ROW_NUMBER() OVER(ORDER BY ProductSalesRequestID) AS CustomerNotesRowNumber
		,valuefound AS CustomerNotes
	FROM
		ServiceOrder.dbo.TBL_ProductSalesRequest p WITH (NOLOCK)
	CROSS APPLY
		NFDT.dbo.Split('|', CustomerNotes) 
	WHERE
		p.ProcessDateTime IS NULL

	UPDATE 
		p
	SET 
		ServiceUnit = s.ServiceUnit
		,ServiceOrder = s.ServiceOrder
	--SELECT DISTINCT s.ServiceUnit, s.ServiceOrder, s.CustomerID, AllRows, MaxRow
	FROM 
		ServiceOrder.dbo.TBL_ServiceOrderProductSalesRequest p WITH (NOLOCK) 
		INNER JOIN (
			SELECT
					s.CustomerID,
					s.ServiceUnit,
					s.ServiceOrder,
					MaxRow,
					row_number() OVER(PARTITION BY s.CustomerID ORDER BY  s.ServiceOrderStatusDate asc) as AllRows
			FROM (
					SELECT
						s.CustomerID, MAX(SEQ) as MaxRow
					FROM  
						ServiceOrder.dbo.TBL_ServiceOrder s WITH (NOLOCK)
						INNER JOIN (
							SELECT
								s.CustomerID
								,row_number() OVER(PARTITION BY s.CustomerID ORDER BY  s.ServiceOrderStatusDate asc) as seq
							FROM  
								ServiceOrder.dbo.TBL_ServiceOrder s WITH (NOLOCK)
								INNER JOIn ServiceOrder.dbo.TBL_ServiceOrderProductSalesRequest as p WITH (nolock) ON s.CustomerID = p.CustomerID
							WHERE
								p.ProcessDateTime IS NULL
								AND p.ServiceOrder IS NULL
						) as sm on s.CustomerID = sm.CustomerID
					GROUP BY
						s.CustomerID
				) as cm
				INNER JOIN ServiceOrder.dbo.TBL_ServiceOrder s WITH (NOLOCK) on s.CustomerID = cm.CustomerID 
		) as s	ON s.CustomerID = p.CustomerID 
	WHERE
		s.MaxRow = s.AllRows

	/* Replaced with the above update
	UPDATE ServiceOrder.dbo.TBL_ServiceOrderProductSalesRequest
	SET ServiceUnit = s.ServiceUnit
	,ServiceOrder = s.ServiceOrder
	--SELECT *
	FROM 
		ServiceOrder.dbo.TBL_ServiceOrderProductSalesRequest p WITH (NOLOCK)
	JOIN
		(SELECT TOP 1 s.ServiceUnit, s.ServiceOrder, s.CustomerID FROM ServiceOrder.dbo.TBL_ServiceOrder s WITH (NOLOCK) JOIN ServiceOrder.dbo.TBL_ServiceOrderProductSalesRequest p WITH (NOLOCK) ON s.CustomerID = p.CustomerID ORDER BY s.ServiceOrderStatusDate DESC) s
		ON p.CustomerID = s.CustomerID
	WHERE
		p.ProcessDateTime IS NULL
		AND p.ServiceOrder IS NULL
   */

   --Process all unprocessed records
	UPDATE s
	SET ProductSalesRequest = ISNULL(ProductSalesRequest, 0) + 1
	,ProductSalesRequestDateTime = GETDATE()
	--SELECT *
	FROM 
		ServiceOrder.dbo.TBL_ServiceOrder s WITH (NOLOCK)
	JOIN 
		ServiceOrder.dbo.TBL_ServiceOrderProductSalesRequest p WITH (NOLOCK)
		ON s.ServiceUnit = p.ServiceUnit
		AND s.ServiceOrder = p.ServiceOrder
	WHERE
		p.ProcessDateTime IS NULL

	--Ensure we process all orders that weren't created at the time the sales request was created
	UPDATE s
	SET ProductSalesRequest = ISNULL(ProductSalesRequest, 0) + 1
	,ProductSalesRequestDateTime = GETDATE()
	--SELECT *
	FROM 
		ServiceOrder.dbo.TBL_ServiceOrder s WITH (NOLOCK)
	JOIN 
		ServiceOrder.dbo.TBL_ServiceOrderProductSalesRequest p WITH (NOLOCK)
		ON s.ServiceUnit = p.ServiceUnit
		AND s.ServiceOrder = p.ServiceOrder
	WHERE
		s.ProductSalesRequestDateTime IS NULL


	UPDATE ServiceOrder.dbo.TBL_ProductSalesRequest
	SET ProcessDateTime = GETDATE()
	WHERE ProcessDateTime IS NULL
	AND ProductSalesRequestID IN (SELECT ProductSalesRequestID FROM ServiceOrder.dbo.TBL_ServiceOrderProductSalesRequest WITH (NOLOCK))

	UPDATE ServiceOrder.dbo.TBL_ServiceOrderProductSalesRequest
	SET ProcessDateTime = GETDATE()
	WHERE ProcessDateTime IS NULL
END





GO
/****** Object:  StoredProcedure [dbo].[PROC_ServiceOrderRealTimeFeedbackUpdate]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
	Developed By: PGALVAN
	Created: 2017-11-07
	Description: Update Service Order table with realtime feedback request counts and datetime.

	Modified: 2018-02-15
	Modified By: PGALVAN
	Modified Reason: added RTF CustomerID update. 

	Modified: 2018-03-21
	Modified By: PGALVAN
	Modified Reason: Added check for 3 days of history

	Modified: 2018-08-6
	Modified By: PGALVAN
	Modified Reason: Added SMS due to SMS flow changing from email flow
*/

CREATE PROCEDURE [dbo].[PROC_ServiceOrderRealTimeFeedbackUpdate]
	@ErrorCode varchar(150) = NULL OUTPUT
AS
BEGIN 
	/*Update ServiceOrder table with Email + SMS Customer Callback Requests*/
	UPDATE
		id
	SET
		RealtimeFeedbackCount = fb.EmailFeedbackCount + fb.TextFeedbackCount
		 ,RealtimeFeedbackDateTime = CASE 
										WHEN EmailEndDate IS NULL  
										THEN TextEndDate 
										WHEN TextEndDate IS NULL 
										THEN EmailEndDate 
										WHEN EmailEndDate > TextEndDate
										THEN EmailEndDate
										WHEN TextEndDate > EmailEndDate
										THEN TextEndDate
										ELSE GETDATE()
									END
	--SELECT id.ServiceUnit,	id.ServiceOrder,	EmailFeedbackCount,	TextFeedbackCount,	EmailEndDate,	TextEndDate
	FROM 
		ServiceOrder.dbo.TBL_ServiceOrder as id with (nolock) 
		INNER JOIN (
			SELECT 
				COALESCE(rEmail.ServiceUnit,  rText.ServiceUnit) as ServiceUnit,
				COALESCE(rEmail.ServiceOrder,  rText.ServiceOrder) as ServiceOrder,
				COALESCE(rEmail.RealTimeFeedbackCounted, 0) as EmailFeedbackCount,
				COALESCE(rText.RealTimeFeedbackCounted,0) as TextFeedbackCount,
				COALESCE(rEmail.MaxFeedbackEnd, NULL) as EmailEndDate,
				COALESCE(rText.MaxFeedbackEnd, NULL) as TextEndDate
			FROM (
				SELECT 
					COUNT(ResponseID) as RealTimeFeedbackCounted,
					MAX(FeedbackEnd) as MaxFeedbackEnd,
					ServiceUnit,
					ServiceOrder
				FROM 
					ServiceOrder.dbo.TBL_RealTimeFeedBack as fb1  with (nolock)
				WHERE 
					RequestContact = 1
					AND EXISTS (
						SELECT 
							fb2.ServiceUnit,
							fb2.ServiceOrder
						FROM
							ServiceOrder.dbo.TBL_RealTimeFeedBack as fb2 with (nolock)
						WHERE 
							fb2.FeedbackEnd >= CAST(DATEADD(day,-3,GETDATE()) as date)
							AND fb2.RequestContact = 1
							AND fb1.ServiceUnit = fb2.ServiceUnit and fb1.ServiceOrder = fb2.ServiceOrder
					)
				GROUP BY
					ServiceUnit,
					ServiceOrder
				) as rEmail
			FULL OUTER JOIN (
				SELECT 
					COUNT(RealTimeFeedbackID) as RealTimeFeedbackCounted,
					MAX(ExternalCreateDate) as MaxFeedbackEnd,
					ServiceUnit,
					ServiceOrder
				FROM 
					ServiceOrder.dbo.TBL_RealTimeFeedBackAll as fb1 with (nolock)
				WHERE 
					RequestContact = 1 
					AND FeedbackScore < 4
					AND EntryType = 'RealTimeFeedbackText'
					AND EXISTS (
						SELECT 
							fb2.ServiceUnit,
							fb2.ServiceOrder
						FROM
							ServiceOrder.dbo.TBL_RealTimeFeedBackAll as fb2 with (nolock)
						WHERE 
							fb2.ExternalCreateDate >= CAST(DATEADD(day,-3,GETDATE()) as date) 
							AND fb2.RequestContact = 1
							AND fb1.ServiceUnit = fb2.ServiceUnit and fb1.ServiceOrder = fb2.ServiceOrder
					)
				GROUP BY
					ServiceUnit,
					ServiceOrder
			) as rText on rEmail.ServiceUnit = rText.ServiceUnit AND rEmail.ServiceOrder = rText.ServiceOrder
		) as fb on fb.ServiceUnit = id.ServiceUnit and fb.ServiceOrder = id.ServiceOrder



	/*Update email RTF data with Customer ID*/
	UPDATE
		rtf
	SET 
		CustomerID = id.CustomerID
	FROM 
		ServiceOrder.dbo.TBL_RealTimeFeedBack as rtf
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrder as id ON rtf.ServiceUnit = id.ServiceUnit and rtf.ServiceOrder = id.ServiceOrder
	WHERE 
		rtf.CustomerID is null
		AND rtf.InsertDate >= CAST(GETDATE() -3 as date)
	

	/*Update SMS RTF data with Customer ID*/
	UPDATE
		rtf
	SET 
		CustomerID = id.CustomerID
	FROM 
		ServiceOrder.dbo.TBL_RealTimeFeedBackAll as rtf
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrder as id ON rtf.ServiceUnit = id.ServiceUnit and rtf.ServiceOrder = id.ServiceOrder
	WHERE 
		rtf.CustomerID is null
		AND rtf.CreateDate >= CAST(GETDATE() -3 as date)

END





GO
/****** Object:  StoredProcedure [dbo].[PROC_ServiceOrderServiceAttemptUpdate]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


	/*
		Developed By: PGALVAN
		Created: 2018-06-08
		Description: Moved into Proc from QEE
	*/

	CREATE PROCEDURE [dbo].[PROC_ServiceOrderServiceAttemptUpdate]
		@ErrorCode varchar(150) = NULL OUTPUT
	AS
	BEGIN 
		IF object_id('tempdb..#TBL_Temp') IS NOT NULL
	BEGIN
		DROP TABLE #TBL_Temp;
	END
		
	CREATE TABLE #TBL_Temp
	(
		[ServiceUnit] [char](7) NOT NULL,
		[ServiceOrder] [char](8) NOT NULL,
		FirstAttemptDate date  NULL,
		LastAttemptDate date  NULL,
		LastAttemptTechID char(7)  NULL,
		LastAttemptTechComments1 char(60)  NULL,
		LastAttemptTechComments2 char(60)  NULL,
		LastAttemptCode varchar(2)  NULL,
		AttemptCount [int]  NULL
		)

	INSERT INTO 
		#TBL_Temp
	SELECT      
		a.ServiceUnit,
		a.ServiceOrder,
		m.FirstAttemptDate,
		m.LastAttemptDate,
		a.ServiceTechEmployee,
		a.TechnicianCommentsOne,
		a.TechnicianCommentsTwo,
		a.CallCode,
		m.Attempts
	FROM 
		(SELECT * FROM ServiceOrder.dbo.TBL_NPSServiceOrderServiceAttempt WITH (NOLOCK) WHERE CAST(LastActionTimestamp AS DATE) = CAST(GETDATE() AS DATE)) a
	JOIN
		(      
			SELECT
				ServiceUnit
				,ServiceOrder
				,MIN(ServiceCallDate) AS FirstAttemptDate
				,MAX(ServiceCallDate) AS LastAttemptDate
				,COUNT(ServiceCallDate) AS Attempts
			FROM
				ServiceOrder.dbo.TBL_NPSServiceOrderServiceAttempt WITH (NOLOCK)
			GROUP BY
				ServiceUnit
				,ServiceOrder
		   ) m
		ON a.ServiceUnit = m.ServiceUnit
		AND a.ServiceOrder = m.ServiceOrder

	UPDATE	
		s
	SET    
		FirstAttemptDate = a.FirstAttemptDate
		,LastAttemptDate = a.LastAttemptDate
		,LastAttemptTechID = a.LastAttemptTechID
		,LastAttemptTechComment1 = a.LastAttemptTechComments1
		,LastAttemptTechComment2 = a.LastAttemptTechComments2
		,LastAttemptCode = a.LastAttemptCode
		,AttemptCount = a.AttemptCount
	FROM 
		#TBL_Temp as a
		INNER JOIN  ServiceOrder.dbo.TBL_ServiceOrder as s    
			ON s.ServiceUnit = a.ServiceUnit
			AND s.ServiceOrder = a.ServiceOrder


	IF object_id('tempdb..#TBL_Temp') IS NOT NULL
	BEGIN
		DROP TABLE #TBL_Temp;
	END


END






GO
/****** Object:  StoredProcedure [dbo].[PROC_ServiceOrderTaskLog_Insert]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Preston Galvanek
-- Create date: 3/09/2018
-- Description: Create New Log entry for a task in Service Pro
-- Modified: --

-- =============================================
CREATE PROCEDURE [dbo].[PROC_ServiceOrderTaskLog_Insert]
	 @TaskID as int,
	 @ActionID as int,
	 @EnterpriseID as varchar(10),
	 @DueDateUpdateFlag bit = NULL,
	 @PreviousLogID int = NULL, /*Optional Field, if there is a previous log item, then upate that item with the modificaiton user and date to identify the task log item as being worked.*/
	 @LogID int = NULL OUTPUT
AS
BEGIN  

if @DueDateUpdateFlag IS NULL
	BEGIN
		SET @DueDateUpdateFlag = 0
	END

IF @PreviousLogID IS NOT NULL
	BEGIN
		UPDATE 
			ServiceOrder.dbo.TBL_ServiceOrderTasksLog 
        SET 
			ModifiedDateTime = GETDATE()
			,EnterpriseID = @EnterpriseID 
        WHERE 
             LogID = @PreviousLogID;
	END

	
DECLARE @OutputTbl TABLE (ID INT)

INSERT INTO 
	ServiceOrder.dbo.TBL_ServiceOrderTasksLog 
	(TaskID, OptionID, EnterpriseID, ModifiedDateTime, CreateEnterpriseID, CreateDateTime) 
OUTPUT 
	INSERTED.LogID INTO @OutputTbl(ID)
SELECT 
	@TaskID 
	,@ActionID 
	,@EnterpriseID 
	,GETDATE() 
	,@EnterpriseID 
	,GETDATE() 
OPTION (MAXDOP 1);

SET @LogID = (SELECT ID FROM @OutputTbl) --scope_identity();

/*update log id in tasks table*/
UPDATE
	ServiceOrder.dbo.TBL_ServiceOrderTasks
SET
	LogID = @LogID
WHERE
	TaskID = @TaskID;


If @LogID = NULL
	BEGIN       
		DECLARE @BodyText varchar(100) =  'TaskID: ' + @TaskID +  ' LogID:' + @LogID + ' Error: NULL LogID for Task table update.'

		EXEC msdb.dbo.sp_send_dbmail
			@profile_name = 'ServicePro',
			@recipients =  'PGALVAN@searshc.com',
			@body = @BodyText,
			@body_format = 'HTML',
			@subject = 'ServicePro - Error - PROC_ServiceOrderTaskLog_Insert';
	END	



/*Update due date when requested.*/
IF @DueDateUpdateFlag = 1
	BEGIN
		UPDATE
			ServiceOrder.dbo.TBL_ServiceOrderTasks
		SET
			DueDate = DATEADD(day,(SELECT DueDateAdjustment FROM ServiceOrder.dbo.TBL_ServiceOrderQueueOptions as o with (nolock) WHERE optionID = @ActionID),DueDate) 
		WHERE
			TaskID = @TaskID;
	END
END





GO
/****** Object:  StoredProcedure [dbo].[PROC_ServiceOrderTasksClaimed_Insert]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Preston Galvanek
-- Create date: 3/09/2018
-- Description: Create New Claim entry for a task log item in Service Pro
-- Modified: --

-- =============================================
CREATE PROCEDURE [dbo].[PROC_ServiceOrderTasksClaimed_Insert]
	 @TaskID as int,
	 @LogID as int,
	 @ClaimedEnterpriseID as varchar(10) = null,
	 @EnterpriseID as varchar(10) = null,
	 @ClaimID int = NULL OUTPUT
AS
BEGIN  

IF @EnterpriseID IS NULL
	BEGIN
		SET @EnterpriseID = 'SYSTEM'
	END

DECLARE @OutputTbl TABLE (ID INT);

/*Insert Claim Record on log item*/
INSERT INTO 
	ServiceOrder.dbo.TBL_ServiceOrderTasksClaimed 
	(TaskID, LogID,ClaimedEnterpriseID,ModifiedEnterpriseID,ModifiedDateTime) 
OUTPUT 
	INSERTED.ClaimID INTO @OutputTbl(ID)
SELECT 
	t.TaskID, l.LogID, @ClaimedEnterpriseID as ClaimedEnterpriseID,@EnterpriseID as ModifiedEnterpriseID, GETDATE() as ModDate
FROM	
	ServiceOrder.dbo.TBL_ServiceOrderTasks as t with (nolock) 
	INNER JOIN (  
		SELECT   
			l.LogID, 
			l.TaskID,  
			l.OptionID,  
			l.CreateDateTime,
			l.CreateEnterpriseID  
		FROM   
			ServiceOrder.dbo.TBL_ServiceOrderTasksLog as l with (nolock)   
				INNER JOIN (    
            		SELECT    
            			TaskID,   
            			MAX(CreateDateTime) as CreateDateTime   
            		FROM   
            			ServiceOrder.dbo.TBL_ServiceOrderTasksLog as l with (nolock)    
            		GROUP BY    
            			TaskID   
				) as lm on l.TaskID = lm.TaskID and l.CreateDateTime = lm.CreateDateTime   
	) as l ON l.TaskID = t.TaskID   
WHERE 
	l.LogID = @LogID 
	AND t.TaskID = @TaskID
OPTION (MAXDOP 1);

SET @ClaimID = (SELECT ID FROM @OutputTbl) --scope_identity();

/*Update Task Table for quick FK reference */
UPDATE
	ServiceOrder.dbo.TBL_ServiceOrderTasks 
SET
	ClaimID = @ClaimID
WHERE
	TaskID = @TaskID

If @ClaimID = NULL
	BEGIN       
		DECLARE @BodyText varchar(100) =  'TaskID: ' + @TaskID +  ' LogID:' + @LogID + ' Error: NULL ClaimID for Task table update.'

		EXEC msdb.dbo.sp_send_dbmail
			@profile_name = 'ServicePro',
			@recipients =  'PGALVAN@searshc.com',
			@body = @BodyText,
			@body_format = 'HTML',
			@subject = 'ServicePro - Error - PROC_ServiceOrderTasksClaimed_Insert';
	END	



END




GO
/****** Object:  StoredProcedure [dbo].[PROC_ServiceOrderTrackingUPSUpdate]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




	/*
		Developed By: PGALVAN
		Created: 2018-10-08
		Description: update service order table with part tracking exception data
				Created per a Dana Porter request for MSO to work address errors before the part is delivered. 
		  Updated: 2018-12-04 by PGALVAN
		  Update: added ScheduledDeliveryDate
		  Update: added RescheduledDeliveryDate
	*/

	CREATE PROCEDURE [dbo].[PROC_ServiceOrderTrackingUPSUpdate]
		@ErrorCode varchar(150) = NULL OUTPUT
	AS
	BEGIN 
		IF object_id('tempdb..#TBL_Temp') IS NOT NULL
	BEGIN
		DROP TABLE #TBL_Temp;
	END
		
	CREATE TABLE #TBL_Temp
	(
		[ServiceUnit] [char](7) NOT NULL,
		[ServiceOrder] [char](8) NOT NULL,
		ExceptionDate date  NULL,
		ScheduledDeliveryDate date NULL,
		RescheduledDeliveryDate date NULL	
		)

	INSERT INTO 
		#TBL_Temp
	SELECT
		so.ServiceUnit      
		,so.ServiceOrder     
		,MAX([ExceptionDate]) as MaxExceptionDate
		,MAX(ScheduledDeliveryDate) as ScheduledDeliveryDate
		,MAX(RescheduledDeliveryDate) as RescheduledDeliveryDate
	FROM 
		Parts.dbo.TBL_ShipmentTrackingUPS ups WITH (NOLOCK)
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderPartsTracking so WITH (NOLOCK) ON  ups.TrackingNumber = so.TrackingNumber
	WHERE 
		(
		ExceptionDate >= CAST(getdate()-1 as date) 
		AND ExceptionReasonCode not in ('2R','2U','8M')--ExceptionReasonDescription NOT LIKE '%LL BE IN YOUR AREA%' 
		)
		OR
		ScheduledDeliveryDate >= CAST(getdate()-1 as date) 
		OR
		RescheduledDeliveryDate >= CAST(getdate()-1 as date) 
	GROUP BY
		so.ServiceUnit      
		,so.ServiceOrder 

	UPDATE	
		s
	SET    
		LastUPSExceptionDate = a.ExceptionDate
		,ScheduledDeliveryDate = a.ScheduledDeliveryDate
		,RescheduledDeliveryDate = a.RescheduledDeliveryDate
	FROM 
		#TBL_Temp as a
		INNER JOIN  ServiceOrder.dbo.TBL_ServiceOrder as s    
			ON s.ServiceUnit = a.ServiceUnit
			AND s.ServiceOrder = a.ServiceOrder


	IF object_id('tempdb..#TBL_Temp') IS NOT NULL
	BEGIN
		DROP TABLE #TBL_Temp;
	END


END










GO
/****** Object:  StoredProcedure [dbo].[PROC_ServiceProAddTask]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Preston Galvanek
-- Create date: 7/20/2017
-- Modified Date: 7/20/2017
-- Modified Date: 12/18/2017 - Added multiple layer group capability
-- Description: Create New Task in Service Pro
-- Modified Date 2/27/2018
-- Modified By: PGALVAN
-- Description: Added check against @ReportID for identifying groups AND the groupIDs
-- =============================================
CREATE PROCEDURE [dbo].[PROC_ServiceProAddTask]
	 @QueueID as int = NULL,
	 @ReportID as int = NULL,
	 @ServiceUnit as varchar(7) = NULL,
	 @ServiceOrder as varchar(8) = NULL,
	 @PriorityValue as int = NULL,
	 @DueDate as varchar(10) = NULL,
	 @AddUser as varchar(10) = NULL,
	 @TaskID int = NULL OUTPUT
AS
BEGIN  
	DECLARE @ControlColumnName as varchar(50) = '';
	DECLARE @strQuery as varchar(MAX);
	DECLARE @groupID as varchar(100)
	DECLARE @cols NVARCHAR(2000) 
	DECLARE @colsDynamic NVARCHAR(2000)
	DECLARE @name NVARCHAR(255)
	DECLARE @priorColumn NVARCHAR(255)
	DECLARE @pos INT
	DECLARE @pos2 INT
	DECLARE @keyvaluepair NVARCHAR(255)
	DECLARE @column NVARCHAR(255)
	DECLARE @value NVARCHAR(255)
	DECLARE @counter INT
	DECLARE @countergroup INT
	DECLARE @colQuery as varchar(MAX)
	DECLARE @groups as varchar(max);
	DECLARE @RowCountHold as int = 0; /*set to 0 in case there are no groups to query through*/

IF object_id('tempdb..#TBL_TaskEntered') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_TaskEntered;
		END
		
CREATE TABLE 
	#TBL_TaskEntered
	(
		TaskID [int] NOT NULL
	)
	
SELECT @groups = STUFF((
		SELECT DISTINCT TOP 100 Percent
			CAST(grp.GroupID as varchar) + ','
		FROM 
			ServiceOrder.dbo.TBL_ServiceOrderGroup as grp with (nolock) 
			INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderAssignment as a with (nolock)  on a.GroupID = grp.GroupID
		WHERE 
			a.QueueID = @QueueID 
			AND a.ReportID = @ReportID 
			AND a.GroupID <> -1
			AND grp.IsEnabled = 1
			AND EXISTS (SELECT TOP 1 Items.GroupID FROM ServiceOrder.dbo.TBL_ServiceOrderGroupItems as items with (nolock) WHERE items.GroupID = grp.GroupID)
		ORDER BY
			CAST(grp.GroupID as varchar) + ','
		FOR XML PATH('')   
		), 1, 0, '')

/*query to test groups being reviewed for entry. SELECT @groups*/
/*Reassign dynamic cols variable to default and counter to 0*/
SELECT @countergroup = 0
WHILE CHARINDEX(',', @groups) > 0 /* OR @groups is null*/
BEGIN
	SELECT @groupID = SUBSTRING(@groups,1,CHARINDEX(',',@groups)-1)
	SELECT 
		@cols = STUFF((
		SELECT DISTINCT TOP 100 Percent
			 ',' + CAST(col.Column_Name as varchar(50)) + '|' + CAST(g.ControlValue as varchar)
		FROM 
			ServiceOrder.dbo.TBL_ControlBuilder as c with (nolock)
			INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderGroupItems as g with (nolock) on c.ControlID = g.ControlID
			INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderGroup as grp with (nolock) on grp.GroupID = g.GroupID
			INNER JOIN ServiceOrder.INFORMATION_SCHEMA.COLUMNS as col with (nolock) on c.QueryColumn = col.Column_Name 
			INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderAssignment as a with (nolock)  on a.GroupID = g.GroupID
		WHERE 
			col.TABLE_NAME = N'VIEW_ServiceOrder'
			AND a.QueueID = @QueueID 
			AND a.GroupID = @groupID
			AND a.ReportID = @ReportID
			AND grp.IsEnabled = 1
		ORDER BY
			',' + CAST(col.Column_Name as varchar(50)) + '|' + CAST(g.ControlValue as varchar)
			FOR XML PATH('')   
			), 1, 1, '')

	/*
	QA: Display column list for each iteration
	SELECT @cols
	*/

	/*Reassign dynamic cols variable to default and counter to 0*/
	SELECT @colsDynamic = @cols
	SELECT @counter = 0
	SELECT @colQuery = ''
	SELECT @priorcolumn = ''
	 WHILE CHARINDEX(',', @colsDynamic) > 0  /*OR @groups is null Is groups is null implying -1 only assignment, then allow for a 1 time entry*/
		 BEGIN
			SELECT @counter = COALESCE(@counter,0) + 1
			SELECT @pos  = CHARINDEX(',', @colsDynamic) 
			SELECT @pos2  = CHARINDEX('|', @colsDynamic) 
			SELECT @keyvaluepair  = SUBSTRING(@colsDynamic, 1, @pos) 
			SELECT @column = SUBSTRING(@colsDynamic, 1, @pos2-1)
			SELECT @value  = SUBSTRING(@keyvaluepair,@pos2+1,@pos - @pos2-1 )
			SELECT @name = 
			CASE 
				WHEN @priorcolumn = ''
				THEN  ' AND (' + @column + ' = '''+ @value + ''''
				WHEN @column = @priorcolumn
				THEN  ' OR ' + @column + ' = '''+ @value + ''''
				ELSE  ') AND (' + @column + ' = '''+ @value + ''''
			END
			SELECT @colQuery = COALESCE(@colQuery,'') + COALESCE(@name,'')
			SELECT @colsDynamic = SUBSTRING(@colsDynamic, @pos+1, LEN(@colsDynamic)-@pos)
			SELECT @priorColumn = @column
		 END

		/* 
		SELECT SUBSTRING(@colsDynamic, 1, CHARINDEX('|', @colsDynamic)-1)
		SELECT @priorColumn
		*/

		SELECT 
			@colQuery = @colQuery + 
			CASE
				WHEN 
					@priorcolumn = ''
				THEN 
					' AND ' + SUBSTRING(@colsDynamic, 1, CHARINDEX('|', @colsDynamic)-1) + ' = ''' + SUBSTRING(@colsDynamic, CHARINDEX('|', @colsDynamic)+1, len( @colsDynamic) -CHARINDEX('|', @colsDynamic)) + ''' '
				WHEN 
					SUBSTRING(@colsDynamic, 1, CHARINDEX('|', @colsDynamic)-1) = @priorColumn
				THEN 
					' OR ' + SUBSTRING(@colsDynamic, 1, CHARINDEX('|', @colsDynamic)-1) + ' = ''' + SUBSTRING(@colsDynamic, CHARINDEX('|', @colsDynamic)+1, len( @colsDynamic) -CHARINDEX('|', @colsDynamic)) + ''') '
				ELSE
					' AND ' + SUBSTRING(@colsDynamic, 1, CHARINDEX('|', @colsDynamic)-1) + ' = ''' + SUBSTRING(@colsDynamic, CHARINDEX('|', @colsDynamic)+1, len( @colsDynamic) -CHARINDEX('|', @colsDynamic)) + ''') '
			END

	/*SELECT @colQuery*/

	SET @strQuery = 
	CASE 
		WHEN @colQuery is null OR @colQuery = ' ' OR @colQuery = ''
		THEN 
			'
		INSERT INTO ServiceOrder.dbo.TBL_ServiceOrderTasks (ServiceUnit, ServiceOrder,QueueID, ReportID,GroupID,PriorityValue,CreatedDate,UpdatedDate, DueDate, CreateEnterpriseID) 
						OUTPUT INSERTED.TaskID INTO #TBL_TaskEntered
					SELECT
						'''+@ServiceUnit+''' as ServiceUnit,
						'''+@ServiceOrder+''' as ServiceOrder,
						'+CAST(@QueueID as varchar)+' as QueueID,
						'+CAST(@ReportID as varchar)+' as ReportID,
						-1 as GroupID,
						'+CAST(@PriorityValue as varchar)+' as PriorityValue,
						GETDATE() as CreatedDate,
						GETDATE() as UpdatedDate,
						'''+@DueDate+''' as DueDate,
						'''+@AddUser+''' as addUser
		'
		ELSE 
		'
			INSERT INTO ServiceOrder.dbo.TBL_ServiceOrderTasks (ServiceUnit, ServiceOrder,QueueID, ReportID,GroupID,PriorityValue,CreatedDate,UpdatedDate, DueDate, CreateEnterpriseID) 
				OUTPUT INSERTED.TaskID INTO #TBL_TaskEntered
			SELECT DISTINCT
				'''+@ServiceUnit+''' as ServiceUnit,
				'''+@ServiceOrder+''' as ServiceOrder,
				'+CAST(@QueueID as varchar)+' as QueueID,
				'+CAST(@ReportID as varchar)+' as ReportID,
				a.GroupID,
				'+CAST(@PriorityValue as varchar)+' as PriorityValue,
				GETDATE() as CreatedDate,
				GETDATE() as UpdatedDate,
				'''+@DueDate+''' as DueDate,
				'''+@AddUser+''' as addUser
			FROM 
				ServiceOrder.dbo.TBL_ControlBuilder as c with (nolock)
				INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderGroupItems as g with (nolock) on c.ControlID = g.ControlID
				INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderGroup as grp with (nolock) on grp.GroupID = g.GroupID
				INNER JOIN ServiceOrder.INFORMATION_SCHEMA.COLUMNS as col with (nolock) on c.QueryColumn = col.Column_Name 
				INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderAssignment as a with (nolock)  on a.GroupID = g.GroupID
			WHERE 
					col.TABLE_NAME = N''VIEW_ServiceOrder''
					AND grp.IsEnabled = 1
					AND a.QueueID = '+CAST(@QueueID as varchar)+' 
					AND a.ReportID = '+CAST(@ReportID as varchar)+' 
					AND a.GroupID = '+CAST(@GroupID as varchar)+' 
					AND EXISTS (
						SELECT ServiceUnit 
						FROM ServiceOrder.dbo.VIEW_ServiceOrder as id with (nolock) 
						WHERE 
							(ServiceUnit ='''+@ServiceUnit+''' AND ServiceOrder = '''+@ServiceOrder+''') 
							'+@colQuery+'
					)
			'
		
	END	
	/*SELECT @strQuery*/
	EXEC(@strQuery)


	/*SELECT @@ROWCOUNT ' After execution'*/
	/*BREAK looking groups if a record was inserted*/
	IF (@@ROWCOUNT > 0) 
	BEGIN
		SET @RowCountHold = 1;
		BREAK;
	END
	/*If Group is null, set it to '' in order to ensure the iteration doesn't contantly recycle*/
	/*SELECT @groups = CASE WHEN @groups is null THEN '' ELSE @groups END*/
	SELECT @groups = SUBSTRING(@groups, CHARINDEX(',',@groups)+1,LEN(@groups))
	
	/*Ensure we inject the order if the script doesn't break and set the row count to 1*/
	SET @RowCountHold = 0;
	
END
/*SELECT @@ROWCOUNT ' After loop'*/
/*If no record was inserted in the looped groups, insert into the -1 group*/
IF @RowCountHold = 0
BEGIN
	/*SELECT 'INSERTING AFTER LOOP'*/
	INSERT INTO ServiceOrder.dbo.TBL_ServiceOrderTasks (ServiceUnit, ServiceOrder,QueueID, ReportID,GroupID,PriorityValue,CreatedDate,UpdatedDate, DueDate, CreateEnterpriseID) 
		OUTPUT INSERTED.TaskID INTO #TBL_TaskEntered
	SELECT
		@ServiceUnit as ServiceUnit,
		@ServiceOrder as ServiceOrder,
		CAST(@QueueID as varchar) as QueueID,
		CAST(@ReportID as varchar) as ReportID,
		-1 as GroupID,
		CAST(@PriorityValue as varchar) as PriorityValue,
		GETDATE() as CreatedDate,
		GETDATE() as UpdatedDate,
		@DueDate as DueDate,
		@AddUser as addUser
END

SELECT 
	@TaskID = TaskID 
FROM 
	#TBL_TaskEntered

/*Commented to check TaskID; NULL when not populated*/
/*SELECT @TaskID*/n

IF object_id('tempdb..#TBL_TaskEntered') IS NOT NULL
BEGIN
	DROP TABLE #TBL_TaskEntered;
END

END



GO
/****** Object:  StoredProcedure [dbo].[PROC_ServiceProEmail]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Preston Galvanek
-- Create date: 6/28/2016
-- Modified Date: 3/3/2017
-- Modified Date: 12/9/2017 - PGALVAN - Changed log from mod date to create date
-- Description: Email Report to Create User
-- =============================================
CREATE PROCEDURE [dbo].[PROC_ServiceProEmail]
@ReportID as int = NULL,
@QueueID as int = NULL,
@GroupID as int = NULL,
@Frequency as varchar(4) = NULL,
@ErrorCode varchar(150) = NULL OUTPUT
AS
BEGIN  
	DECLARE @EmailBodyHTML as varchar(MAX) ;
	DECLARE @Query as varchar(MAX);
	DECLARE @QueryTest as varchar(MAX);
	DECLARE @QueryTopResult as int = 0;
	DECLARE @Today as varchar(50) = CAST(GETDATE() as date);
	DECLARE @ReportName as varchar(250) ;
	DECLARE @QueueName as varchar(250) ;
	DECLARE @GroupName as varchar(250) ;
	DECLARE @QueryCount as int;
	DECLARE @ReportColor as varchar(8);
	DECLARE @Email as varchar(MAX);

	/*used for adding tabs to query results*/ 
	DECLARE @tab char(1) = CHAR(9) ;


	SELECT DISTINCT 
		@ReportName = ReportName,
		@QueueName = QueueName,
		@GroupName = GroupName,
		@ReportColor = COALESCE(ReportColor,'#2C2C2C'),
		@Query =
		CASE
			WHEN @QueueID = -1
			THEN
				CASE 
					WHEN CHARINDEX(',id.ServiceOrder,',r.QuerySelect) > 0
					THEN 
						REPLACE(
							CAST(r.QuerySelect as varchar(max)) 
							,',id.ServiceOrder,'
							,',''=HYPERLINK("https://inhome.searshc.com/serviceorder/detail.aspx?su=''+id.ServiceUnit+''&so=''+id.ServiceOrder+''","''+id.ServiceOrder+''")'' as ServiceOrder,'
						) 
					WHEN CHARINDEX('id.ServiceOrder,',r.QuerySelect) > 0
					THEN 
						REPLACE(
							CAST(r.QuerySelect as varchar(max)) 
							,'id.ServiceOrder,'
							,'''=HYPERLINK("https://inhome.searshc.com/serviceorder/detail.aspx?su=''+id.ServiceUnit+''&so=''+id.ServiceOrder+''","''+id.ServiceOrder+''")'' as ServiceOrder,'
						) 
					WHEN
					  RIGHT(querySelect,16) = ',id.ServiceOrder'
					THEN 
						SUBSTRING(r.QuerySelect,1,Len(r.querySelect) - 16)
						+ REPLACE(
							 CAST(Right(r.QuerySelect,16) as varchar(max)) 
							,',id.ServiceOrder'
							,',''=HYPERLINK("https://inhome.searshc.com/serviceorder/detail.aspx?su=''+id.ServiceUnit+''&so=''+id.ServiceOrder+''","''+id.ServiceOrder+''")'' as ServiceOrder'
						)
					WHEN
					  querySelect = 'id.ServiceOrder'
					THEN 
						REPLACE(
							 CAST(Right(r.QuerySelect,16) as varchar(max)) 
							,'id.ServiceOrder'
							,'''=HYPERLINK("https://inhome.searshc.com/serviceorder/detail.aspx?su=''+id.ServiceUnit+''&so=''+id.ServiceOrder+''","''+id.ServiceOrder+''")'' as ServiceOrder'
						)
					ELSE
						CAST(r.QuerySelect as varchar(max)) 
				END
				+ ' FROM  ' + CAST(r.QueryFrom as varchar(max)) 
				+ CASE 
					WHEN r.QueryWhere <> '' 
					THEN ' WHERE  ' + CAST(r.QueryWhere as varchar(max)) 
					ELSE '' 
				END +
				CASE 
					WHEN r.QueryGroupBy <> '' 
					THEN ' GROUP BY ' + CAST(r.QueryGroupBy as varchar(max))
					ELSE '' 
				END +
				CASE 
					WHEN r.QueryOrderBy <> '' 
					THEN ' ORDER BY ' + CAST(r.QueryOrderBy as varchar(max))
					ELSE '' 
				END 
			WHEN
				@ReportID = -1
			THEN 
				' t.ServiceUnit,'
				+ '''=HYPERLINK("https://inhome.searshc.com/serviceorder/detail.aspx?su=''+t.ServiceUnit+''&so=''+t.ServiceOrder+''","''+t.ServiceOrder+''")'' as ServiceOrder'
				+',t.OptionName,t.ActionFlag,REPLACE(REPLACE(t.Comment, CHAR(13), ''''), CHAR(10), '''') as Comment ,t.EnterpriseID,t.CreateDateTime,t.CreatedDate,t.CycleTime, t.TaskID, t.LogID ' 
				+' FROM '
				+ ' (
					SELECT
						t.ServiceUnit
						,t.ServiceOrder
						,t.TaskID
						,l.LogID
						,COALESCE(l.CreateEnterpriseID,t.UpdateEnterpriseID,t.CreateEnterpriseID) as EnterpriseID
						,t.CreatedDate
						,CONVERT(varchar,l.CreateDateTime,120) as CreateDateTime
						,CASE WHEN COALESCE(o.closed, 0) = 1 THEN DATEDIFF(day,t.CreatedDate,l.CreateDateTime) ELSE DATEDIFF(day,t.CreatedDate,GETDATE()) END as CycleTime
						,o.OptionName
						,o.Closed as ActionFlag
						,REPLACE(REPLACE(c.Comment, CHAR(13), ''''), CHAR(10), '''') as Comment 
					FROM 
						ServiceOrder.dbo.TBL_ServiceOrderTasks as t with (nolock) 
						LEFT OUTER JOIN ServiceOrder.dbo.TBL_ServiceOrderTasksLog as l with (nolock)  ON t.LogID = l.LogID
						LEFT OUTER JOIN ServiceOrder.dbo.TBL_ServiceOrderQueueOptions as o with (nolock) on o.OptionID = l.OptionID 
						LEFT OUTER JOIN ServiceOrder.dbo.TBL_Comments as c with (nolock) on c.OwnerSubID = l.logID AND c.OwnerType = ''ServiceProLogID''
					WHERE
						t.ReportID = '+CAST(@ReportID as varchar)+'
						AND t.QueueID = '+CAST(@QueueID as varchar)+'
						AND t.GroupID = '+CAST(@GroupID as varchar)+'
						AND (
							t.CreatedDate = CAST(dateadd(day,-1,getdate()) as date) 
							OR
							o.Closed = 0
							OR
							o.Closed IS NULL
							)

				) as t 
				ORDER BY
					t.ServiceUnit
					,t.ServiceOrder
				'
			ELSE
				CASE 
					WHEN CHARINDEX(',id.ServiceOrder,',r.QuerySelect) > 0
					THEN 
						REPLACE(
							CAST(r.QuerySelect as varchar(max)) 
							,',id.ServiceOrder,'
							,',''=HYPERLINK("https://inhome.searshc.com/serviceorder/detail.aspx?su=''+id.ServiceUnit+''&so=''+id.ServiceOrder+''","''+id.ServiceOrder+''")'' as ServiceOrder,'
						) 
					WHEN CHARINDEX('id.ServiceOrder,',r.QuerySelect) > 0
					THEN 
						REPLACE(
							CAST(r.QuerySelect as varchar(max)) 
							,'id.ServiceOrder,'
							,'''=HYPERLINK("https://inhome.searshc.com/serviceorder/detail.aspx?su=''+id.ServiceUnit+''&so=''+id.ServiceOrder+''","''+id.ServiceOrder+''")'' as ServiceOrder,'
						) 
					WHEN
					  RIGHT(querySelect,16) = ',id.ServiceOrder'
					THEN 
						SUBSTRING(r.QuerySelect,1,Len(r.querySelect) - 16)
						+ REPLACE(
							 CAST(Right(r.QuerySelect,16) as varchar(max)) 
							,',id.ServiceOrder'
							,',''=HYPERLINK("https://inhome.searshc.com/serviceorder/detail.aspx?su=''+id.ServiceUnit+''&so=''+id.ServiceOrder+''","''+id.ServiceOrder+''")'' as ServiceOrder'
						)
					WHEN
					  querySelect = 'id.ServiceOrder'
					THEN 
						REPLACE(
							 CAST(Right(r.QuerySelect,16) as varchar(max)) 
							,'id.ServiceOrder'
							,'''=HYPERLINK("https://inhome.searshc.com/serviceorder/detail.aspx?su=''+id.ServiceUnit+''&so=''+id.ServiceOrder+''","''+id.ServiceOrder+''")'' as ServiceOrder'
						)
					ELSE
						CAST(r.QuerySelect as varchar(max)) 
				END 
				+ ' ,COALESCE(l.CreateEnterpriseID,t.UpdateEnterpriseID,t.CreateEnterpriseID) as EnterpriseID
					,t.CreatedDate
					,CONVERT(varchar,l.CreateDateTime,120) as CreateDateTime
					,CASE WHEN COALESCE(o.closed, 0) = 1 THEN DATEDIFF(day,t.CreatedDate,l.CreateDateTime) ELSE DATEDIFF(day,t.CreatedDate,GETDATE()) END as CycleTime
					,o.OptionName
					,o.Closed as ActionFlag
					,REPLACE(REPLACE(c.Comment, CHAR(13), ''''), CHAR(10), '''') as Comment  ' 
				+ ' FROM  ' + CAST(r.QueryFrom as varchar(max)) + 
				+ ' INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderTasks as t with (nolock) on id.ServiceUnit = t.ServiceUnit AND id.ServiceOrder = t.ServiceOrder 
					LEFT OUTER JOIN ServiceOrder.dbo.TBL_ServiceOrderTasksLog as l with (nolock)  ON t.LogID = l.LogID
					LEFT OUTER JOIN ServiceOrder.dbo.TBL_ServiceOrderQueueOptions as o with (nolock) on o.OptionID = l.OptionID 
					LEFT OUTER JOIN ServiceOrder.dbo.TBL_Comments as c with (nolock) on c.OwnerSubID = l.logID AND c.OwnerType = ''ServiceProLogID''
				WHERE
					t.ReportID = '+CAST(@ReportID as varchar)+'
					AND t.QueueID = '+CAST(@QueueID as varchar)+'
					AND t.GroupID = '+CAST(@GroupID as varchar)+'
				'+
				CASE 
					WHEN r.QueryWhere <> '' 
					THEN ' AND  (' + CAST(r.QueryWhere as varchar(max)) + ' ) '
					ELSE '' 
				END +
				CASE 
					WHEN r.QueryGroupBy <> '' 
					THEN ' GROUP BY ' + CAST(r.QueryGroupBy as varchar(max))
							+ ' ,COALESCE(l.EnterpriseID,t.UpdateEnterpriseID,t.CreateEnterpriseID)
								,t.CreatedDate
								,CONVERT(varchar,l.CreateDateTime,120) as CreateDateTime
								,CASE WHEN COALESCE(o.closed, 0) = 1 THEN DATEDIFF(day,t.CreatedDate,l.ModifiedDateTime) ELSE DATEDIFF(day,t.CreatedDate,GETDATE()) END as CycleTime
								,o.OptionName
								,o.Closed as ActionFlag
								,c.Comment  ' 
					ELSE '' 
				END +
				CASE 
					WHEN r.QueryOrderBy <> '' 
					THEN ' ORDER BY ' + CAST(r.QueryOrderBy as varchar(max))
					ELSE '' 
				END 
			END,
		@QueryCount =ReportCount,
		@Email = 
				STUFF((		
					SELECT ';' + (SELECT DISTINCT e.Email)
					FROM (
						SELECT
							a.EnterpriseID,
							r.ReportID
						FROM 
							ServiceOrder.dbo.TBL_ServiceOrderAssignment as a with (nolock) 
							INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderReports as r with (nolock) on r.ReportID = a.ReportID
							INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderQueue as q with (nolock) on q.QueueID = a.QueueID  
							INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderGroup as g with (nolock) on g.GroupID = a.GroupID  
							INNER JOIN ServiceOrder.dbo.TBL_Schedule as s with (nolock) on s.OwnerID = a.AssignmentID 
						WHERE 
							r.ReportID = @ReportID
							AND q.QueueID = @QueueID
							AND g.GroupID = @GroupID
							AND s.Frequency = @Frequency
							AND s.OwnerType = 'AssignmentID'
							AND s.Status = 1
						GROUP BY
							a.EnterpriseID,
							r.ReportID
						) as allin
						INNER JOIN Employee.[dbo].[VIEW_Employee] as e with (nolock) ON allin.EnterpriseID = e.EnterpriseID
					WHERE
						allin.ReportID = r.ReportID
					FOR XML PATH('')			
					)			
				, 1, 1, ''	)
	FROM 
		ServiceOrder.dbo.TBL_ServiceOrderAssignment as a with (nolock)
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderReports as r with (nolock) ON a.ReportID = r.ReportID
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderQueue as q with (nolock) on q.QueueID = a.QueueID
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderGroup as g with (nolock) on g.GroupID = a.GroupID  
	WHERE 
		a.ReportID = @ReportID
		AND a.QueueID = @QueueID
		AND a.GroupID = @GroupID;


	IF @Email is null OR @QueryCount is null OR @ReportName is null OR @GroupName is null OR @ReportColor is null OR @Query is null
	BEGIN 
		UPDATE 
			s
		SET 
			LastExecutionDate = GETDATE()
		FROM
			ServiceOrder.dbo.TBL_ServiceOrderAssignment as a 
			INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderReports as r on r.ReportID = a.ReportID
			INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderQueue as q  on q.QueueID = a.QueueID
			INNER JOIN ServiceOrder.dbo.TBL_Schedule as s on s.OwnerID = a.AssignmentID 
		WHERE 
			s.OwnerType = 'AssignmentID'
			AND s.Status = 1
			AND s.Frequency = @Frequency
			AND q.IsEnabled = 1
			AND a.QueueID = @QueueID
			AND a.ReportID = @ReportID;
		return;
	END

		
	SET @QueryTest = 'SELECT TOP 1 ' + @Query
	SET @Query = 'SELECT TOP 50000 ' + @Query

	
	--EXEC (@QueryTest);
	DECLARE @BodyText as varchar(MAX);

	IF @@ROWCOUNT > 0
		BEGIN
			SET @QueryTopResult = 1;
			SET @BodyText = 'Your '+@Frequency+' report is attached. <br/> <br/> All emailed reports are limited to 50,000 records. '
		END
	ELSE
		BEGIN
			SET @QueryTopResult = 0;
			SET @BodyText = 'Your '+@Frequency+' report returned no results.'
		END
	

	DECLARE @EmailSubject as varchar(MAX)
	IF @ReportID = -1
		BEGIN
			SET @EmailSubject = 'ServicePro Queue Only - '+ @QueueName+ ' for '+ @GroupName +' (' + @Today + ')'
		END
	ELSE
		BEGIN
			SET @EmailSubject = 'ServicePro Report - '+ @ReportName+ ' for ' + @GroupName +' (' + @Today + ')'
		END

	DECLARE @HeaderText as varchar(MAX) = 'ServicePro Report'
	DECLARE @RecipientList as varchar(MAX) = @Email +';'

	DECLARE @AttachmentFileName as varchar(MAX)	
	IF @ReportID = -1
		BEGIN
			Set @AttachmentFileName = 'ServiceProQueueOnly_' +@QueueName+'_'+@Today + '.csv'
		END
	ELSE
		BEGIN
			Set @AttachmentFileName = 'ServiceProReport_' +@ReportName+'_'+@Today + '.csv'
		END
	


	/*Create the email body */
	SET @EmailBodyHTML = '

	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
	<html>

	<head>
				<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
	</head>

	<body, #body_style style="width:94%; padding:2%; border-style:solid; border-width:20px; border-color:#ffffff;">
                                    
				<div style="text-align:center; min-width:95.4%; max-width:1000px; font-size:16px; color:#ffffff; border-width:15px; background-color:'+@ReportColor+'; border-color:'+@ReportColor+'; border-radius:3px; border-style:solid; font-family:century gothic, arial;">
							<b style="letter-spacing:1px; font-size:28;">'+ @HeaderText + '</b>
							<br/>
							as reported on ' + @today +
                                                
				'</div>

				<div style="border-width:1px; border-color:#ffffff; border-style:solid;">
							<div style="border-width:1px; border-color:#e8e8e8; border-style:solid;">
										<div style="width:95.4%; font-size:16px; color:#505050; border-width:15px; background-color:#ffffff; border-color:#ffffff; border-style:solid; font-family:calibri, arial;">'

										+ @BodyText +

										'</div>
							</div>
				</div>
				<div style="width:95.4%; text-align:center; font-size:16px; color:#505050; border-width:15px; background-color:#ecf0f1; border-color:#ecf0f1; border-style:solid; font-family:calibri, arial;">
				
				To report a bug/defect click <a style="color:#2980b9; text-decoration:none;" href="https://inhome.searshc.com/nfd/requests/add.asp">HERE</a>, to request an enhancement or modification click <a style="color:#2980b9; text-decoration:none;" href="http://form.jotform.com/80183906278160">HERE</a>.


				</div>

	</body>
	</html>'
	--SELECT @Query
	IF @QueryTopResult = 1
		BEGIN	                    
			EXEC msdb.dbo.sp_send_dbmail
				@profile_name = 'ServicePro',
				@recipients =  @RecipientList,
				--@recipients =  'PGALVAN@searshc.com',
				@body = @EmailBodyHTML,
				@body_format = 'HTML',
				@query = @Query,
				@subject = @EmailSubject,
				@attach_query_result_as_file = 1,
				@query_result_width = 32767,
				@query_attachment_filename= @AttachmentFileName,
				@query_result_separator=@tab,
				@query_result_no_padding=1;
		END
	ELSE 
		BEGIN       
			EXEC msdb.dbo.sp_send_dbmail
				@profile_name = 'ServicePro',
				@recipients =  @RecipientList,
				--@recipients =  'PGALVAN@searshc.com',
				@body = @EmailBodyHTML,
				@body_format = 'HTML',
				@subject = @EmailSubject;
		END

	UPDATE 
		s
	SET 
		LastExecutionDate = GETDATE()
	FROM
		ServiceOrder.dbo.TBL_ServiceOrderAssignment as a 
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderReports as r on r.ReportID = a.ReportID
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderQueue as q  on q.QueueID = a.QueueID
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderGroup as g  on g.GroupID = a.GroupID
		INNER JOIN ServiceOrder.dbo.TBL_Schedule as s on s.OwnerID = a.AssignmentID 
	WHERE 
		s.OwnerType = 'AssignmentID'
		AND s.Status = 1
		AND s.Frequency = @Frequency
		AND q.IsEnabled = 1
		AND g.IsEnabled = 1
		AND a.QueueID = @QueueID
		AND a.ReportID = @ReportID
		AND a.GroupID = @GroupID;

END

GO
/****** Object:  StoredProcedure [dbo].[PROC_ServiceProEmail_Test]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Preston Galvanek
-- Create date: 6/28/2016
-- Modified Date: 3/3/2017
-- Modified Date: 12/9/2017 - PGALVAN - Changed log from mod date to create date
-- Description: Email Report to Create User
-- =============================================
CREATE PROCEDURE [dbo].[PROC_ServiceProEmail_Test]
@ReportID as int = NULL,
@QueueID as int = NULL,
@GroupID as int = NULL,
@Frequency as varchar(4) = NULL,
@ErrorCode varchar(150) = NULL OUTPUT
AS
BEGIN  
	DECLARE @EmailBodyHTML as varchar(MAX) ;
	DECLARE @Query as varchar(MAX);
	DECLARE @QueryTest as varchar(MAX);
	DECLARE @QueryTopResult as int = 0;
	DECLARE @Today as varchar(50) = CAST(GETDATE() as date);
	DECLARE @ReportName as varchar(250) ;
	DECLARE @QueueName as varchar(250) ;
	DECLARE @GroupName as varchar(250) ;
	DECLARE @QueryCount as int;
	DECLARE @ReportColor as varchar(8);
	DECLARE @Email as varchar(MAX);

	/*used for adding tabs to query results*/ 
	DECLARE @tab char(1) = CHAR(9) ;


	SELECT DISTINCT 
		@ReportName = ReportName,
		@QueueName = QueueName,
		@GroupName = GroupName,
		@ReportColor = COALESCE(ReportColor,'#2C2C2C'),
		@Query =
		CASE
			WHEN @QueueID = -1
			THEN
				CASE 
					WHEN CHARINDEX(',id.ServiceOrder,',r.QuerySelect) > 0
					THEN 
						REPLACE(
							CAST(r.QuerySelect as varchar(max)) 
							,',id.ServiceOrder,'
							,',''=HYPERLINK("https://inhome.searshc.com/serviceorder/detail.aspx?su=''+id.ServiceUnit+''&so=''+id.ServiceOrder+''","''+id.ServiceOrder+''")'' as ServiceOrder,'
						) 
					WHEN CHARINDEX('id.ServiceOrder,',r.QuerySelect) > 0
					THEN 
						REPLACE(
							CAST(r.QuerySelect as varchar(max)) 
							,'id.ServiceOrder,'
							,'''=HYPERLINK("https://inhome.searshc.com/serviceorder/detail.aspx?su=''+id.ServiceUnit+''&so=''+id.ServiceOrder+''","''+id.ServiceOrder+''")'' as ServiceOrder,'
						) 
					WHEN
					  RIGHT(querySelect,16) = ',id.ServiceOrder'
					THEN 
						SUBSTRING(r.QuerySelect,1,Len(r.querySelect) - 16)
						+ REPLACE(
							 CAST(Right(r.QuerySelect,16) as varchar(max)) 
							,',id.ServiceOrder'
							,',''=HYPERLINK("https://inhome.searshc.com/serviceorder/detail.aspx?su=''+id.ServiceUnit+''&so=''+id.ServiceOrder+''","''+id.ServiceOrder+''")'' as ServiceOrder'
						)
					WHEN
					  querySelect = 'id.ServiceOrder'
					THEN 
						REPLACE(
							 CAST(Right(r.QuerySelect,16) as varchar(max)) 
							,'id.ServiceOrder'
							,'''=HYPERLINK("https://inhome.searshc.com/serviceorder/detail.aspx?su=''+id.ServiceUnit+''&so=''+id.ServiceOrder+''","''+id.ServiceOrder+''")'' as ServiceOrder'
						)
					ELSE
						CAST(r.QuerySelect as varchar(max)) 
				END
				+ ' FROM  ' + CAST(r.QueryFrom as varchar(max)) 
				+ CASE 
					WHEN r.QueryWhere <> '' 
					THEN ' WHERE  ' + CAST(r.QueryWhere as varchar(max)) 
					ELSE '' 
				END +
				CASE 
					WHEN r.QueryGroupBy <> '' 
					THEN ' GROUP BY ' + CAST(r.QueryGroupBy as varchar(max))
					ELSE '' 
				END +
				CASE 
					WHEN r.QueryOrderBy <> '' 
					THEN ' ORDER BY ' + CAST(r.QueryOrderBy as varchar(max))
					ELSE '' 
				END 
			WHEN
				@ReportID = -1
			THEN 
				' t.ServiceUnit,'
				+ '''=HYPERLINK("https://inhome.searshc.com/serviceorder/detail.aspx?su=''+t.ServiceUnit+''&so=''+t.ServiceOrder+''","''+t.ServiceOrder+''")'' as ServiceOrder'
				+',t.OptionName,t.ActionFlag,REPLACE(REPLACE(t.Comment, CHAR(13), ''''), CHAR(10), '''') as Comment ,t.EnterpriseID,t.CreateDateTime,t.CreatedDate,t.CycleTime, t.TaskID, t.LogID ' 
				+' FROM '
				+ ' (
					SELECT
						t.ServiceUnit
						,t.ServiceOrder
						,t.TaskID
						,l.LogID
						,COALESCE(l.CreateEnterpriseID,t.UpdateEnterpriseID,t.CreateEnterpriseID) as EnterpriseID
						,t.CreatedDate
						,CONVERT(varchar,l.CreateDateTime,120) as CreateDateTime
						,CASE WHEN COALESCE(o.closed, 0) = 1 THEN DATEDIFF(day,t.CreatedDate,l.CreateDateTime) ELSE DATEDIFF(day,t.CreatedDate,GETDATE()) END as CycleTime
						,o.OptionName
						,o.Closed as ActionFlag
						,REPLACE(REPLACE(c.Comment, CHAR(13), ''''), CHAR(10), '''') as Comment 
					FROM 
						ServiceOrder.dbo.TBL_ServiceOrderTasks as t with (nolock) 
						LEFT OUTER JOIN ServiceOrder.dbo.TBL_ServiceOrderTasksLog as l with (nolock)  ON t.LogID = l.LogID
						LEFT OUTER JOIN ServiceOrder.dbo.TBL_ServiceOrderQueueOptions as o with (nolock) on o.OptionID = l.OptionID 
						LEFT OUTER JOIN ServiceOrder.dbo.TBL_Comments as c with (nolock) on c.OwnerSubID = l.logID AND c.OwnerType = ''ServiceProLogID''
					WHERE
						t.ReportID = '+CAST(@ReportID as varchar)+'
						AND t.QueueID = '+CAST(@QueueID as varchar)+'
						AND t.GroupID = '+CAST(@GroupID as varchar)+'
						AND (
							t.CreatedDate = CAST(dateadd(day,-1,getdate()) as date) 
							OR
							o.Closed = 0
							OR
							o.Closed IS NULL
							)

				) as t 
				ORDER BY
					t.ServiceUnit
					,t.ServiceOrder
				'
			ELSE
				CASE 
					WHEN CHARINDEX(',id.ServiceOrder,',r.QuerySelect) > 0
					THEN 
						REPLACE(
							CAST(r.QuerySelect as varchar(max)) 
							,',id.ServiceOrder,'
							,',''=HYPERLINK("https://inhome.searshc.com/serviceorder/detail.aspx?su=''+id.ServiceUnit+''&so=''+id.ServiceOrder+''","''+id.ServiceOrder+''")'' as ServiceOrder,'
						) 
					WHEN CHARINDEX('id.ServiceOrder,',r.QuerySelect) > 0
					THEN 
						REPLACE(
							CAST(r.QuerySelect as varchar(max)) 
							,'id.ServiceOrder,'
							,'''=HYPERLINK("https://inhome.searshc.com/serviceorder/detail.aspx?su=''+id.ServiceUnit+''&so=''+id.ServiceOrder+''","''+id.ServiceOrder+''")'' as ServiceOrder,'
						) 
					WHEN
					  RIGHT(querySelect,16) = ',id.ServiceOrder'
					THEN 
						SUBSTRING(r.QuerySelect,1,Len(r.querySelect) - 16)
						+ REPLACE(
							 CAST(Right(r.QuerySelect,16) as varchar(max)) 
							,',id.ServiceOrder'
							,',''=HYPERLINK("https://inhome.searshc.com/serviceorder/detail.aspx?su=''+id.ServiceUnit+''&so=''+id.ServiceOrder+''","''+id.ServiceOrder+''")'' as ServiceOrder'
						)
					WHEN
					  querySelect = 'id.ServiceOrder'
					THEN 
						REPLACE(
							 CAST(Right(r.QuerySelect,16) as varchar(max)) 
							,'id.ServiceOrder'
							,'''=HYPERLINK("https://inhome.searshc.com/serviceorder/detail.aspx?su=''+id.ServiceUnit+''&so=''+id.ServiceOrder+''","''+id.ServiceOrder+''")'' as ServiceOrder'
						)
					ELSE
						CAST(r.QuerySelect as varchar(max)) 
				END 
				+ ' ,COALESCE(l.CreateEnterpriseID,t.UpdateEnterpriseID,t.CreateEnterpriseID) as EnterpriseID
					,t.CreatedDate
					,CONVERT(varchar,l.CreateDateTime,120) as CreateDateTime
					,CASE WHEN COALESCE(o.closed, 0) = 1 THEN DATEDIFF(day,t.CreatedDate,l.CreateDateTime) ELSE DATEDIFF(day,t.CreatedDate,GETDATE()) END as CycleTime
					,o.OptionName
					,o.Closed as ActionFlag
					,REPLACE(REPLACE(c.Comment, CHAR(13), ''''), CHAR(10), '''') as Comment  ' 
				+ ' FROM  ' + CAST(r.QueryFrom as varchar(max)) + 
				+ ' INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderTasks as t with (nolock) on id.ServiceUnit = t.ServiceUnit AND id.ServiceOrder = t.ServiceOrder 
					LEFT OUTER JOIN ServiceOrder.dbo.TBL_ServiceOrderTasksLog as l with (nolock)  ON t.LogID = l.LogID
					LEFT OUTER JOIN ServiceOrder.dbo.TBL_ServiceOrderQueueOptions as o with (nolock) on o.OptionID = l.OptionID 
					LEFT OUTER JOIN ServiceOrder.dbo.TBL_Comments as c with (nolock) on c.OwnerSubID = l.logID AND c.OwnerType = ''ServiceProLogID''
				WHERE
					t.ReportID = '+CAST(@ReportID as varchar)+'
					AND t.QueueID = '+CAST(@QueueID as varchar)+'
					AND t.GroupID = '+CAST(@GroupID as varchar)+'
				'+
				CASE 
					WHEN r.QueryWhere <> '' 
					THEN ' AND  (' + CAST(r.QueryWhere as varchar(max)) + ' ) '
					ELSE '' 
				END +
				CASE 
					WHEN r.QueryGroupBy <> '' 
					THEN ' GROUP BY ' + CAST(r.QueryGroupBy as varchar(max))
							+ ' ,COALESCE(l.EnterpriseID,t.UpdateEnterpriseID,t.CreateEnterpriseID)
								,t.CreatedDate
								,CONVERT(varchar,l.CreateDateTime,120) as CreateDateTime
								,CASE WHEN COALESCE(o.closed, 0) = 1 THEN DATEDIFF(day,t.CreatedDate,l.ModifiedDateTime) ELSE DATEDIFF(day,t.CreatedDate,GETDATE()) END as CycleTime
								,o.OptionName
								,o.Closed as ActionFlag
								,c.Comment  ' 
					ELSE '' 
				END +
				CASE 
					WHEN r.QueryOrderBy <> '' 
					THEN ' ORDER BY ' + CAST(r.QueryOrderBy as varchar(max))
					ELSE '' 
				END 
			END,
		@QueryCount =ReportCount,
		@Email = 
				STUFF((		
					SELECT ';' + (SELECT DISTINCT e.Email)
					FROM (
						SELECT
							a.EnterpriseID,
							r.ReportID
						FROM 
							ServiceOrder.dbo.TBL_ServiceOrderAssignment as a with (nolock) 
							INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderReports as r with (nolock) on r.ReportID = a.ReportID
							INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderQueue as q with (nolock) on q.QueueID = a.QueueID  
							INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderGroup as g with (nolock) on g.GroupID = a.GroupID  
							INNER JOIN ServiceOrder.dbo.TBL_Schedule as s with (nolock) on s.OwnerID = a.AssignmentID 
						WHERE 
							r.ReportID = @ReportID
							AND q.QueueID = @QueueID
							AND g.GroupID = @GroupID
							AND s.Frequency = @Frequency
							AND s.OwnerType = 'AssignmentID'
							AND s.Status = 1
						GROUP BY
							a.EnterpriseID,
							r.ReportID
						) as allin
						INNER JOIN Employee.[dbo].[VIEW_Employee] as e with (nolock) ON allin.EnterpriseID = e.EnterpriseID
					WHERE
						allin.ReportID = r.ReportID
					FOR XML PATH('')			
					)			
				, 1, 1, ''	)
	FROM 
		ServiceOrder.dbo.TBL_ServiceOrderAssignment as a with (nolock)
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderReports as r with (nolock) ON a.ReportID = r.ReportID
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderQueue as q with (nolock) on q.QueueID = a.QueueID
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderGroup as g with (nolock) on g.GroupID = a.GroupID  
	WHERE 
		a.ReportID = @ReportID
		AND a.QueueID = @QueueID
		AND a.GroupID = @GroupID;


	IF @Email is null OR @QueryCount is null OR @ReportName is null OR @GroupName is null OR @ReportColor is null OR @Query is null
	BEGIN 
		UPDATE 
			s
		SET 
			LastExecutionDate = GETDATE()
		FROM
			ServiceOrder.dbo.TBL_ServiceOrderAssignment as a 
			INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderReports as r on r.ReportID = a.ReportID
			INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderQueue as q  on q.QueueID = a.QueueID
			INNER JOIN ServiceOrder.dbo.TBL_Schedule as s on s.OwnerID = a.AssignmentID 
		WHERE 
			s.OwnerType = 'AssignmentID'
			AND s.Status = 1
			AND s.Frequency = @Frequency
			AND q.IsEnabled = 1
			AND a.QueueID = @QueueID
			AND a.ReportID = @ReportID;
		return;
	END

		
	SET @QueryTest = 'SELECT TOP 1 ' + @Query
	SET @Query = 'SELECT TOP 50000 ' + @Query

	
	--EXEC (@QueryTest);
	DECLARE @BodyText as varchar(MAX);

	IF @@ROWCOUNT > 0
		BEGIN
			SET @QueryTopResult = 1;
			SET @BodyText = 'Your '+@Frequency+' report is attached. <br/> <br/> All emailed reports are limited to 50,000 records. '
		END
	ELSE
		BEGIN
			SET @QueryTopResult = 0;
			SET @BodyText = 'Your '+@Frequency+' report returned no results.'
		END
	

	DECLARE @EmailSubject as varchar(MAX)
	IF @ReportID = -1
		BEGIN
			SET @EmailSubject = 'ServicePro Queue Only - '+ @QueueName+ ' for '+ @GroupName +' (' + @Today + ')'
		END
	ELSE
		BEGIN
			SET @EmailSubject = 'ServicePro Report - '+ @ReportName+ ' for ' + @GroupName +' (' + @Today + ')'
		END

	DECLARE @HeaderText as varchar(MAX) = 'ServicePro Report'
	DECLARE @RecipientList as varchar(MAX) = @Email +';'

	DECLARE @AttachmentFileName as varchar(MAX)	
	IF @ReportID = -1
		BEGIN
			Set @AttachmentFileName = 'ServiceProQueueOnly_' +@QueueName+'_'+@Today + '.csv'
		END
	ELSE
		BEGIN
			Set @AttachmentFileName = 'ServiceProReport_' +@ReportName+'_'+@Today + '.csv'
		END
	


	/*Create the email body */
	SET @EmailBodyHTML = '

	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
	<html>

	<head>
				<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
	</head>

	<body, #body_style style="width:94%; padding:2%; border-style:solid; border-width:20px; border-color:#ffffff;">
                                    
				<div style="text-align:center; min-width:95.4%; max-width:1000px; font-size:16px; color:#ffffff; border-width:15px; background-color:'+@ReportColor+'; border-color:'+@ReportColor+'; border-radius:3px; border-style:solid; font-family:century gothic, arial;">
							<b style="letter-spacing:1px; font-size:28;">'+ @HeaderText + '</b>
							<br/>
							as reported on ' + @today +
                                                
				'</div>

				<div style="border-width:1px; border-color:#ffffff; border-style:solid;">
							<div style="border-width:1px; border-color:#e8e8e8; border-style:solid;">
										<div style="width:95.4%; font-size:16px; color:#505050; border-width:15px; background-color:#ffffff; border-color:#ffffff; border-style:solid; font-family:calibri, arial;">'

										+ @BodyText +

										'</div>
							</div>
				</div>
				<div style="width:95.4%; text-align:center; font-size:16px; color:#505050; border-width:15px; background-color:#ecf0f1; border-color:#ecf0f1; border-style:solid; font-family:calibri, arial;">
				
				To report a bug/defect click <a style="color:#2980b9; text-decoration:none;" href="https://inhome.searshc.com/nfd/requests/add.asp">HERE</a>, to request an enhancement or modification click <a style="color:#2980b9; text-decoration:none;" href="http://form.jotform.com/80183906278160">HERE</a>.


				</div>

	</body>
	</html>'
	--SELECT @Query
	IF @QueryTopResult = 1
		BEGIN	                    
			EXEC msdb.dbo.sp_send_dbmail
				@profile_name = 'ServicePro',
				--@recipients =  @RecipientList,
				@recipients =  'PGALVAN@searshc.com',
				@body = @EmailBodyHTML,
				@body_format = 'HTML',
				@query = @Query,
				@subject = @EmailSubject,
				@attach_query_result_as_file = 1,
				@query_result_width = 32767,
				@query_attachment_filename= @AttachmentFileName,
				@query_result_separator=@tab,
				@query_result_no_padding=1;
		END
	ELSE 
		BEGIN       
			EXEC msdb.dbo.sp_send_dbmail
				@profile_name = 'ServicePro',
				--@recipients =  @RecipientList,
				@recipients =  'PGALVAN@searshc.com',
				@body = @EmailBodyHTML,
				@body_format = 'HTML',
				@subject = @EmailSubject;
		END

	UPDATE 
		s
	SET 
		LastExecutionDate = GETDATE()
	FROM
		ServiceOrder.dbo.TBL_ServiceOrderAssignment as a 
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderReports as r on r.ReportID = a.ReportID
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderQueue as q  on q.QueueID = a.QueueID
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderGroup as g  on g.GroupID = a.GroupID
		INNER JOIN ServiceOrder.dbo.TBL_Schedule as s on s.OwnerID = a.AssignmentID 
	WHERE 
		s.OwnerType = 'AssignmentID'
		AND s.Status = 1
		AND s.Frequency = @Frequency
		AND q.IsEnabled = 1
		AND g.IsEnabled = 1
		AND a.QueueID = @QueueID
		AND a.ReportID = @ReportID
		AND a.GroupID = @GroupID;

END

GO
/****** Object:  StoredProcedure [dbo].[PROC_ServiceProEmailReminder]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		Preston Galvanek
-- Create date: 11/29/2016
-- Modified Date: 11/29/2016
-- Description: Email Reminders to Create User
-- =============================================
CREATE PROCEDURE [dbo].[PROC_ServiceProEmailReminder]
@ReminderID int = NULL,
@ErrorCode varchar(150) = NULL OUTPUT
AS
BEGIN  
	DECLARE @EmailBodyHTML as varchar(MAX) ;
	DECLARE @Today as varchar(50) = CAST(GETDATE() as date);
	DECLARE @ServiceUnit as varchar(7);
	DECLARE @ServiceOrder as varchar(8);
	DECLARE @Comment as varchar(max);
	DECLARE @Email as varchar(250);

	SELECT DISTINCT
		@ServiceUnit =r.ServiceUnit,
		@ServiceOrder = r.ServiceOrder,
		@Comment = c.Comment,
		@Email = e.Email
	FROM 
		ServiceOrder.dbo.TBL_Reminders as r with (nolock)
		INNER JOIN Employee.dbo.TBL_Employee_New as e with (nolock) on r.ModifiedEnterpriseID = e.EnterpriseID
        LEFT OUTER JOIN ServiceOrder.dbo.TBL_Comments as c with (nolock) on c.ServiceUnit = r.ServiceUnit and c.ServiceOrder = r.ServiceOrder and c.OwnerType = 'ServiceProReminderID'
	WHERE 
		r.ReminderID = @ReminderID
		AND LastReminderDateTime is NULL
		
	IF @Email = ''
	BEGIN
		return;
	END

	DECLARE @EmailSubject as varchar(MAX)='ServicePro Reminder: '+ @ServiceUnit+ ' - '+@ServiceOrder+' (' + @Today + ')'
	DECLARE @HeaderText as varchar(MAX) = 'ServicePro Reminder'
	DECLARE @BodyText as varchar(MAX) = 'This is a reminder from Service Pro to review the service order: <a href="http://inhome.searshc.com/serviceorder/detail.aspx?su='+ @ServiceUnit+ '&so='+@ServiceOrder+'"> '+ @ServiceUnit+@ServiceOrder+'</a>. <br/> <br/>  Comments: '+@Comment
	DECLARE @RecipientList as varchar(MAX) = @Email +';'


	/*Create the email body */
	SET @EmailBodyHTML = '

	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
	<html>

	<head>
				<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
	</head>

	<body, #body_style style="width:94%; padding:2%; border-style:solid; border-width:20px; border-color:#ffffff;">
                                    
				<div style="text-align:center; min-width:95.4%; max-width:1000px; font-size:16px; color:#ffffff; border-width:15px; background-color:#2c2c2c; border-color:#2c2c2c; border-radius:3px; border-style:solid; font-family:century gothic, arial;">
							<b style="letter-spacing:1px; font-size:28;">'+ @HeaderText + '</b>
							<br/>
							as reported on ' + @today +
                                                
				'</div>

				<div style="border-width:1px; border-color:#ffffff; border-style:solid;">
							<div style="border-width:1px; border-color:#e8e8e8; border-style:solid;">
										<div style="width:95.4%; font-size:16px; color:#505050; border-width:15px; background-color:#ffffff; border-color:#ffffff; border-style:solid; font-family:calibri, arial;">'

										+ @BodyText +

										'</div>
							</div>
				</div>
				<div style="width:95.4%; text-align:center; font-size:16px; color:#505050; border-width:15px; background-color:#ecf0f1; border-color:#ecf0f1; border-style:solid; font-family:calibri, arial;">
				
				To report a bug/defect click <a style="color:#2980b9; text-decoration:none;" href="http://inhome.searshc.com/nfd/requests/add.asp">HERE</a>, to request an enhancement or modification click <a style="color:#2980b9; text-decoration:none;" href="https://form.jotform.com/80183906278160">HERE</a>.


				</div>

	</body>
	</html>'
     
	IF @ServiceOrder is not null
		BEGIN                   
			EXEC msdb.dbo.sp_send_dbmail
				@profile_name = 'ServicePro',
				@recipients =  @Email,
				/*@recipients =  'PGALVAN@searshc.com',*/
				@body = @EmailBodyHTML,
				@body_format = 'HTML',
				@subject = @EmailSubject;


			UPDATE 
				ServiceOrder.dbo.TBL_Reminders
			SET 
				LastReminderDateTime = GETDATE()
			WHERE 
				ReminderID = @ReminderID;
		END
END




GO
/****** Object:  StoredProcedure [dbo].[PROC_ServiceProEmailTask]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Preston Galvanek
-- Create date: 5/1/2017
-- Modified Date: 5/1/2017
-- Modified Date: 12/9/2017 - pgalvan - changed log to create date and enterprise id from mod date/id
-- Description: Email new tasks entered through ad hoc detail page of service pro
-- =============================================
CREATE PROCEDURE [dbo].[PROC_ServiceProEmailTask]
@TaskID int = NULL,
@ActionName varchar(250) = NULL,
@ErrorCode varchar(150) = NULL OUTPUT
AS
BEGIN  
	DECLARE @EmailBodyHTML as varchar(max) ;
	DECLARE @Today as varchar(50) = CAST(GETDATE() as date);
	DECLARE @CreateEnterpriseID as varchar(50);
	DECLARE @ServiceUnit as varchar(7);
	DECLARE @ServiceOrder as varchar(8);
	DECLARE @Comment as varchar(max);
	DECLARE @ReportName as varchar(max);
	DECLARE @GroupName as varchar(max);
	DECLARE @ReportColor as varchar(max);
	DECLARE @QueueName as varchar(max);
	DECLARE @Email as varchar(max);/*was 250*/

	SELECT
		@Email = 
				STUFF((		
					SELECT ';' + (SELECT DISTINCT e.Email)
					FROM (
						SELECT
							a.EnterpriseID,
							r.ReportID
						FROM 
							ServiceOrder.dbo.TBL_ServiceOrderAssignment as a with (nolock) 
							INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderReports as r with (nolock) on r.ReportID = a.ReportID
							INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderQueue as q with (nolock) on q.QueueID = a.QueueID
							INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderGroup as g with (nolock) on g.GroupID = a.GroupID
							INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderTasks as t with (nolock) on  a.QueueID = t.QueueID AND a.ReportID = t.ReportID  AND a.GroupID = t.GroupID  
							INNER JOIN ServiceOrder.dbo.TBL_Schedule as s with (nolock) on s.OwnerID = a.AssignmentID 
						WHERE 
							t.TaskID = @TaskID
							AND s.OwnerType = 'AssignmentID'
							AND s.Status = 1
							AND s.Frequency = 'SS'
						GROUP BY
							a.EnterpriseID,
							r.REportID
						) as allin
						INNER JOIN Employee.dbo.TBL_Employee as e with (nolock) ON allin.EnterpriseID = e.EnterpriseID
					FOR XML PATH('')			
					)			
				, 1, 1, ''	)

	SELECT DISTINCT
		@ServiceUnit = t.ServiceUnit,
		@ServiceOrder = t.ServiceOrder,
		@CreateEnterpriseID = t.CreateEnterpriseID,
		@Comment = c.Comment,
		@QueueName = q.QueueName,
		@ReportName = r.ReportName,
		@GroupName = g.GroupName,
		@ReportColor = COALESCE(r.ReportColor,'#2C2C2C')
	FROM 
		ServiceOrder.dbo.TBL_ServiceOrderTasks as t with (nolock)
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderQueue as q with ( nolock) on t.QueueID = q.QueueID   
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderReports as r with ( nolock) on t.ReportID = r.ReportID  
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderGroup as g with (nolock) on t.GroupID = g.GroupID  
		LEFT JOIN (  
			SELECT   
				l.LogID,
				l.TaskID,  
				l.OptionID,  
				l.CreateDateTime,  
				l.CreateEnterpriseID  
			FROM   
				ServiceOrder.dbo.TBL_ServiceOrderTasksLog as l with (nolock)   
				INNER JOIN (    
						SELECT    
							TaskID,   
							MAX(CreateDateTime) as CreateDateTime   
						FROM   
							ServiceOrder.dbo.TBL_ServiceOrderTasksLog as l with (nolock)    
						GROUP BY    
							TaskID   
				) as lm on l.TaskID = lm.TaskID and l.CreateDateTime = lm.CreateDateTime 
		) as lm ON lm.TaskID = t.TaskID   
		LEFT JOIN ServiceOrder.dbo.TBL_Comments as c with (nolock)  ON c.OwnerSubID = lm.LogID AND c.OwnerType = 'ServiceProLogID'
	WHERE 
		t.TaskID = @TaskID

    IF @Email is null OR @ReportName is null OR @QueueName is null OR @GroupName is null OR @CreateEnterpriseID is null
	BEGIN 
		return;
	END


	DECLARE @EmailSubject as varchar(MAX)='ServicePro New Task - '+@QueueName+': '+ @ServiceUnit+ ' - '+@ServiceOrder+' (' + @Today + ')'
	DECLARE @HeaderText as varchar(MAX) = 'ServicePro New Task'
	DECLARE @BodyText as varchar(MAX) = 'This is a notice from Service Pro: The order <a href="http://inhome.searshc.com/serviceorder/detail.aspx?su='+ @ServiceUnit+ '&so='+@ServiceOrder+'"> '+ @ServiceUnit+@ServiceOrder+'</a> has been added to the below dashboard. <br /> <br /> <b>Queue</b>: '+@QueueName+' <br />  <b>Report</b>: '+@ReportName+' <br />  <b>Group</b>: '+@GroupName
	
	IF @ActionName != '' AND @ActionName is not null 
	BEGIN
		SET @BodyText = @BodyText +' <br/>  <b>Action</b>: '+@ActionName
	END
	
	SET @BodyText = @BodyText +' <br/>  <b>Comment</b>: '+@Comment
	SET @BodyText = @BodyText +' <br/>  <b>Creator</b>: ' +@CreateEnterpriseID
	DECLARE @RecipientList as varchar(MAX) = @Email +';'


	/*Create the email body */
	SET @EmailBodyHTML = '

	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
	<html>

	<head>
				<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
	</head>

	<body, #body_style style="width:94%; padding:2%; border-style:solid; border-width:20px; border-color:#ffffff;">
				   <div style="text-align:center; min-width:95.4%; max-width:1000px; font-size:16px; color:#ffffff; border-width:15px; background-color:'+@ReportColor+'; border-color:'+@ReportColor+'; border-radius:3px; border-style:solid; font-family:century gothic, arial;">
						<b style="letter-spacing:1px; font-size:28;">'+ @HeaderText + '</b>
							<br/>
							as reported on ' + @today +
                                                
				'</div>

				<div style="border-width:1px; border-color:#ffffff; border-style:solid;">
							<div style="border-width:1px; border-color:#e8e8e8; border-style:solid;">
										<div style="width:95.4%; font-size:16px; color:#505050; border-width:15px; background-color:#ffffff; border-color:#ffffff; border-style:solid; font-family:calibri, arial;">'

										+ @BodyText +

										'</div>
							</div>
				</div>
				<div style="width:95.4%; text-align:center; font-size:16px; color:#505050; border-width:15px; background-color:#ecf0f1; border-color:#ecf0f1; border-style:solid; font-family:calibri, arial;">
				
				To report a bug/defect click <a style="color:#2980b9; text-decoration:none;" href="http://inhome.searshc.com/nfd/requests/add.asp">HERE</a>, to request an enhancement or modification click <a style="color:#2980b9; text-decoration:none;" href="https://form.jotform.com/80183906278160">HERE</a>.

				</div>

	</body>
	</html>'
     
	IF @ServiceOrder is not null
		BEGIN                   
			EXEC msdb.dbo.sp_send_dbmail
				@profile_name = 'ServicePro',
				@recipients =  @Email,
				/*@recipients =  'PGALVAN@searshc.com',*/
				@body = @EmailBodyHTML,
				@body_format = 'HTML',
				@subject = @EmailSubject;

		END
END




GO
/****** Object:  StoredProcedure [dbo].[PROC_ServiceProQueryBuilder]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Preston Galvanek
-- Create date: 10/21/2015
-- Modified Date: 6/13/2016
-- Description:	Build Query String
-- =============================================
CREATE PROCEDURE [dbo].[PROC_ServiceProQueryBuilder]
@reportID as int = NULL,
@querySelect as varchar(max) = NULL OUTPUT,
@queryFrom as varchar(max) = NULL OUTPUT,
@queryWhere as varchar(max) = NULL OUTPUT,
@queryGroupBy as varchar(max) = NULL OUTPUT,
@queryOrderBy as varchar(max) = NULL OUTPUT
AS
BEGIN  
	SELECT	
	/*SELECT Clause*/
	@querySelect = 
	CASE 
		WHEN funct is null
		THEN 
			CASE
				WHEN ServiceUnitValues is null OR selectC is null
				THEN 'id.ServiceUnit,'
				ELSE ''
			END
		ELSE ''
	END +
	CASE 
		WHEN funct is null
		THEN 
			CASE
				WHEN ServiceOrderValues is null AND selectC is not null /*20170331 added AND selectC is not null*/
				THEN 'id.ServiceOrder,'
				WHEN selectC is null
				THEN 'id.ServiceOrder'
				ELSE ''
			END
		ELSE ''
	END +
	CASE
		WHEN selectC is not null AND selectC <> ''
		THEN selectC 
		ELSE ''
	END 
	+
	CASE 
		WHEN funct is not null 
		THEN
			CASE 
				WHEN selectC is not null AND selectC <> ''
				THEN ',' + funct
				ELSE funct
			END
		ELSE ''
	END
	,@queryFrom =  fromC 
	,@queryWhere = 
	CASE
		WHEN whereC is not null
		THEN REPLACE(REPLACE(whereC,'&gt;','>'),'&lt;','<')
		ELSE ''
	END 
	,@queryGroupBy = 
	CASE 
		WHEN funct is not null 
		THEN	
			CASE 
				WHEN groupbyc is not null AND groupbyc <> ''
				THEN groupbyc
				ELSE ''
			END
		ELSE ''
	END	
FROM (
	SELECT DISTINCT
		selectC = STUFF((		
				SELECT TOP 100 PERCENT
					COALESCE(sc.qc,wc.qc)
				FROM (
					SELECT  
							',' + 
							CASE 
								WHEN 
									QueryColumn = ''
								THEN 
									''
								WHEN 
									ControlType in ('date', 'getdate') 
								THEN 
									'CAST(' +'id.' + QueryColumn +' as varchar(111)) as ''' + QueryColumn +''''
								ELSE
									'id.' + QueryColumn
							END as qc,
							cbs.SortOrder
						FROM 
							ServiceOrder.dbo.TBL_QueryBuilder as qbs with (nolock)
							INNER JOIN ServiceOrder.dbo.TBL_ControlBuilder as cbs with (nolock) on qbs.ControlID = cbs.ControlID
						WHERE 
							qbs.ReportID = @ReportID
							AND cbs.ControlSQLClause = 'SELECT'
							AND qbs.ReportID = qb.ReportID
					) as sc
					FULL OUTER JOIN (
						SELECT
							CASE 
								WHEN 
									ControlType not in ('date', 'getdate')
								THEN 
									/*WHEN 
											ControlName = 'SpecialInstructionsDescOneTwoExcept' OR ControlName = 'SpecialInstructionsDescOneTwo'
										THEN
											',id.SpecialInstructionsDescOne,id.SpecialInstructionsDescTwo '
										WHEN 
											ControlName = 'SpecialInstructionsAndServiceRequest'
										THEN
											',id.SpecialInstructionsDescOne,id.SpecialInstructionsDescTwo,id.ServiceRequestDescription '
										WHEN 
											ControlName = 'LastAttemptTechComment1&2Except' OR ControlName = 'LastAttemptTechComment1&2'
										THEN
											',id.LastAttemptTechComment1,id.LastAttemptTechComment2 '
											*/
										CASE 
											WHEN 
												ControlName = 'SpecialInstructionsDescOneTwoExcept' OR ControlName = 'SpecialInstructionsDescOneTwo' 
												OR ControlName = 'SpecInst_SvcReq_TechComm_Except'	OR ControlName = 'SpecInst_SvcReq_TechComm' 
												OR ControlName = 'LastAttemptTechComment1&2Except'	OR ControlName = 'LastAttemptTechComment1&2'
											THEN 
												''
										ELSE
											',' + 'id.' + QueryColumn
									END
								ELSE
									''
							END as qc,
							/*Above CASE statement is replacing the below CASE statement, i am removing the addition of the WHERE clause when the chosen column is a date.
							',' +
							CASE
								WHEN 
									ControlType in ('date', 'getdate')
								THEN 
									'CAST(' +'id.' + QueryColumn +' as varchar(111)) as ''' + QueryColumn +''''
								
								ELSE
									'id.' + QueryColumn
							END  as qc,
							*/
							cbs.SortOrder
						FROM 
							ServiceOrder.dbo.TBL_QueryBuilder as qbs with (nolock)
							INNER JOIN ServiceOrder.dbo.TBL_ControlBuilder as cbs with (nolock) on qbs.ControlID = cbs.ControlID
						WHERE 
							qbs.ReportID = @ReportID
							AND cbs.ControlSQLClause = 'WHERE'
							AND qbs.ReportID = qb.ReportID
					) as wc ON wc.qc = sc.qc
					
				ORDER BY 
					COALESCE(sc.SortOrder,wc.SortOrder)
				FOR XML PATH('')
				), 1, 1, ''	)
		,
		whereC = STUFF((
				SELECT TOP 100 PERCENT
					CASE 
						/*Set Dynamic Date*/
						WHEN 
							ControlType = 'getdate' AND  RIGHT(ControlValue,1) = '|'
						THEN 
							' AND ' + ' id.' +  QueryColumn + ' >= CAST(DATEADD(day,'+ Replace(ControlValue,'|','')+ ',GETDATE()) as Date) '
						WHEN 
							ControlType = 'getdate' AND LEFT(ControlValue,1) = '|'
						THEN 
							' AND ' +  ' id.' + QueryColumn + ' <= CAST(DATEADD(day,'+ Replace(ControlValue,'|','')+ ',GETDATE()) as Date)'
						WHEN 
							ControlType = 'getdate' AND RIGHT(ControlValue,1) <> '|' AND LEFT(ControlValue,1) <> '|'
						THEN
							' AND ' +  ' id.' + QueryColumn + ' between  CAST(DATEADD(day,CAST(SUBSTRING('''+ControlValue+''',1,CHARINDEX(''|'','''+ControlValue+''')-1) as int),GETDATE()) as Date)	AND CAST(DATEADD(day,CAST(SUBSTRING('''+ControlValue+''',CHARINDEX(''|'','''+ControlValue+''')+1,len('''+ControlValue+''')) as int),GETDATE()) as Date) '
						/*Set Date WHERE clause*/
						WHEN 
							ControlType = 'date' AND  RIGHT(ControlValue,1) = '|'
						THEN 
							' AND ' +  ' id.' + QueryColumn + ' >= '''+ Replace(ControlValue,'|','')+ ''''
						WHEN 
							ControlType = 'date' AND LEFT(ControlValue,1) = '|'
						THEN 
							' AND ' +  ' id.' + QueryColumn + ' <= '''+ Replace(ControlValue,'|','')+ ''''
						WHEN 
							ControlType = 'date' AND RIGHT(ControlValue,1) <> '|' AND LEFT(ControlValue,1) <> '|'
						THEN
							' AND ' +  ' id.' + QueryColumn + ' between '''+ Replace(ControlValue,'|',''' AND ''')+ '''' 
						/*Set time WHERE clause*/
						WHEN 
							ControlType = 'time' AND  RIGHT(ControlValue,1) = '|'
						THEN 
							' AND ' +  ' id.' + QueryColumn + ' >= CAST('''+ Replace(ControlValue,'|','')+ ''' as time)'
						WHEN 
							ControlType = 'time' AND LEFT(ControlValue,1) = '|'
						THEN 
							' AND ' +  ' id.' + QueryColumn + ' <= CAST('''+ Replace(ControlValue,'|','')+ ''' as time)'
						WHEN 
							ControlType = 'time' AND RIGHT(ControlValue,1) <> '|' AND LEFT(ControlValue,1) <> '|'
						THEN
							' AND ' +  ' id.' + QueryColumn + ' between CAST('''+ Replace(ControlValue,'|',''' as time) AND CAST(''')+ ''' as time)' 
						/*Set varchar WHERE clause*/
						WHEN 
							ControlType in ('varchar','char','float') AND  ControlValue like '%|%'
						THEN 
						/*new code*/
							+
							CASE 
								WHEN 
									ControlWildcard = 1
								THEN
									CASE 
										WHEN 
											ControlExceptFlag = 1
										THEN
											CASE 
											--LastAttemptTechComment1 + LastAttemptTechComment2
												WHEN
													ControlName = 'SpecialInstructionsDescOneTwoExcept'
												THEN
													' AND ' +  ' (id.SpecialInstructionsDescOne + id.SpecialInstructionsDescTwo NOT LIKE ''%'+ Replace(ControlValue,'|','%'' AND id.SpecialInstructionsDescOne + id.SpecialInstructionsDescTwo NOT LIKE ''%') + '%'') '
												WHEN 
													ControlName = 'SpecInst_SvcReq_TechComm_Except'
												THEN
													' AND ' +  ' (COALESCE(id.SpecialInstructionsDescOne,'''') + COALESCE(id.SpecialInstructionsDescTwo,'''') + COALESCE(id.LastAttemptTechComment1,'''') + COALESCE(id.LastAttemptTechComment2,'''') + COALESCE(id.ServiceRequestDescription,'''') NOT LIKE ''%'
													+ Replace(ControlValue,'|','%'' AND COALESCE(id.SpecialInstructionsDescOne,'''') + COALESCE(id.SpecialInstructionsDescTwo,'''') + COALESCE(id.LastAttemptTechComment1,'''') + COALESCE(id.LastAttemptTechComment2,'''') + COALESCE(id.ServiceRequestDescription,'''') NOT LIKE ''%') + '%'') '
												WHEN 
													ControlName = 'LastAttemptTechComment1&2Except' 
												THEN
													' AND ' +  ' (id.LastAttemptTechComment1 + id.LastAttemptTechComment2 NOT LIKE ''%'+ Replace(ControlValue,'|','%'' AND id.LastAttemptTechComment1 + id.LastAttemptTechComment2 NOT LIKE ''%') + '%'') '
												ELSE
													' AND ' +  ' (id.' + QueryColumn + ' not like ''%'+ Replace(ControlValue,'|','%'' AND id.' + QueryColumn + ' NOT LIKE ''%') + '%'') '
											END
											
										ELSE
											CASE 
												WHEN
													ControlName = 'SpecialInstructionsDescOneTwo'
												THEN
													' AND ' +  ' (id.SpecialInstructionsDescOne + id.SpecialInstructionsDescTwo  LIKE ''%'+ Replace(ControlValue,'|','%'' OR id.SpecialInstructionsDescOne + id.SpecialInstructionsDescTwo LIKE ''%') +'%'') '
												WHEN 
													ControlName = 'SpecInst_SvcReq_TechComm'
												THEN
													' AND ' +  ' (COALESCE(id.SpecialInstructionsDescOne,'''') + COALESCE(id.SpecialInstructionsDescTwo,'''') + COALESCE(id.LastAttemptTechComment1,'''') + COALESCE(id.LastAttemptTechComment2,'''') + COALESCE(id.ServiceRequestDescription,'''')  LIKE ''%'
														+ Replace(ControlValue,'|','%'' OR COALESCE(id.SpecialInstructionsDescOne,'''') + COALESCE(id.SpecialInstructionsDescTwo,'''') + COALESCE(id.LastAttemptTechComment1,'''') + COALESCE(id.LastAttemptTechComment2,'''') + COALESCE(id.ServiceRequestDescription,'''')  LIKE ''%') + '%'') '
												WHEN 
													ControlName = 'LastAttemptTechComment1&2' 
												THEN
													' AND ' +  ' (id.LastAttemptTechComment1 + id.LastAttemptTechComment2  LIKE ''%'+ Replace(ControlValue,'|','%'' OR id.LastAttemptTechComment1 + id.LastAttemptTechComment2 LIKE ''%') + '%'') '
												ELSE
													' AND ' +  ' (id.' + QueryColumn + '  LIKE ''%'+ Replace(ControlValue,'|','%'' OR id.' + QueryColumn + ' LIKE ''%') +'%'') '
											END
									END
								ELSE /*controlWildCard = 0*/
									CASE 
										WHEN 
											ControlExceptFlag = 1
										THEN
											' AND ' +  '( id.' + QueryColumn + ' NOT IN ('''+ Replace(ControlValue,'|',''',''')+ ''') OR id.' + QueryColumn + ' is NULL) '
										ELSE
											' AND ' +  ' id.' + QueryColumn + ' IN ('''+ Replace(ControlValue,'|',''',''')+ ''') '
									END
							END
						/*end new code*/

						--' AND ' +  ' id.' + QueryColumn + ' in ('''+ Replace(ControlValue,'|',''',''')+ ''')'
						WHEN 
							ControlType in ('varchar','char','float') AND ControlValue not like '%|%'
						THEN
							' AND ( ' +  
							CASE
										WHEN 
											ControlName = 'SpecialInstructionsDescOneTwoExcept' OR ControlName = 'SpecialInstructionsDescOneTwo'
										THEN
											'COALESCE(id.SpecialInstructionsDescOne,'''') + COALESCE(id.SpecialInstructionsDescTwo,'''')  '
										WHEN 
											ControlName = 'SpecInst_SvcReq_TechComm' OR ControlName = 'SpecInst_SvcReq_TechComm_Except'
										THEN
											' COALESCE(id.SpecialInstructionsDescOne,'''') + COALESCE(id.SpecialInstructionsDescTwo,'''') + COALESCE(id.LastAttemptTechComment1,'''') + COALESCE(id.LastAttemptTechComment2,'''') + COALESCE(id.ServiceRequestDescription,'''') '
										WHEN 
											ControlName = 'LastAttemptTechComment1&2Except' OR ControlName = 'LastAttemptTechComment1&2'
										THEN
											' COALESCE(id.LastAttemptTechComment1,'''') + COALESCE(id.LastAttemptTechComment2,'''') '
										ELSE
											'id.' + QueryColumn
									END
							+
							CASE 
								WHEN 
									ControlWildcard = 1
								THEN
									CASE 
										WHEN 
											ControlExceptFlag = 1
										THEN
											' not like ''%'+ ControlValue+ '%'' ' 
											+ CASE 
												WHEN QueryColumn <> '' 
												THEN 'OR id.' + QueryColumn + ' IS NULL ) '
												ELSE ' )'
											 END
										ELSE
											' like ''%'+ ControlValue+ '%'' )'
									END
								ELSE
									CASE
										WHEN 
											ControlName = 'ServiceOrderStatusGroup'
										THEN
											CASE 
												WHEN ControlValue = 'Open' THEN ' not in (''CO'',''ED'',''CA'',''CV'',''CP'') ' 
												WHEN ControlValue = 'Completed' THEN ' in (''CO'',''ED'',''CP'') ' 
												WHEN ControlValue = 'Cancelled' THEN ' in (''CA'',''CV'') ' 
												WHEN ControlValue = 'Closed' THEN ' in (''CO'',''ED'',''CA'',''CV'',''CP'') ' 
												ELSE ' is not null )' 
											END
										ELSE 
											CASE 
												WHEN 
													ControlExceptFlag = 1
												THEN
													' <> '''+ ControlValue+ ''' OR id.' + QueryColumn + ' IS NULL ) '
												ELSE
													' = '''+ ControlValue+ ''' )'
											END
									END
							END 
						/*Set int WHERE clause*/
						WHEN 
							ControlType in ('int','decimal','smallint') AND  RIGHT(ControlValue,1) = '|'
						THEN 
							CASE 
								WHEN 
									ControlExceptFlag = 1
								THEN 
									' AND ' +  ' NOT(id.' + QueryColumn + ' >= '+ Replace(ControlValue,'|','')	+ ')'	
								ELSE						
									' AND ' +  ' id.' + QueryColumn + ' >= '+ Replace(ControlValue,'|','')
							END							
						WHEN 
							ControlType in ('int','decimal','smallint') AND  LEFT(ControlValue,1) = '|'
						THEN 
							CASE 
								WHEN 
									ControlExceptFlag = 1
								THEN 
									' AND ' +  ' NOT(id.' + QueryColumn + ' <= '+ Replace(ControlValue,'|','')	+ ')'	
								ELSE						
									' AND ' +  ' id.' + QueryColumn + ' <= '+ Replace(ControlValue,'|','')
							END	
						WHEN 
							ControlType  in ('int','decimal','smallint') AND ControlValue like '%|%' AND RIGHT(ControlValue,1) <> '|' AND LEFT(ControlValue,1) <> '|'
						THEN
							CASE 
								WHEN 
									ControlExceptFlag = 1
								THEN 
									' AND ' +  ' id.' + QueryColumn + ' NOT BETWEEN '+ Replace(ControlValue,'|',' AND ')	
								ELSE						
									' AND ' +  ' id.' + QueryColumn + ' BETWEEN '+ Replace(ControlValue,'|',' AND ')
							END	
						WHEN 
							ControlType in ('int','decimal','smallint') AND ControlValue not like '%|%'
						THEN
							' AND ' +  ' id.' + QueryColumn + 
							CASE 
								WHEN 
									ControlWildcard = 1
								THEN
									CASE 
										WHEN 
											ControlExceptFlag = 1
										THEN
											' not like ''%'+ ControlValue+ '%'''
										ELSE
											' like ''%'+ ControlValue+ '%'''
									END
								ELSE
									' = ' +ControlValue
							END
					END 
				FROM 
					ServiceOrder.dbo.TBL_QueryBuilder as qbs with (nolock)
					INNER JOIN ServiceOrder.dbo.TBL_ControlBuilder as cbs with (nolock) on qbs.ControlID = cbs.ControlID
				WHERE 
					qbs.ReportID = @reportID
					AND cbs.ControlSQLClause = 'WHERE'
					AND qbs.ReportID = qb.ReportID
				ORDER BY
					cbs.SortOrder
				FOR XML PATH('')
				), 1, 5, ''	)
			,
			'ServiceOrder.dbo.VIEW_ServiceOrder as id with (nolock)' as fromC
			,
			funct = STUFF((
				SELECT  TOP 100 PERCENT
					CASE 
						WHEN vbs.ControlType in ('date','getdate')
						THEN ',CAST('+ cbs.ControlName + '(' + 'id.' +vbs.QueryColumn + ') as varchar(111))  as ' + vbs.ControlName + cbs.ControlName
						ELSE ','+ cbs.ControlName + '(' + 'id.' + vbs.QueryColumn + ')  as ' + vbs.ControlName + cbs.ControlName
					END
					/*',CAST(' + cbs.ControlName + '(' + vbs.QueryColumn + ') as '+vbs.ControlType+') as ' + vbs.ControlName + cbs.ControlName*/
				FROM 
					ServiceOrder.dbo.TBL_QueryBuilder as qbs with (nolock)
					INNER JOIN ServiceOrder.dbo.TBL_ControlBuilder as cbs with (nolock) on qbs.ControlID = cbs.ControlID
					INNER JOIN ServiceOrder.dbo.TBL_ControlBuilder as vbs with (nolock) on vbs.QueryColumn = qbs.ControlValue and vbs.ControlSQLClause = 'Select'
				WHERE 
					qbs.ReportID = @ReportID
					AND cbs.ControlSQLClause in ('GROUP BY')
					AND qbs.ReportID = qb.ReportID
				ORDER BY
					cbs.SortOrder
				FOR XML PATH('')
				), 1, 1, ''	)
			,
			groupbyc = STUFF((
					SELECT  TOP 100 PERCENT
						/*
						I am removing the addition of the GROUP BY clause when the chosen column is a date.
						',' + ' id.' + QueryColumn
						
						--8-13-2018 PGALVAN added second WHEN logic to support SELECT clause inclusion for date fields
						*/
						CASE 
								WHEN 
									ControlType not in ('date', 'getdate')
								THEN 
									',' + 'id.' + QueryColumn
								WHEN 
									ControlType in ('date', 'getdate') and ControlSQLClause = 'SELECT'
								THEN 
									',CAST(' +'id.' + QueryColumn +' as varchar(111))'
								ELSE 
									''
						END
					FROM 
						ServiceOrder.dbo.TBL_QueryBuilder as qbs with (nolock)
						INNER JOIN ServiceOrder.dbo.TBL_ControlBuilder as cbs with (nolock) on qbs.ControlID = cbs.ControlID
					WHERE 
						qbs.ReportID = @ReportID
						AND cbs.ControlSQLClause in ('SELECT','WHERE')
						AND qbs.ReportID = qb.ReportID
					ORDER BY
						cbs.SortOrder
				FOR XML PATH('')
				), 1, 1, ''	)
		,ServiceOrderValues = STUFF((
			SELECT 
				','+ CAST(cbs.ControlID as varchar)
			FROM 
				ServiceOrder.dbo.TBL_QueryBuilder as qbs with (nolock)
				INNER JOIN ServiceOrder.dbo.TBL_ControlBuilder as cbs with (nolock) on qbs.ControlID = cbs.ControlID
			WHERE 
				qbs.ReportID = @ReportID
				AND cbs.ControlID in (47,177)
				AND qbs.ReportID = qb.ReportID
				FOR XML PATH('')
			), 1, 1, ''	)
		,ServiceUnitValues = STUFF((
			SELECT
				','+ CAST(cbs.ControlID as varchar)
			FROM 
				ServiceOrder.dbo.TBL_QueryBuilder as qbs with (nolock)
				INNER JOIN ServiceOrder.dbo.TBL_ControlBuilder as cbs with (nolock) on qbs.ControlID = cbs.ControlID
			WHERE 
				qbs.ReportID = @ReportID
				AND cbs.ControlID in (2,30)
				AND qbs.ReportID = qb.ReportID
			FOR XML PATH('')
				), 1, 1, ''	)
	
FROM 
	ServiceOrder.dbo.TBL_QueryBuilder as qb with (nolock)
	INNER JOIN ServiceOrder.dbo.TBL_ControlBuilder as cb with (nolock) on qb.ControlID = cb.ControlID
WHERE
	ReportID =@reportID
) AS main
END









GO
/****** Object:  StoredProcedure [dbo].[PROC_TaskCreation]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Developed by: 
	Preston Galvanek
Created Date: 
	2016-06-15
Description:
	Populate ServicePro with tasks pulled from query.
Updates:	
	2017-07-21: PGALVAN: Updated to include Groups
	2017-11-07: PGALVAN: Updated to allow tasks to be re-entered where the most recent taskID within a queue/report/group is not open or when a completed task hasn't been updated today or yesterday.
    2017-11-28: PGALVAN: Updated to include task log table updates
	2017-12-09: PGALVAN: Updated to include create date and user for log records
	2017-12-13: PGALVAN: Updated to include multiple groups
    2018-02-21: PGALVAN: Separated @FinalQuery into 3 segments due to the 8k varchar limit on varchar(max)
    2018-02-21: PGALVAN: Added TBL_Tasks update for LogID and ClaimID and TBL_Claims update for TaskID
	2018-06-08: PGALVAN: Entered into QEE; added executionorder and executionset.
Execution Duration:
	20 minutes
*/

CREATE PROCEDURE [dbo].[PROC_TaskCreation]
	@Frequency as varchar(4) = NULL,
	@FrequencyModifier as int = NULL,
	@ErrorCode varchar(150) = NULL OUTPUT
AS
BEGIN 

	/*Reassign values when blank to NULL */
	IF @FrequencyModifier = 0
		SET @FrequencyModifier = NULL
	IF @Frequency = ''
		SET @Frequency = NULL	

	/*Error Handling*/
	IF @Frequency Is Null /* Return error code if @Frequency is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for Frequency, it is Required.'
		Return
	END
	IF @FrequencyModifier Is Null /* Return error code if @ExecutionGroup is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for FrequencyModifier, it is Required.'
		Return
	END

	/*Declare variables*/
	DECLARE @SQL as varchar(max)
	DECLARE @ExecutionOrder as int

	/*Iterate through query results*/
	DECLARE QueryCursor CURSOR for
	SELECT
	'
	DECLARE @date as varchar(15)
	DECLARE @datetime as varchar(30)

	SET @date = CAST(CAST(getdate() as date) as varchar)
	SET @datetime = CONVERT(varchar,GETDATE(),121)
	DECLARE @cols NVARCHAR(2000) 
	DECLARE @colsDynamic NVARCHAR(2000)
	DECLARE @name NVARCHAR(255)
	DECLARE @priorColumn NVARCHAR(255)
	DECLARE @pos INT
	DECLARE @pos2 INT
	DECLARE @keyvaluepair NVARCHAR(255)
	DECLARE @column NVARCHAR(255)
	DECLARE @value NVARCHAR(255)
	DECLARE @counter INT
	DECLARE @colQuery as varchar(2000)
	DECLARE @finalQuery as varchar(max)

	SELECT 
		@cols = STUFF((
		SELECT DISTINCT TOP 100 Percent
				'','' + CAST(col.Column_Name as varchar(50)) + ''|'' + CAST(g.ControlValue as varchar)
		FROM 
			ServiceOrder.dbo.TBL_ControlBuilder as c with (nolock)
			INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderGroupItems as g with (nolock) on c.ControlID = g.ControlID
			INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderGroup as grp with (nolock) on grp.GroupID = g.GroupID
			INNER JOIN ServiceOrder.INFORMATION_SCHEMA.COLUMNS as col with (nolock) on c.QueryColumn = col.Column_Name 
			INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderAssignment as a with (nolock)  on a.GroupID = g.GroupID
		WHERE 
			col.TABLE_NAME = N''VIEW_ServiceOrder''
			AND a.QueueID = ' + CAST(q.QueueID as varchar) + '
			AND a.ReportID = ' + CAST(r.ReportID as varchar) + '
			AND a.GroupID = ' + CAST(g.GroupID as varchar) + '
			AND grp.IsEnabled = 1
		ORDER BY
			'','' + CAST(col.Column_Name as varchar(50)) + ''|'' + CAST(g.ControlValue as varchar)
			FOR XML PATH('''')   
			), 1, 1, '''')

	/*
	QA: Display column list for each iteration
	SELECT @cols
	*/

	/*Reassign dynamic cols variable to default and counter to 0*/
	SELECT @colsDynamic = @cols
	SELECT @counter = 0
	SELECT @colQuery = ''''
	SELECT @priorcolumn = ''''
		WHILE CHARINDEX('','', @colsDynamic) > 0
			BEGIN
			SELECT @counter = COALESCE(@counter,0) + 1
			SELECT @pos  = CHARINDEX('','', @colsDynamic) 
			SELECT @pos2  = CHARINDEX(''|'', @colsDynamic) 
			SELECT @keyvaluepair  = SUBSTRING(@colsDynamic, 1, @pos) 
			SELECT @column = SUBSTRING(@colsDynamic, 1, @pos2-1)
			SELECT @value  = SUBSTRING(@keyvaluepair,@pos2+1,@pos - @pos2-1 )
			SELECT @name = 
			CASE 
				WHEN @priorcolumn = ''''
				THEN  '' AND (id.'' + @column + '' = ''''''+ @value + ''''''''
				WHEN @column = @priorcolumn
				THEN  '' OR id.'' + @column + '' = ''''''+ @value + ''''''''
				ELSE  '') AND (id.'' + @column + '' = ''''''+ @value + ''''''''
			END
			SELECT @colQuery = COALESCE(@colQuery,'''') + COALESCE(@name,'''')
			SELECT @colsDynamic = SUBSTRING(@colsDynamic, @pos+1, LEN(@colsDynamic)-@pos)
			SELECT @priorColumn = @column
			END

		/* 
		SELECT SUBSTRING(@colsDynamic, 1, CHARINDEX(''|'', @colsDynamic)-1)
		SELECT @priorColumn
		*/

		SELECT 
			@colQuery = @colQuery + 
			CASE
				WHEN 
					@priorcolumn = ''''
				THEN 
					'' AND id.'' + SUBSTRING(@colsDynamic, 1, CHARINDEX(''|'', @colsDynamic)-1) + '' = '''''' + SUBSTRING(@colsDynamic, CHARINDEX(''|'', @colsDynamic)+1, len( @colsDynamic) -CHARINDEX(''|'', @colsDynamic)) + '''''' ''
				WHEN 
					SUBSTRING(@colsDynamic, 1, CHARINDEX(''|'', @colsDynamic)-1) = @priorColumn
				THEN 
					'' OR id.'' + SUBSTRING(@colsDynamic, 1, CHARINDEX(''|'', @colsDynamic)-1) + '' = '''''' + SUBSTRING(@colsDynamic, CHARINDEX(''|'', @colsDynamic)+1, len( @colsDynamic) -CHARINDEX(''|'', @colsDynamic)) + '''''') ''
				ELSE
					'' AND id.'' + SUBSTRING(@colsDynamic, 1, CHARINDEX(''|'', @colsDynamic)-1) + '' = '''''' + SUBSTRING(@colsDynamic, CHARINDEX(''|'', @colsDynamic)+1, len( @colsDynamic) -CHARINDEX(''|'', @colsDynamic)) + '''''') ''
			END

	/*SELECT @colQuery*/
	SET @finalQuery = ''
		INSERT INTO ServiceOrder.dbo.TBL_ServiceOrderTasks (QueueID,ReportID,GroupID,ServiceUnit,ServiceOrder,PriorityValue,DueDate,CreatedDate,UpdatedDate,CreateEnterpriseID,UpdateEnterpriseID)
		SELECT 
			'+ CAST(q.QueueID as varchar) +' as QueueID
			,'+ CAST(r.ReportID as varchar) + ' as ReportID 
			,'+ COALESCE(CAST(g.GroupID as varchar),'-1') +' as GroupID
			,id.ServiceUnit
			,id.ServiceOrder
			, '+Cast(r.PriorityValue as varchar)+' as PriorityValue
			,DATEADD(Day,1,CAST(''''''+@date+'''''' as date)) as DueDate
			, CAST(''''''+@datetime+'''''' as datetime) as CreatedDate
			, CAST(''''''+@datetime+'''''' as datetime) as UpdatedDate
			,''''SYSTEM'''' as CreateEnterpriseID  
			,''''SYSTEM'''' as UpdatedEnterpriseID 
		FROM  ' + CAST(r.QueryFrom as varchar(max)) +
				CASE 
					WHEN r.QueryWhere <> '' 
					THEN ' WHERE  ' + REPLACE(CAST(r.QueryWhere as varchar(max)),'''','''''')
					+
						CASE 
							WHEN grp.ColumnCount is not null 
							THEN 
							'
							''+@colQuery +''
								AND NOT EXISTS (
								SELECT 
									t.TaskID
									,t.UpdatedDate
									,t.ServiceUnit
									,t.ServiceOrder
									,COALESCE(o.closed,0) as StatusFlag
								FROM
									ServiceOrder.dbo.TBL_ServiceOrderTasks as t with (nolock)
									INNER JOIN (
										SELECT
											MAX(t.TaskID) as TaskID
											,t.ServiceUnit
											,t.ServiceOrder
										FROM 
											ServiceOrder.dbo.TBL_ServiceOrderTasks as t with (nolock) 
										WHERE 
											t.QueueID = ' + CAST(q.QueueID as varchar) + '
											AND t.ReportID = ' + CAST(r.ReportID as varchar) + '
											/*TESTING removal 20171212 AND t.GroupID = ' + CAST(g.GroupID as varchar) + '*/
										GROUP BY
											t.ServiceUnit
											,t.ServiceOrder
									) as mts  ON mts.TaskID = t.TaskID	
									INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderTasksLog as l with (nolock) on t.LogID = l.LogID
									INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderQueueOptions as o with (nolock) on o.OptionID = l.OptionID 
								WHERE 
									(
										(
											COALESCE(o.closed,0) = 1
											AND t.UpdatedDate >= CAST(GETDATE()-1 as date)
										)
										OR
										(
											COALESCE(o.closed,0) = 0
										)
									)
									AND	t.ServiceUnit = id.ServiceUnit and t.ServiceOrder = id.ServiceOrder
							)	
							'
							ELSE '
								AND NOT EXISTS (
								SELECT 
									t.TaskID
									,t.UpdatedDate
									,t.ServiceUnit
									,t.ServiceOrder
									,COALESCE(o.closed,0) as StatusFlag
								FROM
									ServiceOrder.dbo.TBL_ServiceOrderTasks as t with (nolock)
									INNER JOIN (
										SELECT
											MAX(t.TaskID) as TaskID
											,t.ServiceUnit
											,t.ServiceOrder
										FROM 
											ServiceOrder.dbo.TBL_ServiceOrderTasks as t with (nolock) 
										WHERE 
											t.QueueID = ' + CAST(q.QueueID as varchar) + '
											AND t.ReportID = ' + CAST(r.ReportID as varchar) + '
											/*AND t.GroupID <> -1*/
										GROUP BY
											t.ServiceUnit
											,t.ServiceOrder
									) as mts  ON mts.TaskID = t.TaskID	
									INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderTasksLog as l with (nolock) on t.LogID = l.LogID
									INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderQueueOptions as o with (nolock) on o.OptionID = l.OptionID 
								WHERE 
									(
										(
											COALESCE(o.closed,0) = 1
											AND t.UpdatedDate >= CAST(GETDATE()-1 as date)
										)
										OR
										(
											COALESCE(o.closed,0) = 0
										)
									)
									AND	t.ServiceUnit = id.ServiceUnit and t.ServiceOrder = id.ServiceOrder
							)	
								'
							END
					ELSE 
						CASE 
							WHEN grp.ColumnCount is not null 
							THEN 
							+ '
								WHERE 1=1
								''+@colQuery +''
								AND NOT EXISTS (
								SELECT 
									t.TaskID
									,t.UpdatedDate
									,t.ServiceUnit
									,t.ServiceOrder
									,COALESCE(o.closed,0) as StatusFlag
								FROM
									ServiceOrder.dbo.TBL_ServiceOrderTasks as t with (nolock)
									INNER JOIN (
										SELECT
											MAX(t.TaskID) as TaskID
											,t.ServiceUnit
											,t.ServiceOrder
										FROM 
											ServiceOrder.dbo.TBL_ServiceOrderTasks as t with (nolock) 
										WHERE 
											t.QueueID = ' + CAST(q.QueueID as varchar) + '
											AND t.ReportID = ' + CAST(r.ReportID as varchar) + '
											/*TESTING removal 20171212 AND t.GroupID = ' + CAST(g.GroupID as varchar) + '*/
										GROUP BY
											t.ServiceUnit
											,t.ServiceOrder
									) as mts  ON mts.TaskID = t.TaskID	
									INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderTasksLog as l with (nolock) on t.LogID = l.LogID
									INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderQueueOptions as o with (nolock) on o.OptionID = l.OptionID 
								WHERE 
									(
										(
											COALESCE(o.closed,0) = 1
											AND t.UpdatedDate >= CAST(GETDATE()-1 as date)
										)
										OR
										(
											COALESCE(o.closed,0) = 0
										)
									)
									AND	t.ServiceUnit = id.ServiceUnit and t.ServiceOrder = id.ServiceOrder
							)	

							'
							ELSE '
								WHERE NOT EXISTS (
								SELECT 
									t.TaskID
									,t.UpdatedDate
									,t.ServiceUnit
									,t.ServiceOrder
									,COALESCE(o.closed,0) as StatusFlag
								FROM
									ServiceOrder.dbo.TBL_ServiceOrderTasks as t with (nolock)
									INNER JOIN (
										SELECT
											MAX(t.TaskID) as TaskID
											,t.ServiceUnit
											,t.ServiceOrder
										FROM 
											ServiceOrder.dbo.TBL_ServiceOrderTasks as t with (nolock) 
										WHERE 
											t.QueueID = ' + CAST(q.QueueID as varchar) + '
											AND t.ReportID = ' + CAST(r.ReportID as varchar) + '
											/*TESTING removal 20171212 AND t.GroupID = ' + CAST(g.GroupID as varchar) + '*/
											/*AND t.GroupID <> -1*/
										GROUP BY
											t.ServiceUnit
											,t.ServiceOrder
									) as mts  ON mts.TaskID = t.TaskID	
									INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderTasksLog as l with (nolock) on t.LogID = l.LogID
									INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderQueueOptions as o with (nolock) on o.OptionID = l.OptionID 
								WHERE 
									(
										(
											COALESCE(o.closed,0) = 1
											AND t.UpdatedDate >= CAST(GETDATE()-1 as date)
										)
										OR
										(
											COALESCE(o.closed,0) = 0
										)
									)
									AND	t.ServiceUnit = id.ServiceUnit and t.ServiceOrder = id.ServiceOrder
							)	
								'
						END
				END +
				CASE 
					WHEN r.QueryGroupBy <> '' 
					THEN ' GROUP BY ' + CAST(r.QueryGroupBy as varchar(max))
					ELSE '' 
				END + '; 
			'' SET @finalQuery = @finalQuery + ''
			UPDATE 
				t
			SET
				UpdatedDate = CAST(''''''+@datetime+'''''' as datetime)
				,UpdateEnterpriseID = ''''SYSTEM''''		
			FROM 
				' + CAST(r.QueryFrom as varchar(max)) + '
				INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderTasks as t with (nolock) on id.ServiceUnit = t.ServiceUnit and id.ServiceOrder = t.ServiceOrder		
				INNER JOIN (
					SELECT
						MAX(t2.TaskID) as TaskID
						,t2.ServiceUnit
						,t2.ServiceOrder
					FROM 
						ServiceOrder.dbo.TBL_ServiceOrderTasks as t2 with (nolock) 
					WHERE 
						t2.QueueID = '+ CAST(q.QueueID as varchar) +'
						AND t2.ReportID =  ' + CAST(r.ReportID as varchar) + '
						AND t2.GroupID =  ' + COALESCE(CAST(g.GroupID as varchar),'-1') + '
					GROUP BY
						t2.ServiceUnit
						,t2.ServiceOrder
				) as mts  ON mts.TaskID = t.TaskID	
				'		
				+CASE 
					WHEN r.QueryWhere <> '' 
					THEN ' WHERE  ' + REPLACE(CAST(r.QueryWhere as varchar(max)),'''','''''') +'
							AND t.QueueID = '+ CAST(q.QueueID as varchar) +'
							AND t.ReportID =  ' + CAST(r.ReportID as varchar) + '
							AND t.GroupID =  ' + COALESCE(CAST(g.GroupID as varchar),'-1') 
					ELSE ' WHERE t.QueueID = '+ CAST(q.QueueID as varchar) +'
							AND t.ReportID =  ' + CAST(r.ReportID as varchar) + '
							AND t.GroupID =  ' + COALESCE(CAST(g.GroupID as varchar),'-1')
				END 
				+
							CASE 
								WHEN grp.ColumnCount is not null 
								THEN 
								'
								''+@colQuery +''
								'
								ELSE
								''
							END
							+';'
			
				+'			
			'' SET @finalQuery = @finalQuery + ''
			MERGE 
				ServiceOrder.dbo.TBL_ServiceOrderTasksLog AS D
			USING 
				(
					SELECT 
						t.TaskID, -1 as OptionID, ''''SYSTEM'''' as EnterpriseID, CAST(''''''+@datetime+'''''' as datetime) as ModifiedDateTime, ''''SYSTEM'''' as CreateEnterpriseID, CAST(''''''+@datetime+'''''' as datetime) as CreateDateTime
					FROM  
						ServiceOrder.dbo.TBL_ServiceOrderTasks as t with (nolock)
					WHERE 
						t.QueueID = '+ CAST(q.QueueID as varchar) +'
						AND t.ReportID =  ' + CAST(r.ReportID as varchar) + '
						AND t.GroupID =  ' + COALESCE(CAST(g.GroupID as varchar),'-1') + '
						AND CAST(t.CreatedDate as date) = CAST(GETDATE() as date)
				) as S
			ON (
				D.TaskID = S.TaskID
				/*AND D.OptionID = -1 Removing, no need to check for newly inserted items from system. If there is any log item for the task then do not insert a record*/
				)
			WHEN NOT MATCHED BY TARGET
				THEN INSERT(TaskID, OptionID,EnterpriseID, ModifiedDateTime, CreateEnterpriseID,CreateDateTime)
				VALUES(S.TaskID, S.OptionID,S.EnterpriseID, S.ModifiedDateTime,S.CreateEnterpriseID, S.CreateDateTime);

			
			/*Update all Log IDs*/
			UPDATE 
				t 
			SET 
				LogID = l.LogID
			FROM    
				ServiceOrder.dbo.TBL_ServiceOrderTasks as t
				INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderTasksLog as l  ON l.TaskID = t.TaskID    
				INNER JOIN (    
					SELECT    
						TaskID,  
						MAX(CreateDateTime) as CreateDateTime   
					FROM   
						ServiceOrder.dbo.TBL_ServiceOrderTasksLog as l    
					GROUP BY    
						TaskID  
				) as lm on l.TaskID = lm.TaskID and l.CreateDateTime = lm.CreateDateTime    
			WHERE 
				t.LogID is null
				/*AND t.CreatedDate = CAST(''''''+@datetime+'''''' as datetime) */
				AND t.QueueID = '+ CAST(q.QueueID as varchar) +'
				AND t.ReportID =  ' + CAST(r.ReportID as varchar) + '
				AND t.GroupID =  ' + COALESCE(CAST(g.GroupID as varchar),'-1') + '


			'' SET @finalQuery = @finalQuery + ''
			MERGE 
				ServiceOrder.dbo.TBL_ServiceOrderTasksClaimed AS D
			USING 
				(
					SELECT
						l.LogID, NULL as ClaimedEnterpriseID, ''''SYSTEM'''' as ModifiedEnterpriseID, CAST(''''''+@datetime+'''''' as datetime) as ModifiedDateTime
					FROM	
						ServiceOrder.dbo.TBL_ServiceOrderTasks as t with (nolock)   
						INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderTasksLog as l with (nolock) on t.LogID = l.LogID
					WHERE 
						t.QueueID = '+ CAST(q.QueueID as varchar) +'
						AND t.ReportID =  ' + CAST(r.ReportID as varchar) + '
						AND t.GroupID =  ' + COALESCE(CAST(g.GroupID as varchar),'-1') + '
						AND CAST(t.CreatedDate as date) = CAST(GETDATE() as date)
					
				) as S
			ON (
				D.LogID = S.LogID
				)
			WHEN NOT MATCHED BY TARGET
				THEN INSERT(LogID, ClaimedEnterpriseID,ModifiedEnterpriseID, ModifiedDateTime)
				VALUES(S.LogID, ClaimedEnterpriseID, ModifiedEnterpriseID, S.ModifiedDateTime);
			''
			SET @finalQuery = @finalQuery + ''

			/*Update all ClaimIDs*/
			UPDATE 
				t 
			SET 
				ClaimID = tc.ClaimID
			FROM    
				ServiceOrder.dbo.TBL_ServiceOrderTasks as t
				INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderTasksLog as l  ON l.TaskID = t.TaskID    
				INNER JOIN (    
					SELECT    
						TaskID,  
						MAX(CreateDateTime) as CreateDateTime   
					FROM   
						ServiceOrder.dbo.TBL_ServiceOrderTasksLog as l    
					GROUP BY    
						TaskID  
				) as lm on l.TaskID = lm.TaskID and l.CreateDateTime = lm.CreateDateTime    
   				INNER JOIN ServiceOrder.dbo.TBl_ServiceOrderTasksClaimed as tc  ON tc.LogID = l.LogID 
				INNER JOIN (     
					SELECT     
						LogID,    
						MAX(ModifiedDateTime) as ModifiedDateTime    
					FROM    
						ServiceOrder.dbo.TBl_ServiceOrderTasksClaimed    
					GROUP BY     
						LogID    
				) as tcm on tc.LogID = tcm.LogID and tc.ModifiedDateTime = tcm.ModifiedDateTime   
			WHERE 
				t.ClaimID is null 
				/*and t.CreatedDate = CAST(''''''+@datetime+'''''' as datetime) */
				AND t.QueueID = '+ CAST(q.QueueID as varchar) +'
				AND t.ReportID =  ' + CAST(r.ReportID as varchar) + '
				AND t.GroupID =  ' + COALESCE(CAST(g.GroupID as varchar),'-1') + '

			/*Update all Task IDs for Claim Data*/
			UPDATE
				tc
			SET 
				TaskID = l.TaskID
			FROM
				ServiceOrder.dbo.TBL_ServiceOrderTasks as t
				INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderTasksLog as l on t.taskid = l.taskid
				INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderTasksClaimed  as tc on l.logid = tc.logid
			WHERE 
				tc.TaskID is null 
				/* AND l.CreateDateTime = CAST(''''''+@datetime+'''''' as datetime) */
				AND t.QueueID = '+ CAST(q.QueueID as varchar) +'
				AND t.ReportID =  ' + CAST(r.ReportID as varchar) + '
				AND t.GroupID =  ' + COALESCE(CAST(g.GroupID as varchar),'-1') + '
			''

			/*SELECT @finalQuery*/

			EXEC(@finalQuery)

		' as Query
	FROM
		ServiceOrder.dbo.TBL_ServiceOrderAssignment as a with (nolock)
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderReports as r with (nolock) ON a.ReportID = r.ReportID
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderQueue as q with (nolock) ON a.QueueID = q.QueueID
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderGroup as g with (nolock) ON a.GroupID = g.GroupID
		LEFT OUTER JOIN (
			SELECT  
				a2.QueueID,
				a2.ReportID,
				a2.GroupID,
				COUNT(DISTINCT col.Column_Name) as ColumnCount
			FROM 
				ServiceOrder.dbo.TBL_ControlBuilder as c with (nolock)
				INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderGroupItems as g with (nolock) on c.ControlID = g.ControlID
				INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderGroup as grp with (nolock) on grp.GroupID = g.GroupID
				INNER JOIN ServiceOrder.INFORMATION_SCHEMA.COLUMNS as col with (nolock) on c.QueryColumn = col.Column_Name 
				INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderAssignment as a2 with (nolock)  on a2.GroupID = g.GroupID
			WHERE 
				col.TABLE_NAME = N'VIEW_ServiceOrder'
				AND grp.IsEnabled = 1
			GROUP BY
				a2.QueueID,
				a2.ReportID,
				a2.GroupID			
		) as grp on grp.QueueID = a.QueueID and grp.ReportID = a.ReportID and grp.GroupID = a.GroupID
		LEFT JOIN (
			SELECT
				o.QueueID,
				COUNT(Distinct o.OptionID) as openOptions
			FROM 
				ServiceOrder.dbo.TBL_ServiceOrderQueueOptions as o with (nolock) 
				INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderQueueOptions as oNext with (nolock) ON o.QueueID = oNext.QueueID AND o.NextSetID = oNext.SetID
			WHERE
				o.closed = 0
			GROUP BY
				o.QueueID
		) as oOpen ON q.QueueID = oOpen.QueueID
		INNER JOIN (
			SELECT
				o.QueueID,
				COUNT(Distinct o.OptionID) as closedOptions
			FROM 
				ServiceOrder.dbo.TBL_ServiceOrderQueueOptions as o with (nolock) 
			WHERE
				o.closed = 1
			GROUP BY
				o.QueueID
		) as oClosed ON q.QueueID = oClosed.QueueID
		INNER JOIN (
			SELECT  
				COUNT(OptionID) as TotalOptions
				,QueueID 
			FROM 
				ServiceOrder.dbo.TBL_ServiceOrderQueueOptions 
			GROUP BY 
				QueueID
		) as ototal ON ototal.QueueID = q.QueueID
	WHERE 
		COALESCE(oOpen.openOptions,0) + COALESCE(oClosed.closedOptions,0) = ototal.TotalOptions
		AND r.reportid <> -1
		AND q.IsEnabled = 1
		AND g.IsEnabled = 1
		AND r.IsEnabled = 1	
		AND r.Frequency = @Frequency
		AND r.FrequencyModifier = @FrequencyModifier	
	GROUP BY
		r.ReportID
		,q.QueueID
		,g.GroupID 
		,grp.ColumnCount
		,r.PriorityValue
		,r.QuerySelect
		,r.QueryFrom
		,r.QueryWhere
		,r.QueryGroupBy
		,r.QueryOrderBy
	ORDER BY 
		q.QueueID Desc,
		r.ReportID desc,
		g.GroupID desc


	/*Open query with cusor identifier to loop through results*/

	OPEN QueryCursor
	FETCH NEXT FROM QueryCursor INTO  @SQL
	
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXEC (@SQL)
		FETCH NEXT FROM QueryCursor INTO  @SQL
	END
	CLOSE QueryCursor
	DEALLOCATE QueryCursor;			
END







GO
/****** Object:  StoredProcedure [dbo].[PROC_TaskCreationStep1InsertStage]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



/*
Developed by: 
	Preston Galvanek
Created Date: 
	2018-06-14
Description:
	Populate Populate stagging table tasks
*/

CREATE PROCEDURE [dbo].[PROC_TaskCreationStep1InsertStage]
	@Frequency as varchar(4) = NULL,
	@FrequencyModifier as int = NULL,
	@ErrorCode varchar(150) = NULL OUTPUT
AS
BEGIN 

	/*Reassign values when blank to NULL */
	IF @FrequencyModifier = 0
		SET @FrequencyModifier = NULL
	IF @Frequency = ''
		SET @Frequency = NULL	

	/*Error Handling*/
	IF @Frequency Is Null /* Return error code if @Frequency is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for Frequency, it is Required.'
		Return
	END
	IF @FrequencyModifier Is Null /* Return error code if @ExecutionGroup is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for FrequencyModifier, it is Required.'
		Return
	END

	/*Declare variables*/
	DECLARE @SQL as varchar(max)

	/*Iterate through query results*/
	DECLARE QueryCursor CURSOR for
	
	--DECLARE @Frequency  varchar(4) = 'MI'
	--DECLARE @FrequencyModifier  int = 10
	/*Iterate through query results*/
	SELECT
	'
	DECLARE @date as varchar(15)
	DECLARE @datetime as varchar(30)

	SET @date = CAST(CAST(getdate() as date) as varchar)
	SET @datetime = CONVERT(varchar,GETDATE(),121)

	DECLARE @finalQuery as varchar(max)

	/*SELECT @colQuery*/
	SET @finalQuery = ''
		/*STEP 1: Populate stagging table with report query data in*/
		
		INSERT INTO ServiceOrder.dbo.TBL_ServiceOrderTasksStage (QueueID,ReportID,GroupID,ServiceUnit,ServiceOrder,PriorityValue,DueDate,CreatedDate,UpdatedDate,CreateEnterpriseID,UpdateEnterpriseID, Frequency, FrequencyModifier, ReadyForUploadFlag,UploadedFlag)
		SELECT DISTINCT
			'+ CAST(q.QueueID as varchar) +' as QueueID
			,'+ CAST(r.ReportID as varchar) + ' as ReportID 
			,-1 as GroupID
			,id.ServiceUnit
			,id.ServiceOrder
			, '+Cast(r.PriorityValue as varchar)+' as PriorityValue
			,DATEADD(Day,1,CAST(''''''+@date+'''''' as date)) as DueDate
			, CAST(''''''+@datetime+'''''' as datetime) as CreatedDate
			, CAST(''''''+@datetime+'''''' as datetime) as UpdatedDate
			,''''SYSTEM'''' as CreateEnterpriseID  
			,''''SYSTEM'''' as UpdatedEnterpriseID 
			,'''''+CAST(@Frequency as varchar)+''''' as Frequency
			,'+CAST(@FrequencyModifier as varchar)+' as FrequencyModifer
			,0 as ReadyForUploadFlag
			,0 as UploadedFlag
		FROM  ' + CAST(r.QueryFrom as varchar(max)) +
				CASE 
					WHEN r.QueryWhere <> '' 
					THEN ' WHERE  ' + REPLACE(CAST(r.QueryWhere as varchar(max)),'''','''''')
				END +
				CASE 
					WHEN r.QueryGroupBy <> '' 
					THEN ' GROUP BY ' + CAST(r.QueryGroupBy as varchar(max))
					ELSE '' 
				END + '; 

		''
			/*SELECT @finalQuery*/

			EXEC(@finalQuery)
		' as Query
	--SELECT r.ReportID, r.ReportName,q.QUeueID, q.QueueName
	FROM
		ServiceOrder.dbo.TBL_ServiceOrderAssignment as a with (nolock)
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderReports as r with (nolock) ON a.ReportID = r.ReportID
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderQueue as q with (nolock) ON a.QueueID = q.QueueID
		LEFT JOIN (
			SELECT
				o.QueueID,
				COUNT(Distinct o.OptionID) as openOptions
			FROM 
				ServiceOrder.dbo.TBL_ServiceOrderQueueOptions as o with (nolock) 
				INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderQueueOptions as oNext with (nolock) ON o.QueueID = oNext.QueueID AND o.NextSetID = oNext.SetID
			WHERE
				o.closed = 0
			GROUP BY
				o.QueueID
		) as oOpen ON q.QueueID = oOpen.QueueID
		INNER JOIN (
			SELECT
				o.QueueID,
				COUNT(Distinct o.OptionID) as closedOptions
			FROM 
				ServiceOrder.dbo.TBL_ServiceOrderQueueOptions as o with (nolock) 
			WHERE
				o.closed = 1
			GROUP BY
				o.QueueID
		) as oClosed ON q.QueueID = oClosed.QueueID
		INNER JOIN (
			SELECT  
				COUNT(OptionID) as TotalOptions
				,QueueID 
			FROM 
				ServiceOrder.dbo.TBL_ServiceOrderQueueOptions 
			GROUP BY 
				QueueID
		) as ototal ON ototal.QueueID = q.QueueID
	WHERE 
		COALESCE(oOpen.openOptions,0) + COALESCE(oClosed.closedOptions,0) = ototal.TotalOptions
		AND r.reportid <> -1
		AND q.IsEnabled = 1
		AND r.IsEnabled = 1	
		AND r.Frequency = @Frequency
		AND r.FrequencyModifier = @FrequencyModifier
	GROUP BY
		r.ReportID
		,q.QueueID
		,r.ReportName
		, q.QueueName
		,r.PriorityValue
		,r.QuerySelect
		,r.QueryFrom
		,r.QueryWhere
		,r.QueryGroupBy
		,r.QueryOrderBy


	/*Open query with cusor identifier to loop through results*/

	OPEN QueryCursor
	FETCH NEXT FROM QueryCursor INTO  @SQL
	
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXEC (@SQL)
		FETCH NEXT FROM QueryCursor INTO  @SQL
	END
	CLOSE QueryCursor
	DEALLOCATE QueryCursor;			
END






GO
/****** Object:  StoredProcedure [dbo].[PROC_TaskCreationStep2UpdateGroups]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



/*
Developed by: 
	Preston Galvanek
Created Date: 
	2018-06-14
Description:
	Populate Populate group id's for each staging table service order task
*/

CREATE PROCEDURE [dbo].[PROC_TaskCreationStep2UpdateGroups]
	@Frequency as varchar(4) = NULL,
	@FrequencyModifier as int = NULL,
	@ErrorCode varchar(150) = NULL OUTPUT
AS
BEGIN 

	/*Reassign values when blank to NULL */
	IF @FrequencyModifier = 0
		SET @FrequencyModifier = NULL
	IF @Frequency = ''
		SET @Frequency = NULL	

	/*Error Handling*/
	IF @Frequency Is Null /* Return error code if @Frequency is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for Frequency, it is Required.'
		Return
	END
	IF @FrequencyModifier Is Null /* Return error code if @ExecutionGroup is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for FrequencyModifier, it is Required.'
		Return
	END

	/*Declare variables*/
	DECLARE @SQL as varchar(max)

	/*Iterate through query results*/
	DECLARE QueryCursor CURSOR for
	
	--DECLARE @Frequency as varchar(4) = 'MI'
	--DECLARE @FrequencyModifier as int = 1
	SELECT 
		--ts.QueueID,
		--ts.ReportID,
		--g.GroupID,
	
		'
			DECLARE @date as varchar(15)
			DECLARE @datetime as varchar(30)

			SET @date = CAST(CAST(getdate() as date) as varchar)
			SET @datetime = CONVERT(varchar,GETDATE(),121)
			DECLARE @cols NVARCHAR(2000) 
			DECLARE @colsDynamic NVARCHAR(2000)
			DECLARE @name NVARCHAR(255)
			DECLARE @priorColumn NVARCHAR(255)
			DECLARE @pos INT
			DECLARE @pos2 INT
			DECLARE @keyvaluepair NVARCHAR(255)
			DECLARE @column NVARCHAR(255)
			DECLARE @value NVARCHAR(255)
			DECLARE @counter INT
			DECLARE @colQuery as varchar(max)
			DECLARE @finalQuery as varchar(max)

			SELECT 
				@cols = STUFF((
				SELECT DISTINCT TOP 100 Percent
						'','' + CAST(col.Column_Name as varchar(50)) + ''|'' + CAST(g.ControlValue as varchar)
				FROM 
					ServiceOrder.dbo.TBL_ControlBuilder as c with (nolock)
					INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderGroupItems as g with (nolock) on c.ControlID = g.ControlID
					INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderGroup as grp with (nolock) on grp.GroupID = g.GroupID
					INNER JOIN ServiceOrder.INFORMATION_SCHEMA.COLUMNS as col with (nolock) on c.QueryColumn = col.Column_Name 
					INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderAssignment as a with (nolock)  on a.GroupID = g.GroupID
				WHERE 
					col.TABLE_NAME = N''VIEW_ServiceOrder''
					AND a.QueueID = ' + CAST(ts.QueueID as varchar) + '
					AND a.ReportID = ' + CAST(ts.ReportID as varchar) + '
					AND a.GroupID = ' + CAST(g.GroupID as varchar) + '
					AND grp.IsEnabled = 1
				ORDER BY
					'','' + CAST(col.Column_Name as varchar(50)) + ''|'' + CAST(g.ControlValue as varchar)
					FOR XML PATH('''')   
					), 1, 1, '''')

			/*
			QA: Display column list for each iteration
			SELECT @cols
			*/

			/*Reassign dynamic cols variable to default and counter to 0*/
			SELECT @colsDynamic = @cols
			SELECT @counter = 0
			SELECT @colQuery = ''''
			SELECT @priorcolumn = ''''
				WHILE CHARINDEX('','', @colsDynamic) > 0
					BEGIN
					SELECT @counter = COALESCE(@counter,0) + 1
					SELECT @pos  = CHARINDEX('','', @colsDynamic) 
					SELECT @pos2  = CHARINDEX(''|'', @colsDynamic) 
					SELECT @keyvaluepair  = SUBSTRING(@colsDynamic, 1, @pos) 
					SELECT @column = SUBSTRING(@colsDynamic, 1, @pos2-1)
					SELECT @value  = SUBSTRING(@keyvaluepair,@pos2+1,@pos - @pos2-1 )
					SELECT @name = 
					CASE 
						WHEN @priorcolumn = ''''
						THEN  '' AND (id.'' + @column + '' = ''''''+ @value + ''''''''
						WHEN @column = @priorcolumn
						THEN  '' OR id.'' + @column + '' = ''''''+ @value + ''''''''
						ELSE  '') AND (id.'' + @column + '' = ''''''+ @value + ''''''''
					END
					SELECT @colQuery = COALESCE(@colQuery,'''') + COALESCE(@name,'''')
					SELECT @colsDynamic = SUBSTRING(@colsDynamic, @pos+1, LEN(@colsDynamic)-@pos)
					SELECT @priorColumn = @column
					END

				/* 
				SELECT SUBSTRING(@colsDynamic, 1, CHARINDEX(''|'', @colsDynamic)-1)
				SELECT @priorColumn
				*/

				SELECT 
					@colQuery = @colQuery + 
					CASE
						WHEN 
							@priorcolumn = ''''
						THEN 
							'' AND id.'' + SUBSTRING(@colsDynamic, 1, CHARINDEX(''|'', @colsDynamic)-1) + '' = '''''' + SUBSTRING(@colsDynamic, CHARINDEX(''|'', @colsDynamic)+1, len( @colsDynamic) -CHARINDEX(''|'', @colsDynamic)) + '''''' ''
						WHEN 
							SUBSTRING(@colsDynamic, 1, CHARINDEX(''|'', @colsDynamic)-1) = @priorColumn
						THEN 
							'' OR id.'' + SUBSTRING(@colsDynamic, 1, CHARINDEX(''|'', @colsDynamic)-1) + '' = '''''' + SUBSTRING(@colsDynamic, CHARINDEX(''|'', @colsDynamic)+1, len( @colsDynamic) -CHARINDEX(''|'', @colsDynamic)) + '''''') ''
						ELSE
							'' AND id.'' + SUBSTRING(@colsDynamic, 1, CHARINDEX(''|'', @colsDynamic)-1) + '' = '''''' + SUBSTRING(@colsDynamic, CHARINDEX(''|'', @colsDynamic)+1, len( @colsDynamic) -CHARINDEX(''|'', @colsDynamic)) + '''''') ''
					END
		
			/*SELECT @colQuery*/

			SET @finalQuery = ''
			UPDATE
				ts
			SET 
				GroupID = '+ CAST(g.GroupID as varchar) +',
				ReadyForUploadFlag = 1
			FROM  
				' + CAST(r.QueryFrom as varchar(max)) + ' 
				INNER JOIN  ServiceOrder.dbo.TBL_ServiceOrderTasksStage as ts on id.ServiceUnit = ts.ServiceUnit and id.ServiceOrder = ts.ServiceOrder
			WHERE
				ts.Frequency = '''''+CAST(@Frequency as varchar)+'''''
				AND ts.FrequencyModifier = '+CAST(@FrequencyModifier as varchar)+'
				AND ts.QueueID = '+CAST(ts.QueueID as varchar)+'
				AND ts.ReportID = '+CAST(ts.ReportID as varchar)+'
				AND ts.GroupID = -1
			''+@colQuery +''
			''
			EXEC(@finalQuery);
		' as Query
	FROM 
		ServiceOrder.dbo.TBL_ServiceOrderTasksStage as ts /*PGALVAN: we can remove the TBL_ServiceOrderTasksStage and just query against the TBL_ServiceOrderAssignment table (optimized execution plan), however it would require all queues, regardless of task inclusion within the stage, to process an update*/
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderAssignment as a with (nolock) on ts.queueid = a.queueID and ts.reportID = a.reportID
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderGroup as g with (nolock) ON a.GroupID = g.GroupID
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderReports as r with (nolock) ON a.ReportID = r.ReportID
	WHERE
		g.IsEnabled = 1
		AND g.GroupID <> -1
		AND ts.Groupid = -1
		AND EXISTS (SELECT TOP 1 Items.GroupID FROM ServiceOrder.dbo.TBL_ServiceOrderGroupItems as items with (nolock) WHERE items.GroupID = g.GroupID)
		AND ts.Frequency = @Frequency
		AND ts.FrequencyModifier = @FrequencyModifier
	GROUP BY
		ts.QueueID,
		ts.ReportID,
		g.GroupID,
		r.QueryFrom
	ORDER BY 
		ts.QueueID DESC,
		ts.ReportID DESC,
		g.GroupID DESC


	/*Open query with cusor identifier to loop through results*/

	OPEN QueryCursor
	FETCH NEXT FROM QueryCursor INTO  @SQL
	
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXEC (@SQL)
		FETCH NEXT FROM QueryCursor INTO  @SQL
	END
	CLOSE QueryCursor
	DEALLOCATE QueryCursor;		
	
	/*Update -1 groups as ready to upload as we have processed all groups*/
	UPDATE
		ServiceOrder.dbo.TBL_ServiceOrderTasksStage 
	SET 
		ReadyForUploadFlag = 1
	WHERE
		Frequency = @Frequency
		AND FrequencyModifier = @FrequencyModifier
		AND GroupID = -1
		
END






GO
/****** Object:  StoredProcedure [dbo].[PROC_TaskCreationStep3InsertIntoProd]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*

Task Creation - Step 3

Developed by: 
	Preston Galvanek
Created Date: 
	2018-06-15
Description:
	Populate Populate completed staging tasks into prod tasks table, log table and claim table and update all foreign keys
Notes:
	The date and time for the initial insert into the below table and columns must be the same for reporting: 
		TBL_ServiceOrderTasks.CreatedDate 
		TBL_ServiceOrderTasksLog.CreateDateTime
		TBL_ServiceOrderTasksClaimed.ModifiedDateTime
*/

CREATE PROCEDURE [dbo].[PROC_TaskCreationStep3InsertIntoProd]
	@Frequency as varchar(4) = NULL,
	@FrequencyModifier as int = NULL,
	@ErrorCode varchar(150) = NULL OUTPUT
AS
BEGIN 

	/*Reassign values when blank to NULL */
	IF @FrequencyModifier = 0
		SET @FrequencyModifier = NULL
	IF @Frequency = ''
		SET @Frequency = NULL	

	/*Error Handling*/
	IF @Frequency Is Null /* Return error code if @Frequency is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for Frequency, it is Required.'
		Return
	END
	IF @FrequencyModifier Is Null /* Return error code if @ExecutionGroup is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for FrequencyModifier, it is Required.'
		Return
	END


	/*

Merge Staging table into Primary Tasks Table

*/
MERGE 
	Serviceorder.dbo.TBL_ServiceOrderTasks t
USING 
--SELECT * FROM Serviceorder.dbo.TBL_ServiceOrderTasks t RIGHT JOIN
(
	--DECLARE @Frequency  varchar(4) = 'MI'
	--DECLARE @FrequencyModifier  int = 10
	SELECT
		t.TaskID,ts.QueueID,ts.ReportID,ts.GroupID,ts.ServiceUnit,ts.ServiceOrder,ts.PriorityValue,ts.DueDate,ts.CreatedDate,ts.UpdatedDate,ts.CreateEnterpriseID,ts.UpdateEnterpriseID
	FROM
		ServiceOrder.dbo.TBL_ServiceOrderTasksStage as ts with (nolock)
		LEFT JOIN (
			SELECT t.TaskID, t.ServiceUnit, t.ServiceOrder, t.QueueID, t.ReportID
			FROM 
				Serviceorder.dbo.TBL_ServiceOrderTasks as t with (nolock)
				INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderTasksLog as l with (nolock) on t.LogID = l.LogID
				INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderQueueOptions as o with (nolock) on o.OptionID = l.OptionID 
			WHERE
				(
					(
						COALESCE(o.closed,0) = 1
						AND t.UpdatedDate >= CAST(GETDATE()-1 as date)
					)
					OR
					(
						COALESCE(o.closed,0) = 0
					)
				)
			) as t on ts.ServiceUnit = t.ServiceUnit and ts.ServiceOrder = t.ServiceOrder and ts.QueueID = t.QueueID and ts.ReportID = t.ReportID
	WHERE 
		ts.Frequency = @Frequency
		AND ts.FrequencyModifier = @FrequencyModifier
		AND ts.ReadyForUploadFlag = 1
) s
ON  
	t.TaskID = s.TaskID
WHEN MATCHED THEN
UPDATE 
	SET 
		t.UpdatedDate = CAST(GETDATE() as datetime) 
		,t.UpdateEnterpriseID = 'SYSTEM'
WHEN NOT MATCHED BY TARGET THEN
	INSERT 
		(QueueID,ReportID,GroupID,ServiceUnit,ServiceOrder,PriorityValue,DueDate,CreatedDate,UpdatedDate,CreateEnterpriseID,UpdateEnterpriseID)
	VALUES
		(s.QueueID,s.ReportID,s.GroupID,s.ServiceUnit,s.ServiceOrder,s.PriorityValue,s.DueDate,s.CreatedDate,s.UpdatedDate,s.CreateEnterpriseID,s.UpdateEnterpriseID);

/*

Log data insert records

*/
MERGE 
	ServiceOrder.dbo.TBL_ServiceOrderTasksLog AS D
USING 
	(
		SELECT 
			t.TaskID, -1 as OptionID, 'SYSTEM' as EnterpriseID, t.UpdatedDate as ModifiedDateTime, 'SYSTEM' as CreateEnterpriseID,t.CreatedDate as CreateDateTime
		FROM  
			ServiceOrder.dbo.TBL_ServiceOrderTasks as t with (nolock)
		WHERE 
			CAST(t.CreatedDate as date) = CAST(GETDATE() as date)
	) as S
ON (
	D.TaskID = S.TaskID
	)
WHEN NOT MATCHED BY TARGET
	THEN INSERT(TaskID, OptionID,EnterpriseID, ModifiedDateTime, CreateEnterpriseID,CreateDateTime)
	VALUES(S.TaskID, S.OptionID,S.EnterpriseID, S.ModifiedDateTime,S.CreateEnterpriseID, S.CreateDateTime);



/*

Log IDs update for task table

*/
UPDATE 
	t 
SET 
	LogID = l.LogID
--SELECT *
FROM    
	ServiceOrder.dbo.TBL_ServiceOrderTasks as t
	INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderTasksLog as l  ON l.TaskID = t.TaskID    
	INNER JOIN (    
		SELECT    
			TaskID,  
			MAX(CreateDateTime) as CreateDateTime   
		FROM   
			ServiceOrder.dbo.TBL_ServiceOrderTasksLog as l    
		GROUP BY    
			TaskID  
	) as lm on l.TaskID = lm.TaskID and l.CreateDateTime = lm.CreateDateTime    
WHERE 
	t.LogID is null

	
/*

Claim data insert process

*/
MERGE 
	ServiceOrder.dbo.TBL_ServiceOrderTasksClaimed AS D
USING 
	(
		SELECT
			l.LogID, NULL as ClaimedEnterpriseID, 'SYSTEM' as ModifiedEnterpriseID, CAST(t.CreatedDate as datetime) as ModifiedDateTime, t.TaskID
		FROM	
			ServiceOrder.dbo.TBL_ServiceOrderTasks as t with (nolock)   
			INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderTasksLog as l with (nolock) on t.LogID = l.LogID
		WHERE 
			CAST(t.CreatedDate as date) = CAST(GETDATE() as date)			
	) as S
ON (
	D.LogID = S.LogID
	)
WHEN NOT MATCHED BY TARGET
	THEN INSERT(LogID, ClaimedEnterpriseID,ModifiedEnterpriseID, ModifiedDateTime, TaskID)
	VALUES(S.LogID, ClaimedEnterpriseID, ModifiedEnterpriseID, S.ModifiedDateTime, S.TaskID);

/*

ClaimID update Task Table

*/
UPDATE 
	t 
SET 
	ClaimID = tc.ClaimID
--SELECT *
FROM    
	ServiceOrder.dbo.TBL_ServiceOrderTasks as t
	INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderTasksLog as l  ON l.TaskID = t.TaskID    
	INNER JOIN (    
		SELECT    
			TaskID,  
			MAX(CreateDateTime) as CreateDateTime   
		FROM   
			ServiceOrder.dbo.TBL_ServiceOrderTasksLog as l    
		GROUP BY    
			TaskID  
	) as lm on l.TaskID = lm.TaskID and l.CreateDateTime = lm.CreateDateTime    
   	INNER JOIN ServiceOrder.dbo.TBl_ServiceOrderTasksClaimed as tc  ON tc.LogID = l.LogID 
	INNER JOIN (     
		SELECT     
			LogID,    
			MAX(ModifiedDateTime) as ModifiedDateTime    
		FROM    
			ServiceOrder.dbo.TBl_ServiceOrderTasksClaimed    
		GROUP BY     
			LogID    
	) as tcm on tc.LogID = tcm.LogID and tc.ModifiedDateTime = tcm.ModifiedDateTime   
WHERE 
	t.ClaimID is null 


		
END






GO
/****** Object:  StoredProcedure [dbo].[PROC_TaskCreationStep4AutoClose]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



/*

Task Creation - Step 4

Developed by: 
	Preston Galvanek
Created Date: 
	2018-11-6
Description:
	Close all tasks that meet the queue auto close criteria.
Notes:
	
*/

CREATE PROCEDURE [dbo].[PROC_TaskCreationStep4AutoClose]
	@Frequency as varchar(4) = NULL,
	@FrequencyModifier as int = NULL,
	@ErrorCode varchar(150) = NULL OUTPUT
AS
BEGIN 
/*Reassign values when blank to NULL */
	IF @FrequencyModifier = 0
		SET @FrequencyModifier = NULL
	IF @Frequency = ''
		SET @Frequency = NULL	

	/*Error Handling*/
	IF @Frequency Is Null /* Return error code if @Frequency is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for Frequency, it is Required.'
		Return
	END
	IF @FrequencyModifier Is Null /* Return error code if @ExecutionGroup is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for FrequencyModifier, it is Required.'
		Return
	END

	/*Declare variables*/
	DECLARE @SQL as varchar(max)

	/*Iterate through query results*/
	DECLARE QueryCursor CURSOR for
	
	--DECLARE @Frequency  varchar(4) = 'MI' --QA Only
	--DECLARE @FrequencyModifier  int = 10 --QA Only
	/*Iterate through query results*/
	SELECT DISTINCT
	'
	/*Running Queue: '+CAST(q.QueueID as varchar)+'*/
	DECLARE @finalQuery as varchar(max)

	/*SELECT @colQuery*/
	SET @finalQuery = ''
		/*Step 1.1 : Identify all auto close queues on report criteria*/
		INSERT INTO 
			ServiceOrder.dbo.TBL_ServiceOrderTasksLog (TaskID, OptionID, EnterpriseID, ModifiedDateTime,CreateEnterpriseID,CreateDateTime) 
		 SELECT  
			t.TaskID 
			,-2 /*default closed value*/ 
			,''''SYSTEM'''' 
			,GETDATE() 
			,''''SYSTEM''''
			,GETDATE() 
		FROM (
				SELECT
					*
				FROM 
					ServiceOrder.dbo.TBL_ServiceOrderTasksStage as ts 
				WHERE 
					ts.QueueID = '+CAST(q.QueueID as varchar)+'
					AND ts.Frequency = '''''+CAST(@Frequency as varchar)+'''''
					AND ts.FrequencyModifier = '''''+CAST(@FrequencyModifier as varchar)+'''''
			) as ts
			FULL JOIN (
				SELECT 
					t.TaskID, t.ServiceUnit, t.ServiceOrder, t.QueueID, t.ReportID, t.GroupID, t.CreatedDate, s.SupValue, lc.Description
				FROM 
					Serviceorder.dbo.TBL_ServiceOrderTasks as t 
					INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderTasksLog as l  on t.LogID = l.LogID
					INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderQueueOptions as o on o.OptionID = l.OptionID 
					INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderQueue as q  on t.QueueID = q.QueueID
					INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderReports as r ON t.ReportID = r.ReportID
					INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderGroup as g on t.GroupID = g.GroupID  
					INNER JOIN ServiceOrder.dbo.TBL_SupplementaryValues as s  ON s.OwnerID = q.QueueID 
					INNER JOIN ServiceOrder.dbo.TBL_LookupCodes as lc on s.SupValue = lc.NumericValue 
				WHERE 
					s.OwnerType = ''''QueueIDAutoClose''''
					AND lc.OwnerType = ''''SP_QueueAutoClose''''
					AND t.QueueID = '+CAST(q.QueueID as varchar)+'
					AND s.SupValue = 200 /*Remove items no longer meeting report criteria*/
					AND o.closed = 0 
					AND q.IsEnabled in (1,2)
					AND r.IsEnabled = 1
					AND g.IsEnabled = 1
					AND t.ReportID <> -1 /*Exclude no report selection */
			) as t on ts.ServiceUnit = t.ServiceUnit and ts.ServiceOrder = t.ServiceOrder and ts.QueueID = t.QueueID and ts.ReportID = t.ReportID AND ts.GroupID = t.GroupID
		WHERE 
			ts.ServiceOrder is null

		/*Step 1.2 : Identify all auto close queues on age*/
		INSERT INTO 
			ServiceOrder.dbo.TBL_ServiceOrderTasksLog (TaskID, OptionID, EnterpriseID, ModifiedDateTime,CreateEnterpriseID,CreateDateTime) 
		 SELECT  
			t.TaskID 
			,-2 /*default closed value*/ 
			,''''SYSTEM'''' 
			,GETDATE() 
			,''''SYSTEM''''
			,GETDATE() 
		FROM (
				SELECT DISTINCT
					t.TaskID, t.ServiceUnit, t.ServiceOrder, t.QueueID, t.ReportID, t.CreatedDate
				FROM 
					Serviceorder.dbo.TBL_ServiceOrderTasks as t 
					INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderTasksLog as l  on t.LogID = l.LogID
					INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderQueueOptions as o on o.OptionID = l.OptionID 
					INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderQueue as q  on t.QueueID = q.QueueID
					INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderReports as r ON t.ReportID = r.ReportID
					INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderGroup as g on t.GroupID = g.GroupID  
					INNER JOIN ServiceOrder.dbo.TBL_SupplementaryValues as s  on  s.OwnerID = q.QueueID 
					INNER JOIN ServiceOrder.dbo.TBL_LookupCodes as lc on s.SupValue = lc.NumericValue 
				WHERE 
					s.OwnerType = ''''QueueIDAutoClose''''
					AND lc.OwnerType = ''''SP_QueueAutoClose''''
					AND t.QueueID = '+CAST(q.QueueID as varchar)+'
					AND s.SupValue in (10,20,30) /*10, 20, 30 days old*/
					AND t.CreatedDate <= DATEADD(day,-s.SupValue,CAST(GETDATE() as date))
					AND NOT EXISTS (SELECT * FROM ServiceOrder.dbo.TBL_ServiceOrderTasksLog as l where l.TaskID = t.TaskID AND l.OptionID = -2 )
					AND o.closed = 0 
					AND q.IsEnabled in (1,2)
					AND r.IsEnabled = 1
					AND g.IsEnabled = 1
			) as t 

		/*Step 2: Update Task Table with New Entries into Log*/
		UPDATE 
			t 
		SET 
			LogID = l.LogID
		--SELECT *
		FROM    
			ServiceOrder.dbo.TBL_ServiceOrderTasks as t
			INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderTasksLog as l  ON l.TaskID = t.TaskID    
			INNER JOIN (    
				SELECT    
					TaskID,  
					MAX(CreateDateTime) as CreateDateTime   
				FROM   
					ServiceOrder.dbo.TBL_ServiceOrderTasksLog as l    
				GROUP BY    
					TaskID  
			) as lm on l.TaskID = lm.TaskID and l.CreateDateTime = lm.CreateDateTime    
		WHERE
			t.queueid = '+CAST(q.QueueID as varchar)+'
			AND t.LogID <> l.logid

		/*Step 3: insert new claim entry for new log entries for -2 option or manually closed orders today*/
		INSERT INTO ServiceOrder.dbo.TBL_ServiceOrderTasksClaimed (LogID,TaskID,ClaimedEnterpriseID,ModifiedEnterpriseID,ModifiedDateTime) 
		SELECT 
			l.LogID,t.TaskID, NULL,''''SYSTEM'''' as ModEnterpriseID, GETDATE() as ModDate 
		FROM	
			ServiceOrder.dbo.TBL_ServiceOrderTasks as t with (nolock) 
			INNER JOIN (  
				SELECT   
					l.LogID, 
					l.TaskID,  
					l.OptionID,  
					l.CreateDateTime,
					l.CreateEnterpriseID  
				FROM   
					ServiceOrder.dbo.TBL_ServiceOrderTasksLog as l with (nolock)   
					INNER JOIN (    
						SELECT    
                			TaskID,   
                			MAX(CreateDateTime) as CreateDateTime   
						FROM   
                			ServiceOrder.dbo.TBL_ServiceOrderTasksLog as l with (nolock)    
						GROUP BY    
                			TaskID   
					) as lm on l.TaskID = lm.TaskID and l.CreateDateTime = lm.CreateDateTime   
			) as l ON l.TaskID = t.TaskID   
		WHERE 
			l.OptionID = -2
			and t.queueid = '+CAST(q.QueueID as varchar)+'
			AND NOT EXISTS (SELECT * FROM ServiceOrder.dbo.TBL_ServiceOrderTasksClaimed where logid = l.LogID)

		/*Step 4: Update task table claim ID with new claimed record that was created */
		/*Update all ClaimIDs*/
		UPDATE 
			t 
		SET 
			ClaimID = tc.ClaimID
		--SELECT *
		FROM    
			ServiceOrder.dbo.TBL_ServiceOrderTasks as t 
   			INNER JOIN ServiceOrder.dbo.TBl_ServiceOrderTasksClaimed as tc  ON tc.LogID = t.LogID 
			INNER JOIN (     
				SELECT     
					LogID,    
					MAX(ModifiedDateTime) as ModifiedDateTime    
				FROM    
					ServiceOrder.dbo.TBl_ServiceOrderTasksClaimed    
				GROUP BY     
					LogID    
			) as tcm on tc.LogID = tcm.LogID and tc.ModifiedDateTime = tcm.ModifiedDateTime    
		WHERE
			t.queueid = '+CAST(q.QueueID as varchar)+'
			AND t.ClaimID <> tc.ClaimID

		''
			--SELECT @finalQuery

			EXEC(@finalQuery)
		' as Query
	--SELECT DISTINCT r.ReportID, r.ReportName,q.QueueID, q.QueueName,a.groupid, g.groupname, r.Frequency, r.FrequencyModifier
	FROM
		ServiceOrder.dbo.TBL_ServiceOrderAssignment as a with (nolock)
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderReports as r with (nolock) ON a.ReportID = r.ReportID
		INNER JOIN ServiceOrder.dbo.TBL_ServiceOrderQueue as q with (nolock) ON a.QueueID = q.QueueID
		INNER JOIN ServiceOrder.dbo.TBL_Serviceordergroup as g on g.groupid = a.groupid
		INNER JOIN ServiceOrder.dbo.TBL_SupplementaryValues as s with (nolock)  on  s.OwnerID = q.QueueID 
	WHERE 
		s.OwnerType = 'QueueIDAutoClose'	
		AND s.SupValue in (200,10,20,30)
		AND q.IsEnabled in (1,2)
		AND g.isenabled = 1
		AND r.isenabled = 1
		AND r.Frequency = @Frequency
		and r.FrequencyModifier = @FrequencyModifier

	/*Open query with cusor identifier to loop through results*/

	OPEN QueryCursor
	FETCH NEXT FROM QueryCursor INTO  @SQL
	
	WHILE @@FETCH_STATUS = 0
		BEGIN
			EXEC (@SQL)
		FETCH NEXT FROM QueryCursor INTO  @SQL
	END
	CLOSE QueryCursor
	DEALLOCATE QueryCursor;			
END








GO
/****** Object:  StoredProcedure [dbo].[usp_Reorg]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PRocedure [dbo].[usp_Reorg]
as
SET NOCOUNT ON;

DECLARE @objectid int;

DECLARE @indexid int;

DECLARE @partitioncount bigint;

DECLARE @schemaname nvarchar(130);

DECLARE @objectname nvarchar(130);

DECLARE @indexname nvarchar(130);

DECLARE @partitionnum bigint;

DECLARE @indexcursor bigint;

DECLARE @frag float;

DECLARE @QUERY nvarchar(4000);

DECLARE @dbid smallint;

SET @dbid = DB_ID();

IF OBJECT_ID('tempdb..#FragmentedIndex') IS NOT NULL DROP TABLE #FragmentedIndex
SELECT

    [object_id] AS objectid,

    index_id AS indexid,

    partition_number AS partitionnum,

    avg_fragmentation_in_percent AS frag, page_count

INTO #FragmentedIndex

FROM sys.dm_db_index_physical_stats (@dbid, NULL, NULL , NULL, 'LIMITED')

WHERE avg_fragmentation_in_percent > 10.0  --Allow limited fragmentation

AND index_id > 0

AND page_count > 100 ORDER BY avg_fragmentation_in_percent DESC ;

select * from #FragmentedIndex

-- Declare the cursor for the list of indexcursor to be processed.

DECLARE indexcursor CURSOR FOR SELECT objectid,indexid, partitionnum,frag FROM #FragmentedIndex;

--Open the cursor.

OPEN indexcursor;

--Loop through the indexcursor.

WHILE (1=1)

BEGIN

FETCH NEXT

FROM indexcursor

INTO @objectid, @indexid, @partitionnum, @frag;

IF @@FETCH_STATUS < 0 BREAK;

SELECT @objectname = QUOTENAME(o.name), @schemaname = QUOTENAME(s.name)

FROM sys.objects AS o with (NOLOCK)

JOIN sys.schemas as s with (NOLOCK) ON s.schema_id = o.schema_id

WHERE o.object_id = @objectid;

SELECT @indexname = QUOTENAME(name)

FROM sys.indexes with (NOLOCK)

WHERE object_id = @objectid AND index_id = @indexid;

SELECT @partitioncount = count (*)

FROM sys.partitions with (NOLOCK)

WHERE object_id = @objectid AND index_id = @indexid;



SET @QUERY = N'ALTER INDEX ' + @indexname + N' ON ' + @schemaname + N'.' + @objectname + N' REORGANIZE';

IF @partitioncount > 1

SET @QUERY = @QUERY + N' PARTITION=' + CAST(@partitionnum AS nvarchar(10));

EXEC (@QUERY);

PRINT N'Executed: ' + @QUERY;

END

-- Close and deallocate the cursor.

CLOSE indexcursor;

DEALLOCATE indexcursor;
select * from #FragmentedIndex
-- Drop the temporary table.

DROP TABLE #FragmentedIndex;




GO
/****** Object:  UserDefinedFunction [dbo].[KeepOnlyChars]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*************************************************************************
	Function Name: 	KeepOnlyChars
	Type:			Scaler
	Primary User:	DELDERK
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Mon Dec 24 14:49:25 CST 2012 @909 /Internet Time/
	Updated by:		Dave Elderkin
	Information:	this function removes selected characters from a string.
	Format:			variable
	Length:			variable
	************************************************************************
	Version:		1.00
	Date:			Mon Dec 24 14:49:26 CST 2012 @909 /Internet Time/
	Updated By:		Dave Elderkin
	************************************************************************
	Version:		2.00
	Date:			Tue Nov 19 11:58:15 CST 2013 @790 /Internet Time/
	Updated By:		Dave Elderkin
	Changes:		Updated to accept varchar(max) instead of varchar(8000)
***************************************************************************/
CREATE Function [dbo].[KeepOnlyChars]
	(
	@InputString varchar(max) = Null, 
	@LettersToKeep varchar(max) = Null
	) 
	Returns varchar(max)
AS
Begin		
	if @InputString is null
		return null
	
    DECLARE @TempStor1 varchar(max)
    DECLARE @CharStor1 varchar(1)
    DECLARE @CurPlace int
    SET @TempStor1 = ''
    SET @CharStor1 = ''
	SET @CurPlace = 1
    While @CurPlace <=  Len(@InputString)
    	BEGIN    
		SET @CharStor1 = SUBSTRING(@InputString, @CurPlace, 1)
        If CHARINDEX(@CharStor1, @LettersToKeep) > 0
			SET @TempStor1 = @TempStor1 + @CharStor1
		SET @CurPlace = @CurPlace + 1
	END
	RETURN @TempStor1
END



GO
/****** Object:  UserDefinedFunction [dbo].[SplitCol]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[SplitCol]
(
	@RowData nvarchar(2000),
	@SplitOn nvarchar(5)
)  
RETURNS @RtnValue table 
(
	Id int identity(1,1),
	Data nvarchar(100)
) 
AS  
BEGIN 
	Declare @Cnt int
	Set @Cnt = 1

	While (Charindex(@SplitOn,@RowData)>0)
	Begin
		Insert Into @RtnValue (data)
		Select 
			Data = ltrim(rtrim(Substring(@RowData,1,Charindex(@SplitOn,@RowData)-1)))

		Set @RowData = Substring(@RowData,Charindex(@SplitOn,@RowData)+1,len(@RowData))
		Set @Cnt = @Cnt + 1
	End
	
	Insert Into @RtnValue (data)
	Select Data = ltrim(rtrim(@RowData))

	Return
END


GO
/****** Object:  Table [dbo].[_DELETE_20180601_TBL_ServiceOrderRealTimeFeedBack]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[_DELETE_20180601_TBL_ServiceOrderRealTimeFeedBack](
	[RTF_ID] [int] IDENTITY(1,1) NOT NULL,
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[RouteDate] [date] NOT NULL,
	[MemberName] [varchar](50) NULL,
	[MemberEmailAddress] [varchar](50) NULL,
	[MemberPhoneNumber] [varchar](50) NULL,
	[PMCheckCode] [varchar](50) NULL,
	[CallType] [varchar](50) NULL,
	[BrandName] [varchar](50) NULL,
	[FeedbackSource] [varchar](1) NULL,
	[FeedbackScore] [varchar](1) NULL,
	[MemberComments] [varchar](max) NULL,
	[FeedbackDate] [datetime2](7) NULL,
	[InsertDate] [datetime2](7) NULL,
	[RequestContact] [smallint] NULL,
	[CustomerID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC,
	[RouteDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[_DELETE_ON_20180401_TBL_ProductSalesRequests]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[_DELETE_ON_20180401_TBL_ProductSalesRequests](
	[ProductSalesRequestID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[CustomerFirstName] [varchar](50) NULL,
	[CustomerLastName] [varchar](50) NULL,
	[CustomerPhoneNumber] [char](10) NULL,
	[CustomerPhoneNumberSecondary] [char](10) NULL,
	[CustomerEmail] [varchar](50) NULL,
	[CustomerEmailSeconday] [varchar](50) NULL,
	[ItemSuffixNumber] [int] NULL,
	[CurrentApplianceDetails] [nvarchar](max) NULL,
	[CustomerNotes] [nvarchar](max) NULL,
	[CallBackDate] [date] NULL,
	[CallBackDateTimeFrom] [char](4) NULL,
	[CallBackDateTimeTo] [char](4) NULL,
	[CallBackRequested] [char](1) NULL,
	[GoodBetterBest] [nvarchar](max) NULL,
	[CouponInfo] [nvarchar](max) NULL,
	[CreatedDateTime] [datetime2](7) NULL,
	[CreatedBy] [varchar](10) NULL,
	[LoadDateTime] [datetime2](7) NULL,
	[ProcessDateTime] [datetime2](7) NULL,
	[ServiceUnit] [char](7) NULL,
	[ServiceOrder] [char](8) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[_DELETE_ON_20180501_TBL_EmailRequest]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[_DELETE_ON_20180501_TBL_EmailRequest](
	[EmailRequestID] [int] IDENTITY(1,1) NOT NULL,
	[OwnerType] [varchar](30) NOT NULL,
	[OwnerID] [int] NOT NULL,
	[Frequency] [varchar](4) NOT NULL,
	[CreatedEnterpriseID] [varchar](8) NOT NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[EmailStatusID] [int] NOT NULL,
	[EmailDateTime] [datetime] NULL,
 CONSTRAINT [PK_TBL_EmailRequest] PRIMARY KEY CLUSTERED 
(
	[EmailRequestID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[_DELETE_ON_20181001_RealTimeFeedbackStaging]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[_DELETE_ON_20181001_RealTimeFeedbackStaging](
	[responseid] [varchar](50) NULL,
	[respid] [varchar](50) NULL,
	[interview_start] [varchar](50) NULL,
	[interview_end] [varchar](50) NULL,
	[status] [varchar](50) NULL,
	[CustName] [varchar](50) NULL,
	[Rating] [varchar](50) NULL,
	[Mfg_bnd_nm] [varchar](50) NULL,
	[Payment_met] [varchar](50) NULL,
	[Date_Service] [varchar](50) NULL,
	[Source] [varchar](50) NULL,
	[Pm_chk_cd] [varchar](50) NULL,
	[CustEmlAdd] [varchar](max) NULL,
	[SV_Ord_No] [varchar](50) NULL,
	[SV_Ord_UnNo] [varchar](50) NULL,
	[Q1_1] [varchar](50) NULL,
	[Q2a_1] [varchar](50) NULL,
	[Q2a_2] [varchar](50) NULL,
	[Q2a_3] [varchar](50) NULL,
	[Q2a_4] [varchar](50) NULL,
	[Q2a_5] [varchar](50) NULL,
	[Q2a_6] [varchar](50) NULL,
	[Q2ao] [varchar](max) NULL,
	[Q2b_1] [varchar](50) NULL,
	[Q2b_2] [varchar](50) NULL,
	[Q2b_3] [varchar](50) NULL,
	[Q2b_4] [varchar](50) NULL,
	[Q2b_5] [varchar](50) NULL,
	[Q2b_6] [varchar](50) NULL,
	[Q2bo] [varchar](max) NULL,
	[Q3] [varchar](50) NULL,
	[Q4_1] [varchar](50) NULL,
	[Q4_2] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[JobCodes]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[JobCodes](
	[specialty] [varchar](50) NULL,
	[majorheading] [varchar](50) NULL,
	[subheading] [varchar](50) NULL,
	[jobcode] [varchar](50) NULL,
	[jobcodedescription] [varchar](50) NULL,
	[difficulty_level] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewProcs]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NewProcs](
	[thd_pty_id] [varchar](50) NULL,
	[thd_pty_ds] [varchar](100) NULL,
	[thd_pty_typ] [varchar](50) NULL,
	[client_nm] [varchar](50) NULL,
	[sub_client_nm] [varchar](50) NULL,
	[plan] [varchar](100) NULL,
	[deduct] [varchar](50) NULL,
	[tax_deduct] [varchar](50) NULL,
	[cvg_cd] [varchar](50) NULL,
	[thd_pty_id_typ] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[npsxtcl]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[npsxtcl](
	[svc_un_no] [char](7) NOT NULL,
	[so_no] [char](8) NOT NULL,
	[cus_no] [int] NULL,
	[loc_suf_no] [smallint] NOT NULL,
	[itm_suf_no] [int] NULL,
	[cus_typ_cd] [char](1) NULL,
	[cus_nm] [varchar](50) NULL,
	[cus_ad] [varchar](50) NULL,
	[cus_cty_nm] [varchar](12) NULL,
	[cus_st_cd] [char](2) NULL,
	[zip_cd] [char](5) NULL,
	[zip_suf_cd] [char](4) NULL,
	[cus_phn_no] [varchar](10) NULL,
	[cus_alt_phn_no] [varchar](10) NULL,
	[div_no] [char](3) NULL,
	[bnd_nm] [char](12) NULL,
	[mdl_no] [char](24) NULL,
	[srl_no] [char](20) NULL,
	[srs_sld_cd] [char](1) NULL,
	[srs_str_no] [char](7) NULL,
	[cus_pur_dt] [date] NULL,
	[cus_dvr_dt] [date] NULL,
	[prt_war_epr_dt] [date] NULL,
	[lab_war_epr_dt] [date] NULL,
	[exp_prt_war_epr_dt] [date] NULL,
	[rpr_loc_un_no] [char](7) NULL,
	[lst_act_cd] [char](1) NULL,
	[lst_act_ts] [datetime2](7) NULL,
 CONSTRAINT [PK_npsxtcl] PRIMARY KEY CLUSTERED 
(
	[svc_un_no] ASC,
	[so_no] ASC,
	[loc_suf_no] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[npsxtid]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[npsxtid](
	[SVC_UN_NO] [char](7) NOT NULL,
	[SO_NO] [char](8) NOT NULL,
	[RPR_TAG_BCD_NO] [char](7) NULL,
	[SVC_RTL_UN_NO] [char](7) NULL,
	[SVC_CUS_ID_NO] [int] NULL,
	[LOC_SUF_NO] [char](1) NULL,
	[ITM_SUF_NO] [int] NULL,
	[SYS_ITM_SUF_NO] [int] NULL,
	[AGR_SUF_NO] [int] NULL,
	[SMG_STR_STK_NO] [char](10) NULL,
	[SVC_MDS_BIN_LOC_CD] [char](10) NULL,
	[SVC_TYP_CD] [char](1) NULL,
	[IDY_CD] [char](2) NULL,
	[MDS_CD] [varchar](12) NULL,
	[BR_SVC_UN_NO] [char](7) NULL,
	[LCL_NTL_PRO_CD] [char](1) NULL,
	[TIR_PRC_SCH_NO] [char](2) NULL,
	[INQ_TAK_FL] [char](1) NULL,
	[SVC_OGP_CD] [char](1) NULL,
	[ORI_CD] [char](4) NULL,
	[ORI_THD_PTY_ID] [char](6) NULL,
	[FST_AVL_DT] [date] NULL,
	[PM_CHK_CD] [char](1) NULL,
	[LST_ACT_CD] [char](1) NULL,
	[LST_ACT_TS] [datetime2](7) NULL,
 CONSTRAINT [PK_npsxtid] PRIMARY KEY CLUSTERED 
(
	[SVC_UN_NO] ASC,
	[SO_NO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[npsxtld]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[npsxtld](
	[tbl_nm] [varchar](50) NOT NULL,
	[lst_act_ts] [datetime2](7) NULL,
	[lst_upd_ts] [datetime2](7) NULL,
 CONSTRAINT [PK_npsxtld] PRIMARY KEY CLUSTERED 
(
	[tbl_nm] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[npsxtot]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[npsxtot](
	[SVC_UN_NO] [char](7) NOT NULL,
	[SO_NO] [char](8) NOT NULL,
	[SVC_CAL_DT] [date] NOT NULL,
	[SVC_ATP_RSC_DT] [date] NULL,
	[SVC_ATP_RSC_FRM_TM] [char](8) NULL,
	[SVC_ATP_RSC_TO_TM] [char](8) NULL,
	[SVC_ATP_TEC_ARV_TM] [time](6) NULL,
	[SVC_ATP_END_CAL_TM] [time](6) NULL,
	[DWD_IND_CD] [char](2) NULL,
	[LST_ACT_CD] [char](1) NULL,
	[LST_ACT_TS] [datetime2](7) NULL,
 CONSTRAINT [PK_npsxtot] PRIMARY KEY CLUSTERED 
(
	[SVC_UN_NO] ASC,
	[SO_NO] ASC,
	[SVC_CAL_DT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[npsxtpm]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[npsxtpm](
	[SVC_UN_NO] [char](7) NOT NULL,
	[SO_NO] [char](8) NOT NULL,
	[SVC_CAL_DT] [date] NOT NULL,
	[SO_PY_MET_CD] [char](8) NOT NULL,
	[CUS_CRG_ACN_NO] [char](16) NULL,
	[CUS_CRG_ACN_EPR_DT] [date] NULL,
	[SVC_ORD_CLL_AM] [decimal](9, 4) NULL,
	[PY_CRG_APV_NO] [char](7) NULL,
	[PO_NO] [char](18) NULL,
	[LST_ACT_CD] [char](1) NULL,
	[LST_ACT_TS] [datetime2](7) NULL,
 CONSTRAINT [PK_npsxtpm] PRIMARY KEY CLUSTERED 
(
	[SVC_UN_NO] ASC,
	[SO_NO] ASC,
	[SVC_CAL_DT] ASC,
	[SO_PY_MET_CD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[npsxtpn]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[npsxtpn](
	[SVC_UN_NO] [char](7) NOT NULL,
	[SO_NO] [char](8) NOT NULL,
	[PRT_SEQ_NO] [int] NOT NULL,
	[SVC_DIV_NO] [char](3) NULL,
	[PRT_PRC_LIS_SRC_NO] [char](3) NULL,
	[SVC_PRT_NO] [char](24) NULL,
	[PRT_DS] [char](15) NULL,
	[SVC_PRT_TYP_CD] [char](1) NULL,
	[SVC_ATP_PRT_QT] [int] NULL,
	[SVC_PRT_QT] [int] NULL,
	[CVG_CD] [char](2) NULL,
	[PRT_SEL_PRC_AM] [decimal](9, 4) NULL,
	[SVC_PRT_STS_CD] [char](1) NULL,
	[SVC_PRT_STS_DT] [date] NULL,
	[SVC_PRT_WAR_SLD_FL] [char](1) NULL,
	[PRT_PRO_ID] [char](7) NULL,
	[PRT_ORD_DT] [date] NULL,
	[EMP_ID_NO] [char](7) NULL,
	[PRT_ORD_QT] [int] NULL,
	[PRT_RECD_QT] [int] NULL,
	[PRT_RECD_DT] [date] NULL,
	[PRT_INS_DT] [date] NULL,
	[PRT_STK_BIN_NO] [char](4) NULL,
	[TRK_BIN_NO] [char](3) NULL,
	[PRT_DEST_BIN_NO] [char](5) NULL,
	[ORD_LN_EA_PRC_AM] [decimal](9, 4) NULL,
	[PRT_CST_PRC_AM] [decimal](9, 4) NULL,
	[PRT_PRM_PRC_AM] [decimal](9, 4) NULL,
	[PRT_TRK_INQ_CD] [char](1) NULL,
	[MDS_CMP_TYP_CD] [char](3) NULL,
	[PRT_OBL_KEY] [char](6) NULL,
	[LST_ACT_CD] [char](1) NULL,
	[LST_ACT_TS] [datetime2](7) NULL,
 CONSTRAINT [PK_npsxtpn] PRIMARY KEY CLUSTERED 
(
	[SVC_UN_NO] ASC,
	[SO_NO] ASC,
	[PRT_SEQ_NO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[npsxtsa]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[npsxtsa](
	[SVC_UN_NO] [char](7) NOT NULL,
	[SO_NO] [char](8) NOT NULL,
	[SVC_CAL_DT] [date] NOT NULL,
	[SVC_TEC_EMP_NO] [char](7) NULL,
	[SVC_CAL_CD] [char](2) NULL,
	[SVC_AGR_CNT_STS_FL] [char](1) NULL,
	[SVC_AGR_CNT_SLS_FL] [char](1) NULL,
	[SVC_ATP_APL_MTR_QT] [decimal](9, 4) NULL,
	[TST_TM_QT] [int] NULL,
	[SVC_ATP_CMT_1_DS] [char](60) NULL,
	[SVC_ATP_CMT_2_DS] [char](60) NULL,
	[ETA_MET_FL] [char](1) NULL,
	[LST_ACT_CD] [char](1) NULL,
	[LST_ACT_TS] [datetime2](7) NULL,
	[TechnicianType] [int] NULL,
	[RescheduleCode] [char](4) NULL,
 CONSTRAINT [PK_npsxtsa] PRIMARY KEY CLUSTERED 
(
	[SVC_UN_NO] ASC,
	[SO_NO] ASC,
	[SVC_CAL_DT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[npsxtsj]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[npsxtsj](
	[SVC_UN_NO] [char](7) NOT NULL,
	[SO_NO] [char](8) NOT NULL,
	[JOB_CD_SEQ_NO] [int] NOT NULL,
	[JOB_CD] [char](5) NULL,
	[JOB_CD_CRG_CD] [char](1) NULL,
	[SVC_CAL_DT] [date] NULL,
	[CVG_CD] [char](2) NULL,
	[LAB_ENT_AM] [decimal](9, 4) NULL,
	[CRG_CD_NREL_FL] [char](1) NULL,
	[MDS_CMP_TYP_CD] [char](3) NULL,
	[JOB_OBL_KEY] [char](6) NULL,
	[LST_ACT_CD] [char](1) NULL,
	[LST_ACT_TS] [datetime2](7) NULL,
 CONSTRAINT [PK_npsxtsj] PRIMARY KEY CLUSTERED 
(
	[SVC_UN_NO] ASC,
	[SO_NO] ASC,
	[JOB_CD_SEQ_NO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[npsxtsr]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[npsxtsr](
	[SVC_UN_NO] [char](7) NOT NULL,
	[SO_NO] [char](8) NOT NULL,
	[WRK_ARE_CD] [char](3) NULL,
	[SO_PY_MET_CD] [char](2) NULL,
	[CUS_CRG_ACN_NO] [char](16) NULL,
	[CUS_CRG_ACN_EPR_DT] [date] NULL,
	[RPR_CRG_TYP_CD] [char](1) NULL,
	[RPR_CRG_FLT_RT_AM] [decimal](9, 4) NULL,
	[SVC_SCH_DT] [date] NULL,
	[SVC_SCH_FRM_TM] [char](8) NULL,
	[SVC_SCH_TO_TM] [char](8) NULL,
	[SVC_ORI_SCH_DT] [date] NULL,
	[SVC_ORI_SCH_FRM_TM] [char](8) NULL,
	[SVC_ORI_SCH_TO_TM] [char](8) NULL,
	[SVC_WT_TO_AVG_NO] [decimal](9, 4) NULL,
	[SO_CRT_DT] [date] NULL,
	[SO_CRT_TM] [time](6) NULL,
	[SO_CRT_UN_NO] [char](7) NULL,
	[SO_CRT_EMP_NO] [char](7) NULL,
	[SO_CUS_MAX_AM] [decimal](9, 4) NULL,
	[SVC_RQ_DS] [varchar](66) NULL,
	[SO_INS_1_DS] [varchar](60) NULL,
	[SO_INS_2_DS] [varchar](60) NULL,
	[CNT_NM] [char](30) NULL,
	[CNT_PHN_NO] [char](10) NULL,
	[SVC_EST_RQ_CD] [char](1) NULL,
	[SVC_EST_MIN_AM] [decimal](9, 4) NULL,
	[SVC_EST_MAX_AM] [decimal](9, 4) NULL,
	[SO_STS_CD] [char](2) NULL,
	[SO_TYP_CD] [char](4) NULL,
	[SO_PRI_CD] [char](1) NULL,
	[SO_HPR_NED_FL] [char](1) NULL,
	[SO_CRG_PER_CAL_AM] [decimal](9, 4) NULL,
	[MOD_EXT_DT] [datetime2](7) NULL,
	[MOD_ID] [char](7) NULL,
	[SPD_SLL_PRC_AM] [decimal](9, 4) NULL,
	[SO_TOT_AM] [decimal](9, 4) NULL,
	[EST_APV_DT] [date] NULL,
	[SO_QTE_CRG_AM] [decimal](9, 4) NULL,
	[CAL_LD_TYP_CD] [char](4) NULL,
	[MDS_SP_CD] [char](10) NULL,
	[TEC_SP_PRC_AM] [decimal](9, 4) NULL,
	[SVC_ORD_CLO_DT] [date] NULL,
	[LST_ACT_CD] [char](1) NULL,
	[LST_ACT_TS] [datetime2](7) NULL,
 CONSTRAINT [PK_npsxtsr] PRIMARY KEY CLUSTERED 
(
	[SVC_UN_NO] ASC,
	[SO_NO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[npsxtto]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[npsxtto](
	[SVC_UN_NO] [char](7) NOT NULL,
	[SO_NO] [char](8) NOT NULL,
	[THD_PTY_ID] [char](6) NOT NULL,
	[CTA_NO] [char](20) NULL,
	[CTA_EXP_DT] [date] NULL,
	[ATH_NO] [char](20) NULL,
	[SVC_ORD_RCL_FL] [char](1) NULL,
	[FRT_FAC_RT] [decimal](9, 4) NULL,
	[LAB_FLT_AM] [decimal](9, 4) NULL,
	[PRT_CST_TAX_AM] [decimal](9, 4) NULL,
	[LST_ACT_CD] [char](1) NULL,
	[LST_ACT_TS] [datetime2](7) NULL,
 CONSTRAINT [PK_npsxtto] PRIMARY KEY CLUSTERED 
(
	[SVC_UN_NO] ASC,
	[SO_NO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ServiceOrder]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ServiceOrder](
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[RecallDays] [int] NULL,
 CONSTRAINT [PK_ServiceOrder] PRIMARY KEY CLUSTERED 
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SSTOrderUploadServiceOrderJobCodes]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SSTOrderUploadServiceOrderJobCodes](
	[order_upload_id] [int] NOT NULL,
	[jobCodeSequence] [int] NOT NULL,
	[unit] [char](7) NULL,
	[orderNumber] [char](8) NULL,
	[routeDate] [date] NULL,
	[coverageCode] [varchar](250) NULL,
	[jobCode] [varchar](250) NULL,
	[chargeCode] [varchar](250) NULL,
	[relatedCodesFlag] [varchar](250) NULL,
	[calculatedPrice] [varchar](250) NULL,
	[oblKey] [varchar](250) NULL,
	[custPayChargeAmount] [varchar](250) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SSTOrderUploadStaging]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SSTOrderUploadStaging](
	[order_upload_id] [bigint] NOT NULL,
	[user_id] [varchar](8) NOT NULL,
	[nps_id] [varchar](7) NOT NULL,
	[unit_lu_id] [bigint] NOT NULL,
	[unit_number] [varchar](7) NOT NULL,
	[order_number] [varchar](8) NOT NULL,
	[route_date] [date] NOT NULL,
	[CallCode] [char](2) NULL,
	[RescheduleDate] [date] NULL,
	[rescheduleFromTime] [char](5) NULL,
	[rescheduleToTime] [char](5) NULL,
	[helperFlag] [char](1) NULL,
	[ArriveTime] [char](5) NULL,
	[CallCloseTime] [char](5) NULL,
	[PrintTime] [char](5) NULL,
	[thirdPartyExpirationDate] [date] NULL,
	[thirdPartyContractNumber] [varchar](20) NULL,
	[authorizationNumber] [varchar](20) NULL,
	[thirdPartyContractNumDelete] [char](1) NULL,
	[thirdPartyExpirationDateDelete] [char](1) NULL,
	[RescheduleReasonCode] [char](4) NULL,
	[spDeclineReasonCode] [char](1) NULL,
	[cancelReasonCode] [char](4) NULL,
	[specialInstructions] [varchar](250) NULL,
	[thirdPartyId] [char](6) NULL,
	[techComments] [varchar](250) NULL,
	[RescheduleStatus] [char](1) NULL,
	[partPickupFl] [char](1) NULL,
	[forceFl] [char](1) NULL,
	[extraDuration] [int] NULL,
	[OfferID] [int] NULL,
	[AttemptCount] [int] NULL,
	[emergencyFl] [char](1) NULL,
	[recallFl] [char](1) NULL,
	[rescheduleNoAttemptFl] [char](1) NULL,
	[returnTechType] [char](1) NULL,
	[helperCode] [char](1) NULL,
	[importanceAttemptCount] [int] NULL,
	[sywrMemberId] [varchar](20) NULL,
	[partInstaller] [char](1) NULL,
	[purchaseDate] [date] NULL,
	[purchaseLocation] [char](7) NULL,
	[deliveryDate] [date] NULL,
	[productReading] [varchar](250) NULL,
	[searsPurchase] [char](1) NULL,
	[productUsage] [char](1) NULL,
	[description] [varchar](250) NULL,
	[brand] [varchar](12) NULL,
	[model] [varchar](24) NULL,
	[serial] [varchar](20) NULL,
	[modelSerialNaReason] [char](4) NULL,
	[namePrefix] [varchar](4) NULL,
	[firstName] [varchar](50) NULL,
	[middleName] [varchar](50) NULL,
	[lastName] [varchar](50) NULL,
	[nameSuffix] [varchar](4) NULL,
	[address1] [varchar](50) NULL,
	[address2] [varchar](50) NULL,
	[apartment] [varchar](50) NULL,
	[city] [varchar](50) NULL,
	[state] [varchar](50) NULL,
	[zipCode] [varchar](50) NULL,
	[mainPhone] [varchar](50) NULL,
	[altPhone] [varchar](50) NULL,
	[customerInstructions] [varchar](250) NULL,
	[CustomerNumber] [int] NULL,
	[email] [varchar](250) NULL,
	[emailPromoFlag] [char](1) NULL,
	[spApproval] [char](1) NULL,
	[promoteOtherWithReason] [char](1) NULL,
	[laborTimeAndHalf] [char](1) NULL,
	[productSellingPrice] [decimal](9, 2) NULL,
	[GrandTotal] [decimal](9, 2) NULL,
	[prePaidAmount] [decimal](9, 2) NULL,
	[AmountCollected] [decimal](9, 2) NULL,
	[partsTaxPercent] [decimal](9, 3) NULL,
	[partsTax] [decimal](9, 2) NULL,
	[partsNetAmount] [decimal](9, 2) NULL,
	[laborTaxPercent] [decimal](9, 3) NULL,
	[laborTaxCollected] [decimal](9, 2) NULL,
	[laborTotal] [decimal](9, 2) NULL,
	[basicHomeCallCharge] [decimal](9, 3) NULL,
	[serviceProductDollars] [decimal](9, 2) NULL,
	[serviceProductTaxDollars] [decimal](9, 2) NULL,
	[spTotal] [decimal](9, 3) NULL,
	[acceptIndicator] [varchar](5) NULL,
	[reasonCodeEstimateDeclined] [char](4) NULL,
	[taxExemptNum] [varchar](50) NULL,
	[custPayTotal] [decimal](9, 2) NULL,
	[deductibleApplied] [decimal](9, 2) NULL,
	[newWaiveBasicCharge] [char](1) NULL,
	[partsAssociateDiscount] [decimal](9, 2) NULL,
	[partsPromotionDiscount] [decimal](9, 2) NULL,
	[partsCouponDiscount] [decimal](9, 2) NULL,
	[laborAssociateDiscount] [decimal](9, 2) NULL,
	[laborPromotionDiscount] [decimal](9, 2) NULL,
	[laborCouponDiscount] [decimal](9, 2) NULL,
	[spAssociateDiscount] [decimal](9, 2) NULL,
	[couponNumber] [varchar](50) NULL,
	[discountReasonCode1] [char](4) NULL,
	[discountReasonCode2] [char](4) NULL,
	[primaryPaymentMethod] [char](2) NULL,
	[primaryAmountCollected] [decimal](9, 2) NULL,
	[primaryExpirationDate] [varchar](10) NULL,
	[secondaryPaymentMethod] [char](2) NULL,
	[secondaryAmountCollected] [decimal](9, 2) NULL,
	[secondaryExpirationDate] [varchar](10) NULL,
	[payrollTransferStore] [char](7) NULL,
	[payrollTransferSubAccount] [varchar](3) NULL,
	[primaryToken] [varchar](50) NULL,
	[primarySettlementKey] [varchar](50) NULL,
	[secondaryToken] [varchar](50) NULL,
	[secondarySettlementKey] [varchar](50) NULL,
	[parts] [nvarchar](max) NULL,
	[jobCode] [nvarchar](max) NULL,
	[created_by] [varchar](255) NOT NULL,
	[created_ts] [datetime2](7) NOT NULL,
	[updated_by] [varchar](255) NOT NULL,
	[updated_ts] [datetime2](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[order_upload_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sstxtjc_stg]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sstxtjc_stg](
	[sp_cd] [varchar](10) NOT NULL,
	[job_cd] [decimal](5, 0) NOT NULL,
	[job_cd_crg_cd] [varchar](1) NOT NULL,
	[job_cd_cat_cd] [varchar](2) NOT NULL,
	[job_cd_ds] [varchar](25) NOT NULL,
	[atv_dt] [date] NOT NULL,
	[lst_upd_dt] [date] NULL,
	[dac_dt] [date] NULL,
	[cyi_fl] [varchar](1) NOT NULL,
	[ihm_fl] [varchar](1) NOT NULL,
	[itl_fl] [varchar](1) NOT NULL,
 CONSTRAINT [pk_sstxtjc_stg] PRIMARY KEY CLUSTERED 
(
	[sp_cd] ASC,
	[job_cd] ASC,
	[job_cd_crg_cd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_CiboodleCustomerSession]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_CiboodleCustomerSession](
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[SessionID] [int] NOT NULL,
	[CustomerID] [int] NOT NULL,
	[SessionStartDateTime] [datetime2](7) NOT NULL,
	[SessionEndDateTime] [datetime2](7) NOT NULL,
	[SessionLengthMinutes] [decimal](8, 2) NOT NULL,
	[TimeZoneCode] [char](1) NOT NULL,
	[SessionComments] [varchar](240) NOT NULL,
	[SessionLoadDateTime] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_TBL_CiboodleCustomerSession] PRIMARY KEY CLUSTERED 
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC,
	[SessionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_Comments]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Comments](
	[CommentID] [int] IDENTITY(1,1) NOT NULL,
	[OwnerType] [varchar](30) NOT NULL,
	[OwnerSubID] [int] NULL,
	[OwnerCategory] [int] NULL,
	[ServiceUnit] [varchar](7) NULL,
	[ServiceOrder] [varchar](8) NULL,
	[Comment] [varchar](max) NOT NULL,
	[CreatedEnterpriseID] [varchar](8) NOT NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[ModifiedEnterpriseID] [varchar](8) NOT NULL,
	[ModifiedDateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_TBL_Comments] PRIMARY KEY CLUSTERED 
(
	[CommentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_CompressorZips]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_CompressorZips](
	[ZipCode] [char](5) NOT NULL,
 CONSTRAINT [PK_TBL_CompressorZips] PRIMARY KEY CLUSTERED 
(
	[ZipCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ControlBuilder]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ControlBuilder](
	[ControlID] [int] IDENTITY(1,1) NOT NULL,
	[SortOrder] [int] NULL,
	[ControlSQLClause] [varchar](20) NULL,
	[ControlName] [varchar](50) NULL,
	[ControlObject] [varchar](50) NULL,
	[ControlQuantity] [int] NULL,
	[ControlDescription] [varchar](max) NULL,
	[ControlValidation] [varchar](250) NULL,
	[ControlValidationExpression] [varchar](250) NULL,
	[ControlValidationExpressionDesc] [varchar](250) NULL,
	[ControlValidationMin] [varchar](50) NULL,
	[ControlValidationMax] [varchar](50) NULL,
	[ControlValidationErrorMessage] [varchar](500) NULL,
	[ControlInitialValue] [varchar](50) NULL,
	[ControlInitialText] [varchar](50) NULL,
	[ControlType] [varchar](50) NULL,
	[ControlWildcard] [bit] NULL,
	[ControlExceptFlag] [bit] NULL,
	[ControlSelectColumn] [varchar](50) NULL,
	[ControlServer] [varchar](50) NULL,
	[ControlFromTable] [varchar](250) NULL,
	[ControlWhereColumn] [varchar](50) NULL,
	[ControlWhereFilterValue] [varchar](50) NULL,
	[ControlWhereFilterType] [varchar](50) NULL,
	[ControlCustomSQL] [varchar](max) NULL,
	[LookupCodeEnabled] [bit] NULL,
	[QueryColumn] [varchar](100) NULL,
 CONSTRAINT [PK_TBL_QueryClauseBuilderRawQA] PRIMARY KEY CLUSTERED 
(
	[ControlID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ControlBuilderBackup]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ControlBuilderBackup](
	[ControlID] [int] IDENTITY(1,1) NOT NULL,
	[SortOrder] [int] NULL,
	[ControlSQLClause] [varchar](20) NULL,
	[ControlName] [varchar](50) NULL,
	[ControlObject] [varchar](50) NULL,
	[ControlQuantity] [int] NULL,
	[ControlDescription] [varchar](max) NULL,
	[ControlValidation] [varchar](250) NULL,
	[ControlValidationExpression] [varchar](250) NULL,
	[ControlValidationExpressionDesc] [varchar](250) NULL,
	[ControlValidationMin] [varchar](50) NULL,
	[ControlValidationMax] [varchar](50) NULL,
	[ControlValidationErrorMessage] [varchar](500) NULL,
	[ControlInitialValue] [varchar](50) NULL,
	[ControlInitialText] [varchar](50) NULL,
	[ControlType] [varchar](50) NULL,
	[ControlWildcard] [bit] NULL,
	[ControlExceptFlag] [bit] NULL,
	[ControlSelectColumn] [varchar](50) NULL,
	[ControlServer] [varchar](50) NULL,
	[ControlFromTable] [varchar](250) NULL,
	[ControlWhereColumn] [varchar](50) NULL,
	[ControlWhereFilterValue] [varchar](50) NULL,
	[ControlWhereFilterType] [varchar](50) NULL,
	[ControlCustomSQL] [varchar](max) NULL,
	[LookupCodeEnabled] [bit] NULL,
	[QueryColumn] [varchar](100) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_CustomerAddressGeocoding]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_CustomerAddressGeocoding](
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[LocationSuffix] [smallint] NOT NULL,
	[InputAddress] [varchar](500) NOT NULL,
	[PlaceID] [varchar](300) NULL,
	[StreetNumber] [varchar](300) NULL,
	[StreetNumberLong] [varchar](300) NULL,
	[Route] [varchar](300) NULL,
	[RouteLong] [varchar](300) NULL,
	[SubPremise] [varchar](300) NULL,
	[SubPremiseLong] [varchar](300) NULL,
	[Neighborhood] [varchar](300) NULL,
	[NeighborhoodLong] [varchar](300) NULL,
	[Locality] [varchar](300) NULL,
	[LocalityLong] [varchar](300) NULL,
	[AdministrativeAreaLevelThree] [varchar](300) NULL,
	[AdministrativeAreaLevelThreeLong] [varchar](300) NULL,
	[AdministrativeAreaLevelTwo] [varchar](300) NULL,
	[AdministrativeAreaLevelTwoLong] [varchar](300) NULL,
	[AdministrativeAreaLevelOne] [varchar](300) NULL,
	[AdministrativeAreaLevelOneLong] [varchar](300) NULL,
	[Country] [varchar](300) NULL,
	[CountryLong] [varchar](300) NULL,
	[PostalCode] [varchar](300) NULL,
	[PostalCodeLong] [varchar](300) NULL,
	[PostalCodeSuffix] [varchar](300) NULL,
	[PostalCodeSuffixLong] [varchar](300) NULL,
	[FormattedAddress] [varchar](300) NULL,
	[LocationType] [varchar](300) NULL,
	[Latitude] [decimal](17, 13) NOT NULL,
	[Longitude] [decimal](17, 13) NOT NULL,
	[Location] [geography] NOT NULL,
	[NorthEastLatitude] [decimal](17, 13) NULL,
	[NorthEastLongitude] [decimal](17, 13) NULL,
	[SouthWestLatitude] [decimal](17, 13) NULL,
	[SouthWestLongitude] [decimal](17, 13) NULL,
	[InsertDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ServiceOrder] ASC,
	[ServiceUnit] ASC,
	[LocationSuffix] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_CustomerAddressGeocodingStaging]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_CustomerAddressGeocodingStaging](
	[InputAddress] [varchar](500) NOT NULL,
	[PlaceID] [varchar](300) NULL,
	[StreetNumber] [varchar](300) NULL,
	[StreetNumberLong] [varchar](300) NULL,
	[Route] [varchar](300) NULL,
	[RouteLong] [varchar](300) NULL,
	[SubPremise] [varchar](300) NULL,
	[SubPremiseLong] [varchar](300) NULL,
	[Neighborhood] [varchar](300) NULL,
	[NeighborhoodLong] [varchar](300) NULL,
	[Locality] [varchar](300) NULL,
	[LocalityLong] [varchar](300) NULL,
	[AdministrativeAreaLevelThree] [varchar](300) NULL,
	[AdministrativeAreaLevelThreeLong] [varchar](300) NULL,
	[AdministrativeAreaLevelTwo] [varchar](300) NULL,
	[AdministrativeAreaLevelTwoLong] [varchar](300) NULL,
	[AdministrativeAreaLevelOne] [varchar](300) NULL,
	[AdministrativeAreaLevelOneLong] [varchar](300) NULL,
	[Country] [varchar](300) NULL,
	[CountryLong] [varchar](300) NULL,
	[PostalCode] [varchar](300) NULL,
	[PostalCodeLong] [varchar](300) NULL,
	[PostalCodeSuffix] [varchar](300) NULL,
	[PostalCodeSuffixLong] [varchar](300) NULL,
	[FormattedAddress] [varchar](300) NULL,
	[LocationType] [varchar](300) NULL,
	[Latitude] [decimal](17, 13) NOT NULL,
	[Longitude] [decimal](17, 13) NOT NULL,
	[Location] [geography] NOT NULL,
	[NorthEastLatitude] [decimal](17, 13) NULL,
	[NorthEastLongitude] [decimal](17, 13) NULL,
	[SouthWestLatitude] [decimal](17, 13) NULL,
	[SouthWestLongitude] [decimal](17, 13) NULL,
	[InsertDate] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[InputAddress] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_DiscountReasons]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_DiscountReasons](
	[rsn_cd] [char](2) NOT NULL,
	[rsn_ds] [varchar](45) NULL,
	[prt_vld_fl] [char](1) NULL,
	[ma_vld_fl] [char](1) NULL,
	[lab_vld_fl] [char](1) NULL,
	[dnt_typ_cd] [char](1) NULL,
	[lst_md_dt] [date] NULL,
	[PartReasonCode] [char](4) NULL,
	[LaborReasonCode] [char](4) NULL,
 CONSTRAINT [PK_TBL_DiscountReasons] PRIMARY KEY CLUSTERED 
(
	[rsn_cd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_FiscalCalendar]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_FiscalCalendar](
	[AccountingMonthName] [char](9) NULL,
	[CalendarDate] [date] NULL,
	[AccountingDate] [date] NOT NULL,
	[DayofWeek] [float] NULL,
	[DayofWeekName] [char](10) NULL,
	[DayofWeekNameShort] [char](3) NULL,
	[AccountingWeekStartDate] [date] NULL,
	[AccountingWeekEndDate] [date] NULL,
	[AccountingMonthStartDate] [date] NULL,
	[AccountingMonthEndDate] [date] NULL,
	[CalendarMonthStartDate] [date] NULL,
	[AccountingYear] [float] NULL,
	[AccountingWeek] [float] NULL,
	[AccountingYearWeek] [float] NULL,
	[AccountingMonth] [float] NULL,
	[AccountingYearMonth] [float] NULL,
	[AccountingQuarter] [float] NULL,
	[AccountingWeekInMonth] [float] NULL,
	[AccountingWeekInQuarter] [float] NULL,
	[TotalWeeksInMonth] [float] NULL,
	[CalendarYear] [float] NULL,
	[CalendarMonth] [float] NULL,
 CONSTRAINT [PK_TBL_FiscalCalendar] PRIMARY KEY CLUSTERED 
(
	[AccountingDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_LookupCodes]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_LookupCodes](
	[LookupID] [int] IDENTITY(1,1) NOT NULL,
	[OwnerType] [varchar](20) NOT NULL,
	[OwnerID] [int] NULL,
	[Description] [varchar](50) NOT NULL,
	[LongDescription] [varchar](500) NULL,
	[NumericValue] [int] NULL,
	[SortOrder] [int] NOT NULL,
	[IsEnabled] [bit] NOT NULL,
	[IsUserAllowed] [bit] NOT NULL,
	[LastModified] [datetime] NOT NULL,
	[LastModifiedBy] [varchar](10) NULL,
 CONSTRAINT [PK_TBL_LookupCodes] PRIMARY KEY NONCLUSTERED 
(
	[LookupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_MerchandiseCode]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_MerchandiseCode](
	[MD_SM_CODE] [char](12) NOT NULL,
	[MD_DIV_NO_SM] [char](3) NULL,
	[MD_LINE_NO_SM] [char](2) NULL,
	[MD_MDSE_SPECIALTY] [char](10) NULL,
	[MD_SM_CODE_DESC] [char](25) NULL,
	[MD_SM_CODE_DESC2] [char](25) NULL,
	[MD_SM_CODE_TSR_D] [char](25) NULL,
	[MD_SM_CODE_TSR_D2] [char](25) NULL,
	[MD_SM_BUILT_IN_IND] [char](1) NULL,
	[MD_SM_USAGE_TYPE] [char](1) NULL,
	[MD_SM_L_WRNTY_NO] [numeric](3, 0) NULL,
	[MD_SM_L_WRNTY_DMY] [char](1) NULL,
	[MD_SM_P_WRNTY_NO] [numeric](3, 0) NULL,
	[MD_SM_P_WRNTY_DMY] [char](1) NULL,
	[MD_SM_XP_WRNTY_NO] [numeric](3, 0) NULL,
	[MD_SM_XP_WRNTY_DMY] [char](1) NULL,
	[MD_IW_SVCE_LOCN] [char](1) NULL,
	[MD_SM_CODE_NEW] [char](12) NULL,
	[MD_CHANGE_STATUS] [char](1) NULL,
	[MD_CHANGE_TSTAMP] [datetime2](7) NULL,
	[MDS_MAJ_BND_SVC_FL] [char](1) NULL,
	[MDS_PRO_AGE_LMT_NO] [numeric](2, 0) NULL,
	[MTC_PNT_FTR_TX] [varchar](300) NULL,
	[MDS_PRC_TIR_CD] [char](1) NULL,
	[PPT_TYP_CD] [char](1) NULL,
	[CRY_IN_RTE_CD] [numeric](2, 0) NULL,
 CONSTRAINT [PK_TBL_MerchandiseCode] PRIMARY KEY CLUSTERED 
(
	[MD_SM_CODE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_Notifications]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Notifications](
	[NotificationID] [uniqueidentifier] NOT NULL,
	[NotificationType] [varchar](20) NOT NULL,
	[SourceID] [int] NULL,
	[SourceType] [varchar](30) NULL,
	[CreateDateTime] [datetime] NULL,
	[CreateEnterpriseID] [varchar](8) NULL,
 CONSTRAINT [PK_TBL_Notifications] PRIMARY KEY CLUSTERED 
(
	[NotificationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_NPJCustomerEmail]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_NPJCustomerEmail](
	[CustomerID] [int] NOT NULL,
	[CustomerEmailAddress] [varchar](50) NOT NULL,
	[CreateTimeStamp] [datetime2](7) NOT NULL,
	[SourceTypeCode] [char](3) NOT NULL,
	[MMHOptInCode] [char](1) NULL,
	[MMHOptInChangeTimeStamp] [datetime2](7) NULL,
	[SHCOptInCode] [char](1) NULL,
	[SHCOptInChangeTimeStamp] [datetime2](7) NULL,
	[EmailSourceCode] [varchar](20) NULL,
	[EmailSourceFunctionDescription] [varchar](20) NULL,
	[EmailCaptureUnit] [char](7) NULL,
	[EmailCaptureEmployee] [char](7) NULL,
	[EmailATVCode] [char](1) NULL,
	[EmailTypeCode] [char](2) NULL,
	[LastModifiedTimeStamp] [datetime2](7) NOT NULL,
	[CurrentEmail] [smallint] NULL,
 CONSTRAINT [PK_TBL_NPJCustomerEmail] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC,
	[CustomerEmailAddress] ASC,
	[CreateTimeStamp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_NPJCustomerNameAddress]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_NPJCustomerNameAddress](
	[Customer] [int] NOT NULL,
	[AddressLocation] [smallint] NOT NULL,
	[SearsStoreAddressCode] [char](1) NOT NULL,
	[EmployeeRetiree] [varchar](1) NOT NULL,
	[EmployeeCompany] [varchar](1) NOT NULL,
	[Occupant] [varchar](1) NOT NULL,
	[DoNotPromote] [varchar](2) NOT NULL,
	[DMADoNotSell] [varchar](2) NOT NULL,
	[CustomerNamePrefix] [varchar](4) NOT NULL,
	[CustomerLastName] [varchar](18) NOT NULL,
	[CustomerLastNameAdditional] [varchar](18) NOT NULL,
	[CustomerLastNameCode] [char](1) NOT NULL,
	[CustomerLastNameChangeDate] [date] NOT NULL,
	[CustomerFirstName] [varchar](11) NOT NULL,
	[CustomerMiddleName] [varchar](11) NOT NULL,
	[CustomerNameSuffix] [varchar](4) NOT NULL,
	[AddressType] [char](1) NOT NULL,
	[AddressNumber] [varchar](10) NOT NULL,
	[AddressStreet] [varchar](22) NOT NULL,
	[AddressStreetSuffix] [varchar](4) NOT NULL,
	[AddressStreetSuffixModifier] [varchar](2) NOT NULL,
	[POBox] [varchar](10) NOT NULL,
	[AddressUnitType] [varchar](1) NOT NULL,
	[AddressUnitNumber] [varchar](6) NOT NULL,
	[AddressLineTwo] [varchar](25) NOT NULL,
	[AddressCity] [varchar](20) NOT NULL,
	[AddressState] [char](2) NOT NULL,
	[AddressZip] [varchar](6) NOT NULL,
	[AddressZipPlusFour] [varchar](4) NOT NULL,
	[AddressZipCarrierRouteWalkSequence] [varchar](4) NOT NULL,
	[AddressHashed] [varchar](8) NOT NULL,
	[AddressEffectiveDate] [date] NOT NULL,
	[AddressObsoleteDate] [date] NOT NULL,
	[CustomerPhone] [varchar](10) NOT NULL,
	[CustomerAlternatePhone] [varchar](13) NOT NULL,
	[CustomerRecordSource] [char](2) NOT NULL,
	[GroupID] [decimal](13, 0) NOT NULL,
	[NixieCode] [varchar](1) NOT NULL,
	[PurgeDate] [date] NOT NULL,
	[CommitTimestamp] [datetime] NOT NULL,
	[CustomerEmailAddress] [varchar](50) NULL,
 CONSTRAINT [PK_TBL_NPJCustomerNameAddress] PRIMARY KEY CLUSTERED 
(
	[Customer] ASC,
	[AddressLocation] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_NPJMerchandiseCode]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_NPJMerchandiseCode](
	[Merchandise] [varchar](12) NOT NULL,
	[MerchandiseDivision] [char](3) NOT NULL,
	[MerchandiseLine] [char](2) NOT NULL,
	[MerchandiseSpecialty] [varchar](10) NOT NULL,
	[MerchandiseDescription] [varchar](25) NOT NULL,
	[MerchandiseDescriptionTwo] [varchar](25) NOT NULL,
	[TSRMerchandiseDescription] [varchar](25) NOT NULL,
	[TSRMerchandiseDescriptionTwo] [varchar](25) NOT NULL,
	[MerchandiseBuiltIn] [char](1) NOT NULL,
	[UsageType] [char](1) NOT NULL,
	[LaborWarrantyLength] [int] NOT NULL,
	[LaborWarrantyLengthType] [char](1) NOT NULL,
	[PartsWarrantyLength] [int] NOT NULL,
	[PartsWarrantyLengthType] [char](1) NOT NULL,
	[ExceptionPartsWarrantyLength] [int] NOT NULL,
	[ExceptionPartsWarrantyLengthType] [char](1) NOT NULL,
	[ServiceLocation] [char](1) NOT NULL,
	[MerchandiseNewCode] [varchar](12) NOT NULL,
	[ChangeStatus] [char](1) NOT NULL,
	[MerchandiseCodeLastUpdateDate] [datetime] NOT NULL,
	[MajorBrandService] [char](1) NOT NULL,
	[MerchandisePromoteAgeLimit] [int] NOT NULL,
	[PreventativeMaintenanceInstruc] [varchar](300) NOT NULL,
	[MerchandisePriceTier] [char](1) NOT NULL,
	[MerchandisePropertyType] [char](1) NOT NULL,
	[CarryInRouting] [int] NOT NULL,
	[LastActionTimestamp] [datetime] NOT NULL,
 CONSTRAINT [PK_TBL_NPJMerchandiseCode] PRIMARY KEY CLUSTERED 
(
	[Merchandise] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_NPNServiceOrderParts]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_NPNServiceOrderParts](
	[NPNOrder] [varchar](7) NOT NULL,
	[NPNOrderDate] [date] NOT NULL,
	[NPNOrderLine] [smallint] NOT NULL,
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[NPSPartSequence] [smallint] NOT NULL,
	[SKU] [varchar](30) NOT NULL,
	[NPNOrderStatus] [char](2) NOT NULL,
	[NPNOrderStatusDate] [date] NOT NULL,
	[NPNLocation] [varchar](5) NOT NULL,
	[ShipDate] [date] NOT NULL,
	[PurchaseRequisitionOrder] [varchar](7) NOT NULL,
	[PurchaseRequisitionOrderType] [char](1) NOT NULL,
	[TrackingNumber] [varchar](30) NULL,
	[CarrierCode] [varchar](4) NULL,
	[LastActionTimestamp] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_TBL_NPNServiceOrderParts] PRIMARY KEY CLUSTERED 
(
	[NPNOrder] ASC,
	[NPNOrderDate] ASC,
	[NPNOrderLine] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_NPSCustomerServicableMerchandise]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_NPSCustomerServicableMerchandise](
	[Customer] [int] NOT NULL,
	[ItemSuffix] [int] NOT NULL,
	[CustomerInteractionID] [decimal](18, 0) NOT NULL,
	[ItemConversionFlag] [varchar](1) NOT NULL,
	[MerchandiseCode] [varchar](12) NOT NULL,
	[PurchaseDate] [date] NOT NULL,
	[DeliveryInstallGiftReceiptDate] [date] NOT NULL,
	[WhereBoughtCode] [varchar](1) NOT NULL,
	[SearsStoreWhereBought] [varchar](7) NOT NULL,
	[UsageType] [char](1) NOT NULL,
	[PromoteServiceContractFlag] [char](1) NOT NULL,
	[DoNotPromoteServiceContractReason] [varchar](1) NOT NULL,
	[LaborWarrantyExpirationDate] [date] NOT NULL,
	[GeneralPartsWarrantyExpirationDate] [date] NOT NULL,
	[ExceptionPartsWarrantyExpirationDate] [date] NOT NULL,
	[ItemStatusCode] [varchar](1) NOT NULL,
	[ServiceProductCode] [varchar](3) NOT NULL,
	[CurrentServiceProductExpirationDate] [date] NOT NULL,
	[ServiceProductPlan] [varchar](6) NOT NULL,
	[ServiceLocation] [char](1) NOT NULL,
	[ModelNumber] [varchar](24) NOT NULL,
	[Division] [char](3) NOT NULL,
	[ProductItem] [varchar](5) NOT NULL,
	[SerialNumber] [varchar](20) NOT NULL,
	[BrandName] [varchar](12) NOT NULL,
	[LastModifiedDate] [date] NOT NULL,
	[PriceLookupAmount] [decimal](9, 2) NOT NULL,
	[RegularRetailAmount] [decimal](9, 2) NOT NULL,
	[SaleAmount] [decimal](9, 2) NOT NULL,
	[TotalLineMarkdownAmount] [decimal](9, 2) NOT NULL,
	[ProductInternalNumber] [int] NOT NULL,
	[CommitTimestamp] [datetime] NOT NULL,
	[SalescheckNumber] [char](12) NULL,
	[SalescheckItemLineNumber] [smallint] NULL,
	[TotalLineDiscountAmount] [decimal](9, 2) NULL,
	[LineItemTaxAmount] [decimal](9, 2) NULL,
	[LineItemTaxPercent] [decimal](9, 2) NULL,
	[ManuallyEnteredFlag] [char](1) NULL,
	[SalescheckDateTime] [datetime] NULL,
 CONSTRAINT [PK_TBL_NPSCustomerServicableMerchandise] PRIMARY KEY CLUSTERED 
(
	[Customer] ASC,
	[ItemSuffix] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_NPSCustomerServicableMerchandiseSalescheck]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_NPSCustomerServicableMerchandiseSalescheck](
	[Customer] [int] NOT NULL,
	[ItemSuffix] [int] NOT NULL,
	[SalescheckNumber] [char](12) NULL,
	[SalescheckDateTime] [datetime] NULL,
	[SalescheckItemLineNumber] [smallint] NULL,
	[SalesAmount] [decimal](9, 2) NULL,
	[TotalLineDiscountAmount] [decimal](9, 2) NULL,
	[LineItemTaxAmount] [decimal](9, 2) NULL,
	[LineItemTaxPercent] [decimal](9, 2) NULL,
	[ManuallyEnteredFlag] [char](1) NULL,
 CONSTRAINT [PK_TBL_NPSCustomerServicableMerchandiseSalescheck] PRIMARY KEY CLUSTERED 
(
	[Customer] ASC,
	[ItemSuffix] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_NPSServiceOrderAdditionalInfo]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_NPSServiceOrderAdditionalInfo](
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[UserDefinedFieldID] [int] NOT NULL,
	[FACCategory] [char](1) NOT NULL,
	[UserDefinedFieldLineItem] [char](2) NOT NULL,
	[UserDefinedFieldLineItemSequence] [smallint] NOT NULL,
	[UserDefinedFieldRuleID] [int] NOT NULL,
	[EnterpriseID] [char](8) NOT NULL,
	[UserDefinedFieldName] [char](20) NOT NULL,
	[UserDefinedFieldValue] [char](120) NOT NULL,
	[PFL_FL] [char](1) NOT NULL,
	[CreateTimeStamp] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_TBL_NPSServiceOrderAdditionalInfo] PRIMARY KEY CLUSTERED 
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC,
	[CreateTimeStamp] ASC,
	[UserDefinedFieldID] ASC,
	[UserDefinedFieldLineItem] ASC,
	[UserDefinedFieldLineItemSequence] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_NPSServiceOrderAdditionalRemarks]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_NPSServiceOrderAdditionalRemarks](
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[LastModifiedDateTime] [datetime] NOT NULL,
	[TextCode] [char](2) NOT NULL,
	[EmployeeID] [varchar](7) NOT NULL,
	[AdditionalRemarks] [varchar](160) NOT NULL,
	[ModificationID] [char](7) NOT NULL,
	[InquiryNumber] [int] NOT NULL,
	[EntryNumber] [int] NOT NULL,
	[CallBackDateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_TBL_NPSServiceOrderAdditionalRemarks] PRIMARY KEY CLUSTERED 
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC,
	[LastModifiedDateTime] ASC,
	[TextCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_NPSServiceOrderAttempt]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_NPSServiceOrderAttempt](
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[ServiceCallDate] [date] NOT NULL,
	[ServiceTechEmployee] [char](7) NOT NULL,
	[CallCode] [char](2) NOT NULL,
	[AgreementApproval] [char](1) NOT NULL,
	[OtherApproval] [char](1) NOT NULL,
	[MeterReading] [int] NOT NULL,
	[TransitTimeMinutes] [int] NOT NULL,
	[TechnicianCommentsOne] [varchar](60) NOT NULL,
	[TechnicianCommentsTwo] [varchar](60) NOT NULL,
	[ETAMet] [char](1) NOT NULL,
	[LastActionTimestamp] [datetime] NOT NULL,
	[TechArrivalTime] [time](6) NULL,
	[TechEndTime] [time](6) NULL,
	[ServiceTotalMinutes] [smallint] NULL,
	[ServiceRescheduledDate] [date] NULL,
	[ServiceRescheduledTimeFrom] [char](8) NULL,
	[ServiceRescheduledTimeTo] [char](8) NULL,
	[RescheduleCode] [char](4) NULL,
	[TechnicianType] [int] NULL,
	[AttemptSequence] [int] NULL,
	[RouteSequence] [int] NULL,
	[LastAttemptFlag] [int] NULL,
	[LastRouteAttemptFlag] [int] NULL,
	[OffsiteFlag] [int] NULL,
	[AttemptCount] [int] NULL,
	[CompleteCount] [int] NULL,
	[ClickCallID] [varchar](64) NULL,
	[ClickCallNumber] [int] NULL,
	[ServiceTechFirstName] [varchar](100) NULL,
	[ServiceTechLastName] [varchar](100) NULL,
	[ServiceTechEnterpriseID] [varchar](28) NULL,
 CONSTRAINT [PK_TBL_NPSServiceOrderAttempt] PRIMARY KEY CLUSTERED 
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC,
	[ServiceCallDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_NPSServiceOrderAttemptJobCode]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_NPSServiceOrderAttemptJobCode](
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[ServiceCallDate] [date] NOT NULL,
	[JobCode] [int] NOT NULL,
	[JobCodeChargeCode] [char](1) NOT NULL,
	[JobCodeEffectiveDate] [date] NOT NULL,
	[JobCodeSequenceNumber] [int] NOT NULL,
	[CoverageCode] [char](2) NULL,
	[LaborEnteredAmount] [decimal](9, 2) NULL,
	[ChargeCodeRelatedCode] [char](1) NULL,
	[MerchandiseComponentType] [varchar](3) NULL,
	[ServiceObligerCode] [varchar](6) NULL,
	[OPType] [char](1) NULL,
	[SerialNumber] [int] NULL,
	[CommitTimestamp] [datetime] NOT NULL,
	[MerchandiseSpecialtyCode] [varchar](10) NULL,
 CONSTRAINT [PK_TBL_NPSServiceOrderAttemptJobCode] PRIMARY KEY CLUSTERED 
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC,
	[JobCodeSequenceNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_NPSServiceOrderAttemptJobCodeDesc]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_NPSServiceOrderAttemptJobCodeDesc](
	[MerchandiseSpecialtyCode] [varchar](10) NOT NULL,
	[JobCode] [int] NOT NULL,
	[JobCodeChargeCode] [char](1) NOT NULL,
	[JobCodeEffectiveDate] [date] NOT NULL,
	[JobCodeDescription] [varchar](50) NULL,
	[LastModificationDate] [date] NULL,
	[JobCodeSiteFlag] [char](1) NULL,
	[JobCodeShopFlag] [char](1) NULL,
	[JobCodeInstallationFlag] [char](1) NULL,
 CONSTRAINT [PK_TBL_NPSServiceOrderAttemptJobCodeDesc] PRIMARY KEY CLUSTERED 
(
	[MerchandiseSpecialtyCode] ASC,
	[JobCode] ASC,
	[JobCodeChargeCode] ASC,
	[JobCodeEffectiveDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_NPSServiceOrderClosed]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_NPSServiceOrderClosed](
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[ServiceOrderStatus] [char](2) NOT NULL,
	[ServiceOrderStatusDate] [date] NOT NULL,
 CONSTRAINT [PK_TBL_NPSServiceOrderClosed] PRIMARY KEY CLUSTERED 
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_NPSServiceOrderCustomerLookaside]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_NPSServiceOrderCustomerLookaside](
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[Customer] [int] NOT NULL,
	[AddressLocation] [smallint] NOT NULL,
	[ItemSuffix] [int] NOT NULL,
	[CustomerStreetAddress] [varchar](50) NOT NULL,
	[CustomerCity] [varchar](12) NOT NULL,
	[CustomerState] [char](2) NOT NULL,
	[CustomerZip] [char](5) NOT NULL,
	[CustomerZipPlusFour] [char](4) NOT NULL,
	[CustomerPhone] [varchar](10) NOT NULL,
	[CustomerAlternatePhone] [varchar](10) NOT NULL,
	[RepairServiceUnit] [char](7) NULL,
	[OptionType] [char](1) NULL,
	[SerialNumber] [bigint] NULL,
	[LastActionTimestamp] [datetime] NULL,
	[CustomerNamePrefix] [varchar](4) NULL,
	[CustomerLastName] [varchar](18) NULL,
	[CustomerLastNameAdditional] [varchar](18) NULL,
	[CustomerFirstName] [varchar](11) NULL,
	[CustomerMiddleName] [varchar](11) NULL,
	[CustomerNameSuffix] [varchar](4) NULL,
	[CustomerEmailAddress] [varchar](50) NULL,
	[CustomerPhoneSHCDoNotCallFlag] [char](1) NULL,
	[CustomerAltPhoneSHCDoNotCallFlag] [char](1) NULL,
 CONSTRAINT [PK_TBL_NPSServiceOrderCustomerLookaside] PRIMARY KEY CLUSTERED 
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC,
	[AddressLocation] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_NPSServiceOrderCustomerRelationsInquiry]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_NPSServiceOrderCustomerRelationsInquiry](
	[SerialNumber] [bigint] NULL,
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[OptionType] [char](1) NULL,
	[InquiryNumber] [decimal](3, 0) NOT NULL,
	[InquiryTargetCommunityCode] [char](1) NULL,
	[InquiryStatusCode] [char](1) NULL,
	[InquiryReasonCode] [char](2) NULL,
	[CustomerNetworkInquiryFlag] [char](1) NULL,
	[InquiryTargetViewFlag] [char](1) NULL,
	[TimeZoneCode] [char](1) NULL,
	[CustomerCallCountNumber] [smallint] NULL,
	[InquiryCreateCommunityCode] [char](1) NULL,
	[EmployeeID] [char](7) NULL,
	[ModificationID] [char](7) NULL,
	[ModificationDateTime] [datetime2](7) NOT NULL,
	[InquiryCreateDateTime] [datetime2](7) NOT NULL,
	[CallBackDateTime] [datetime2](7) NULL,
	[CommitDateTime] [datetime2](7) NULL,
	[LoadDateTime] [datetime2](7) NULL,
 CONSTRAINT [PK_TBL_NPSServiceOrderCustomerRelationsInquiry] PRIMARY KEY CLUSTERED 
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC,
	[InquiryCreateDateTime] ASC,
	[InquiryNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_NPSServiceOrderDiscountCoupon]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_NPSServiceOrderDiscountCoupon](
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[ReasonCode] [char](4) NOT NULL,
	[CouponNumber] [char](11) NULL,
	[TechManagerApprovalFlag] [char](1) NULL,
	[ModifiedDate] [date] NOT NULL,
 CONSTRAINT [PK_TBL_NPSServiceOrderDiscountCoupon] PRIMARY KEY CLUSTERED 
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC,
	[ReasonCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_NPSServiceOrderMonetaryCharges]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_NPSServiceOrderMonetaryCharges](
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[Coverage] [char](2) NOT NULL,
	[ServiceCallDate] [date] NOT NULL,
	[PartsTotal] [decimal](9, 2) NOT NULL,
	[PartsDiscount] [decimal](9, 2) NOT NULL,
	[PartsNet] [decimal](9, 2) NOT NULL,
	[PartsTax] [decimal](9, 2) NOT NULL,
	[PartsGross] [decimal](9, 2) NOT NULL,
	[LaborTotal] [decimal](9, 2) NOT NULL,
	[LaborDiscount] [decimal](9, 2) NOT NULL,
	[LaborNet] [decimal](9, 2) NOT NULL,
	[LaborTax] [decimal](9, 2) NOT NULL,
	[LaborGross] [decimal](9, 2) NOT NULL,
	[BasicCharge] [decimal](7, 2) NOT NULL,
	[LaborPlusBasicCharge] [decimal](9, 2) NOT NULL,
	[PointofSaleTransaction] [varchar](4) NOT NULL,
	[PointofSaleRegister] [varchar](3) NOT NULL,
	[LastModifiedDate] [datetime] NOT NULL,
	[PartsDiscountCode57] [decimal](9, 2) NOT NULL,
	[PartsDiscountCode58] [decimal](9, 2) NOT NULL,
	[LaborDiscountCode57] [decimal](9, 2) NOT NULL,
	[LaborDiscountCode58] [decimal](9, 2) NOT NULL,
	[DiscrepancyAllowance] [decimal](9, 2) NOT NULL,
	[TaxExemption] [char](1) NOT NULL,
	[TechEndTime] [time](6) NULL,
	[TechArrivalTime] [time](6) NULL,
	[PartDiscountReason] [char](4) NULL,
	[LaborDiscountReason] [char](4) NULL,
	[CouponNumber] [char](11) NULL,
	[DiscrepancyAllowanceReasonCode] [int] NULL,
 CONSTRAINT [PK_TBL_NPSServiceOrderMonetaryCharges] PRIMARY KEY CLUSTERED 
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC,
	[Coverage] ASC,
	[ServiceCallDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_NPSServiceOrderParts]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_NPSServiceOrderParts](
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[PartSequence] [smallint] NOT NULL,
	[PartDivision] [char](3) NOT NULL,
	[PartListSource] [char](3) NOT NULL,
	[Part] [varchar](24) NOT NULL,
	[PartDescription] [varchar](15) NOT NULL,
	[PartType] [char](1) NOT NULL,
	[PartQuantity] [int] NOT NULL,
	[PartQuantityUsed] [int] NOT NULL,
	[Coverage] [char](2) NOT NULL,
	[PartActualSellPrice] [decimal](9, 2) NOT NULL,
	[PartStatus] [char](1) NOT NULL,
	[PartStatusDate] [date] NOT NULL,
	[PartExtendedWarrantySold] [char](1) NOT NULL,
	[PartPromotion] [varchar](7) NOT NULL,
	[PartOrderDate] [date] NOT NULL,
	[EmployeeIdentification] [char](7) NOT NULL,
	[PartQuantityOrdered] [int] NOT NULL,
	[PartQuantityReceived] [int] NOT NULL,
	[PartQuantityReceivedDate] [date] NOT NULL,
	[PartInstalledDate] [date] NOT NULL,
	[PartStockBin] [varchar](4) NOT NULL,
	[TruckBin] [varchar](3) NOT NULL,
	[PartDestinationBin] [varchar](5) NOT NULL,
	[PartPriceEach] [decimal](7, 2) NOT NULL,
	[PartCostEach] [decimal](7, 2) NOT NULL,
	[PartPremiumPriceEach] [decimal](7, 2) NOT NULL,
	[PartTrackingInquiryCode] [char](1) NOT NULL,
	[MerchandiseComponentType] [varchar](3) NOT NULL,
	[LastActionTimestamp] [datetime] NOT NULL,
 CONSTRAINT [PK_TBL_NPSServiceOrderParts] PRIMARY KEY CLUSTERED 
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC,
	[PartSequence] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_NPSServiceOrderPartsStatusDescription]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_NPSServiceOrderPartsStatusDescription](
	[PartStatus] [char](1) NOT NULL,
	[PartStatusDescription] [varchar](50) NOT NULL,
 CONSTRAINT [PK_TBL_NPSServiceOrderPartsStatusDescription] PRIMARY KEY CLUSTERED 
(
	[PartStatus] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_NPSServiceOrderPaymentMethod]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_NPSServiceOrderPaymentMethod](
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[PaymentMethod] [char](2) NOT NULL,
	[ServiceCallDate] [date] NOT NULL,
	[CustomerChargeAccountNumber] [char](16) NULL,
	[CustomerChargeAccountExpirationDate] [date] NULL,
	[CreditApprovalNumber] [char](7) NULL,
	[CollectedAmount] [decimal](8, 2) NULL,
	[PurchaseOrderNumber] [char](18) NULL,
	[CardAccountTakenNumber] [char](16) NULL,
	[CommitTimestamp] [datetime] NULL,
	[RecordStatus] [int] NULL,
 CONSTRAINT [PK_TBL_NPSServiceOrderPaymentMethod] PRIMARY KEY CLUSTERED 
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC,
	[PaymentMethod] ASC,
	[ServiceCallDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_NPSServiceOrderPaymentMethodReview]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_NPSServiceOrderPaymentMethodReview](
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[PaymentMethod] [char](2) NOT NULL,
	[ServiceCallDate] [date] NOT NULL,
	[CustomerChargeAccountNumber] [char](16) NULL,
	[CustomerChargeAccountExpirationDate] [date] NULL,
	[CreditApprovalNumber] [char](7) NULL,
	[CollectedAmount] [decimal](8, 2) NULL,
	[PurchaseOrderNumber] [char](18) NULL,
	[CardAccountTakenNumber] [char](16) NULL,
	[CommitTimestamp] [datetime] NULL,
	[RecordStatus] [int] NULL,
 CONSTRAINT [PK_TBL_NPSServiceOrderPaymentMethodReview] PRIMARY KEY CLUSTERED 
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC,
	[PaymentMethod] ASC,
	[ServiceCallDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_NPSServiceOrderRecalls]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_NPSServiceOrderRecalls](
	[CustomerID] [int] NOT NULL,
	[LocationSuffix] [int] NOT NULL,
	[ItemSuffix] [int] NOT NULL,
	[ChildServiceUnit] [char](7) NOT NULL,
	[ChildServiceOrder] [char](8) NOT NULL,
	[ChildCreateDate] [date] NULL,
	[ChildAttemptCount] [int] NULL,
	[ChildCompleteDate] [date] NULL,
	[ChildCompleteCode] [char](2) NULL,
	[ParentServiceUnit] [char](7) NOT NULL,
	[ParentServiceOrder] [char](8) NOT NULL,
	[ParentCreateDate] [date] NULL,
	[ParentAttemptCount] [int] NULL,
	[ParentCompleteDate] [date] NULL,
	[ParentCompleteCode] [char](2) NULL,
	[ParentNPSEmployeeID] [char](14) NULL,
	[RecallDays] [int] NULL,
	[CustomerRecall] [int] NULL,
	[UnitRecall] [int] NULL,
 CONSTRAINT [PK_TBL_NPSServiceOrderRecalls] PRIMARY KEY CLUSTERED 
(
	[ChildServiceUnit] ASC,
	[ChildServiceOrder] ASC,
	[ParentServiceUnit] ASC,
	[ParentServiceOrder] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_NPSServiceOrderRescheduleReasonCodes]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_NPSServiceOrderRescheduleReasonCodes](
	[ScheduleChangeReasonCode] [char](4) NOT NULL,
	[ScheduleChangeReasonDescription] [varchar](60) NOT NULL,
 CONSTRAINT [PK_TBL_NPSServiceOrderRescheduleReasonCodes] PRIMARY KEY CLUSTERED 
(
	[ScheduleChangeReasonCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_NPSServiceOrderReschedules]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_NPSServiceOrderReschedules](
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[HomeServicesCustomer] [int] NOT NULL,
	[StatusCode] [char](2) NOT NULL,
	[ScheduleChangeReasonCode] [char](4) NOT NULL,
	[SystemTypeCode] [char](1) NOT NULL,
	[ModificationID] [varchar](7) NOT NULL,
	[ServiceScheduleDate] [date] NOT NULL,
	[ModificationUnit] [varchar](7) NOT NULL,
	[LastModifiedDateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_TBL_NPSServiceOrderReschedules] PRIMARY KEY CLUSTERED 
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC,
	[LastModifiedDateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_NPSServiceOrderServiceAttempt]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_NPSServiceOrderServiceAttempt](
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[ServiceCallDate] [date] NOT NULL,
	[ServiceTechEmployee] [char](7) NOT NULL,
	[CallCode] [char](2) NOT NULL,
	[AgreementApproval] [char](1) NOT NULL,
	[OtherApproval] [char](1) NOT NULL,
	[MeterReading] [int] NOT NULL,
	[TransitTimeMinutes] [int] NOT NULL,
	[TechnicianCommentsOne] [varchar](60) NOT NULL,
	[TechnicianCommentsTwo] [varchar](60) NOT NULL,
	[ETAMet] [char](1) NOT NULL,
	[LastActionTimestamp] [datetime] NOT NULL,
	[TechArrivalTime] [time](6) NULL,
	[TechEndTime] [time](6) NULL,
	[ServiceTotalMinutes] [smallint] NULL,
	[ServiceRescheduledDate] [date] NULL,
	[ServiceRescheduledTimeFrom] [char](8) NULL,
	[ServiceRescheduledTimeTo] [char](8) NULL,
	[ServiceTechFirstName] [varchar](100) NULL,
	[ServiceTechLastName] [varchar](100) NULL,
	[ServiceTechEnterpriseID] [varchar](28) NULL,
 CONSTRAINT [PK_TBL_NPSServiceOrderServiceAttempt] PRIMARY KEY CLUSTERED 
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC,
	[ServiceCallDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_NPSServiceOrderTaxRates]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_NPSServiceOrderTaxRates](
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[ServiceOrderTaxRate] [decimal](6, 5) NULL,
	[LaborTaxFlag] [char](1) NULL,
	[PartTaxFlag] [char](1) NULL,
	[PATaxFlag] [char](1) NULL,
	[TaxingGeoCode] [char](9) NULL,
	[TaxingCountyCode] [char](3) NULL,
	[JurisdictionCountyCode] [char](1) NULL,
	[JurisdictionCountySpecialCode] [char](1) NULL,
	[JurisdictionCountyLocalCode] [char](1) NULL,
	[JurisdictionCountyLocalSpecialCode] [char](1) NULL,
	[JurisdictionTransitCode] [char](1) NULL,
	[JurisdictionSpecialCode] [char](1) NULL,
	[JurisdictionOtherCode] [char](1) NULL,
	[TP_TaxRatePercent] [decimal](6, 5) NULL,
	[TP_TaxRateJob] [decimal](6, 5) NULL,
	[ST_TaxRatePercent] [decimal](6, 5) NULL,
 CONSTRAINT [PK_TBL_NPSServiceOrderTaxRates] PRIMARY KEY CLUSTERED 
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_npsxtpm]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_npsxtpm](
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[PaymentMethod] [char](2) NOT NULL,
	[ServiceCallDate] [date] NOT NULL,
	[CustomerChargeAccountNumber] [char](16) NULL,
	[CustomerChargeAccountExpirationDate] [date] NULL,
	[CreditApprovalNumber] [char](7) NULL,
	[CollectedAmount] [decimal](8, 2) NULL,
	[PurchaseOrderNumber] [char](18) NULL,
	[CardAccountTakenNumber] [char](16) NULL,
	[CommitTimestamp] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ProductSalesRequest]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ProductSalesRequest](
	[ProductSalesRequestID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[CustomerFirstName] [varchar](50) NULL,
	[CustomerLastName] [varchar](50) NULL,
	[CustomerPhoneNumber] [char](10) NULL,
	[CustomerPhoneNumberSecondary] [char](10) NULL,
	[CustomerEmail] [varchar](50) NULL,
	[CustomerEmailSeconday] [varchar](50) NULL,
	[ItemSuffixNumber] [int] NULL,
	[CurrentApplianceDetails] [nvarchar](max) NULL,
	[CustomerNotes] [nvarchar](max) NULL,
	[CallBackDate] [date] NULL,
	[CallBackDateTimeFrom] [char](4) NULL,
	[CallBackDateTimeTo] [char](4) NULL,
	[CallBackRequested] [char](1) NULL,
	[GoodBetterBest] [nvarchar](max) NULL,
	[CouponInfo] [nvarchar](max) NULL,
	[CreatedDateTime] [datetime2](7) NULL,
	[CreatedBy] [varchar](10) NULL,
	[LoadDateTime] [datetime2](7) NULL,
	[ProcessDateTime] [datetime2](7) NULL,
	[ServiceUnit] [char](7) NULL,
	[ServiceOrder] [char](8) NULL,
 CONSTRAINT [PK_TBL_ProductSalesRequest] PRIMARY KEY CLUSTERED 
(
	[ProductSalesRequestID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_QueryBuilder]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_QueryBuilder](
	[QueryID] [int] IDENTITY(1,1) NOT NULL,
	[ReportID] [int] NULL,
	[ControlID] [int] NULL,
	[ControlValue] [varchar](max) NULL,
 CONSTRAINT [PK_TBL_QueryBuilder] PRIMARY KEY CLUSTERED 
(
	[QueryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_QueryBuilderBackUp]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_QueryBuilderBackUp](
	[QueryID] [int] IDENTITY(1,1) NOT NULL,
	[ReportID] [int] NULL,
	[ControlID] [int] NULL,
	[ControlValue] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_RealTimeFeedBack]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_RealTimeFeedBack](
	[RTFID] [int] IDENTITY(1,1) NOT NULL,
	[ResponseID] [int] NOT NULL,
	[RespID] [int] NULL,
	[CustomerID] [int] NULL,
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[RouteDate] [date] NOT NULL,
	[FeedbackStart] [datetime2](7) NULL,
	[FeedbackEnd] [datetime2](7) NULL,
	[MemberName] [varchar](50) NULL,
	[MemberEmailAddress] [varchar](50) NULL,
	[MemberPhoneNumber] [varchar](50) NULL,
	[PMCheckCode] [varchar](50) NULL,
	[CallType] [varchar](50) NULL,
	[BrandName] [varchar](50) NULL,
	[FeedbackSource] [varchar](2) NULL,
	[FeedbackScore] [int] NULL,
	[OptionType] [int] NULL,
	[Option1Value] [int] NULL,
	[Option2Value] [int] NULL,
	[Option3Value] [int] NULL,
	[Option4Value] [int] NULL,
	[Option5Value] [int] NULL,
	[Option6Value] [int] NULL,
	[RequestContact] [smallint] NULL,
	[MemberComments] [varchar](max) NULL,
	[InsertDate] [datetime2](7) NULL,
	[CallbackEmailAddress] [varchar](50) NULL,
	[CallbackPhoneNumber] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ResponseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_RealTimeFeedBackAll]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_RealTimeFeedBackAll](
	[RealTimeFeedbackID] [int] IDENTITY(1,1) NOT NULL,
	[EntryType] [varchar](20) NOT NULL,
	[EntryStatusID] [int] NOT NULL,
	[EntryResponseID] [int] NOT NULL,
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[TechEnterpriseID] [varchar](7) NULL,
	[TechInternalID] [varchar](12) NULL,
	[RouteDate] [date] NOT NULL,
	[CustomerID] [int] NULL,
	[CustomerName] [varchar](50) NULL,
	[CustomerEmailAddress] [varchar](50) NULL,
	[CustomerPhoneNumber] [varchar](50) NULL,
	[CallbackEmailAddress] [varchar](50) NULL,
	[CallbackPhoneNumber] [varchar](50) NULL,
	[FeedbackStart] [datetime2](7) NULL,
	[FeedbackEnd] [datetime2](7) NULL,
	[PMCheckCode] [varchar](50) NULL,
	[CallType] [varchar](50) NULL,
	[BrandName] [varchar](50) NULL,
	[FeedbackSource] [varchar](5) NULL,
	[FeedbackScore] [decimal](2, 1) NULL,
	[OptionType] [smallint] NULL,
	[Option1Value] [bit] NULL,
	[Option2Value] [bit] NULL,
	[Option3Value] [bit] NULL,
	[Option4Value] [bit] NULL,
	[Option5Value] [bit] NULL,
	[Option6Value] [bit] NULL,
	[RequestContact] [bit] NULL,
	[QuestionOtherComment] [varchar](max) NULL,
	[TextRatingComment] [varchar](max) NULL,
	[TextSurveyComment] [varchar](max) NULL,
	[ExternalCreateDate] [datetime2](7) NULL,
	[CreateDate] [datetime2](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[RealTimeFeedbackID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_RealTimeFeedBackTD]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_RealTimeFeedBackTD](
	[RealTimeFeedbackID] [bigint] IDENTITY(1,1) NOT NULL,
	[SMSStatusID] [bigint] NOT NULL,
	[SMSResponseID] [bigint] NOT NULL,
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[RouteDate] [date] NOT NULL,
	[TechInternalID] [varchar](12) NULL,
	[TechEnterpriseID] [varchar](7) NULL,
	[BrandName] [varchar](50) NULL,
	[PaymentMethod] [varchar](20) NULL,
	[StatusCode] [varchar](10) NULL,
	[CallCode] [varchar](10) NULL,
	[PMCheckCode] [varchar](10) NULL,
	[CustomerID] [int] NULL,
	[CustomerName] [varchar](100) NULL,
	[CustomerZipCode] [varchar](10) NULL,
	[NotificationType] [varchar](10) NOT NULL,
	[NotificationID] [varchar](100) NOT NULL,
	[FeedbackScore] [decimal](2, 1) NULL,
	[InitialFeedbackScore] [decimal](2, 1) NULL,
	[FinalFeedbackScore] [decimal](2, 1) NULL,
	[LowRatingAdditionalFeedback] [varchar](max) NULL,
	[HighRatingAdditionalFeedback] [varchar](max) NULL,
	[FeedbackRatingComment] [varchar](max) NULL,
	[Q2A_1] [bit] NOT NULL,
	[Q2A_2] [bit] NOT NULL,
	[Q2A_3] [bit] NOT NULL,
	[Q2A_4] [bit] NOT NULL,
	[Q2A_5] [bit] NOT NULL,
	[Q2A_6] [bit] NOT NULL,
	[Q2A_Other] [varchar](max) NULL,
	[Q2B_1] [bit] NOT NULL,
	[Q2B_2] [bit] NOT NULL,
	[Q2B_3] [bit] NOT NULL,
	[Q2B_4] [bit] NOT NULL,
	[Q2B_5] [bit] NOT NULL,
	[Q2B_6] [bit] NOT NULL,
	[Q2B_Other] [varchar](max) NULL,
	[Q3] [bit] NOT NULL,
	[Q4_1] [varchar](100) NULL,
	[Q4_2] [varchar](50) NULL,
	[SourceType] [varchar](5) NULL,
	[SourceCreatedDateTime] [datetime2](7) NOT NULL,
	[ConfirmITResponseID] [bigint] NULL,
	[ConfirmITSurveyID] [varchar](20) NULL,
	[ConfirmITStatus] [varchar](30) NULL,
	[InterviewStart] [datetime2](7) NULL,
	[InterviewEnd] [datetime2](7) NULL,
	[CreateDateTime] [datetime2](7) NULL,
	[ModifiedDateTime] [datetime2](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[RealTimeFeedbackID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_RealTimeFeedBackTextResp]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_RealTimeFeedBackTextResp](
	[RTFResponseID] [int] NOT NULL,
	[RTFStatusID] [int] NOT NULL,
	[Response] [varchar](max) NULL,
	[ResponseModfied] [varchar](500) NULL,
	[ResponseCreatedDateTime] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[RTFResponseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_RealTimeFeedBackTextStatus]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_RealTimeFeedBackTextStatus](
	[RTFStatusID] [int] NOT NULL,
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[FromNumber] [varchar](50) NULL,
	[ToNumber] [varchar](50) NULL,
	[SourceType] [varchar](3) NULL,
	[StatusState] [varchar](50) NULL,
	[StatusCreatedDateTime] [datetime2](7) NULL,
	[StatusModifiedDateTime] [datetime2](7) NULL,
	[TechInternalID] [varchar](12) NULL,
	[TechUserID] [varchar](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[RTFStatusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_Reminders]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Reminders](
	[ReminderID] [int] NOT NULL,
	[OwnerType] [varchar](30) NULL,
	[OwnerSubID] [int] NULL,
	[ServiceUnit] [varchar](7) NULL,
	[ServiceOrder] [varchar](8) NULL,
	[ReminderDate] [date] NULL,
	[ModifiedEnterpriseID] [varchar](8) NULL,
	[ModifiedDateTime] [datetime2](7) NULL,
	[LastReminderDateTime] [datetime2](7) NULL,
 CONSTRAINT [PK_TBL_Reminders] PRIMARY KEY CLUSTERED 
(
	[ReminderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_Schedule]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Schedule](
	[ScheduleID] [int] IDENTITY(1,1) NOT NULL,
	[OwnerType] [varchar](30) NOT NULL,
	[OwnerID] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[LastExecutionDate] [datetime] NULL,
	[StartTime] [time](0) NOT NULL,
	[EndTime] [time](0) NOT NULL,
	[Frequency] [varchar](4) NOT NULL,
	[FrequencyModifier] [int] NOT NULL,
	[DaysOfWeek] [varchar](7) NOT NULL,
	[CalendarWeekOfMonth] [varchar](6) NOT NULL,
	[CreatedEnterpriseID] [varchar](8) NOT NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[ModifiedEnterpriseID] [varchar](50) NULL,
	[ModifiedDateTime] [datetime] NULL,
 CONSTRAINT [PK_TBL_Schedule] PRIMARY KEY CLUSTERED 
(
	[ScheduleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ServiceOrder]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ServiceOrder](
	[Region] [char](7) NULL,
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[LocationSuffix] [smallint] NOT NULL,
	[ItemSuffix] [int] NOT NULL,
	[ServicingOrganizationCode] [char](1) NULL,
	[ServicingOrganization] [char](5) NULL,
	[PaymentMethod] [char](5) NULL,
	[ThirdPartyType] [char](1) NULL,
	[ClientName] [char](45) NULL,
	[ClientCoverage] [char](2) NULL,
	[OriginalThirdPartyID] [char](6) NULL,
	[ThirdPartyID] [char](6) NULL,
	[CombinedThirdPartyID] [char](6) NULL,
	[CTA] [char](20) NULL,
	[CTAExpirationDate] [date] NULL,
	[AuthorizationNumber] [char](20) NULL,
	[PONumber] [char](18) NULL,
	[Rework] [smallint] NOT NULL,
	[RecallParent] [smallint] NOT NULL,
	[RecallChild] [smallint] NOT NULL,
	[RecallParentServiceOrder] [char](8) NULL,
	[RecallChildServiceOrder] [char](8) NULL,
	[PreventiveMaintenanceCheck] [char](1) NULL,
	[HelperNeeded] [char](1) NULL,
	[ServiceOrderType] [char](4) NULL,
	[ServiceTypeCode] [char](1) NULL,
	[IndustryCode] [char](2) NULL,
	[TPSIndustryCode] [char](2) NULL,
	[TPSSpecialtyCode] [char](15) NULL,
	[NPSSpecialty] [char](10) NULL,
	[MerchandiseSpecialtyCode] [char](10) NULL,
	[MerchandiseCode] [char](12) NULL,
	[WhereBoughtCode] [char](1) NULL,
	[WhereBoughtID] [char](7) NULL,
	[PartDivision] [char](3) NULL,
	[ManufacturerBrandName] [char](12) NULL,
	[SearsBrand] [char](12) NULL,
	[ProductItem] [char](5) NULL,
	[ModelNumber] [char](24) NULL,
	[SerialNumber] [char](20) NULL,
	[SellDate] [date] NULL,
	[ReceiveDate] [date] NULL,
	[BranchUnit] [char](7) NULL,
	[WorkAreaCode] [char](3) NULL,
	[CustomerZip] [char](5) NULL,
	[CustomerType] [char](1) NULL,
	[StoreStock] [smallint] NOT NULL,
	[ServiceOrderPaymentMethod] [char](2) NULL,
	[ServiceRequestDescription] [varchar](66) NULL,
	[SpecialInstructionsDescOne] [varchar](60) NULL,
	[SpecialInstructionsDescTwo] [varchar](60) NULL,
	[CreateUnitType] [char](10) NULL,
	[CreateUnitName] [char](27) NULL,
	[ServiceOrderCreateUnit] [char](7) NULL,
	[ServiceOrderCreateEmployee] [char](7) NULL,
	[ServiceOrderCreateDate] [date] NULL,
	[ServiceOrderCreateTime] [time](3) NULL,
	[FirstAvailableDate] [date] NULL,
	[ServiceOrigScheduleDate] [date] NULL,
	[ServiceOrigScheduleFromTime] [char](8) NULL,
	[ServiceOrigScheduleToTime] [char](8) NULL,
	[ServiceScheduleDate] [date] NULL,
	[ServiceScheduleFromTime] [char](8) NULL,
	[ServiceScheduleToTime] [char](8) NULL,
	[ServiceOrderStatusCode] [char](2) NULL,
	[ServiceOrderStatusDate] [date] NULL,
	[CompleteCode] [char](2) NULL,
	[SubAccountCode] [char](3) NULL,
	[ServiceCallType] [char](5) NULL,
	[PrimaryJobCode] [decimal](5, 0) NULL,
	[JobCodeChargeCode] [char](1) NULL,
	[JobCodeDescription] [char](50) NULL,
	[ChargeUnit] [char](7) NULL,
	[CancelCode] [char](4) NULL,
	[PriorityCode] [char](1) NULL,
	[StoreStockNumber] [char](10) NULL,
	[CreateAccountingYear] [float] NULL,
	[CreateAccountingQuater] [float] NULL,
	[CreateAccountingMonth] [float] NULL,
	[CreateAccountingWeek] [float] NULL,
	[CreateDayOfWeek] [float] NULL,
	[CreateAccountingYearWeek] [float] NULL,
	[StatusAccountingYear] [float] NULL,
	[StatusAccountingQuater] [float] NULL,
	[StatusAccountingMonth] [float] NULL,
	[StatusAccountingWeek] [float] NULL,
	[StatusDayofWeek] [float] NULL,
	[StatusAccountingYearWeek] [float] NULL,
	[ResponseCalendar] [smallint] NULL,
	[ResponseTime] [smallint] NULL,
	[StateOfServiceCalendar] [smallint] NULL,
	[StateOfService] [smallint] NULL,
	[DaysOld] [smallint] NOT NULL,
	[AttemptCount] [smallint] NOT NULL,
	[NotHomeCount] [smallint] NOT NULL,
	[PUSRCount] [smallint] NOT NULL,
	[OrderPartCount] [smallint] NOT NULL,
	[OtherCount] [smallint] NOT NULL,
	[CancelCount] [smallint] NOT NULL,
	[TransitTime] [smallint] NOT NULL,
	[ServiceAttemptTotalTime] [smallint] NOT NULL,
	[PartGrossAmountCC] [decimal](9, 2) NOT NULL,
	[LaborGrossAmountCC] [decimal](9, 2) NOT NULL,
	[TotalGrossAmountCC] [decimal](9, 2) NOT NULL,
	[CollectAmount] [decimal](8, 2) NOT NULL,
	[PartsRequired] [smallint] NOT NULL,
	[PartsResearch] [smallint] NOT NULL,
	[PartsCount] [smallint] NOT NULL,
	[PartsAmount] [decimal](8, 2) NOT NULL,
	[PartsOrderedCount] [smallint] NOT NULL,
	[PartsShippedCount] [smallint] NOT NULL,
	[PartsUsedCount] [smallint] NOT NULL,
	[PartsInstalledCount] [smallint] NOT NULL,
	[PartsInstalledAmount] [decimal](8, 2) NOT NULL,
	[PartsUninstalledCount] [smallint] NOT NULL,
	[PartsUninstalledAmount] [decimal](8, 2) NOT NULL,
	[PartsBackorderedCount] [smallint] NULL,
	[TSSL] [smallint] NOT NULL,
	[PPOPartsCount] [smallint] NOT NULL,
	[FirstPartOrderDate] [date] NULL,
	[LastPartOrderDate] [date] NULL,
	[RescheduleCount] [smallint] NOT NULL,
	[FirstRescheduleDate] [date] NULL,
	[LastRescheduleDate] [date] NULL,
	[FirstAttemptDate] [date] NULL,
	[LastAttemptDate] [date] NULL,
	[LastAttemptTechID] [char](7) NULL,
	[LastAttemptTechComment1] [char](60) NULL,
	[LastAttemptTechComment2] [char](60) NULL,
	[Servicer] [char](10) NULL,
	[Reserve] [smallint] NOT NULL,
	[MustWin] [smallint] NOT NULL,
	[TimeWindow] [char](10) NULL,
	[TimeWindowComp] [char](10) NULL,
	[MSOInboundCallCount] [smallint] NOT NULL,
	[MSOInboundCallsTotalMinutes] [decimal](8, 2) NOT NULL,
	[STACCallCount] [smallint] NOT NULL,
	[STACCallsTotalMinutes] [decimal](8, 2) NOT NULL,
	[AttemptDefectTotal] [int] NOT NULL,
	[RescheduleDefectTotal] [int] NOT NULL,
	[AgeDefectTotal] [int] NOT NULL,
	[PartsDefectTotal] [int] NOT NULL,
	[MSODefectTotal] [int] NOT NULL,
	[DefectTotal] [int] NOT NULL,
	[NextAttemptIncompleteRisk] [decimal](3, 2) NOT NULL,
	[PredictedNPSRepairScore] [smallint] NOT NULL,
	[ActualNPSRepairScore] [smallint] NULL,
	[Latitude] [decimal](9, 6) NULL,
	[Longitude] [decimal](9, 6) NULL,
	[NPSSurveyCustomerRequestCallback] [int] NULL,
	[SaleAmount] [decimal](9, 2) NULL,
	[WhereBoughtUnitType] [char](3) NULL,
	[RoutingStatus] [varchar](50) NULL,
	[RoutingStatusDateTime] [datetime2](7) NULL,
	[RealtimeFeedbackCount] [smallint] NULL,
	[RealtimeFeedbackDateTime] [datetime2](7) NULL,
	[RoutingStatusCode] [int] NULL,
	[FirstRescheduleCode] [varchar](4) NULL,
	[LastRescheduleCode] [varchar](4) NULL,
	[FirstAttemptCode] [varchar](2) NULL,
	[LastAttemptCode] [varchar](2) NULL,
	[AssignedTechnician] [varchar](8) NULL,
	[RouteSummaryStatusCode] [varchar](2) NULL,
	[RouteSummaryStatusDateTime] [datetime2](7) NULL,
	[EventLocation] [geography] NULL,
	[EventLatitude]  AS ([EventLocation].[Lat]),
	[EventLongitude]  AS ([EventLocation].[Long]),
	[ProductSalesRequest] [int] NULL,
	[ProductSalesRequestDateTime] [datetime2](7) NULL,
	[RecallDays] [int] NULL,
	[BadAddressFlag] [bit] NULL,
	[DoNotCallSHCFlag] [char](1) NULL,
	[LastUPSExceptionDate] [date] NULL,
	[HomeWarrantyLeadCoverageCode] [char](4) NULL,
	[GeneralPartsWarrantyExpirationDate] [date] NULL,
	[CurrentServiceProductExpirationDate] [date] NULL,
	[ScheduledDeliveryDate] [date] NULL,
	[RescheduledDeliveryDate] [date] NULL,
 CONSTRAINT [PK_TBL_ServiceOrder] PRIMARY KEY CLUSTERED 
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ServiceOrderAssignment]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ServiceOrderAssignment](
	[AssignmentID] [int] IDENTITY(1,1) NOT NULL,
	[QueueID] [int] NOT NULL,
	[ReportID] [int] NOT NULL,
	[GroupID] [int] NOT NULL,
	[AssociationID] [int] NULL,
	[EnterpriseID] [varchar](8) NOT NULL,
	[CreatedEnterpriseID] [varchar](8) NOT NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[ModifiedEnterpriseID] [varchar](50) NOT NULL,
	[ModifiedDateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_TBL_ServiceOrderAssignment] PRIMARY KEY CLUSTERED 
(
	[AssignmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ServiceOrderAssociation]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ServiceOrderAssociation](
	[AssociationID] [int] IDENTITY(1,1) NOT NULL,
	[QueueID] [int] NOT NULL,
	[ReportID] [int] NOT NULL,
	[GroupID] [int] NULL,
	[CreatedEnterpriseID] [varchar](8) NOT NULL,
	[CreatedDateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_TBL_ServiceOrderAssociation] PRIMARY KEY CLUSTERED 
(
	[AssociationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ServiceOrderAssociationGroup]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ServiceOrderAssociationGroup](
	[AG_GroupID] [varchar](10) NOT NULL,
	[AG_GroupName] [varchar](50) NOT NULL,
	[AG_GroupDescription] [varchar](200) NULL,
	[AG_IsActive] [bit] NOT NULL,
	[AG_LastUpdate] [datetime] NOT NULL,
	[AG_LastUpdateBy] [varchar](10) NOT NULL,
	[AG_NumericGroupID] [int] IDENTITY(1,1) NOT NULL,
	[AG_UpdateType] [int] NOT NULL,
	[AC_OwnerEnterpriseID] [varchar](10) NULL,
 CONSTRAINT [PK_TBL_ServiceOrderAssociationGroup] PRIMARY KEY CLUSTERED 
(
	[AG_GroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ServiceOrderAssociationPermission]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ServiceOrderAssociationPermission](
	[AP_AssociationID] [int] NOT NULL,
	[AP_OwnerID] [int] NOT NULL,
	[AP_GroupID] [varchar](10) NOT NULL,
	[AP_PermissionName] [varchar](50) NOT NULL,
	[AP_IsGranted] [bit] NOT NULL,
	[AP_LastUpdate] [datetime] NOT NULL,
	[AP_LastUpdateBy] [varchar](10) NOT NULL,
 CONSTRAINT [PK_TBL_ServiceOrderAssignmentRoles] PRIMARY KEY NONCLUSTERED 
(
	[AP_AssociationID] ASC,
	[AP_OwnerID] ASC,
	[AP_GroupID] ASC,
	[AP_PermissionName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ServiceOrderDataLastUpdate]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_ServiceOrderDataLastUpdate](
	[LastUpdate] [datetime] NOT NULL,
	[DailyUpdate] [int] NULL,
	[MySQLUpdate] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[LastUpdate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_ServiceOrderDefectCalculations]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ServiceOrderDefectCalculations](
	[DefectName] [varchar](15) NOT NULL,
	[InputValue] [int] NOT NULL,
	[OutputValue] [int] NOT NULL,
 CONSTRAINT [PK_TBL_ServiceOrderDefectCalculations] PRIMARY KEY CLUSTERED 
(
	[DefectName] ASC,
	[InputValue] ASC,
	[OutputValue] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ServiceOrderGroup]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ServiceOrderGroup](
	[GroupID] [int] NOT NULL,
	[GroupName] [varchar](100) NOT NULL,
	[IsEnabled] [bit] NULL,
	[OwnerEnterpriseID] [varchar](8) NULL,
	[CreatedDateTime] [datetime] NULL,
	[ModifiedDateTime] [datetime] NULL,
 CONSTRAINT [PK_TBL_ServiceOrderGroup] PRIMARY KEY CLUSTERED 
(
	[GroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ServiceOrderGroupItems]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ServiceOrderGroupItems](
	[GroupItemID] [int] IDENTITY(1,1) NOT NULL,
	[GroupID] [int] NOT NULL,
	[ControlID] [int] NOT NULL,
	[ControlValue] [varchar](100) NOT NULL,
	[CreateEnterpriseID] [varchar](100) NOT NULL,
	[CreateDateTime] [datetime] NULL,
 CONSTRAINT [PK_TBL_ServiceOrderGroupItems] PRIMARY KEY CLUSTERED 
(
	[GroupItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ServiceOrderPartsRecovery]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ServiceOrderPartsRecovery](
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[PartSequence] [int] NOT NULL,
	[RequestUser] [varchar](8) NOT NULL,
	[RequestCode] [int] NOT NULL,
	[RequestUserComments] [varchar](200) NULL,
	[RequestUserDate] [datetime] NULL,
	[ActionUser] [varchar](8) NULL,
	[ActionCode] [int] NOT NULL,
	[ActionUserComments] [varchar](200) NULL,
	[ActionUserDate] [datetime] NOT NULL,
 CONSTRAINT [PK_TBL_ServiceOrderPartsRecovery] PRIMARY KEY CLUSTERED 
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC,
	[PartSequence] ASC,
	[ActionUserDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ServiceOrderPartsRecoveryActions]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ServiceOrderPartsRecoveryActions](
	[ActionCode] [int] NOT NULL,
	[ActionType] [varchar](10) NOT NULL,
	[ActionDescription] [varchar](50) NOT NULL,
	[ModifyUser] [varchar](8) NULL,
	[ModifyDate] [date] NULL,
 CONSTRAINT [PK_TBL_ServiceOrderPartsRecoveryActions] PRIMARY KEY CLUSTERED 
(
	[ActionCode] ASC,
	[ActionType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ServiceOrderPartsRecoveryTechToTechAudit]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ServiceOrderPartsRecoveryTechToTechAudit](
	[Region] [char](7) NOT NULL,
	[District] [char](7) NOT NULL,
	[PostDate] [datetime] NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[ServiceOrderStatusCode] [char](2) NULL,
	[PartStatusCode] [char](1) NULL,
	[PartOrderDate] [datetime] NULL,
	[PartOrderTypeCode] [char](1) NULL,
	[PartDivision] [char](3) NULL,
	[PartListSource] [char](3) NULL,
	[PartNumber] [char](24) NULL,
	[PartDescription] [char](30) NULL,
	[PartOrderQuantity] [decimal](3, 0) NULL,
	[PartSellPrice] [decimal](9, 2) NULL,
	[PartSequenceNumber] [int] NOT NULL,
	[NPSStatus] [char](15) NULL,
	[NPNStatus] [char](2) NULL,
	[PartSupplier] [char](5) NULL,
	[PartCarrierCode] [char](4) NULL,
	[PartShipDate] [datetime] NULL,
	[PartTrackingNumber] [char](35) NULL,
	[DeliveryLastDate] [datetime] NULL,
	[PartOrderStatusGroup] [varchar](50) NULL,
	[ReturnStatus] [varchar](20) NULL,
	[ReturnReason] [varchar](10) NULL,
	[TransferType] [varchar](3) NOT NULL,
	[TransferDate] [date] NOT NULL,
	[TransferTime] [time](7) NOT NULL,
	[RecoveryType] [varchar](25) NULL,
	[ServiceOrderPaymentMethod] [varchar](2) NULL,
	[ServiceOrderStatusDate] [datetime] NULL,
	[PartDelivered] [int] NULL,
 CONSTRAINT [PK_ServiceOrderPartsRecoveryTechToTechAudit] PRIMARY KEY CLUSTERED 
(
	[District] ASC,
	[ServiceOrder] ASC,
	[PartSequenceNumber] ASC,
	[TransferType] ASC,
	[TransferDate] ASC,
	[TransferTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ServiceOrderPartsTracking]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ServiceOrderPartsTracking](
	[ServiceUnit] [varchar](7) NOT NULL,
	[ServiceOrder] [varchar](8) NOT NULL,
	[SOLine] [int] NOT NULL,
	[TrackingNumber] [varchar](30) NULL,
	[SOStatus] [varchar](2) NULL,
	[PromiseDate] [date] NULL,
	[Loc] [varchar](1) NULL,
	[NPSStatus] [varchar](1) NULL,
	[NPSStatusDate] [date] NULL,
	[LawsonPO] [varchar](7) NULL,
	[OrderDate] [date] NULL,
	[POLine] [int] NULL,
	[Div] [varchar](3) NULL,
	[Pls] [varchar](3) NULL,
	[Part] [varchar](24) NULL,
	[Description] [varchar](12) NULL,
	[Qty] [int] NULL,
	[NPNStatus] [varchar](2) NULL,
	[NPNStatusDate] [date] NULL,
	[ShipDate] [date] NULL,
	[Vendor] [varchar](5) NULL,
	[CarrierCode] [varchar](4) NULL,
 CONSTRAINT [PK_TBL_ServiceOrderPartsTracking_1] PRIMARY KEY CLUSTERED 
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC,
	[SOLine] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ServiceOrderProductSalesRequest]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ServiceOrderProductSalesRequest](
	[ProductSalesRequestID] [int] NOT NULL,
	[CustomerID] [int] NOT NULL,
	[ServiceUnit] [char](7) NULL,
	[ServiceOrder] [char](8) NULL,
	[CustomerFirstName] [varchar](50) NULL,
	[CustomerLastName] [varchar](50) NULL,
	[CustomerPhoneNumber] [char](10) NULL,
	[CustomerPhoneNumberSecondary] [char](10) NULL,
	[CustomerEmail] [varchar](50) NULL,
	[CustomerEmailSeconday] [varchar](50) NULL,
	[ItemSuffixNumber] [int] NULL,
	[CurrentApplianceDetails] [nvarchar](max) NULL,
	[CallBackDate] [date] NULL,
	[CallBackDateTimeFrom] [datetime2](7) NULL,
	[CallBackDateTimeTo] [datetime2](7) NULL,
	[CallBackRequested] [char](1) NULL,
	[CouponInfo] [nvarchar](max) NULL,
	[CreatedDateTime] [datetime2](7) NULL,
	[CreatedBy] [varchar](10) NULL,
	[LoadDateTime] [datetime2](7) NULL,
	[ProcessDateTime] [datetime2](7) NULL,
 CONSTRAINT [PK_TBL_ServiceOrderProductSalesRequest] PRIMARY KEY CLUSTERED 
(
	[ProductSalesRequestID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ServiceOrderPSRCustomerNotes]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ServiceOrderPSRCustomerNotes](
	[ProductSalesRequestID] [int] NOT NULL,
	[CustomerNotesRowNumber] [int] NOT NULL,
	[CustomerNotes] [nvarchar](max) NULL,
	[CreatedDateTime] [datetime2](7) NULL,
	[CreatedBy] [varchar](10) NULL,
	[LoadDateTime] [datetime2](7) NULL,
	[ProcessDateTime] [datetime2](7) NULL,
 CONSTRAINT [PK_TBL_ServiceOrderPSRCustomerNotes] PRIMARY KEY CLUSTERED 
(
	[ProductSalesRequestID] ASC,
	[CustomerNotesRowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ServiceOrderPSRGoodBetterBest]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ServiceOrderPSRGoodBetterBest](
	[ProductSalesRequestID] [int] NOT NULL,
	[GoodBetterBestRowNumber] [int] NOT NULL,
	[GoodBetterBest] [nvarchar](max) NULL,
	[CreatedDateTime] [datetime2](7) NULL,
	[CreatedBy] [varchar](10) NULL,
	[LoadDateTime] [datetime2](7) NULL,
	[ProcessDateTime] [datetime2](7) NULL,
 CONSTRAINT [PK_TBL_ServiceOrderPSRGoodBetterBest] PRIMARY KEY CLUSTERED 
(
	[ProductSalesRequestID] ASC,
	[GoodBetterBestRowNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ServiceOrderQA]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ServiceOrderQA](
	[Region] [char](7) NULL,
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[LocationSuffix] [smallint] NOT NULL,
	[ItemSuffix] [int] NOT NULL,
	[ServicingOrganizationCode] [char](1) NULL,
	[ServicingOrganization] [char](5) NULL,
	[PaymentMethod] [char](5) NULL,
	[ThirdPartyType] [char](1) NULL,
	[ClientName] [char](45) NULL,
	[ClientCoverage] [char](2) NULL,
	[OriginalThirdPartyID] [char](6) NULL,
	[ThirdPartyID] [char](6) NULL,
	[CombinedThirdPartyID] [char](6) NULL,
	[CTA] [char](20) NULL,
	[CTAExpirationDate] [date] NULL,
	[AuthorizationNumber] [char](20) NULL,
	[PONumber] [char](18) NULL,
	[Rework] [smallint] NOT NULL,
	[RecallParent] [smallint] NOT NULL,
	[RecallChild] [smallint] NOT NULL,
	[RecallParentServiceOrder] [char](8) NULL,
	[RecallChildServiceOrder] [char](8) NULL,
	[PreventiveMaintenanceCheck] [char](1) NULL,
	[HelperNeeded] [char](1) NULL,
	[ServiceOrderType] [char](4) NULL,
	[ServiceTypeCode] [char](1) NULL,
	[IndustryCode] [char](2) NULL,
	[TPSIndustryCode] [char](2) NULL,
	[TPSSpecialtyCode] [char](15) NULL,
	[NPSSpecialty] [char](10) NULL,
	[MerchandiseSpecialtyCode] [char](10) NULL,
	[MerchandiseCode] [char](12) NULL,
	[WhereBoughtCode] [char](1) NULL,
	[WhereBoughtID] [char](7) NULL,
	[PartDivision] [char](3) NULL,
	[ManufacturerBrandName] [char](12) NULL,
	[SearsBrand] [char](12) NULL,
	[ProductItem] [char](5) NULL,
	[ModelNumber] [char](24) NULL,
	[SerialNumber] [char](20) NULL,
	[SellDate] [date] NULL,
	[ReceiveDate] [date] NULL,
	[BranchUnit] [char](7) NULL,
	[WorkAreaCode] [char](3) NULL,
	[CustomerZip] [char](5) NULL,
	[CustomerType] [char](1) NULL,
	[StoreStock] [smallint] NOT NULL,
	[ServiceOrderPaymentMethod] [char](2) NULL,
	[ServiceRequestDescription] [varchar](66) NULL,
	[SpecialInstructionsDescOne] [varchar](60) NULL,
	[SpecialInstructionsDescTwo] [varchar](60) NULL,
	[CreateUnitType] [char](10) NULL,
	[CreateUnitName] [char](27) NULL,
	[ServiceOrderCreateUnit] [char](7) NULL,
	[ServiceOrderCreateEmployee] [char](7) NULL,
	[ServiceOrderCreateDate] [date] NULL,
	[ServiceOrderCreateTime] [time](3) NULL,
	[FirstAvailableDate] [date] NULL,
	[ServiceOrigScheduleDate] [date] NULL,
	[ServiceOrigScheduleFromTime] [char](8) NULL,
	[ServiceOrigScheduleToTime] [char](8) NULL,
	[ServiceScheduleDate] [date] NULL,
	[ServiceScheduleFromTime] [char](8) NULL,
	[ServiceScheduleToTime] [char](8) NULL,
	[ServiceOrderStatusCode] [char](2) NULL,
	[ServiceOrderStatusDate] [date] NULL,
	[CompleteCode] [char](2) NULL,
	[SubAccountCode] [char](3) NULL,
	[ServiceCallType] [char](5) NULL,
	[PrimaryJobCode] [decimal](5, 0) NULL,
	[JobCodeChargeCode] [char](1) NULL,
	[JobCodeDescription] [char](50) NULL,
	[ChargeUnit] [char](7) NULL,
	[CancelCode] [char](4) NULL,
	[PriorityCode] [char](1) NULL,
	[StoreStockNumber] [char](10) NULL,
	[CreateAccountingYear] [float] NULL,
	[CreateAccountingQuater] [float] NULL,
	[CreateAccountingMonth] [float] NULL,
	[CreateAccountingWeek] [float] NULL,
	[CreateDayOfWeek] [float] NULL,
	[CreateAccountingYearWeek] [float] NULL,
	[StatusAccountingYear] [float] NULL,
	[StatusAccountingQuater] [float] NULL,
	[StatusAccountingMonth] [float] NULL,
	[StatusAccountingWeek] [float] NULL,
	[StatusDayofWeek] [float] NULL,
	[StatusAccountingYearWeek] [float] NULL,
	[ResponseCalendar] [smallint] NULL,
	[ResponseTime] [smallint] NULL,
	[StateOfServiceCalendar] [smallint] NULL,
	[StateOfService] [smallint] NULL,
	[DaysOld] [smallint] NOT NULL,
	[AttemptCount] [smallint] NOT NULL,
	[NotHomeCount] [smallint] NOT NULL,
	[PUSRCount] [smallint] NOT NULL,
	[OrderPartCount] [smallint] NOT NULL,
	[OtherCount] [smallint] NOT NULL,
	[CancelCount] [smallint] NOT NULL,
	[TransitTime] [smallint] NOT NULL,
	[ServiceAttemptTotalTime] [smallint] NOT NULL,
	[PartGrossAmountCC] [decimal](9, 2) NOT NULL,
	[LaborGrossAmountCC] [decimal](9, 2) NOT NULL,
	[TotalGrossAmountCC] [decimal](9, 2) NOT NULL,
	[CollectAmount] [decimal](8, 2) NOT NULL,
	[PartsRequired] [smallint] NOT NULL,
	[PartsResearch] [smallint] NOT NULL,
	[PartsCount] [smallint] NOT NULL,
	[PartsAmount] [decimal](8, 2) NOT NULL,
	[PartsOrderedCount] [smallint] NOT NULL,
	[PartsShippedCount] [smallint] NOT NULL,
	[PartsUsedCount] [smallint] NOT NULL,
	[PartsInstalledCount] [smallint] NOT NULL,
	[PartsInstalledAmount] [decimal](8, 2) NOT NULL,
	[PartsUninstalledCount] [smallint] NOT NULL,
	[PartsUninstalledAmount] [decimal](8, 2) NOT NULL,
	[PartsBackorderedCount] [smallint] NULL,
	[TSSL] [smallint] NOT NULL,
	[PPOPartsCount] [smallint] NOT NULL,
	[FirstPartOrderDate] [date] NULL,
	[LastPartOrderDate] [date] NULL,
	[RescheduleCount] [smallint] NOT NULL,
	[FirstRescheduleDate] [date] NULL,
	[LastRescheduleDate] [date] NULL,
	[FirstAttemptDate] [date] NULL,
	[LastAttemptDate] [date] NULL,
	[LastAttemptTechID] [char](7) NULL,
	[LastAttemptTechComment1] [char](60) NULL,
	[LastAttemptTechComment2] [char](60) NULL,
	[Servicer] [char](10) NULL,
	[Reserve] [smallint] NOT NULL,
	[MustWin] [smallint] NOT NULL,
	[TimeWindow] [char](10) NULL,
	[TimeWindowComp] [char](10) NULL,
	[MSOInboundCallCount] [smallint] NOT NULL,
	[MSOInboundCallsTotalMinutes] [decimal](8, 2) NOT NULL,
	[STACCallCount] [smallint] NOT NULL,
	[STACCallsTotalMinutes] [decimal](8, 2) NOT NULL,
	[AttemptDefectTotal] [int] NOT NULL,
	[RescheduleDefectTotal] [int] NOT NULL,
	[AgeDefectTotal] [int] NOT NULL,
	[PartsDefectTotal] [int] NOT NULL,
	[MSODefectTotal] [int] NOT NULL,
	[DefectTotal] [int] NOT NULL,
	[NextAttemptIncompleteRisk] [decimal](3, 2) NOT NULL,
	[PredictedNPSRepairScore] [smallint] NOT NULL,
	[ActualNPSRepairScore] [smallint] NULL,
	[Latitude] [decimal](9, 6) NULL,
	[Longitude] [decimal](9, 6) NULL,
	[NPSSurveyCustomerRequestCallback] [int] NULL,
	[SaleAmount] [decimal](9, 2) NULL,
 CONSTRAINT [PK_TBL_ServiceOrderQA] PRIMARY KEY CLUSTERED 
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ServiceOrderQueue]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ServiceOrderQueue](
	[QueueID] [int] NOT NULL,
	[QueueName] [varchar](100) NOT NULL,
	[EnterpriseID] [varchar](8) NOT NULL,
	[IsEnabled] [int] NOT NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[ModifiedDateTime] [datetime] NOT NULL,
	[IntendedUsers] [varchar](250) NULL,
	[Purpose] [varchar](500) NULL,
	[InjectionType] [int] NULL,
 CONSTRAINT [PK_TBL_ServiceOrderQueue] PRIMARY KEY CLUSTERED 
(
	[QueueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ServiceOrderQueueOptions]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ServiceOrderQueueOptions](
	[OptionID] [int] NOT NULL,
	[QueueID] [int] NOT NULL,
	[LayerID] [int] NULL,
	[SetID] [int] NOT NULL,
	[OptionName] [varchar](100) NOT NULL,
	[OptionOrder] [int] NOT NULL,
	[Closed] [int] NOT NULL,
	[DueDateAdjustment] [int] NOT NULL,
	[NextSetID] [int] NULL,
	[EnterpriseID] [varchar](8) NOT NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[ModifiedDateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_TBL_ServiceOrderQueueOptions_New] PRIMARY KEY CLUSTERED 
(
	[OptionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ServiceOrderReports]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ServiceOrderReports](
	[ReportID] [int] NOT NULL,
	[ReportName] [varchar](100) NOT NULL,
	[ReportCount] [int] NOT NULL,
	[ReportColor] [varchar](8) NOT NULL,
	[PriorityValue] [int] NOT NULL,
	[IsEnabled] [bit] NULL,
	[QuerySelect] [varchar](max) NOT NULL,
	[QueryFrom] [varchar](max) NOT NULL,
	[QueryWhere] [varchar](max) NOT NULL,
	[QueryGroupBy] [varchar](max) NOT NULL,
	[QueryOrderBy] [varchar](max) NOT NULL,
	[EnterpriseID] [varchar](8) NOT NULL,
	[CreateEnterpriseID] [varchar](8) NULL,
	[ModifyEnterpriseID] [varchar](8) NULL,
	[CreateDateTime] [datetime] NULL,
	[LastModifiedDateTime] [datetime] NULL,
	[LastUpdateDateTime] [datetime] NULL,
	[Frequency] [varchar](4) NULL,
	[FrequencyModifier] [int] NULL,
 CONSTRAINT [PK_TBL_ServiceOrderReports] PRIMARY KEY CLUSTERED 
(
	[ReportID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ServiceOrderReportsBackup]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ServiceOrderReportsBackup](
	[ReportID] [int] NOT NULL,
	[EnterpriseID] [varchar](8) NOT NULL,
	[ReportName] [varchar](100) NOT NULL,
	[ReportQuery] [varchar](max) NOT NULL,
	[ReportQueryCount] [varchar](max) NOT NULL,
	[ReportCount] [int] NOT NULL,
	[QueueQuery] [varchar](max) NOT NULL,
	[ReportColor] [varchar](8) NULL,
	[LastModifiedDateTime] [datetime] NULL,
	[LastUpdateDateTime] [datetime] NULL,
	[EmailRequested] [bit] NOT NULL,
	[LastEmailDateTime] [datetime] NULL,
	[QuerySelect] [varchar](max) NULL,
	[QueryFrom] [varchar](max) NULL,
	[QueryWhere] [varchar](max) NULL,
	[QueryGroupBy] [varchar](max) NULL,
	[QueryOrderBy] [varchar](max) NULL,
 CONSTRAINT [PK_TBL_ServiceOrderReportsBackup] PRIMARY KEY CLUSTERED 
(
	[ReportID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ServiceOrderSurveys]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ServiceOrderSurveys](
	[ResponseID] [varchar](30) NOT NULL,
	[ResponseSet] [varchar](30) NULL,
	[IPAddress] [varchar](20) NULL,
	[FeedbackStatus] [int] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Finished] [int] NULL,
	[Customer] [int] NULL,
	[CustomerName] [varchar](150) NULL,
	[CustomerPhone] [char](10) NULL,
	[CustomerEmailAddress] [varchar](50) NULL,
	[ServiceUnit] [char](7) NULL,
	[ServiceOrder] [char](8) NULL,
	[FeedbackDate] [datetime] NULL,
	[QuestionOne] [int] NULL,
	[AssociateRating] [int] NULL,
	[QuestionThree] [int] NULL,
	[SatisfactionRating] [int] NULL,
	[CustomerRequestCallback] [int] NULL,
	[CustomerCallbackFirstName] [varchar](100) NULL,
	[CustomerCallbackLastName] [varchar](100) NULL,
	[CustomerCallbackEmailAddress] [varchar](100) NULL,
	[CustomerCallbackPhone] [char](10) NULL,
	[CustomerComments] [varchar](max) NULL,
	[CustomerSuggestions] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[ResponseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ServiceOrderTasks]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ServiceOrderTasks](
	[TaskID] [int] IDENTITY(1,1) NOT NULL,
	[QueueID] [int] NOT NULL,
	[ReportID] [int] NOT NULL,
	[GroupID] [int] NOT NULL,
	[AssociationID] [int] NULL,
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[PriorityValue] [int] NOT NULL,
	[DueDate] [date] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NOT NULL,
	[CreateEnterpriseID] [varchar](8) NULL,
	[UpdateEnterpriseID] [varchar](8) NULL,
	[ClaimedEnterpriseID] [varchar](8) NULL,
	[LogID] [int] NULL,
	[ClaimID] [int] NULL,
 CONSTRAINT [PK_TBL_ServiceOrderTask] PRIMARY KEY CLUSTERED 
(
	[TaskID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ServiceOrderTasksClaimed]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ServiceOrderTasksClaimed](
	[ClaimID] [int] IDENTITY(1,1) NOT NULL,
	[LogID] [int] NULL,
	[ClaimedEnterpriseID] [varchar](8) NULL,
	[ModifiedEnterpriseID] [varchar](8) NULL,
	[ModifiedDateTime] [datetime] NULL,
	[TaskID] [int] NULL,
 CONSTRAINT [PK_TBL_ServiceOrderClaimLog] PRIMARY KEY CLUSTERED 
(
	[ClaimID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ServiceOrderTasksLog]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ServiceOrderTasksLog](
	[LogID] [int] IDENTITY(1,1) NOT NULL,
	[TaskID] [int] NOT NULL,
	[OptionID] [int] NOT NULL,
	[EnterpriseID] [varchar](8) NOT NULL,
	[ModifiedDateTime] [datetime] NOT NULL,
	[CreateDateTime] [datetime] NULL,
	[CreateEnterpriseID] [varchar](8) NULL,
 CONSTRAINT [PK_TBL_ServiceOrderTasksLog] PRIMARY KEY CLUSTERED 
(
	[LogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ServiceOrderTasksStage]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ServiceOrderTasksStage](
	[QueueID] [int] NOT NULL,
	[ReportID] [int] NOT NULL,
	[GroupID] [int] NOT NULL,
	[AssociationID] [int] NULL,
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[PriorityValue] [int] NOT NULL,
	[DueDate] [date] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NOT NULL,
	[CreateEnterpriseID] [varchar](8) NULL,
	[UpdateEnterpriseID] [varchar](8) NULL,
	[Frequency] [varchar](4) NOT NULL,
	[FrequencyModifier] [int] NOT NULL,
	[ReadyForUploadFlag] [bit] NOT NULL,
	[UploadedFlag] [bit] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_SSTOrderUpload]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_SSTOrderUpload](
	[order_upload_id] [bigint] NOT NULL,
	[user_id] [varchar](8) NOT NULL,
	[nps_id] [varchar](7) NOT NULL,
	[unit_lu_id] [bigint] NOT NULL,
	[unit_number] [varchar](7) NOT NULL,
	[order_number] [varchar](8) NOT NULL,
	[route_date] [date] NOT NULL,
	[CallCode] [char](2) NULL,
	[RescheduleDate] [date] NULL,
	[rescheduleFromTime] [char](5) NULL,
	[rescheduleToTime] [char](5) NULL,
	[helperFlag] [char](1) NULL,
	[ArriveTime] [char](5) NULL,
	[CallCloseTime] [char](5) NULL,
	[PrintTime] [char](5) NULL,
	[thirdPartyExpirationDate] [date] NULL,
	[thirdPartyContractNumber] [varchar](20) NULL,
	[authorizationNumber] [varchar](20) NULL,
	[thirdPartyContractNumDelete] [char](1) NULL,
	[thirdPartyExpirationDateDelete] [char](1) NULL,
	[RescheduleReasonCode] [char](4) NULL,
	[spDeclineReasonCode] [char](1) NULL,
	[cancelReasonCode] [char](4) NULL,
	[specialInstructions] [varchar](250) NULL,
	[thirdPartyId] [char](6) NULL,
	[techComments] [varchar](250) NULL,
	[RescheduleStatus] [char](1) NULL,
	[partPickupFl] [char](1) NULL,
	[forceFl] [char](1) NULL,
	[extraDuration] [int] NULL,
	[OfferID] [int] NULL,
	[AttemptCount] [int] NULL,
	[emergencyFl] [char](1) NULL,
	[recallFl] [char](1) NULL,
	[rescheduleNoAttemptFl] [char](1) NULL,
	[returnTechType] [char](1) NULL,
	[helperCode] [char](1) NULL,
	[importanceAttemptCount] [int] NULL,
	[sywrMemberId] [varchar](20) NULL,
	[partInstaller] [char](1) NULL,
	[purchaseDate] [date] NULL,
	[purchaseLocation] [char](7) NULL,
	[deliveryDate] [date] NULL,
	[productReading] [varchar](250) NULL,
	[searsPurchase] [char](1) NULL,
	[productUsage] [char](1) NULL,
	[description] [varchar](250) NULL,
	[brand] [varchar](12) NULL,
	[model] [varchar](24) NULL,
	[serial] [varchar](20) NULL,
	[modelSerialNaReason] [char](4) NULL,
	[namePrefix] [varchar](4) NULL,
	[firstName] [varchar](50) NULL,
	[middleName] [varchar](50) NULL,
	[lastName] [varchar](50) NULL,
	[nameSuffix] [varchar](4) NULL,
	[address1] [varchar](50) NULL,
	[address2] [varchar](50) NULL,
	[apartment] [varchar](50) NULL,
	[city] [varchar](50) NULL,
	[state] [varchar](50) NULL,
	[zipCode] [varchar](50) NULL,
	[mainPhone] [varchar](50) NULL,
	[altPhone] [varchar](50) NULL,
	[customerInstructions] [varchar](250) NULL,
	[CustomerNumber] [int] NULL,
	[email] [varchar](250) NULL,
	[emailPromoFlag] [char](1) NULL,
	[spApproval] [char](1) NULL,
	[promoteOtherWithReason] [char](1) NULL,
	[laborTimeAndHalf] [char](1) NULL,
	[productSellingPrice] [decimal](9, 2) NULL,
	[GrandTotal] [decimal](9, 2) NULL,
	[prePaidAmount] [decimal](9, 2) NULL,
	[AmountCollected] [decimal](9, 2) NULL,
	[partsTaxPercent] [decimal](9, 3) NULL,
	[partsTax] [decimal](9, 2) NULL,
	[partsNetAmount] [decimal](9, 2) NULL,
	[laborTaxPercent] [decimal](9, 3) NULL,
	[laborTaxCollected] [decimal](9, 2) NULL,
	[laborTotal] [decimal](9, 2) NULL,
	[basicHomeCallCharge] [decimal](9, 3) NULL,
	[serviceProductDollars] [decimal](9, 2) NULL,
	[serviceProductTaxDollars] [decimal](9, 2) NULL,
	[spTotal] [decimal](9, 3) NULL,
	[acceptIndicator] [varchar](5) NULL,
	[reasonCodeEstimateDeclined] [char](4) NULL,
	[taxExemptNum] [varchar](50) NULL,
	[custPayTotal] [decimal](9, 2) NULL,
	[deductibleApplied] [decimal](9, 2) NULL,
	[newWaiveBasicCharge] [char](1) NULL,
	[partsAssociateDiscount] [decimal](9, 2) NULL,
	[partsPromotionDiscount] [decimal](9, 2) NULL,
	[partsCouponDiscount] [decimal](9, 2) NULL,
	[laborAssociateDiscount] [decimal](9, 2) NULL,
	[laborPromotionDiscount] [decimal](9, 2) NULL,
	[laborCouponDiscount] [decimal](9, 2) NULL,
	[spAssociateDiscount] [decimal](9, 2) NULL,
	[couponNumber] [varchar](50) NULL,
	[discountReasonCode1] [char](4) NULL,
	[discountReasonCode2] [char](4) NULL,
	[primaryPaymentMethod] [char](2) NULL,
	[primaryAmountCollected] [decimal](9, 2) NULL,
	[primaryExpirationDate] [varchar](10) NULL,
	[secondaryPaymentMethod] [char](2) NULL,
	[secondaryAmountCollected] [decimal](9, 2) NULL,
	[secondaryExpirationDate] [varchar](10) NULL,
	[payrollTransferStore] [char](7) NULL,
	[payrollTransferSubAccount] [varchar](3) NULL,
	[primaryToken] [varchar](50) NULL,
	[primarySettlementKey] [varchar](50) NULL,
	[secondaryToken] [varchar](50) NULL,
	[secondarySettlementKey] [varchar](50) NULL,
	[parts] [nvarchar](max) NULL,
	[jobCode] [nvarchar](max) NULL,
	[created_by] [varchar](255) NOT NULL,
	[created_ts] [datetime2](7) NOT NULL,
	[updated_by] [varchar](255) NOT NULL,
	[updated_ts] [datetime2](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[order_upload_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_SSTOrderUploadJobCodes]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_SSTOrderUploadJobCodes](
	[order_upload_id] [int] NOT NULL,
	[jobCodeSequence] [int] NOT NULL,
	[unit] [char](7) NULL,
	[orderNumber] [char](8) NULL,
	[routeDate] [date] NULL,
	[coverageCode] [char](2) NULL,
	[jobCode] [char](5) NULL,
	[chargeCode] [char](1) NULL,
	[relatedCodesFlag] [char](1) NULL,
	[calculatedPrice] [decimal](9, 2) NULL,
	[oblKey] [varchar](5) NULL,
	[custPayChargeAmount] [decimal](9, 2) NULL,
 CONSTRAINT [PK_TBL_SSTOrderUploadJobCodes] PRIMARY KEY CLUSTERED 
(
	[order_upload_id] ASC,
	[jobCodeSequence] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_SSTOrderUploadParts]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_SSTOrderUploadParts](
	[order_upload_id] [int] NOT NULL,
	[partSequence] [int] NOT NULL,
	[unit] [char](7) NULL,
	[orderNumber] [char](8) NULL,
	[routeDate] [date] NULL,
	[div] [char](2) NULL,
	[pls] [char](3) NULL,
	[partNumber] [varchar](24) NULL,
	[quantity] [int] NULL,
	[status] [char](1) NULL,
	[location] [char](1) NULL,
	[coverageCode] [char](2) NULL,
	[calculatedPartsPrice] [decimal](9, 2) NULL,
	[enteredPartPrice] [decimal](9, 2) NULL,
	[dateUsed] [date] NULL,
	[downloadPartIndicator] [char](1) NULL,
	[description] [varchar](22) NULL,
	[partInquiryValue] [char](1) NULL,
	[partPriceIndicator] [char](1) NULL,
	[oblKey] [varchar](5) NULL,
 CONSTRAINT [PK_TBL_SSTOrderUploadParts] PRIMARY KEY CLUSTERED 
(
	[order_upload_id] ASC,
	[partSequence] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_SSTOrderUploadStaging]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_SSTOrderUploadStaging](
	[order_upload_id] [bigint] NOT NULL,
	[user_id] [varchar](8) NOT NULL,
	[nps_id] [varchar](7) NOT NULL,
	[unit_lu_id] [bigint] NOT NULL,
	[unit_number] [varchar](7) NOT NULL,
	[order_number] [varchar](8) NOT NULL,
	[route_date] [date] NOT NULL,
	[CallCode] [char](2) NULL,
	[RescheduleDate] [date] NULL,
	[rescheduleFromTime] [char](5) NULL,
	[rescheduleToTime] [char](5) NULL,
	[helperFlag] [char](1) NULL,
	[ArriveTime] [char](5) NULL,
	[CallCloseTime] [char](5) NULL,
	[PrintTime] [char](5) NULL,
	[thirdPartyExpirationDate] [date] NULL,
	[thirdPartyContractNumber] [varchar](20) NULL,
	[authorizationNumber] [varchar](20) NULL,
	[thirdPartyContractNumDelete] [char](1) NULL,
	[thirdPartyExpirationDateDelete] [char](1) NULL,
	[RescheduleReasonCode] [char](4) NULL,
	[spDeclineReasonCode] [char](1) NULL,
	[cancelReasonCode] [char](4) NULL,
	[specialInstructions] [varchar](250) NULL,
	[thirdPartyId] [char](6) NULL,
	[techComments] [varchar](250) NULL,
	[RescheduleStatus] [char](1) NULL,
	[partPickupFl] [char](1) NULL,
	[forceFl] [char](1) NULL,
	[extraDuration] [int] NULL,
	[OfferID] [int] NULL,
	[AttemptCount] [int] NULL,
	[emergencyFl] [char](1) NULL,
	[recallFl] [char](1) NULL,
	[rescheduleNoAttemptFl] [char](1) NULL,
	[returnTechType] [char](1) NULL,
	[helperCode] [char](1) NULL,
	[importanceAttemptCount] [int] NULL,
	[sywrMemberId] [varchar](20) NULL,
	[partInstaller] [char](1) NULL,
	[purchaseDate] [date] NULL,
	[purchaseLocation] [char](7) NULL,
	[deliveryDate] [date] NULL,
	[productReading] [varchar](250) NULL,
	[searsPurchase] [char](1) NULL,
	[productUsage] [char](1) NULL,
	[description] [varchar](250) NULL,
	[brand] [varchar](12) NULL,
	[model] [varchar](24) NULL,
	[serial] [varchar](20) NULL,
	[modelSerialNaReason] [char](4) NULL,
	[namePrefix] [varchar](4) NULL,
	[firstName] [varchar](50) NULL,
	[middleName] [varchar](50) NULL,
	[lastName] [varchar](50) NULL,
	[nameSuffix] [varchar](4) NULL,
	[address1] [varchar](50) NULL,
	[address2] [varchar](50) NULL,
	[apartment] [varchar](50) NULL,
	[city] [varchar](50) NULL,
	[state] [varchar](50) NULL,
	[zipCode] [varchar](50) NULL,
	[mainPhone] [varchar](50) NULL,
	[altPhone] [varchar](50) NULL,
	[customerInstructions] [varchar](250) NULL,
	[CustomerNumber] [int] NULL,
	[email] [varchar](250) NULL,
	[emailPromoFlag] [char](1) NULL,
	[spApproval] [char](1) NULL,
	[promoteOtherWithReason] [char](1) NULL,
	[laborTimeAndHalf] [char](1) NULL,
	[productSellingPrice] [decimal](9, 2) NULL,
	[GrandTotal] [decimal](9, 2) NULL,
	[prePaidAmount] [decimal](9, 2) NULL,
	[AmountCollected] [decimal](9, 2) NULL,
	[partsTaxPercent] [decimal](9, 3) NULL,
	[partsTax] [decimal](9, 2) NULL,
	[partsNetAmount] [decimal](9, 2) NULL,
	[laborTaxPercent] [decimal](9, 3) NULL,
	[laborTaxCollected] [decimal](9, 2) NULL,
	[laborTotal] [decimal](9, 2) NULL,
	[basicHomeCallCharge] [decimal](9, 3) NULL,
	[serviceProductDollars] [decimal](9, 2) NULL,
	[serviceProductTaxDollars] [decimal](9, 2) NULL,
	[spTotal] [decimal](9, 3) NULL,
	[acceptIndicator] [varchar](5) NULL,
	[reasonCodeEstimateDeclined] [char](4) NULL,
	[taxExemptNum] [varchar](50) NULL,
	[custPayTotal] [decimal](9, 2) NULL,
	[deductibleApplied] [decimal](9, 2) NULL,
	[newWaiveBasicCharge] [char](1) NULL,
	[partsAssociateDiscount] [decimal](9, 2) NULL,
	[partsPromotionDiscount] [decimal](9, 2) NULL,
	[partsCouponDiscount] [decimal](9, 2) NULL,
	[laborAssociateDiscount] [decimal](9, 2) NULL,
	[laborPromotionDiscount] [decimal](9, 2) NULL,
	[laborCouponDiscount] [decimal](9, 2) NULL,
	[spAssociateDiscount] [decimal](9, 2) NULL,
	[couponNumber] [varchar](50) NULL,
	[discountReasonCode1] [char](4) NULL,
	[discountReasonCode2] [char](4) NULL,
	[primaryPaymentMethod] [char](2) NULL,
	[primaryAmountCollected] [decimal](9, 2) NULL,
	[primaryExpirationDate] [varchar](10) NULL,
	[secondaryPaymentMethod] [char](2) NULL,
	[secondaryAmountCollected] [decimal](9, 2) NULL,
	[secondaryExpirationDate] [varchar](10) NULL,
	[payrollTransferStore] [char](7) NULL,
	[payrollTransferSubAccount] [varchar](3) NULL,
	[primaryToken] [varchar](50) NULL,
	[primarySettlementKey] [varchar](50) NULL,
	[secondaryToken] [varchar](50) NULL,
	[secondarySettlementKey] [varchar](50) NULL,
	[parts] [nvarchar](max) NULL,
	[jobCode] [nvarchar](max) NULL,
	[created_by] [varchar](255) NOT NULL,
	[created_ts] [datetime2](7) NOT NULL,
	[updated_by] [varchar](255) NOT NULL,
	[updated_ts] [datetime2](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[order_upload_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_STACCallTakerOccurrences]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_STACCallTakerOccurrences](
	[ReferenceNumber] [varchar](20) NOT NULL,
	[ServiceUnit] [varchar](8) NOT NULL,
	[ServiceOrder] [varchar](13) NOT NULL,
	[TechID] [varchar](10) NULL,
	[TechName] [varchar](50) NULL,
	[UserID] [varchar](10) NULL,
	[UserName] [varchar](50) NULL,
	[OpenDateTime] [datetime] NOT NULL,
	[Abandoned] [varchar](2) NULL,
	[CallDuration] [numeric](8, 2) NULL,
	[Symptom] [varchar](max) NULL,
	[Cure] [varchar](max) NULL,
	[CallCode] [varchar](254) NULL,
	[LastChangeDatetime] [datetime] NULL,
 CONSTRAINT [PK_TBL_STACCallTakerOccurrences] PRIMARY KEY CLUSTERED 
(
	[ReferenceNumber] ASC,
	[ServiceUnit] ASC,
	[ServiceOrder] ASC,
	[OpenDateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_SupplementaryValues]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_SupplementaryValues](
	[SupID] [int] IDENTITY(1,1) NOT NULL,
	[OwnerType] [varchar](100) NOT NULL,
	[OwnerID] [int] NULL,
	[SupValue] [int] NULL,
	[CreateEnterpriseID] [varchar](8) NOT NULL,
	[CreateDateTime] [datetime] NOT NULL,
	[ModifiedEnterpriseID] [varchar](8) NOT NULL,
	[ModifiedDateTime] [datetime] NOT NULL,
	[SecondaryValue] [int] NULL,
 CONSTRAINT [PK_TBL_SupplementaryValues] PRIMARY KEY CLUSTERED 
(
	[SupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_Tech_Route]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Tech_Route](
	[TR_RouteDate] [datetime] NOT NULL,
	[TR_TechID] [varchar](7) NOT NULL,
	[TR_OrderID] [int] NOT NULL,
	[TR_UnitNumber] [varchar](7) NOT NULL,
	[TR_ServiceOrder] [varchar](8) NOT NULL,
	[TR_Sequence] [tinyint] NOT NULL,
	[TR_ETAFrom] [datetime] NULL,
	[TR_ETATo] [datetime] NULL,
	[TR_Status] [char](2) NULL,
	[TR_MerchCode] [varchar](10) NULL,
	[TR_CustName] [varchar](30) NULL,
	[TR_CustAddy] [varchar](30) NULL,
	[TR_CustCity] [varchar](20) NULL,
	[TR_CustState] [char](2) NULL,
	[TR_CustZip] [char](5) NULL,
	[TR_LastUpdate] [datetime] NOT NULL,
	[TR_Lat] [numeric](10, 6) NULL,
	[TR_Long] [numeric](10, 6) NULL,
	[TR_StillOnRoute] [tinyint] NULL,
	[TR_Insert] [datetime] NULL,
	[TR_StatusTm] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_TechHubAfterAttempt]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[TBL_TechHubAfterAttempt](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[EnterpriseID] [varchar](255) NOT NULL,
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[TransactionDateTime] [datetime2](7) NOT NULL,
	[AfterAttempt] [nvarchar](max) NOT NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_TBL_TechHubAfterAttempt] PRIMARY KEY CLUSTERED 
(
	[EntryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_TechHubJobCodes]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_TechHubJobCodes](
	[Specialty] [char](10) NOT NULL,
	[MajorHeading] [varchar](50) NOT NULL,
	[SubHeading] [varchar](50) NOT NULL,
	[JobCode] [char](5) NOT NULL,
	[JobCodeDescription] [varchar](50) NOT NULL,
	[DifficultyLevel] [varchar](50) NOT NULL,
 CONSTRAINT [PK_TBL_TechHubJobCodes] PRIMARY KEY CLUSTERED 
(
	[Specialty] ASC,
	[MajorHeading] ASC,
	[SubHeading] ASC,
	[JobCode] ASC,
	[JobCodeDescription] ASC,
	[DifficultyLevel] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_TechHubReceiptNotify]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_TechHubReceiptNotify](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[EnterpriseID] [varchar](255) NOT NULL,
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[TransactionDateTime] [datetime2](7) NOT NULL,
	[Receipt] [nvarchar](max) NOT NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_TBL_TechHubReceiptNotify] PRIMARY KEY CLUSTERED 
(
	[EntryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ThirdPartyClientList]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ThirdPartyClientList](
	[thd_pty_id] [char](6) NOT NULL,
	[thd_pty_ds] [char](100) NULL,
	[thd_pty_typ] [char](1) NULL,
	[client_nm] [char](45) NULL,
	[sub_client_nm] [char](45) NULL,
	[plan] [char](100) NULL,
	[deduct] [int] NULL,
	[tax_deduct] [char](1) NULL,
	[cvg_cd] [char](2) NULL,
	[thd_pty_id_typ] [char](25) NULL,
 CONSTRAINT [PK_TBL_ThirdPartyClientList] PRIMARY KEY CLUSTERED 
(
	[thd_pty_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[VIEW_CustomerAddressBaseForGeocoding]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE VIEW [dbo].[VIEW_CustomerAddressBaseForGeocoding]
AS

SELECT
	CUSTADD.ServiceUnit
	,CUSTADD.ServiceOrder
	,Customer
	,AddressLocation
	,REPLACE(REPLACE(REPLACE(CUSTADD.InputAddress, '  ', ' '), '#', '%23'), ' ', '+') AS InputAddress
FROM
	(SELECT
		CUSTADD.ServiceUnit
		,CUSTADD.ServiceOrder
		,Customer
		,AddressLocation
		,CASE
			WHEN CustomerState = 'PR' THEN
			CASE
				WHEN LEFT(LTRIM(RTRIM(REPLACE(REPLACE(CustomerStreetAddress, ' # NONE', ''), '# NONE', ''))), 1) = '-' THEN RIGHT(LTRIM(RTRIM(REPLACE(REPLACE(CustomerStreetAddress, ' # NONE', ''), '# NONE', ''))), LEN(LTRIM(RTRIM(REPLACE(REPLACE(CustomerStreetAddress, ' # NONE', ''), '# NONE', '')))) - 1)
			ELSE
				LTRIM(RTRIM(REPLACE(REPLACE(CustomerStreetAddress, ' # NONE', ''), '# NONE', '')))
			END
			+ ', ' +
			LTRIM(RTRIM(CustomerCity))
			+ ', ' +
			LTRIM(RTRIM(CustomerZip))
			+ ', Puerto Rico'			
		ELSE
			CASE
				WHEN LEFT(LTRIM(RTRIM(REPLACE(REPLACE(CustomerStreetAddress, ' # NONE', ''), '# NONE', ''))), 1) = '-' THEN RIGHT(LTRIM(RTRIM(REPLACE(REPLACE(CustomerStreetAddress, ' # NONE', ''), '# NONE', ''))), LEN(LTRIM(RTRIM(REPLACE(REPLACE(CustomerStreetAddress, ' # NONE', ''), '# NONE', '')))) - 1)
			ELSE
				LTRIM(RTRIM(REPLACE(REPLACE(CustomerStreetAddress, ' # NONE', ''), '# NONE', '')))
			END
			+ ', ' +
			LTRIM(RTRIM(CustomerCity))
			+ ', ' +
			LTRIM(RTRIM(CustomerState))
			+ ' ' +
			LTRIM(RTRIM(CustomerZip))
		END AS InputAddress
	FROM
		ServiceOrder.dbo.TBL_NPSServiceOrderCustomerLookaside CUSTADD WITH (NOLOCK)
	WHERE
		CustomerStreetAddress NOT LIKE '%PO BOX%') CUSTADD


GO
/****** Object:  View [dbo].[VIEW_CustomerAddressForGeocoding]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







CREATE VIEW [dbo].[VIEW_CustomerAddressForGeocoding]
AS

SELECT DISTINCT
	CUSTADD.InputAddress
FROM
	ServiceOrder.dbo.VIEW_CustomerAddressBaseForGeocoding CUSTADD WITH (NOLOCK)
INNER JOIN
	(SELECT
		ServiceUnit
		,ServiceOrder
	FROM
		ServiceOrder.dbo.TBL_ServiceOrder WITH (NOLOCK)
	WHERE
		ServiceOrderStatusCode IN ('AT','NH','O','PA','PU','RN','WP','WS')
		AND
		ServiceScheduleDate IN (CAST(GETDATE() - 4 AS DATE), CAST(GETDATE() - 3 AS DATE), CAST(GETDATE() - 2 AS DATE), CAST(GETDATE() - 1 AS DATE),CAST(GETDATE() AS DATE), CAST(GETDATE() + 1 AS DATE), CAST(GETDATE() + 2 AS DATE) ,CAST(GETDATE() + 3 AS DATE), CAST(GETDATE() + 4 AS DATE)) -- remove after 2/10/17
		) SO
ON
	CUSTADD.ServiceUnit = SO.ServiceUnit
	AND
	CUSTADD.ServiceOrder = SO.ServiceOrder
LEFT JOIN
	(SELECT
		CustomerID
		,LocationSuffix
		,InputAddress
	FROM
		ServiceOrder.dbo.TBL_CustomerAddressGeocoding) CUSTADDGEO
ON
	CUSTADD.InputAddress = CUSTADDGEO.InputAddress
WHERE
	CUSTADDGEO.InputAddress IS NULL







GO
/****** Object:  View [dbo].[ServiceOrderDistressed]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ServiceOrderDistressed]
AS
SELECT 
	CAST(GETDATE() AS DATE) AS report_date
	, s.ServiceScheduleDate AS next_scheduled_date
	, s.AttemptCount AS made_attempts
	, ISNULL(a.Attempts_offsite, 0) AS Attempts_offsite
	, s.RescheduleCount AS reschedules
	, NULL AS sears_init_resched
	, s.DaysOld AS so_age
	, s.Region AS rgn_no
	, s.ServiceUnit AS svc_un_no
	, s.ServiceOrder AS so_no
	, CASE WHEN s.Servicer = 'W2' THEN s.ServicingOrganization ELSE s.Servicer END AS ServiceProvider
	, s.ServiceOrderStatusCode AS SOStatusCode
	, s.ServiceOrderStatusDate AS SOStatusDate
	, s.ServiceOrderCreateDate AS SOCreateDate
	, s.LastAttemptDate AS last_attempt_dt
	, s.LastAttemptCode AS last_attempt_callcode
	, s.PartDivision AS product_div
	, m.MerchandiseDescriptionTwo AS product_desc
	, s.OriginalThirdPartyID AS OriginalPROCID
	, s.CombinedThirdPartyID AS CurrentPROCID
	, t.thd_pty_id_typ AS ThirdPartyTypeCode
	, s.PaymentMethod AS coverage
	, s.PaymentMethod AS coverageGroup
	, c.CustomerName AS customername
	, s.PartsOrderedCount AS PartOrdered
	, s.LastPartOrderDate AS LastPartOrdered
FROM 
	ServiceOrder.dbo.TBL_ServiceOrder s WITH (NOLOCK)
LEFT JOIN
	(	SELECT 
			ServiceUnit
			,ServiceOrder
			,LTRIM(RTRIM(ISNULL(LTRIM(RTRIM(CustomerNamePrefix)), '') 
			+ ' ' + ISNULL(LTRIM(RTRIM(CustomerFirstName)), '') 
			+ ' ' +  ISNULL(LTRIM(RTRIM(CustomerMiddleName)), '') 
			+ CASE WHEN ISNULL(LTRIM(RTRIM(CustomerMiddleName)), '') = '' THEN ISNULL(LTRIM(RTRIM(CustomerLastName)), '') ELSE ' ' +  ISNULL(LTRIM(RTRIM(CustomerLastName)), '') END
			+ ' ' +  ISNULL(LTRIM(RTRIM(CustomerLastNameAdditional)), '') 
			+ ' ' +  ISNULL(LTRIM(RTRIM(CustomerNameSuffix)), ''))) 
			AS CustomerName
		FROM 
			ServiceOrder.dbo.TBL_NPSServiceOrderCustomerLookaside WITH (NOLOCK)
	) c
	ON s.ServiceUnit = c.ServiceUnit
	AND s.ServiceOrder = c.ServiceOrder
LEFT JOIN
	(	SELECT 
			ServiceUnit
			,ServiceOrder
			,SUM(ISNULL(OffsiteFlag, 0)) AS Attempts_offsite
		FROM 
			ServiceOrder.dbo.TBL_NPSServiceOrderAttempt WITH (NOLOCK)
		WHERE
			ISNULL(OffsiteFlag, 0) > 0
		GROUP BY
			ServiceUnit
			,ServiceOrder
	) a
	ON s.ServiceUnit = a.ServiceUnit
	AND s.ServiceOrder = a.ServiceOrder
LEFT JOIN
	ServiceOrder.dbo.TBL_ThirdPartyClientList t WITH (NOLOCK)
	ON s.CombinedThirdPartyID = t.thd_pty_id
LEFT JOIN
	ServiceOrder.dbo.TBL_NPJMerchandiseCode m WITH (NOLOCK)
	ON s.MerchandiseCode = m.Merchandise
WHERE
	s.ServiceOrderStatusCode NOT IN ('CO', 'CA', 'CP', 'ED', 'CV', 'IT', 'BS' ) 
	AND s.Region IN ('0000820', '0000830', '0000890', '0000900')
	AND s.ServiceOrderType IN ('SITE')
	AND (s.AttemptCount > 2 OR s.DaysOld > 14 OR s.RescheduleCount > 3)


GO
/****** Object:  View [dbo].[VIEW_CiboodleCustomerSession]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[VIEW_CiboodleCustomerSession]
AS

SELECT
	ServiceUnit
	,ServiceOrder
	,CustomerID
	,SessionID
	,SessionStartDateTime
	,SessionEndDateTime
	,SessionLengthMinutes
	,SessionComments
	,SessionLoadDateTime
FROM
	ServiceOrder.dbo.TBL_CiboodleCustomerSession WITH (NOLOCK)






GO
/****** Object:  View [dbo].[VIEW_NPSFirstDataConfirmation]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VIEW_NPSFirstDataConfirmation]
AS 
SELECT 
	ServiceUnit
	,ServiceOrder
	,UserDefinedFieldID
	,FACCategory
	,UserDefinedFieldLineItem
	,UserDefinedFieldLineItemSequence
	,UserDefinedFieldRuleID
	,EnterpriseID
	,UserDefinedFieldName
	,UserDefinedFieldValue
	,PFL_FL
	,CreateTimeStamp
FROM 
	ServiceOrder.dbo.TBL_NPSServiceOrderAdditionalInfo WITH (NOLOCK)
WHERE
	UserDefinedFieldName IN ('CC1CONFIRMATION', 'CC2CONFIRMATION')

GO
/****** Object:  View [dbo].[VIEW_NPSServiceOrderAdditionalRemarks]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[VIEW_NPSServiceOrderAdditionalRemarks]
AS

SELECT
	ServiceUnit
	,ServiceOrder
	,TextCode
	,EmployeeID
	,AdditionalRemarks
	,ModificationID
	,InquiryNumber
	,EntryNumber
	,CallBackDateTime
	,LastModifiedDateTime
FROM
	ServiceOrder.dbo.TBL_NPSServiceOrderAdditionalRemarks WITH (NOLOCK)



GO
/****** Object:  View [dbo].[VIEW_NPSServiceOrderAttemptJobCode]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[VIEW_NPSServiceOrderAttemptJobCode]
AS

SELECT
	ServiceUnit
	,ServiceOrder
	,ServiceCallDate
	,JobCode
	,JobCodeChargeCode
	,JobCodeEffectiveDate
	,JobCodeSequenceNumber
	,CoverageCode
	,LaborEnteredAmount
	,ChargeCodeRelatedCode
	,MerchandiseComponentType
	,ServiceObligerCode
	,OPType
	,SerialNumber
	,CommitTimestamp
FROM
	ServiceOrder.dbo.TBL_NPSServiceOrderAttemptJobCode WITH (NOLOCK)





GO
/****** Object:  View [dbo].[VIEW_NPSServiceOrderParts]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[VIEW_NPSServiceOrderParts]
AS

SELECT
	ServiceUnit
	,ServiceOrder
	,PartSequence
	,PartDivision
	,PartListSource
	,Part
	,PartDescription
	,PartType
	,PartQuantity
	,PartQuantityUsed
	,Coverage
	,PartActualSellPrice
	,PartStatus
	,PartStatusDate
	,PartExtendedWarrantySold
	,PartPromotion
	,PartOrderDate
	,EmployeeIdentification
	,PartQuantityOrdered
	,PartQuantityReceived
	,PartQuantityReceivedDate
	,PartInstalledDate
	,PartStockBin
	,TruckBin
	,PartDestinationBin
	,PartPriceEach
	,PartCostEach
	,PartPremiumPriceEach
	,PartTrackingInquiryCode
	,MerchandiseComponentType
	,LastActionTimestamp
FROM
	ServiceOrder.dbo.TBL_NPSServiceOrderParts WITH (NOLOCK)




GO
/****** Object:  View [dbo].[VIEW_NPSServiceOrderReschedules]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[VIEW_NPSServiceOrderReschedules]
AS

SELECT 
	ServiceUnit
	,ServiceOrder
	,HomeServicesCustomer
	,StatusCode
	,ScheduleChangeReasonCode
	,SystemTypeCode
	,ModificationID
	,ServiceScheduleDate
	,ModificationUnit
	,LastModifiedDateTime
FROM
	ServiceOrder.dbo.TBL_NPSServiceOrderReschedules WITH (NOLOCK)



GO
/****** Object:  View [dbo].[VIEW_NPSServiceOrderServiceAttempt]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VIEW_NPSServiceOrderServiceAttempt]
AS
SELECT        ServiceUnit, ServiceOrder, ServiceTotalMinutes, ServiceRescheduledDate, ServiceRescheduledTimeFrom, ServiceRescheduledTimeTo, ServiceCallDate, 
                         ServiceTechEmployee, ServiceTechFirstName, ServiceTechLastName, CallCode, AgreementApproval, OtherApproval, MeterReading, TransitTimeMinutes, 
                         TechnicianCommentsOne, TechnicianCommentsTwo, ETAMet, LastActionTimestamp, TechArrivalTime, TechEndTime
FROM            dbo.TBL_NPSServiceOrderServiceAttempt WITH (NOLOCK)

GO
/****** Object:  View [dbo].[VIEW_ServiceOrder]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO










CREATE VIEW [dbo].[VIEW_ServiceOrder]
AS  

SELECT
	COALESCE(HIER.Region, so.Region) AS Region
	, HIER.RegionName
	, HIER.RVPEnterpriseID
	, HIER.RVPFullName
	, HIER.TFDEnterpriseID
	, HIER.TFDFullName
	, HIER.TerritoryName	
	, SO.ServiceUnit
	, cr.UnitType
	, HIER.UnitName AS ServiceUnitName
	, SO.ServiceOrder
	, SO.CustomerID
	, SO.LocationSuffix
	, SO.ItemSuffix
	, SO.ServicingOrganizationCode
	, SO.ServicingOrganization
	, SO.PaymentMethod
	, SO.ThirdPartyType
	, SO.ClientName
	, SO.ClientCoverage
	, SO.OriginalThirdPartyID
	, SO.ThirdPartyID
	, SO.CombinedThirdPartyID
	, SO.CTA
	, SO.CTAExpirationDate
	, SO.AuthorizationNumber
	, SO.PONumber
	, SO.Rework
	, SO.RecallParent
	, SO.RecallChild
	, SO.RecallParentServiceOrder
	, SO.RecallChildServiceOrder
	, SO.PreventiveMaintenanceCheck
	, SO.HelperNeeded
	, SO.ServiceOrderType
	, SO.ServiceTypeCode
	, SO.IndustryCode
	, SO.TPSIndustryCode
	, SO.TPSSpecialtyCode
	, SO.NPSSpecialty
	, SO.MerchandiseSpecialtyCode
	, SO.MerchandiseCode
	, SO.WhereBoughtCode
	, SO.WhereBoughtID
	, SO.WhereBoughtUnitType
	, SO.SaleAmount
	, SO.PartDivision
	, SO.ManufacturerBrandName
	, SO.SearsBrand
	, SO.ProductItem
	, SO.ModelNumber
	, SO.SerialNumber
	, SO.SellDate
	, SO.ReceiveDate
	, SO.BranchUnit
	, SO.WorkAreaCode
	, SO.CustomerZip
	, SO.CustomerType
	, SO.StoreStock
	, SO.ServiceOrderPaymentMethod
	, SO.ServiceRequestDescription
	, SO.SpecialInstructionsDescOne
	, SO.SpecialInstructionsDescTwo
	, SO.CreateUnitType
	, SO.CreateUnitName
	, SO.ServiceOrderCreateUnit
	, SO.ServiceOrderCreateEmployee
	, SO.ServiceOrderCreateDate
	, SO.ServiceOrderCreateTime
	, SO.FirstAvailableDate
	, SO.ServiceOrigScheduleDate
	, SO.ServiceOrigScheduleFromTime
	, SO.ServiceOrigScheduleToTime
	, SO.ServiceScheduleDate
	, SO.ServiceScheduleFromTime
	, SO.ServiceScheduleToTime
	, SO.ServiceOrderStatusCode
	, SO.ServiceOrderStatusDate
	, SO.CompleteCode
	, SO.SubAccountCode
	, SO.ServiceCallType
	, SO.PrimaryJobCode
	, SO.JobCodeChargeCode
	, SO.JobCodeDescription
	, SO.ChargeUnit
	, SO.CancelCode
	, SO.PriorityCode
	, SO.StoreStockNumber
	, SO.CreateAccountingYear
	, SO.CreateAccountingQuater
	, SO.CreateAccountingMonth
	, SO.CreateAccountingWeek
	, SO.CreateDayOfWeek
	, SO.CreateAccountingYearWeek
	, SO.StatusAccountingYear
	, SO.StatusAccountingQuater
	, SO.StatusAccountingMonth
	, SO.StatusAccountingWeek
	, SO.StatusDayofWeek
	, SO.StatusAccountingYearWeek
	, SO.ResponseCalendar
	, SO.ResponseTime
	, SO.StateOfServiceCalendar
	, SO.StateOfService
	, SO.DaysOld
	, SO.AttemptCount
	, SO.NotHomeCount
	, SO.PUSRCount
	, SO.OrderPartCount
	, SO.OtherCount
	, SO.CancelCount
	, SO.TransitTime
	, SO.ServiceAttemptTotalTime
	, SO.PartGrossAmountCC
	, SO.LaborGrossAmountCC
	, SO.TotalGrossAmountCC
	, SO.CollectAmount
	, SO.PartsRequired
	, SO.PartsResearch
	, SO.PartsCount
	, SO.PartsAmount
	, SO.PartsOrderedCount
	, SO.PartsShippedCount
	, SO.PartsUsedCount
	, SO.PartsInstalledCount
	, SO.PartsInstalledAmount
	, SO.PartsUninstalledCount
	, SO.PartsUninstalledAmount
	, SO.PartsBackorderedCount
	, SO.TSSL
	, SO.PPOPartsCount
	, SO.FirstPartOrderDate
	, SO.LastPartOrderDate
	, SO.RescheduleCount
	, SO.FirstRescheduleDate
	, SO.LastRescheduleDate
	, SO.FirstAttemptDate
	, SO.LastAttemptDate
    , THIER.DSMEnterpriseID as LastAttemptDSMEnterpriseID
    , THIER.DSMFullName  as LastAttemptDSMName
	, THIER.TMEnterpriseID  as LastAttemptTMEnterpriseID
    , THIER.TMFullName as LastAttemptTMName
	, THIER.EnterpriseID as LastAttemptTechEnterpriseID
	, SO.LastAttemptTechID
	, SO.LastAttemptTechComment1
	, SO.LastAttemptTechComment2
	, SO.Servicer
	, SO.Reserve
	, SO.MustWin
	, SO.TimeWindow
	, SO.TimeWindowComp
	, SO.MSOInboundCallCount
	, SO.MSOInboundCallsTotalMinutes
	, SO.STACCallCount
	, SO.STACCallsTotalMinutes
	, SO.AttemptDefectTotal
	, SO.RescheduleDefectTotal
	, SO.AgeDefectTotal
	, SO.PartsDefectTotal
	, SO.MSODefectTotal
	, SO.DefectTotal
	, SO.NextAttemptIncompleteRisk
	, SO.PredictedNPSRepairScore
	, SO.ActualNPSRepairScore
	, SO.Latitude
	, SO.Longitude
	, SO.NPSSurveyCustomerRequestCallback
	, SO.RoutingStatus
	, SO.RoutingStatusDateTime
	, SO.RealtimeFeedbackCount
	, SO.RealtimeFeedbackDateTime
	, SO.FirstRescheduleCode
	, SO.LastRescheduleCode
	, SO.FirstAttemptCode
	, SO.LastAttemptCode
	, SO.RouteSummaryStatusCode
	, SO.RouteSummaryStatusDateTime
	, SO.AssignedTechnician
	, SO.ProductSalesRequest
	, SO.ProductSalesRequestDateTime
	, SO.RecallDays
	, SO.BadAddressFlag
	, SO.DoNotCallSHCFlag
	, SO.LastUPSExceptionDate
	, SO.HomeWarrantyLeadCoverageCode
	, SO.GeneralPartsWarrantyExpirationDate
	, SO.CurrentServiceProductExpirationDate
	, SO.ScheduledDeliveryDate
	, SO.RescheduledDeliveryDate
FROM
	ServiceOrder.dbo.TBL_ServiceOrder AS SO WITH (NOLOCK) 
LEFT OUTER JOIN
	(	SELECT 
			Unit
			, UnitName
			, TFDEnterpriseID
			, TFDFullName
			, TerritoryName
			, Region
			, RegionName
			, RVPDirEnterpriseID AS RVPEnterpriseID
			, RVPDirFullName AS RVPFullName
		FROM
			Employee.dbo.VIEW_EmployeeHierarchyIHHVACUnitRegion WITH (nolock)
	) AS HIER 
	ON SO.ServiceUnit = HIER.Unit
	LEFT OUTER JOIN  Employee.dbo.TBL_EmployeeHierarchyIHTechRegion as THIER WITH (nolock) ON THIER.NPSID = SO.LastAttemptTechID and THIER.Unit = SO.ServiceUnit
	LEFT OUTER JOIN Employee.dbo.TBL_UnitCrossReference as cr with (nolock) ON cr.unit = so.serviceunit











GO
/****** Object:  View [dbo].[VIEW_ServiceOrderDetailPage]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







CREATE VIEW [dbo].[VIEW_ServiceOrderDetailPage]
AS  

SELECT
	HIER.Region
	, HIER.RegionName
	, HIER.RVPEnterpriseID
	, HIER.RVPFullName
	, HIER.TFDEnterpriseID
	, HIER.TFDFullName
	, HIER.TerritoryName	
	, SO.ServiceUnit
	, HIER.UnitName AS ServiceUnitName
	, COALESCE(x.UnitType,'') as UnitType
	, COALESCE(x.NPSTypeCode,'') as NPSTypeCode
	, SO.ServiceOrder
	, SO.CustomerID
	, SO.LocationSuffix
	, SO.ItemSuffix
	, SO.ServicingOrganizationCode
	, SO.ServicingOrganization
	, SO.PaymentMethod
	, SO.ThirdPartyType
	, SO.ClientName
	, SO.ClientCoverage
	, SO.OriginalThirdPartyID
	, SO.ThirdPartyID
	, SO.CombinedThirdPartyID
	, SO.CTA
	, SO.CTAExpirationDate
	, SO.AuthorizationNumber
	, SO.PONumber
	, SO.Rework
	, SO.RecallParent
	, SO.RecallChild
	, SO.RecallParentServiceOrder
	, SO.RecallChildServiceOrder
	, SO.PreventiveMaintenanceCheck
	, SO.HelperNeeded
	, SO.ServiceOrderType
	, SO.ServiceTypeCode
	, SO.IndustryCode
	, SO.TPSIndustryCode
	, SO.TPSSpecialtyCode
	, SO.NPSSpecialty
	, SO.MerchandiseSpecialtyCode
	, SO.MerchandiseCode
	, SO.WhereBoughtCode
	, SO.WhereBoughtID
	, SO.WhereBoughtUnitType
	, SO.SaleAmount
	, SO.PartDivision
	, SO.ManufacturerBrandName
	, SO.SearsBrand
	, SO.ProductItem
	, SO.ModelNumber
	, SO.SerialNumber
	, SO.SellDate
	, SO.ReceiveDate
	, SO.BranchUnit
	, SO.WorkAreaCode
	, SO.CustomerZip
	, SO.CustomerType
	, SO.StoreStock
	, SO.ServiceOrderPaymentMethod
	, SO.ServiceRequestDescription
	, SO.SpecialInstructionsDescOne
	, SO.SpecialInstructionsDescTwo
	, SO.CreateUnitType
	, SO.CreateUnitName
	, SO.ServiceOrderCreateUnit
	, SO.ServiceOrderCreateEmployee
	, SO.ServiceOrderCreateDate
	, SO.ServiceOrderCreateTime
	, SO.FirstAvailableDate
	, SO.ServiceOrigScheduleDate
	, SO.ServiceOrigScheduleFromTime
	, SO.ServiceOrigScheduleToTime
	, SO.ServiceScheduleDate
	, SO.ServiceScheduleFromTime
	, SO.ServiceScheduleToTime
	, SO.ServiceOrderStatusCode
	, SO.ServiceOrderStatusDate
	, SO.CompleteCode
	, SO.SubAccountCode
	, SO.ServiceCallType
	, SO.PrimaryJobCode
	, SO.JobCodeChargeCode
	, SO.JobCodeDescription
	, SO.ChargeUnit
	, SO.CancelCode
	, SO.PriorityCode
	, SO.StoreStockNumber
	, SO.CreateAccountingYear
	, SO.CreateAccountingQuater
	, SO.CreateAccountingMonth
	, SO.CreateAccountingWeek
	, SO.CreateDayOfWeek
	, SO.CreateAccountingYearWeek
	, SO.StatusAccountingYear
	, SO.StatusAccountingQuater
	, SO.StatusAccountingMonth
	, SO.StatusAccountingWeek
	, SO.StatusDayofWeek
	, SO.StatusAccountingYearWeek
	, SO.ResponseCalendar
	, SO.ResponseTime
	, SO.StateOfServiceCalendar
	, SO.StateOfService
	, SO.DaysOld
	, SO.AttemptCount
	, SO.NotHomeCount
	, SO.PUSRCount
	, SO.OrderPartCount
	, SO.OtherCount
	, SO.CancelCount
	, SO.TransitTime
	, SO.ServiceAttemptTotalTime
	, SO.PartGrossAmountCC
	, SO.LaborGrossAmountCC
	, SO.TotalGrossAmountCC
	, SO.CollectAmount
	, SO.PartsRequired
	, SO.PartsResearch
	, SO.PartsCount
	, SO.PartsAmount
	, SO.PartsOrderedCount
	, SO.PartsShippedCount
	, SO.PartsUsedCount
	, SO.PartsInstalledCount
	, SO.PartsInstalledAmount
	, SO.PartsUninstalledCount
	, SO.PartsUninstalledAmount
	, SO.PartsBackorderedCount
	, SO.TSSL
	, SO.PPOPartsCount
	, SO.FirstPartOrderDate
	, SO.LastPartOrderDate
	, SO.RescheduleCount
	, SO.FirstRescheduleDate
	, SO.LastRescheduleDate
	, SO.FirstAttemptDate
	, SO.LastAttemptDate
    , THIER.DSMEnterpriseID as LastAttemptDSMEnterpriseID
    , THIER.DSMFullName  as LastAttemptDSMName
	, THIER.TMEnterpriseID  as LastAttemptTMEnterpriseID
    , THIER.TMFullName as LastAttemptTMName
	, THIER.EnterpriseID as LastAttemptTechEnterpriseID
	, SO.LastAttemptTechID
	, SO.LastAttemptTechComment1
	, SO.LastAttemptTechComment2
	, SO.Servicer
	, SO.Reserve
	, SO.MustWin
	, SO.TimeWindow
	, SO.TimeWindowComp
	, SO.MSOInboundCallCount
	, SO.MSOInboundCallsTotalMinutes
	, SO.STACCallCount
	, SO.STACCallsTotalMinutes
	, SO.AttemptDefectTotal
	, SO.RescheduleDefectTotal
	, SO.AgeDefectTotal
	, SO.PartsDefectTotal
	, SO.MSODefectTotal
	, SO.DefectTotal
	, SO.NextAttemptIncompleteRisk
	, SO.PredictedNPSRepairScore
	, SO.ActualNPSRepairScore
	, SO.EventLatitude
	, SO.EventLongitude
	, SO.NPSSurveyCustomerRequestCallback
	, SO.RoutingStatus
	, SO.RoutingStatusDateTime
	, SO.RealtimeFeedbackCount
	, SO.RealtimeFeedbackDateTime
	, SO.FirstRescheduleCode
	, SO.LastRescheduleCode
	, SO.FirstAttemptCode
	, SO.LastAttemptCode
	, SO.RouteSummaryStatusCode
	, SO.RouteSummaryStatusDateTime
	, SO.AssignedTechnician
	, SO.ProductSalesRequest
	, SO.ProductSalesRequestDateTime
	, SO.RecallDays
FROM
	ServiceOrder.dbo.TBL_ServiceOrder AS SO WITH (NOLOCK) 
LEFT OUTER JOIN
	(	SELECT 
			Unit
			, UnitName
			, TFDEnterpriseID
			, TFDFullName
			, TerritoryName
			, Region
			, RegionName
			, RVPDirEnterpriseID AS RVPEnterpriseID
			, RVPDirFullName AS RVPFullName
		FROM
			Employee.dbo.VIEW_EmployeeHierarchyIHHVACUnitRegion WITH (nolock)
	) AS HIER 
	ON SO.ServiceUnit = HIER.Unit
	LEFT OUTER JOIN  Employee.dbo.TBL_EmployeeHierarchyIHTechRegion as THIER WITH (nolock) ON THIER.NPSID = SO.LastAttemptTechID and THIER.Unit = SO.ServiceUnit
	LEFT OUTER JOIN Employee.dbo.TBL_UnitCrossReference as x WITH (NOLOCK) on SO.serviceunit = x.Unit







GO
/****** Object:  View [dbo].[VIEW_ServiceOrderPartsRecovery]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







CREATE VIEW [dbo].[VIEW_ServiceOrderPartsRecovery]
AS

SELECT
	PR.ServiceUnit
	,PR.ServiceOrder
	,PR.PartSequence
	,RequestUser
	,PR.RequestCode
	,RequestDescription
	,RequestUserComments
	,RequestUserDate
	,ActionUser
	,PR.ActionCode
	,ActionDescription
	,ActionUserComments
	,ActionUserDate
FROM
	ServiceOrder.dbo.TBL_ServiceOrderPartsRecovery PR WITH (NOLOCK)
INNER JOIN
	(SELECT
		ServiceUnit
		,ServiceOrder
		,PartSequence
		,MAX(ActionUserDate) AS MaxActionUserDate
	FROM
		ServiceOrder.dbo.TBL_ServiceOrderPartsRecovery WITH (NOLOCK)
	GROUP BY
		ServiceUnit
		,ServiceOrder
		,PartSequence) AS PRMAX
ON
	PR.ServiceUnit = PRMAX.ServiceUnit
	AND
	PR.ServiceOrder = PRMAX.ServiceOrder
	AND
	PR.PartSequence = PRMAX.PartSequence
	AND
	PR.ActionUserDate = PRMAX.MaxActionUserDate
INNER JOIN
	(SELECT
		ActionCode AS RequestCode
		,ActionDescription As RequestDescription
	FROM
		ServiceOrder.dbo.TBL_ServiceOrderPartsRecoveryActions WITH (NOLOCK)) REQ
ON
	PR.RequestCode = REQ.RequestCode
INNER JOIN
	(SELECT
		ActionCode
		,ActionDescription
	FROM
		ServiceOrder.dbo.TBL_ServiceOrderPartsRecoveryActions WITH (NOLOCK)) ACT
ON
	PR.ActionCode = ACT.ActionCode









GO
/****** Object:  View [dbo].[VIEW_ServiceOrderPartsRecoveryPerformance]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[VIEW_ServiceOrderPartsRecoveryPerformance]
AS

SELECT
	ISNULL(ServiceRegion, 'UNKNOWN') AS ServiceRegion
	,PR.ServiceUnit
	,PR.ServiceOrder
	,SKU
	,PR.PartSequence
	,PartQuantity
	,PartPriceEach
	,RequestUser
	,PR.RequestCode
	,RequestDescription
	,RequestUserComments
	,RequestUserDate
	,DATEDIFF(DD, RequestUserDate, CASE WHEN ActionType = 'Closed' THEN ActionUserDate ELSE GETDATE() END) AS RequestAge
	,ActionUser
	,ISNULL(ActionUserName, ActionUser) AS ActionUserName
	,PR.ActionCode
	,ActionType
	,ActionDescription
	,ActionUserComments
	,ActionUserDate
	,DATEDIFF(DD, ActionUserDate, CASE WHEN ActionType = 'Closed' THEN ActionUserDate ELSE GETDATE() END) AS ActionAge
FROM
	ServiceOrder.dbo.TBL_ServiceOrderPartsRecovery PR WITH (NOLOCK)
INNER JOIN
	(SELECT
		ActionCode AS RequestCode
		,ActionDescription AS RequestDescription
	FROM
		ServiceOrder.dbo.TBL_ServiceOrderPartsRecoveryActions WITH (NOLOCK)) REQ
ON
	PR.RequestCode = REQ.RequestCode
LEFT JOIN
	(SELECT
		ActionCode
		,ActionType
		,ActionDescription
	FROM
		ServiceOrder.dbo.TBL_ServiceOrderPartsRecoveryActions WITH (NOLOCK)) ACT
ON
	PR.ActionCode = ACT.ActionCode
INNER JOIN
	(SELECT
		ServiceUnit
		,ServiceOrder
		,PartDivision + PartListSource + Part AS SKU
		,PartSequence
		,PartQuantity
		,PartPriceEach
	FROM
		ServiceOrder.dbo.TBL_NPSServiceOrderParts WITH (NOLOCK)) PRT
ON
	PR.ServiceUnit = PRT.ServiceUnit
	AND
	PR.ServiceOrder = PRT.ServiceOrder
	AND
	PR.PartSequence = PRT.PartSequence
LEFT JOIN
	(SELECT
		Unit
		,ReportingRegion AS ServiceRegion
	FROM
		Employee.dbo.TBL_UnitCrossReference WITH (NOLOCK)) UCR
ON
	PR.ServiceUnit = UCR.Unit
LEFT JOIN
	(SELECT
		EnterpriseID
		,FirstName + ' ' + LastName AS ActionUserName
	FROM
		Employee.dbo.VIEW_Employee
	WHERE
		EnterpriseID IS NOT NULL
		AND
		EnterpriseID <> '') EMP
ON
	PR.ActionUser = EMP.EnterpriseID



GO
/****** Object:  View [dbo].[VIEW_ServiceOrderPartsRecoveryReporting]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [dbo].[VIEW_ServiceOrderPartsRecoveryReporting]
AS

SELECT
	ISNULL(ServiceRegion, 'UNKNOWN') AS ServiceRegion
	,PR.ServiceUnit
	,PR.ServiceOrder
	/* added Service Order Status and Payment - appears to be needed for some reporting*/
	,ServiceOrderStatusCode
	,ServiceOrderPaymentMethod
	/* added breakout of SKU required for some reporting*/
	,PRT.PartDivision
	,PRT.PartListSource
	,PRT.Part
	,SKU
	,PR.PartSequence
	,PartQuantity
	,PartPriceEach
	,RequestUser
	,PR.RequestCode
	,RequestDescription
	,RequestUserComments
	,RequestUserDate
	,DATEDIFF(DD, RequestUserDate, CASE WHEN ActionType = 'Close' THEN ActionUserDate ELSE GETDATE() END) AS RequestAge
	,ActionUser
	,ISNULL(ActionUserName, ActionUser) AS ActionUserName
	,PR.ActionCode
	,ActionType
	,ActionDescription
	,ActionUserComments
	,ActionUserDate
	,DATEDIFF(DD, ActionUserDate, CASE WHEN ActionType = 'Close' THEN ActionUserDate ELSE GETDATE() END) AS ActionAge
FROM
	ServiceOrder.dbo.TBL_ServiceOrderPartsRecovery PR WITH (NOLOCK)
INNER JOIN
	(	SELECT
			ServiceUnit
            ,ServiceOrder
            ,PartSequence
            ,MAX(ActionUserDate) AS MaxActionUserDate
		FROM
			ServiceOrder.dbo.TBL_ServiceOrderPartsRecovery WITH (NOLOCK)
		GROUP BY
			ServiceUnit
			,ServiceOrder
			,PartSequence
	) AS PRMAX
	ON PR.ServiceUnit = PRMAX.ServiceUnit
	AND PR.ServiceOrder = PRMAX.ServiceOrder
	AND PR.PartSequence = PRMAX.PartSequence
	AND PR.ActionUserDate = PRMAX.MaxActionUserDate
INNER JOIN
	(	SELECT
		ActionCode AS RequestCode
		,ActionDescription AS RequestDescription
		FROM
			ServiceOrder.dbo.TBL_ServiceOrderPartsRecoveryActions WITH (NOLOCK)
	) REQ
	ON PR.RequestCode = REQ.RequestCode
LEFT JOIN
	(	SELECT
			ActionCode
			,ActionType
			,ActionDescription
		FROM
            ServiceOrder.dbo.TBL_ServiceOrderPartsRecoveryActions WITH (NOLOCK)
	) ACT
	ON PR.ActionCode = ACT.ActionCode
INNER JOIN
	(	SELECT
			ServiceUnit
			,ServiceOrder
			,PartDivision
			,PartListSource
			,Part
			,PartDivision + PartListSource + Part AS SKU
			,PartSequence
			/* added Case statement to identify which part quantity to use*/
			, Case when PartQuantity < PartQuantityOrdered then PartQuantityOrdered else PartQuantity END as PartQuantity
			/* Took out PartPriceEach and using National Part Sell Price as PartPriceEach */
			/*,PartPriceEach*/
		FROM
			ServiceOrder.dbo.TBL_NPSServiceOrderParts WITH (NOLOCK)
	) PRT
	ON PR.ServiceUnit = PRT.ServiceUnit
	AND PR.ServiceOrder = PRT.ServiceOrder
	AND PR.PartSequence = PRT.PartSequence
INNER JOIN
	(	SELECT
			ServiceUnit
			,ServiceOrder
			,ServiceOrderStatusCode
			,ServiceOrderPaymentMethod
		FROM 
			ServiceOrder.dbo.TBL_ServiceOrder WITH (NOLOCK)
	) SO 
	ON PR.ServiceUnit = SO.ServiceUnit
	AND PR.ServiceOrder = SO.ServiceOrder
LEFT JOIN
	(	SELECT
			PartSellPrice as PartPriceEach
			,PartDivision
			,PartListSource
			,PartNumber
		FROM
			Parts.dbo.VIEW_Part WITH (NOLOCK)
	) PART
	ON PRT.PartDivision = PART.PartDivision 
	AND PRT.PartListSource = PART.PartListSource   
	AND PRT.Part = PART.PartNumber 
LEFT JOIN
	(	SELECT
			Unit
			,ReportingRegion AS ServiceRegion
		FROM
			Employee.dbo.TBL_UnitCrossReference WITH (NOLOCK)
	) UCR
	ON PR.ServiceUnit = UCR.Unit
LEFT JOIN
	(	SELECT
			EnterpriseID
			,FirstName + ' ' + LastName AS ActionUserName
		FROM
			Employee.dbo.VIEW_Employee
		WHERE
			EnterpriseID IS NOT NULL
			AND EnterpriseID <> ''
	) EMP
	ON PR.ActionUser = EMP.EnterpriseID
	



GO
/****** Object:  View [dbo].[VIEW_ServiceOrderSurveys]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VIEW_ServiceOrderSurveys]
AS
SELECT        ResponseID, ResponseSet, IPAddress, FeedbackStatus, StartDate, EndDate, Finished, Customer, CustomerName, CustomerPhone, CustomerEmailAddress, 
                         ServiceUnit, ServiceOrder, FeedbackDate, QuestionOne, AssociateRating, QuestionThree, SatisfactionRating, CustomerRequestCallback, 
                         CustomerCallbackFirstName, CustomerCallbackLastName, CustomerCallbackEmailAddress, CustomerCallbackPhone, CustomerComments, 
                         CustomerSuggestions
FROM            dbo.TBL_ServiceOrderSurveys WITH (NOLOCK)

GO
/****** Object:  View [dbo].[View_ServiceOrderWP]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_ServiceOrderWP] AS
SELECT 
	s.Region
	,s.ServiceUnit
	,s.ServiceOrder
	,s.CustomerID
--	,s.LocationSuffix
	,s.ItemSuffix
--	,s.ServicingOrganizationCode
	,s.ServicingOrganization
	,s.PaymentMethod
	,s.ThirdPartyType
	,s.ClientName
--	,s.ClientCoverage
--	,s.OriginalThirdPartyID
--	,s.ThirdPartyID
	,s.CombinedThirdPartyID
--	,s.CTA
--	,s.CTAExpirationDate
--	,s.AuthorizationNumber
--	,s.PONumber
--	,s.Rework
--	,s.RecallParent
	,s.RecallChild
	,s.RecallParentServiceOrder
--	,s.RecallChildServiceOrder
	,s.PreventiveMaintenanceCheck
	,s.HelperNeeded
	,s.ServiceOrderType
	,s.ServiceTypeCode
--	,s.IndustryCode
--	,s.TPSIndustryCode
--	,s.TPSSpecialtyCode
	,s.NPSSpecialty
	,s.MerchandiseSpecialtyCode
	,s.MerchandiseCode
--	,s.WhereBoughtCode
--	,s.WhereBoughtID
	,s.PartDivision
	,s.ManufacturerBrandName
--	,s.SearsBrand
	,s.ProductItem
	,s.ModelNumber
	,s.SerialNumber
	,s.SellDate
	,s.ReceiveDate
--	,s.BranchUnit
--	,s.WorkAreaCode
	,s.CustomerZip
	,s.CustomerType
	,s.StoreStock
	,s.ServiceOrderPaymentMethod
	,s.ServiceRequestDescription
	,s.SpecialInstructionsDescOne
	,s.SpecialInstructionsDescTwo
	,s.CreateUnitType
	,s.CreateUnitName
	,s.ServiceOrderCreateUnit
	,s.ServiceOrderCreateEmployee
	,s.ServiceOrderCreateDate
--	,s.ServiceOrderCreateTime
	,s.FirstAvailableDate
	,s.ServiceOrigScheduleDate
--	,s.ServiceOrigScheduleFromTime
--	,s.ServiceOrigScheduleToTime
	,s.ServiceScheduleDate
	,s.ServiceScheduleFromTime
	,s.ServiceScheduleToTime
	,s.ServiceOrderStatusCode
	,s.ServiceOrderStatusDate
--	,s.CompleteCode
--	,s.SubAccountCode
--	,s.ServiceCallType
--	,s.PrimaryJobCode
--	,s.JobCodeChargeCode
--	,s.JobCodeDescription
--	,s.ChargeUnit
--	,s.CancelCode
	,s.PriorityCode
--	,s.StoreStockNumber
--	,s.CreateAccountingYear
--	,s.CreateAccountingQuater
--	,s.CreateAccountingMonth
--	,s.CreateAccountingWeek
--	,s.CreateDayOfWeek
--	,s.CreateAccountingYearWeek
--	,s.StatusAccountingYear
--	,s.StatusAccountingQuater
--	,s.StatusAccountingMonth
--	,s.StatusAccountingWeek
--	,s.StatusDayofWeek
--	,s.StatusAccountingYearWeek
	,s.ResponseCalendar
	,s.ResponseTime
	,s.StateOfServiceCalendar
	,s.StateOfService
	,s.DaysOld
	,s.AttemptCount
	,s.NotHomeCount
	,s.PUSRCount
	,s.OrderPartCount
	,s.OtherCount
	,s.CancelCount
	,s.TransitTime
	,s.ServiceAttemptTotalTime
--	,s.PartGrossAmountCC
--	,s.LaborGrossAmountCC
--	,s.TotalGrossAmountCC
--	,s.CollectAmount
	,s.PartsRequired
	,s.PartsResearch
	,s.PartsCount
	,s.PartsAmount
	,s.PartsOrderedCount
	,s.PartsShippedCount
	,s.PartsUsedCount
	,s.PartsInstalledCount
	,s.PartsInstalledAmount
	,s.PartsUninstalledCount
	,s.PartsUninstalledAmount
	,s.PartsBackorderedCount
--	,s.TSSL
--	,s.PPOPartsCount
	,s.FirstPartOrderDate
	,s.LastPartOrderDate
	,s.RescheduleCount
	,s.FirstRescheduleDate
	,s.LastRescheduleDate
	,s.FirstAttemptDate
	,s.LastAttemptDate
	,s.LastAttemptTechID
	,s.LastAttemptTechComment1
	,s.LastAttemptTechComment2
	,s.Servicer
--	,s.Reserve
--	,s.MustWin
--	,s.TimeWindow
--	,s.TimeWindowComp
	,s.MSOInboundCallCount
	,s.MSOInboundCallsTotalMinutes
	,s.STACCallCount
	,s.STACCallsTotalMinutes
--	,s.AttemptDefectTotal
--	,s.RescheduleDefectTotal
--	,s.AgeDefectTotal
--	,s.PartsDefectTotal
--	,s.MSODefectTotal
--	,s.DefectTotal
--	,s.NextAttemptIncompleteRisk
--	,s.PredictedNPSRepairScore
--	,s.ActualNPSRepairScore
--	,s.Latitude
--	,s.Longitude
--	,s.NPSSurveyCustomerRequestCallback
--	,s.SaleAmount
--	,s.WhereBoughtUnitType
--	,s.RoutingStatus
--	,s.RoutingStatusDateTime
--	,s.RealtimeFeedbackCount
--	,s.RealtimeFeedbackDateTime
--	,s.RoutingStatusCode
	,s.FirstRescheduleCode
	,s.LastRescheduleCode
	,s.FirstAttemptCode
	,s.LastAttemptCode
--	,s.AssignedTechnician
--	,s.RouteSummaryStatusCode
--	,s.RouteSummaryStatusDateTime
--	,s.EventLocation
--	,s.EventLatitude
--	,s.EventLongitude
--	,s.ProductSalesRequest
--	,s.ProductSalesRequestDateTime
--	,s.RecallDays
--	,s.BadAddressFlag
--	,s.DoNotCallSHCFlag
--	,s.LastUPSExceptionDate
--	,s.HomeWarrantyLeadCoverageCode
--	,s.GeneralPartsWarrantyExpirationDate
--	,s.CurrentServiceProductExpirationDate
--	,s.ScheduledDeliveryDate
--	,s.RescheduledDeliveryDate
FROM 
	ServiceOrder.dbo.TBL_ServiceOrder s WITH (NOLOCK)
WHERE
	s.ServiceOrderStatusCode = 'WP'
	AND s.Region IN ('0000820', '0000890')


GO
/****** Object:  View [dbo].[VIEW_SOAP_WPOrders]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[VIEW_SOAP_WPOrders]
AS  

SELECT
	d.PartStatus AS WP_NPSStatus
	,d.Region AS WP_Region
	,d.ServiceUnit AS WP_District
	,d.ServiceUnit AS WP_Unit
	,d.ServiceOrder AS WP_ServiceOrder
    ,d.IndustryCode AS WP_Industry
    ,d.PartType AS WP_Location
    ,d.NPNOrderStatus AS WP_NPNStatus
    ,d.NPNOrderLine AS WP_POLine
    ,RIGHT(d.PartDivision, 2) AS WP_Division
    ,d.PartListSource AS WP_PLS
    ,d.Part AS WP_PartNumber
    ,d.PartDescription AS WP_Description
    ,d.PartQuantityOrdered AS WP_Qty
    ,d.NPNOrderStatusDate AS WP_DateStatus
    ,d.PartOrderDate AS WP_DateOrder
    ,d.ShipDate AS WP_DateShip
    ,d.ServiceOrderCreateDate AS WP_DateCreate
    ,d.NPNOrder AS WP_PurchaseOrder
    ,d.ServicingOrganizationCode AS WP_ServiceOrg
    ,d.ServiceScheduleDate AS WP_DatePromise
    ,d.CustomerZip AS WP_ZipCode
    ,'Z' AS WP_TimeZone
    ,d.NPNLocation AS WP_Vendor5
    ,CAST(GETDATE() AS DATE) AS WP_ReportDate
FROM
	(	SELECT 
			s.Region
			,s.ServiceUnit
			,s.ServiceOrder
			,s.CustomerID
			,s.LocationSuffix
			,s.ItemSuffix
			,s.ServicingOrganization
			,s.ServicingOrganizationCode
			,s.PaymentMethod
			,s.ClientName
			,s.CombinedThirdPartyID
			,s.IndustryCode
			,s.MerchandiseSpecialtyCode
			,s.ManufacturerBrandName
			,s.StoreStock
			,s.CustomerZip
			,s.ServiceOrderCreateDate
			,s.ServiceScheduleDate
			,s.ServiceOrderStatusCode
			,s.ServiceOrderStatusDate
			,s.CreateAccountingYearWeek
			,s.DaysOld
			,s.PartsResearch
			,s.PartsCount
			,s.PartsAmount
			,s.PartsOrderedCount
			,s.PartsShippedCount
			,s.PartsUsedCount
			,s.PartsInstalledCount
			,s.PartsInstalledAmount
			,s.PartsUninstalledCount
			,s.PartsUninstalledAmount
			,s.PartsBackorderedCount
			,s.FirstPartOrderDate
			,s.LastPartOrderDate
			,q.NPN_Parts
			,q.BO_Parts
			,q.BH_Parts
			,q.IP_Parts
			,q.OR_Parts
			,q.NA_Parts
			,q.SC_Parts
			,q.QIP_Parts
			,q.QOR_Parts
			,p.PartSequence
			,p.PartDivision
			,p.PartListSource
			,p.Part
			,p.PartDescription
			,p.PartType
			,p.PartQuantityOrdered
			,p.PartQuantity
			,p.Coverage
			,p.PartPriceEach
			,p.PartActualSellPrice
			,p.PartStatus
			,p.PartOrderDate
			,p.PartStatusDate
			,p.LastActionTimestamp
			,p.NPNOrder
			,p.NPNOrderDate
			,p.NPNOrderLine
			,p.NPNOrderStatus
			,p.NPNOrderStatusDate
			,p.NPNLocation
			,p.ShipDate
			,p.TrackingNumber
			,p.CarrierCode
		FROM 
			ServiceOrder.dbo.TBL_ServiceOrder s WITH (NOLOCK)
		LEFT JOIN
			(	SELECT
					p.ServiceUnit
					,p.ServiceOrder
					,p.EmployeeIdentification
					,p.PartSequence
					,p.PartDivision
					,p.PartListSource
					,p.Part
					,p.PartDescription
					,p.PartType
					,p.PartQuantityOrdered
					,p.PartQuantity
					,p.PartQuantityUsed
					,p.Coverage
					,p.PartPriceEach
					,p.PartActualSellPrice
					,p.PartStatus
					,p.PartOrderDate
					,p.PartStatusDate
					,p.PartInstalledDate
					,p.LastActionTimestamp
					,n.NPNOrder
					,n.NPNOrderDate
					,n.NPNOrderLine
					,n.NPNOrderStatus
					,n.NPNOrderStatusDate
					,n.NPNLocation
					,n.ShipDate
					,n.TrackingNumber
					,n.CarrierCode
				FROM 
					ServiceOrder.dbo.TBL_NPSServiceOrderParts p WITH (NOLOCK)
				LEFT JOIN
					ServiceOrder.dbo.TBL_NPNServiceOrderParts n WITH (NOLOCK)
					ON p.ServiceUnit = n.ServiceUnit
					AND p.ServiceOrder = n.ServiceOrder
					AND p.PartSequence = n.NPSPartSequence
				WHERE
					NOT p.PartStatus IN ('H', 'I', 'U')
			) p
			ON s.ServiceUnit = p.ServiceUnit
			AND s.ServiceOrder = p.ServiceOrder
		LEFT JOIN
			(	SELECT
					p.ServiceUnit
					,p.ServiceOrder
					,SUM(CASE WHEN n.NPNOrderStatus IN ('BO', 'FB', 'IT') THEN 1 ELSE 0 END) AS BO_Parts
					,SUM(CASE WHEN n.NPNOrderStatus IN ('BH') THEN 1 ELSE 0 END) AS BH_Parts
					,SUM(CASE WHEN n.NPNOrderStatus IN ('IP') THEN 1 ELSE 0 END) AS IP_Parts
					,SUM(CASE WHEN n.NPNOrderStatus IN ('FO', 'OR') THEN 1 ELSE 0 END) AS OR_Parts
					,SUM(CASE WHEN n.NPNOrderStatus IN ('NA') THEN 1 ELSE 0 END) AS NA_Parts
					,SUM(CASE WHEN n.NPNOrderStatus IN ('CA') THEN 1 ELSE 0 END) AS CA_Parts
					,SUM(CASE WHEN n.NPNOrderStatus IN ('SC', 'SU') THEN 1 ELSE 0 END) AS SC_Parts
					,SUM(CASE WHEN n.NPNOrderStatus IN ('IP') AND n.NPNOrderStatusDate < CAST(GETDATE() - 4 AS DATE) THEN 1 ELSE 0 END) AS QIP_Parts
					,SUM(CASE WHEN n.NPNOrderStatus IN ('FO', 'OR') AND n.NPNOrderStatusDate < CAST(GETDATE() - 4 AS DATE) THEN 1 ELSE 0 END) AS QOR_Parts
					,COUNT(*) AS NPN_Parts
				FROM 
					ServiceOrder.dbo.TBL_NPSServiceOrderParts p WITH (NOLOCK)
				LEFT JOIN
					ServiceOrder.dbo.TBL_NPNServiceOrderParts n WITH (NOLOCK)
					ON p.ServiceUnit = n.ServiceUnit
					AND p.ServiceOrder = n.ServiceOrder
					AND p.PartSequence = n.NPSPartSequence
				WHERE
					NOT p.PartStatus IN ('H', 'I', 'U')
				GROUP BY
					p.ServiceUnit
					,p.ServiceOrder
			) q
			ON s.ServiceUnit = q.ServiceUnit
			AND s.ServiceOrder = q.ServiceOrder
		WHERE
			s.ServiceOrderStatusCode = 'WP'
			AND s.ServiceOrderType = 'SITE'
			AND s.Servicer = 'W2'
		--	AND p.PartType = 'E'
		--	AND p.PartOrderDate < '2019-06-25'
		--	AND p.NPNOrderStatus IN ('BO', 'FB')
	) d
WHERE
	d.Part IS NOT NULL











GO
/****** Object:  View [dbo].[VIEW_STACCallTakerOccurrences]    Script Date: 9/18/2019 6:58:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[VIEW_STACCallTakerOccurrences]
AS

SELECT
	ReferenceNumber
	,ServiceUnit
	,ServiceOrder
	,TechID
	,TechName
	,UserID
	,UserName
	,OpenDateTime
	,Abandoned
	,CallDuration
	,Symptom
	,Cure
	,CallCode
	,LastChangeDatetime
FROM
	ServiceOrder.dbo.TBL_STACCallTakerOccurrences WITH (NOLOCK)





GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_LookupCodes_OwnerType_OwnerID]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE CLUSTERED INDEX [IX_TBL_LookupCodes_OwnerType_OwnerID] ON [dbo].[TBL_LookupCodes]
(
	[OwnerType] ASC,
	[OwnerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IDX_TBL_CiboodleCustomerSession_ServiceUnit_ServiceOrder]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IDX_TBL_CiboodleCustomerSession_ServiceUnit_ServiceOrder] ON [dbo].[TBL_CiboodleCustomerSession]
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC
)
INCLUDE ( 	[SessionLoadDateTime]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_Comments_OwnerType]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_Comments_OwnerType] ON [dbo].[TBL_Comments]
(
	[OwnerSubID] ASC,
	[OwnerType] ASC
)
INCLUDE ( 	[CommentID],
	[ServiceUnit],
	[ServiceOrder],
	[Comment],
	[CreatedEnterpriseID],
	[CreatedDateTime],
	[ModifiedEnterpriseID],
	[ModifiedDateTime]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_Comments_SO_SU]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_Comments_SO_SU] ON [dbo].[TBL_Comments]
(
	[ServiceOrder] ASC,
	[ServiceUnit] ASC
)
INCLUDE ( 	[CommentID],
	[OwnerType],
	[OwnerSubID],
	[Comment],
	[CreatedEnterpriseID],
	[CreatedDateTime],
	[ModifiedEnterpriseID],
	[ModifiedDateTime]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_Notifications_SourceID_SourceType]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_Notifications_SourceID_SourceType] ON [dbo].[TBL_Notifications]
(
	[SourceID] ASC,
	[SourceType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IDX_TBL_NPJCustomerEmail_CustomerID]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IDX_TBL_NPJCustomerEmail_CustomerID] ON [dbo].[TBL_NPJCustomerEmail]
(
	[CustomerID] ASC
)
INCLUDE ( 	[CustomerEmailAddress],
	[CurrentEmail]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_NPJCustomerNameAddress_AltPhone]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_NPJCustomerNameAddress_AltPhone] ON [dbo].[TBL_NPJCustomerNameAddress]
(
	[CustomerAlternatePhone] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_NPJCustomerNameAddress_CustomerPhone]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_NPJCustomerNameAddress_CustomerPhone] ON [dbo].[TBL_NPJCustomerNameAddress]
(
	[CustomerPhone] ASC,
	[CustomerAlternatePhone] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IDX_TBL_NPNServiceOrderParts_ServiceUnit_ServiceOrder]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IDX_TBL_NPNServiceOrderParts_ServiceUnit_ServiceOrder] ON [dbo].[TBL_NPNServiceOrderParts]
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IDX_TBL_NPNServiceOrderParts_UnitServiceOrderPartSequence]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IDX_TBL_NPNServiceOrderParts_UnitServiceOrderPartSequence] ON [dbo].[TBL_NPNServiceOrderParts]
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC,
	[NPSPartSequence] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TBLCustomerServiceableMerchandise_CommitTimestamp]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBLCustomerServiceableMerchandise_CommitTimestamp] ON [dbo].[TBL_NPSCustomerServicableMerchandise]
(
	[CommitTimestamp] ASC
)
INCLUDE ( 	[Customer],
	[ItemSuffix],
	[GeneralPartsWarrantyExpirationDate],
	[CurrentServiceProductExpirationDate]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IDX_TBL_ServiceOrderAdditionalRemarks_ServiceUnit_ServiceOrder]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IDX_TBL_ServiceOrderAdditionalRemarks_ServiceUnit_ServiceOrder] ON [dbo].[TBL_NPSServiceOrderAdditionalRemarks]
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC
)
INCLUDE ( 	[LastModifiedDateTime]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_NPSServiceOrderCustomerLookaside_Customer]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_NPSServiceOrderCustomerLookaside_Customer] ON [dbo].[TBL_NPSServiceOrderCustomerLookaside]
(
	[Customer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_NPSServiceOrderCustomerLookaside]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_NPSServiceOrderCustomerLookaside] ON [dbo].[TBL_NPSServiceOrderCustomerLookaside]
(
	[CustomerPhone] ASC,
	[CustomerAlternatePhone] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_NPSServiceOrderCustomerLookaside_AltPhone]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_NPSServiceOrderCustomerLookaside_AltPhone] ON [dbo].[TBL_NPSServiceOrderCustomerLookaside]
(
	[CustomerAlternatePhone] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_NPSServiceOrderCustomerLookaside_CustomerZip]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_NPSServiceOrderCustomerLookaside_CustomerZip] ON [dbo].[TBL_NPSServiceOrderCustomerLookaside]
(
	[CustomerZip] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_MissingIndex_serviceunitservicecalldate]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_MissingIndex_serviceunitservicecalldate] ON [dbo].[TBL_NPSServiceOrderPaymentMethod]
(
	[ServiceUnit] ASC,
	[ServiceCallDate] ASC
)
INCLUDE ( 	[ServiceOrder],
	[PaymentMethod],
	[CustomerChargeAccountNumber],
	[CustomerChargeAccountExpirationDate],
	[CreditApprovalNumber],
	[CollectedAmount],
	[PurchaseOrderNumber],
	[CardAccountTakenNumber],
	[CommitTimestamp]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IDX_TBL_NPSServiceOrderRecallsServiceUnitServiceOrder]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IDX_TBL_NPSServiceOrderRecallsServiceUnitServiceOrder] ON [dbo].[TBL_NPSServiceOrderRecalls]
(
	[ChildServiceUnit] ASC,
	[ChildServiceOrder] ASC
)
INCLUDE ( 	[RecallDays],
	[CustomerRecall],
	[UnitRecall]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_NPSServiceOrderServiceAttempt_TechID_Date_Unit]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_NPSServiceOrderServiceAttempt_TechID_Date_Unit] ON [dbo].[TBL_NPSServiceOrderServiceAttempt]
(
	[ServiceTechEmployee] ASC,
	[ServiceCallDate] ASC,
	[ServiceUnit] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_RTF_ServiceUnitServiceOrder]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_RTF_ServiceUnitServiceOrder] ON [dbo].[TBL_RealTimeFeedBack]
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TBL_RealTimeFeedBack_CustomerID]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_RealTimeFeedBack_CustomerID] ON [dbo].[TBL_RealTimeFeedBack]
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_Reminders_ServiceUnitServiceOrder]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_Reminders_ServiceUnitServiceOrder] ON [dbo].[TBL_Reminders]
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_Schedule_OwnerID_OwnerType_Frequency_Status]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_Schedule_OwnerID_OwnerType_Frequency_Status] ON [dbo].[TBL_Schedule]
(
	[OwnerID] ASC,
	[OwnerType] ASC,
	[Frequency] ASC,
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UIX_TBL_Shedule_OwnerType_OwnerID_Frequency]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UIX_TBL_Shedule_OwnerType_OwnerID_Frequency] ON [dbo].[TBL_Schedule]
(
	[OwnerType] ASC,
	[OwnerID] ASC,
	[Frequency] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IDX_TBL_ServiceOrder_CreateDate]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IDX_TBL_ServiceOrder_CreateDate] ON [dbo].[TBL_ServiceOrder]
(
	[ServiceOrderCreateDate] ASC
)
INCLUDE ( 	[CreateAccountingYear],
	[CreateAccountingQuater],
	[CreateAccountingMonth],
	[CreateAccountingWeek],
	[CreateAccountingYearWeek],
	[CreateDayOfWeek]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IDX_TBL_ServiceOrder_CustomerID]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IDX_TBL_ServiceOrder_CustomerID] ON [dbo].[TBL_ServiceOrder]
(
	[CustomerID] ASC
)
INCLUDE ( 	[ServiceUnit],
	[ServiceOrder],
	[ServiceOrderStatusCode],
	[ServiceScheduleDate]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IDX_TBL_ServiceOrder_CustomerIDItemSuffix]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IDX_TBL_ServiceOrder_CustomerIDItemSuffix] ON [dbo].[TBL_ServiceOrder]
(
	[CustomerID] ASC,
	[ItemSuffix] ASC
)
INCLUDE ( 	[ServiceOrderStatusCode],
	[ServiceOrderStatusDate]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IDX_TBL_ServiceOrder_LastAttemptDate]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IDX_TBL_ServiceOrder_LastAttemptDate] ON [dbo].[TBL_ServiceOrder]
(
	[LastAttemptDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IDX_TBL_ServiceOrder_OrigScheduleDate]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IDX_TBL_ServiceOrder_OrigScheduleDate] ON [dbo].[TBL_ServiceOrder]
(
	[ServiceOrigScheduleDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IDX_TBL_ServiceOrder_ProductSalesRequestDateTime]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IDX_TBL_ServiceOrder_ProductSalesRequestDateTime] ON [dbo].[TBL_ServiceOrder]
(
	[ProductSalesRequestDateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IDX_TBL_ServiceOrder_RealtimeFeedbackDateTime]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IDX_TBL_ServiceOrder_RealtimeFeedbackDateTime] ON [dbo].[TBL_ServiceOrder]
(
	[RealtimeFeedbackDateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IDX_TBL_ServiceOrder_ScheduleDate]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IDX_TBL_ServiceOrder_ScheduleDate] ON [dbo].[TBL_ServiceOrder]
(
	[ServiceScheduleDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IDX_TBL_ServiceOrder_StatusDate]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IDX_TBL_ServiceOrder_StatusDate] ON [dbo].[TBL_ServiceOrder]
(
	[ServiceOrderStatusDate] ASC
)
INCLUDE ( 	[StatusAccountingYear],
	[StatusAccountingQuater],
	[StatusAccountingMonth],
	[StatusAccountingWeek],
	[StatusAccountingYearWeek],
	[StatusDayofWeek]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_MissingIndex_ServiceUnitNPSSurveyCustomerRequestCallbackLastAttemptDate_20170426]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_MissingIndex_ServiceUnitNPSSurveyCustomerRequestCallbackLastAttemptDate_20170426] ON [dbo].[TBL_ServiceOrder]
(
	[ServiceUnit] ASC,
	[NPSSurveyCustomerRequestCallback] ASC,
	[LastAttemptDate] ASC
)
INCLUDE ( 	[ServiceOrder]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_ServiceOrderAssignment_EnterpriseID]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_ServiceOrderAssignment_EnterpriseID] ON [dbo].[TBL_ServiceOrderAssignment]
(
	[EnterpriseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_ServiceORderAssignment_QueueID_ReportID_GroupID_EnterpriseID]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_TBL_ServiceORderAssignment_QueueID_ReportID_GroupID_EnterpriseID] ON [dbo].[TBL_ServiceOrderAssignment]
(
	[QueueID] ASC,
	[ReportID] ASC,
	[GroupID] ASC,
	[EnterpriseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_ServiceOrderAssignment_ReportID]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_ServiceOrderAssignment_ReportID] ON [dbo].[TBL_ServiceOrderAssignment]
(
	[ReportID] ASC
)
INCLUDE ( 	[QueueID],
	[EnterpriseID],
	[AssignmentID],
	[CreatedEnterpriseID],
	[CreatedDateTime],
	[ModifiedEnterpriseID],
	[ModifiedDateTime]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_ServiceOrderPartsRecovery_ActionCode]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_ServiceOrderPartsRecovery_ActionCode] ON [dbo].[TBL_ServiceOrderPartsRecovery]
(
	[ActionCode] ASC
)
INCLUDE ( 	[ActionUserDate],
	[PartSequence],
	[ServiceOrder],
	[ServiceUnit]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_ServiceOrderPartsRecoveryActions_ActionCode_ActionDescription]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_ServiceOrderPartsRecoveryActions_ActionCode_ActionDescription] ON [dbo].[TBL_ServiceOrderPartsRecoveryActions]
(
	[ActionCode] ASC,
	[ActionDescription] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IDX_TBL_ServiceOrderQA_ScheduleDate]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IDX_TBL_ServiceOrderQA_ScheduleDate] ON [dbo].[TBL_ServiceOrderQA]
(
	[ServiceScheduleDate] ASC
)
INCLUDE ( 	[ServiceOrigScheduleDate]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IDX_TBL_ServiceOrderQA_StatusDate]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IDX_TBL_ServiceOrderQA_StatusDate] ON [dbo].[TBL_ServiceOrderQA]
(
	[ServiceOrderStatusDate] ASC
)
INCLUDE ( 	[StatusAccountingYear],
	[StatusAccountingQuater],
	[StatusAccountingMonth],
	[StatusAccountingWeek],
	[StatusAccountingYearWeek]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_ServiceOrderQueueOptions_QueueID]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_ServiceOrderQueueOptions_QueueID] ON [dbo].[TBL_ServiceOrderQueueOptions]
(
	[QueueID] ASC
)
INCLUDE ( 	[OptionID],
	[LayerID],
	[OptionName],
	[OptionOrder],
	[Closed],
	[DueDateAdjustment],
	[NextSetID],
	[EnterpriseID],
	[CreatedDateTime],
	[ModifiedDateTime],
	[SetID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TBL_ServiceOrderQueueOptions_Closed_NextSet]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_ServiceOrderQueueOptions_Closed_NextSet] ON [dbo].[TBL_ServiceOrderQueueOptions]
(
	[Closed] ASC,
	[NextSetID] ASC
)
INCLUDE ( 	[QueueID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_MissingIndex_ServiceUnitServiceOrder_20170426]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_MissingIndex_ServiceUnitServiceOrder_20170426] ON [dbo].[TBL_ServiceOrderSurveys]
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TBL_ServiceOrderSurveys_Customer]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_ServiceOrderSurveys_Customer] ON [dbo].[TBL_ServiceOrderSurveys]
(
	[Customer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_ServiceOrderTasks_PriorityValue]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_ServiceOrderTasks_PriorityValue] ON [dbo].[TBL_ServiceOrderTasks]
(
	[PriorityValue] ASC
)
INCLUDE ( 	[TaskID],
	[QueueID],
	[ReportID],
	[GroupID],
	[ServiceUnit],
	[ServiceOrder],
	[DueDate],
	[CreatedDate],
	[CreateEnterpriseID],
	[LogID],
	[ClaimID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_ServiceOrderTasks_QueueID_ReportID_GroupID]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_ServiceOrderTasks_QueueID_ReportID_GroupID] ON [dbo].[TBL_ServiceOrderTasks]
(
	[QueueID] ASC,
	[ReportID] ASC,
	[GroupID] ASC
)
INCLUDE ( 	[TaskID],
	[ServiceUnit],
	[ServiceOrder],
	[CreatedDate],
	[UpdatedDate],
	[DueDate],
	[AssociationID],
	[PriorityValue],
	[CreateEnterpriseID],
	[UpdateEnterpriseID],
	[ClaimedEnterpriseID],
	[LogID],
	[ClaimID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_ServiceOrderTasks_ReportID_QueueID_GroupID_PV]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_ServiceOrderTasks_ReportID_QueueID_GroupID_PV] ON [dbo].[TBL_ServiceOrderTasks]
(
	[ReportID] ASC,
	[QueueID] ASC,
	[GroupID] ASC,
	[PriorityValue] ASC
)
INCLUDE ( 	[TaskID],
	[ServiceUnit],
	[ServiceOrder],
	[CreatedDate],
	[LogID],
	[ClaimID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_ServiceOrderTasks_ServiceUnit_ServiceOrder]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_ServiceOrderTasks_ServiceUnit_ServiceOrder] ON [dbo].[TBL_ServiceOrderTasks]
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UIX_TBL_ServiceOrderTasksClaimed_LogID_ModDate]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UIX_TBL_ServiceOrderTasksClaimed_LogID_ModDate] ON [dbo].[TBL_ServiceOrderTasksClaimed]
(
	[LogID] ASC,
	[ModifiedDateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TBL_ServiceOrderTasksLog_OptionID_CreateDate]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_ServiceOrderTasksLog_OptionID_CreateDate] ON [dbo].[TBL_ServiceOrderTasksLog]
(
	[OptionID] ASC,
	[CreateDateTime] ASC
)
INCLUDE ( 	[LogID],
	[TaskID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_ServiceOrderTasksLog_TaskID]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_ServiceOrderTasksLog_TaskID] ON [dbo].[TBL_ServiceOrderTasksLog]
(
	[TaskID] ASC
)
INCLUDE ( 	[LogID],
	[OptionID],
	[EnterpriseID],
	[ModifiedDateTime]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_SSTOrderUploadOrderNumberUnitNumber]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_SSTOrderUploadOrderNumberUnitNumber] ON [dbo].[TBL_SSTOrderUpload]
(
	[order_number] ASC,
	[unit_number] ASC
)
INCLUDE ( 	[order_upload_id],
	[route_date]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TBL_SSTOrderUpload_CustomerNumber]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_SSTOrderUpload_CustomerNumber] ON [dbo].[TBL_SSTOrderUpload]
(
	[CustomerNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_SSTOrderUpload_NPSID_UnitNumber_RouteDate]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_SSTOrderUpload_NPSID_UnitNumber_RouteDate] ON [dbo].[TBL_SSTOrderUpload]
(
	[nps_id] ASC,
	[unit_number] ASC,
	[route_date] ASC
)
INCLUDE ( 	[order_upload_id],
	[user_id],
	[order_number],
	[CallCode],
	[PrintTime]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IDX_TBL_STACCallTakerOccurrences_ServiceUnit_ServiceOrder]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IDX_TBL_STACCallTakerOccurrences_ServiceUnit_ServiceOrder] ON [dbo].[TBL_STACCallTakerOccurrences]
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_SupplementaryValues_OwnerID_OwnerType]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_SupplementaryValues_OwnerID_OwnerType] ON [dbo].[TBL_SupplementaryValues]
(
	[OwnerID] ASC,
	[OwnerType] ASC
)
INCLUDE ( 	[SupValue]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_Tech_Route_TR_ServiceOrderTR_UnitNumber]    Script Date: 9/18/2019 6:58:26 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_Tech_Route_TR_ServiceOrderTR_UnitNumber] ON [dbo].[TBL_Tech_Route]
(
	[TR_ServiceOrder] ASC,
	[TR_UnitNumber] ASC
)
INCLUDE ( 	[TR_RouteDate],
	[TR_TechID],
	[TR_Status],
	[TR_StillOnRoute]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
ALTER TABLE [dbo].[_DELETE_20180601_TBL_ServiceOrderRealTimeFeedBack] ADD  CONSTRAINT [RequestContact_def]  DEFAULT ((0)) FOR [RequestContact]
GO
ALTER TABLE [dbo].[SSTOrderUploadStaging] ADD  CONSTRAINT [DF_SSTOrderUploadStagingGrandTotal]  DEFAULT ((0.0)) FOR [GrandTotal]
GO
ALTER TABLE [dbo].[SSTOrderUploadStaging] ADD  CONSTRAINT [DF_SSTOrderUploadStagingprePaidAmount]  DEFAULT ((0.0)) FOR [prePaidAmount]
GO
ALTER TABLE [dbo].[SSTOrderUploadStaging] ADD  CONSTRAINT [DF_SSTOrderUploadStagingAmountCollected]  DEFAULT ((0.0)) FOR [AmountCollected]
GO
ALTER TABLE [dbo].[SSTOrderUploadStaging] ADD  CONSTRAINT [DF_SSTOrderUploadStagingpartsTaxPercent]  DEFAULT ((0.0)) FOR [partsTaxPercent]
GO
ALTER TABLE [dbo].[SSTOrderUploadStaging] ADD  CONSTRAINT [DF_SSTOrderUploadStagingpartsTax]  DEFAULT ((0.0)) FOR [partsTax]
GO
ALTER TABLE [dbo].[SSTOrderUploadStaging] ADD  CONSTRAINT [DF_SSTOrderUploadStagingpartsNetAmount]  DEFAULT ((0.0)) FOR [partsNetAmount]
GO
ALTER TABLE [dbo].[SSTOrderUploadStaging] ADD  CONSTRAINT [DF_SSTOrderUploadStaginglaborTaxPercent]  DEFAULT ((0.0)) FOR [laborTaxPercent]
GO
ALTER TABLE [dbo].[SSTOrderUploadStaging] ADD  CONSTRAINT [DF_SSTOrderUploadStaginglaborTaxCollected]  DEFAULT ((0.0)) FOR [laborTaxCollected]
GO
ALTER TABLE [dbo].[SSTOrderUploadStaging] ADD  CONSTRAINT [DF_SSTOrderUploadStaginglaborTotal]  DEFAULT ((0.0)) FOR [laborTotal]
GO
ALTER TABLE [dbo].[SSTOrderUploadStaging] ADD  CONSTRAINT [DF_SSTOrderUploadStagingbasicHomeCallCharge]  DEFAULT ((0.0)) FOR [basicHomeCallCharge]
GO
ALTER TABLE [dbo].[SSTOrderUploadStaging] ADD  CONSTRAINT [DF_SSTOrderUploadStagingserviceProductDollars]  DEFAULT ((0.0)) FOR [serviceProductDollars]
GO
ALTER TABLE [dbo].[SSTOrderUploadStaging] ADD  CONSTRAINT [DF_SSTOrderUploadStagingserviceProductTaxDollars]  DEFAULT ((0.0)) FOR [serviceProductTaxDollars]
GO
ALTER TABLE [dbo].[SSTOrderUploadStaging] ADD  CONSTRAINT [DF_SSTOrderUploadStagingspTotal]  DEFAULT ((0.0)) FOR [spTotal]
GO
ALTER TABLE [dbo].[SSTOrderUploadStaging] ADD  CONSTRAINT [DF_SSTOrderUploadStagingcustPayTotal]  DEFAULT ((0.0)) FOR [custPayTotal]
GO
ALTER TABLE [dbo].[SSTOrderUploadStaging] ADD  CONSTRAINT [DF_SSTOrderUploadStagingdeductibleApplied]  DEFAULT ((0.0)) FOR [deductibleApplied]
GO
ALTER TABLE [dbo].[SSTOrderUploadStaging] ADD  CONSTRAINT [DF_SSTOrderUploadStagingpartsAssociateDiscount]  DEFAULT ((0.0)) FOR [partsAssociateDiscount]
GO
ALTER TABLE [dbo].[SSTOrderUploadStaging] ADD  CONSTRAINT [DF_SSTOrderUploadStagingpartsPromotionDiscount]  DEFAULT ((0.0)) FOR [partsPromotionDiscount]
GO
ALTER TABLE [dbo].[SSTOrderUploadStaging] ADD  CONSTRAINT [DF_SSTOrderUploadStagingpartsCouponDiscount]  DEFAULT ((0.0)) FOR [partsCouponDiscount]
GO
ALTER TABLE [dbo].[SSTOrderUploadStaging] ADD  CONSTRAINT [DF_SSTOrderUploadStaginglaborAssociateDiscount]  DEFAULT ((0.0)) FOR [laborAssociateDiscount]
GO
ALTER TABLE [dbo].[SSTOrderUploadStaging] ADD  CONSTRAINT [DF_SSTOrderUploadStaginglaborPromotionDiscount]  DEFAULT ((0.0)) FOR [laborPromotionDiscount]
GO
ALTER TABLE [dbo].[SSTOrderUploadStaging] ADD  CONSTRAINT [DF_SSTOrderUploadStaginglaborCouponDiscount]  DEFAULT ((0.0)) FOR [laborCouponDiscount]
GO
ALTER TABLE [dbo].[SSTOrderUploadStaging] ADD  CONSTRAINT [DF_SSTOrderUploadStagingspAssociateDiscount]  DEFAULT ((0.0)) FOR [spAssociateDiscount]
GO
ALTER TABLE [dbo].[SSTOrderUploadStaging] ADD  CONSTRAINT [DF_SSTOrderUploadStagingprimaryAmountCollected]  DEFAULT ((0.0)) FOR [primaryAmountCollected]
GO
ALTER TABLE [dbo].[SSTOrderUploadStaging] ADD  CONSTRAINT [DF_SSTOrderUploadStagingsecondaryAmountCollected]  DEFAULT ((0.0)) FOR [secondaryAmountCollected]
GO
ALTER TABLE [dbo].[TBL_Notifications] ADD  CONSTRAINT [Const_TBL_Notifications_NotificationID_GUID]  DEFAULT (newid()) FOR [NotificationID]
GO
ALTER TABLE [dbo].[TBL_Notifications] ADD  CONSTRAINT [DF_TBL_Notifications_NotificationType]  DEFAULT ('Notifications') FOR [NotificationType]
GO
ALTER TABLE [dbo].[TBL_ProductSalesRequest] ADD  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[TBL_ProductSalesRequest] ADD  DEFAULT ('SYSTEM') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[TBL_ProductSalesRequest] ADD  CONSTRAINT [DF_TBL_ProductSalesRequest_LoadDateTime]  DEFAULT (getdate()) FOR [LoadDateTime]
GO
ALTER TABLE [dbo].[TBL_RealTimeFeedBack] ADD  CONSTRAINT [RTF_RequestContact_def]  DEFAULT ((0)) FOR [RequestContact]
GO
ALTER TABLE [dbo].[TBL_RealTimeFeedBack] ADD  CONSTRAINT [RTF_InsertDate_def]  DEFAULT (getdate()) FOR [InsertDate]
GO
ALTER TABLE [dbo].[TBL_RealTimeFeedBackAll] ADD  CONSTRAINT [RealTimeFeedbackAll_CreateDate_def]  DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[TBL_RealTimeFeedBackTD] ADD  CONSTRAINT [Constraint_RTF_CreateDateTime]  DEFAULT (getdate()) FOR [CreateDateTime]
GO
ALTER TABLE [dbo].[TBL_RealTimeFeedBackTD] ADD  CONSTRAINT [Constraint_RTF_ModifiedDateTime]  DEFAULT (getdate()) FOR [ModifiedDateTime]
GO
ALTER TABLE [dbo].[TBL_ServiceOrder] ADD  CONSTRAINT [RealtimeFeedbackCount_def]  DEFAULT ((0)) FOR [RealtimeFeedbackCount]
GO
ALTER TABLE [dbo].[TBL_ServiceOrderAssociationPermission] ADD  CONSTRAINT [DF_TBL_Permissions_P_IsGranted]  DEFAULT ((1)) FOR [AP_IsGranted]
GO
ALTER TABLE [dbo].[TBL_ServiceOrderAssociationPermission] ADD  CONSTRAINT [DF_TBL_Permissions_P_LastUpdate]  DEFAULT (getdate()) FOR [AP_LastUpdate]
GO
ALTER TABLE [dbo].[TBL_ServiceOrderProductSalesRequest] ADD  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[TBL_ServiceOrderProductSalesRequest] ADD  DEFAULT ('SYSTEM') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[TBL_ServiceOrderProductSalesRequest] ADD  CONSTRAINT [DF_TBL_ServiceOrderProductSalesRequest_LoadDateTime]  DEFAULT (getdate()) FOR [LoadDateTime]
GO
ALTER TABLE [dbo].[TBL_ServiceOrderPSRCustomerNotes] ADD  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[TBL_ServiceOrderPSRCustomerNotes] ADD  DEFAULT ('SYSTEM') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[TBL_ServiceOrderPSRCustomerNotes] ADD  CONSTRAINT [DF_TBL_ServiceOrderPSRCustomerNotes_LoadDateTime]  DEFAULT (getdate()) FOR [LoadDateTime]
GO
ALTER TABLE [dbo].[TBL_ServiceOrderPSRGoodBetterBest] ADD  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[TBL_ServiceOrderPSRGoodBetterBest] ADD  DEFAULT ('SYSTEM') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[TBL_ServiceOrderPSRGoodBetterBest] ADD  CONSTRAINT [DF_TBL_ServiceOrderPSRGoodBetterBest_LoadDateTime]  DEFAULT (getdate()) FOR [LoadDateTime]
GO
ALTER TABLE [dbo].[TBL_SSTOrderUpload] ADD  CONSTRAINT [DF_SSTOrderUploadGrandTotal]  DEFAULT ((0.0)) FOR [GrandTotal]
GO
ALTER TABLE [dbo].[TBL_SSTOrderUpload] ADD  CONSTRAINT [DF_SSTOrderUploadprePaidAmount]  DEFAULT ((0.0)) FOR [prePaidAmount]
GO
ALTER TABLE [dbo].[TBL_SSTOrderUpload] ADD  CONSTRAINT [DF_SSTOrderUploadAmountCollected]  DEFAULT ((0.0)) FOR [AmountCollected]
GO
ALTER TABLE [dbo].[TBL_SSTOrderUpload] ADD  CONSTRAINT [DF_SSTOrderUploadpartsTaxPercent]  DEFAULT ((0.0)) FOR [partsTaxPercent]
GO
ALTER TABLE [dbo].[TBL_SSTOrderUpload] ADD  CONSTRAINT [DF_SSTOrderUploadpartsTax]  DEFAULT ((0.0)) FOR [partsTax]
GO
ALTER TABLE [dbo].[TBL_SSTOrderUpload] ADD  CONSTRAINT [DF_SSTOrderUploadpartsNetAmount]  DEFAULT ((0.0)) FOR [partsNetAmount]
GO
ALTER TABLE [dbo].[TBL_SSTOrderUpload] ADD  CONSTRAINT [DF_SSTOrderUploadlaborTaxPercent]  DEFAULT ((0.0)) FOR [laborTaxPercent]
GO
ALTER TABLE [dbo].[TBL_SSTOrderUpload] ADD  CONSTRAINT [DF_SSTOrderUploadlaborTaxCollected]  DEFAULT ((0.0)) FOR [laborTaxCollected]
GO
ALTER TABLE [dbo].[TBL_SSTOrderUpload] ADD  CONSTRAINT [DF_SSTOrderUploadlaborTotal]  DEFAULT ((0.0)) FOR [laborTotal]
GO
ALTER TABLE [dbo].[TBL_SSTOrderUpload] ADD  CONSTRAINT [DF_SSTOrderUploadbasicHomeCallCharge]  DEFAULT ((0.0)) FOR [basicHomeCallCharge]
GO
ALTER TABLE [dbo].[TBL_SSTOrderUpload] ADD  CONSTRAINT [DF_SSTOrderUploadserviceProductDollars]  DEFAULT ((0.0)) FOR [serviceProductDollars]
GO
ALTER TABLE [dbo].[TBL_SSTOrderUpload] ADD  CONSTRAINT [DF_SSTOrderUploadserviceProductTaxDollars]  DEFAULT ((0.0)) FOR [serviceProductTaxDollars]
GO
ALTER TABLE [dbo].[TBL_SSTOrderUpload] ADD  CONSTRAINT [DF_SSTOrderUploadspTotal]  DEFAULT ((0.0)) FOR [spTotal]
GO
ALTER TABLE [dbo].[TBL_SSTOrderUpload] ADD  CONSTRAINT [DF_SSTOrderUploadcustPayTotal]  DEFAULT ((0.0)) FOR [custPayTotal]
GO
ALTER TABLE [dbo].[TBL_SSTOrderUpload] ADD  CONSTRAINT [DF_SSTOrderUploaddeductibleApplied]  DEFAULT ((0.0)) FOR [deductibleApplied]
GO
ALTER TABLE [dbo].[TBL_SSTOrderUpload] ADD  CONSTRAINT [DF_SSTOrderUploadpartsAssociateDiscount]  DEFAULT ((0.0)) FOR [partsAssociateDiscount]
GO
ALTER TABLE [dbo].[TBL_SSTOrderUpload] ADD  CONSTRAINT [DF_SSTOrderUploadpartsPromotionDiscount]  DEFAULT ((0.0)) FOR [partsPromotionDiscount]
GO
ALTER TABLE [dbo].[TBL_SSTOrderUpload] ADD  CONSTRAINT [DF_SSTOrderUploadpartsCouponDiscount]  DEFAULT ((0.0)) FOR [partsCouponDiscount]
GO
ALTER TABLE [dbo].[TBL_SSTOrderUpload] ADD  CONSTRAINT [DF_SSTOrderUploadlaborAssociateDiscount]  DEFAULT ((0.0)) FOR [laborAssociateDiscount]
GO
ALTER TABLE [dbo].[TBL_SSTOrderUpload] ADD  CONSTRAINT [DF_SSTOrderUploadlaborPromotionDiscount]  DEFAULT ((0.0)) FOR [laborPromotionDiscount]
GO
ALTER TABLE [dbo].[TBL_SSTOrderUpload] ADD  CONSTRAINT [DF_SSTOrderUploadlaborCouponDiscount]  DEFAULT ((0.0)) FOR [laborCouponDiscount]
GO
ALTER TABLE [dbo].[TBL_SSTOrderUpload] ADD  CONSTRAINT [DF_SSTOrderUploadspAssociateDiscount]  DEFAULT ((0.0)) FOR [spAssociateDiscount]
GO
ALTER TABLE [dbo].[TBL_SSTOrderUpload] ADD  CONSTRAINT [DF_SSTOrderUploadprimaryAmountCollected]  DEFAULT ((0.0)) FOR [primaryAmountCollected]
GO
ALTER TABLE [dbo].[TBL_SSTOrderUpload] ADD  CONSTRAINT [DF_SSTOrderUploadsecondaryAmountCollected]  DEFAULT ((0.0)) FOR [secondaryAmountCollected]
GO
ALTER TABLE [dbo].[TBL_TechHubAfterAttempt] ADD  CONSTRAINT [DF_TBL_TechHubAfterAttempt_CreatedDateTime]  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[TBL_TechHubReceiptNotify] ADD  CONSTRAINT [DF_TBL_TechHubReceiptNotify_CreatedDateTime]  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "TBL_NPSServiceOrderServiceAttempt (dbo)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 267
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VIEW_NPSServiceOrderServiceAttempt'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VIEW_NPSServiceOrderServiceAttempt'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -768
         Left = 0
      End
      Begin Tables = 
         Begin Table = "SO"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 323
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "HIER"
            Begin Extent = 
               Top = 6
               Left = 361
               Bottom = 135
               Right = 534
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VIEW_ServiceOrder'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VIEW_ServiceOrder'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "TBL_ServiceOrderSurveys (dbo)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 295
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VIEW_ServiceOrderSurveys'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VIEW_ServiceOrderSurveys'
GO
USE [master]
GO
ALTER DATABASE [ServiceOrder] SET  READ_WRITE 
GO
