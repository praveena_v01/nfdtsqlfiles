USE [master]
GO
/****** Object:  Database [NFDT]    Script Date: 9/18/2019 7:02:25 PM ******/
CREATE DATABASE [NFDT]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'NFDT', FILENAME = N'D:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\NFDT.mdf' , SIZE = 9988288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 256000KB )
 LOG ON 
( NAME = N'NFDT_log', FILENAME = N'E:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\Data\NFDT_log.ldf' , SIZE = 816KB , MAXSIZE = 2048GB , FILEGROWTH = 256000KB ), 
( NAME = N'NFDT_log2', FILENAME = N'F:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\Data\NFDT_log2.ldf' , SIZE = 512000KB , MAXSIZE = 2048GB , FILEGROWTH = 256000KB )
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [NFDT].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [NFDT] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [NFDT] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [NFDT] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [NFDT] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [NFDT] SET ARITHABORT OFF 
GO
ALTER DATABASE [NFDT] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [NFDT] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [NFDT] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [NFDT] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [NFDT] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [NFDT] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [NFDT] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [NFDT] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [NFDT] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [NFDT] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [NFDT] SET  ENABLE_BROKER 
GO
ALTER DATABASE [NFDT] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [NFDT] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [NFDT] SET TRUSTWORTHY ON 
GO
ALTER DATABASE [NFDT] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [NFDT] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [NFDT] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [NFDT] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [NFDT] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [NFDT] SET  MULTI_USER 
GO
ALTER DATABASE [NFDT] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [NFDT] SET DB_CHAINING OFF 
GO
ALTER DATABASE [NFDT] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [NFDT] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [NFDT]
GO
/****** Object:  User [WFP_CPTY]    Script Date: 9/18/2019 7:02:27 PM ******/
CREATE USER [WFP_CPTY] FOR LOGIN [WFP_CPTY] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [SEARS2\delderk]    Script Date: 9/18/2019 7:02:27 PM ******/
CREATE USER [SEARS2\delderk] FOR LOGIN [SEARS2\delderk] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [os_spt]    Script Date: 9/18/2019 7:02:27 PM ******/
CREATE USER [os_spt] FOR LOGIN [os_spt] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [nfdt_web]    Script Date: 9/18/2019 7:02:27 PM ******/
CREATE USER [nfdt_web] FOR LOGIN [nfdt_web] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [nfdt_dbconnect]    Script Date: 9/18/2019 7:02:28 PM ******/
CREATE USER [nfdt_dbconnect] FOR LOGIN [nfdt_dbconnect] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [KMART\njain3]    Script Date: 9/18/2019 7:02:28 PM ******/
CREATE USER [KMART\njain3] FOR LOGIN [KMART\njain3] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [KMART\HomeServices-HSNFDTTM-local]    Script Date: 9/18/2019 7:02:28 PM ******/
CREATE USER [KMART\HomeServices-HSNFDTTM-local] FOR LOGIN [KMART\HomeServices-HSNFDTTM-local]
GO
/****** Object:  User [KMART\HomeServices-HSNFDADMLocal]    Script Date: 9/18/2019 7:02:28 PM ******/
CREATE USER [KMART\HomeServices-HSNFDADMLocal] FOR LOGIN [KMART\HomeServices-HSNFDADMLocal]
GO
/****** Object:  DatabaseRole [db_execute]    Script Date: 9/18/2019 7:02:29 PM ******/
CREATE ROLE [db_execute]
GO
ALTER ROLE [db_execute] ADD MEMBER [SEARS2\delderk]
GO
ALTER ROLE [db_owner] ADD MEMBER [SEARS2\delderk]
GO
ALTER ROLE [db_datareader] ADD MEMBER [SEARS2\delderk]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [SEARS2\delderk]
GO
ALTER ROLE [db_execute] ADD MEMBER [os_spt]
GO
ALTER ROLE [db_owner] ADD MEMBER [os_spt]
GO
ALTER ROLE [db_datareader] ADD MEMBER [os_spt]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [os_spt]
GO
ALTER ROLE [db_execute] ADD MEMBER [nfdt_web]
GO
ALTER ROLE [db_datareader] ADD MEMBER [nfdt_web]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [nfdt_web]
GO
ALTER ROLE [db_execute] ADD MEMBER [nfdt_dbconnect]
GO
ALTER ROLE [db_datareader] ADD MEMBER [nfdt_dbconnect]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [nfdt_dbconnect]
GO
ALTER ROLE [db_owner] ADD MEMBER [KMART\njain3]
GO
ALTER ROLE [db_datareader] ADD MEMBER [KMART\njain3]
GO
ALTER ROLE [db_execute] ADD MEMBER [KMART\HomeServices-HSNFDTTM-local]
GO
ALTER ROLE [db_datareader] ADD MEMBER [KMART\HomeServices-HSNFDTTM-local]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [KMART\HomeServices-HSNFDTTM-local]
GO
ALTER ROLE [db_execute] ADD MEMBER [KMART\HomeServices-HSNFDADMLocal]
GO
ALTER ROLE [db_owner] ADD MEMBER [KMART\HomeServices-HSNFDADMLocal]
GO
ALTER ROLE [db_datareader] ADD MEMBER [KMART\HomeServices-HSNFDADMLocal]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [KMART\HomeServices-HSNFDADMLocal]
GO
/****** Object:  StoredProcedure [dbo].[PROC_Admin_GenerateDeleteStatement]    Script Date: 9/18/2019 7:02:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[PROC_Admin_GenerateDeleteStatement]
	@DestinationDB varchar(128),
	@DestinationTableSchema varchar(128),
	@DestinationTableName varchar(128),
	@SourceSQLStatement varchar(max),
	@FlipDestinationAndSource bit = 0,
	@DeleteStatement varchar(max) OUTPUT
WITH RECOMPILE
AS
BEGIN
	SET NOCOUNT ON 
	
	DECLARE @sql varchar(MAX)
	DECLARE @SourceInsertColumns varchar(MAX)
	DECLARE @DestInsertColumns varchar(MAX)
	DECLARE @UpdateClause varchar(MAX)
	DECLARE @ColumnName varchar(128)
	DECLARE @JoinColumns varchar(MAX)
	DECLARE @IsPrimaryKey int
	DECLARE @IsComputed int 
	DECLARE @DataType varchar(128)
	DECLARE @WhereClause varchar(max)
	
	set @SourceInsertColumns = ''
	set @DestInsertColumns = ''
	set @UpdateClause = ''
	set @ColumnName = ''
	set @IsPrimaryKey = 0
	set @IsComputed = 0
	set @JoinColumns = ''
	set @DataType = ''

	DECLARE @TableColumnData TABLE
		(
		 Column_Name nvarchar(128),
		 IsIdentity bit,
		 IsPrimaryKey bit,
		 IsComputed bit,
		 Data_Type nvarchar(128),
		 ordinal_position int
		)

	set @sql = 'USE [' + @DestinationDB + ']'
	set @sql = @sql + '	select '
	set @sql = @sql + '		col.Column_Name, '
	set @sql = @sql + '		isnull(COLUMNPROPERTY(object_id(col.TABLE_NAME), col.Column_Name, ''IsIdentity''),0) as IsIdentity ,'
	set @sql = @sql + '		CASE WHEN pk.IsPK = 1 then 1 ELSE 0 END as IsPrimaryKey,'
	set @sql = @sql + '		isnull(COLUMNPROPERTY(object_id(col.table_schema + ''.'' + col.TABLE_NAME), col.Column_Name, ''IsComputed''),0) as IsComputed , '
	set @sql = @sql + '		col.Data_Type,'
	set @sql = @sql + '		col.ordinal_position'
	set @sql = @sql + '	from '
	set @sql = @sql + '		information_schema.columns col LEFT OUTER JOIN '
	set @sql = @sql + '		('
	set @sql = @sql + '			SELECT '
	set @sql = @sql + '				tc.table_catalog,'
	set @sql = @sql + '				tc.table_schema,'
	set @sql = @sql + '				tc.table_name,'
	set @sql = @sql + '				ccu.Column_Name,'
	set @sql = @sql + '				1 as IsPK'
	set @sql = @sql + '			FROM '
	set @sql = @sql + '				INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc'
	set @sql = @sql + '				JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu ON tc.CONSTRAINT_NAME = ccu.Constraint_name'
	set @sql = @sql + '			WHERE '
	set @sql = @sql + '				tc.CONSTRAINT_TYPE = ''Primary Key'''
	set @sql = @sql + '		) as Pk ON col.Column_Name = pk.Column_Name AND col.TABLE_NAME = pk.table_name AND col.table_schema = pk.table_schema'
	set @sql = @sql + '	where '
	set @sql = @sql + '		col.table_name = ''' + @DestinationTableName + ''' AND col.table_schema=''' + @DestinationTableSchema + ''' order by col.ordinal_position'
	
	INSERT INTO @TableColumnData
	EXECUTE (@sql)
	
	
	IF @@ROWCOUNT = 0
		BEGIN
		RAISERROR ('Table not found or you do not have permission on it',16,1)
		RETURN
	END

	DECLARE @ColNames CURSOR
	SET @ColNames = CURSOR FOR 
		select 
			Column_Name, 
			IsPrimaryKey,
			IsComputed, 
			Data_Type
		from 
			@TableColumnData order by ordinal_position


	OPEN @ColNames
	FETCH NEXT FROM @ColNames INTO @ColumnName, @IsPrimaryKey, @IsComputed, @DataType
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		if @IsComputed = 0 and @DataType <> 'timestamp'
			BEGIN
			set @SourceInsertColumns = @SourceInsertColumns + case when @SourceInsertColumns = '' THEN '' ELSE ',' end + 'S.' + @ColumnName
			set @DestInsertColumns = @DestInsertColumns + case when @DestInsertColumns = '' THEN '' ELSE ',' end + @ColumnName
			
			if @IsPrimaryKey = 1 
				BEGIN
				set @JoinColumns = @JoinColumns + case when @JoinColumns = '' THEN char(9) ELSE 'AND ' end + 'D.' + @ColumnName + ' = ' + 'S.' + @ColumnName + char(10) + char(9)
				IF @WhereClause IS NULL AND @FlipDestinationAndSource = 0
					SET @WhereClause = 'WHERE S.' + @ColumnName + ' IS NULL' + char(10) + char(9)
				IF @WhereClause IS NULL AND @FlipDestinationAndSource = 1
					SET @WhereClause = 'WHERE D.' + @ColumnName + ' IS NULL' + char(10) + char(9)
				END
			ELSE
				set @UpdateClause = @UpdateClause + case when @UpdateClause = '' THEN char(9) ELSE ',' end + @ColumnName + ' = ' + 'S.' + @ColumnName + char(10) + char(9)
		END
		
		FETCH NEXT FROM @ColNames INTO @ColumnName, @IsPrimaryKey, @IsComputed, @DataType
	END
	
	CLOSE @ColNames
	DEALLOCATE @ColNames

	
	IF ltrim(rtrim(@JoinColumns)) = ''
		BEGIN
		RAISERROR ('Table does not have a Primary Key',16,1)
		RETURN
	END
	
	IF ltrim(rtrim(@UpdateClause)) = ''
		BEGIN
		RAISERROR ('Unable to determine columns in table',16,1)
		RETURN
	END
	
	SET @sql = ''
	set @sql = @sql + ' DELETE FROM ' + char(10) + char(9) + @DestinationDB + '.' + @DestinationTableSchema + '.' + @DestinationTableName + char(10)
	set @sql = @sql + ' FROM ' + char(10) + char(9) + '(' + char(10) + char(9) + @SourceSQLStatement + char(10) + char(9) + ') as S LEFT OUTER JOIN ' + @DestinationDB + '.' + @DestinationTableSchema + '.' + @DestinationTableName + ' AS D' + char(10)
	set @sql = @sql + ' ON (' + char(10) + @JoinColumns + ')' + char(10)
	set @sql = @sql + ' ' + @WhereClause
	set @sql = @sql + ';'

	IF @FlipDestinationAndSource = 1
		begin
		SET @sql = ''
		set @sql = @sql + ' DELETE FROM S'
		set @sql = @sql + ' FROM ' + char(10) + char(9) + '(' + char(10) + char(9) + @SourceSQLStatement + char(10) + char(9) + ') as S LEFT OUTER JOIN ' + @DestinationDB + '.' + @DestinationTableSchema + '.' + @DestinationTableName + ' AS D' + char(10)
		set @sql = @sql + ' ON (' + char(10) + @JoinColumns + ')' + char(10)
		set @sql = @sql + ' ' + @WhereClause
		set @sql = @sql + ';'
	END
	
	
	SET @DeleteStatement = @SQL

END






GO
/****** Object:  StoredProcedure [dbo].[PROC_Admin_GenerateInsertStatement]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[PROC_Admin_GenerateInsertStatement]
	@DestinationDB varchar(128),
	@DestinationTableSchema varchar(128),
	@DestinationTableName varchar(128),
	@SourceSQLStatement varchar(max),
	@FlipDestinationAndSource bit = 0,
	@InsertStatement varchar(max) OUTPUT
WITH RECOMPILE
AS
BEGIN
	SET NOCOUNT ON 
	
	DECLARE @sql varchar(MAX)
	DECLARE @SourceInsertColumns varchar(MAX)
	DECLARE @DestInsertColumns varchar(MAX)
	DECLARE @UpdateClause varchar(MAX)
	DECLARE @ColumnName varchar(128)
	DECLARE @JoinColumns varchar(MAX)
	DECLARE @IsPrimaryKey int
	DECLARE @IsComputed int 
	DECLARE @DataType varchar(128)
	DECLARE @WhereClause varchar(max)
	
	set @SourceInsertColumns = ''
	set @DestInsertColumns = ''
	set @UpdateClause = ''
	set @ColumnName = ''
	set @IsPrimaryKey = 0
	set @IsComputed = 0
	set @JoinColumns = ''
	set @DataType = ''

	DECLARE @TableColumnData TABLE
		(
		 Column_Name nvarchar(128),
		 IsIdentity bit,
		 IsPrimaryKey bit,
		 IsComputed bit,
		 Data_Type nvarchar(128),
		 ordinal_position int
		)

	set @sql = 'USE [' + @DestinationDB + ']'
	set @sql = @sql + '	select '
	set @sql = @sql + '		col.Column_Name, '
	set @sql = @sql + '		isnull(COLUMNPROPERTY(object_id(col.TABLE_NAME), col.Column_Name, ''IsIdentity''),0) as IsIdentity ,'
	set @sql = @sql + '		CASE WHEN pk.IsPK = 1 then 1 ELSE 0 END as IsPrimaryKey,'
	set @sql = @sql + '		isnull(COLUMNPROPERTY(object_id(col.table_schema + ''.'' + col.TABLE_NAME), col.Column_Name, ''IsComputed''),0) as IsComputed , '
	set @sql = @sql + '		col.Data_Type,'
	set @sql = @sql + '		col.ordinal_position'
	set @sql = @sql + '	from '
	set @sql = @sql + '		information_schema.columns col LEFT OUTER JOIN '
	set @sql = @sql + '		('
	set @sql = @sql + '			SELECT '
	set @sql = @sql + '				tc.table_catalog,'
	set @sql = @sql + '				tc.table_schema,'
	set @sql = @sql + '				tc.table_name,'
	set @sql = @sql + '				ccu.Column_Name,'
	set @sql = @sql + '				1 as IsPK'
	set @sql = @sql + '			FROM '
	set @sql = @sql + '				INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc'
	set @sql = @sql + '				JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu ON tc.CONSTRAINT_NAME = ccu.Constraint_name'
	set @sql = @sql + '			WHERE '
	set @sql = @sql + '				tc.CONSTRAINT_TYPE = ''Primary Key'''
	set @sql = @sql + '		) as Pk ON col.Column_Name = pk.Column_Name AND col.TABLE_NAME = pk.table_name AND col.table_schema = pk.table_schema'
	set @sql = @sql + '	where '
	set @sql = @sql + '		col.table_name = ''' + @DestinationTableName + ''' AND col.table_schema=''' + @DestinationTableSchema + ''' order by col.ordinal_position'
	
	INSERT INTO @TableColumnData
	EXECUTE (@sql)
	
	
	IF @@ROWCOUNT = 0
		BEGIN
		RAISERROR ('Table not found or you do not have permission on it',16,1)
		RETURN
	END

	DECLARE @ColNames CURSOR
	SET @ColNames = CURSOR FOR 
		select 
			Column_Name, 
			IsPrimaryKey,
			IsComputed, 
			Data_Type
		from 
			@TableColumnData order by ordinal_position


	OPEN @ColNames
	FETCH NEXT FROM @ColNames INTO @ColumnName, @IsPrimaryKey, @IsComputed, @DataType
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		if @IsComputed = 0 and @DataType <> 'timestamp'
			BEGIN
			set @SourceInsertColumns = @SourceInsertColumns + case when @SourceInsertColumns = '' THEN '' ELSE ',' end + 'S.' + @ColumnName
			set @DestInsertColumns = @DestInsertColumns + case when @DestInsertColumns = '' THEN '' ELSE ',' end + 'D.' + @ColumnName
			
			if @IsPrimaryKey = 1 
				BEGIN
				set @JoinColumns = @JoinColumns + case when @JoinColumns = '' THEN char(9) ELSE 'AND ' end + 'D.' + @ColumnName + ' = ' + 'S.' + @ColumnName + char(10) + char(9)
				IF @WhereClause IS NULL AND @FlipDestinationAndSource = 0
					SET @WhereClause = 'WHERE D.' + @ColumnName + ' IS NULL' + char(10) + char(9)
				IF @WhereClause IS NULL AND @FlipDestinationAndSource = 1
					SET @WhereClause = 'WHERE S.' + @ColumnName + ' IS NULL' + char(10) + char(9)
				END
			ELSE
				set @UpdateClause = @UpdateClause + case when @UpdateClause = '' THEN char(9) ELSE ',' end + @ColumnName + ' = ' + 'S.' + @ColumnName + char(10) + char(9)
		END
		
		FETCH NEXT FROM @ColNames INTO @ColumnName, @IsPrimaryKey, @IsComputed, @DataType
	END
	
	CLOSE @ColNames
	DEALLOCATE @ColNames

	
	IF ltrim(rtrim(@JoinColumns)) = ''
		BEGIN
		RAISERROR ('Table does not have a Primary Key',16,1)
		RETURN
	END
	
	IF ltrim(rtrim(@UpdateClause)) = ''
		BEGIN
		RAISERROR ('Unable to determine columns in table',16,1)
		RETURN
	END
	
	SET @sql = ''
	set @sql = @sql + ' INSERT INTO '+ @DestinationDB + '.' + @DestinationTableSchema + '.' + @DestinationTableName +' (' + @DestInsertColumns  + ') ' + char(10)
	set @sql = @sql + ' SELECT ' + @SourceInsertColumns + char(10)
	set @sql = @sql + ' FROM ' + char(10) + char(9) + '(' + char(10) + char(9) + @SourceSQLStatement + char(10) + char(9) + ') as S LEFT OUTER JOIN ' + @DestinationDB + '.' + @DestinationTableSchema + '.' + @DestinationTableName + ' AS D' + char(10)
	set @sql = @sql + ' ON (' + char(10) + @JoinColumns + ')' + char(10)
	set @sql = @sql + ' ' + @WhereClause
	set @sql = @sql + ';'

	IF @FlipDestinationAndSource = 1
		begin
		SET @sql = ''
		set @sql = @sql + ' INSERT INTO ' + char(10) + char(9) + STUFF(@SourceSQLStatement, CHARINDEX ('select * from',@SourceSQLStatement) , len('select * from'), '')  + char(10) + char(9) /* replace because annoying tsql syntax */
		set @sql = @sql + ' SELECT ' + @DestInsertColumns + char(10)
		set @sql = @sql + ' FROM ' + char(10) + char(9) + '(' + char(10) + char(9) + @SourceSQLStatement + char(10) + char(9) + ') as S RIGHT OUTER JOIN ' + @DestinationDB + '.' + @DestinationTableSchema + '.' + @DestinationTableName + ' AS D' + char(10)
		set @sql = @sql + ' ON (' + char(10) + @JoinColumns + ')' + char(10)
		set @sql = @sql + ' ' + @WhereClause
		set @sql = @sql + ';'
	END
	
	
	SET @InsertStatement = @SQL

END









GO
/****** Object:  StoredProcedure [dbo].[PROC_Admin_GenerateMergeStatement]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[PROC_Admin_GenerateMergeStatement]
	@DestinationDB varchar(128),
	@DestinationTableSchema varchar(128),
	@DestinationTableName varchar(128),
	@SourceSQLStatement varchar(max),
	@FlipDestinationAndSource bit = 0,
	@MergeStatement  varchar(max) OUTPUT
WITH RECOMPILE
AS
BEGIN
	SET NOCOUNT ON 
	
	DECLARE @sql varchar(MAX)
	DECLARE @SourceInsertColumns varchar(MAX)
	DECLARE @DestInsertColumns varchar(MAX)
	DECLARE @UpdateClause varchar(MAX)
	DECLARE @ColumnName varchar(128)
	DECLARE @JoinColumns varchar(MAX)
	DECLARE @IsPrimaryKey int
	DECLARE @IsComputed int 
	DECLARE @DataType varchar(128)
	
	set @SourceInsertColumns = ''
	set @DestInsertColumns = ''
	set @UpdateClause = ''
	set @ColumnName = ''
	set @IsPrimaryKey = 0
	set @IsComputed = 0
	set @JoinColumns = ''
	set @DataType = ''

	DECLARE @TableColumnData TABLE
		(
		 Column_Name nvarchar(128),
		 IsIdentity bit,
		 IsPrimaryKey bit,
		 IsComputed bit,
		 Data_Type nvarchar(128),
		 ordinal_position int
		)

	set @sql = 'USE [' + @DestinationDB + ']'
	set @sql = @sql + '	select '
	set @sql = @sql + '		col.Column_Name, '
	set @sql = @sql + '		isnull(COLUMNPROPERTY(object_id(col.TABLE_NAME), col.Column_Name, ''IsIdentity''),0) as IsIdentity ,'
	set @sql = @sql + '		CASE WHEN pk.IsPK = 1 then 1 ELSE 0 END as IsPrimaryKey,'
	set @sql = @sql + '		isnull(COLUMNPROPERTY(object_id(col.table_schema + ''.'' + col.TABLE_NAME), col.Column_Name, ''IsComputed''),0) as IsComputed , '
	set @sql = @sql + '		col.Data_Type,'
	set @sql = @sql + '		col.ordinal_position'
	set @sql = @sql + '	from '
	set @sql = @sql + '		information_schema.columns col LEFT OUTER JOIN '
	set @sql = @sql + '		('
	set @sql = @sql + '			SELECT '
	set @sql = @sql + '				tc.table_catalog,'
	set @sql = @sql + '				tc.table_schema,'
	set @sql = @sql + '				tc.table_name,'
	set @sql = @sql + '				ccu.Column_Name,'
	set @sql = @sql + '				1 as IsPK'
	set @sql = @sql + '			FROM '
	set @sql = @sql + '				INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc'
	set @sql = @sql + '				JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu ON tc.CONSTRAINT_NAME = ccu.Constraint_name'
	set @sql = @sql + '			WHERE '
	set @sql = @sql + '				tc.CONSTRAINT_TYPE = ''Primary Key'''
	set @sql = @sql + '		) as Pk ON col.Column_Name = pk.Column_Name AND col.TABLE_NAME = pk.table_name AND col.table_schema = pk.table_schema'
	set @sql = @sql + '	where '
	set @sql = @sql + '		col.table_name = ''' + @DestinationTableName + ''' AND col.table_schema=''' + @DestinationTableSchema + ''' order by col.ordinal_position'
	
	INSERT INTO @TableColumnData
	EXECUTE (@sql)
	
	
	IF @@ROWCOUNT = 0
		BEGIN
		RAISERROR ('Table not found or you do not have permission on it',16,1)
		RETURN
	END

	DECLARE @ColNames CURSOR
	SET @ColNames = CURSOR FOR 
		select 
			Column_Name, 
			IsPrimaryKey,
			IsComputed, 
			Data_Type
		from 
			@TableColumnData order by ordinal_position


	OPEN @ColNames
	FETCH NEXT FROM @ColNames INTO @ColumnName, @IsPrimaryKey, @IsComputed, @DataType
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		if @IsComputed = 0 and @DataType <> 'timestamp'
			BEGIN
			set @SourceInsertColumns = @SourceInsertColumns + case when @SourceInsertColumns = '' THEN '' ELSE ',' end + 'S.' + @ColumnName
			set @DestInsertColumns = @DestInsertColumns + case when @DestInsertColumns = '' THEN '' ELSE ',' end + @ColumnName
			
			if @IsPrimaryKey = 1 
				set @JoinColumns = @JoinColumns + case when @JoinColumns = '' THEN char(9) ELSE 'AND ' end + 'D.' + @ColumnName + ' = ' + 'S.' + @ColumnName + char(10) + char(9)
			ELSE
				set @UpdateClause = @UpdateClause + case when @UpdateClause = '' THEN char(9) ELSE ',' end + @ColumnName + ' = ' + 'S.' + @ColumnName + char(10) + char(9)
		END
		
		FETCH NEXT FROM @ColNames INTO @ColumnName, @IsPrimaryKey, @IsComputed, @DataType
	END
	
	CLOSE @ColNames
	DEALLOCATE @ColNames

	
	IF ltrim(rtrim(@JoinColumns)) = ''
		BEGIN
		RAISERROR ('Table does not have a Primary Key',16,1)
		RETURN
	END
	
	IF ltrim(rtrim(@UpdateClause)) = ''
		BEGIN
		RAISERROR ('Unable to determine columns in table',16,1)
		RETURN
	END
	
	SET @sql = ''
	set @sql = @sql + ' MERGE ' + char(10) + char(9) + @DestinationDB + '.' + @DestinationTableSchema + '.' + @DestinationTableName + ' AS D' + char(10)
	set @sql = @sql + ' USING ' + char(10) + char(9) + '(' + char(10) + char(9) + @SourceSQLStatement + char(10) + char(9) + ') as S' + char(10)
	set @sql = @sql + ' ON (' + char(10) + @JoinColumns + ')' + char(10)
	set @sql = @sql + ' WHEN NOT MATCHED BY TARGET' + char(10)
	set @sql = @sql + char(9) + ' THEN INSERT(' + @DestInsertColumns + ') ' + char(10)
	set @sql = @sql + char(9) + ' VALUES(' + @SourceInsertColumns + ')' + char(10)
	set @sql = @sql + ' WHEN MATCHED THEN UPDATE SET ' + char(10) + @UpdateClause
	set @sql = @sql + ';'

	IF @FlipDestinationAndSource = 1
		begin
		SET @sql = ''
		set @sql = @sql + ' MERGE ' + char(10) + char(9) + '(' + char(10) + char(9) + @SourceSQLStatement + char(10) + char(9) + ') AS D' + char(10)
		set @sql = @sql + ' USING ' + char(10) + char(9) + @DestinationDB + '.' + @DestinationTableSchema + '.' + @DestinationTableName + ' as S' + char(10)
		set @sql = @sql + ' ON (' + char(10) + @JoinColumns + ')' + char(10)
		set @sql = @sql + ' WHEN NOT MATCHED BY TARGET' + char(10)
		set @sql = @sql + char(9) + ' THEN INSERT(' + @DestInsertColumns + ') ' + char(10)
		set @sql = @sql + char(9) + ' VALUES(' + @SourceInsertColumns + ')' + char(10)
		set @sql = @sql + ' WHEN MATCHED THEN UPDATE SET ' + char(10) + @UpdateClause
		set @sql = @sql + ';'
	END
	
	
	SET @MergeStatement = @SQL

END




GO
/****** Object:  StoredProcedure [dbo].[PROC_Admin_GenerateUpdateStatement]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[PROC_Admin_GenerateUpdateStatement]
	@DestinationDB varchar(128),
	@DestinationTableSchema varchar(128),
	@DestinationTableName varchar(128),
	@SourceSQLStatement varchar(max),
	@FlipDestinationAndSource bit = 0,
	@UpdateStatement varchar(max) OUTPUT
WITH RECOMPILE
AS
BEGIN
	SET NOCOUNT ON 
	
	DECLARE @sql varchar(MAX)
	DECLARE @SourceInsertColumns varchar(MAX)
	DECLARE @DestInsertColumns varchar(MAX)
	DECLARE @UpdateClause varchar(MAX)
	DECLARE @ColumnName varchar(128)
	DECLARE @JoinColumns varchar(MAX)
	DECLARE @IsPrimaryKey int
	DECLARE @IsComputed int 
	DECLARE @DataType varchar(128)
	
	set @SourceInsertColumns = ''
	set @DestInsertColumns = ''
	set @UpdateClause = ''
	set @ColumnName = ''
	set @IsPrimaryKey = 0
	set @IsComputed = 0
	set @JoinColumns = ''
	set @DataType = ''

	DECLARE @TableColumnData TABLE
		(
		 Column_Name nvarchar(128),
		 IsIdentity bit,
		 IsPrimaryKey bit,
		 IsComputed bit,
		 Data_Type nvarchar(128),
		 ordinal_position int
		)

	set @sql = 'USE [' + @DestinationDB + ']'
	set @sql = @sql + '	select '
	set @sql = @sql + '		col.Column_Name, '
	set @sql = @sql + '		isnull(COLUMNPROPERTY(object_id(col.TABLE_NAME), col.Column_Name, ''IsIdentity''),0) as IsIdentity ,'
	set @sql = @sql + '		CASE WHEN pk.IsPK = 1 then 1 ELSE 0 END as IsPrimaryKey,'
	set @sql = @sql + '		isnull(COLUMNPROPERTY(object_id(col.table_schema + ''.'' + col.TABLE_NAME), col.Column_Name, ''IsComputed''),0) as IsComputed , '
	set @sql = @sql + '		col.Data_Type,'
	set @sql = @sql + '		col.ordinal_position'
	set @sql = @sql + '	from '
	set @sql = @sql + '		information_schema.columns col LEFT OUTER JOIN '
	set @sql = @sql + '		('
	set @sql = @sql + '			SELECT '
	set @sql = @sql + '				tc.table_catalog,'
	set @sql = @sql + '				tc.table_schema,'
	set @sql = @sql + '				tc.table_name,'
	set @sql = @sql + '				ccu.Column_Name,'
	set @sql = @sql + '				1 as IsPK'
	set @sql = @sql + '			FROM '
	set @sql = @sql + '				INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc'
	set @sql = @sql + '				JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu ON tc.CONSTRAINT_NAME = ccu.Constraint_name'
	set @sql = @sql + '			WHERE '
	set @sql = @sql + '				tc.CONSTRAINT_TYPE = ''Primary Key'''
	set @sql = @sql + '		) as Pk ON col.Column_Name = pk.Column_Name AND col.TABLE_NAME = pk.table_name AND col.table_schema = pk.table_schema'
	set @sql = @sql + '	where '
	set @sql = @sql + '		col.table_name = ''' + @DestinationTableName + ''' AND col.table_schema=''' + @DestinationTableSchema + ''' order by col.ordinal_position'
	
	INSERT INTO @TableColumnData
	EXECUTE (@sql)
	
	
	IF @@ROWCOUNT = 0
		BEGIN
		RAISERROR ('Table not found or you do not have permission on it',16,1)
		RETURN
	END

	DECLARE @ColNames CURSOR
	SET @ColNames = CURSOR FOR 
		select 
			Column_Name, 
			IsPrimaryKey,
			IsComputed, 
			Data_Type
		from 
			@TableColumnData order by ordinal_position


	OPEN @ColNames
	FETCH NEXT FROM @ColNames INTO @ColumnName, @IsPrimaryKey, @IsComputed, @DataType
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		if @IsComputed = 0 and @DataType <> 'timestamp'
			BEGIN
			set @SourceInsertColumns = @SourceInsertColumns + case when @SourceInsertColumns = '' THEN '' ELSE ',' end + 'S.' + @ColumnName
			set @DestInsertColumns = @DestInsertColumns + case when @DestInsertColumns = '' THEN '' ELSE ',' end + @ColumnName
			
			if @IsPrimaryKey = 1 
				set @JoinColumns = @JoinColumns + case when @JoinColumns = '' THEN char(9) ELSE 'AND ' end + 'D.' + @ColumnName + ' = ' + 'S.' + @ColumnName + char(10) + char(9)
			ELSE
				BEGIN
				IF @FlipDestinationAndSource = 1
					set @UpdateClause = @UpdateClause + case when @UpdateClause = '' THEN char(9) ELSE ',' end + 'S.' + @ColumnName + ' = ' + 'D.' + @ColumnName + char(10) + char(9)
				IF @FlipDestinationAndSource = 0
					set @UpdateClause = @UpdateClause + case when @UpdateClause = '' THEN char(9) ELSE ',' end + 'D.' + @ColumnName + ' = ' + 'S.' + @ColumnName + char(10) + char(9)
			END
		END
		
		FETCH NEXT FROM @ColNames INTO @ColumnName, @IsPrimaryKey, @IsComputed, @DataType
	END
	
	CLOSE @ColNames
	DEALLOCATE @ColNames

	
	IF ltrim(rtrim(@JoinColumns)) = ''
		BEGIN
		RAISERROR ('Table does not have a Primary Key',16,1)
		RETURN
	END
	
	IF ltrim(rtrim(@UpdateClause)) = ''
		BEGIN
		RAISERROR ('Unable to determine columns in table',16,1)
		RETURN
	END
	
	SET @sql = ''
	set @sql = @sql + ' UPDATE D SET ' + char(10)
	set @sql = @sql +  ' ' + @UpdateClause + char(10)
	set @sql = @sql + ' FROM ' + char(10) + char(9) + '(' + char(10) + char(9) + @SourceSQLStatement + char(10) + char(9) + ') as S INNER JOIN ' + @DestinationDB + '.' + @DestinationTableSchema + '.' + @DestinationTableName + ' AS D' + char(10)
	set @sql = @sql + ' ON (' + char(10) + @JoinColumns + ')' + char(10)
	set @sql = @sql + ';'

	IF @FlipDestinationAndSource = 1
		begin
		SET @sql = ''
		set @sql = @sql + ' UPDATE S SET ' + char(10)
		set @sql = @sql +  ' ' + @UpdateClause + char(10)
		set @sql = @sql + ' FROM ' + char(10) + char(9) + '(' + char(10) + char(9) + @SourceSQLStatement + char(10) + char(9) + ') as S INNER JOIN ' + @DestinationDB + '.' + @DestinationTableSchema + '.' + @DestinationTableName + ' AS D' + char(10)
		set @sql = @sql + ' ON (' + char(10) + @JoinColumns + ')' + char(10)
		set @sql = @sql + ';'
	END
	
	
	SET @UpdateStatement = @SQL
END






GO
/****** Object:  StoredProcedure [dbo].[PROC_AnyOrderAsyncQueryExecQueue01_Insert]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*************************************************************************
	Procedure Name: PROC_AnyOrderAsyncQueryExecQueue01_Insert
	Returns:		Failure Code (@ErrorCode)
	Primary User:	Admins
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Thu Apr 25 17:29:30 CDT 2013 @978 /Internet Time/
	Documentation:	
	Information:	Procedure that adds a SQL Statement to run async.
	************************************************************************
	Version:		1.00
	Date:			Thu Apr 25 17:29:30 CDT 2013 @978 /Internet Time/
	Updated By:		Dave Elderkin
***************************************************************************/
CREATE procedure [dbo].[PROC_AnyOrderAsyncQueryExecQueue01_Insert]
	@SQLStatement varchar(max)
	,@Source  varchar(50) 						/* source system identifier for reporting and such */
	,@SourceID int = NULL						/* source ID that would uniquely identify an element in the source */
	,@SourceLogID int = NULL					/* if the source has a log system, this is the ID number entry for that log. */
	,@QueueToken uniqueidentifier = null output	/* the message token returned from the queue.  can be used to look up results. */ 
	,@ErrorCode varchar(150) = null output		/* very basic error checker */
as
begin
	DECLARE @ConversationHandle uniqueidentifier
	DECLARE @TestSQL varchar(max)
	DECLARE @xmlBody xml
	DECLARE @trancount int
	set NOCOUNT on


	/* check if a transaction is open.  If not, open one.  If already open, create a save point */
	set @trancount = @@trancount
	if @trancount = 0
		begin transaction
	else
		save transaction QueueInsert
		
	/* create a logging comment inside the SQL statement to it is easily found if researching executing queries */
	SET @SQLStatement = '/* Source: ' + isnull(@Source, 'Unassigned') + ' ID: ' + isnull(cast(@SourceID as varchar(20)),'Unassigned') + '. Submitted ' + convert(varchar,getdate(),113) + ' */' + char(13) + char(10) + @SQLStatement
	
		
	/* create conversation and insert into log table */
	begin try
		begin dialog conversation @ConversationHandle
			from service [AnyOrderAsyncQueryExecQueueService01]
			to service N'AnyOrderAsyncQueryExecQueueService01', 'current database'
			with encryption = off
			
		/* send the SQL statement as a message in the dialog.  Doesn't matter what is sent acutally, it isn't used later. */	
		SET @QueueToken = (SELECT [conversation_id] from sys.conversation_endpoints where [conversation_handle] = @ConversationHandle)
		SET @xmlBody = (select @SQLStatement as [statement] for xml path('statements'), type)
		;send on conversation @ConversationHandle (@xmlBody);
		
		
		insert into nfdt.dbo.TBL_AsynchronousQueryQueueLog(EntryID, SubmitDate, Statement, Source, SourceID, SourceLogID)
		values (@QueueToken, GETDATE(), @SQLStatement, @Source, @SourceID, @SourceLogID)
			
		if @trancount = 0 /* clsoe the transaction if we started the transaction above */
			commit
	end try
	
	begin catch
		DECLARE @Error int
		DECLARE @ErrorMessage nvarchar(2048)
		DECLARE @XactState smallint
		SET @Error = ERROR_NUMBER()
		SET @ErrorMessage = ERROR_MESSAGE()
		SET @XactState = XACT_STATE()
			
		/* determiune if we are rolling back this transaction, or just to the save point */
		if @XactState = -1
			rollback
		if @XactState = 1 and @trancount = 0
			rollback
		if @XactState = 1 and @trancount > 0
			rollback transaction QueueInsert
		SET @ErrorCode = 'Error: ' + left(@ErrorMessage,140)
	end catch
	
end


GO
/****** Object:  StoredProcedure [dbo].[PROC_AsynchronousQueryQueueLog_CaptureExecution_NA]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*************************************************************************
	Procedure Name: PROC_AsynchronousQueryQueueLog_CaptureExecution_NA
	Returns:		Failure Code (@ErrorCode)
	Primary User:	website
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Tue Apr 01 15:59:01 CDT 2014 @915 /Internet Time/
	Updated by:		Dave Elderkin
	Information:	Procedure that stores execution data for a query set
	************************************************************************
	Version:		1.00
	Date:			Tue Apr 01 15:59:01 CDT 2014 @915 /Internet Time/
	Updated By:		Dave Elderkin
***************************************************************************/

CREATE Procedure [dbo].[PROC_AsynchronousQueryQueueLog_CaptureExecution_NA] 
	@EntryID uniqueidentifier
AS
	SET NOCOUNT ON
	DECLARE @SQLHandle as varbinary(64)
	DECLARE @PlanHandle as varbinary(64)
	DECLARE @PlanXML as XML
	
	BEGIN TRY
			
		SELECT 
			@SQLHandle = sql_handle
			,@PlanHandle = plan_handle
		FROM 
			sys.dm_exec_query_stats AS querystats
			CROSS APPLY sys.dm_exec_sql_text (querystats.plan_handle) AS sqldata
		WHERE
			sqldata.text = (SELECT Statement FROM nfdt.dbo.TBL_AsynchronousQueryQueueLog WHERE EntryID = @EntryID)
					
					
		IF NOT @SQLHandle IS NULL
			BEGIN
			UPDATE nfdt.dbo.TBL_AsynchronousQueryQueueLog SET
				SQLHandle = @SQLHandle
				,PlanHandle = @PlanHandle
			WHERE EntryID = @EntryID
			
			IF (SELECT max(version) as NewestDbVersion from master.dbo.sysdatabases) >= 661 
				BEGIN
				/* Database Versions.  Will possibly change if DB is moved between servers.  More accurate than compatibility_level since it can be changed.  Techincally @@Version is better, but is text and annooying to parse.
					SQL 2014 	782
					SQL 2012 	706
					SQL 2008R2 	661
					SQL 2008 	655
					SQL 2005 	611
					SQL 2000 	539
				*/
				INSERT INTO NFDT.dbo.TBL_AsynchronousQueryQueueStats(EntryID, StatementNumber, StatementText, CPUTime, Reads, Writes, ExecutionTime, QueryHash, QueryPlanHash, RowCountAffected)
				select 
					@EntryID
					,row_number() over (order by Statement_Start_Offset) as Statement_Number
					,substring(st.text, (qs.statement_start_offset/2) + 1,((case statement_end_offset when -1 then datalength(st.text) else qs.statement_end_offset end - qs.statement_start_offset)/2) + 1) as statement_text
					,Last_Worker_Time
					,Last_Logical_Reads
					,Last_Logical_Writes
					,Last_Elapsed_Time
					,Query_Hash
					,Query_Plan_Hash
					,last_rows  /* added in sql version 2008 R2.  2008 and earlier does not have this column in sys.dm_exec_query_stats */
				from 
					sys.dm_exec_query_stats qs
					cross apply sys.dm_exec_sql_text(qs.sql_handle) st
				WHERE
					qs.sql_handle = @SQLHandle
					AND qs.plan_handle = @PlanHandle 
				END
			ELSE
				BEGIN
				INSERT INTO NFDT.dbo.TBL_AsynchronousQueryQueueStats(EntryID, StatementNumber, StatementText, CPUTime, Reads, Writes, ExecutionTime, QueryHash, QueryPlanHash)
				select 
					@EntryID
					,row_number() over (order by Statement_Start_Offset) as Statement_Number
					,substring(st.text, (qs.statement_start_offset/2) + 1,((case statement_end_offset when -1 then datalength(st.text) else qs.statement_end_offset end - qs.statement_start_offset)/2) + 1) as statement_text
					,Last_Worker_Time
					,Last_Logical_Reads
					,Last_Logical_Writes
					,Last_Elapsed_Time
					,Query_Hash
					,Query_Plan_Hash
				from 
					sys.dm_exec_query_stats qs
					cross apply sys.dm_exec_sql_text(qs.sql_handle) st
				WHERE
					qs.sql_handle = @SQLHandle
					AND qs.plan_handle = @PlanHandle 
			END
				
			IF NOT @PlanHandle IS NULL
				SET @PlanXML = (select query_plan from sys.dm_exec_query_plan(@PlanHandle))
				
			IF NOT @PlanHandle IS NULL AND NOT @PlanXML IS NULL AND (select count(*) from TBL_AsynchronousQueryQueuePlan WHERE PlanHandle = @PlanHandle) = 0
				BEGIN
				INSERT INTO TBL_AsynchronousQueryQueuePlan (PlanHandle, PlanXML)
				VALUES(@PlanHandle, @PlanXML)
			END
		END
	
	END TRY
	
	BEGIN CATCH
		/* fail silently */
	END CATCH
	
	
	



GO
/****** Object:  StoredProcedure [dbo].[PROC_AsyncQueryExecQueue_ExecuteAnyOrder01]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





/*************************************************************************
	Procedure Name: PROC_AsyncQueryExecQueue_ExecuteAnyOrder01
	Returns:		Failure Code (@ErrorCode)
	Primary User:	Admins
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Thu Apr 25 17:29:30 CDT 2013 @978 /Internet Time/
	Documentation:	
	Information:	Procedure that gets a statement from the queue and runs it.
	************************************************************************
	Version:		1.00
	Date:			Thu Apr 25 17:29:30 CDT 2013 @978 /Internet Time/
	Updated By:		Dave Elderkin
	************************************************************************
	Version:		1.00
	Date:			Tue Jan 27 17:32:08 CST 2015 @22 /Internet Time/
	Updated By:		Dave Elderkin
	Changes:		Added notification when rowcount range is outsize specific values
***************************************************************************/
CREATE procedure [dbo].[PROC_AsyncQueryExecQueue_ExecuteAnyOrder01]
WITH EXECUTE AS OWNER
as
begin
	SET NOCOUNT ON
	DECLARE @SQL varchar(max)
	DECLARE @TestSQL varchar(max)
	DECLARE @ConversationHandle uniqueidentifier
	DECLARE @messageTypeName sysname
	DECLARE @messageBody varbinary(max)
	DECLARE @xmlBody xml
	DECLARE @execErrorNumber int
	DECLARE @execErrorMessage nvarchar(2048)
	DECLARE @XactState smallint
	DECLARE @QueueToken uniqueidentifier
	DECLARE @QuerySetQueryID int					/* id number from the query if submitted though the query set process */
	DECLARE @SourceLogID int						/* log ID from the query log table */
	DECLARE @RowCount int
	DECLARE @NotifyIfOverRows int
	DECLARE @NotifyIfUnderRows int
	DECLARE @NotifyIfRowsOutsideDeviation decimal(9,4)
	DECLARE @StandardDeviationRows int /* cast as integer when pulled from the table */
	DECLARE @AverageRows decimal(28,6)
	DECLARE @NotifyText varchar(max)
	DECLARE @QuerySetOwnerEmail varchar(28);
	

	receive top(1) 
	@ConversationHandle = [conversation_handle]
	,@messageTypeName = [message_type_name]
	,@messageBody = [message_body]
	from [AnyOrderAsyncQueryExecQueue01];
		
	if @ConversationHandle is null
		RETURN
		
	SET @QueueToken = (SELECT [conversation_id] from sys.conversation_endpoints where [conversation_handle] = @ConversationHandle)
	end conversation @ConversationHandle /* mark the conversation itself as over, as the conversation isn't needed once recieved. Calling program is not expecting a response */
	
	if (@messageTypeName = N'DEFAULT') /* The DEFAULT message type is a normal type containing a SQL statement to run */
		begin
			/* 
				The below 2 statements will pull the XML from the message, but we will use the unchanged/converted query from the entry table 
				select @xmlBody = CAST(@messageBody as xml)
				select @SQL = @xmlBody.value('(//statements/statement)[1]', 'varchar')
			 */
			 
			 /* get the SQL statement and the source of the sql statement so it can be updated with the results of the SQL statement */
			SET @SQL = (SELECT Statement FROM NFDT.dbo.TBL_AsynchronousQueryQueueLog WHERE EntryID = @QueueToken)
			SET @QuerySetQueryID = (SELECT SourceID FROM NFDT.dbo.TBL_AsynchronousQueryQueueLog WHERE EntryID = @QueueToken AND Source = 'Query Set')
			SET @SourceLogID = (SELECT SourceLogID FROM NFDT.dbo.TBL_AsynchronousQueryQueueLog WHERE EntryID = @QueueToken)
			
			update NFDT.dbo.TBL_AsynchronousQueryQueueLog set StatusText = 'Statement Parsed' where EntryID = @QueueToken
			
			IF @SQL IS NULL /* shouldn't happen unless a manual delete was run on the table or there is an insert issue. */
				BEGIN
				UPDATE NFDT.dbo.TBL_AsynchronousQueryQueueLog set EndDate = GETDATE(), StatusText = 'Statement Empty', ErrorMessage = 'Batch Statement Submitted was Emtpy' where EntryID = @QueueToken
				RETURN 
			END
			
			update NFDT.dbo.TBL_AsynchronousQueryQueueLog set StartDate = getdate(), StatusText = 'Statement Starting' where EntryID = @QueueToken
			begin try
				EXEC (@SQL)
				SET @RowCount = @@ROWCOUNT 

				/* Record Success */
				update NFDT.dbo.TBL_AsynchronousQueryQueueLog set EndDate = GETDATE(), StatusText = 'Statement Completed', RowCountAffected = @RowCount where EntryID = @QueueToken
				IF NOT @QuerySetQueryID IS NULL /* if from a queryset, update the queryset entry */
					BEGIN
					UPDATE NFDT.dbo.TBL_QuerySet SET LastSuccess = GETDATE() WHERE BatchID = @QuerySetQueryID
					
					/* note: Production does nto have the notification system installed. This is only on TRYDVSQL1 */
					SELECT
						@NotifyIfOverRows = NotifyIfOverRows
						,@NotifyIfUnderRows = NotifyIfUnderRows
						,@NotifyIfRowsOutsideDeviation = NotifyIfRowsOutsideDeviation
						,@StandardDeviationRows = cast(StandardDeviationRows as int)
						,@AverageRows = AverageRows
						,@QuerySetOwnerEmail = OwnerEnterpriseID + '@Searshc.com' /* TODO: update to a email lookup once the tables exist */
					FROM 
						NFDT.dbo.TBL_QuerySet S with (nolock) INNER JOIN NFDT.dbo.TBL_QuerySetList SL with (nolock) ON S.SetID = SL.SetID
					WHERE 
						BatchID = @QuerySetQueryID
						
					IF @RowCount > @NotifyIfOverRows AND NOT @NotifyIfOverRows IS NULL AND NOT @QuerySetOwnerEmail IS NULL
						BEGIN
						SET @NotifyText = 'Query Set ' + cast( @QuerySetQueryID as varchar(10)) + ' modified more rows (' + cast(@RowCount as varchar(10)) + ') than the high limit of ' + cast(@NotifyIfOverRows as varchar(10)) + '. Link to <a href="http://inhome.searshc.com/nfd/admin/servers/TRYDVSQL1/querysetbatchlog.asp?batchid=' + cast( @QuerySetQueryID as varchar(10)) + '">Query Set Batch</a>'
						EXECUTE NFDT.dbo.PROC_SendApplicationEmail_NA @EmailID = 41, @BodyHTML = @NotifyText, @SendTo = @QuerySetOwnerEmail
					END
					IF @RowCount < @NotifyIfUnderRows AND NOT @NotifyIfUnderRows IS NULL AND NOT @QuerySetOwnerEmail IS NULL
						BEGIN
						SET @NotifyText = 'Query Set ' + cast( @QuerySetQueryID as varchar(10)) + ' modified fewer rows (' + cast(@RowCount as varchar(10)) + ') than the low limit of ' + cast(@NotifyIfUnderRows as varchar(10)) + '. Link to <a href="http://inhome.searshc.com/nfd/admin/servers/TRYDVSQL1/querysetbatchlog.asp?batchid=' + cast( @QuerySetQueryID as varchar(10)) + '">Query Set Batch</a>'
						EXECUTE NFDT.dbo.PROC_SendApplicationEmail_NA @EmailID = 41, @BodyHTML = @NotifyText, @SendTo = @QuerySetOwnerEmail
					END
					IF NOT @NotifyIfRowsOutsideDeviation IS NULL AND NOT @StandardDeviationRows IS NULL AND NOT @AverageRows IS NULL /* 3 elements needed for the calculation */
						BEGIN
						IF @RowCount > (@AverageRows + (@StandardDeviationRows * @NotifyIfRowsOutsideDeviation)) OR @RowCount < (@AverageRows - (@StandardDeviationRows * @NotifyIfRowsOutsideDeviation)) /* rowcount is not between the std deviation range */
							BEGIN				
							SET @NotifyText = 'Query Set ' + cast( @QuerySetQueryID as varchar(10)) + ' modified a rowcount of ' + cast(@RowCount as varchar(10)) + '.  This is ouside the maximum deviation allowed range of ' + cast((@AverageRows - (@StandardDeviationRows * @NotifyIfRowsOutsideDeviation)) as varchar(100)) + ' and ' + cast((@AverageRows + (@StandardDeviationRows * @NotifyIfRowsOutsideDeviation)) as varchar(100)) + '. Link to <a href="http://inhome.searshc.com/nfd/admin/servers/TRYDVSQL1/querysetbatchlog.asp?batchid=' + cast( @QuerySetQueryID as varchar(10)) + '">Query Set Batch</a>'
							EXECUTE NFDT.dbo.PROC_SendApplicationEmail_NA @EmailID = 41, @BodyHTML = @NotifyText, @SendTo = @QuerySetOwnerEmail
						END
					END
				END
			end try

			begin catch
				select @execErrorNumber = ERROR_NUMBER(), @execErrorMessage = ERROR_MESSAGE() /* catch errors driven by bad SQL or something in the SQL statement */
				
				update NFDT.dbo.TBL_AsynchronousQueryQueueLog set EndDate = GETDATE(), StatusText = 'Statement Failed', ErrorNumber = @execErrorNumber, ErrorMessage = @execErrorMessage, RowCountAffected = @RowCount where EntryID = @QueueToken
				
				/* if from a queryset, update the queryset entry */
				IF NOT @QuerySetQueryID IS NULL
					BEGIN
					UPDATE NFDT.dbo.TBL_QuerySet SET LastFailure = GETDATE(), LastErrorMessage = @execErrorMessage WHERE BatchID = @QuerySetQueryID
					UPDATE NFDT.dbo.TBL_QuerySetList SET LastFailure = GETDATE() WHERE SetID = (Select SetID FROM NFDT.dbo.TBL_QuerySet with (nolock) WHERE BatchID = @QuerySetQueryID)
				END
			end catch

			/* record stats for the executed query */
			EXECUTE PROC_AsynchronousQueryQueueLog_CaptureExecution_NA @QueueToken
			/* update things updated on sucess or failure */
			IF NOT @SourceLogID IS NULL
				BEGIN
				UPDATE NFDT.dbo.TBL_QuerySetExecutionLog SET StatementCountCompleted = StatementCountCompleted + 1 WHERE LogID = @SourceLogID /* update the execution Log */
				EXECUTE NFDT.dbo.PROC_QuerySetExecutionLog_ReviewLog_NA @QueryLogID = @SourceLogID /* look for errors and clean up as required. */
			END
		end 
	else if (@messageTypeName = N'http://schemas.microsoft.com/SQL/ServiceBroker/EndDialog') /* Don't care about the end */
			begin
			WAITFOR DELAY '00:00:01'
		end
	else if (@messageTypeName = N'http://schemas.microsoft.com/SQL/ServiceBroker/Error') /* service broker is broken. record it */
		begin
			declare @ErrorNumber int
			declare @errorMessage nvarchar(4000)
			select @xmlBody = CAST(@messageBody as xml);
			with xmlnamespaces (DEFAULT N'http://schemas.microsoft.com/SQL/ServiceBroker/Error')
			select @ErrorNumber = @xmlBody.value ('(/Error/Code)[1]', 'INT'), @errorMessage = @xmlBody.value ('(/Error/Description)[1]', 'NVARCHAR(4000)')

			update NFDT.dbo.TBL_AsynchronousQueryQueueLog set EndDate = GETDATE(), StatusText = 'Broker Error', ErrorNumber = @ErrorNumber ,ErrorMessage = @errorMessage where EntryID = @QueueToken
				
			/* if from a queryset, update the queryset entry */
			IF NOT @QuerySetQueryID IS NULL
				BEGIN
				UPDATE NFDT.dbo.TBL_QuerySet SET LastFailure = GETDATE() WHERE BatchID = @QuerySetQueryID
			END
				
			end conversation @ConversationHandle
		 end
	else
		begin
		raiserror(N'Received unexpected message type: %s', 16, 50, @messageTypeName) /* message type was entered that was not expected */
		end conversation @ConversationHandle
	end
end



GO
/****** Object:  StoredProcedure [dbo].[PROC_AsyncQueryExecQueue_ExecuteInOrder01]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



/*************************************************************************
	Procedure Name: PROC_AsyncQueryExecQueue_ExecuteInOrder01
	Returns:		Failure Code (@ErrorCode)
	Primary User:	Admins
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Thu Apr 25 17:29:30 CDT 2013 @978 /Internet Time/
	Documentation:	
	Information:	Procedure that gets a statement from the queue and runs it.
	************************************************************************
	Version:		1.00
	Date:			Thu Apr 25 17:29:30 CDT 2013 @978 /Internet Time/
	Updated By:		Dave Elderkin
***************************************************************************/
CREATE procedure [dbo].[PROC_AsyncQueryExecQueue_ExecuteInOrder01]
WITH EXECUTE AS OWNER
as
begin
	SET NOCOUNT ON
	DECLARE @SQL varchar(max)
	DECLARE @TestSQL varchar(max)
	DECLARE @ConversationHandle uniqueidentifier
	DECLARE @messageTypeName sysname
	DECLARE @messageBody varbinary(max)
	DECLARE @xmlBody xml
	DECLARE @execErrorNumber int
	DECLARE @execErrorMessage nvarchar(2048)
	DECLARE @XactState smallint
	DECLARE @QueueToken uniqueidentifier
	DECLARE @QuerySetQueryID int					/* id number from the query if submitted though the query set process */
	DECLARE @SourceLogID int						/* log ID from the query log table */
	DECLARE @RowCount int


	begin try;
		receive top(1) 
			@ConversationHandle = [conversation_handle]
			, @messageTypeName = [message_type_name]
			, @messageBody = [message_body]
			from [InOrderAsyncQueryExecQueue01];
				
		if (@ConversationHandle is not null)
			begin
			SET @QueueToken = (SELECT [conversation_id] from sys.conversation_endpoints where [conversation_handle] = @ConversationHandle)
			END conversation @ConversationHandle /* mark the conversation itself as over, as the conversation isn't needed once recieved.  Calling program is not expecting a response */
			
			if (@messageTypeName = N'DEFAULT') /* The DEFAULT message type is a normal type containing a SQL statement to run */
				begin
					/* 
						The below 2 statements will pull the XML from the message, but we will use the unchanged/converted query from the entry table 
						select @xmlBody = CAST(@messageBody as xml)
						select @SQL = @xmlBody.value('(//statements/statement)[1]', 'varchar')
					*/
		
					 /* get the SQL statement and the source of the sql statement so it can be updated with the results of the SQL statement */
					SET @SQL = (SELECT Statement FROM NFDT.dbo.TBL_AsynchronousQueryQueueLog WHERE EntryID =  @QueueToken)
					SET @QuerySetQueryID = (SELECT SourceID FROM NFDT.dbo.TBL_AsynchronousQueryQueueLog WHERE EntryID =  @QueueToken AND Source = 'Query Set')
					SET @SourceLogID = (SELECT SourceLogID FROM NFDT.dbo.TBL_AsynchronousQueryQueueLog WHERE EntryID =  @QueueToken)
					
					update NFDT.dbo.TBL_AsynchronousQueryQueueLog set StartDate = getdate() where EntryID = @QueueToken
					
					/* Execute the statement and return the results */
					begin try
						EXEC (@SQL)
						SET @RowCount = @@ROWCOUNT 
						
						update NFDT.dbo.TBL_AsynchronousQueryQueueLog set EndDate = GETDATE(), RowCountAffected = @RowCount where EntryID = @QueueToken
					end try

					/* catch errors driven by bad SQL or something in the SQL statement */
					begin catch
						select @execErrorNumber = ERROR_NUMBER(), @execErrorMessage = ERROR_MESSAGE()
						
						update NFDT.dbo.TBL_AsynchronousQueryQueueLog set EndDate = GETDATE() ,ErrorNumber = @execErrorNumber ,ErrorMessage = @execErrorMessage ,RowCountAffected = @RowCount where EntryID = @QueueToken
						
						/* if from a queryset, update the queryset entry */
						IF NOT @QuerySetQueryID IS NULL
							BEGIN
							UPDATE NFDT.dbo.TBL_QuerySet SET LastFailure = GETDATE(), LastErrorMessage = left('Run from async Queue, ID: ' + cast(@QueueToken as varchar(50)) + @execErrorMessage,2048)  WHERE BatchID = @QuerySetQueryID
						END
					end catch
					
					/* record stats for the executed query */
					EXECUTE PROC_AsynchronousQueryQueueLog_CaptureExecution_NA @QueueToken
					
					/* if from a queryset, update the queryset entry and perform other queryset tasks as required */
					IF NOT @QuerySetQueryID IS NULL
						BEGIN
						IF @execErrorNumber IS NULL
							BEGIN
							UPDATE NFDT.dbo.TBL_QuerySet SET LastSuccess = GETDATE() WHERE BatchID = @QuerySetQueryID
							END
						ELSE
							BEGIN
							UPDATE NFDT.dbo.TBL_QuerySetList SET LastFailure = GETDATE() WHERE SetID = (Select SetID FROM NFDT.dbo.TBL_QuerySet with (nolock) WHERE BatchID = @QuerySetQueryID)
						END	
					END
					
					IF NOT @SourceLogID IS NULL
						BEGIN
						UPDATE NFDT.dbo.TBL_QuerySetExecutionLog SET StatementCountCompleted = StatementCountCompleted + 1 WHERE LogID = @SourceLogID /* update the execution Log */
						EXECUTE NFDT.dbo.PROC_QuerySetExecutionLog_ReviewLog_NA @QueryLogID = @SourceLogID /* look for errors and clean up as required. */
					END
					
						
				end 
			else if (@messageTypeName = N'http://schemas.microsoft.com/SQL/ServiceBroker/EndDialog')  /* Don't care about the enddialog */
					begin
					WAITFOR DELAY '00:00:01'
					 /* don't need to close, because it was already closed as part of the processing.  end conversation @ConversationHandle*/
				end
			else if (@messageTypeName = N'http://schemas.microsoft.com/SQL/ServiceBroker/Error') /* service broker error message. record it */
				begin
					declare @ErrorNumber int
					declare @errorMessage nvarchar(4000)
					select @xmlBody = CAST(@messageBody as xml);
					with xmlnamespaces (DEFAULT N'http://schemas.microsoft.com/SQL/ServiceBroker/Error')
					select @ErrorNumber = @xmlBody.value ('(/Error/Code)[1]', 'INT'), @errorMessage = @xmlBody.value ('(/Error/Description)[1]', 'NVARCHAR(4000)')
		
					update NFDT.dbo.TBL_AsynchronousQueryQueueLog set
						ErrorNumber = @ErrorNumber
						,ErrorMessage = @errorMessage
						where EntryID = @QueueToken
						                                                                                                                  
					/* if from a queryset, update the queryset entry */
					IF NOT @QuerySetQueryID IS NULL
						BEGIN
						UPDATE NFDT.dbo.TBL_QuerySet SET LastFailure = GETDATE() WHERE BatchID = @QuerySetQueryID
					END
						
					end conversation @ConversationHandle
				 end
			else
				begin
				raiserror(N'Received unexpected message type: %s', 16, 50, @messageTypeName) /* message type was entered that was not expected */
				end conversation @ConversationHandle
			end
		end
	end try
	begin catch
		declare @error int
		declare @message varchar(2048)
		select @error = ERROR_NUMBER()
			, @message = ERROR_MESSAGE()

		raiserror(N'Error: %i, %s', 1, 60,  @error, @message) with log /* raise error mesasge for server to handle */
	end catch
end





GO
/****** Object:  StoredProcedure [dbo].[PROC_AsyncQueryExecQueue_ExecuteInOrder02]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*************************************************************************
	Procedure Name: PROC_AsyncQueryExecQueue_ExecuteInOrder02
	Returns:		Failure Code (@ErrorCode)
	Primary User:	Admins
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Thu Apr 25 17:29:30 CDT 2013 @978 /Internet Time/
	Documentation:	
	Information:	Procedure that gets a statement from the queue and runs it.
	************************************************************************
	Version:		1.00
	Date:			Thu Apr 25 17:29:30 CDT 2013 @978 /Internet Time/
	Updated By:		Dave Elderkin
***************************************************************************/
CREATE procedure [dbo].[PROC_AsyncQueryExecQueue_ExecuteInOrder02]
WITH EXECUTE AS OWNER
as
begin
	SET NOCOUNT ON
	DECLARE @SQL varchar(max)
	DECLARE @TestSQL varchar(max)
	DECLARE @ConversationHandle uniqueidentifier
	DECLARE @messageTypeName sysname
	DECLARE @messageBody varbinary(max)
	DECLARE @xmlBody xml
	DECLARE @execErrorNumber int
	DECLARE @execErrorMessage nvarchar(2048)
	DECLARE @XactState smallint
	DECLARE @QueueToken uniqueidentifier
	DECLARE @QuerySetQueryID int					/* id number from the query if submitted though the query set process */
	DECLARE @SourceLogID int						/* log ID from the query log table */
	DECLARE @RowCount int


	begin try;
		receive top(1) 
			@ConversationHandle = [conversation_handle]
			, @messageTypeName = [message_type_name]
			, @messageBody = [message_body]
			from [InOrderAsyncQueryExecQueue02];
				
		if (@ConversationHandle is not null)
			begin
			SET @QueueToken = (SELECT [conversation_id] from sys.conversation_endpoints where [conversation_handle] = @ConversationHandle)
			END conversation @ConversationHandle /* mark the conversation itself as over, as the conversation isn't needed once recieved.  Calling program is not expecting a response */
			
			if (@messageTypeName = N'DEFAULT') /* The DEFAULT message type is a normal type containing a SQL statement to run */
				begin
					/* 
						The below 2 statements will pull the XML from the message, but we will use the unchanged/converted query from the entry table 
						select @xmlBody = CAST(@messageBody as xml)
						select @SQL = @xmlBody.value('(//statements/statement)[1]', 'varchar')
					*/
		
					 /* get the SQL statement and the source of the sql statement so it can be updated with the results of the SQL statement */
					SET @SQL = (SELECT Statement FROM NFDT.dbo.TBL_AsynchronousQueryQueueLog WHERE EntryID =  @QueueToken)
					SET @QuerySetQueryID = (SELECT SourceID FROM NFDT.dbo.TBL_AsynchronousQueryQueueLog WHERE EntryID =  @QueueToken AND Source = 'Query Set')
					SET @SourceLogID = (SELECT SourceLogID FROM NFDT.dbo.TBL_AsynchronousQueryQueueLog WHERE EntryID =  @QueueToken)
					
					update NFDT.dbo.TBL_AsynchronousQueryQueueLog set StartDate = getdate() where EntryID = @QueueToken
					
					/* Execute the statement and return the results */
					begin try
						EXEC (@SQL)
						SET @RowCount = @@ROWCOUNT 
						
						update NFDT.dbo.TBL_AsynchronousQueryQueueLog set EndDate = GETDATE(), RowCountAffected = @RowCount where EntryID = @QueueToken
					end try

					/* catch errors driven by bad SQL or something in the SQL statement */
					begin catch
						select @execErrorNumber = ERROR_NUMBER(), @execErrorMessage = ERROR_MESSAGE()
						
						update NFDT.dbo.TBL_AsynchronousQueryQueueLog set EndDate = GETDATE() ,ErrorNumber = @execErrorNumber ,ErrorMessage = @execErrorMessage ,RowCountAffected = @RowCount where EntryID = @QueueToken
						
						/* if from a queryset, update the queryset entry */
						IF NOT @QuerySetQueryID IS NULL
							BEGIN
							UPDATE NFDT.dbo.TBL_QuerySet SET LastFailure = GETDATE(), LastErrorMessage = left('Run from async Queue, ID: ' + cast(@QueueToken as varchar(50)) + @execErrorMessage,2048)  WHERE BatchID = @QuerySetQueryID
						END
					end catch
					
					/* record stats for the executed query */
					EXECUTE PROC_AsynchronousQueryQueueLog_CaptureExecution_NA @QueueToken
					
					/* if from a queryset, update the queryset entry and perform other queryset tasks as required */
					IF NOT @QuerySetQueryID IS NULL
						BEGIN
						IF @execErrorNumber IS NULL
							BEGIN
							UPDATE NFDT.dbo.TBL_QuerySet SET LastSuccess = GETDATE() WHERE BatchID = @QuerySetQueryID
							END
						ELSE
							BEGIN
							UPDATE NFDT.dbo.TBL_QuerySetList SET LastFailure = GETDATE() WHERE SetID = (Select SetID FROM NFDT.dbo.TBL_QuerySet with (nolock) WHERE BatchID = @QuerySetQueryID)
						END	
					END
					
					IF NOT @SourceLogID IS NULL
						BEGIN
						UPDATE NFDT.dbo.TBL_QuerySetExecutionLog SET StatementCountCompleted = StatementCountCompleted + 1 WHERE LogID = @SourceLogID /* update the execution Log */
						EXECUTE NFDT.dbo.PROC_QuerySetExecutionLog_ReviewLog_NA @QueryLogID = @SourceLogID /* look for errors and clean up as required. */
					END
					
						
				end 
			else if (@messageTypeName = N'http://schemas.microsoft.com/SQL/ServiceBroker/EndDialog')  /* Don't care about the enddialog */
					begin
					WAITFOR DELAY '00:00:01'
					 /* don't need to close, because it was already closed as part of the processing.  end conversation @ConversationHandle*/
				end
			else if (@messageTypeName = N'http://schemas.microsoft.com/SQL/ServiceBroker/Error') /* service broker error message. record it */
				begin
					declare @ErrorNumber int
					declare @errorMessage nvarchar(4000)
					select @xmlBody = CAST(@messageBody as xml);
					with xmlnamespaces (DEFAULT N'http://schemas.microsoft.com/SQL/ServiceBroker/Error')
					select @ErrorNumber = @xmlBody.value ('(/Error/Code)[1]', 'INT'), @errorMessage = @xmlBody.value ('(/Error/Description)[1]', 'NVARCHAR(4000)')
		
					update NFDT.dbo.TBL_AsynchronousQueryQueueLog set
						ErrorNumber = @ErrorNumber
						,ErrorMessage = @errorMessage
						where EntryID = @QueueToken
						                                                                                                                  
					/* if from a queryset, update the queryset entry */
					IF NOT @QuerySetQueryID IS NULL
						BEGIN
						UPDATE NFDT.dbo.TBL_QuerySet SET LastFailure = GETDATE() WHERE BatchID = @QuerySetQueryID
					END
						
					end conversation @ConversationHandle
				 end
			else
				begin
				raiserror(N'Received unexpected message type: %s', 16, 50, @messageTypeName) /* message type was entered that was not expected */
				end conversation @ConversationHandle
			end
		end
	end try
	begin catch
		declare @error int
		declare @message varchar(2048)
		select @error = ERROR_NUMBER()
			, @message = ERROR_MESSAGE()

		raiserror(N'Error: %i, %s', 1, 60,  @error, @message) with log /* raise error mesasge for server to handle */
	end catch
end




GO
/****** Object:  StoredProcedure [dbo].[PROC_AsyncQueryExecQueue_ExecuteInOrder03]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



/*************************************************************************
	Procedure Name: PROC_AsyncQueryExecQueue_ExecuteInOrder03
	Returns:		Failure Code (@ErrorCode)
	Primary User:	Admins
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Thu Apr 25 17:29:30 CDT 2013 @978 /Internet Time/
	Documentation:	
	Information:	Procedure that gets a statement from the queue and runs it.
	************************************************************************
	Version:		1.00
	Date:			Thu Apr 25 17:29:30 CDT 2013 @978 /Internet Time/
	Updated By:		Dave Elderkin
***************************************************************************/
CREATE procedure [dbo].[PROC_AsyncQueryExecQueue_ExecuteInOrder03]
WITH EXECUTE AS OWNER
as
begin
	SET NOCOUNT ON
	DECLARE @SQL varchar(max)
	DECLARE @TestSQL varchar(max)
	DECLARE @ConversationHandle uniqueidentifier
	DECLARE @messageTypeName sysname
	DECLARE @messageBody varbinary(max)
	DECLARE @xmlBody xml
	DECLARE @execErrorNumber int
	DECLARE @execErrorMessage nvarchar(2048)
	DECLARE @XactState smallint
	DECLARE @QueueToken uniqueidentifier
	DECLARE @QuerySetQueryID int					/* id number from the query if submitted though the query set process */
	DECLARE @SourceLogID int						/* log ID from the query log table */
	DECLARE @RowCount int


	begin try;
		receive top(1) 
			@ConversationHandle = [conversation_handle]
			, @messageTypeName = [message_type_name]
			, @messageBody = [message_body]
			from [InOrderAsyncQueryExecQueue03];
				
		if (@ConversationHandle is not null)
			begin
			SET @QueueToken = (SELECT [conversation_id] from sys.conversation_endpoints where [conversation_handle] = @ConversationHandle)
			END conversation @ConversationHandle /* mark the conversation itself as over, as the conversation isn't needed once recieved.  Calling program is not expecting a response */
			
			if (@messageTypeName = N'DEFAULT') /* The DEFAULT message type is a normal type containing a SQL statement to run */
				begin
					/* 
						The below 2 statements will pull the XML from the message, but we will use the unchanged/converted query from the entry table 
						select @xmlBody = CAST(@messageBody as xml)
						select @SQL = @xmlBody.value('(//statements/statement)[1]', 'varchar')
					*/
		
					 /* get the SQL statement and the source of the sql statement so it can be updated with the results of the SQL statement */
					SET @SQL = (SELECT Statement FROM NFDT.dbo.TBL_AsynchronousQueryQueueLog WHERE EntryID =  @QueueToken)
					SET @QuerySetQueryID = (SELECT SourceID FROM NFDT.dbo.TBL_AsynchronousQueryQueueLog WHERE EntryID =  @QueueToken AND Source = 'Query Set')
					SET @SourceLogID = (SELECT SourceLogID FROM NFDT.dbo.TBL_AsynchronousQueryQueueLog WHERE EntryID =  @QueueToken)
					
					update NFDT.dbo.TBL_AsynchronousQueryQueueLog set StartDate = getdate() where EntryID = @QueueToken
					
					/* Execute the statement and return the results */
					begin try
						EXEC (@SQL)
						SET @RowCount = @@ROWCOUNT 
						
						update NFDT.dbo.TBL_AsynchronousQueryQueueLog set EndDate = GETDATE(), RowCountAffected = @RowCount where EntryID = @QueueToken
					end try

					/* catch errors driven by bad SQL or something in the SQL statement */
					begin catch
						select @execErrorNumber = ERROR_NUMBER(), @execErrorMessage = ERROR_MESSAGE()
						
						update NFDT.dbo.TBL_AsynchronousQueryQueueLog set EndDate = GETDATE() ,ErrorNumber = @execErrorNumber ,ErrorMessage = @execErrorMessage ,RowCountAffected = @RowCount where EntryID = @QueueToken
						
						/* if from a queryset, update the queryset entry */
						IF NOT @QuerySetQueryID IS NULL
							BEGIN
							UPDATE NFDT.dbo.TBL_QuerySet SET LastFailure = GETDATE(), LastErrorMessage = left('Run from async Queue, ID: ' + cast(@QueueToken as varchar(50)) + @execErrorMessage,2048)  WHERE BatchID = @QuerySetQueryID
						END
					end catch
					
					/* record stats for the executed query */
					EXECUTE PROC_AsynchronousQueryQueueLog_CaptureExecution_NA @QueueToken
					
					/* if from a queryset, update the queryset entry and perform other queryset tasks as required */
					IF NOT @QuerySetQueryID IS NULL
						BEGIN
						IF @execErrorNumber IS NULL
							BEGIN
							UPDATE NFDT.dbo.TBL_QuerySet SET LastSuccess = GETDATE() WHERE BatchID = @QuerySetQueryID
							END
						ELSE
							BEGIN
							UPDATE NFDT.dbo.TBL_QuerySetList SET LastFailure = GETDATE() WHERE SetID = (Select SetID FROM NFDT.dbo.TBL_QuerySet with (nolock) WHERE BatchID = @QuerySetQueryID)
						END	
					END
					
					IF NOT @SourceLogID IS NULL
						BEGIN
						UPDATE NFDT.dbo.TBL_QuerySetExecutionLog SET StatementCountCompleted = StatementCountCompleted + 1 WHERE LogID = @SourceLogID /* update the execution Log */
						EXECUTE NFDT.dbo.PROC_QuerySetExecutionLog_ReviewLog_NA @QueryLogID = @SourceLogID /* look for errors and clean up as required. */
					END
					
						
				end 
			else if (@messageTypeName = N'http://schemas.microsoft.com/SQL/ServiceBroker/EndDialog')  /* Don't care about the enddialog */
					begin
					WAITFOR DELAY '00:00:01'
					 /* don't need to close, because it was already closed as part of the processing.  end conversation @ConversationHandle*/
				end
			else if (@messageTypeName = N'http://schemas.microsoft.com/SQL/ServiceBroker/Error') /* service broker error message. record it */
				begin
					declare @ErrorNumber int
					declare @errorMessage nvarchar(4000)
					select @xmlBody = CAST(@messageBody as xml);
					with xmlnamespaces (DEFAULT N'http://schemas.microsoft.com/SQL/ServiceBroker/Error')
					select @ErrorNumber = @xmlBody.value ('(/Error/Code)[1]', 'INT'), @errorMessage = @xmlBody.value ('(/Error/Description)[1]', 'NVARCHAR(4000)')
		
					update NFDT.dbo.TBL_AsynchronousQueryQueueLog set
						ErrorNumber = @ErrorNumber
						,ErrorMessage = @errorMessage
						where EntryID = @QueueToken
						                                                                                                                  
					/* if from a queryset, update the queryset entry */
					IF NOT @QuerySetQueryID IS NULL
						BEGIN
						UPDATE NFDT.dbo.TBL_QuerySet SET LastFailure = GETDATE() WHERE BatchID = @QuerySetQueryID
					END
						
					end conversation @ConversationHandle
				 end
			else
				begin
				raiserror(N'Received unexpected message type: %s', 16, 50, @messageTypeName) /* message type was entered that was not expected */
				end conversation @ConversationHandle
			end
		end
	end try
	begin catch
		declare @error int
		declare @message varchar(2048)
		select @error = ERROR_NUMBER()
			, @message = ERROR_MESSAGE()

		raiserror(N'Error: %i, %s', 1, 60,  @error, @message) with log /* raise error mesasge for server to handle */
	end catch
end




GO
/****** Object:  StoredProcedure [dbo].[PROC_AsyncQueryExecQueue_ExecuteInOrder04]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*************************************************************************
	Procedure Name: PROC_AsyncQueryExecQueue_ExecuteInOrder04
	Returns:		Failure Code (@ErrorCode)
	Primary User:	Admins
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Thu Apr 25 17:29:30 CDT 2013 @978 /Internet Time/
	Documentation:	
	Information:	Procedure that gets a statement from the queue and runs it.
	************************************************************************
	Version:		1.00
	Date:			Thu Apr 25 17:29:30 CDT 2013 @978 /Internet Time/
	Updated By:		Dave Elderkin
***************************************************************************/
CREATE procedure [dbo].[PROC_AsyncQueryExecQueue_ExecuteInOrder04]
WITH EXECUTE AS OWNER
as
begin
	SET NOCOUNT ON
	DECLARE @SQL varchar(max)
	DECLARE @TestSQL varchar(max)
	DECLARE @ConversationHandle uniqueidentifier
	DECLARE @messageTypeName sysname
	DECLARE @messageBody varbinary(max)
	DECLARE @xmlBody xml
	DECLARE @execErrorNumber int
	DECLARE @execErrorMessage nvarchar(2048)
	DECLARE @XactState smallint
	DECLARE @QueueToken uniqueidentifier
	DECLARE @QuerySetQueryID int					/* id number from the query if submitted though the query set process */
	DECLARE @SourceLogID int						/* log ID from the query log table */
	DECLARE @RowCount int


	begin try;
		receive top(1) 
			@ConversationHandle = [conversation_handle]
			, @messageTypeName = [message_type_name]
			, @messageBody = [message_body]
			from [InOrderAsyncQueryExecQueue04];
				
		if (@ConversationHandle is not null)
			begin
			SET @QueueToken = (SELECT [conversation_id] from sys.conversation_endpoints where [conversation_handle] = @ConversationHandle)
			END conversation @ConversationHandle /* mark the conversation itself as over, as the conversation isn't needed once recieved.  Calling program is not expecting a response */
			
			if (@messageTypeName = N'DEFAULT') /* The DEFAULT message type is a normal type containing a SQL statement to run */
				begin
					/* 
						The below 2 statements will pull the XML from the message, but we will use the unchanged/converted query from the entry table 
						select @xmlBody = CAST(@messageBody as xml)
						select @SQL = @xmlBody.value('(//statements/statement)[1]', 'varchar')
					*/
		
					 /* get the SQL statement and the source of the sql statement so it can be updated with the results of the SQL statement */
					SET @SQL = (SELECT Statement FROM NFDT.dbo.TBL_AsynchronousQueryQueueLog WHERE EntryID =  @QueueToken)
					SET @QuerySetQueryID = (SELECT SourceID FROM NFDT.dbo.TBL_AsynchronousQueryQueueLog WHERE EntryID =  @QueueToken AND Source = 'Query Set')
					SET @SourceLogID = (SELECT SourceLogID FROM NFDT.dbo.TBL_AsynchronousQueryQueueLog WHERE EntryID =  @QueueToken)
					
					update NFDT.dbo.TBL_AsynchronousQueryQueueLog set StartDate = getdate() where EntryID = @QueueToken
					
					/* Execute the statement and return the results */
					begin try
						EXEC (@SQL)
						SET @RowCount = @@ROWCOUNT 
						
						update NFDT.dbo.TBL_AsynchronousQueryQueueLog set EndDate = GETDATE(), RowCountAffected = @RowCount where EntryID = @QueueToken
					end try

					/* catch errors driven by bad SQL or something in the SQL statement */
					begin catch
						select @execErrorNumber = ERROR_NUMBER(), @execErrorMessage = ERROR_MESSAGE()
						
						update NFDT.dbo.TBL_AsynchronousQueryQueueLog set EndDate = GETDATE() ,ErrorNumber = @execErrorNumber ,ErrorMessage = @execErrorMessage ,RowCountAffected = @RowCount where EntryID = @QueueToken
						
						/* if from a queryset, update the queryset entry */
						IF NOT @QuerySetQueryID IS NULL
							BEGIN
							UPDATE NFDT.dbo.TBL_QuerySet SET LastFailure = GETDATE(), LastErrorMessage = left('Run from async Queue, ID: ' + cast(@QueueToken as varchar(50)) + @execErrorMessage,2048)  WHERE BatchID = @QuerySetQueryID
						END
					end catch
					
					/* record stats for the executed query */
					EXECUTE PROC_AsynchronousQueryQueueLog_CaptureExecution_NA @QueueToken
					
					/* if from a queryset, update the queryset entry and perform other queryset tasks as required */
					IF NOT @QuerySetQueryID IS NULL
						BEGIN
						IF @execErrorNumber IS NULL
							BEGIN
							UPDATE NFDT.dbo.TBL_QuerySet SET LastSuccess = GETDATE() WHERE BatchID = @QuerySetQueryID
							END
						ELSE
							BEGIN
							UPDATE NFDT.dbo.TBL_QuerySetList SET LastFailure = GETDATE() WHERE SetID = (Select SetID FROM NFDT.dbo.TBL_QuerySet with (nolock) WHERE BatchID = @QuerySetQueryID)
						END	
					END
					
					IF NOT @SourceLogID IS NULL
						BEGIN
						UPDATE NFDT.dbo.TBL_QuerySetExecutionLog SET StatementCountCompleted = StatementCountCompleted + 1 WHERE LogID = @SourceLogID /* update the execution Log */
						EXECUTE NFDT.dbo.PROC_QuerySetExecutionLog_ReviewLog_NA @QueryLogID = @SourceLogID /* look for errors and clean up as required. */
					END
					
						
				end 
			else if (@messageTypeName = N'http://schemas.microsoft.com/SQL/ServiceBroker/EndDialog')  /* Don't care about the enddialog */
					begin
					WAITFOR DELAY '00:00:01'
					 /* don't need to close, because it was already closed as part of the processing.  end conversation @ConversationHandle*/
				end
			else if (@messageTypeName = N'http://schemas.microsoft.com/SQL/ServiceBroker/Error') /* service broker error message. record it */
				begin
					declare @ErrorNumber int
					declare @errorMessage nvarchar(4000)
					select @xmlBody = CAST(@messageBody as xml);
					with xmlnamespaces (DEFAULT N'http://schemas.microsoft.com/SQL/ServiceBroker/Error')
					select @ErrorNumber = @xmlBody.value ('(/Error/Code)[1]', 'INT'), @errorMessage = @xmlBody.value ('(/Error/Description)[1]', 'NVARCHAR(4000)')
		
					update NFDT.dbo.TBL_AsynchronousQueryQueueLog set
						ErrorNumber = @ErrorNumber
						,ErrorMessage = @errorMessage
						where EntryID = @QueueToken
						                                                                                                                  
					/* if from a queryset, update the queryset entry */
					IF NOT @QuerySetQueryID IS NULL
						BEGIN
						UPDATE NFDT.dbo.TBL_QuerySet SET LastFailure = GETDATE() WHERE BatchID = @QuerySetQueryID
					END
						
					end conversation @ConversationHandle
				 end
			else
				begin
				raiserror(N'Received unexpected message type: %s', 16, 50, @messageTypeName) /* message type was entered that was not expected */
				end conversation @ConversationHandle
			end
		end
	end try
	begin catch
		declare @error int
		declare @message varchar(2048)
		select @error = ERROR_NUMBER()
			, @message = ERROR_MESSAGE()

		raiserror(N'Error: %i, %s', 1, 60,  @error, @message) with log /* raise error mesasge for server to handle */
	end catch
end




GO
/****** Object:  StoredProcedure [dbo].[PROC_AsyncQueryExecQueue_ExecuteInOrder05]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*************************************************************************
	Procedure Name: PROC_AsyncQueryExecQueue_ExecuteInOrder05
	Returns:		Failure Code (@ErrorCode)
	Primary User:	Admins
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Thu Apr 25 17:29:30 CDT 2013 @978 /Internet Time/
	Documentation:	
	Information:	Procedure that gets a statement from the queue and runs it.
	************************************************************************
	Version:		1.00
	Date:			Thu Apr 25 17:29:30 CDT 2013 @978 /Internet Time/
	Updated By:		Dave Elderkin
***************************************************************************/
CREATE procedure [dbo].[PROC_AsyncQueryExecQueue_ExecuteInOrder05]
WITH EXECUTE AS OWNER
as
begin
	SET NOCOUNT ON
	DECLARE @SQL varchar(max)
	DECLARE @TestSQL varchar(max)
	DECLARE @ConversationHandle uniqueidentifier
	DECLARE @messageTypeName sysname
	DECLARE @messageBody varbinary(max)
	DECLARE @xmlBody xml
	DECLARE @execErrorNumber int
	DECLARE @execErrorMessage nvarchar(2048)
	DECLARE @XactState smallint
	DECLARE @QueueToken uniqueidentifier
	DECLARE @QuerySetQueryID int					/* id number from the query if submitted though the query set process */
	DECLARE @SourceLogID int						/* log ID from the query log table */
	DECLARE @RowCount int


	begin try;
		receive top(1) 
			@ConversationHandle = [conversation_handle]
			, @messageTypeName = [message_type_name]
			, @messageBody = [message_body]
			from [InOrderAsyncQueryExecQueue05];
				
		if (@ConversationHandle is not null)
			begin
			SET @QueueToken = (SELECT [conversation_id] from sys.conversation_endpoints where [conversation_handle] = @ConversationHandle)
			END conversation @ConversationHandle /* mark the conversation itself as over, as the conversation isn't needed once recieved.  Calling program is not expecting a response */
			
			if (@messageTypeName = N'DEFAULT') /* The DEFAULT message type is a normal type containing a SQL statement to run */
				begin
					/* 
						The below 2 statements will pull the XML from the message, but we will use the unchanged/converted query from the entry table 
						select @xmlBody = CAST(@messageBody as xml)
						select @SQL = @xmlBody.value('(//statements/statement)[1]', 'varchar')
					*/
		
					 /* get the SQL statement and the source of the sql statement so it can be updated with the results of the SQL statement */
					SET @SQL = (SELECT Statement FROM NFDT.dbo.TBL_AsynchronousQueryQueueLog WHERE EntryID =  @QueueToken)
					SET @QuerySetQueryID = (SELECT SourceID FROM NFDT.dbo.TBL_AsynchronousQueryQueueLog WHERE EntryID =  @QueueToken AND Source = 'Query Set')
					SET @SourceLogID = (SELECT SourceLogID FROM NFDT.dbo.TBL_AsynchronousQueryQueueLog WHERE EntryID =  @QueueToken)
					
					update NFDT.dbo.TBL_AsynchronousQueryQueueLog set StartDate = getdate() where EntryID = @QueueToken
					
					/* Execute the statement and return the results */
					begin try
						EXEC (@SQL)
						SET @RowCount = @@ROWCOUNT 
						
						update NFDT.dbo.TBL_AsynchronousQueryQueueLog set EndDate = GETDATE(), RowCountAffected = @RowCount where EntryID = @QueueToken
					end try

					/* catch errors driven by bad SQL or something in the SQL statement */
					begin catch
						select @execErrorNumber = ERROR_NUMBER(), @execErrorMessage = ERROR_MESSAGE()
						
						update NFDT.dbo.TBL_AsynchronousQueryQueueLog set EndDate = GETDATE() ,ErrorNumber = @execErrorNumber ,ErrorMessage = @execErrorMessage ,RowCountAffected = @RowCount where EntryID = @QueueToken
						
						/* if from a queryset, update the queryset entry */
						IF NOT @QuerySetQueryID IS NULL
							BEGIN
							UPDATE NFDT.dbo.TBL_QuerySet SET LastFailure = GETDATE(), LastErrorMessage = left('Run from async Queue, ID: ' + cast(@QueueToken as varchar(50)) + @execErrorMessage,2048)  WHERE BatchID = @QuerySetQueryID
						END
					end catch
					
					/* record stats for the executed query */
					EXECUTE PROC_AsynchronousQueryQueueLog_CaptureExecution_NA @QueueToken
					
					/* if from a queryset, update the queryset entry and perform other queryset tasks as required */
					IF NOT @QuerySetQueryID IS NULL
						BEGIN
						IF @execErrorNumber IS NULL
							BEGIN
							UPDATE NFDT.dbo.TBL_QuerySet SET LastSuccess = GETDATE() WHERE BatchID = @QuerySetQueryID
							END
						ELSE
							BEGIN
							UPDATE NFDT.dbo.TBL_QuerySetList SET LastFailure = GETDATE() WHERE SetID = (Select SetID FROM NFDT.dbo.TBL_QuerySet with (nolock) WHERE BatchID = @QuerySetQueryID)
						END	
					END
					
					IF NOT @SourceLogID IS NULL
						BEGIN
						UPDATE NFDT.dbo.TBL_QuerySetExecutionLog SET StatementCountCompleted = StatementCountCompleted + 1 WHERE LogID = @SourceLogID /* update the execution Log */
						EXECUTE NFDT.dbo.PROC_QuerySetExecutionLog_ReviewLog_NA @QueryLogID = @SourceLogID /* look for errors and clean up as required. */
					END
					
						
				end 
			else if (@messageTypeName = N'http://schemas.microsoft.com/SQL/ServiceBroker/EndDialog')  /* Don't care about the enddialog */
					begin
					WAITFOR DELAY '00:00:01'
					 /* don't need to close, because it was already closed as part of the processing.  end conversation @ConversationHandle*/
				end
			else if (@messageTypeName = N'http://schemas.microsoft.com/SQL/ServiceBroker/Error') /* service broker error message. record it */
				begin
					declare @ErrorNumber int
					declare @errorMessage nvarchar(4000)
					select @xmlBody = CAST(@messageBody as xml);
					with xmlnamespaces (DEFAULT N'http://schemas.microsoft.com/SQL/ServiceBroker/Error')
					select @ErrorNumber = @xmlBody.value ('(/Error/Code)[1]', 'INT'), @errorMessage = @xmlBody.value ('(/Error/Description)[1]', 'NVARCHAR(4000)')
		
					update NFDT.dbo.TBL_AsynchronousQueryQueueLog set
						ErrorNumber = @ErrorNumber
						,ErrorMessage = @errorMessage
						where EntryID = @QueueToken
						                                                                                                                  
					/* if from a queryset, update the queryset entry */
					IF NOT @QuerySetQueryID IS NULL
						BEGIN
						UPDATE NFDT.dbo.TBL_QuerySet SET LastFailure = GETDATE() WHERE BatchID = @QuerySetQueryID
					END
						
					end conversation @ConversationHandle
				 end
			else
				begin
				raiserror(N'Received unexpected message type: %s', 16, 50, @messageTypeName) /* message type was entered that was not expected */
				end conversation @ConversationHandle
			end
		end
	end try
	begin catch
		declare @error int
		declare @message varchar(2048)
		select @error = ERROR_NUMBER()
			, @message = ERROR_MESSAGE()

		raiserror(N'Error: %i, %s', 1, 60,  @error, @message) with log /* raise error mesasge for server to handle */
	end catch
end




GO
/****** Object:  StoredProcedure [dbo].[PROC_Comments_Update_NA]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*************************************************************************
	Procedure Name: PROC_Comments_Update_NA
	Returns:		Failure Code (@ErrorCode)
	Primary User:	Everyone
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Tue Feb 16 17:22:23 CST 2016 @15 /Internet Time/
	Updated by:		Dave Elderkin
	Information:	Procedure for adding or updating a comment, and forwarding to NPS or another system
	************************************************************************
	Version:		1.00
	Date:			Tue Feb 16 17:22:23 CST 2016 @15 /Internet Time/
	Updated By:		Dave Elderkin
***************************************************************************/
CREATE Procedure [dbo].[PROC_Comments_Update_NA]
	@CommentID int = Null OUTPUT,
	@OwnerID int,
	@OwnerType varchar(10),
	@Comment varchar(max),
	@EnterpriseID varchar(10) = Null,
	@IsPrivate bit = 0,
	@UnitNumber varchar(7) = NULL,
	@ServiceOrderNumber varchar(8) = NULL,
	@Destination int = NULL,						/* destination 1 means NPS */
	@ErrorCode varchar(150) = NULL OUTPUT
AS
	SET NOCOUNT ON
	IF @OwnerType = '' /* set @OwnerType to null if empty string */
		SET @OwnerType = NULL
	IF @UnitNumber = '' /* set @OwnerType to null if empty string */
		SET @UnitNumber = NULL
	IF @ServiceOrderNumber = '' /* set @OwnerType to null if empty string */
		SET @ServiceOrderNumber = NULL
	IF @EnterpriseID = '' /* set @EnterpriseID to null if empty string */
		SET @EnterpriseID = NULL
	IF @CommentID = 0 /* set @CommentID to null if zero */
		SET @CommentID = NULL
	IF @OwnerID = 0 /* set @OwnerID to null if zero */
		SET @OwnerID = NULL
	IF @IsPrivate IS NULL /* set @IsPrivate to 0 if Null */
		SET @IsPrivate = 0
	
	/* check that everything is passed in */
	IF @OwnerType Is Null /* Return error code if @OwnerType is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for OwnerType, it is Required.'
		Return
	END
	IF @Comment Is Null /* Return error code if @Comment is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for Comment, it is Required.'
		Return
	END
	IF @OwnerID Is Null /* Return error code if @OwnerID is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for OwnerID, it is Required.'
		Return
	END
	IF @EnterpriseID Is Null /* Return error code if @EnterpriseID is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for EnterpriseID, it is Required.'
		Return
	END
	
	IF NOT @ServiceOrderNumber IS NULL
		SET @ServiceOrderNumber = ltrim(rtrim(right('00000000' + @ServiceOrderNumber,8)))
	
	IF NOT @UnitNumber IS NULL
		SET @UnitNumber = ltrim(rtrim(right('0000000' + @UnitNumber,7)))
	
	/* if a comment ID was passed in, update it. otherwise create a new row */
	IF @CommentID IS NULL /* add row */
		BEGIN
		INSERT INTO TBL_Comments (OwnerType, OwnerID, EnterpriseID, Comment, IsPrivate, CommentDate, UnitNumber, ServiceOrderNumber, Destination)
		VALUES(@OwnerType, @OwnerID, @EnterpriseID, @Comment, @IsPrivate, GETDATE(), @UnitNumber, @ServiceOrderNumber, @Destination)
		
		SET @CommentID = scope_identity()
		
		END		
	ELSE /* update row */
		BEGIN
		
		INSERT INTO TBL_CommentLog (CommentID, OriginalDate, UpdateDate, OriginalText, OriginalEnterpriseID, UpdateBy)
		SELECT CommentID, CommentDate, GETDATE(), Comment, EnterpriseID, @EnterpriseID FROM TBL_Comments WHERE CommentID = @CommentID
 		
		UPDATE TBL_Comments SET
			Comment = @Comment,
			IsPrivate = @IsPrivate,
			ModifiedEnterpriseID = @EnterpriseID,
			LastUpdate = GETDATE()
			WHERE CommentID = @CommentID
	END
	
	EXECUTE PROC_HashTag_ProcessText_NA @InputText = @Comment, @SourceOwnerType = 'Comment', @SourceOwnerID = @CommentID, @LDAPID = @EnterpriseID, @UpdateSourceData = 1
	
	IF @Destination & 1 = 1 AND NOT @ServiceOrderNumber = '' AND NOT @UnitNumber = '' /* if the bitmask contains 1, then send to NPS */
		BEGIN
		DECLARE @RemoteID int
		SET @Comment = left(@Comment,1980) + '...' + @EnterpriseID /* nps insert only supports 2000 chars and 1980 was a great year */
		
		
		EXECUTE HFDVXSQL3.NPSTemp.dbo.PROSOMessages_Insert_NA
		
			@UnitNumber = @UnitNumber
			,@ServiceOrder = @ServiceOrderNumber
			,@MessageString = @Comment
			,@EnterpriseID = @EnterpriseID
			,@MessID = @RemoteID OUTPUT
			,@ErrorCode = @ErrorCode OUTPUT
			
		IF NOT @RemoteID IS NULL
			BEGIN
			INSERT INTO TBL_CommentForwardLog (CommentID, Destination, DestinationID, Status, StatusDate)
			VALUES(@CommentID, 1, @RemoteID, 0, getdate())
			END 
		ELSE
			BEGIN
			INSERT INTO TBL_CommentForwardLog (CommentID, Destination, Status, StatusDate)
			VALUES(@CommentID, 1, -1, getdate())
		END 
			
	END
	


GO
/****** Object:  StoredProcedure [dbo].[PROC_CrossReference_Update_NA]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*************************************************************************
	Procedure Name: PROC_CrossReference_Update_NA
	Returns:		Failure Code (@ErrorCode)
	Primary User:	PD Team
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Mon Oct 26 17:01:09 CDT 2015 @959 /Internet Time/
	Updated by:		Dave Elderkin
	Information:	Procedure for updating data in the cross reference table
	************************************************************************
	Version:		1.00
	Date:			Fri Jan 04 13:59:03 CST 2013 @874 /Internet Time/
	Updated By:		Dave Elderkin
	************************************************************************
	Version:		1.00
	Date:			Mon Oct 26 17:01:09 CDT 2015 @959 /Internet Time/
	Updated By:		Dave Elderkin
	Changes:		Added additional columns to match to lookup table
	Documentation:	http://inhome.searshc.com/nfd/forms/view.asp?SubmittedFormID=615488
***************************************************************************/
CREATE Procedure [dbo].[PROC_CrossReference_Update_NA]
	@OwnerType varchar(10) = NULL,
	@OwnerID int = NULL,
	@RelatedType varchar(10) = NULL,
	@RelatedID int = NULL,
	@Note varchar(250) = NULL,
	@EmpID varchar(10) = NULL,
	@SortOrder int = NULL,
	@AncillaryShort varchar(50) = NULL,
	@AncillaryNumeric int = NULL,
	@Delete Bit = NULL,
	@ErrorCode varchar(150) = NULL OUTPUT
AS
	SET NOCOUNT ON  
	SET @ErrorCode = NULL
	IF @OwnerType = '' /* set @OwnerType to NULL if empty string */
		SET @OwnerType = NULL
	IF @OwnerID = 0 /* set @OwnerID to NULL if zero */
		SET @OwnerID = NULL
	IF @RelatedType = '' /* set @RelatedType to NULL if empty string */
		SET @RelatedType = NULL
	IF @RelatedID = 0 /* set @RelatedID to NULL if zero */
		SET @RelatedID = NULL
	IF @Note = '' /* set @Note to NULL if empty string */
		SET @Note = NULL
	IF @AncillaryShort = '' /* set @AncillaryShort to NULL if empty string */
		SET @AncillaryShort = NULL
	IF @SortOrder = 0 /* set @SortOrder to NULL if zero */
		SET @SortOrder = NULL
	IF @AncillaryNumeric = 0 /* set @AncillaryNumeric to NULL if zero */
		SET @AncillaryNumeric = NULL	

	IF @OwnerType Is NULL /* Return error code if @OwnerType is NULL */
	 	Begin
		SET @ErrorCode = 'Please enter a value for OwnerType, it is Required.'
		Return
	END
	IF @OwnerID Is NULL /* Return error code if @OwnerID is NULL */
	 	Begin
		SET @ErrorCode = 'Please enter a value for OwnerID, it is Required.'
		Return
	END
	IF @RelatedType Is NULL /* Return error code if @RelatedType is NULL */
	 	Begin
		SET @ErrorCode = 'Please enter a value for RelatedType, it is Required.'
		Return
	END
	IF @RelatedID Is NULL /* Return error code if @RelatedID is NULL */
	 	Begin
		SET @ErrorCode = 'Please enter a value for RelatedID, it is Required.'
		Return
	END
	IF @EmpID Is NULL /* Return error code if @EmpID is NULL */
	 	Begin
		SET @ErrorCode = 'Please enter a value for EmpID, it is Required.'
		Return
	END
	IF NOT @Delete = 1 /* if delete is not one, then it is zero */
		SET @Delete = 0
		

	/* if deleteing, remove row */
	IF @Delete = 1
		BEGIN
		DELETE FROM TBL_CrossReference WHERE OwnerType = @OwnerType AND OwnerID = @OwnerID AND RelatedType = @RelatedType AND RelatedID = @RelatedID
		RETURN
	END
	
	
	/* check to see if there is a row. if there is, update it. otherwize, add a new one */
	If (Select count(*) from TBL_CrossReference WHERE OwnerType = @OwnerType AND OwnerID = @OwnerID AND RelatedType = @RelatedType AND RelatedID = @RelatedID) = 0
		BEGIN
		INSERT INTO TBL_CrossReference (OwnerType, OwnerID, RelatedType, RelatedID, Note, SortOrder, AncillaryShort, AncillaryNumeric, LastUpdateBy, LastUpdate)
		
		
		VALUES(@OwnerType, @OwnerID, @RelatedType, @RelatedID, @Note, @SortOrder, @AncillaryShort, @AncillaryNumeric, @EmpID, GETDATE())
		END
	ELSE
		BEGIN
		UPDATE TBL_CrossReference SET
			LastUpdateBy = @EmpID,
			SortOrder = @SortOrder,
			AncillaryShort = @AncillaryShort,
			AncillaryNumeric = @AncillaryNumeric,
			LastUpdate = GETDATE()
			WHERE OwnerType = @OwnerType AND OwnerID = @OwnerID AND RelatedType = @RelatedType AND RelatedID = @RelatedID
	END
	/* check for correct insert or update*/
	If NOT @@Rowcount > 0
		SET @ErrorCode = 'Failed to add to table'

GO
/****** Object:  StoredProcedure [dbo].[PROC_EndPointAddUser]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


Create PROCEDURE .[dbo].[PROC_EndPointAddUser]
    @endPointID INT = NULL,
    @userName VARCHAR(20), 
    @password VARCHAR(50),
    @business VARCHAR(100) = NULL, 
    @businessOwnerEnterpriseID VARCHAR(10) = NULL, 
    @responseMessage NVARCHAR(250) OUTPUT
AS
BEGIN
    SET NOCOUNT ON

    DECLARE @salt UNIQUEIDENTIFIER=NEWID()
    BEGIN TRY

        INSERT INTO NFDT.dbo.TBL_EndPointUsers (EndPointID, UserName, PasswordHash, Salt, Business, BusinessOwnerEnterpriseID, CreateDate)
        VALUES(@endPointID, @userName, HASHBYTES('SHA2_512', @password+CAST(@salt AS NVARCHAR(36))), @salt, @business, @businessOwnerEnterpriseID, GETDATE())

       SET @responseMessage='Success'

    END TRY
    BEGIN CATCH
        SET @responseMessage=ERROR_MESSAGE() 
    END CATCH

END
	
GO
/****** Object:  StoredProcedure [dbo].[PROC_EndPointLoginCheck]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[PROC_EndPointLoginCheck]
    @userName VARCHAR(20), 
    @password VARCHAR(50),
    @responseMessage NVARCHAR(250)='' OUTPUT
AS
BEGIN

    SET NOCOUNT ON

    DECLARE @userID INT

    IF EXISTS (SELECT TOP 1 EndPointUserID FROM NFDT.dbo.TBL_EndPointUsers WHERE UserName=@userName)
    BEGIN
        SET @userID=(SELECT EndPointUserID FROM NFDT.dbo.TBL_EndPointUsers WHERE UserName=@userName AND PasswordHash=HASHBYTES('SHA2_512', @password+CAST(Salt AS NVARCHAR(36))))

       IF(@userID IS NULL)
           SET @responseMessage='Incorrect password'
       ELSE 
           SET @responseMessage='User successfully logged in'
    END
    ELSE
       SET @responseMessage='Invalid login'

END
GO
/****** Object:  StoredProcedure [dbo].[PROC_GroupDefinitions_Update_NA]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*************************************************************************
	Procedure Name: PROC_GroupDefinitions_Update_NA
	Returns:		Failure Code (@ErrorCode)
	Primary User:	Everyone
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Fri Nov 06 14:43:50 CST 2015 @905 /Internet Time/
	Updated by:		Dave Elderkin
	Information:	Procedure for Adding/Modifying Articles
	************************************************************************
	Version:		1.00
	Date:			Fri Nov 06 14:43:50 CST 2015 @905 /Internet Time/
	Updated By:		Dave Elderkin
***************************************************************************/
CREATE Procedure [dbo].[PROC_GroupDefinitions_Update_NA]
	@GroupID int = Null OUTPUT,
	@GroupType int = Null,
	@GroupName varchar (100) = Null,
	@GroupDescription varchar (MAX) = Null,
	@OwnerEnterpriseID varchar (10) = Null,
	@StatusCode int = Null,
	@ErrorCode varchar(150) = Null OUTPUT
AS
	SET NOCOUNT ON
	SET @ErrorCode = NULL
	IF @GroupID = 0 /*set @GroupID to null if empty string */
		SET @GroupID = NULL
	IF @GroupType = 0 /*set @GroupType to null if empty string */
		SET @GroupType = NULL
	IF @StatusCode = NULL /*set @GroupType to null if empty string */
		SET @StatusCode = 0
	IF @GroupName = '' /* set @GroupName to null if empty string */
		SET @GroupName = NULL
	IF @GroupDescription = '' /* set @GroupDescription to null if empty string */
		SET @GroupDescription = NULL
	IF @OwnerEnterpriseID = '' /* set @OwnerEnterpriseID to null if empty string */
		SET @OwnerEnterpriseID = NULL

 	IF @GroupName Is Null /* Return error code if @GroupName is null */
	 	Begin
		SET @ErrorCode = 'A GroupName was not assigned, it is Required.'
		Return
	END 
	 IF @GroupDescription Is Null /* Return error code if @GroupDescription is null */
	 	Begin
		SET @ErrorCode = 'A GroupDescription was not assigned, it is Required.'
		Return
	END 
	IF @OwnerEnterpriseID Is Null /* Return error code if @OwnerEnterpriseID is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for OwnerEnterpriseID, it is Required.'
		Return
	END
	

	/* check to see if this group exists. if so update, else add new */
	IF @GroupID Is Null
		BEGIN
		INSERT INTO TBL_GroupDefinitions (GroupType, GroupName, GroupDescription, GroupOwnerEnterpriseID, StatusCode, LastUpdate)
		VALUES (@GroupType, @GroupName, @GroupDescription, @OwnerEnterpriseID, @StatusCode, getdate())
		SET @GroupID = @@IDENTITY
		END
	ELSE
		BEGIN
		UPDATE TBL_GroupDefinitions SET
		GroupType = @GroupType
		,GroupName = @GroupName
		,GroupDescription = @GroupDescription
		,GroupOwnerEnterpriseID = @OwnerEnterpriseID
		,StatusCode = @StatusCode
		,LastUpdate = getdate()
		where GroupID = @GroupID
	END
	





GO
/****** Object:  StoredProcedure [dbo].[PROC_HashTag_ProcessText_NA]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*************************************************************************
	Procedure Name: PROC_HashTag_ProcessText_NA
	Returns:		Failure Code (@ErrorCode)
	Primary User:	Everyone
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Tue Feb 11 17:04:35 CST 2014 @3 /Internet Time/
	Updated by:		Dave Elderkin
	Information:	processes input text for hash tags
	************************************************************************
	Version:		1.00
	Date:			Tue Feb 11 17:04:35 CST 2014 @3 /Internet Time/
	Updated By:		Dave Elderkin
***************************************************************************/
CREATE PROCEDURE [dbo].[PROC_HashTag_ProcessText_NA]
	@InputText 				varchar(max) = NULL, 	/* big 'ol mess of text to process for hash tags */
	@SourceOwnerType		varchar(10) = NULL,		/* the data source that this text is from */
	@SourceOwnerID			int = NULL,				/* the data source ID that this text is from */
	@LDAPID					varchar(10) = NULL,		/* the LDAP ID that is creating this text */
	@UpdateSourceData		bit = 0,				/* This will overwrite the search entries for the entires for @SourceOwnerType and @SourceOwnerID if sent to 1 */
	@NewTagsProcessed		Int = null OUTPUT,				/* Count of new tags entered */
	@TotalTagsProcessed		Int = null OUTPUT,				/* Count of tags found */
	@ErrorCode 				varchar(150) = NULL OUTPUT
AS
	SET NOCOUNT ON
	SET @ErrorCode = NULL
	
	DECLARE @StrLine VARCHAR(max)
	DECLARE @ValueLength BIGINT
	DECLARE @StartPos BIGINT
	DECLARE @InsertCount int
	DECLARE @Separator char(1)
	DECLARE @TempTable TABLE(RowID INT IDENTITY(1,1), SeperatedValue VARCHAR(140), ValueLength INT, OwnerID INT NULL, OwnerType varchar(10) NULL)
	
	
	IF @InputText = ''
		SET @InputText = NULL
	IF @LDAPID = ''
		SET @LDAPID = NULL
	IF @SourceOwnerID = 0
		SET @SourceOwnerID = NULL

	SET @Separator = ' ' /* hard coded seperator of a space */
	SET @ValueLength = 1
	SET @StartPos = 1
	SET @InsertCount = 0
	SET @NewTagsProcessed = 0
	SET @TotalTagsProcessed = 0
	
	/* clean up the formatting of the input text before searching for tags */
	SET @InputText = ' ' + @InputText + ' '
	SET @InputText = replace(@InputText,'.',' ')
	SET @InputText = replace(@InputText,'?',' ')
	SET @InputText = replace(@InputText,'!',' ')
	SET @InputText = replace(@InputText,'<br>',' ')
	SET @InputText = replace(@InputText,'</a>',' ')
	SET @InputText = replace(@InputText,'</ br>',' ')
	SET @InputText = replace(@InputText,char(13),' ')
	SET @InputText = replace(@InputText,char(10),' ')
	
	WHILE (@StartPos < DATALENGTH(@InputText) + 1) BEGIN /* do until end of data */
		SET @ValueLength = CHARINDEX(@Separator, SUBSTRING(@InputText, @StartPos, DATALENGTH(@InputText)), 1)
		IF @ValueLength = 0 
			SET @ValueLength = DATALENGTH(@InputText) - @StartPos + 1
		SET @StrLine = SUBSTRING(SUBSTRING(@InputText, @StartPos, DATALENGTH(@InputText)), 1, @ValueLength)
		SET @StrLine = REPLACE(@StrLine,@Separator,'')
		IF @ValueLength > 3 AND @ValueLength <= 140 AND SUBSTRING(@StrLine,1,1) IN ('#','@') /*  valid hash tags of # for now, add @ later, probably as LDAP ID. */
			BEGIN
			SET @InsertCount = @InsertCount + 1
			INSERT INTO @TempTable(SeperatedValue, ValueLength, OwnerID, OwnerType) 
			VALUES(@StrLine, LEN(@StrLine),@SourceOwnerID, @SourceOwnerType)
		END
		SET @StartPos = @StartPos + @ValueLength
	END
	IF @InsertCount = 0
		RETURN
	
	

		
	/* delete data from the temp table that isn't a hash tag */
	DELETE FROM @TempTable WHERE dbo.KeepOnlyChars(SUBSTRING(SeperatedValue,2,1),'abcdefghijklmnopqrstuvwxyz') = '' 								/* drop any tags that don't start with a letter */
	DELETE FROM @TempTable WHERE NOT dbo.KeepOnlyChars(SeperatedValue,'abcdefghijklmnopqrstuvwxyz1234567890_#') = SeperatedValue 					/* drop any tags that have bad characters in them like punctuation.*/
	--DELETE FROM @TempTable WHERE SUBSTRING(SeperatedValue,1,1) = '@' and ValueLength > 8 															/* drop any @ tags that are over the length of an enterprise id */
	--DELETE FROM @TempTable WHERE SUBSTRING(SeperatedValue,1,1) = '@' and charindex('.',SeperatedValue) > 0										/* drop any @ tags that look like an email address with a space in it (meaning they have a period in them) */
	
	
	SET @TotalTagsProcessed = (select count(distinct SeperatedValue) from @TempTable)
	
	/* insert new tags to the tag table */
	INSERT INTO TBL_HashTag (Tag, Type, FirstUseEnterpriseID)
	SELECT distinct right(SeperatedValue,len(SeperatedValue)-1) as tag, left(SeperatedValue,1) as type , @LDAPID 
	FROM
		@TempTable as t LEFT OUTER JOIN TBL_HashTag ON SeperatedValue = Type + Tag
	WHERE
		Tag IS NULL


	SET @NewTagsProcessed = @NewTagsProcessed + @@ROWCOUNT
	
	
	/* If the owner info was passed in, record it in the search table */
	IF NOT @SourceOwnerType IS NULL AND NOT @SourceOwnerID IS NULL
		BEGIN
		IF @UpdateSourceData = 1
			BEGIN
			DELETE FROM TBL_HashTagInstances WHERE OwnerID = @SourceOwnerID AND OwnerType = @SourceOwnerType
		END
		INSERT INTO TBL_HashTagInstances(Tag, Type, OwnerID, OwnerType)
		SELECT distinct right(SeperatedValue,len(SeperatedValue)-1) as tag, LEFT(SeperatedValue,1) as type, @SourceOwnerID, @SourceOwnerType
		FROM
			@TempTable as t LEFT OUTER JOIN TBL_HashTagInstances HTI ON SeperatedValue = Type + Tag AND HTI.OwnerID = t.OwnerID AND HTI.OwnerType = t.OwnerType
		WHERE
			Tag IS NULL
	END


GO
/****** Object:  StoredProcedure [dbo].[PROC_HashTag_Update_NA]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*************************************************************************
	Procedure Name: PROC_HashTag_Update_NA
	Returns:		Failure Code (@ErrorCode)
	Primary User:	website
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Tue Jun 18 13:34:42 CDT 2013 @815 /Internet Time/
	Updated by:		Dave Elderkin
	Information:	Procedure for updating data in the hashtag table
	************************************************************************
	Version:		1.00
	Date:			Tue Jun 18 13:34:42 CDT 2013 @815 /Internet Time/
	Updated By:		Dave Elderkin
***************************************************************************/
CREATE Procedure [dbo].[PROC_HashTag_Update_NA]
	@HashTag varchar(141) = NULL,
	@LinkView varchar(MAX) = NULL,
	@LinkSearch varchar(MAX) = NULL,
	@Description varchar(MAX) = NULL,
	@OwnerType varchar(10) = NULL,
	@OwnerID int = NULL,
	@EmpID varchar(10) = NULL,
	@ErrorCode varchar(150) = NULL OUTPUT

AS


	SET NOCOUNT ON 
	SET @ErrorCode = NULL
	
	IF @HashTag = '' /* set @Title to null if empty string */
		SET @HashTag = NULL
	IF @LinkView = '' /* set @LinkView to null if empty string */
		SET @LinkView = NULL
	IF @LinkSearch = '' /* set @LinkSearch to null if empty string */
		SET @LinkSearch = NULL
	IF @Description = '' /* set @Description to null if empty string */
		SET @Description = NULL
	IF @OwnerType = '' /* set @OwnerType to null if empty string */
		SET @OwnerType = NULL
	IF @EmpID = '' /* set @EmpID to null if empty string */
		SET @EmpID = NULL
	IF @OwnerID = 0 /* set @OwnerID to null if zero */
		SET @OwnerID = NULL

	IF @HashTag IS NULL
		BEGIN
		SET @ErrorCode = 'Hash Tag Required'
		RETURN
	END
	
	IF len(@HashTag) < 3 
		BEGIN
		SET @ErrorCode = 'Hash Tag Invalid'
		RETURN
	END
	
	IF NOT LEFT(@HashTag, 1) in ('#', '@')
		BEGIN
		SET @ErrorCode = 'Hash Tag Invalid'
		RETURN
	END
		
	/* check to see if there is a row. if there is, update it. otherwize, add a new one */
	If (Select count(*) from TBL_HashTag WHERE Type + Tag = @HashTag) = 0
		BEGIN
		INSERT INTO TBL_HashTag (Tag, Type, LinkView, LinkSearch, Description, OwnerType, OwnerID, FirstUseLDAP, UpdatedByLDAP, UpdatedDate)
		VALUES(right(@HashTag,len(@HashTag)-1),left(@HashTag,1), @LinkView, @LinkSearch, @Description, @OwnerType, @OwnerID, @EmpID, @EmpID, GETDATE())
		END
	ELSE
		BEGIN
		UPDATE TBL_HashTag SET
			Tag = right(@HashTag,len(@HashTag)-1),
			Type	= left(@HashTag,1),
			LinkView	= @LinkView,
			LinkSearch= @LinkSearch,
			Description= @Description,
			OwnerType= @OwnerType,
			OwnerID = @OwnerID,
			UpdatedByLDAP = @EmpID,
			UpdatedDate = GETDATE()
			WHERE Type + Tag = @HashTag
	END

		
	/* check for correct insert or update*/
	If NOT @@Rowcount > 0
		SET @ErrorCode = 'Failed to add/update to table'


GO
/****** Object:  StoredProcedure [dbo].[PROC_InOrderAsyncQueryExecQueue01_Insert]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*************************************************************************
	Procedure Name: Proc_InOrderAsyncQueryExecQueue01_Insert
	Returns:		Failure Code (@ErrorCode)
	Primary User:	Admins
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Thu Apr 25 17:29:30 CDT 2013 @978 /Internet Time/
	Documentation:	
	Information:	Procedure that adds a SQL Statement to run async.
	************************************************************************
	Version:		1.00
	Date:			Thu Apr 25 17:29:30 CDT 2013 @978 /Internet Time/
	Updated By:		Dave Elderkin
***************************************************************************/
CREATE procedure [dbo].[PROC_InOrderAsyncQueryExecQueue01_Insert]
	@SQLStatement varchar(max)
	,@Source  varchar(50) 							/* source system identifier for reporting and such */
	,@SourceID int = NULL							/* source ID that would uniquely identify an element in the source */
	,@SourceLogID int = NULL						/* if the source has a log system, this is the ID number entry for that log. */
	,@QueueToken uniqueidentifier = NULL output		/* the message token returned from the queue.  can be used to look up results. */ 
	,@ErrorCode varchar(150) = NULL output			/* this insert process does basic checking, such as valid tsql syntax, and will return an error message if there is one */
as
begin
	DECLARE @ConversationHandle uniqueidentifier
	DECLARE @TestSQL varchar(max)
	DECLARE @xmlBody xml
	DECLARE @trancount int
	set NOCOUNT on


	
	
	/* check if a transaction is open.  If not, open one.  If already open, create a save point */
	set @trancount = @@trancount
	if @trancount = 0
		begin transaction
	else
		save transaction QueueInsert
		
	/* create a logging comment inside the SQL statement to it is easily found if researching executing queries */
	SET @SQLStatement = '/* Source: ' + isnull(@Source, 'Unassigned') + ' ID: ' + isnull(cast(@SourceID as varchar(20)),'Unassigned') + '. Submitted ' + convert(varchar,getdate(),113) + ' */' + char(13) + char(10) + @SQLStatement
	
		
	/* create conversation and insert into log table */
	begin try
		begin dialog conversation @ConversationHandle
			from service [InOrderAsyncQueryExecQueueService01]
			to service N'InOrderAsyncQueryExecQueueService01', 'current database'
			with encryption = off
			
		/* send the SQL statement as a message in the dialog.  Doesn't matter what is sent acutally, it isn't used later. */	
		SET @QueueToken = (SELECT [conversation_id] from sys.conversation_endpoints where [conversation_handle] = @ConversationHandle)
		SET @xmlBody = (select @SQLStatement as [statement] for xml path('statements'), type)
		;send on conversation @ConversationHandle (@xmlBody);
		
		
		insert into nfdt.dbo.TBL_AsynchronousQueryQueueLog(EntryID, SubmitDate, Statement, Source, SourceID, SourceLogID)
		values (@QueueToken, GETDATE(), @SQLStatement, @Source, @SourceID, @SourceLogID)
			
		if @trancount = 0 /* clsoe the transaction if we started the transaction above */
			commit
	end try
	
	begin catch
		DECLARE @Error int
		DECLARE @ErrorMessage nvarchar(2048)
		DECLARE @XactState smallint
		SET @Error = ERROR_NUMBER()
		SET @ErrorMessage = ERROR_MESSAGE()
		SET @XactState = XACT_STATE()
			
		/* determiune if we are rolling back this transaction, or just to the save point */
		if @XactState = -1
			rollback
		if @XactState = 1 and @trancount = 0
			rollback
		if @XactState = 1 and @trancount > 0
			rollback transaction QueueInsert
		SET @ErrorCode = 'Error: ' + left(@ErrorMessage,140)
	end catch
	
end


GO
/****** Object:  StoredProcedure [dbo].[PROC_InOrderAsyncQueryExecQueue02_Insert]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





/*************************************************************************
	Procedure Name: Proc_InOrderAsyncQueryExecQueue02_Insert
	Returns:		Failure Code (@ErrorCode)
	Primary User:	Admins
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Thu Apr 25 17:29:30 CDT 2013 @978 /Internet Time/
	Documentation:	
	Information:	Procedure that adds a SQL Statement to run async.
	************************************************************************
	Version:		1.00
	Date:			Thu Apr 25 17:29:30 CDT 2013 @978 /Internet Time/
	Updated By:		Dave Elderkin
***************************************************************************/
CREATE procedure [dbo].[PROC_InOrderAsyncQueryExecQueue02_Insert]
	@SQLStatement varchar(max)
	,@Source  varchar(50) 							/* source system identifier for reporting and such */
	,@SourceID int = NULL							/* source ID that would uniquely identify an element in the source */
	,@SourceLogID int = NULL						/* if the source has a log system, this is the ID number entry for that log. */
	,@QueueToken uniqueidentifier = NULL output		/* the message token returned from the queue.  can be used to look up results. */ 
	,@ErrorCode varchar(150) = NULL output			/* this insert process does basic checking, such as valid tsql syntax, and will return an error message if there is one */
as
begin
	DECLARE @ConversationHandle uniqueidentifier
	DECLARE @TestSQL varchar(max)
	DECLARE @xmlBody xml
	DECLARE @trancount int
	set NOCOUNT on


	
	
	/* check if a transaction is open.  If not, open one.  If already open, create a save point */
	set @trancount = @@trancount
	if @trancount = 0
		begin transaction
	else
		save transaction QueueInsert
		
	/* create a logging comment inside the SQL statement to it is easily found if researching executing queries */
	SET @SQLStatement = '/* Source: ' + isnull(@Source, 'Unassigned') + ' ID: ' + isnull(cast(@SourceID as varchar(20)),'Unassigned') + '. Submitted ' + convert(varchar,getdate(),113) + ' */' + char(13) + char(10) + @SQLStatement
	
	
	/* create conversation and insert into log table */
	begin try
		begin dialog conversation @ConversationHandle
			from service [InOrderAsyncQueryExecQueueService02]
			to service N'InOrderAsyncQueryExecQueueService02', 'current database'
			with encryption = off
			
		/* send the SQL statement as a message in the dialog.  Doesn't matter what is sent acutally, it isn't used later. */	
		SET @QueueToken = (SELECT [conversation_id] from sys.conversation_endpoints where [conversation_handle] = @ConversationHandle)
		SET @xmlBody = (select @SQLStatement as [statement] for xml path('statements'), type)
		;send on conversation @ConversationHandle (@xmlBody);
		
		
		insert into nfdt.dbo.TBL_AsynchronousQueryQueueLog(EntryID, SubmitDate, Statement, Source, SourceID, SourceLogID)
		values (@QueueToken, GETDATE(), @SQLStatement, @Source, @SourceID, @SourceLogID)
			
		if @trancount = 0 /* clsoe the transaction if we started the transaction above */
			commit
	end try
	
	begin catch
		DECLARE @Error int
		DECLARE @ErrorMessage nvarchar(2048)
		DECLARE @XactState smallint
		SET @Error = ERROR_NUMBER()
		SET @ErrorMessage = ERROR_MESSAGE()
		SET @XactState = XACT_STATE()
			
		/* determiune if we are rolling back this transaction, or just to the save point */
		if @XactState = -1
			rollback
		if @XactState = 1 and @trancount = 0
			rollback
		if @XactState = 1 and @trancount > 0
			rollback transaction QueueInsert
		SET @ErrorCode = 'Error: ' + left(@ErrorMessage,140)
	end catch
	
end


GO
/****** Object:  StoredProcedure [dbo].[PROC_InOrderAsyncQueryExecQueue03_Insert]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*************************************************************************
	Procedure Name: Proc_InOrderAsyncQueryExecQueue03_Insert
	Returns:		Failure Code (@ErrorCode)
	Primary User:	Admins
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Thu Apr 25 17:29:30 CDT 2013 @978 /Internet Time/
	Documentation:	
	Information:	Procedure that adds a SQL Statement to run async.
	************************************************************************
	Version:		1.00
	Date:			Thu Apr 25 17:29:30 CDT 2013 @978 /Internet Time/
	Updated By:		Dave Elderkin
***************************************************************************/
CREATE procedure [dbo].[PROC_InOrderAsyncQueryExecQueue03_Insert]
	@SQLStatement varchar(max)
	,@Source  varchar(50) 							/* source system identifier for reporting and such */
	,@SourceID int = NULL							/* source ID that would uniquely identify an element in the source */
	,@SourceLogID int = NULL						/* if the source has a log system, this is the ID number entry for that log. */
	,@QueueToken uniqueidentifier = NULL output		/* the message token returned from the queue.  can be used to look up results. */ 
	,@ErrorCode varchar(150) = NULL output			/* this insert process does basic checking, such as valid tsql syntax, and will return an error message if there is one */
as
begin
	DECLARE @ConversationHandle uniqueidentifier
	DECLARE @TestSQL varchar(max)
	DECLARE @xmlBody xml
	DECLARE @trancount int
	set NOCOUNT on


	
	
	/* check if a transaction is open.  If not, open one.  If already open, create a save point */
	set @trancount = @@trancount
	if @trancount = 0
		begin transaction
	else
		save transaction QueueInsert
		
	/* create a logging comment inside the SQL statement to it is easily found if researching executing queries */
	SET @SQLStatement = '/* Source: ' + isnull(@Source, 'Unassigned') + ' ID: ' + isnull(cast(@SourceID as varchar(20)),'Unassigned') + '. Submitted ' + convert(varchar,getdate(),113) + ' */' + char(13) + char(10) + @SQLStatement
	
		
	/* create conversation and insert into log table */
	begin try
		begin dialog conversation @ConversationHandle
			from service [InOrderAsyncQueryExecQueueService03]
			to service N'InOrderAsyncQueryExecQueueService03', 'current database'
			with encryption = off
			
		/* send the SQL statement as a message in the dialog.  Doesn't matter what is sent acutally, it isn't used later. */	
		SET @QueueToken = (SELECT [conversation_id] from sys.conversation_endpoints where [conversation_handle] = @ConversationHandle)
		SET @xmlBody = (select @SQLStatement as [statement] for xml path('statements'), type)
		;send on conversation @ConversationHandle (@xmlBody);
		
		
		insert into nfdt.dbo.TBL_AsynchronousQueryQueueLog(EntryID, SubmitDate, Statement, Source, SourceID, SourceLogID)
		values (@QueueToken, GETDATE(), @SQLStatement, @Source, @SourceID, @SourceLogID)
			
		if @trancount = 0 /* clsoe the transaction if we started the transaction above */
			commit
	end try
	
	begin catch
		DECLARE @Error int
		DECLARE @ErrorMessage nvarchar(2048)
		DECLARE @XactState smallint
		SET @Error = ERROR_NUMBER()
		SET @ErrorMessage = ERROR_MESSAGE()
		SET @XactState = XACT_STATE()
			
		/* determiune if we are rolling back this transaction, or just to the save point */
		if @XactState = -1
			rollback
		if @XactState = 1 and @trancount = 0
			rollback
		if @XactState = 1 and @trancount > 0
			rollback transaction QueueInsert
		SET @ErrorCode = 'Error: ' + left(@ErrorMessage,140)
	end catch
	
end


GO
/****** Object:  StoredProcedure [dbo].[PROC_InOrderAsyncQueryExecQueue04_Insert]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*************************************************************************
	Procedure Name: Proc_InOrderAsyncQueryExecQueue04_Insert
	Returns:		Failure Code (@ErrorCode)
	Primary User:	Admins
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Thu Apr 25 17:29:30 CDT 2013 @978 /Internet Time/
	Documentation:	
	Information:	Procedure that adds a SQL Statement to run async.
	************************************************************************
	Version:		1.00
	Date:			Thu Apr 25 17:29:30 CDT 2013 @978 /Internet Time/
	Updated By:		Dave Elderkin
***************************************************************************/
CREATE procedure [dbo].[PROC_InOrderAsyncQueryExecQueue04_Insert]
	@SQLStatement varchar(max)
	,@Source  varchar(50) 							/* source system identifier for reporting and such */
	,@SourceID int = NULL							/* source ID that would uniquely identify an element in the source */
	,@SourceLogID int = NULL						/* if the source has a log system, this is the ID number entry for that log. */
	,@QueueToken uniqueidentifier = NULL output		/* the message token returned from the queue.  can be used to look up results. */ 
	,@ErrorCode varchar(150) = NULL output			/* this insert process does basic checking, such as valid tsql syntax, and will return an error message if there is one */
as
begin
	DECLARE @ConversationHandle uniqueidentifier
	DECLARE @TestSQL varchar(max)
	DECLARE @xmlBody xml
	DECLARE @trancount int
	set NOCOUNT on


	
	
	/* check if a transaction is open.  If not, open one.  If already open, create a save point */
	set @trancount = @@trancount
	if @trancount = 0
		begin transaction
	else
		save transaction QueueInsert
		
	/* create a logging comment inside the SQL statement to it is easily found if researching executing queries */
	SET @SQLStatement = '/* Source: ' + isnull(@Source, 'Unassigned') + ' ID: ' + isnull(cast(@SourceID as varchar(20)),'Unassigned') + '. Submitted ' + convert(varchar,getdate(),113) + ' */' + char(13) + char(10) + @SQLStatement
	
		
	/* create conversation and insert into log table */
	begin try
		begin dialog conversation @ConversationHandle
			from service [InOrderAsyncQueryExecQueueService04]
			to service N'InOrderAsyncQueryExecQueueService04', 'current database'
			with encryption = off
			
		/* send the SQL statement as a message in the dialog.  Doesn't matter what is sent acutally, it isn't used later. */	
		SET @QueueToken = (SELECT [conversation_id] from sys.conversation_endpoints where [conversation_handle] = @ConversationHandle)
		SET @xmlBody = (select @SQLStatement as [statement] for xml path('statements'), type)
		;send on conversation @ConversationHandle (@xmlBody);
		
		
		insert into nfdt.dbo.TBL_AsynchronousQueryQueueLog(EntryID, SubmitDate, Statement, Source, SourceID, SourceLogID)
		values (@QueueToken, GETDATE(), @SQLStatement, @Source, @SourceID, @SourceLogID)
			
		if @trancount = 0 /* clsoe the transaction if we started the transaction above */
			commit
	end try
	
	begin catch
		DECLARE @Error int
		DECLARE @ErrorMessage nvarchar(2048)
		DECLARE @XactState smallint
		SET @Error = ERROR_NUMBER()
		SET @ErrorMessage = ERROR_MESSAGE()
		SET @XactState = XACT_STATE()
			
		/* determiune if we are rolling back this transaction, or just to the save point */
		if @XactState = -1
			rollback
		if @XactState = 1 and @trancount = 0
			rollback
		if @XactState = 1 and @trancount > 0
			rollback transaction QueueInsert
		SET @ErrorCode = 'Error: ' + left(@ErrorMessage,140)
	end catch
	
end


GO
/****** Object:  StoredProcedure [dbo].[PROC_InOrderAsyncQueryExecQueue05_Insert]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*************************************************************************
	Procedure Name: Proc_InOrderAsyncQueryExecQueue05_Insert
	Returns:		Failure Code (@ErrorCode)
	Primary User:	Admins
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Thu Apr 25 17:29:30 CDT 2013 @978 /Internet Time/
	Documentation:	
	Information:	Procedure that adds a SQL Statement to run async.
	************************************************************************
	Version:		1.00
	Date:			Thu Apr 25 17:29:30 CDT 2013 @978 /Internet Time/
	Updated By:		Dave Elderkin
***************************************************************************/
CREATE procedure [dbo].[PROC_InOrderAsyncQueryExecQueue05_Insert]
	@SQLStatement varchar(max)
	,@Source  varchar(50) 							/* source system identifier for reporting and such */
	,@SourceID int = NULL							/* source ID that would uniquely identify an element in the source */
	,@SourceLogID int = NULL						/* if the source has a log system, this is the ID number entry for that log. */
	,@QueueToken uniqueidentifier = NULL output		/* the message token returned from the queue.  can be used to look up results. */ 
	,@ErrorCode varchar(150) = NULL output			/* this insert process does basic checking, such as valid tsql syntax, and will return an error message if there is one */
as
begin
	DECLARE @ConversationHandle uniqueidentifier
	DECLARE @TestSQL varchar(max)
	DECLARE @xmlBody xml
	DECLARE @trancount int
	set NOCOUNT on


	
	
	/* check if a transaction is open.  If not, open one.  If already open, create a save point */
	set @trancount = @@trancount
	if @trancount = 0
		begin transaction
	else
		save transaction QueueInsert
		
	/* create a logging comment inside the SQL statement to it is easily found if researching executing queries */
	SET @SQLStatement = '/* Source: ' + isnull(@Source, 'Unassigned') + ' ID: ' + isnull(cast(@SourceID as varchar(20)),'Unassigned') + '. Submitted ' + convert(varchar,getdate(),113) + ' */' + char(13) + char(10) + @SQLStatement
	
		
	/* create conversation and insert into log table */
	begin try
		begin dialog conversation @ConversationHandle
			from service [InOrderAsyncQueryExecQueueService05]
			to service N'InOrderAsyncQueryExecQueueService05', 'current database'
			with encryption = off
			
		/* send the SQL statement as a message in the dialog.  Doesn't matter what is sent acutally, it isn't used later. */	
		SET @QueueToken = (SELECT [conversation_id] from sys.conversation_endpoints where [conversation_handle] = @ConversationHandle)
		SET @xmlBody = (select @SQLStatement as [statement] for xml path('statements'), type)
		;send on conversation @ConversationHandle (@xmlBody);
		
		
		insert into nfdt.dbo.TBL_AsynchronousQueryQueueLog(EntryID, SubmitDate, Statement, Source, SourceID, SourceLogID)
		values (@QueueToken, GETDATE(), @SQLStatement, @Source, @SourceID, @SourceLogID)
			
		if @trancount = 0 /* clsoe the transaction if we started the transaction above */
			commit
	end try
	
	begin catch
		DECLARE @Error int
		DECLARE @ErrorMessage nvarchar(2048)
		DECLARE @XactState smallint
		SET @Error = ERROR_NUMBER()
		SET @ErrorMessage = ERROR_MESSAGE()
		SET @XactState = XACT_STATE()
			
		/* determiune if we are rolling back this transaction, or just to the save point */
		if @XactState = -1
			rollback
		if @XactState = 1 and @trancount = 0
			rollback
		if @XactState = 1 and @trancount > 0
			rollback transaction QueueInsert
		SET @ErrorCode = 'Error: ' + left(@ErrorMessage,140)
	end catch
	
end


GO
/****** Object:  StoredProcedure [dbo].[PROC_QuerySet_Update_NA]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*************************************************************************
	Procedure Name: PROC_QuerySet_Update_NA
	Returns:		Failure Code (@ErrorCode)
	Primary User:	website
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Thu May 22 14:41:44 CDT 2014 @862 /Internet Time/
	Updated by:		Dave Elderkin
	Information:	Procedure for adding or updating the queryset
	************************************************************************
	Version:		1.00
	Date:			Thu May 22 14:41:44 CDT 2014 @862 /Internet Time/
	Updated By:		Dave Elderkin
	************************************************************************
	Version:		1.01
	Date:			Tue Feb 03 12:52:13 CST 2015 @827 /Internet Time/
	Updated By:		Dave Elderkin
***************************************************************************/
CREATE Procedure [dbo].[PROC_QuerySet_Update_NA]
	@ID int = Null OUTPUT,
	@SetID int = Null,
	@QueryString varchar(max) = NULL,
	@BatchName varchar(100) = Null,
	@Description varchar(max) = NULL,
	@EmployeeLDAPID varchar(10) = NULL,
	@IsConfidential bit = NULL,
	@IsEnabled bit = NULL,
	@SortOrder int = null,
	@ApplicationFormID INT = NULL,
	@NotifyIfOverRows int = NULL,
	@NotifyIfUnderRows int = NULL,
	@NotifyIfRowsOutsideDeviation decimal(9,4) = NULL,
	@ErrorCode varchar(150) = NULL OUTPUT
AS
	SET NOCOUNT ON
	IF @Description = '' /* set @Description to null if empty string */
		SET @Description = NULL
	IF @BatchName = '' /* set @Name to null if empty string */
		SET @BatchName = NULL
	IF @EmployeeLDAPID = '' /* set @EmployeeLDAPID to null if empty string */
		SET @EmployeeLDAPID = NULL
	SET @EmployeeLDAPID = UPPER(@EmployeeLDAPID)
	
	/* check that everything is passed in */
	IF @EmployeeLDAPID Is Null /* Return error code if @EmployeeLDAPID is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for EmployeeLDAPID, it is Required.'
		Return
	END
	
	IF @ID Is Null AND @SetID  IS NULL/* Return error code if @ID is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for ID or SetID, one is Required.'
		Return
	END
	
	IF @ID Is Null /* insert new record */
		BEGIN
			INSERT INTO TBL_QuerySet (SetID, QueryString, IsEnabled, SortOrder, BatchName, Description, LastUpdateBy, LastUpdate, IsConfidential, ApplicationFormID, NotifyIfOverRows, NotifyIfUnderRows, NotifyIfRowsOutsideDeviation)
			VALUES(@SetID, @QueryString, @IsEnabled, @SortOrder, @BatchName, @Description, @EmployeeLDAPID, getdate(), @IsConfidential, @ApplicationFormID, @NotifyIfOverRows, @NotifyIfUnderRows, @NotifyIfRowsOutsideDeviation)
			SET @ID = scope_identity()
		END
	ELSE /* update existing record */
		BEGIN
	
	
		/* insert existing row to the source control table */
		INSERT INTO TBL_ArchiveQuerySet (DateRecorded, BatchID, SetID, QueryString, LastRun, LastSuccess, LastFailure, IsEnabled, SortOrder, BatchName, Description, LastUpdateBy, LastUpdate, LastErrorMessage, IsConfidential)
		SELECT 
			GETDATE(), BatchID, SetID, QueryString, LastRun, LastSuccess, LastFailure, IsEnabled, SortOrder, BatchName, Description, LastUpdateBy, LastUpdate, LastErrorMessage, IsConfidential
		FROM
			nfdt.dbo.TBL_QuerySet
		WHERE
			BatchID = @ID
			
		/* only updates are allowed though this proc as of now.*/
		UPDATE TBL_QuerySet SET
			SetID = isnull(@SetID, SetID)
			,QueryString = isnull(@QueryString, QueryString)
			,IsEnabled = isnull(@IsEnabled, IsEnabled)
			,SortOrder = isnull(@SortOrder, SortOrder)
			,BatchName = isnull(@BatchName, BatchName)
			,Description = isnull(@Description, Description)
			,LastUpdateBy = @EmployeeLDAPID
			,LastUpdate = GETDATE()
			,IsConfidential = isnull(@IsConfidential, IsConfidential)
			,ApplicationFormID = isnull(@ApplicationFormID, ApplicationFormID)
			,NotifyIfOverRows = @NotifyIfOverRows
			,NotifyIfUnderRows = @NotifyIfUnderRows
			,NotifyIfRowsOutsideDeviation = @NotifyIfRowsOutsideDeviation
		WHERE BatchID = @ID
	END
		








GO
/****** Object:  StoredProcedure [dbo].[PROC_QuerySetExecutionLog_ReviewLog_NA]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*************************************************************************
	Procedure Name: PROC_QuerySetExecutionLog_ReviewLog_NA
	Returns:		Failure Code (@ErrorCode)
	Primary User:	website
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Thu Oct 24 11:17:35 CDT 2013 @720 /Internet Time/
	Updated by:		Dave Elderkin
	Information:	Procedure for reviewing a query set log for status or errors
	************************************************************************
	Version:		1.00
	Date:			Thu Oct 24 11:17:35 CDT 2013 @720 /Internet Time/
	Updated By:		Dave Elderkin
***************************************************************************/
CREATE Procedure [dbo].[PROC_QuerySetExecutionLog_ReviewLog_NA] 
	@QuerySetID int = NULL,									/* ID Number to review */
	@QueryLogID int = NULL,									/* ID Number to review */
	@ErrorCode varchar(150) = NULL OUTPUT
AS
	SET NOCOUNT ON
	DECLARE @NextQuerySet int
	DECLARE @StatementCountSubmitted int
	DECLARE @StatementCountCompleted int
	DECLARE @EmailTemplateSuccess INT
	DECLARE @EmailTemplateFailure INT
	DECLARE @EmailBody varchar(max)

	
	/* if the log ID is null, get the most recent for the query set and review that one. In general, use the log ID. */
	IF @QueryLogID IS NULL AND NOT @QuerySetID IS NULL
		BEGIN
		SET @QueryLogID = (SELECT MAX(LogID) FROM TBL_QuerySetExecutionLog with (nolock) WHERE SetID = @QuerySetID)
	END
	
	IF @QueryLogID IS NULL
		BEGIN
		SET @ErrorCode = 'Query Set Log Entry not found'
		RETURN
	END
	
	/* Get the set ID for this log entry */
	IF @QuerySetID IS NULL
		BEGIN
		SET @QuerySetID = (SELECT SetID FROM TBL_QuerySetExecutionLog with (nolock) WHERE LogID = @QueryLogID)
	END
	
	
	/* Check the log table for this querylog to see the current status of submitted vs completed */
	SELECT
		@StatementCountSubmitted = StatementCountSubmitted
		,@StatementCountCompleted = StatementCountCompleted
		,@QuerySetID = SetID
	FROM
		TBL_QuerySetExecutionLog with (nolock)
	WHERE 
		LogID = @QueryLogID
		
	/* get the email ID */
	IF NOT @QuerySetID IS NULL
		BEGIN
		SELECT
			@EmailTemplateSuccess = SuccessApplicationEmailID,
			@EmailTemplateFailure = FailureApplicationEmailID,
			@EmailBody = SetName
		FROM
			TBL_QuerySetList  with (nolock) 
		WHERE
			SetID = @QuerySetID
	END
	
	
	IF @StatementCountSubmitted = @StatementCountCompleted 
		BEGIN
		IF (SELECT count(*) FROM TBL_AsynchronousQueryQueueLog with (nolock) WHERE Source = 'Query Set' AND SourceLogID = @QueryLogID AND NOT ErrorMessage IS NULL ) > 0
			BEGIN
			UPDATE TBL_QuerySetExecutionLog SET Status = 'Error', EndDate = GETDATE() WHERE LogID = @QueryLogID
			UPDATE TBL_QuerySetList SET LastFailure = GETDATE(), Status = 'Scheduled' WHERE SetID = @QuerySetID
			
			
			IF NOT @EmailTemplateFailure IS NULL  /* send notify email */
				BEGIN
				SET  @EmailBody = 'A Statement Failed while being executed in query set ' + @EmailBody + '. Check Log for Details'
				EXECUTE PROC_SendApplicationEmail_NA @EmailID = @EmailTemplateFailure, @Subject = 'Query Set Execution Failure', @BodyHTML = @EmailBody
				END
			END
		ELSE IF (SELECT count(*) FROM TBL_AsynchronousQueryQueueLog with (nolock) WHERE Source = 'Query Set' AND SourceLogID = @QueryLogID AND ErrorMessage IS NULL ) <= @StatementCountCompleted
			BEGIN
			UPDATE TBL_QuerySetExecutionLog SET Status = 'Completed', EndDate = GETDATE() WHERE LogID = @QueryLogID
			UPDATE TBL_QuerySetList SET LastSuccessfulExecute = GETDATE(), Status = 'Scheduled' WHERE SetID = @QuerySetID
			IF NOT @EmailTemplateSuccess IS NULL  /* send notify email */
				BEGIN
				SET  @EmailBody = 'Query Set ' + @EmailBody + ' Succeeded. Check Log for Details'
				EXECUTE PROC_SendApplicationEmail_NA @EmailID = @EmailTemplateSuccess, @Subject = 'Query Set Execution Success', @BodyHTML = @EmailBody
			END
		END
		
		UPDATE TBL_QuerySetList SET LastSuccessfulExecute = GETDATE(), Status = 'Scheduled' WHERE SetID = @QuerySetID
		SET @NextQuerySet = (SELECT NextSetToExecute FROM TBL_QuerySetList WHERE SetID = @QuerySetID)
		IF NOT @NextQuerySet IS NULL
			BEGIN
			UPDATE TBL_QuerySetList SET Status = 'Execute Now' WHERE SetID = @NextQuerySet AND IsEnabled = 1
			END
		END
	ELSE IF @StatementCountCompleted < @StatementCountSubmitted 
		BEGIN
		UPDATE TBL_QuerySetList SET LastSuccessfulExecute = GETDATE(), Status = 'Executing' WHERE SetID = @QuerySetID
		
	END






GO
/****** Object:  StoredProcedure [dbo].[PROC_QuerySetList_Update_NA]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*************************************************************************
	Procedure Name: PROC_QuerySetList_Update_NA
	Returns:		Failure Code (@ErrorCode)
	Primary User:	website
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Tue Feb 16 17:17:06 CST 2016 @11 /Internet Time/
	Updated by:		Dave Elderkin
	Information:	Procedure for adding or updating the queryset header
	************************************************************************
	Version:		1.00
	Date:			Tue Feb 16 17:17:06 CST 2016 @11 /Internet Time/
	Updated By:		Dave Elderkin
***************************************************************************/
CREATE Procedure [dbo].[PROC_QuerySetList_Update_NA]
	@SetID int = Null OUTPUT,
	@SetName varchar(100) = Null,
	@Description varchar(max) = NULL,
	@IsEnabled bit = NULL,
	@OwnerEnterpriseID varchar(10) = NULL,
	@FailureApplicationEmailID int = NULL,
	@SuccessApplicationEmailID int = NULL,
	@StartTime time = NULL,
	@EndTime time = NULL,
	@NextSetToExecute int = NULL,
	@Frequency varchar(4) = NULL,
	@FrequencyModifier int = NULL,
	@QueryOrder varchar(10) = NULL,
	@DaysOfWeek varchar(7) = NULL,
	@CalendarWeekOfMonth varchar(6) = Null,
	@Type varchar(10) = NULL,
	@EmployeeLDAPID varchar(10) = NULL,
	@ErrorCode varchar(150) = NULL OUTPUT
AS
	SET NOCOUNT ON
	IF @Description = '' /* set @Description to null if empty string */
		SET @Description = NULL
	IF @SetName = '' /* set @SetName to null if empty string */
		SET @SetName = NULL
	IF @EmployeeLDAPID = '' /* set @EmployeeLDAPID to null if empty string */
		SET @EmployeeLDAPID = NULL
	SET @EmployeeLDAPID = UPPER(@EmployeeLDAPID)
	IF @OwnerEnterpriseID = '' /* set @OwnerEnterpriseID to null if empty string */
		SET @OwnerEnterpriseID = NULL
	SET @OwnerEnterpriseID = UPPER(@OwnerEnterpriseID)
	IF @Frequency = '' /* set @Frequency to null if empty string */
		SET @Frequency = NULL
	IF @FrequencyModifier = '' /* set @FrequencyModifier to null if empty string */
		SET @FrequencyModifier = NULL
	IF @QueryOrder = '' /* set @QueryOrder to null if empty string */
		SET @QueryOrder = NULL
	IF @DaysOfWeek = '' /* set @DaysOfWeek to null if empty string */
		SET @DaysOfWeek = NULL
	IF @CalendarWeekOfMonth = '' /* set @CalendarWeekOfMonth to null if empty string */
		SET @CalendarWeekOfMonth = NULL
	IF @Type = '' /* set @Description to null if empty string */
		SET @Type = NULL
	IF @FailureApplicationEmailID = 0 /* set @FailureApplicationEmailID to null if empty string */
		SET @FailureApplicationEmailID = NULL
	IF @SuccessApplicationEmailID = 0 /* set @SuccessApplicationEmailID to null if empty string */
		SET @SuccessApplicationEmailID = NULL
	IF @NextSetToExecute = 0 /* set @NextSetToExecute to null if empty string */
		SET @NextSetToExecute = NULL
				
	/* check that everything is passed in */
	IF @EmployeeLDAPID Is Null /* Return error code if @EmployeeLDAPID is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for EmployeeLDAPID, it is Required.'
		Return
	END
	

	
	IF @SetID Is Null /* insert new record */
		BEGIN
			IF @Type IS NULL
				SET @Type = 'Parent'
			IF @QueryOrder IS NULL
				SET @QueryOrder = 'Any Order'
			IF @Frequency IS NULL
				SET @Frequency = 'DD'
			IF @DaysOfWeek IS NULL
				SET @DaysOfWeek = '1234567'
			IF @CalendarWeekOfMonth IS NULL
				SET @CalendarWeekOfMonth = '123456'
			IF @IsEnabled IS NULL
				SET @IsEnabled = 0
				
			INSERT INTO TBL_QuerySetList (SetName, Description, IsEnabled, OwnerEnterpriseID, FailureApplicationEmailID, SuccessApplicationEmailID, StartTime, EndTime, NextSetToExecute, Frequency, FrequencyModifier, QueryOrder, DaysOfWeek, CalendarWeekOfMonth, LastUpdate, LastUpdateby, Type)
			VALUES(@SetName, @Description, @IsEnabled, @OwnerEnterpriseID, @FailureApplicationEmailID, @SuccessApplicationEmailID, @StartTime, @EndTime, @NextSetToExecute, @Frequency, @FrequencyModifier, @QueryOrder, @DaysOfWeek, @CalendarWeekOfMonth, getdate(), @EmployeeLDAPID, @Type)
			SET @SetID = scope_identity()
		END
	ELSE /* update existing record */
		BEGIN
			
		/* only updates are allowed though this proc as of now.*/
		UPDATE TBL_QuerySetList SET
			SetName = isnull(@SetName,SetName),
			Description = isnull(@Description,Description),
			IsEnabled = isnull(@IsEnabled,IsEnabled),
			OwnerEnterpriseID = isnull(@OwnerEnterpriseID,OwnerEnterpriseID),
			FailureApplicationEmailID = @FailureApplicationEmailID,
			SuccessApplicationEmailID = @SuccessApplicationEmailID,
			StartTime = isnull(@StartTime,StartTime),
			EndTime = isnull(@EndTime,EndTime),
			NextSetToExecute = @NextSetToExecute,
			Frequency = isnull(@Frequency,Frequency),
			FrequencyModifier = isnull(@FrequencyModifier,FrequencyModifier),
			QueryOrder = isnull(@QueryOrder,QueryOrder),
			DaysOfWeek = isnull(@DaysOfWeek,DaysOfWeek),
			CalendarWeekOfMonth = isnull(@CalendarWeekOfMonth,CalendarWeekOfMonth),
			LastUpdate = getdate(),
			LastUpdateby = @EmployeeLDAPID,
			Type = isnull(@Type,Type)
		WHERE SetID = @SetID 
	END
		
	







GO
/****** Object:  StoredProcedure [dbo].[PROC_Report_Submit_NA]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*************************************************************************
	Procedure Name: PROC_Report_Submit_NA
	Returns:		Failure Code (@ErrorCode)
	Primary User:	Everyone
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Fri Oct 09 15:49:26 CDT 2015 @909 /Internet Time/
	Updated by:		Dave Elderkin
	Documentation:	http://inhome.searshc.com/nfd/forms/view.asp?SubmittedFormID=607993
	Information:	Procedure for adding a new report request for a canned report
	************************************************************************
	Version:		1.00
	Date:			Fri Oct 09 15:49:26 CDT 2015 @909 /Internet Time/
	Updated By:		Dave Elderkin
***************************************************************************/
CREATE PROCEDURE [dbo].[PROC_Report_Submit_NA]
	@ReportID int = null,
	@LDAPID varchar(10) = null,
	@SqlStatement varchar(max) = null,
	@DisplayName varchar(50),
	@FileLocation varchar(250) = null,
	@EmailAddressList varchar(max) = null,
	@SaveType varchar(4) = NULL,
	@ErrorCode varchar(150) = NULL OUTPUT	
AS
	SET NOCOUNT ON
	SET @ErrorCode = NULL
	DECLARE @TimeStr as varchar(50) /* to hold the current date/time */
	DECLARE @FileName as varchar(255)
	DECLARE @ColumnDelimiter  as varchar(2)
	
	IF @ReportID = 0 /* set @MessageID to null if zero */
		SET @ReportID = NULL
	IF @LDAPID = '' /* set @LDAPID to null if zero */
		SET @LDAPID = NULL
	IF @DisplayName = '' /* set @DisplayName to null if zero */
		SET @DisplayName = NULL
	IF @SaveType = '' /* set @SaveType to null if zero */
		SET @SaveType = NULL
	IF @SaveType IS NULL
		SET @SaveType = 'XLSX'
	IF ltrim(rtrim(@FileLocation)) = '' /* set @SaveType to null if zero */
		SET @FileLocation = NULL
	IF ltrim(rtrim(@EmailAddressList)) = '' /* set @SaveType to null if zero */
		SET @EmailAddressList = NULL
	IF NOT CHARINDEX('@',@EmailAddressList) > 0
		SET @EmailAddressList = NULL
		
		
		
	IF @ReportID Is Null /* Return error code if @ReportID is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for ReportID, it is Required.'
		Return
	END
	IF @LDAPID Is Null /* Return error code if @LDAPID is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for LDAPID, it is Required.'
		Return
	END
	IF @SqlStatement Is Null /* Return error code if @SqlStatement is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for SqlStatement, it is Required.'
		Return
	END
	
	/* determine delimiter */
	IF @SaveType = 'TAB'
		SET @ColumnDelimiter = char(9)
	IF @SaveType = 'TXT'
		SET @ColumnDelimiter = '|'
	IF @SaveType = 'CSV'
		SET @ColumnDelimiter = ','
	IF @SaveType = 'TER'
		SET @ColumnDelimiter = '|'
	IF @SaveType = 'CSVQ'
		SET @ColumnDelimiter = ','
	
	/* create time string */
	SET @TimeStr = '_' + replace(replace(replace(convert(varchar,getdate(),120),'-',''),':',''),' ','')
	SET @FileName = (SELECT BaseFileName + @TimeStr + '.' + @SaveType FROM TBL_Reports WHERE ReportID = @ReportID)
	IF @DisplayName is null
		SET @DisplayName = @FileName
		
	INSERT INTO TBL_ReportRequests (ReportID, LDAPID, SaveAsType, FileName, DisplayName, EmailAddressList, FileLocation, SQLStatement, Delimiter)
	Values( @ReportID, @LDAPID, @SaveType, @FileName, @DisplayName, @EmailAddressList, @FileLocation, @SqlStatement, @ColumnDelimiter)

	/* Get the added row */ 
	If NOT @@Rowcount > 0
		SET @ErrorCode = 'Failed to create report'









GO
/****** Object:  StoredProcedure [dbo].[PROC_Report_Update_NA]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*************************************************************************
	Procedure Name: PROC_Report_Update_NA
	Returns:		Failure Code (@ErrorCode)
	Primary User:	Everyone
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Thu Oct 08 15:44:38 CDT 2015 @905 /Internet Time/
	Updated by:		Dave Elderkin
	Information:	Procedure for adding or updating a report
	************************************************************************
	Version:		1.00
	Date:			Thu Oct 08 15:44:38 CDT 2015 @905 /Internet Time/
	Updated By:		Dave Elderkin
***************************************************************************/
CREATE PROCEDURE [dbo].[PROC_Report_Update_NA]
	@ReportID int = null OUTPUT,
	@BaseFileName varchar(50) = null,
	@Name varchar(200) = null,
	@Category int = null,
	@Description varchar(1000) = null,
	@IsEnabled bit = null,
	@BaseSQLStatement varchar(max) = null,
	@LDAPID varchar(10) = null,
	@RestrictedAccess int = NULL,
	@ErrorCode varchar(150) = NULL OUTPUT
AS
	SET NOCOUNT ON
	SET @ErrorCode = NULL
	
	IF @IsEnabled is null
		set @IsEnabled = 0
	IF @ReportID = 0
		SET @ReportID = NULL
	IF @BaseFileName = ''
		SET @BaseFileName = NULL
	IF @Name = ''
		SET @Name = NULL
	IF @Category = 0
		SET @Category = NULL
	IF @Description = ''
		SET @Description = NULL
	IF @BaseSQLStatement = ''
		SET @BaseSQLStatement = NULL
	IF @RestrictedAccess IS NULL
		SET @RestrictedAccess = 0

	IF @ReportID IS NULL /* add a new record */
		BEGIN
		INSERT INTO nfdt.dbo.TBL_Reports (BaseFileName, Name, Category, Description, CreateDate, UpdateDate, IsEnabled, BaseSQLStatement, LastUpdateBy, RestrictedAccess)
		VALUES (@BaseFileName, @Name, @Category, @Description, GETDATE(), GETDATE(), @IsEnabled, @BaseSQLStatement, @LDAPID, @RestrictedAccess )
		If NOT @@Rowcount > 0
			SET @ErrorCode = 'Failed to create a new report' 
		SET @ReportID = @@IDENTITY

		RETURN
	END
	
	
	UPDATE nfdt.dbo.TBL_Reports SET 
		BaseFileName = @BaseFileName
		,Name = @Name
		,Category = @Category
		,Description = @Description
		,UpdateDate = GETDATE()
		,IsEnabled = @IsEnabled
		,BaseSQLStatement = @BaseSQLStatement
		,LastUpdateBy = @LDAPID
		,RestrictedAccess = @RestrictedAccess
	WHERE ReportID = @ReportID

	If NOT @@Rowcount > 0
		SET @ErrorCode = 'Failed to udpate report'




GO
/****** Object:  StoredProcedure [dbo].[PROC_ReportCriteria_Update_NA]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*************************************************************************
	Procedure Name: PROC_ReportCriteria_Update_NA
	Returns:		Failure Code (@ErrorCode)
	Primary User:	Everyone
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Thu Oct 08 15:44:38 CDT 2015 @905 /Internet Time/
	Updated by:		Dave Elderkin
	Information:	Procedure for adding or updating a report criteria
	************************************************************************
	Version:		1.00
	Date:			Thu Oct 08 15:44:38 CDT 2015 @905 /Internet Time/
	Updated By:		Dave Elderkin
***************************************************************************/
CREATE PROCEDURE [dbo].[PROC_ReportCriteria_Update_NA]
	@CriteriaID int = NULL OUTPUT,
	@ReportID int = NULL, 
	@SortOrder int = NULL, 
	@DisplayName varchar(250) = NULL, 
	@SQLStatement varchar(max) = NULL, 
	@Description varchar(1000) = NULL, 
	@DataType int = NULL, 
	@MaxLength int = NULL, 
	@Size int = NULL, 
	@DropdownType varchar(30) = NULL, 
	@DropdownStatement varchar(1000) = NULL, 
	@ValidationStatement varchar(500) = NULL, 
	@SpecialDataType varchar(30) = NULL, 
	@IsEnabled bit = NULL, 
	@ErrorCode varchar(150) = NULL OUTPUT
AS

	SET NOCOUNT ON
	SET @ErrorCode = NULL
	
	IF @IsEnabled is null
		set @IsEnabled = 0
	IF @ReportID = 0
		SET @ReportID = NULL
	IF @CriteriaID = 0
		SET @CriteriaID = NULL
	IF @Size = 0
		SET @Size = NULL
	IF @MaxLength = 0
		SET @MaxLength = NULL
	IF @MaxLength = 0
		SET @MaxLength = NULL
	IF @SortOrder is null
		SET @SortOrder = 1
	IF @DisplayName = ''
		SET @DisplayName = NULL
	IF @SQLStatement = ''
		SET @SQLStatement = NULL
	IF @Description = ''
		SET @Description = NULL
	IF @DropdownType = ''
		SET @DropdownType = NULL	
	IF @DropdownStatement = ''
		SET @DropdownStatement = NULL
	IF @SpecialDataType = ''
		SET @SpecialDataType = NULL
		
		
	IF @CriteriaID IS NULL /* add a new record */
		BEGIN
		INSERT INTO nfdt.dbo.TBL_ReportCriteria (ReportID, SortOrder, DisplayName, SQLStatement, Description, DataType, MaxLength, Size, DropdownType, DropdownStatement, ValidationStatement, SpecialDataType, IsEnabled)
		VALUES (@ReportID, @SortOrder, @DisplayName, @SQLStatement, @Description, @DataType, @MaxLength, @Size, @DropdownType, @DropdownStatement, @ValidationStatement, @SpecialDataType, @IsEnabled)
		If NOT @@Rowcount > 0
			SET @ErrorCode = 'Failed to create a new Criteria' 
		SET @ReportID = @@IDENTITY
		RETURN
	END
	
	UPDATE nfdt.dbo.TBL_ReportCriteria SET 
		ReportID = @ReportID
		,SortOrder = @SortOrder
		,DisplayName = @DisplayName
		,SQLStatement = @SQLStatement
		,Description = @Description
		,DataType = @DataType
		,MaxLength = @MaxLength
		,Size = @Size
		,DropdownType = @DropdownType
		,DropdownStatement = @DropdownStatement
		,ValidationStatement = @ValidationStatement
		,SpecialDataType = @SpecialDataType
		,IsEnabled = @IsEnabled
	WHERE CriteriaID = @CriteriaID

	If NOT @@Rowcount > 0
		SET @ErrorCode = 'Failed to udpate Criteria'



GO
/****** Object:  StoredProcedure [dbo].[PROC_ReportSchedule_Update_NA]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*************************************************************************
	Procedure Name: PROC_ReportSchedule_Update_NA
	Returns:		Failure Code (@ErrorCode)
	Primary User:	website
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Thu May 22 14:41:44 CDT 2014 @862 /Internet Time/
	Updated by:		Dave Elderkin
	Information:	Procedure for adding or updating the queryset header
	************************************************************************
	Version:		1.00
	Date:			Thu May 22 14:41:44 CDT 2014 @862 /Internet Time/ 
	Updated By:		Dave Elderkin
***************************************************************************/
CREATE PROCEDURE [dbo].[PROC_ReportSchedule_Update_NA]
	@ScheduleID int = Null OUTPUT,
	@ReportID int = Null OUTPUT,
	@OwnerLDAPID varchar(10) = NULL,
	@Criteria varchar(max) = NULL,
	@Status int = NULL,
	@StartTime time = NULL,
	@EndTime time = NULL,
	@Frequency varchar(4) = NULL,
	@FrequencyModifier int = NULL,
	@DaysOfWeek varchar(7) = NULL,
	@CalendarWeekOfMonth varchar(6) = Null,
	@EmailList varchar(max) = NULL,
	@FileLocation varchar(250) = Null,
	@FileType varchar(4) = NULL,
	@Delimiter varchar(2) = NULL,
	@ErrorCode varchar(150) = NULL OUTPUT
AS
	SET NOCOUNT ON

	IF @Criteria = '' /* set @Criteria to null if empty string */
		SET @Criteria = NULL
	IF @Frequency = '' /* set @Frequency to null if empty string */
		SET @Frequency = NULL
	SET @OwnerLDAPID = UPPER(@OwnerLDAPID)
	IF @OwnerLDAPID = '' /* set @OwnerLDAPID to null if empty string */
		SET @OwnerLDAPID = NULL
	IF @Frequency = '' /* set @Frequency to null if empty string */
		SET @Frequency = NULL
	IF @FrequencyModifier = '' /* set @FrequencyModifier to null if empty string */
		SET @FrequencyModifier = NULL
	IF @DaysOfWeek = '' /* set @DaysOfWeek to null if empty string */
		SET @DaysOfWeek = NULL
	IF @CalendarWeekOfMonth = '' /* set @CalendarWeekOfMonth to null if empty string */
		SET @CalendarWeekOfMonth = NULL
	IF @FileType = '' /* set @FileType to null if empty string */
		SET @FileType = NULL
	IF @Delimiter = '' /* set @Delimiter to null if empty string */
		SET @Delimiter = NULL
	IF ltrim(rtrim(@FileLocation)) = '' /* set @FileLocation to null if zero */
		SET @FileLocation = NULL
	IF ltrim(rtrim(@EmailList)) = '' /* set @EmailList to null if zero */
		SET @EmailList = NULL
	IF NOT CHARINDEX('@',@EmailList) > 0
		SET @EmailList = NULL
		
		
				
	/* check that everything is passed in */
	IF @OwnerLDAPID Is Null /* Return error code if @OwnerLDAPID is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for OwnerLDAPID, it is Required.'
		Return
	END
	
	IF @ReportID Is Null /* Return error code if @ReportID is null */
	 	Begin
		SET @ErrorCode = 'Please enter a value for ReportID, it is Required.'
		Return
	END
	
	/* defaults if nothign was passed in */
	IF @FileType IS NULL
		SET @FileType = 'XLSX'
	IF @Frequency IS NULL
		SET @Frequency = 'DD'
	IF @DaysOfWeek IS NULL
		SET @DaysOfWeek = '1234567'
	IF @CalendarWeekOfMonth IS NULL
		SET @CalendarWeekOfMonth = '123456'
	IF @Status IS NULL
		SET @Status = 1
				
	/* determine delimiter */
	IF @FileType = 'TAB'
		SET @Delimiter = char(9)
	IF @FileType = 'TXT'
		SET @Delimiter = '|'
	IF @FileType = 'CSV'
		SET @Delimiter = ','
	IF @FileType = 'TER'
		SET @Delimiter = '|'
	IF @FileType = 'CSVQ'
		SET @Delimiter = ','
	
	IF @ScheduleID Is Null /* insert new record */
		BEGIN
			INSERT INTO TBL_ReportSchedule (ReportID, OwnerLDAPID, Criteria, Status, StartTime, EndTime, Frequency, FrequencyModifier, DaysOfWeek, CalendarWeekOfMonth, EmailList, FileLocation, FileType, Delimiter, LastValidatedDate)
			VALUES(@ReportID, @OwnerLDAPID, @Criteria, @Status, @StartTime, @EndTime, @Frequency, @FrequencyModifier, @DaysOfWeek, @CalendarWeekOfMonth, @EmailList, @FileLocation, @FileType, @Delimiter, GETDATE())
			SET @ScheduleID = scope_identity()
		END
	ELSE /* update existing record */
		BEGIN
		UPDATE TBL_ReportSchedule SET
			ReportID = isnull(@ReportID, ReportID)
			,OwnerLDAPID = @OwnerLDAPID 
			,Criteria = isnull(@Criteria, Criteria)
			,Status = isnull(@Status, Status)
			,StartTime = isnull(@StartTime, StartTime)
			,EndTime = isnull(@EndTime, EndTime)
			,Frequency = isnull(@Frequency, Frequency)
			,FrequencyModifier = isnull(@FrequencyModifier, FrequencyModifier)
			,DaysOfWeek = isnull(@DaysOfWeek, DaysOfWeek)
			,CalendarWeekOfMonth = isnull(@CalendarWeekOfMonth, CalendarWeekOfMonth)
			,EmailList = isnull(@EmailList, EmailList)
			,FileLocation = isnull(@FileLocation, FileLocation)
			,FileType = isnull(@FileType, FileType)
			,Delimiter = isnull(@Delimiter, Delimiter)
			,LastValidatedDate = getdate()
		WHERE ScheduleID = @ScheduleID 
	END
		
	



GO
/****** Object:  StoredProcedure [dbo].[PROC_SendApplicationEmail_NA]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*************************************************************************
	Function Name: 	PROC_SendApplicationEmail_NA
	Type:			Scaler
	Primary User:	Server
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Thu Feb 14 11:37:09 CST 2013 @775 /Internet Time/
	Updated by:		Dave Elderkin
	Information:	This procedure sends a preformatted application email.
	************************************************************************
	Version:		1.00
	Date:			Thu Feb 14 11:40:02 CST 2013 @777 /Internet Time/
	Updated By:		Dave Elderkin
	************************************************************************
	Version:		1.01
	Date:			Thu Feb 28 15:29:33 CST 2013 @937 /Internet Time/
	Updated By:		Dave Elderkin
	Documentation:	http://nfdtqa.searshc.com/nfd/requests/View.asp?Num=80
	Changes:		Added a 'To' option in the table, and added that functionality here.  Sendto can 
					be null in the procedure and pulled from the table instead.
	************************************************************************
	Version:		1.02
	Date:			3/20/2013
	Updated By:		Dave Elderkin
	Documentation:	http://nfdtqa.searshc.com/nfd/forms/view.asp?SubmittedFormID=32
	Changes:		Added a '@FromAddress' option to the procedure which will allow the over-riding of
					the from field in the email that is sent.
	************************************************************************
	Version:		1.03
	Date:			Wed Jun 19 17:23:07 CDT 2013 @974 /Internet Time/
	Updated By:		Dave Elderkin
	Changes:		Added a '@Subject' option to the procedure which will allow the over-riding of
					the subject field in the email that is sent.
	************************************************************************
	Version:		2.0
	Date:			Tue Jan 26 17:46:22 CST 2016 @32 /Internet Time/
	Updated By:		Dave Elderkin
	Changes:		Updated to produciton format and tables.
***************************************************************************/
CREATE PROCEDURE [dbo].[PROC_SendApplicationEmail_NA]
	(
	@EmailID int,
	@BodyHTML varchar(max),
	@SendTo varchar(max) = NULL,
	@MailItemID INT = NULL OUTPUT,
	@Attachment varchar(255) = NULL,
	@FromAddress varchar(max) = NULL,
	@Subject varchar(max) = NULL
	) 
AS		
	DECLARE @ApplicaitonID INT /* submitted form ID for the application that owns this script */
	DECLARE @EmailReplyTo varchar(max) /* email that will get replies */
	DECLARE @Options varchar(max)
	DECLARE @TempBodyHTML varchar(max)
	DECLARE @FromAddressApplication varchar(max)
	DECLARE @ExtraCC varchar(max)
	DECLARE @ExtraBCC varchar(max)
	DECLARE @ExtraTo varchar(max)
	DECLARE @EmailSubject varchar(max)
	
	SET @Options = '|EmailID='+ cast(@EmailID as varchar) + '|'
	
	
	SET @ApplicaitonID = (SELECT OwnerID FROM TBL_EmailTemplates with (nolock) where EmailID = @EmailID)
	SET @EmailReplyTo = (SELECT DataValue + '@searshc.com' FROM TBL_FormsSubmittedChar  with (nolock) WHERE SubmittedID = @ApplicaitonID AND FieldID = 5) /* owner ID field from the applicaiton form is 5 */
	SET @FromAddressApplication = (SELECT DataValue FROM TBL_FormsSubmittedChar  with (nolock) WHERE SubmittedID = @ApplicaitonID AND FieldID = 1) /* name field from the applicaiton form is 1*/
	SET @FromAddressApplication = @FromAddressApplication + ' <donotreply@searshc.com>'
	
	IF @Subject IS NULL
		BEGIN
		SET @EmailSubject = (SELECT EmailSubject FROM TBL_EmailTemplates with (nolock) where EmailID = @EmailID)
		SET @EmailSubject = dbo.StringReplacement(@EmailSubject, @Options)
		END
	ELSE
		BEGIN
		SET @EmailSubject = dbo.StringReplacement(@Subject, @Options)
	END
	
	SET @TempBodyHTML = (SELECT BodyLayout FROM TBL_EmailTemplates with (nolock) WHERE EmailID = @EmailID)

	SET @TempBodyHTML = dbo.StringReplacement(@TempBodyHTML, @Options)
	SET @TempBodyHTML = replace(@TempBodyHTML, '[Email Content]',@BodyHTML)

	SET @ExtraCC = (SELECT ExtraCC FROM TBL_EmailTemplates with (nolock) where EmailID = @EmailID)
	SET @ExtraBCC = (SELECT ExtraBCC FROM TBL_EmailTemplates with (nolock) where EmailID = @EmailID)
	SET @ExtraTo = (SELECT ExtraTo FROM TBL_EmailTemplates with (nolock) where EmailID = @EmailID)
	
	IF NOT @ExtraTo IS NULL
		SET @SendTo = isnull(@SendTo,'') + ';' + @ExtraTo
		
	IF NOT @FromAddress IS NULL
		BEGIN
		SET @FromAddressApplication = @FromAddress
		SET @EmailReplyTo = @FromAddress
	END
	IF NOT @TempBodyHTML IS NULL
		BEGIN
		EXECUTE msdb.dbo.sp_send_dbmail
			@profile_name ='NFDT',
			@from_address = @FromAddressApplication,
			@reply_to = @EmailReplyTo,
			@blind_copy_recipients = @ExtraBCC,
			@copy_recipients = @ExtraCC, 
			@recipients = @SendTo,
			@subject = @EmailSubject,
			@body = @TempBodyHTML,
			@body_format = 'HTML',
			@file_attachments = @Attachment,
			@mailitem_id = @MailItemID OUTPUT
	END


GO
/****** Object:  StoredProcedure [dbo].[testlookupcodes]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[testlookupcodes]
As
select 
[LineID],
[OwnerType],
[Description]
from nfdt.dbo.TBL_LookupCodes


GO
/****** Object:  UserDefinedFunction [dbo].[CountWeekdayInstances]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*************************************************************************
	Function Name: 	CountWeekdayInstances
	Type:			Scaler
	Primary User:	DELDERK
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Thu Nov 21 16:30:02 CST 2013 @979 /Internet Time/
	Updated by:		Dave Elderkin
	Information:	This function counts the number of instances of a weekday between 2 dates
	************************************************************************
	Version:		1.00
	Date:			Thu Nov 21 16:30:02 CST 2013 @979 /Internet Time/
	Updated By:		Dave Elderkin
***************************************************************************/
CREATE Function [dbo].[CountWeekdayInstances]
	(
	@FromDate date, 
	@ToDate date,
	@WeekdayNumber int /* number between 1 and 7 */
	) 
	Returns int
AS
Begin		
	Declare @RetVal int
	
	SET @RetVal = 
		CASE 
			WHEN @WeekdayNumber = 1 THEN  datediff(DD, -1, @ToDate) / 7-datediff(DD, 0, @FromDate) / 7 /* sunday */
			WHEN @WeekdayNumber = 2 THEN  datediff(DD, -7, @ToDate) / 7-datediff(DD, -6, @FromDate) / 7 /* monday */
			WHEN @WeekdayNumber = 3 THEN  datediff(DD, -6, @ToDate) / 7-datediff(DD, -5, @FromDate) / 7 /* tues */
			WHEN @WeekdayNumber = 4 THEN  datediff(DD, -5, @ToDate) / 7-datediff(DD, -4, @FromDate) / 7 /* wed */
			WHEN @WeekdayNumber = 5 THEN  datediff(DD, -4, @ToDate) / 7-datediff(DD, -3, @FromDate) / 7 /* thurs */
			WHEN @WeekdayNumber = 6 THEN  datediff(DD, -3, @ToDate) / 7-datediff(DD, -2, @FromDate) / 7 /* friday */
			WHEN @WeekdayNumber = 7 THEN  datediff(DD, -2, @ToDate) / 7-datediff(DD, -1, @FromDate) / 7 /* saturday */
		END
		
	RETURN @RetVal
END


GO
/****** Object:  UserDefinedFunction [dbo].[DateFromParts]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*************************************************************************
	Function Name: 	DateFromParts
	Type:			Scaler
	Primary User:	DELDERK
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Thu Sep 21 11:01:11 CDT 2017 @709 /Internet Time/
	Updated by:		Dave Elderkin
	Information:	this function creates a date from year, month and date individual parts.
	Format:			date
	Length:			8
	Documentation:	Create to match the datefromparts funcciton in sql server 2012 and later: https://docs.microsoft.com/en-us/sql/t-sql/functions/datefromparts-transact-sql
	************************************************************************
	Version:		1.00
	Date:			Mon Dec 24 14:49:26 CST 2012 @909 /Internet Time/
	Updated By:		Dave Elderkin
***************************************************************************/
CREATE Function [dbo].[DateFromParts]
	(
	@Year int = Null, 
	@Month int = Null,
	@Day int = Null
	) 
	Returns Date
AS
Begin		
	IF @Year is Null OR @Year = 0
		RETURN Null 
		
	IF @Month is Null OR @Month = 0 OR @Month < 1 OR @Month > 12
		RETURN Null 
		
	IF @Day is Null OR @Day = 0  OR @Day < 1
		RETURN Null
		
		
	/* dtermine if we have a valid number of days for this specific month */
	IF @Month = 1 AND @Day > 31 RETURN NULL
	IF @Month = 2 AND @Day > 29 RETURN NULL /* possible failure on non-leap years */
	IF @Month = 3 AND @Day > 31 RETURN NULL
	IF @Month = 4 AND @Day > 30 RETURN NULL
	IF @Month = 5 AND @Day > 31 RETURN NULL
	IF @Month = 6 AND @Day > 30 RETURN NULL
	IF @Month = 7 AND @Day > 31 RETURN NULL
	IF @Month = 8 AND @Day > 31 RETURN NULL
	IF @Month = 9 AND @Day > 30 RETURN NULL
	IF @Month = 10 AND @Day > 31 RETURN NULL
	IF @Month = 11 AND @Day > 30 RETURN NULL
	IF @Month = 12 AND @Day > 31 RETURN NULL
	
		
	DECLARE @DateString VARCHAR(20)
	DECLARE @YearCutoff INT
	
	SET @YearCutoff = (SELECT top 1 (coalesce(cast(value as int),2049)) as Value FROM sys.configurations WHERE name = 'two digit year cutoff')
	SET @YearCutoff = CAST(RIGHT(CAST(@YearCutoff as varchar),2) as int)
	
	IF @Year < 100 --2 digits were provided.
		BEGIN
		IF @Year > @YearCutoff
			SET @Year = @Year + 1900
		ELSE
			SET @Year = @Year + 2000
	END
	
	
	SET @DateString = RIGHT('0000' + CAST(@Year as varchar),4) + '-' + RIGHT('0' + CAST(@Month as varchar),2) + '-' + RIGHT('0' + CAST(@Day as varchar),2)
	
	IF ISDATE(@DateString) = 1
		RETURN cast(cast(@DateString as datetime) as date)
	ELSE
		RETURN NULL
	
	RETURN NULL
END






GO
/****** Object:  UserDefinedFunction [dbo].[DateFromString]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*************************************************************************
	Function Name: 	DateFromString
	Type:			Scaler
	Primary User:	DELDERK
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Thu Sep 21 11:01:11 CDT 2017 @709 /Internet Time/
	Updated by:		Dave Elderkin
	Information:	this function returns a date from a string
	Format:			date
	Length:			3
	Documentation:	Create to match the datefromparts funcciton in sql server 2012 and later: https://docs.microsoft.com/en-us/sql/t-sql/functions/datefromparts-transact-sql
	************************************************************************
	Version:		1.00
	Date:			Mon Dec 24 14:49:26 CST 2012 @909 /Internet Time/
	Updated By:		Dave Elderkin
***************************************************************************/
CREATE Function [dbo].[DateFromString]
	(
	@InputString varchar(10)
	,@StringFormat varchar(10) 			/* the format of the date.  Supported: MMDDYY, MMDDYYYY, YYYYMMDD, MM-DD-YY, MM/DD/YY */
	) 
	Returns Date
AS
Begin		
	SET @InputString = LTRIM(RTRIM(@InputString))
	SET @StringFormat = UPPER(@StringFormat)
	
	/* this would be a lot better using charindex to determine all possible combinations of string format. */
	
	/* no delimiters */
	IF @StringFormat = 'MMDDYY' AND LEN(@InputString) = 6
		BEGIN
		RETURN nfdt.dbo.DateFromParts(RIGHT(@InputString,2), LEFT(@InputString,2), SUBSTRING(@InputString, 3, 2))
	END
	IF @StringFormat = 'MMDDYYYY' AND LEN(@InputString) = 8
		BEGIN
		RETURN nfdt.dbo.DateFromParts(RIGHT(@InputString,4), LEFT(@InputString,2), SUBSTRING(@InputString, 3, 2))
	END
	IF @StringFormat = 'YYYYMMDD' AND LEN(@InputString) = 8
		BEGIN
		RETURN nfdt.dbo.DateFromParts(LEFT(@InputString,4), SUBSTRING(@InputString, 5, 2), RIGHT(@InputString,2))
	END	
	
	
	
	IF @StringFormat = 'MM-DD-YY' AND LEN(@InputString) = 8
		BEGIN
		RETURN nfdt.dbo.DateFromParts(RIGHT(@InputString,2), LEFT(@InputString,2), SUBSTRING(@InputString, 4, 2))
	END
	IF @StringFormat = 'MM/DD/YY' AND LEN(@InputString) = 8
		BEGIN
		RETURN nfdt.dbo.DateFromParts(RIGHT(@InputString,2), LEFT(@InputString,2), SUBSTRING(@InputString, 4, 2))
	END
	
	
	
	RETURN NULL
END




GO
/****** Object:  UserDefinedFunction [dbo].[EmailTemplateFormat]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*************************************************************************
	Function Name: 	dbo.EmailTemplateFormat
	Type:			Scaler
	Primary User:	Server
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Thu Feb 13 17:02:40 CST 2014 @1 /Internet Time/
	Updated by:		Dave Elderkin
	Information:	Returns the info needed from an email template, already formatted.
	************************************************************************
	Version:		1.00
	Date:			Thu Feb 13 17:02:40 CST 2014 @1 /Internet Time/
	Updated By:		Dave Elderkin
***************************************************************************/

CREATE FUNCTION [dbo].[EmailTemplateFormat](@EmailID int, @BodyHTML varchar(max))
RETURNS @ReturnTable TABLE 
(
	EmailID INT NOT NULL,
	Subject varchar(max),
	BodyHTML varchar(max),
	ExtraTo varchar(max),
	ExtraCC varchar(max),
	ExtraBCC varchar(max)
)
AS
BEGIN

	DECLARE @Options varchar(max)
	DECLARE @TempBodyHTML varchar(max)
	DECLARE @ExtraCC varchar(max)
	DECLARE @ExtraBCC varchar(max)
	DECLARE @ExtraTo varchar(max)
	DECLARE @EmailSubject varchar(max)
	
	IF @BodyHTML IS NULL
		SET @BodyHTML = ''
		
	SET @Options = '|EmailID='+ cast(@EmailID as varchar) + '|'
	
	
	

	SET @EmailSubject = (SELECT EmailSubject FROM nfdt.dbo.TBL_EmailTemplates with (nolock) where EmailID = @EmailID)
	SET @EmailSubject = dbo.StringReplacement(@EmailSubject, @Options)

	
	SET @TempBodyHTML = (SELECT BodyLayout FROM TBL_EmailTemplates with (nolock) WHERE EmailID = @EmailID)
	SET @TempBodyHTML = dbo.StringReplacement(@TempBodyHTML, @Options)
	SET @TempBodyHTML = replace(@TempBodyHTML, '[Email Content]', @BodyHTML)

	
	SET @ExtraCC = (SELECT ExtraCC FROM nfdt.dbo.TBL_EmailTemplates with (nolock) where EmailID = @EmailID)
	SET @ExtraBCC = (SELECT ExtraBCC FROM nfdt.dbo.TBL_EmailTemplates with (nolock) where EmailID = @EmailID)
	SET @ExtraTo = (SELECT ExtraTo FROM nfdt.dbo.TBL_EmailTemplates with (nolock) where EmailID = @EmailID)
	
	INSERT INTO @ReturnTable(EmailID, Subject, BodyHTML, ExtraTo, ExtraCC, ExtraBCC)
	VALUES(@EmailID, @EmailSubject, @TempBodyHTML, @ExtraTo, @ExtraCC, @ExtraBCC)
	RETURN
END

GO
/****** Object:  UserDefinedFunction [dbo].[FindPatternLocation]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[FindPatternLocation]
(
    @String NVARCHAR(MAX),
    @TermToFind NVARCHAR(255)
)
RETURNS 
	@tab TABLE 
	(
	 CharacterPosition int
	)

AS
	BEGIN
	DECLARE @FindASCII int = 128
	
	/*loop to find a unused ASCII character.  */
	WHILE NOT CHARINDEX(char(@FindASCII),@String) = 0
		BEGIN
		SET @FindASCII = @FindASCII + 1
		IF @FindASCII > 255
			BEGIN
			/* every extended ascii code between 128 and 255 is contianed in String.  Pick one and hope for the best. */
			SET @FindASCII = 206
			BREAK
		END
	END
	
	SET @String = @String + char(@FindASCII) /* add a character not normally used so that we can also search the last character in @string. */
	
	DECLARE @StringLength BIGINT = len(@String)
	

	
	INSERT INTO @tab(CharacterPosition)
	SELECT Number - LEN(@termToFind) as Position
	FROM (
		SELECT 
			Number
			,Item = LTRIM(RTRIM(SUBSTRING(@String, Number, CHARINDEX(@termToFind, @String + @termToFind, Number) - Number)))
		FROM ( /* create a table with all numbers between 1 and the full length of @String. */
			SELECT TOP (@StringLength) row_number() OVER (
					ORDER BY t1.number
					) AS N
			FROM master..spt_values t1
			CROSS JOIN master..spt_values t2
			) AS n(Number)
		WHERE Number > 1
			AND Number <= CONVERT(INT, LEN(@String))
			AND SUBSTRING(@termToFind + @String, Number, LEN(@termToFind)) = @termToFind
		) AS WhoCares
	RETURN
END
GO
/****** Object:  UserDefinedFunction [dbo].[FormatPhoneNumber]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[FormatPhoneNumber](@PhoneNumber VARCHAR(MAX))
RETURNS VARCHAR(16)
BEGIN

	SET @PhoneNumber = dbo.KeepOnlyChars(@PhoneNumber, '0123456789')
	IF LEFT(@PhoneNumber,1) = '1'
		SET @PhoneNumber = right(@PhoneNumber, len(@PhoneNumber) - 1)
	

    IF LEN(@PhoneNumber) = 10 /* local US */
		RETURN SUBSTRING(@PhoneNumber, 1, 3) + '-' + SUBSTRING(@PhoneNumber, 4, 3) + '-' + SUBSTRING(@PhoneNumber, 7, 4)

	IF LEN(@PhoneNumber) = 13 /* international */
		RETURN SUBSTRING(@PhoneNumber, 1, 3) + '-' + SUBSTRING(@PhoneNumber, 4, 3) + '-' + SUBSTRING(@PhoneNumber, 7, 3) + '-' + right(@PhoneNumber, 4)

	/* failed international ansd local */
	RETURN NULL
END


GO
/****** Object:  UserDefinedFunction [dbo].[KeepOnlyChars]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*************************************************************************
	Function Name: 	KeepOnlyChars
	Type:			Scaler
	Primary User:	DELDERK
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Mon Dec 24 14:49:25 CST 2012 @909 /Internet Time/
	Updated by:		Dave Elderkin
	Information:	this function removes selected characters from a string.
	Format:			variable
	Length:			variable
	************************************************************************
	Version:		1.00
	Date:			Mon Dec 24 14:49:26 CST 2012 @909 /Internet Time/
	Updated By:		Dave Elderkin
	************************************************************************
	Version:		2.00
	Date:			Tue Nov 19 11:58:15 CST 2013 @790 /Internet Time/
	Updated By:		Dave Elderkin
	Changes:		Updated to accept varchar(max) instead of varchar(8000)
***************************************************************************/
CREATE Function [dbo].[KeepOnlyChars]
	(
	@InputString varchar(max) = Null, 
	@LettersToKeep varchar(max) = Null
	) 
	Returns varchar(max)
AS
Begin		
	if @InputString is null
		return null
	
    DECLARE @TempStor1 varchar(max)
    DECLARE @CharStor1 varchar(1)
    DECLARE @CurPlace int
    SET @TempStor1 = ''
    SET @CharStor1 = ''
	SET @CurPlace = 1
    While @CurPlace <=  Len(@InputString)
    	BEGIN    
		SET @CharStor1 = SUBSTRING(@InputString, @CurPlace, 1)
        If CHARINDEX(@CharStor1, @LettersToKeep) > 0
			SET @TempStor1 = @TempStor1 + @CharStor1
		SET @CurPlace = @CurPlace + 1
	END
	RETURN @TempStor1
END


GO
/****** Object:  UserDefinedFunction [dbo].[LinkHashTags]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[LinkHashTags]
(
	@InputText varchar(max)
)
RETURNS varchar(max)
AS
BEGIN
	/* replace every instance of a hash tag with a link if it has one */	
	DECLARE @WorkingText varchar(max)
	DECLARE @TemporaryLink varchar(max)
	DECLARE @StrLine VARCHAR(max)
	DECLARE @ValueLength BIGINT
	DECLARE @StartPos BIGINT
	DECLARE @Separator char(1)
	DECLARE @MaxWordCount int 
	DECLARE @WordsSoFar int

	SET @Separator = ' ' /* hard coded seperator of a space */
	SET @MaxWordCount = 250 /* check the first 250 words for a hash tag */
	
	SET @WorkingText = @Separator + @InputText +@Separator
	SET @WorkingText = replace(@WorkingText, '.', @Separator)
	SET @WorkingText = replace(@WorkingText, '?', @Separator)
	SET @WorkingText = replace(@WorkingText, '!', @Separator)
	SET @WorkingText = replace(@WorkingText, '<br>', @Separator)
	SET @WorkingText = replace(@WorkingText, '</a>', @Separator)
	SET @WorkingText = replace(@WorkingText, '</ br>', @Separator)
	SET @WorkingText = replace(@WorkingText, '</td>', @Separator)
	SET @WorkingText = replace(@WorkingText, char(10), @Separator)
	SET @WorkingText = replace(@WorkingText, char(13), @Separator)
	
	/* replace duplicate seperators with a single one */
	WHILE NOT @WorkingText = replace(@WorkingText, @Separator + @Separator, @Separator) BEGIN
		SET @WorkingText = replace(@WorkingText, @Separator + @Separator, @Separator)
	END

	SET @ValueLength = 1
	SET @StartPos = 1
	SET @WordsSoFar = 0
	
	WHILE (@StartPos < DATALENGTH(@WorkingText) + 1) AND @WordsSoFar < @MaxWordCount BEGIN /* do until end of data */
		SET @ValueLength = CHARINDEX(@Separator, SUBSTRING(@WorkingText, @StartPos, DATALENGTH(@WorkingText)), 1)
		IF @ValueLength = 0 
			SET @ValueLength = DATALENGTH(@WorkingText) - @StartPos + 1
		SET @StrLine = SUBSTRING(SUBSTRING(@WorkingText, @StartPos, DATALENGTH(@WorkingText)), 1, @ValueLength)
		SET @StrLine = REPLACE(@StrLine,@Separator,'')
		IF @ValueLength > 3 AND @ValueLength <= 140 AND SUBSTRING(@StrLine,1,1) IN ('#','@') /* valid hash tags of # for now, add @ later, probably as LDAP ID. */
			BEGIN
			SET @TemporaryLink = (SELECT '<a href="' + isnull(isnull(LinkView, LinkSearch),'/nfd/hashtags/search.asp?hashtag=' + replace(Type,'#','%23') + Tag) + '" title="' + isnull(Description,'') + '">' + Type + Tag + '</a>' FROM TBL_HashTag with (nolock) WHERE Type + Tag = @StrLine)
			IF NOT @TemporaryLink IS NULL
				SET @InputText = replace(@InputText, @StrLine, @TemporaryLink)
		END
		SET @StartPos = @StartPos + @ValueLength
		SET @WordsSoFar = @WordsSoFar + 1
	END

	RETURN ltrim(rtrim(@InputText))
END





GO
/****** Object:  UserDefinedFunction [dbo].[RandomCharacters]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*************************************************************************
	Function Name: 	RandomCharacters
	Type:			Scaler
	Primary User:	DELDERK
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Tue Jun 03 14:41:08 CDT 2014 @861 /Internet Time/
	Updated by:		Dave Elderkin
	Information:	this function creates a random string
	Format:			variable
	Length:			variable
	************************************************************************
	Version:		1.00
	Date:			Tue Jun 03 14:41:08 CDT 2014 @861 /Internet Time/
	Updated By:		Dave Elderkin
	************************************************************************
	Version:		1.00
	Date:			10/24/2017
	Updated By:		Dave Elderkin
	Changes:		Made the character pool settable from outside the function.
***************************************************************************/
CREATE Function [dbo].[RandomCharacters]
	(
	@MinimumStringLength INT = 8,
	@MaximumStringLength INT = 8,
	@CharacterPool varchar(max) = 'abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ23456789'
	) 
	Returns varchar(max)
AS
Begin		
	DECLARE @PoolLength int
	DECLARE @Length int
	DECLARE @LoopCount int
	DECLARE @RandomString varchar(max)
	Declare @RandomDecimal decimal(19,8)
	IF @CharacterPool = '' OR @CharacterPool IS NULL
		SET @CharacterPool = 'abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ23456789'


	SET @RandomDecimal = (SELECT RandomDecimal from nfdt.dbo.VIEW_FunctionOptions)

	/* generate string */
	SET @Length = ROUND(((@MaximumStringLength - @MinimumStringLength -1) * @RandomDecimal + @MinimumStringLength), 0) /* randomize string length */
	SET @PoolLength = DataLength(@CharacterPool)
	
	SET @LoopCount = 0
	SET @RandomString = ''
	
	WHILE (@LoopCount < @Length) BEGIN
		SET @RandomDecimal = (SELECT RandomDecimal from nfdt.dbo.VIEW_FunctionOptions)
		SET @RandomString = @RandomString + SUBSTRING(@CharacterPool, CONVERT(int, @RandomDecimal * @PoolLength), 1)
		SET @LoopCount = @LoopCount + 1
	END

	RETURN @RandomString
END



GO
/****** Object:  UserDefinedFunction [dbo].[Split]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*************************************************************************
	Function Name: 	Split
	Primary User:	Everyone
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Thu Feb 14 11:56:02 CST 2013 @788 /Internet Time/
	Updated by:		Dave Elderkin
	Information:	Fucntion that splits a string based on an a seperator.
	************************************************************************
	Version:		1.00
	Date:			Thu Feb 14 11:56:24 CST 2013 @789 /Internet Time/
	By:				Dave Elderkin
***************************************************************************/
CREATE FUNCTION [dbo].[Split] (@Seperator VARCHAR(MAX), @String VARCHAR(MAX))
RETURNS @ResultTable TABLE
    (
        ValueFound VARCHAR(MAX)
    )   
AS
    BEGIN
        DECLARE @xml XML
        DECLARE @CharsToKeep varchar(max)
        
        /* decide which characters to keep in the 2 strings because MS has some wierd issues converting XML with some ASCII characters when those characters are actually allowed in XML. */
        SET @CharsToKeep = 'abcdefghijklm nopqrstuvwxyz1234567890' + '<>&`~!@#$%^*()_+-=/|\,.?:;"'' ' + char(13) + char(10)
        SET @CharsToKeep = @CharsToKeep + UPPER(@CharsToKeep)
        SET @String = dbo.KeepOnlyChars(@String,@CharsToKeep)
        
        /* Only 2 characters that are not actually allowed in XML */
        SET @String = replace(replace(@String,'&','&amp;'),'<','&lt;')
        SET @Seperator = replace(replace(@Seperator,'&','&amp;'),'<','&lt;')
        
        SET @XML = N'<root><r>' + REPLACE(@String, @Seperator, '</r><r>') + '</r></root>'

        INSERT INTO @ResultTable(ValueFound)
        SELECT r.value('.','VARCHAR(MAX)') as Item
        FROM @xml.nodes('//root/r') AS RECORDS(r)

        RETURN
    END


GO
/****** Object:  UserDefinedFunction [dbo].[Split_FindInstances]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*************************************************************************
	Function Name: 	Split_FindInstances
	Primary User:	Everyone
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Thu Feb 14 11:56:02 CST 2013 @788 /Internet Time/
	Updated by:		Dave Elderkin
	Information:	Function that takes input text and a delimiter and searches 
					text for that result and returns a count of how many were found
	************************************************************************
	Version:		1.00
	Date:			Thu Feb 14 11:56:24 CST 2013 @789 /Internet Time/
	By:				Dave Elderkin
***************************************************************************/
CREATE FUNCTION [dbo].[Split_FindInstances]
	(
	@SearchText VARCHAR(MAX),		/* input string to search for a set of values */
	@FindText VARCHAR(MAX),			/* input string that contains a bunch of values to find */
	@Separator VARCHAR(MAX)			/* Seperator for both the SearchText and FindText */
	) 
	Returns Int 					/* how many times the given FindText was within SearchText */
AS
	BEGIN
	DECLARE @numFound int
	
	
	SET @numFound = 
		(
			SELECT
				COUNT(*)
			FROM
				dbo.Split (@Separator, @SearchText) as Searching
				INNER JOIN
				dbo.Split (@Separator, @FindText) as Finding
				ON Searching.ValueFound = Finding.ValueFound
		)
	
	RETURN @numFound
	
END


GO
/****** Object:  UserDefinedFunction [dbo].[Split_ReturnIndex]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*************************************************************************
	Function Name: 	Split_ReturnIndex
	Primary User:	Everyone
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Thu Feb 14 11:56:02 CST 2013 @788 /Internet Time/
	Updated by:		Dave Elderkin
	Information:	Function that takes input text and splits it into pieces and returns the index specified.
	************************************************************************
	Version:		1.00
	Date:			Thu Feb 14 11:56:24 CST 2013 @789 /Internet Time/
	By:				Dave Elderkin
***************************************************************************/
CREATE FUNCTION [dbo].[Split_ReturnIndex]
	(
	@Text varchar(max),					/* input string to convert */
	@Separator VARCHAR(1),		/* Seperator */
	@ReturnIndex int = 1		/* only return the value with this index */
	) 
	Returns varchar(max)
AS
	BEGIN
	DECLARE @StrLine VARCHAR(max)
	DECLARE @ValueLength BIGINT
	DECLARE @StartPos BIGINT
	DECLARE @InsertCount int

	SET @ValueLength = 1
	SET @StartPos = 1
	SET @InsertCount = 0
	
	WHILE (@StartPos < DATALENGTH(@Text) + 1) BEGIN /* do until end of data */
		SET @ValueLength = CHARINDEX(@Separator, SUBSTRING(@Text, @StartPos, DATALENGTH(@Text)), 1)
		IF @ValueLength = 0 
			SET @ValueLength = DATALENGTH(@Text) - @StartPos + 1
		SET @StrLine = SUBSTRING(SUBSTRING(@Text, @StartPos, DATALENGTH(@Text)), 1, @ValueLength)
		SET @StrLine = REPLACE(@StrLine,@Separator,'')
		SET @InsertCount = @InsertCount + 1
		IF  @InsertCount = @ReturnIndex
			BEGIN
			RETURN @StrLine
		END
		SET @StartPos = @StartPos + @ValueLength
	END
	RETURN NULL
END


GO
/****** Object:  UserDefinedFunction [dbo].[StringReplacement]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



/*************************************************************************
	Function Name: 	StringReplacement
	Type:			Scaler
	Primary User:	Server
	Designer:		Dave Elderkin
	Company:		Sears, Roebuck and Co
	Last Updated:	Thu Feb 14 11:37:09 CST 2013 @775 /Internet Time/
	Updated by:		Dave Elderkin
	Information:	This function replaces a preformatted string with the specified variables replaced with the value for that variable
	Format:			variable
	Length:			variable Text
	************************************************************************
	Version:		1.00
	Date:			Thu Feb 14 11:40:02 CST 2013 @777 /Internet Time/
	Updated By:		Dave Elderkin
***************************************************************************/
CREATE Function [dbo].[StringReplacement]
	(
	@InputString varchar(max) = Null,
	@Options varchar(max) = NULL
	) 
	Returns varchar(max)
AS
Begin		
	DECLARE @Tempint int
	DECLARE @TempValue varchar(max)
	DECLARE @TempName varchar(max)
	DECLARE @TempValue2 varchar(max)
	DECLARE @TempDecimal decimal(18,9)
	DECLARE @ApplicationID int
	

	IF @InputString is null
		return Null
	
	SET @InputString = replace(@InputString,'[Date]', left(convert(varchar,getdate(),126),10))
	SET @InputString = replace(@InputString,'[Time]', convert(varchar,getdate(),108))
	SET @InputString = replace(@InputString,'[Datetime]', left(convert(varchar,getdate(),126),10) + ' ' + convert(varchar,getdate(),108))

	/* check for any options specified in Options. Is pipe delimited and pipe bracketed with a = between the name/value pair. Example: '|who=cares|I=Do|'. names = 'who' and 'i'. values = 'cares' and 'do'*/
	IF NOT @Options IS NULL
		BEGIN
		SET @Tempint = LEN(@Options) - LEN(REPLACE(@Options, '|', ''))
		WHILE @Tempint > 0
			BEGIN
			
			SET @TempValue = dbo.Split_ReturnIndex(@Options, '|',@Tempint)
			SET @TempName = dbo.Split_ReturnIndex(@TempValue, '=',1) 
			SET @TempValue = dbo.Split_ReturnIndex(@TempValue, '=',2) 
			IF NOT @TempName IS NULL
				BEGIN
				/* if an email id number was passed in, get information for that email. Format is |EmailID=1|*/
				
				IF @TempName = 'EmailID'
					BEGIN
					
					SET @ApplicationID = (Select OwnerID FROM NFDT.dbo.TBL_EmailTemplates with (nolock) WHERE EmailID = @TempValue AND OwnerType = 'Application')
					
					IF charindex('[Email Name]', @InputString) > 0
						BEGIN
						SET @TempValue2 = (Select TemplateName FROM NFDT.dbo.TBL_EmailTemplates with (nolock) WHERE EmailID = @TempValue)
						IF NOT @TempValue2 IS NULL
							BEGIN
							SET @TempValue2 = dbo.StringReplacement(@TempValue2,@Options)
							SET @InputString = replace(@InputString,'[Email Name]', @TempValue2)
						END
					END
					IF charindex('[Email Description]', @InputString) > 0
						BEGIN
						SET @TempValue2 = (Select Description FROM NFDT.dbo.TBL_EmailTemplates with (nolock) WHERE EmailID = @TempValue)
						IF NOT @TempValue2 IS NULL
							BEGIN
							SET @TempValue2 = dbo.StringReplacement(@TempValue2,@Options)
							SET @InputString = replace(@InputString,'[Email Description]', @TempValue2)
						END
					END
					IF charindex('[Email Subject]', @InputString) > 0
						BEGIN
						SET @TempValue2 = (Select EmailSubject FROM NFDT.dbo.TBL_EmailTemplates with (nolock) WHERE EmailID = @TempValue)
						IF NOT @TempValue2 IS NULL
							BEGIN
							SET @TempValue2 = dbo.StringReplacement(@TempValue2,@Options)
							SET @InputString = replace(@InputString,'[Email Subject]', @TempValue2)
						END
					END
					IF charindex('[Email Header Block]', @InputString) > 0
						BEGIN
						SET @TempValue2 = (Select EmailHeaderBlock FROM NFDT.dbo.TBL_EmailTemplates with (nolock) WHERE EmailID = @TempValue)
						IF NOT @TempValue2 IS NULL
							BEGIN
							SET @TempValue2 = dbo.StringReplacement(@TempValue2,@Options)
							SET @InputString = replace(@InputString,'[Email Header Block]', @TempValue2)
							END
						ELSE
							BEGIN
							SET @InputString = replace(@InputString,'[Email Header Block]', '')
						END
					END
					IF charindex('[Email Footer Block]', @InputString) > 0
						BEGIN
						SET @TempValue2 = (Select EmailFooterBlock FROM NFDT.dbo.TBL_EmailTemplates with (nolock) WHERE EmailID = @TempValue)
						IF NOT @TempValue2 IS NULL
							BEGIN
							SET @TempValue2 = dbo.StringReplacement(@TempValue2,@Options)
							SET @InputString = replace(@InputString,'[Email Footer Block]', @TempValue2)
							END
						ELSE
							BEGIN
							SET @InputString = replace(@InputString,'[Email Footer Block]', '')
						END
					END
					IF charindex('[Email Description Block]', @InputString) > 0
						BEGIN
						SET @TempValue2 = (Select EmailDescriptonBlock FROM NFDT.dbo.TBL_EmailTemplates with (nolock) WHERE EmailID = @TempValue)
						IF NOT @TempValue2 IS NULL
							BEGIN
							SET @TempValue2 = dbo.StringReplacement(@TempValue2,@Options)
							SET @InputString = replace(@InputString,'[Email Description Block]', @TempValue2)
							END
						ELSE
							BEGIN
							SET @InputString = replace(@InputString,'[Email Description Block]', '')
						END
					END
					IF charindex('[Application Name]', @InputString) > 0
						BEGIN
						SET @TempValue2 = (SELECT max(CASE WHEN FieldID = 1 THEN DataValue ELSE NULL END) as ApplicationName FROM NFDT.dbo.TBL_FormsSubmittedChar with (nolock) INNER JOIN NFDT.dbo.TBL_EmailTemplates WITH (NOLOCK) ON SubmittedID = OwnerID WHERE FieldID in (1) AND EmailID = @TempValue AND OwnerType = 'Application' GROUP BY SubmittedID)
						IF NOT @TempValue2 IS NULL
							BEGIN
							SET @TempValue2 = dbo.StringReplacement(@TempValue2,@Options)
							SET @InputString = replace(@InputString,'[Application Name]', @TempValue2)
						END
					END
					IF charindex('[Application ID]', @InputString) > 0
						BEGIN
						SET @TempValue2 = cast(@ApplicationID as varchar(20))
						IF NOT @TempValue2 IS NULL
							BEGIN
							SET @TempValue2 = dbo.StringReplacement(@TempValue2,@Options)
							SET @InputString = replace(@InputString,'[Application ID]', @TempValue2)
						END
					END
					IF charindex('[Faq Entry]', @InputString) > 0 AND NOT @ApplicationID IS NULL /* get a random FAQ entry or use the default one */
						BEGIN
						SET @TempDecimal = (SELECT TOP 1 RandomDecimal from NFDT.dbo.VIEW_FunctionOptions)
						SET @TempValue2 = (SELECT TOP 1 Answer FROM NFDT.dbo.TBL_FAQ with (nolock) WHERE IsEnabled = 1 AND ApplicationID = @ApplicationID ORDER BY (ABS(CAST((BINARY_CHECKSUM(*) * @TempDecimal) as int)) % 100))
						IF NOT @TempValue2 IS NULL
							BEGIN
							SET @InputString = replace(@InputString,'[Faq Entry]', @TempValue2)
							END
						ELSE
							BEGIN
							SET @TempValue2 = 'The NFDT has a Frequently Asked Questions list for most applications that is updated often. The FAQ can be accessed online by <a href="http://inhome.searshc.com/nfd/faq/index.asp">clicking here</a>.'
							SET @InputString = replace(@InputString,'[Faq Entry]', @TempValue2)
						END
					END
					IF charindex('[Application Link]', @InputString) > 0
						BEGIN
						SET @TempValue2 = (SELECT max(CASE WHEN FieldID = 8 THEN DataValue ELSE NULL END) as ApplicationName FROM NFDT.dbo.TBL_FormsSubmittedChar with (nolock) INNER JOIN NFDT.dbo.TBL_EmailTemplates WITH (NOLOCK) ON SubmittedID = OwnerID WHERE FieldID in (8) AND EmailID = @TempValue AND OwnerType = 'Application' GROUP BY SubmittedID)
						IF NOT @TempValue2 IS NULL
							BEGIN
							SET @TempValue2 = dbo.StringReplacement(@TempValue2,@Options)
							SET @InputString = replace(@InputString,'[Application Link]', @TempValue2)
							END
						ELSE
							BEGIN
							SET @InputString = replace(@InputString,'[Application Link]', '')
						END
					END
					IF charindex('[Application Color]', @InputString) > 0
						BEGIN
						SET @TempValue2 = (SELECT max(CASE WHEN FieldID = 99 THEN DataValue ELSE NULL END) as ApplicationName FROM NFDT.dbo.TBL_FormsSubmittedChar with (nolock) INNER JOIN NFDT.dbo.TBL_EmailTemplates WITH (NOLOCK) ON SubmittedID = OwnerID WHERE FieldID in (99) AND EmailID = @TempValue AND OwnerType = 'Application' GROUP BY SubmittedID)
						IF NOT @TempValue2 IS NULL
							BEGIN
							SET @TempValue2 = dbo.StringReplacement(@TempValue2,@Options)
							SET @InputString = replace(@InputString,'[Application Color]', @TempValue2)
							END
						ELSE
							BEGIN
							SET @InputString = replace(@InputString,'[Application Color]', '0071bf')
						END
					END
					
				END
			END 
			SET @Tempint = @Tempint - 1
		END
	END
	
	RETURN @InputString

END
GO
/****** Object:  Table [dbo].[TBL_ArchiveQuerySet]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ArchiveQuerySet](
	[DateRecorded] [datetime] NOT NULL,
	[BatchID] [int] NOT NULL,
	[SetID] [int] NOT NULL,
	[UpdateID] [int] IDENTITY(1,1) NOT NULL,
	[QueryString] [varchar](max) NULL,
	[LastRun] [datetime] NULL,
	[LastSuccess] [datetime] NULL,
	[LastFailure] [datetime] NULL,
	[IsEnabled] [bit] NOT NULL,
	[SortOrder] [int] NOT NULL,
	[BatchName] [varchar](100) NULL,
	[Description] [varchar](max) NULL,
	[LastUpdateBy] [varchar](10) NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
	[LastErrorMessage] [varchar](2048) NULL,
	[IsConfidential] [bit] NOT NULL,
 CONSTRAINT [PK_TBL_Archive_QuerySet] PRIMARY KEY CLUSTERED 
(
	[DateRecorded] ASC,
	[BatchID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_AsynchronousQueryQueueLog]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_AsynchronousQueryQueueLog](
	[EntryID] [uniqueidentifier] NOT NULL,
	[SubmitDate] [datetime] NOT NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[ErrorNumber] [int] NULL,
	[ErrorMessage] [nvarchar](2048) NULL,
	[Statement] [varchar](max) NULL,
	[Source] [varchar](50) NULL,
	[SourceID] [int] NULL,
	[SourceLogID] [int] NULL,
	[RowCountAffected] [int] NULL,
	[SQLHandle] [varbinary](64) NULL,
	[PlanHandle] [varbinary](64) NULL,
	[StatusText] [varchar](max) NULL,
 CONSTRAINT [PK_TBL_AsynchronousQueryQueueLog] PRIMARY KEY NONCLUSTERED 
(
	[EntryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_AsynchronousQueryQueuePlan]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_AsynchronousQueryQueuePlan](
	[PlanHandle] [varbinary](64) NOT NULL,
	[PlanXML] [xml] NOT NULL,
 CONSTRAINT [PK_TBL_AsynchronousQueryQueuePlan] PRIMARY KEY CLUSTERED 
(
	[PlanHandle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_AsynchronousQueryQueueStats]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_AsynchronousQueryQueueStats](
	[EntryID] [uniqueidentifier] NOT NULL,
	[StatementNumber] [int] NOT NULL,
	[StatementText] [varchar](max) NULL,
	[CPUTime] [bigint] NULL,
	[Reads] [bigint] NULL,
	[Writes] [bigint] NULL,
	[RowCountAffected] [bigint] NULL,
	[ExecutionTime] [bigint] NULL,
	[QueryHash] [varbinary](8) NULL,
	[QueryPlanHash] [varbinary](8) NULL,
 CONSTRAINT [PK_TBL_a] PRIMARY KEY CLUSTERED 
(
	[EntryID] ASC,
	[StatementNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_CommentForwardLog]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_CommentForwardLog](
	[CommentID] [int] NOT NULL,
	[Destination] [int] NOT NULL,
	[DestinationID] [int] NULL,
	[Status] [int] NULL,
	[StatusDate] [datetime2](0) NULL,
 CONSTRAINT [PK_TBL_CommentForwardLog] PRIMARY KEY CLUSTERED 
(
	[CommentID] ASC,
	[Destination] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_CommentLog]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_CommentLog](
	[ChangeID] [int] IDENTITY(1,1) NOT NULL,
	[CommentID] [int] NOT NULL,
	[OriginalDate] [datetime2](0) NOT NULL,
	[UpdateDate] [datetime2](0) NOT NULL,
	[OriginalText] [text] NOT NULL,
	[OriginalEnterpriseID] [varchar](10) NOT NULL,
	[UpdateBy] [varchar](10) NOT NULL,
 CONSTRAINT [PK_TBL_CommentLog] PRIMARY KEY NONCLUSTERED 
(
	[ChangeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_Comments]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Comments](
	[CommentID] [int] IDENTITY(1,1) NOT NULL,
	[OwnerType] [varchar](10) NOT NULL,
	[CommentDate] [datetime2](0) NOT NULL,
	[OwnerID] [int] NOT NULL,
	[EnterpriseID] [varchar](10) NOT NULL,
	[Comment] [varchar](max) NOT NULL,
	[IsPrivate] [bit] NOT NULL,
	[ModifiedEnterpriseID] [varchar](10) NULL,
	[LastUpdate] [datetime2](0) NULL,
	[UnitNumber] [char](7) NULL,
	[ServiceOrderNumber] [char](8) NULL,
	[Destination] [int] NULL,
 CONSTRAINT [PK_TBL_Comments] PRIMARY KEY NONCLUSTERED 
(
	[CommentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_CrossReference]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_CrossReference](
	[OwnerType] [varchar](10) NOT NULL,
	[OwnerID] [int] NOT NULL,
	[RelatedType] [varchar](10) NOT NULL,
	[RelatedID] [int] NOT NULL,
	[Note] [varchar](250) NULL,
	[SortOrder] [int] NULL,
	[AncillaryShort] [varchar](50) NULL,
	[AncillaryNumeric] [int] NULL,
	[LastUpdate] [datetime] NOT NULL,
	[LastUpdateBy] [varchar](10) NOT NULL,
 CONSTRAINT [PK_TBL_CrossReference] PRIMARY KEY CLUSTERED 
(
	[OwnerType] ASC,
	[OwnerID] ASC,
	[RelatedType] ASC,
	[RelatedID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_DataLoadConfiguration]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_DataLoadConfiguration](
	[LoadID] [int] IDENTITY(1,1) NOT NULL,
	[SourceSystem] [varchar](128) NOT NULL,
	[SourceDatabase] [varchar](128) NULL,
	[SourceSchema] [varchar](128) NULL,
	[SourceTable] [varchar](256) NOT NULL,
	[QualifiedSourceTable] [varchar](128) NULL,
	[DestinationSystem] [varchar](128) NOT NULL,
	[DestinationDatabase] [varchar](128) NULL,
	[DestinationSchema] [varchar](128) NULL,
	[DestinationTable] [varchar](128) NOT NULL,
	[QualifiedDestinationTable] [varchar](128) NULL,
	[LastLoadAttempt] [datetime] NULL,
	[LastLoadSuccess] [datetime] NULL,
	[LastLoadFailure] [datetime] NULL,
	[SourceIDField] [varchar](128) NULL,
	[SourceDateField] [varchar](128) NULL,
	[MaxSourceID] [bigint] NULL,
	[MaxSourceDate] [datetime] NULL,
	[MaxDestinationID] [bigint] NULL,
	[MaxDestinationDate] [datetime] NULL,
	[OverrideNextLoadID] [int] NULL,
	[OverrideNextLoadDate] [datetime] NULL,
	[HistoryDaysToLoad] [int] NULL,
	[FutureDaystoLoad] [int] NULL,
	[LoadScript] [varchar](max) NULL,
	[ExecutionFrequency] [varchar](4) NULL,
	[ExecutionDaysOfWeek] [varchar](7) NULL,
	[ExecutionCalendarWeekOfMonth] [varchar](6) NULL,
	[ExecutionFiscalWeekOfMonth] [varchar](5) NULL,
	[ExecutionStartTime] [datetime] NULL,
	[RunQuerySetOnSuccess] [int] NULL,
	[ProcessInsert] [bit] NOT NULL,
	[ProcessUpdate] [bit] NOT NULL,
	[ProcessDelete] [bit] NOT NULL,
	[IsEnabled] [bit] NOT NULL,
	[OwnerEnterpriseID] [varchar](28) NULL,
	[Notes] [varchar](max) NULL,
	[Status] [varchar](20) NOT NULL,
 CONSTRAINT [PK_TBL_DataLoadConfiguration] PRIMARY KEY CLUSTERED 
(
	[LoadID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_DataLoadLog]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_DataLoadLog](
	[LogID] [int] IDENTITY(1,1) NOT NULL,
	[LoadID] [int] NOT NULL,
	[LogDate] [datetime2](3) NOT NULL,
	[RecordsUpdated] [int] NULL,
	[ErrorMessage] [varchar](max) NULL,
	[SQLStatement] [varchar](max) NULL,
 CONSTRAINT [PK_TBL_DataLoadLog] PRIMARY KEY CLUSTERED 
(
	[LogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EmailTemplates]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EmailTemplates](
	[EmailID] [int] IDENTITY(1,1) NOT NULL,
	[OwnerType] [varchar](20) NULL,
	[OwnerID] [int] NULL,
	[TemplateName] [varchar](200) NULL,
	[Description] [varchar](max) NULL,
	[ExtraTo] [varchar](500) NULL,
	[ExtraCC] [varchar](500) NULL,
	[ExtraBCC] [varchar](500) NULL,
	[EmailSubject] [varchar](500) NOT NULL,
	[EmailHeaderBlock] [varchar](max) NULL,
	[EmailFooterBlock] [varchar](max) NULL,
	[EmailDescriptonBlock] [varchar](max) NULL,
	[BodyLayout] [varchar](max) NOT NULL,
	[IsEnabled] [bit] NOT NULL,
	[LastUpdate] [datetime2](0) NOT NULL,
	[LastUpdateBy] [varchar](10) NOT NULL,
 CONSTRAINT [PK_EmailTemplates] PRIMARY KEY CLUSTERED 
(
	[EmailID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EndPoints]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EndPoints](
	[EndPointID] [int] IDENTITY(1,1) NOT NULL,
	[EndPointName] [varchar](50) NOT NULL,
	[ApplicationID] [int] NOT NULL,
	[IsEnabled] [bit] NOT NULL,
	[EndPointURL] [varchar](200) NOT NULL,
	[CreateEnterpriseID] [varchar](10) NOT NULL,
	[ModifiedEnterpriseID] [varchar](10) NOT NULL,
	[CreateDate] [datetime2](7) NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_TBL_EndPoints] PRIMARY KEY CLUSTERED 
(
	[EndPointID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EndPointUsers]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EndPointUsers](
	[EndPointUserID] [int] IDENTITY(1,1) NOT NULL,
	[EndPointID] [int] NOT NULL,
	[UserName] [varchar](20) NOT NULL,
	[PasswordHash] [binary](64) NOT NULL,
	[Salt] [uniqueidentifier] NOT NULL,
	[Business] [varchar](100) NULL,
	[BusinessOwnerEnterpriseID] [varchar](10) NULL,
	[CreateDate] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_TBL_EndPointUsers] PRIMARY KEY CLUSTERED 
(
	[EndPointUserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_FAQ]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_FAQ](
	[AnswerID] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationID] [int] NOT NULL,
	[Category] [int] NOT NULL,
	[Title] [varchar](100) NOT NULL,
	[Question] [varchar](max) NOT NULL,
	[Answer] [varchar](max) NOT NULL,
	[IsEnabled] [bit] NOT NULL,
	[SortOrder] [int] NOT NULL,
	[AddedBy] [varchar](10) NOT NULL,
	[LastUpdateBy] [varchar](10) NOT NULL,
	[LastUpdate] [datetime2](0) NOT NULL,
	[AddedDate] [datetime2](0) NOT NULL,
	[UseCommentFormatting] [bit] NOT NULL,
 CONSTRAINT [PK_TBL_FAQ] PRIMARY KEY CLUSTERED 
(
	[AnswerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_FileProcessing]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_FileProcessing](
	[FileID] [int] IDENTITY(1,1) NOT NULL,
	[Destination] [varchar](100) NOT NULL,
	[ProcessName] [varchar](50) NOT NULL,
	[ProcessDescription] [varchar](max) NOT NULL,
	[LastFoundFileName] [varchar](1024) NULL,
	[LastCheck] [datetime] NULL,
	[LastFound] [datetime] NULL,
	[LastStart] [datetime] NULL,
	[LastFailure] [datetime] NULL,
	[LastImport] [datetime] NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NOT NULL,
	[FileLocation] [varchar](1024) NOT NULL,
	[FileNameSearch] [varchar](50) NOT NULL,
	[FileExtension] [varchar](4) NOT NULL,
	[ArchiveLocation] [varchar](1024) NULL,
	[ArchiveFileNamePrefix] [varchar](50) NULL,
	[ArchiveFileCount] [int] NULL,
	[ArchiveFileSizeMB] [decimal](18, 2) NULL,
	[ArchiveAge] [int] NOT NULL,
	[ArchiveLastCleanup] [datetime] NULL,
	[ArchiveCleanupScript] [varchar](max) NULL,
	[IsEnabled] [bit] NOT NULL,
	[QuerySetIDOnSuccess] [int] NULL,
	[ProcessingType] [varchar](100) NULL,
	[ProcessingScript] [varchar](max) NULL,
	[FormatFile] [varchar](1024) NULL,
	[OwnerEnterpriseID] [varchar](10) NOT NULL,
	[SkipRows] [int] NOT NULL,
 CONSTRAINT [PK_TBL_FileProcessing] PRIMARY KEY CLUSTERED 
(
	[FileID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_FileProcessingArchive]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_FileProcessingArchive](
	[FileID] [int] IDENTITY(1,1) NOT NULL,
	[Destination] [varchar](100) NOT NULL,
	[ProcessName] [varchar](50) NOT NULL,
	[ProcessDescription] [varchar](max) NOT NULL,
	[LastFoundFileName] [varchar](1024) NULL,
	[LastCheck] [datetime] NULL,
	[LastFound] [datetime] NULL,
	[LastStart] [datetime] NULL,
	[LastFailure] [datetime] NULL,
	[LastImport] [datetime] NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NOT NULL,
	[FileLocation] [varchar](1024) NOT NULL,
	[FileNameSearch] [varchar](50) NOT NULL,
	[FileExtension] [varchar](4) NOT NULL,
	[ArchiveLocation] [varchar](1024) NULL,
	[ArchiveFileNamePrefix] [varchar](50) NULL,
	[ArchiveFileCount] [int] NULL,
	[ArchiveFileSizeMB] [decimal](18, 2) NULL,
	[ArchiveAge] [int] NOT NULL,
	[ArchiveLastCleanup] [datetime] NULL,
	[ArchiveCleanupScript] [varchar](max) NULL,
	[IsEnabled] [bit] NOT NULL,
	[QuerySetIDOnSuccess] [int] NULL,
	[ProcessingType] [varchar](100) NULL,
	[ProcessingScript] [varchar](max) NULL,
	[FormatFile] [varchar](1024) NULL,
	[OwnerEnterpriseID] [varchar](10) NOT NULL,
	[SkipRows] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_FileProcessingLog]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_FileProcessingLog](
	[LogID] [int] IDENTITY(1,1) NOT NULL,
	[FileID] [int] NOT NULL,
	[LogDate] [datetime] NOT NULL,
	[StatusCode] [int] NOT NULL,
	[LogText] [varchar](500) NULL,
	[LogData] [varchar](max) NULL,
 CONSTRAINT [PK_TBL_FileProcessingLog] PRIMARY KEY CLUSTERED 
(
	[LogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_FormFieldChangeLog]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_FormFieldChangeLog](
	[ChangeID] [int] IDENTITY(1,1) NOT NULL,
	[SubmittedID] [int] NOT NULL,
	[FieldID] [int] NOT NULL,
	[Message] [varchar](300) NOT NULL,
	[EnterpriseID] [varchar](10) NOT NULL,
	[ChangeDate] [datetime2](0) NOT NULL,
 CONSTRAINT [PK_TBL_FormFieldChangeLog] PRIMARY KEY NONCLUSTERED 
(
	[ChangeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_FormFields]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_FormFields](
	[FieldID] [int] IDENTITY(1,1) NOT NULL,
	[FormID] [int] NOT NULL,
	[SectionID] [int] NOT NULL,
	[DisplayName] [varchar](500) NULL,
	[MaxLength] [int] NOT NULL,
	[DisplaySize] [int] NOT NULL,
	[TextareaRows] [int] NOT NULL,
	[TextareaCols] [int] NOT NULL,
	[DataType] [int] NOT NULL,
	[Description] [varchar](250) NULL,
	[SortOrder] [int] NOT NULL,
	[DropdownType] [varchar](10) NULL,
	[DropdownStatement] [varchar](1000) NULL,
	[ValidationStatement] [varchar](500) NULL,
	[SpecialDataType] [varchar](30) NULL,
	[FieldNote] [varchar](100) NULL,
	[DisplayFieldNote] [bit] NOT NULL,
	[LockIfFormStatusIn] [varchar](100) NULL,
	[LockIfFormSubStatusIn] [varchar](100) NULL,
	[LockField] [int] NULL,
	[LockIfLockFieldValueIn] [varchar](500) NULL,
	[IsEnabled] [bit] NOT NULL,
	[IsRequired] [bit] NOT NULL,
	[IsRequiredMasterField] [int] NULL,
	[IsTitleField] [bit] NOT NULL,
	[IsFormatedAsComment] [bit] NOT NULL,
	[IsNoEmoticonsComment] [bit] NOT NULL,
	[IsConfidential] [bit] NOT NULL,
	[LogChanges] [bit] NOT NULL,
	[OnlyUpdatableAfterSubmit] [bit] NOT NULL,
	[OnlyUpdatableBySubmitter] [bit] NOT NULL,
	[IsUpdatableAfterSubmit] [bit] NOT NULL,
	[CaptionIsBold] [bit] NOT NULL,
	[DataIsBold] [bit] NOT NULL,
	[IsShownOnIndex] [bit] NOT NULL,
	[IsShownOnIndexWidth] [varchar](10) NULL,
	[ExtraDataType] [varchar](20) NULL,
	[ExtraData] [varchar](500) NULL,
	[LastUpdate] [datetime] NOT NULL,
	[LastUpdateBy] [varchar](10) NOT NULL,
 CONSTRAINT [PK_TBL_FormFields] PRIMARY KEY NONCLUSTERED 
(
	[FieldID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_FormOptions]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_FormOptions](
	[OptionID] [int] IDENTITY(1,1) NOT NULL,
	[FormID] [int] NOT NULL,
	[OptionName] [varchar](30) NOT NULL,
	[Description] [varchar](1000) NULL,
	[Parameter1] [varchar](50) NULL,
	[Parameter2] [varchar](50) NULL,
	[Parameter3] [varchar](50) NULL,
	[Parameter4] [varchar](250) NULL,
	[Parameter5] [varchar](1000) NULL,
	[IsEnabled] [bit] NOT NULL,
	[LastUpdateBy] [varchar](10) NOT NULL,
	[LastUpdate] [datetime2](0) NOT NULL,
 CONSTRAINT [PK_TBL_FormOptions] PRIMARY KEY NONCLUSTERED 
(
	[OptionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_Forms]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Forms](
	[FormID] [int] IDENTITY(1,1) NOT NULL,
	[FormType] [varchar](50) NOT NULL,
	[FormName] [varchar](100) NOT NULL,
	[OwnerEnterpriseID] [varchar](10) NOT NULL,
	[Description] [varchar](500) NOT NULL,
	[LongDescription] [varchar](6000) NULL,
	[IsEnabled] [bit] NOT NULL,
	[EnableSearch] [bit] NOT NULL,
	[EnableAdd] [bit] NOT NULL,
	[EnableReports] [bit] NOT NULL,
	[IsMainForm] [bit] NOT NULL,
	[ExpandSubmittedBy] [bit] NOT NULL,
	[ExpandWorkedBy] [bit] NOT NULL,
	[AllowAnonSubmit] [bit] NOT NULL,
	[AllowLinkedForms] [bit] NOT NULL,
	[AllowComments] [bit] NOT NULL,
	[AllowUploads] [bit] NOT NULL,
	[AllowTasks] [bit] NOT NULL,
	[AllowStatusChange] [bit] NOT NULL,
	[AllowToBeClosed] [bit] NOT NULL,
	[AutoEditOnSubmit] [bit] NOT NULL,
	[SubmitMessage] [varchar](500) NULL,
	[FormKey1Field] [int] NULL,
	[FormKey2Field] [int] NULL,
	[FormKey3Field] [int] NULL,
	[TimeoutSeconds] [int] NULL,
	[DocNumber] [varchar](10) NULL,
	[LastUpdate] [datetime2](0) NOT NULL,
	[LastUpdateBy] [varchar](10) NOT NULL,
	[AddPermissionLimited] [int] NOT NULL,
 CONSTRAINT [PK_TBL_Forms] PRIMARY KEY CLUSTERED 
(
	[FormID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_FormSections]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_FormSections](
	[SectionID] [int] IDENTITY(1,1) NOT NULL,
	[FormID] [int] NOT NULL,
	[Caption] [varchar](50) NOT NULL,
	[SectionOrder] [int] NOT NULL,
	[ColumnCount] [int] NOT NULL,
	[ColumnSizes] [varchar](100) NOT NULL,
	[ShowCaption] [bit] NOT NULL,
	[HasCellBorders] [bit] NOT NULL,
	[EndWithEmptyRow] [bit] NOT NULL,
	[MinFieldCountRequired] [int] NOT NULL,
	[MaxFieldCountRequired] [int] NOT NULL,
 CONSTRAINT [PK_TBL_FormSections] PRIMARY KEY NONCLUSTERED 
(
	[SectionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_FormsSubmitted]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_FormsSubmitted](
	[SubmittedID] [int] IDENTITY(1,1) NOT NULL,
	[FormID] [int] NOT NULL,
	[FormTitle] [varchar](250) NULL,
	[SubmittedDate] [datetime2](0) NOT NULL,
	[SubmittedBy] [varchar](10) NOT NULL,
	[Status] [int] NOT NULL,
	[SubStatus] [int] NOT NULL,
	[CurrentOwner] [varchar](10) NULL,
	[FormKey1Value] [varchar](250) NULL,
	[FormKey2Value] [varchar](250) NULL,
	[FormKey3Value] [varchar](250) NULL,
	[LastUpdate] [datetime2](0) NOT NULL,
	[LastUpdateBy] [varchar](10) NOT NULL,
 CONSTRAINT [PK_TBL_FormsSubmitted] PRIMARY KEY CLUSTERED 
(
	[SubmittedID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_FormsSubmittedChar]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_FormsSubmittedChar](
	[FieldID] [int] NOT NULL,
	[SubmittedID] [int] NOT NULL,
	[DataValue] [varchar](250) NOT NULL,
	[LastUpdate] [datetime2](0) NOT NULL,
	[LastUpdateBy] [varchar](10) NOT NULL,
 CONSTRAINT [PK_TBL_FormsSubmittedChar] PRIMARY KEY NONCLUSTERED 
(
	[FieldID] ASC,
	[SubmittedID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_FormsSubmittedDate]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_FormsSubmittedDate](
	[FieldID] [int] NOT NULL,
	[SubmittedID] [int] NOT NULL,
	[DataValue] [datetime2](0) NOT NULL,
	[LastUpdate] [datetime2](0) NOT NULL,
	[LastUpdateBy] [varchar](10) NOT NULL,
 CONSTRAINT [PK_TBL_FormsSubmittedDate] PRIMARY KEY NONCLUSTERED 
(
	[FieldID] ASC,
	[SubmittedID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_FormsSubmittedInt]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_FormsSubmittedInt](
	[FieldID] [int] NOT NULL,
	[SubmittedID] [int] NOT NULL,
	[DataValue] [int] NOT NULL,
	[LastUpdate] [datetime2](0) NOT NULL,
	[LastUpdateBy] [varchar](10) NOT NULL,
 CONSTRAINT [PK_TBL_FormsSubmittedInt] PRIMARY KEY NONCLUSTERED 
(
	[FieldID] ASC,
	[SubmittedID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_FormsSubmittedNumeric]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_FormsSubmittedNumeric](
	[FieldID] [int] NOT NULL,
	[SubmittedID] [int] NOT NULL,
	[DataValue] [decimal](18, 4) NOT NULL,
	[LastUpdate] [datetime2](0) NOT NULL,
	[LastUpdateBy] [varchar](10) NOT NULL,
 CONSTRAINT [PK_TBL_FormsSubmittedNumeric] PRIMARY KEY NONCLUSTERED 
(
	[FieldID] ASC,
	[SubmittedID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_FormsSubmittedText]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_FormsSubmittedText](
	[FieldID] [int] NOT NULL,
	[SubmittedID] [int] NOT NULL,
	[DataValue] [varchar](max) NOT NULL,
	[LastUpdate] [datetime2](0) NOT NULL,
	[LastUpdateBy] [varchar](10) NOT NULL,
 CONSTRAINT [PK_TBL_FormsSubmittedText] PRIMARY KEY NONCLUSTERED 
(
	[FieldID] ASC,
	[SubmittedID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_GroupDefinitions]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_GroupDefinitions](
	[GroupID] [int] IDENTITY(1,1) NOT NULL,
	[GroupType] [int] NOT NULL,
	[GroupName] [varchar](100) NOT NULL,
	[GroupDescription] [varchar](max) NOT NULL,
	[GroupOwnerEnterpriseID] [varchar](28) NOT NULL,
	[StatusCode] [int] NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
 CONSTRAINT [PK_TBL_GroupDefinitions] PRIMARY KEY CLUSTERED 
(
	[GroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_HashTag]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_HashTag](
	[Tag] [varchar](140) NOT NULL,
	[Type] [char](1) NOT NULL,
	[LinkView] [varchar](max) NULL,
	[LinkSearch] [varchar](max) NULL,
	[Description] [varchar](max) NULL,
	[OwnerType] [varchar](10) NULL,
	[OwnerID] [int] NULL,
	[FirstUse] [datetime2](0) NOT NULL,
	[FirstUseEnterpriseID] [varchar](10) NULL,
	[LastUpdate] [datetime2](0) NOT NULL,
	[LastUpdateBy] [varchar](10) NULL,
	[RankShortTerm] [int] NULL,
	[RankLongTerm] [int] NULL,
	[RankForever] [int] NULL,
	[TotalUses] [int] NULL,
 CONSTRAINT [PK_TBL_HashTag] PRIMARY KEY CLUSTERED 
(
	[Tag] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_HashTagInstances]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_HashTagInstances](
	[Tag] [varchar](140) NOT NULL,
	[Type] [char](1) NOT NULL,
	[OwnerID] [int] NOT NULL,
	[OwnerType] [varchar](10) NOT NULL,
 CONSTRAINT [PK_TBL_HashTagInstances] PRIMARY KEY CLUSTERED 
(
	[Tag] ASC,
	[Type] ASC,
	[OwnerID] ASC,
	[OwnerType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_HashTagSynonyms]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_HashTagSynonyms](
	[SourceTag] [varchar](140) NOT NULL,
	[SourceType] [char](1) NOT NULL,
	[DestinationTag] [varchar](140) NOT NULL,
	[DestinationType] [char](1) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[LastUpdate] [datetime2](0) NOT NULL,
	[LastUpdateBy] [varchar](10) NOT NULL,
 CONSTRAINT [PK_TBL_HashTagSynonyms] PRIMARY KEY CLUSTERED 
(
	[SourceTag] ASC,
	[SourceType] ASC,
	[DestinationTag] ASC,
	[DestinationType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_Help]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Help](
	[H_WebPage] [varchar](900) NOT NULL,
	[H_WebTrail] [varchar](1000) NULL,
	[H_Title] [varchar](1000) NOT NULL,
	[H_Content] [varchar](max) NOT NULL,
	[H_LastUpdate] [datetime2](0) NOT NULL,
	[H_LastUpdateBy] [varchar](10) NOT NULL,
	[H_DateCreate] [datetime] NOT NULL,
	[H_ApplicationFormID] [int] NULL,
	[H_UserDocumenationID] [int] NULL,
	[H_DescriptionBoxHTML] [varchar](max) NULL,
	[H_DescriptionBoxTitle] [varchar](100) NULL,
	[H_WebPageModifiedDate] [datetime2](0) NOT NULL,
 CONSTRAINT [PK_TBL_Help] PRIMARY KEY CLUSTERED 
(
	[H_WebPage] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_LoginGroupMembership]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_LoginGroupMembership](
	[LGM_GroupID] [varchar](10) NOT NULL,
	[LGM_EmpID] [varchar](10) NOT NULL,
 CONSTRAINT [PK_TBL_LoginGroupMembership] PRIMARY KEY CLUSTERED 
(
	[LGM_GroupID] ASC,
	[LGM_EmpID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_LoginGroups]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_LoginGroups](
	[LG_GroupID] [varchar](10) NOT NULL,
	[LG_GroupName] [varchar](50) NOT NULL,
	[LG_GroupDescription] [varchar](200) NULL,
	[LG_IsActive] [bit] NOT NULL,
	[LG_LastUpdate] [datetime2](7) NOT NULL,
	[LG_LastUpdateBy] [varchar](10) NOT NULL,
	[LG_NumericGroupID] [int] IDENTITY(1,1) NOT NULL,
	[LG_UpdateType] [int] NOT NULL,
	[LC_OwnerEnterpriseID] [varchar](10) NULL,
 CONSTRAINT [PK_TBL_LoginGroups] PRIMARY KEY CLUSTERED 
(
	[LG_GroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_LookupCodes]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_LookupCodes](
	[LineID] [int] IDENTITY(1,1) NOT NULL,
	[OwnerType] [varchar](20) NOT NULL,
	[OwnerID] [int] NULL,
	[Description] [varchar](50) NOT NULL,
	[LongDescription] [varchar](500) NULL,
	[NumericValue] [int] NULL,
	[SortOrder] [int] NOT NULL,
	[IsEnabled] [bit] NOT NULL,
	[IsUserAllowed] [bit] NOT NULL,
	[AncillaryShort] [varchar](50) NULL,
	[AncillaryNumeric] [int] NULL,
	[AncillaryLong] [varchar](max) NULL,
	[LastUpdate] [datetime2](0) NOT NULL,
	[LastUpdateBy] [varchar](10) NULL,
 CONSTRAINT [PK_TBL_LookupCodes] PRIMARY KEY CLUSTERED 
(
	[LineID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_Menu]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Menu](
	[MenuID] [int] IDENTITY(1,1) NOT NULL,
	[MenuType] [varchar](10) NULL,
	[ParentMenuID] [int] NULL,
	[DisplayName] [varchar](200) NULL,
	[DisplayWidth] [int] NULL,
	[DisplayIconPath] [varchar](255) NULL,
	[Link] [varchar](2000) NULL,
	[SortOrder] [int] NULL,
	[IsEnabled] [bit] NOT NULL,
	[EntryType] [int] NOT NULL,
	[Query] [varchar](max) NULL,
	[Administrator] [bit] NOT NULL,
	[LogInRequired] [bit] NOT NULL,
	[LogOutRequired] [bit] NOT NULL,
 CONSTRAINT [PK_TBL_Menu] PRIMARY KEY CLUSTERED 
(
	[MenuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_Metadata]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Metadata](
	[MetadataID] [int] IDENTITY(1,1) NOT NULL,
	[Server] [varchar](128) NOT NULL,
	[DatabaseName] [varchar](128) NULL,
	[Owner] [varchar](256) NULL,
	[ObjectName] [varchar](128) NOT NULL,
	[ObjectType] [varchar](20) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[ShortDescription] [varchar](50) NULL,
	[Description] [varchar](max) NULL,
	[LastUpdated] [datetime] NOT NULL,
	[LastUpdateBy] [varchar](10) NOT NULL,
	[AddedDate] [datetime] NOT NULL,
	[Rows] [decimal](18, 0) NULL,
	[Size] [decimal](18, 0) NULL,
	[Ordinal] [int] NULL,
	[DefaultValue] [varchar](max) NULL,
	[IsNullable] [bit] NULL,
	[DataType] [varchar](max) NULL,
	[Definition] [nvarchar](max) NULL,
	[DataMaxLength] [int] NULL,
	[ConfidentialAssociateLevel] [int] NULL,
	[ConfidentialCustomerLevel] [int] NULL,
	[ConfidentialSOXLevel] [int] NULL,
	[ConfidentialPCILevel] [int] NULL,
 CONSTRAINT [PK_TBL_Metadata] PRIMARY KEY CLUSTERED 
(
	[MetadataID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_Password]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[TBL_Password](
	[Name] [varchar](100) NOT NULL,
	[Description] [varchar](max) NULL,
	[Password] [varbinary](4000) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
SET ANSI_PADDING ON
ALTER TABLE [dbo].[TBL_Password] ADD [Salt] [varchar](20) NULL
 CONSTRAINT [PK_TBL_Password] PRIMARY KEY CLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_Permissions]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Permissions](
	[P_ApplicationID] [int] NOT NULL,
	[P_OwnerID] [int] NOT NULL,
	[P_GroupID] [varchar](10) NOT NULL,
	[P_PermissionName] [varchar](50) NOT NULL,
	[P_IsGranted] [bit] NOT NULL,
	[P_LastUpdate] [datetime2](7) NOT NULL,
	[P_LastUpdateBy] [varchar](10) NOT NULL,
 CONSTRAINT [PK_TBL_Permissions] PRIMARY KEY NONCLUSTERED 
(
	[P_ApplicationID] ASC,
	[P_OwnerID] ASC,
	[P_GroupID] ASC,
	[P_PermissionName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_QuerySet]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_QuerySet](
	[BatchID] [int] IDENTITY(1,1) NOT NULL,
	[SetID] [int] NOT NULL,
	[QueryString] [varchar](max) NULL,
	[LastRun] [datetime2](3) NULL,
	[LastSuccess] [datetime2](3) NULL,
	[LastFailure] [datetime2](3) NULL,
	[IsEnabled] [bit] NOT NULL,
	[SortOrder] [int] NOT NULL,
	[BatchName] [varchar](100) NULL,
	[Description] [varchar](max) NULL,
	[LastUpdate] [datetime2](0) NOT NULL,
	[LastUpdateBy] [varchar](10) NOT NULL,
	[LastErrorMessage] [varchar](2048) NULL,
	[IsConfidential] [bit] NOT NULL,
	[ApplicationFormID] [int] NULL,
	[AverageRows] [decimal](28, 6) NULL,
	[StandardDeviationRows] [decimal](28, 6) NULL,
	[NotifyIfRowsOutsideDeviation] [decimal](9, 4) NULL,
	[NotifyIfOverRows] [int] NULL,
	[NotifyIfUnderRows] [int] NULL,
 CONSTRAINT [PK_TBL_QuerySet] PRIMARY KEY CLUSTERED 
(
	[BatchID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_QuerySetExecutionLog]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_QuerySetExecutionLog](
	[LogID] [int] IDENTITY(1,1) NOT NULL,
	[SetID] [int] NOT NULL,
	[StartDate] [datetime2](0) NOT NULL,
	[EndDate] [datetime2](0) NULL,
	[StatementCountSubmitted] [int] NOT NULL,
	[StatementCountCompleted] [int] NOT NULL,
	[Status] [varchar](20) NOT NULL,
 CONSTRAINT [PK_TBL_QuerySetExecutionLog] PRIMARY KEY CLUSTERED 
(
	[LogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_QuerySetList]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_QuerySetList](
	[SetID] [int] IDENTITY(1,1) NOT NULL,
	[LastExecuteAttempt] [datetime2](0) NULL,
	[LastSuccessfulExecute] [datetime2](0) NULL,
	[LastFailure] [datetime2](0) NULL,
	[SetName] [varchar](100) NOT NULL,
	[Description] [varchar](max) NOT NULL,
	[IsEnabled] [bit] NOT NULL,
	[OwnerEnterpriseID] [varchar](10) NOT NULL,
	[FailureApplicationEmailID] [int] NULL,
	[SuccessApplicationEmailID] [int] NULL,
	[StartTime] [time](0) NOT NULL,
	[EndTime] [time](0) NOT NULL,
	[NextSetToExecute] [int] NULL,
	[Frequency] [varchar](4) NOT NULL,
	[FrequencyModifier] [int] NULL,
	[QueryOrder] [varchar](10) NOT NULL,
	[DaysOfWeek] [varchar](7) NOT NULL,
	[CalendarWeekOfMonth] [varchar](6) NOT NULL,
	[LastUpdate] [datetime2](0) NOT NULL,
	[LastUpdateBy] [varchar](10) NOT NULL,
	[Status] [varchar](20) NOT NULL,
	[Type] [varchar](10) NOT NULL,
 CONSTRAINT [PK_TBL_QuerySetList] PRIMARY KEY CLUSTERED 
(
	[SetID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ReportCriteria]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ReportCriteria](
	[CriteriaID] [int] IDENTITY(1,1) NOT NULL,
	[ReportID] [int] NOT NULL,
	[SortOrder] [int] NOT NULL,
	[DisplayName] [varchar](250) NOT NULL,
	[SQLStatement] [varchar](max) NOT NULL,
	[Description] [varchar](1000) NOT NULL,
	[DataType] [int] NOT NULL,
	[MaxLength] [int] NOT NULL,
	[Size] [int] NOT NULL,
	[DropdownType] [varchar](30) NULL,
	[DropdownStatement] [varchar](1000) NULL,
	[ValidationStatement] [varchar](500) NULL,
	[SpecialDataType] [varchar](30) NULL,
	[IsEnabled] [bit] NOT NULL,
 CONSTRAINT [PK_TBL_ReportCriteria] PRIMARY KEY CLUSTERED 
(
	[CriteriaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ReportRequests]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ReportRequests](
	[RequestID] [int] IDENTITY(1,1) NOT NULL,
	[ReportID] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[StatusDate] [datetime] NOT NULL,
	[LDAPID] [varchar](10) NOT NULL,
	[EmailAddressList] [varchar](max) NULL,
	[SaveAsType] [varchar](4) NOT NULL,
	[Delimiter] [varchar](2) NULL,
	[FileName] [varchar](250) NOT NULL,
	[FileLocation] [varchar](250) NULL,
	[DisplayName] [varchar](250) NULL,
	[SQLStatement] [varchar](max) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[ReportResultFile] [varbinary](max) NULL,
 CONSTRAINT [PK_TBL_ReportRequests] PRIMARY KEY CLUSTERED 
(
	[RequestID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_Reports]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Reports](
	[ReportID] [int] IDENTITY(1,1) NOT NULL,
	[BaseFileName] [varchar](50) NULL,
	[Name] [varchar](200) NOT NULL,
	[Category] [int] NOT NULL,
	[Description] [varchar](1000) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[IsEnabled] [bit] NOT NULL,
	[BaseSQLStatement] [varchar](max) NULL,
	[LastUpdateBy] [varchar](10) NOT NULL,
	[FileSaveDirectory] [varchar](250) NULL,
	[RestrictedAccess] [int] NOT NULL,
 CONSTRAINT [PK_TBL_Reports] PRIMARY KEY CLUSTERED 
(
	[ReportID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ReportSchedule]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ReportSchedule](
	[ScheduleID] [int] IDENTITY(1,1) NOT NULL,
	[ReportID] [int] NULL,
	[OwnerLDAPID] [varchar](28) NULL,
	[Criteria] [varchar](max) NULL,
	[Status] [int] NULL,
	[LastExecutionDate] [datetime] NULL,
	[StartTime] [time](0) NULL,
	[EndTime] [time](0) NULL,
	[Frequency] [varchar](4) NULL,
	[FrequencyModifier] [int] NULL,
	[DaysOfWeek] [varchar](7) NULL,
	[CalendarWeekOfMonth] [varchar](6) NULL,
	[EmailList] [varchar](max) NULL,
	[FileLocation] [varchar](250) NULL,
	[FileType] [varchar](4) NULL,
	[Delimiter] [varchar](2) NULL,
	[LastValidatedDate] [datetime] NULL,
 CONSTRAINT [PK_TBL_ReportSchedule] PRIMARY KEY CLUSTERED 
(
	[ScheduleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_Session]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Session](
	[SessionGUID] [uniqueidentifier] NOT NULL,
	[EnterpriseID] [varchar](10) NOT NULL,
	[StartTime] [datetime2](0) NOT NULL,
	[LastUseTime] [datetime2](0) NOT NULL,
 CONSTRAINT [PK_TBL_WebSession] PRIMARY KEY CLUSTERED 
(
	[SessionGUID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_SessionLog]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_SessionLog](
	[RowIDNumber] [bigint] IDENTITY(1,1) NOT NULL,
	[SessionGUID] [uniqueidentifier] NOT NULL,
	[DateStart] [datetime2](0) NOT NULL,
	[Type] [char](1) NOT NULL,
	[Text] [varchar](max) NOT NULL,
	[DateEnd] [datetime2](0) NULL,
 CONSTRAINT [PK_TBL_SessionLog] PRIMARY KEY CLUSTERED 
(
	[RowIDNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_test]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_test](
	[rownumber] [int] IDENTITY(1,1) NOT NULL,
	[SourceData] [varchar](max) NOT NULL,
 CONSTRAINT [PK_Table_1] PRIMARY KEY CLUSTERED 
(
	[rownumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_Test2]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Test2](
	[FileData] [varbinary](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[VIEW_FunctionOptions]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[VIEW_FunctionOptions]
AS
SELECT 
	RAND() as RandomDecimal
	,NewID() as NewGUID


GO
/****** Object:  View [dbo].[VIEW_OwnerIDAndObjectNamesSucccess]    Script Date: 9/18/2019 7:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[VIEW_OwnerIDAndObjectNamesSucccess]
AS
	SELECT
		AllTypes.OwnerType
		,AllTypes.OwnerID
		,AllTypes.OwnerTitle
		,AllTypes.LastSuccessDate
		,AllTypes.LastFailureDate
		,AllTypes.LastStatusDate
		,Comments.LastCommentDate
		,(SELECT Max(anyupdate) FROM (VALUES (AllTypes.LastSuccessDate), (AllTypes.LastFailureDate), (AllTypes.LastStatusDate), (Comments.LastCommentDate)) AS virtualtable (anyupdate)) as LastUpdateDate
		,OwnerNames.OwnerName 
		,OwnerNames.OwnerDescription 
		,OwnerNames.OwnerLink + CAST(AllTypes.OwnerID as varchar(20)) as  OwnerLink
	FROM
		(
		

		/* submitted forms */
		SELECT
			'Form' as OwnerType
			,SubmittedID as OwnerID
			,FormTitle as OwnerTitle
			,Null as LastSuccessDate
			,Null as LastFailureDate
			,LastUpdate as LastStatusDate
		FROM
			dbo.TBL_FormsSubmitted with (nolock)
		   
		   
		UNION ALL

			
		SELECT
			'QuerySet' as OwnerType
			,BatchID as OwnerID
			,BatchName as OwnerTitle
			,LastSuccess as LastSuccessDate
			,LastFailure as LastFailureDate
			,LastUpdate as LastStatusDate
		FROM
			dbo.TBL_QuerySet with (nolock)    
		
			UNION ALL
		

		
		SELECT 
			'Torii' as OwnerType
			,LoadID as OwnerID
			,QualifiedDestinationTable as OwnerTitle
			,LastLoadSuccess as LastSuccessDate
			,LastLoadFailure as LastFailureDate
			,LastLoadSuccess as LastStatusDate
		FROM 
			dbo.TBL_DataLoadConfiguration with (nolock)
			
		UNION ALL
		

		
		SELECT 
			'QueryGroup' as OwnerType
			,SetID as OwnerID
			,SetName as OwnerTitle
			,LastSuccessfulExecute as LastSuccessDate
			,LastFailure as LastFailureDate
			,LastUpdate as LastStatusDate
		FROM 
			dbo.TBL_QuerySetList with (nolock)
			
		UNION ALL
		
		SELECT 
			'AgentJob' as OwnerType
			,HASHBYTES('SHA1', cast(SJ.job_id as nvarchar(36))) % (power(cast(2 as bigint), cast(31 as bigint))-1) as OwnerID
			,max(SJ.NAME) as OwnerTitle
			,max(CASE WHEN jh.run_status = 1 THEN msdb.dbo.agent_datetime(run_date, run_time) ELSE NULL END) as LastSuccessDate
			,max(CASE WHEN jh.run_status = 0 THEN msdb.dbo.agent_datetime(run_date, run_time) ELSE NULL END) as  LastFailureDate
			,NULL as LastStatusDate
		FROM
			msdb.dbo.sysjobs SJ LEFT OUTER JOIN msdb.dbo.sysjobhistory JH ON SJ.job_id = JH.job_id
		WHERE 
			JH.step_id = 0
			AND jh.run_status in(0, 1)
		GROUP BY
			HASHBYTES('SHA1', cast(SJ.job_id as nvarchar(36))) % (power(cast(2 as bigint), cast(31 as bigint))-1)
			
			
		) as AllTypes
		LEFT OUTER JOIN 
		(
			SELECT
				OwnerType
				,OwnerID
				,MAX(CommentDate) as LastCommentDate
			FROM 
				dbo.TBL_Comments with (nolock)
			GROUP BY
				OwnerType
				,OwnerID
		) as Comments on AllTypes.OwnerType = Comments.OwnerType AND AllTypes.OwnerID = Comments.OwnerID		
		LEFT OUTER JOIN 
		(
			SELECT
				Description as OwnerName 
				,LongDescription as OwnerDescription
				,AncillaryLong as OwnerLink
			FROM
				dbo.TBL_LookupCodes with (nolock)
			WHERE
				OwnerType = 'Owner Type'
				AND IsEnabled = 1
		) as OwnerNames on AllTypes.OwnerType = OwnerNames.OwnerName
	
	
	










GO
/****** Object:  Index [IX_TBL_QuerySet_SetID]    Script Date: 9/18/2019 7:02:31 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_QuerySet_SetID] ON [dbo].[TBL_QuerySet]
(
	[SetID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TBL_ArchiveQuerySet] ADD  CONSTRAINT [DF_TBL_Archive_QuerySet_QS_DateRecorded]  DEFAULT (getdate()) FOR [DateRecorded]
GO
ALTER TABLE [dbo].[TBL_AsynchronousQueryQueueLog] ADD  CONSTRAINT [DF_TBL_AsynchronousQueryQueueLog_SubmitDate]  DEFAULT (getdate()) FOR [SubmitDate]
GO
ALTER TABLE [dbo].[TBL_Comments] ADD  CONSTRAINT [DF_TBL_CommentsDate]  DEFAULT (getdate()) FOR [CommentDate]
GO
ALTER TABLE [dbo].[TBL_Comments] ADD  CONSTRAINT [DF_TBL_CommentsIsPrivate]  DEFAULT ((0)) FOR [IsPrivate]
GO
ALTER TABLE [dbo].[TBL_CrossReference] ADD  CONSTRAINT [DF_TBL_CrossReference_LastUpdatedDate]  DEFAULT (getdate()) FOR [LastUpdate]
GO
ALTER TABLE [dbo].[TBL_DataLoadConfiguration] ADD  CONSTRAINT [DF_TBL_DataLoadConfiguration_ProcessInsert]  DEFAULT ((1)) FOR [ProcessInsert]
GO
ALTER TABLE [dbo].[TBL_DataLoadConfiguration] ADD  CONSTRAINT [DF_TBL_DataLoadConfiguration_ProcessUpdate]  DEFAULT ((1)) FOR [ProcessUpdate]
GO
ALTER TABLE [dbo].[TBL_DataLoadConfiguration] ADD  CONSTRAINT [DF_TBL_DataLoadConfiguration_ProcessDelete]  DEFAULT ((1)) FOR [ProcessDelete]
GO
ALTER TABLE [dbo].[TBL_DataLoadConfiguration] ADD  CONSTRAINT [DF_TBL_DataLoadConfiguration_IsEnabled]  DEFAULT ((1)) FOR [IsEnabled]
GO
ALTER TABLE [dbo].[TBL_DataLoadConfiguration] ADD  CONSTRAINT [DF_TBL_DataLoadConfiguration_Status]  DEFAULT ('Scheduled') FOR [Status]
GO
ALTER TABLE [dbo].[TBL_DataLoadLog] ADD  CONSTRAINT [DF_TBL_DataLoadLog_LogDate]  DEFAULT (getdate()) FOR [LogDate]
GO
ALTER TABLE [dbo].[TBL_EmailTemplates] ADD  CONSTRAINT [DF_EmailTemplates_IsEnabled]  DEFAULT ((1)) FOR [IsEnabled]
GO
ALTER TABLE [dbo].[TBL_EmailTemplates] ADD  CONSTRAINT [DF_EmailTemplates_LastUpdated]  DEFAULT (getdate()) FOR [LastUpdate]
GO
ALTER TABLE [dbo].[TBL_FAQ] ADD  DEFAULT ((1)) FOR [IsEnabled]
GO
ALTER TABLE [dbo].[TBL_FAQ] ADD  DEFAULT ((0)) FOR [SortOrder]
GO
ALTER TABLE [dbo].[TBL_FAQ] ADD  CONSTRAINT [DTBL_FAQ_LastUpdated]  DEFAULT (getdate()) FOR [LastUpdate]
GO
ALTER TABLE [dbo].[TBL_FAQ] ADD  CONSTRAINT [DTBL_FAQ_AddedDate]  DEFAULT (getdate()) FOR [AddedDate]
GO
ALTER TABLE [dbo].[TBL_FAQ] ADD  CONSTRAINT [DTBL_FAQ_UseCommentFormatting]  DEFAULT ((1)) FOR [UseCommentFormatting]
GO
ALTER TABLE [dbo].[TBL_FileProcessing] ADD  CONSTRAINT [DF_TBL_FileProcessing_Destination]  DEFAULT ('Teradata') FOR [Destination]
GO
ALTER TABLE [dbo].[TBL_FileProcessing] ADD  CONSTRAINT [DF_TBL_FileProcessing_ArchiveAge]  DEFAULT ((5)) FOR [ArchiveAge]
GO
ALTER TABLE [dbo].[TBL_FileProcessing] ADD  CONSTRAINT [DF_TBL_FileProcessing_SkipRows]  DEFAULT ((0)) FOR [SkipRows]
GO
ALTER TABLE [dbo].[TBL_FileProcessingLog] ADD  CONSTRAINT [DF_TBL_FileProcessingLog_LogDate]  DEFAULT (getdate()) FOR [LogDate]
GO
ALTER TABLE [dbo].[TBL_FileProcessingLog] ADD  CONSTRAINT [DF_TBL_FileProcessingLog_StatusCode]  DEFAULT ((0)) FOR [StatusCode]
GO
ALTER TABLE [dbo].[TBL_FormFieldChangeLog] ADD  CONSTRAINT [DF_TBL_FormFieldChangeLog_Date]  DEFAULT (getdate()) FOR [ChangeDate]
GO
ALTER TABLE [dbo].[TBL_FormFields] ADD  CONSTRAINT [DF_TBL_FormFieldsLastUpdate]  DEFAULT (getdate()) FOR [LastUpdate]
GO
ALTER TABLE [dbo].[TBL_FormOptions] ADD  CONSTRAINT [DF_TBL_FormOptions_IsEnabled]  DEFAULT ((1)) FOR [IsEnabled]
GO
ALTER TABLE [dbo].[TBL_FormOptions] ADD  CONSTRAINT [DF_TBL_FormOptions_LastUpdateDate]  DEFAULT (getdate()) FOR [LastUpdate]
GO
ALTER TABLE [dbo].[TBL_Forms] ADD  CONSTRAINT [DF_TBL_Forms_IsEnabled]  DEFAULT ((1)) FOR [IsEnabled]
GO
ALTER TABLE [dbo].[TBL_Forms] ADD  CONSTRAINT [DF_TBL_Forms_EnableSearch]  DEFAULT ((1)) FOR [EnableSearch]
GO
ALTER TABLE [dbo].[TBL_Forms] ADD  CONSTRAINT [DF_TBL_Forms_EnableAdd]  DEFAULT ((1)) FOR [EnableAdd]
GO
ALTER TABLE [dbo].[TBL_Forms] ADD  CONSTRAINT [DF_TBL_Forms_EnableReports]  DEFAULT ((1)) FOR [EnableReports]
GO
ALTER TABLE [dbo].[TBL_Forms] ADD  CONSTRAINT [DF_TBL_Forms_IsMainForm]  DEFAULT ((1)) FOR [IsMainForm]
GO
ALTER TABLE [dbo].[TBL_Forms] ADD  CONSTRAINT [DF_TBL_Forms_ExpandSubmittedBy]  DEFAULT ((1)) FOR [ExpandSubmittedBy]
GO
ALTER TABLE [dbo].[TBL_Forms] ADD  CONSTRAINT [DF_TBL_Forms_ExpandWorkedBy]  DEFAULT ((1)) FOR [ExpandWorkedBy]
GO
ALTER TABLE [dbo].[TBL_Forms] ADD  CONSTRAINT [DF_TBL_Forms_AllowAnonSubmit]  DEFAULT ((0)) FOR [AllowAnonSubmit]
GO
ALTER TABLE [dbo].[TBL_Forms] ADD  CONSTRAINT [DF_TBL_Forms_AllowLinkedForms]  DEFAULT ((1)) FOR [AllowLinkedForms]
GO
ALTER TABLE [dbo].[TBL_Forms] ADD  CONSTRAINT [DF_TBL_Forms_AllowComments]  DEFAULT ((1)) FOR [AllowComments]
GO
ALTER TABLE [dbo].[TBL_Forms] ADD  CONSTRAINT [DF_TBL_Forms_AllowUploads]  DEFAULT ((1)) FOR [AllowUploads]
GO
ALTER TABLE [dbo].[TBL_Forms] ADD  CONSTRAINT [DF_TBL_Forms_AllowTasks]  DEFAULT ((1)) FOR [AllowTasks]
GO
ALTER TABLE [dbo].[TBL_Forms] ADD  CONSTRAINT [DF_TBL_Forms_AllowStatusChange]  DEFAULT ((1)) FOR [AllowStatusChange]
GO
ALTER TABLE [dbo].[TBL_Forms] ADD  CONSTRAINT [DF_TBL_Forms_AllowToBeClosed]  DEFAULT ((1)) FOR [AllowToBeClosed]
GO
ALTER TABLE [dbo].[TBL_Forms] ADD  CONSTRAINT [DF_TBL_Forms_AutoEditOnSubmit]  DEFAULT ((0)) FOR [AutoEditOnSubmit]
GO
ALTER TABLE [dbo].[TBL_Forms] ADD  CONSTRAINT [DF_TBL_Forms_LastUpdatedDate]  DEFAULT (getdate()) FOR [LastUpdate]
GO
ALTER TABLE [dbo].[TBL_Forms] ADD  CONSTRAINT [DF_TBL_Form_sAddPermissionLimited]  DEFAULT ((1)) FOR [AddPermissionLimited]
GO
ALTER TABLE [dbo].[TBL_FormSections] ADD  CONSTRAINT [DF_TBL_FormSections_FS_ShowCaption]  DEFAULT ((1)) FOR [ShowCaption]
GO
ALTER TABLE [dbo].[TBL_FormSections] ADD  CONSTRAINT [DF_TBL_FormSections_FS_HasCellBorders]  DEFAULT ((0)) FOR [HasCellBorders]
GO
ALTER TABLE [dbo].[TBL_FormSections] ADD  CONSTRAINT [DF_TBL_FormSections_FS_EndWithEmptyRow]  DEFAULT ((1)) FOR [EndWithEmptyRow]
GO
ALTER TABLE [dbo].[TBL_FormSections] ADD  CONSTRAINT [DF_TBL_FormSections_FS_MinFieldCountRequired]  DEFAULT ((0)) FOR [MinFieldCountRequired]
GO
ALTER TABLE [dbo].[TBL_FormSections] ADD  CONSTRAINT [DF_TBL_FormSections_FS_MaxFieldCountRequired]  DEFAULT ((0)) FOR [MaxFieldCountRequired]
GO
ALTER TABLE [dbo].[TBL_FormsSubmitted] ADD  CONSTRAINT [DF_TBL_FormsSubmitted_SF_SubmittedDate]  DEFAULT (getdate()) FOR [SubmittedDate]
GO
ALTER TABLE [dbo].[TBL_FormsSubmitted] ADD  CONSTRAINT [DF_TBL_FormsSubmitted_SF_LastUpdatedDate]  DEFAULT (getdate()) FOR [LastUpdate]
GO
ALTER TABLE [dbo].[TBL_GroupDefinitions] ADD  CONSTRAINT [DF_TBL_GroupDefinitions_StatusCode]  DEFAULT ((0)) FOR [StatusCode]
GO
ALTER TABLE [dbo].[TBL_GroupDefinitions] ADD  CONSTRAINT [DF_TBL_GroupDefinitions_LastUpdate]  DEFAULT (getdate()) FOR [LastUpdate]
GO
ALTER TABLE [dbo].[TBL_HashTag] ADD  CONSTRAINT [DF_TBL_HashTag_Type]  DEFAULT ('#') FOR [Type]
GO
ALTER TABLE [dbo].[TBL_HashTag] ADD  CONSTRAINT [DF_TBL_HashTag_FirstUse]  DEFAULT (getdate()) FOR [FirstUse]
GO
ALTER TABLE [dbo].[TBL_HashTag] ADD  CONSTRAINT [DF_TBL_HashTag_UpdatedDate]  DEFAULT (getdate()) FOR [LastUpdate]
GO
ALTER TABLE [dbo].[TBL_HashTagSynonyms] ADD  CONSTRAINT [DF_TBL_HashTagSynonyms_HTS_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[TBL_HashTagSynonyms] ADD  CONSTRAINT [DF_TBL_HashTagSynonyms_HTS_LastUpdate]  DEFAULT (getdate()) FOR [LastUpdate]
GO
ALTER TABLE [dbo].[TBL_Help] ADD  CONSTRAINT [DF_TBL_Help_H_LastUpdate]  DEFAULT (getdate()) FOR [H_LastUpdate]
GO
ALTER TABLE [dbo].[TBL_Help] ADD  CONSTRAINT [DF_TBL_Help_H_DateCreate]  DEFAULT (getdate()) FOR [H_DateCreate]
GO
ALTER TABLE [dbo].[TBL_Help] ADD  CONSTRAINT [DF_TBL_Help_H_WebPageModifiedDate]  DEFAULT (getdate()) FOR [H_WebPageModifiedDate]
GO
ALTER TABLE [dbo].[TBL_LoginGroups] ADD  CONSTRAINT [DF_TBL_LoginGroups_LG_LastUpdate]  DEFAULT (getdate()) FOR [LG_LastUpdate]
GO
ALTER TABLE [dbo].[TBL_LoginGroups] ADD  CONSTRAINT [DF_TBL_LoginGroups_LG_UpdateType]  DEFAULT ((0)) FOR [LG_UpdateType]
GO
ALTER TABLE [dbo].[TBL_Metadata] ADD  DEFAULT ('TRDVNFDTSQL02') FOR [Server]
GO
ALTER TABLE [dbo].[TBL_Metadata] ADD  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[TBL_Metadata] ADD  DEFAULT (getdate()) FOR [LastUpdated]
GO
ALTER TABLE [dbo].[TBL_Metadata] ADD  DEFAULT ('SYSTEM') FOR [LastUpdateBy]
GO
ALTER TABLE [dbo].[TBL_Metadata] ADD  DEFAULT (getdate()) FOR [AddedDate]
GO
ALTER TABLE [dbo].[TBL_Metadata] ADD  DEFAULT ((-1)) FOR [ConfidentialAssociateLevel]
GO
ALTER TABLE [dbo].[TBL_Metadata] ADD  DEFAULT ((-1)) FOR [ConfidentialCustomerLevel]
GO
ALTER TABLE [dbo].[TBL_Metadata] ADD  DEFAULT ((-1)) FOR [ConfidentialSOXLevel]
GO
ALTER TABLE [dbo].[TBL_Metadata] ADD  DEFAULT ((-1)) FOR [ConfidentialPCILevel]
GO
ALTER TABLE [dbo].[TBL_Permissions] ADD  CONSTRAINT [DF_TBL_Permissions_P_IsGranted]  DEFAULT ((1)) FOR [P_IsGranted]
GO
ALTER TABLE [dbo].[TBL_Permissions] ADD  CONSTRAINT [DF_TBL_Permissions_P_LastUpdate]  DEFAULT (getdate()) FOR [P_LastUpdate]
GO
ALTER TABLE [dbo].[TBL_QuerySet] ADD  CONSTRAINT [DF_TBL_QuerySet_IsEnabled]  DEFAULT ((1)) FOR [IsEnabled]
GO
ALTER TABLE [dbo].[TBL_QuerySet] ADD  CONSTRAINT [DF_TBL_QuerySet_SortOrder]  DEFAULT ((5)) FOR [SortOrder]
GO
ALTER TABLE [dbo].[TBL_QuerySet] ADD  CONSTRAINT [DF_TBL_QuerySet_IsConfidential]  DEFAULT ((0)) FOR [IsConfidential]
GO
ALTER TABLE [dbo].[TBL_QuerySetExecutionLog] ADD  CONSTRAINT [DF_TBL_QuerySetExecutionLog_StartDate]  DEFAULT (getdate()) FOR [StartDate]
GO
ALTER TABLE [dbo].[TBL_QuerySetExecutionLog] ADD  CONSTRAINT [DF_TBL_QuerySetExecutionLog_StatementCountSubmitted]  DEFAULT ((0)) FOR [StatementCountSubmitted]
GO
ALTER TABLE [dbo].[TBL_QuerySetExecutionLog] ADD  CONSTRAINT [DF_TBL_QuerySetExecutionLog_StatementCountCompleted]  DEFAULT ((0)) FOR [StatementCountCompleted]
GO
ALTER TABLE [dbo].[TBL_QuerySetExecutionLog] ADD  CONSTRAINT [DF_TBL_QuerySetExecutionLog_Status]  DEFAULT ('Executing') FOR [Status]
GO
ALTER TABLE [dbo].[TBL_QuerySetList] ADD  CONSTRAINT [DF_TBL_QuerySetList_Status]  DEFAULT ('Scheduled') FOR [Status]
GO
ALTER TABLE [dbo].[TBL_ReportCriteria] ADD  CONSTRAINT [DF_TBL_ReportCriteria_RC_SortOrder]  DEFAULT ((0)) FOR [SortOrder]
GO
ALTER TABLE [dbo].[TBL_ReportCriteria] ADD  CONSTRAINT [DF_TBL_ReportCriteria_RC_IsEnabled]  DEFAULT ((1)) FOR [IsEnabled]
GO
ALTER TABLE [dbo].[TBL_ReportRequests] ADD  CONSTRAINT [DF_TBL_ReportRequests_RR_Status]  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [dbo].[TBL_ReportRequests] ADD  CONSTRAINT [DF_TBL_ReportRequests_RR_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[TBL_ReportRequests] ADD  CONSTRAINT [DF_TBL_ReportRequests_RR_StatusDate]  DEFAULT (getdate()) FOR [StatusDate]
GO
ALTER TABLE [dbo].[TBL_ReportRequests] ADD  CONSTRAINT [DF_TBL_ReportRequests_RR_SaveAsType]  DEFAULT ('XLS') FOR [SaveAsType]
GO
ALTER TABLE [dbo].[TBL_ReportRequests] ADD  CONSTRAINT [DF_TBL_ReportRequests_RR_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[TBL_Reports] ADD  CONSTRAINT [DF_TBL_Reports_R_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[TBL_Reports] ADD  CONSTRAINT [DF_TBL_Reports_R_UpdateDate]  DEFAULT (getdate()) FOR [UpdateDate]
GO
ALTER TABLE [dbo].[TBL_Reports] ADD  CONSTRAINT [DF_TBL_Reports_R_IsEnabled]  DEFAULT ((0)) FOR [IsEnabled]
GO
ALTER TABLE [dbo].[TBL_Reports] ADD  CONSTRAINT [DF_TBL_Reports_RestrictedAccess]  DEFAULT ((0)) FOR [RestrictedAccess]
GO
ALTER TABLE [dbo].[TBL_Session] ADD  CONSTRAINT [DF_TBL_WebSession_GUID]  DEFAULT (newid()) FOR [SessionGUID]
GO
ALTER TABLE [dbo].[TBL_SessionLog] ADD  CONSTRAINT [DF_TBL_SessionLog_SP_Date]  DEFAULT (getdate()) FOR [DateStart]
GO
ALTER TABLE [dbo].[TBL_SessionLog] ADD  CONSTRAINT [DF_TBL_SessionLog_SP_Type]  DEFAULT ('W') FOR [Text]
GO
USE [master]
GO
ALTER DATABASE [NFDT] SET  READ_WRITE 
GO
