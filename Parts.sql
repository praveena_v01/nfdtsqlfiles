USE [master]
GO
/****** Object:  Database [Parts]    Script Date: 9/18/2019 8:02:13 PM ******/
CREATE DATABASE [Parts]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Parts', FILENAME = N'D:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\Parts.mdf' , SIZE = 23008256KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1048576KB )
 LOG ON 
( NAME = N'Parts_log', FILENAME = N'E:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\Data\Parts_log.ldf' , SIZE = 531456KB , MAXSIZE = 2048GB , FILEGROWTH = 524288KB ), 
( NAME = N'Parts_log2', FILENAME = N'F:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\Data\Parts_log2.ldf' , SIZE = 257024KB , MAXSIZE = 2048GB , FILEGROWTH = 256000KB )
GO
ALTER DATABASE [Parts] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Parts].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Parts] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Parts] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Parts] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Parts] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Parts] SET ARITHABORT OFF 
GO
ALTER DATABASE [Parts] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Parts] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [Parts] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Parts] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Parts] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Parts] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Parts] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Parts] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Parts] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Parts] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Parts] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Parts] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Parts] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Parts] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Parts] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Parts] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Parts] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Parts] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Parts] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Parts] SET  MULTI_USER 
GO
ALTER DATABASE [Parts] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Parts] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Parts] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Parts] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [Parts]
GO
/****** Object:  User [SEARS2\JDECARO]    Script Date: 9/18/2019 8:02:14 PM ******/
CREATE USER [SEARS2\JDECARO] FOR LOGIN [SEARS2\JDECARO] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [os_spt]    Script Date: 9/18/2019 8:02:14 PM ******/
CREATE USER [os_spt] FOR LOGIN [os_spt] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [nfdt_soprodev]    Script Date: 9/18/2019 8:02:15 PM ******/
CREATE USER [nfdt_soprodev] FOR LOGIN [nfdt_soprodev] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [nfdt_partspro]    Script Date: 9/18/2019 8:02:15 PM ******/
CREATE USER [nfdt_partspro] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [nfdt_dbconnect]    Script Date: 9/18/2019 8:02:15 PM ******/
CREATE USER [nfdt_dbconnect] FOR LOGIN [nfdt_dbconnect] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [nfdt_coeprodev]    Script Date: 9/18/2019 8:02:15 PM ******/
CREATE USER [nfdt_coeprodev] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [KMART\njain3]    Script Date: 9/18/2019 8:02:15 PM ******/
CREATE USER [KMART\njain3] FOR LOGIN [KMART\njain3] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [KMART\lpnprod]    Script Date: 9/18/2019 8:02:16 PM ******/
CREATE USER [KMART\lpnprod] FOR LOGIN [KMART\lpnprod] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [KMART\HomeServices-HSNFDTTM-local]    Script Date: 9/18/2019 8:02:16 PM ******/
CREATE USER [KMART\HomeServices-HSNFDTTM-local] FOR LOGIN [KMART\HomeServices-HSNFDTTM-local]
GO
/****** Object:  User [KMART\HomeServices-HSNFDADMLocal]    Script Date: 9/18/2019 8:02:16 PM ******/
CREATE USER [KMART\HomeServices-HSNFDADMLocal] FOR LOGIN [KMART\HomeServices-HSNFDADMLocal] WITH DEFAULT_SCHEMA=[KMART\HomeServices-HSNFDADMLocal]
GO
/****** Object:  User [HFDVXSQL3_APP]    Script Date: 9/18/2019 8:02:16 PM ******/
CREATE USER [HFDVXSQL3_APP] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  DatabaseRole [db_execute]    Script Date: 9/18/2019 8:02:16 PM ******/
CREATE ROLE [db_execute]
GO
ALTER ROLE [db_owner] ADD MEMBER [SEARS2\JDECARO]
GO
ALTER ROLE [db_execute] ADD MEMBER [os_spt]
GO
ALTER ROLE [db_owner] ADD MEMBER [os_spt]
GO
ALTER ROLE [db_datareader] ADD MEMBER [os_spt]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [os_spt]
GO
ALTER ROLE [db_execute] ADD MEMBER [nfdt_soprodev]
GO
ALTER ROLE [db_datareader] ADD MEMBER [nfdt_soprodev]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [nfdt_soprodev]
GO
ALTER ROLE [db_execute] ADD MEMBER [nfdt_dbconnect]
GO
ALTER ROLE [db_datareader] ADD MEMBER [nfdt_dbconnect]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [nfdt_dbconnect]
GO
ALTER ROLE [db_owner] ADD MEMBER [KMART\njain3]
GO
ALTER ROLE [db_datareader] ADD MEMBER [KMART\njain3]
GO
ALTER ROLE [db_owner] ADD MEMBER [KMART\lpnprod]
GO
ALTER ROLE [db_execute] ADD MEMBER [KMART\HomeServices-HSNFDTTM-local]
GO
ALTER ROLE [db_datareader] ADD MEMBER [KMART\HomeServices-HSNFDTTM-local]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [KMART\HomeServices-HSNFDTTM-local]
GO
ALTER ROLE [db_owner] ADD MEMBER [KMART\HomeServices-HSNFDADMLocal]
GO
/****** Object:  Schema [KMART\HomeServices-HSNFDADMLocal]    Script Date: 9/18/2019 8:02:18 PM ******/
CREATE SCHEMA [KMART\HomeServices-HSNFDADMLocal]
GO
/****** Object:  StoredProcedure [dbo].[PROC_HSSCPDCOTCCIInventoryImport]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ==========================================================================================
-- Author:		Aaron Cote
-- Create date: 08/12/2015
-- Description:	Imports latest Home Services Supply Chain (HSSC) inventory data.
-- ==========================================================================================
CREATE PROCEDURE [dbo].[PROC_HSSCPDCOTCCIInventoryImport]

AS
	BEGIN
	
		IF object_id('tempdb..#TBL_HSSCPDCOTCCIInventoryTemp') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_HSSCPDCOTCCIInventoryTemp;
		END

		CREATE TABLE 
			#TBL_HSSCPDCOTCCIInventoryTemp
			([Channel] [varchar](3) NOT NULL,
			[Unit] [varchar](5) NOT NULL,
			[UnitName] [varchar](35) NOT NULL,
			[UnitType] [char](1) NOT NULL,
			[SVGPart] [varchar](32) NOT NULL,
			[SKU] [varchar](30) NOT NULL,
			[PartDivision] [varchar](3) NOT NULL,
			[PartListSource] [varchar](3) NOT NULL,
			[PartNumber] [varchar](24) NOT NULL,
			[PartDescription] [varchar](15) NOT NULL,
			[PartCost] [decimal](7, 2) NOT NULL,
			[PartSellPrice] [decimal](7, 2) NOT NULL,
			[SON] [varchar](4) NULL,
			[IsALSItem] [varchar](5) NULL,
			[DateAdded] [varchar](30) NULL,
			[OnHandNewQuantity] [int] NOT NULL,
			[OnOrderQuantity] [int] NOT NULL,
			[BackOrderQuantity] [varchar](10) NULL,
			[AllocatedQuantity] [varchar](10) NOT NULL,
			[NetAvailable] [int] NULL,
			[RolledUpNetAvail] [int] NULL,
			[MovementClass] [varchar](8) NULL,
			[MinimumStockQuantity] [int] NULL,
			[SL] [int] NOT NULL,
			[ROP] [int] NOT NULL,
			[EOQ] [int] NOT NULL,
			[StockMax] [int] NOT NULL,
			[AnnualDemandQuantity] [int] NULL,
			[StartDatePlanPart] [varchar](30) NULL,
			[DNPCODE] [varchar](3) NULL,
			[LT] [int] NOT NULL,
			[InventoryPlanQuantity] [int] NULL,
			[AvailSt] [varchar](10) NULL,
			[LawsonProcure] [varchar](6) NULL,
			[SLDS] [varchar](3) NOT NULL,
			[ForcastMethod] [varchar](16) NULL,
			[PotentialOrdersThisWeek] [decimal](7, 2) NULL,
			[PotentialOrdersNextWeek] [decimal](7, 2) NULL);

		--BULK INSERT #TBL_HSSCPDCOTCCIInventoryTemp FROM '\\hfdvxsql3\ssis_store\Parts\Imports\ALL_CHANNELS_STOCKED_PARTS_MASTER_xx-xx-xx.txt' WITH (FIRSTROW = 2, FIELDTERMINATOR ='|', ROWTERMINATOR = '\n')
		BULK INSERT #TBL_HSSCPDCOTCCIInventoryTemp FROM '\\ushofsvpnetapp1\D702PSXS\702PSO\Inventory Management\HSSC\ALL_CHANNELS_STOCKED_PARTS_MASTER_xx-xx-xx.txt' WITH (FIRSTROW = 2, FIELDTERMINATOR ='|', ROWTERMINATOR = '\n')

		MERGE 
			Parts.dbo.TBL_HSSCPDCOTCCIInventory AS T
		USING 
			(SELECT 
				Channel
				,Unit
				,UnitName
				,UnitType
				,SVGPart
				,'0' + PartDivision + PartListSource + PartNumber AS SKU
				,'0' + PartDivision AS PartDivision
				,PartListSource
				,PartNumber
				,PartDescription
				,PartCost
				,PartSellPrice
				,SON
				,IsALSItem
				,DateAdded
				,OnHandNewQuantity
				,OnOrderQuantity
				,FLOOR(CAST(BackOrderQuantity AS DECIMAL(10,2))) AS BackOrderQuantity
				,FLOOR(CAST(AllocatedQuantity AS DECIMAL(10,2))) AS AllocatedQuantity
				,NetAvailable
				,RolledUpNetAvail
				,MovementClass
				,MinimumStockQuantity
				,SL
				,ROP
				,EOQ
				,StockMax
				,AnnualDemandQuantity
				,StartDatePlanPart
				,DNPCODE
				,LT
				,InventoryPlanQuantity
				,AvailSt
				,LawsonProcure
				,SLDS
				,ForcastMethod
				,PotentialOrdersThisWeek
				,PotentialOrdersNextWeek
			FROM 
				#TBL_HSSCPDCOTCCIInventoryTemp) AS S
		ON 
			(T.Unit = S.Unit
			AND
			T.SKU = S.SKU)
		WHEN MATCHED THEN
			UPDATE 
			SET 
				T.Channel = S.Channel
				,T.UnitName = S.UnitName
				,T.UnitType = S.UnitType
				,T.SVGPart = S.SVGPart
				,T.PartDivision = S.PartDivision
				,T.PartListSource = S.PartListSource
				,T.PartNumber = S.PartNumber
				,T.PartDescription = S.PartDescription
				,T.PartCost = S.PartCost
				,T.PartSellPrice = S.PartSellPrice
				,T.SON = S.SON
				,T.IsALSItem = S.IsALSItem
				,T.DateAdded = S.DateAdded
				,T.OnHandNewQuantity = S.OnHandNewQuantity
				,T.OnOrderQuantity = S.OnOrderQuantity
				,T.BackOrderQuantity = S.BackOrderQuantity
				,T.AllocatedQuantity = S.AllocatedQuantity
				,T.NetAvailableQuantity = S.NetAvailable
				,T.RolledUpNetAvail = S.RolledUpNetAvail
				,T.MovementClass = S.MovementClass
				,T.MinimumStockQuantity = S.MinimumStockQuantity
				,T.SL = S.SL
				,T.ROP = S.ROP
				,T.EOQ = S.EOQ
				,T.StockMax = S.StockMax
				,T.AnnualDemandQuantity = S.AnnualDemandQuantity
				,T.StartDatePlanPart = S.StartDatePlanPart
				,T.DNPCODE = S.DNPCODE
				,T.LT = S.LT
				,T.InventoryPlanQuantity = S.InventoryPlanQuantity
				,T.AvailSt = S.AvailSt
				,T.LawsonProcure = S.LawsonProcure
				,T.SLDS = S.SLDS
				,T.ForcastMethod = S.ForcastMethod
				,T.PotentialOrdersThisWeek = S.PotentialOrdersThisWeek
				,T.PotentialOrdersNextWeek = S.PotentialOrdersNextWeek
		WHEN NOT MATCHED BY TARGET THEN
			INSERT 
				(Channel
				,Unit
				,UnitName
				,UnitType
				,SVGPart
				,SKU
				,PartDivision
				,PartListSource
				,PartNumber
				,PartDescription
				,PartCost
				,PartSellPrice
				,SON
				,IsALSItem
				,DateAdded
				,OnHandNewQuantity
				,OnOrderQuantity
				,BackOrderQuantity
				,AllocatedQuantity
				,NetAvailableQuantity
				,RolledUpNetAvail
				,MovementClass
				,MinimumStockQuantity
				,SL
				,ROP
				,EOQ
				,StockMax
				,AnnualDemandQuantity
				,StartDatePlanPart
				,DNPCODE
				,LT
				,InventoryPlanQuantity
				,AvailSt
				,LawsonProcure
				,SLDS
				,ForcastMethod
				,PotentialOrdersThisWeek
				,PotentialOrdersNextWeek)
			VALUES 
				(S.Channel
				,S.Unit
				,S.UnitName
				,S.UnitType
				,S.SVGPart
				,S.SKU
				,S.PartDivision
				,S.PartListSource
				,S.PartNumber
				,S.PartDescription
				,S.PartCost
				,S.PartSellPrice
				,S.SON
				,S.IsALSItem
				,S.DateAdded
				,S.OnHandNewQuantity
				,S.OnOrderQuantity
				,S.BackOrderQuantity
				,S.AllocatedQuantity
				,S.NetAvailable
				,S.RolledUpNetAvail
				,S.MovementClass
				,S.MinimumStockQuantity
				,S.SL
				,S.ROP
				,S.EOQ
				,S.StockMax
				,S.AnnualDemandQuantity
				,S.StartDatePlanPart
				,S.DNPCODE
				,S.LT
				,S.InventoryPlanQuantity
				,S.AvailSt
				,S.LawsonProcure
				,S.SLDS
				,S.ForcastMethod
				,S.PotentialOrdersThisWeek
				,S.PotentialOrdersNextWeek)
		WHEN NOT MATCHED BY SOURCE THEN
			DELETE;

		IF object_id('tempdb..#TBL_HSSCPDCOTCCIInventoryTemp') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_HSSCPDCOTCCIInventoryTemp;
		END
		
		-- Merge into primary inventory table
		MERGE 
			Parts.dbo.TBL_LocationInventory AS T
		USING 
			(SELECT 
				'00' + Unit AS LocationID
				,SKU
				,OnHandNewQuantity
				,'' AS OnHandLocation
				,AllocatedQuantity
				,NetAvailableQuantity
				,BackorderQuantity
				,OnOrderQuantity
				,PartCost
				,PartSellPrice
			FROM 
				Parts.dbo.TBL_HSSCPDCOTCCIInventory) AS S
		ON 
			(T.LocationID = S.LocationID
			AND
			T.SKU = S.SKU)
		WHEN MATCHED THEN
			UPDATE 
			SET 
				T.OnHandQuantity = S.OnHandNewQuantity
				,T.OnHandLocation = S.OnHandLocation
				,T.AllocatedQuantity = S.AllocatedQuantity
				,T.NetOnHandQuantity = S.NetAvailableQuantity
				,T.BackorderedQuantity = S.BackorderQuantity
				,T.OnOrderQuantity = S.OnOrderQuantity
				,T.PartCost = S.PartCost
				,T.PartSellPrice = S.PartSellPrice
		WHEN NOT MATCHED BY TARGET THEN
			INSERT 
				(LocationID
				,SKU
				,OnHandQuantity
				,OnHandLocation
				,AllocatedQuantity
				,NetOnHandQuantity
				,BackorderedQuantity
				,OnOrderQuantity
				,PartCost
				,PartSellPrice)
			VALUES 
				(S.LocationID
				,S.SKU
				,S.OnHandNewQuantity
				,S.OnHandLocation
				,S.AllocatedQuantity
				,S.NetAvailableQuantity
				,S.BackorderQuantity
				,S.OnOrderQuantity
				,S.PartCost
				,S.PartSellPrice);

		-- Merge into primary location table
		MERGE 
			Parts.dbo.TBL_Location AS T
		USING 
			(SELECT 
				NPSBU.Unit AS LocationID
				,Channel AS LocationType
				,Channel + ' - ' + UnitName AS LocationName
				,UnitStreetAddressLine1 AS AddressPartOne
				,UnitStreetAddressLine2 AS AddressPartTwo
				,UnitCityName AS City
				,UnitStateCode AS [State]
				,ZIPCode AS Zip
				,ZIPPlus4Code AS ZipPlusFour
				,CASE WHEN NPSBUGEO.Latitude IS NULL THEN 0 ELSE NPSBUGEO.Latitude END AS Latitude
				,CASE WHEN NPSBUGEO.Longitude IS NULL THEN 0 ELSE NPSBUGEO.Longitude END AS Longitude
				,CASE WHEN Channel = 'PDC' THEN 'TECH TRANSFER' ELSE 'P-CARD' END AS ChargeAccount
				,1 AS PickupCapable
				,1 AS ShipCapable
			FROM 
				Employee.dbo.TBL_NPSBaseUnit NPSBU
			INNER JOIN
				(SELECT DISTINCT 
					Channel
					,'00' + Unit AS Unit
				FROM 
					Parts.dbo.TBL_HSSCPDCOTCCIInventory) HSSCPDCOTCCI
			ON
				NPSBU.Unit = HSSCPDCOTCCI.Unit
			LEFT JOIN
				(SELECT 
					Unit
					,Latitude
					,Longitude
				FROM 
					Employee.dbo.TBL_NPSBaseUnitGeocode) NPSBUGEO
			ON
				NPSBU.Unit = NPSBUGEO.Unit) AS S
		ON 
			(T.LocationID = S.LocationID)
		WHEN NOT MATCHED BY TARGET THEN
			INSERT 
				(LocationID
				,LocationType
				,LocationName
				,AddressPartOne
				,AddressPartTwo
				,City
				,[State]
				,Zip
				,ZipPlusFour
				,Latitude
				,Longitude
				,ChargeAccount
				,PickupCapable
				,ShipCapable)
			VALUES 
				(S.LocationID
				,S.LocationType
				,S.LocationName
				,S.AddressPartOne
				,S.AddressPartTwo
				,S.City
				,S.[State]
				,S.Zip
				,S.ZipPlusFour
				,S.Latitude
				,S.Longitude
				,S.ChargeAccount
				,S.PickupCapable
				,S.ShipCapable);

		-- Remove extra records that no longer have supply chain inventories
		DELETE 
			LOC
		FROM
			Parts.dbo.TBL_Location LOC
		LEFT JOIN
			(SELECT DISTINCT 
				'00' + Unit AS LocationID
			FROM 
				Parts.dbo.TBL_HSSCPDCOTCCIInventory) HSSCPDCOTCCI
		ON
			LOC.LocationID = HSSCPDCOTCCI.LocationID
		WHERE
			LocationType IN ('CI', 'OTC', 'PDC')
			AND
			HSSCPDCOTCCI.LocationID IS NULL;
END
	
	
















GO
/****** Object:  StoredProcedure [dbo].[PROC_LPNInventoryImport]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







-- ==========================================================================================
-- Author:		Aaron Cote
-- Create date: 08/20/2015
-- Description:	Imports latest Local Provider Network (LPN) inventory data.
-- ==========================================================================================
CREATE PROCEDURE [dbo].[PROC_LPNInventoryImport]

AS
	BEGIN
	
		-- Merge into primary inventory table
		MERGE 
			Parts.dbo.TBL_LocationInventory AS T
		USING 
			(SELECT DISTINCT
				UPPER(SUBSTRING(LTRIM(RTRIM(INV.TI_Account_ID)), 11, 5)) + REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(UPPER(LTRIM(RTRIM(INV.TI_Location_ID))), ' ', ''), '(', ''), ')', ''), '#', ''), ',', ''), '-', '') AS LocationID
				,PartSKU AS SKU
				,TI_On_Hand_Qty AS OnHandQuantity
				,'' AS OnHandLocation
				,ISNULL(TI_Allocated_Qty, 0) AS AllocatedQuantity
				,TI_On_Hand_Qty - ISNULL(TI_Allocated_Qty, 0) AS NetOnHandQuantity
				,0 AS BackorderedQuantity
				,0 AS OnOrderQuantity
				,0 AS PartCost
				,CAST(TI_Cost_Amount AS DECIMAL(7,2)) AS PartSellPrice
			FROM 
				HS_Procurement.dbo.TBL_Inventory INV
			INNER JOIN
				(SELECT
					TI_Account_ID
					,TI_Location_ID
					,TI_Part_No
					,MAX(TI_On_Hand_Qty) AS MaxQty
					,MAX(TI_Cost_Amount) AS MaxCost
				FROM
					HS_Procurement.dbo.TBL_Inventory
				GROUP BY
					TI_Account_ID
					,TI_Location_ID
					,TI_Part_No) INVMAX
			ON
				INV.TI_Account_ID = INVMAX.TI_Account_ID
				AND
				INV.TI_Location_ID = INVMAX.TI_Location_ID
				AND
				INV.TI_Part_No = INVMAX.TI_Part_No
				AND
				INV.TI_On_Hand_Qty = INVMAX.MaxQty
				AND
				INV.TI_Cost_Amount = INVMAX.MaxCost
			INNER JOIN
				(SELECT 
					PartSKU
					,PART.PartNumber
				FROM 
					Parts.dbo.TBL_Part PART
				INNER JOIN
					(SELECT
						PartNumber
						,MAX(PartAddDate) AS MaxDate
					FROM
						Parts.dbo.TBL_Part
					GROUP BY
						PartNumber) MAXPART
				ON
					PART.PartNumber = MAXPART.PartNumber
					AND
					PART.PartAddDate = MAXPART.MaxDate) PART
			ON
				INV.TI_Part_No = PART.PartNumber
			WHERE
				SUBSTRING(INV.TI_Account_ID, 11, 5) <> '00SPR'
				AND
				INV.TI_Location_ID <> '') AS S
		ON 
			(T.LocationID = S.LocationID
			AND
			T.SKU = S.SKU)
		WHEN MATCHED THEN
			UPDATE 
			SET 
				T.OnHandQuantity = S.OnHandQuantity
				,T.OnHandLocation = S.OnHandLocation
				,T.AllocatedQuantity = S.AllocatedQuantity
				,T.NetOnHandQuantity = S.NetOnHandQuantity
				,T.BackorderedQuantity = S.BackorderedQuantity
				,T.OnOrderQuantity = S.OnOrderQuantity
				,T.PartCost = S.PartCost
				,T.PartSellPrice = S.PartSellPrice
		WHEN NOT MATCHED BY TARGET THEN
			INSERT 
				(LocationID
				,SKU
				,OnHandQuantity
				,OnHandLocation
				,AllocatedQuantity
				,NetOnHandQuantity
				,BackorderedQuantity
				,OnOrderQuantity
				,PartCost
				,PartSellPrice)
			VALUES 
				(S.LocationID
				,S.SKU
				,S.OnHandQuantity
				,S.OnHandLocation
				,S.AllocatedQuantity
				,S.NetOnHandQuantity
				,S.BackorderedQuantity
				,S.OnOrderQuantity
				,S.PartCost
				,S.PartSellPrice);

		-- Merge into primary location table
		MERGE 
			Parts.dbo.TBL_Location AS T
		USING 
			(SELECT 
				UPPER(LTRIM(RTRIM(LPNSL.AbbrAcctID))) + REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(UPPER(LTRIM(RTRIM(Location_ID))), ' ', ''), '(', ''), ')', ''), '#', ''), ',', ''), '-', '') AS LocationID
				,'LPN' AS LocationType
				,LEFT(UPPER(RTRIM(SupplierName)) + ' - ' + REPLACE(UPPER(RTRIM(Location_Name)), UPPER(RTRIM(SupplierName)), ''), 100) AS LocationName
				,UPPER(RTRIM(Addr1)) AS AddressPartOne
				,UPPER(RTRIM(Addr2)) AS AddressPartTwo
				,UPPER(RTRIM(City)) AS City
				,UPPER(RTRIM(St_CD)) AS [State]
				,CASE 
					WHEN LEN(RTRIM(Zip_CD)) = 4 THEN '0' + Zip_CD
					WHEN LEN(RTRIM(Zip_CD)) > 5 THEN LEFT(Zip_CD, 5)
				END AS Zip
				,Latitude
				,Longitude
				,CASE WHEN RTRIM(Phone_No) = '' THEN NULL ELSE REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(RTRIM(Phone_No), ' ', ''), '(', ''), ')', ''), '-', ''), '.', '') END AS Phone
				,CASE WHEN RTRIM(Alt_Phone_No) = '' THEN NULL ELSE REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(RTRIM(Alt_Phone_No), ' ', ''), '(', ''), ')', ''), '-', ''), '.', '') END AS AlternatePhone
				,CASE WHEN RTRIM(Alt_Phone_No_Ext) = '' THEN NULL ELSE REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(RTRIM(Alt_Phone_No_Ext), ' ', ''), '(', ''), ')', ''), '-', ''), '.', '') END AS AlternatePhoneExtension
				,CASE WHEN RTRIM(Fax_No) = '' THEN NULL ELSE REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(RTRIM(Fax_No), ' ', ''), '(', ''), ')', ''), '-', ''), '.', '') END AS Fax
				,CASE
					WHEN RTRIM(lpnAcctNo) = '' THEN NULL 
					WHEN RTRIM(lpnAcctNo) = 'P-Card Purchases Only Allowed' THEN 'P-Card' 
					ELSE RTRIM(lpnAcctNo) 
				END AS ChargeAccount
				,1 AS PickupCapable
				,CASE WHEN ShipCapability = 'Y' THEN 1 ELSE 0 END AS ShipCapable
			FROM 
				HS_Procurement.dbo.LPN_SupplierLocations LPNSL
			INNER JOIN
				(SELECT DISTINCT
					SUBSTRING(TI_Account_ID, 11, 5) AS AbbrAcctID
					,TI_Location_ID
				FROM 
					HS_Procurement.dbo.TBL_Inventory) INV
			ON
				LPNSL.AbbrAcctID = INV.AbbrAcctID
				AND
				LPNSL.Location_ID = INV.TI_Location_ID
			INNER JOIN
				(SELECT 
					SupplierName
					,AbbrAcctID
				FROM 
					HS_Procurement.dbo.LPN_SupplierInfo) LPNSI
			ON
				LPNSL.AbbrAcctID = LPNSI.AbbrAcctID
			WHERE
				Location_ID <> ''
				AND
				LPNSL.AbbrAcctID <> '00SPR'
				AND
				ISNUMERIC(Latitude) = 1
				AND
				ISNUMERIC(Longitude) = 1) AS S
		ON 
			(T.LocationID = S.LocationID)
		WHEN NOT MATCHED BY TARGET THEN
			INSERT 
				(LocationID
				,LocationType
				,LocationName
				,AddressPartOne
				,AddressPartTwo
				,City
				,[State]
				,Zip
				,Latitude
				,Longitude
				,ChargeAccount
				,PickupCapable
				,ShipCapable)
			VALUES 
				(S.LocationID
				,S.LocationType
				,S.LocationName
				,S.AddressPartOne
				,S.AddressPartTwo
				,S.City
				,S.[State]
				,S.Zip
				,S.Latitude
				,S.Longitude
				,S.ChargeAccount
				,S.PickupCapable
				,S.ShipCapable);
				
		-- Remove extra records that no longer have lpn inventories
		DELETE 
			LOC
		FROM
			Parts.dbo.TBL_Location LOC
		LEFT JOIN
			(SELECT DISTINCT
				UPPER(LTRIM(RTRIM(LPNSL.AbbrAcctID))) + REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(UPPER(LTRIM(RTRIM(Location_ID))), ' ', ''), '(', ''), ')', ''), '#', ''), ',', ''), '-', '') AS LocationID
			FROM 
				HS_Procurement.dbo.LPN_SupplierLocations LPNSL
			INNER JOIN
				(SELECT DISTINCT
					SUBSTRING(TI_Account_ID, 11, 5) AS AbbrAcctID
					,TI_Location_ID
				FROM 
					HS_Procurement.dbo.TBL_Inventory) INV
			ON
				LPNSL.AbbrAcctID = INV.AbbrAcctID
				AND
				LPNSL.Location_ID = INV.TI_Location_ID
			INNER JOIN
				(SELECT 
					SupplierName
					,AbbrAcctID
				FROM 
					HS_Procurement.dbo.LPN_SupplierInfo
				WHERE
					[Status] = 'A') LPNSI
			ON
				LPNSL.AbbrAcctID = LPNSI.AbbrAcctID
			WHERE
				Location_ID <> ''
				AND
				LPNSL.AbbrAcctID <> '00SPR') LPN
		ON
			LOC.LocationID = LPN.LocationID
		WHERE
			LocationType = ('LPN')
			AND
			LPN.LocationID IS NULL;
	END





GO
/****** Object:  StoredProcedure [dbo].[PROC_SupplyChainPartMasterImport]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- ==========================================================================================
-- Author:		Aaron Cote
-- Create date: 08/10/2015
-- Description:	Imports supply chain master parts lists.
-- ==========================================================================================
CREATE PROCEDURE [dbo].[PROC_SupplyChainPartMasterImport]

AS
	BEGIN
		DECLARE @mycursor CURSOR
		DECLARE @partdivision CHAR(3) = NULL;
		DECLARE @querystring NVARCHAR(MAX) = NULL;
		
		IF object_id('tempdb..#TBL_SupplyChainPartMasterTemp') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_SupplyChainPartMasterTemp;
		END
		
		CREATE TABLE 
			#TBL_SupplyChainPartMasterTemp
			([PartSKU] [varchar](30) NOT NULL,
			[PartDivision] [char](3) NOT NULL,
			[PartListSource] [char](3) NOT NULL,
			[PartNumber] [varchar](24) NOT NULL,
			[PartDescription] [varchar](20) NOT NULL,
			[PartSourceOrderNumber] [char](4) NOT NULL,
			[PartAvailability] [char](1) NOT NULL,
			[PartSellPrice] [decimal](7, 2) NOT NULL,
			[PartCost] [decimal](7, 2) NOT NULL,
			[PartPriceModifyDate] [date] NOT NULL,
			[HazardousMaterialCatalogCode] [char](1) NOT NULL,
			[PartAddDate] [date] NOT NULL,
			[SubPartSKU] [varchar](30) NOT NULL,
			[SubPartDivision] [char](3) NOT NULL,
			[SubPartListSource] [char](3) NOT NULL,
			[SubPart] [varchar](24) NOT NULL,
			[SubPartWayCode] [varchar](1) NOT NULL);
		
		-- Create table to contain list of divs.
		-- Data will be pulled into temp table 1 div at a time
		-- so that the entire dataset of several million records
		-- is not pulled all at once.
		
		IF object_id('tempdb..#TBL_SupplyChainPartMasterDivsTemp') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_SupplyChainPartMasterDivsTemp;
		END
		
		CREATE TABLE 
			#TBL_SupplyChainPartMasterDivsTemp
			([PartDivision] [char](3) NOT NULL);
		
		SET @querystring = 'INSERT INTO #TBL_SupplyChainPartMasterDivsTemp (PartDivision)'

		SET @querystring = @querystring + ' SELECT PRT_DIV_NO FROM OPENQUERY(DB2_SUPPLYCHAIN, '

		SET @querystring = @querystring + ' ''SELECT PRT_DIV_NO FROM RFSADM.SCDMTPM_PRT_MST WHERE PRT_DIV_NO IS NOT NULL GROUP BY PRT_DIV_NO;'''

		SET @querystring = @querystring + ')'
		
		-- Populate temp div table.
		EXEC(@querystring);
		
		-- Loop through divs
		SET @mycursor = CURSOR FAST_FORWARD 
		FOR 
			SELECT
				PartDivision
			FROM
				#TBL_SupplyChainPartMasterDivsTemp
			ORDER BY
				PartDivision
				
		OPEN @mycursor 
		FETCH NEXT FROM @mycursor 
		INTO @partdivision

		WHILE @@FETCH_STATUS = 0 
		BEGIN 

			SET @querystring = ''
		
			SET @querystring = 'INSERT INTO #TBL_SupplyChainPartMasterTemp (PartSKU, PartDivision, PartListSource, PartNumber, PartDescription, PartSourceOrderNumber, PartAvailability, PartSellPrice, PartCost, PartPriceModifyDate, HazardousMaterialCatalogCode, PartAddDate, SubPartSKU, SubPartDivision, SubPartListSource, SubPart, SubPartWayCode)'

			SET @querystring = @querystring + ' SELECT LTRIM(RTRIM(PRT_DIV_NO)) + LTRIM(RTRIM(PRT_PRC_LIS_SRC_NO)) + LTRIM(RTRIM(PRT_NO)) AS PRT_SKU, LTRIM(RTRIM(PRT_DIV_NO)) AS PRT_DIV_NO, LTRIM(RTRIM(PRT_PRC_LIS_SRC_NO)) AS PRT_PRC_LIS_SRC_NO, LTRIM(RTRIM(PRT_NO)) AS PRT_NO, LTRIM(RTRIM(PRT_DS)) AS PRT_DS, LTRIM(RTRIM(SRC_ORD_NO)) AS SRC_ORD_NO, LTRIM(RTRIM(PRT_AVL_CD)) AS PRT_AVL_CD, LTRIM(RTRIM(PRT_SLL_PRC_AM)) AS PRT_SLL_PRC_AM, LTRIM(RTRIM(PRT_CST_AM)) AS PRT_CST_AM, LTRIM(RTRIM(PRT_PRC_MOD_DT)) AS PRT_PRC_MOD_DT, LTRIM(RTRIM(HZD_MTL_CAT_CD)) AS HZD_MTL_CAT_CD, LTRIM(RTRIM(PRT_ADD_DT)) AS PRT_ADD_DT, LTRIM(RTRIM(SUB_PRT_DIV_NO)) + LTRIM(RTRIM(SUB_PRC_LIS_SRC_NO)) + LTRIM(RTRIM(SUB_PRT_NO)) AS SUB_PRT_SKU, LTRIM(RTRIM(SUB_PRT_DIV_NO)) AS SUB_PRT_DIV_NO, LTRIM(RTRIM(SUB_PRC_LIS_SRC_NO)) AS SUB_PRC_LIS_SRC_NO, LTRIM(RTRIM(SUB_PRT_NO)) AS SUB_PRT_NO, LTRIM(RTRIM(SUB_WAY_CD)) AS SUB_WAY_CD FROM OPENQUERY(DB2_SUPPLYCHAIN, '

			SET @querystring = @querystring + ' ''SELECT PRT_DIV_NO, PRT_PRC_LIS_SRC_NO, PRT_NO, PRT_DS, SRC_ORD_NO, PRT_AVL_CD, PRT_SLL_PRC_AM, PRT_CST_AM, PRT_PRC_MOD_DT, HZD_MTL_CAT_CD, PRT_ADD_DT, SUB_PRT_DIV_NO, SUB_PRC_LIS_SRC_NO, SUB_PRT_NO, SUB_WAY_CD FROM RFSADM.SCDMTPM_PRT_MST WHERE PRT_DIV_NO = ''''' + @partdivision + ''''' AND PRT_PRC_LIS_SRC_NO IS NOT NULL AND PRT_NO IS NOT NULL;'''

			SET @querystring = @querystring + ')'
			
			-- Populate temp table.
			EXEC(@querystring);

		FETCH NEXT FROM @mycursor 
		INTO @partdivision
		END 

		CLOSE @mycursor 
		DEALLOCATE @mycursor

		-- Merge into primary table
		MERGE 
			Parts.dbo.TBL_SupplyChainPartMaster AS T
		USING 
			(SELECT 
				PartSKU
				,PartDivision
				,PartListSource
				,PartNumber
				,PartDescription
				,PartSourceOrderNumber
				,PartAvailability
				,PartSellPrice
				,PartCost
				,PartPriceModifyDate
				,HazardousMaterialCatalogCode
				,PartAddDate
				,SubPartSKU
				,SubPartDivision
				,SubPartListSource
				,SubPart
				,SubPartWayCode
			FROM 
				#TBL_SupplyChainPartMasterTemp) AS S
		ON 
			(T.PartSKU = S.PartSKU)
		WHEN MATCHED THEN
			UPDATE 
			SET 
				T.PartDivision = S.PartDivision
				,T.PartListSource = S.PartListSource
				,T.PartNumber = S.PartNumber
				,T.PartDescription = S.PartDescription
				,T.PartSourceOrderNumber = S.PartSourceOrderNumber
				,T.PartAvailability = S.PartAvailability
				,T.PartSellPrice = S.PartSellPrice
				,T.PartCost = S.PartCost
				,T.PartPriceModifyDate = S.PartPriceModifyDate
				,T.HazardousMaterialCatalogCode = S.HazardousMaterialCatalogCode
				,T.PartAddDate = S.PartAddDate
				,T.SubPartSKU = S.SubPartSKU
				,T.SubPartDivision = S.SubPartDivision
				,T.SubPartListSource = S.SubPartListSource
				,T.SubPart = S.SubPart
				,T.SubPartWayCode = S.SubPartWayCode
		WHEN NOT MATCHED BY TARGET THEN
			INSERT 
				(PartSKU
				,PartDivision
				,PartListSource
				,PartNumber
				,PartDescription
				,PartSourceOrderNumber
				,PartAvailability
				,PartSellPrice
				,PartCost
				,PartPriceModifyDate
				,HazardousMaterialCatalogCode
				,PartAddDate
				,SubPartSKU
				,SubPartDivision
				,SubPartListSource
				,SubPart
				,SubPartWayCode)
			VALUES 
				(S.PartSKU
				,S.PartDivision
				,S.PartListSource
				,S.PartNumber
				,S.PartDescription
				,S.PartSourceOrderNumber
				,S.PartAvailability
				,S.PartSellPrice
				,S.PartCost
				,S.PartPriceModifyDate
				,S.HazardousMaterialCatalogCode
				,S.PartAddDate
				,S.SubPartSKU
				,S.SubPartDivision
				,S.SubPartListSource
				,S.SubPart
				,S.SubPartWayCode)
		WHEN NOT MATCHED BY SOURCE THEN
			DELETE;
				
		IF object_id('tempdb..#TBL_SupplyChainPartMasterTemp') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_SupplyChainPartMasterTemp;
		END
		
		IF object_id('tempdb..#TBL_SupplyChainPartMasterDivsTemp') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_SupplyChainPartMasterDivsTemp;
		END
END
	
	











GO
/****** Object:  UserDefinedFunction [dbo].[GetWorkingDays]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[GetWorkingDays]  
(  
    @StartDate date,  
    @EndDate smalldatetime  
) 
RETURNS INT  
AS  
BEGIN
	RETURN ((DATEDIFF(dd, @StartDate, @EndDate) + 1) - (DATEDIFF(wk, @StartDate, @EndDate) * 2) - (CASE WHEN DATEPART(dw, @StartDate) = 1 THEN 1 ELSE 0 END) - (CASE WHEN DATEPART(dw, @EndDate) = 7 THEN 1 ELSE 0 END))

  
/*
	OLD
	BEGIN 
		DECLARE @range INT; 
	 
		SET @range = DATEDIFF(DAY, CONVERT(CHAR(8),@StartDate, 112), CONVERT(CHAR(8),@EndDate, 112))+1; 
	 
		RETURN  
		( 
			SELECT  
				@range / 7 * 5 + @range % 7 -  
				( 
					SELECT COUNT(*)  
				FROM 
					( 
						SELECT 1 AS d 
						UNION ALL SELECT 2  
						UNION ALL SELECT 3  
						UNION ALL SELECT 4  
						UNION ALL SELECT 5  
						UNION ALL SELECT 6  
						UNION ALL SELECT 7 
					) weekdays 
					WHERE d <= @range % 7  
					AND DATENAME(WEEKDAY, @EndDate - d + 1)  
					IN 
					( 
						'Saturday', 
						'Sunday' 
					) 
				) 
		); 
	END  

*/

END

GO
/****** Object:  Table [dbo].[image_lookup$]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[image_lookup$](
	[itm_id] [nvarchar](255) NULL,
	[prd_gro_id] [nvarchar](255) NULL,
	[spp_id] [nvarchar](255) NULL,
	[orb_itm_id] [nvarchar](255) NULL,
	[prt_img_url] [nvarchar](255) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[nomatch$]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[nomatch$](
	[Barcode#] [nvarchar](255) NULL,
	[RMA#] [nvarchar](255) NULL,
	[SKU#] [nvarchar](255) NULL,
	[District#] [nvarchar](255) NULL,
	[Truck#] [nvarchar](255) NULL,
	[F6] [nvarchar](255) NULL,
	[F7] [nvarchar](255) NULL,
	[F8] [nvarchar](255) NULL,
	[F9] [nvarchar](255) NULL,
	[F10] [nvarchar](255) NULL,
	[F11] [nvarchar](255) NULL,
	[F12] [nvarchar](255) NULL,
	[F13] [nvarchar](255) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Query1]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Query1](
	[Div] [varchar](50) NULL,
	[Pls] [varchar](50) NULL,
	[Part] [varchar](50) NULL,
	[length] [varchar](50) NULL,
	[width] [varchar](50) NULL,
	[height] [varchar](50) NULL,
	[weight] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ReasotoValues$]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReasotoValues$](
	[BarcodePartNumber] [nvarchar](255) NULL,
	[UpdateTo] [nvarchar](255) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[rtn_approval]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[rtn_approval](
	[District] [varchar](3000) NULL,
	[Truck] [varchar](3000) NULL,
	[Tech] [varchar](3000) NULL,
	[RMA] [varchar](3000) NULL,
	[RMALine] [varchar](3000) NULL,
	[OldSKU] [varchar](3000) NULL,
	[OldPartDescription] [varchar](3000) NULL,
	[NewSKU] [varchar](3000) NULL,
	[NewPartDescription] [varchar](3000) NULL,
	[CostAmount] [varchar](3000) NULL,
	[RFSBarcode] [varchar](3000) NULL,
	[ServiceOrder] [varchar](3000) NULL,
	[PartReturnQuantity] [varchar](3000) NULL,
	[partreturnreasoncode] [varchar](3000) NULL,
	[Partreturnstatus] [varchar](3000) NULL,
	[newpartreasoncode] [varchar](3000) NULL,
	[binnumber] [varchar](3000) NULL,
	[transactiondate] [varchar](3000) NULL,
	[approvalstatus] [varchar](3000) NULL,
	[ApprovalComments] [varchar](3000) NULL,
	[ApprovalUser] [varchar](3000) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_AIMAccountingTransactions]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[TBL_AIMAccountingTransactions](
	[AccountingTransactionDate] [date] NULL,
	[SofteonTransactionDate] [date] NULL,
	[SofteonTransactionTime] [timestamp] NULL,
	[UnitNumber] [char](7) NOT NULL,
	[AccountNumber] [varchar](5) NOT NULL,
	[PartDivision] [char](3) NULL,
	[PartListSource] [char](3) NULL,
	[PartNumber] [varchar](24) NULL,
	[SKU] [varchar](30) NOT NULL,
	[PartDescription] [varchar](15) NULL,
	[PartQuantity] [int] NULL,
	[CostAmount] [decimal](8, 2) NOT NULL,
	[SellPrice] [decimal](8, 2) NOT NULL,
	[TransactionReason] [varchar](100) NULL,
	[SofteonTransactionNumber] [varchar](12) NOT NULL,
	[ServiceOrder] [varchar](8) NULL,
	[Truck] [varchar](7) NULL,
	[Tech] [varchar](7) NULL,
	[User] [varchar](20) NULL,
	[ImportDate] [datetime] NULL,
 CONSTRAINT [PK_TBL_AIMAccountingTransactions] PRIMARY KEY NONCLUSTERED 
(
	[SofteonTransactionNumber] ASC,
	[UnitNumber] ASC,
	[AccountNumber] ASC,
	[SKU] ASC,
	[CostAmount] ASC,
	[SellPrice] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_AIMAccountingTransactionsFallout]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_AIMAccountingTransactionsFallout](
	[AccountingTransactionDate] [date] NOT NULL,
	[UnitNumber] [varchar](7) NOT NULL,
	[OtherUnitNumber] [varchar](7) NULL,
	[PartDivision] [char](3) NULL,
	[PartListSource] [char](3) NULL,
	[PartNumber] [varchar](24) NULL,
	[SKU] [varchar](30) NOT NULL,
	[PartDescription] [varchar](15) NULL,
	[PartQuantity] [int] NULL,
	[CostAmount] [decimal](8, 2) NULL,
	[SellPrice] [decimal](8, 2) NULL,
	[TransactionCode] [varchar](3) NULL,
	[TransactionDescription] [varchar](200) NULL,
	[ProcessCode] [varchar](1) NULL,
	[TransactionFalloutReason] [varchar](1500) NULL,
	[TransactionID] [varchar](15) NOT NULL,
	[ImportDate] [datetime] NOT NULL,
 CONSTRAINT [Pk_AIMAccountingTransactionsFallout] PRIMARY KEY CLUSTERED 
(
	[SKU] ASC,
	[TransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_AIMClaimExtract]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_AIMClaimExtract](
	[RequiredReturnDate] [date] NOT NULL,
	[FromUnitNumber] [varchar](7) NOT NULL,
	[ServiceOrder] [varchar](8) NOT NULL,
	[PartSourceCode] [varchar](5) NULL,
	[OrderItemNumber] [varchar](30) NOT NULL,
	[ReceiveItemNumber] [varchar](30) NULL,
	[TrackingNumber] [varchar](30) NULL,
	[ReverseFlowCode] [varchar](10) NULL,
	[ReturnOrderNumber] [varchar](15) NOT NULL,
	[PurchaseOrderNumber] [varchar](10) NULL,
	[CostAmount] [decimal](8, 2) NULL,
	[SellPrice] [decimal](8, 2) NULL,
	[CoreValueAmount] [decimal](8, 2) NULL,
	[ReasonCode] [varchar](3) NULL,
	[PSSCode] [varchar](1) NULL,
 CONSTRAINT [PK_TBL_AIMClaimExtract] PRIMARY KEY CLUSTERED 
(
	[FromUnitNumber] ASC,
	[ServiceOrder] ASC,
	[OrderItemNumber] ASC,
	[ReturnOrderNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_AIMDistrictInventory]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_AIMDistrictInventory](
	[District] [varchar](7) NOT NULL,
	[Tech] [varchar](7) NULL,
	[Truck] [varchar](7) NOT NULL,
	[RFSBarcode] [varchar](20) NOT NULL,
	[RGI] [varchar](80) NULL,
	[PartDivision] [varchar](3) NULL,
	[PartListSource] [varchar](3) NULL,
	[PartNumber] [varchar](24) NULL,
	[SKU] [varchar](30) NOT NULL,
	[PartDescription] [varchar](20) NULL,
	[Quantity] [int] NULL,
	[CostAmount] [decimal](8, 2) NULL,
	[BinNumber] [varchar](5) NULL,
	[ASNNumber] [varchar](30) NOT NULL,
	[CoreFlag] [varchar](10) NULL,
	[UPSTrackingNumber] [varchar](30) NULL,
	[ReturnStatusDate] [date] NULL,
	[DroppedDistrictDate] [date] NULL,
	[ReceivedDistrictDate] [date] NULL,
	[StatusPlacedDate] [date] NULL,
	[SG01BinAddDate] [date] NULL,
	[SG01ShippedOutDate] [date] NULL,
	[ReceiveBinPlacedDate] [date] NULL,
	[PDCShippedDate] [date] NULL,
	[WISTransDate] [date] NULL,
	[WISApprovedUser] [varchar](12) NULL,
	[WISApprovedDate] [date] NULL,
	[RejectedUser] [varchar](20) NULL,
	[RejectedDate] [date] NULL,
	[ModifyDate] [date] NOT NULL,
 CONSTRAINT [pk_AIMDistrictInventory] PRIMARY KEY CLUSTERED 
(
	[District] ASC,
	[Truck] ASC,
	[RFSBarcode] ASC,
	[ASNNumber] ASC,
	[SKU] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_AIMEndToEndUPS]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_AIMEndToEndUPS](
	[District] [varchar](7) NOT NULL,
	[Truck] [varchar](7) NOT NULL,
	[Tech] [varchar](7) NULL,
	[Barcode] [varchar](30) NOT NULL,
	[RMANumber] [varchar](20) NOT NULL,
	[RGINumber] [varchar](25) NULL,
	[UPSTrackingNumber] [varchar](30) NULL,
	[UPSFlag] [varchar](10) NULL,
	[PartDivision] [char](3) NULL,
	[PartListSource] [char](3) NULL,
	[PartNumber] [varchar](24) NULL,
	[SKU] [varchar](30) NOT NULL,
	[PartDescription] [varchar](20) NULL,
	[OrderQuantity] [int] NOT NULL,
	[CostAmount] [decimal](8, 2) NULL,
	[ShipToPDC] [varchar](10) NULL,
	[ReturnScanDate] [date] NULL,
	[ReturnShipDate] [date] NULL,
	[ReturnPickupDate] [date] NULL,
	[ReturnDeliveryDate] [date] NULL,
	[PDCPullFlag] [varchar](3) NULL,
	[PDCRecordCloseDate] [date] NULL,
	[Status] [varchar](10) NOT NULL,
	[Reason] [varchar](50) NOT NULL,
	[ExpectedSKU] [varchar](30) NOT NULL,
	[Core] [char](1) NOT NULL,
	[UserID] [varchar](12) NULL,
	[LastModifyDate] [datetime] NOT NULL,
	[ShippingStatus] [varchar](20) NULL,
 CONSTRAINT [Pk_AIMEndToEndUPS] PRIMARY KEY CLUSTERED 
(
	[District] ASC,
	[Truck] ASC,
	[Barcode] ASC,
	[RMANumber] ASC,
	[SKU] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_AIMEndToEndUPS_Duplicates]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_AIMEndToEndUPS_Duplicates](
	[District] [varchar](7) NOT NULL,
	[Truck] [varchar](7) NOT NULL,
	[Tech] [varchar](7) NULL,
	[Barcode] [varchar](30) NOT NULL,
	[RMANumber] [varchar](20) NOT NULL,
	[RGINumber] [varchar](25) NULL,
	[UPSTrackingNumber] [varchar](30) NULL,
	[UPSFlag] [varchar](10) NULL,
	[PartDivision] [char](3) NULL,
	[PartListSource] [char](3) NULL,
	[PartNumber] [varchar](24) NULL,
	[SKU] [varchar](30) NOT NULL,
	[PartDescription] [varchar](20) NULL,
	[OrderQuantity] [int] NOT NULL,
	[CostAmount] [decimal](8, 2) NULL,
	[ShipToPDC] [varchar](10) NULL,
	[ReturnScanDate] [date] NULL,
	[ReturnShipDate] [date] NULL,
	[ReturnPickupDate] [date] NULL,
	[ReturnDeliveryDate] [date] NULL,
	[PDCPullFlag] [varchar](3) NULL,
	[PDCRecordCloseDate] [date] NULL,
	[Status] [varchar](10) NOT NULL,
	[Reason] [varchar](50) NOT NULL,
	[ExpectedSKU] [varchar](30) NOT NULL,
	[Core] [char](1) NOT NULL,
	[UserID] [varchar](12) NULL,
	[LastModifyDate] [datetime] NOT NULL,
	[ShippingStatus] [varchar](20) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_AIMEndToEndUPS_Parts]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_AIMEndToEndUPS_Parts](
	[EndToEndPartID] [int] IDENTITY(1,1) NOT NULL,
	[EndToEndShipmentID] [int] NOT NULL,
	[PartDivision] [char](3) NULL,
	[PartListSource] [char](3) NULL,
	[PartNumber] [varchar](24) NULL,
	[SKU] [varchar](30) NOT NULL,
	[PartDescription] [varchar](20) NULL,
	[OrderQuantity] [int] NOT NULL,
	[CostAmount] [decimal](8, 2) NULL,
	[ShipToPDC] [varchar](10) NULL,
	[ReturnScanDate] [date] NULL,
	[ReturnShipDate] [date] NULL,
	[ReturnPickupDate] [date] NULL,
	[ReturnDeliveryDate] [date] NULL,
	[PDCPullFlag] [varchar](3) NULL,
	[PDCRecordCloseDate] [date] NULL,
	[Status] [varchar](10) NOT NULL,
	[Reason] [varchar](50) NOT NULL,
	[ExpectedSKU] [varchar](30) NOT NULL,
	[Core] [char](1) NOT NULL,
	[UserID] [varchar](12) NULL,
	[ShippingStatus] [varchar](20) NULL,
	[LastModifyDate] [datetime] NOT NULL,
 CONSTRAINT [Pk_AIMEndToEndUPS_EndToEndPartID] PRIMARY KEY CLUSTERED 
(
	[EndToEndPartID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_AIMEndToEndUPS_Shipment]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_AIMEndToEndUPS_Shipment](
	[EndToEndShipmentID] [int] IDENTITY(1,1) NOT NULL,
	[UPSTrackingNumberID] [int] NOT NULL,
	[District] [varchar](7) NOT NULL,
	[Truck] [varchar](7) NOT NULL,
	[Tech] [varchar](7) NULL,
	[Barcode] [varchar](30) NOT NULL,
	[RMANumber] [varchar](20) NOT NULL,
	[RGINumber] [varchar](25) NULL,
	[LastModifyDate] [datetime] NOT NULL,
 CONSTRAINT [Pk_AIMEndToEndUPS_EndToEndShipmentID] PRIMARY KEY CLUSTERED 
(
	[EndToEndShipmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_AIMEndToEndUPS_UPSTrackingNumber]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_AIMEndToEndUPS_UPSTrackingNumber](
	[UPSTrackingNumberID] [int] IDENTITY(1,1) NOT NULL,
	[UPSTrackingNumber] [varchar](30) NOT NULL,
	[UPSFlag] [varchar](10) NULL,
	[LastModifyDate] [datetime] NOT NULL,
 CONSTRAINT [Pk_AIMEndToEndUPS_UPSTrackingNumberID] PRIMARY KEY CLUSTERED 
(
	[UPSTrackingNumberID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_AIMErrorReport]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_AIMErrorReport](
	[SequenceNumber] [int] NOT NULL,
	[District] [varchar](7) NULL,
	[Tech] [varchar](7) NULL,
	[EnterpriseID] [varchar](8) NULL,
	[Truck] [varchar](7) NULL,
	[MethodName] [varchar](20) NULL,
	[ErrorCode] [varchar](6) NULL,
	[ErrorDescription] [varchar](1000) NULL,
	[PartDivision] [char](3) NULL,
	[PartListSource] [char](3) NULL,
	[PartNumber] [varchar](24) NULL,
	[SKU] [varchar](30) NOT NULL,
	[PartQuantity] [int] NOT NULL,
	[TruckBin] [varchar](6) NOT NULL,
	[TrackingNumber] [varchar](30) NULL,
	[RFSBarcode] [varchar](20) NULL,
	[ShipmentNumber] [varchar](30) NULL,
	[TaskNumber] [varchar](15) NULL,
	[TaskLineNumber] [varchar](3) NULL,
	[InsertDate] [datetime] NOT NULL,
 CONSTRAINT [PK_TBL_AIMErrorReport] PRIMARY KEY CLUSTERED 
(
	[SequenceNumber] ASC,
	[SKU] ASC,
	[PartQuantity] ASC,
	[TruckBin] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_AIMInventoryLevelBinDomsNew]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_AIMInventoryLevelBinDomsNew](
	[SKU] [varchar](30) NULL,
	[Truck] [varchar](8) NOT NULL,
	[InventoryType] [varchar](5) NULL,
	[PartQuantity] [int] NULL,
	[Unknown] [varchar](7) NULL,
	[SourceDate] [date] NULL,
	[District] [varchar](7) NULL,
	[BinNumber] [varchar](3) NOT NULL,
	[PullDate] [datetime] NULL,
 CONSTRAINT [PK_TBL_AIMInventoryLevelBinDomsNew] PRIMARY KEY CLUSTERED 
(
	[Truck] ASC,
	[BinNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_AIMInventoryLevelLocation]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_AIMInventoryLevelLocation](
	[SourceDate] [date] NOT NULL,
	[Region] [varchar](7) NULL,
	[District] [varchar](7) NULL,
	[Truck] [varchar](7) NOT NULL,
	[Tech] [varchar](7) NULL,
	[PartQuantity] [int] NULL,
	[TotalQuantity] [varchar](50) NULL,
	[PartReceiveQuantity] [int] NULL,
	[PartReturnQuantity] [int] NULL,
	[PartQAQuantity] [int] NULL,
	[TRFQuantity] [varchar](7) NULL,
	[UNIQuantity] [varchar](7) NULL,
	[INTQuantity] [varchar](7) NULL,
	[TotalValue] [decimal](18, 2) NULL,
	[Status] [varchar](20) NULL,
 CONSTRAINT [PK_TBL_AIMInventoryLevelLocation] PRIMARY KEY CLUSTERED 
(
	[Truck] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_AIMReceiveTasks]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_AIMReceiveTasks](
	[Truck] [varchar](6) NULL,
	[TaskReferenceNumber] [varchar](30) NOT NULL,
	[TaskReferenceLineNumber] [int] NOT NULL,
	[TaskDate] [date] NULL,
	[DeliveryDate] [date] NULL,
	[PartDivision] [char](3) NULL,
	[PartListSource] [char](3) NULL,
	[PartNumber] [varchar](24) NULL,
	[SKU] [varchar](30) NULL,
	[PartDescription] [varchar](20) NULL,
	[TaskQuantity] [int] NULL,
	[CompletedQuantity] [int] NULL,
	[TaskStatus] [varchar](5) NULL,
	[TrackingNumber] [varchar](30) NULL,
	[ServiceOrder] [varchar](8) NULL,
	[District] [varchar](7) NULL,
	[Tech] [varchar](7) NULL,
	[BinNumber] [varchar](3) NULL,
	[FileName] [varchar](200) NULL,
	[TaskClosedDate] [date] NULL,
 CONSTRAINT [PK_TBL_AIMReceiveTasks] PRIMARY KEY CLUSTERED 
(
	[TaskReferenceNumber] ASC,
	[TaskReferenceLineNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_AIMReturnApproval]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_AIMReturnApproval](
	[District] [varchar](7) NOT NULL,
	[Truck] [varchar](7) NULL,
	[Tech] [varchar](7) NULL,
	[RMA] [varchar](30) NULL,
	[RMALine] [int] NULL,
	[OldSKU] [varchar](30) NULL,
	[OldPartDescription] [varchar](20) NULL,
	[NewSKU] [varchar](30) NULL,
	[NewPartDescription] [varchar](20) NULL,
	[CostAmount] [decimal](8, 2) NULL,
	[UpdatedCostAmount] [decimal](8, 2) NULL,
	[UpdatedSellAmount] [decimal](8, 2) NULL,
	[RFSBarcode] [varchar](20) NOT NULL,
	[ServiceOrder] [varchar](10) NULL,
	[PartReturnQuantity] [int] NULL,
	[PartReturnReasonCode] [varchar](3) NULL,
	[PartReturnStatus] [varchar](3) NULL,
	[NewPartReasonCode] [varchar](3) NULL,
	[BinNumber] [varchar](3) NULL,
	[TransactionDate] [date] NULL,
	[ApprovalUser] [varchar](12) NULL,
	[ApprovalStatus] [varchar](20) NULL,
	[ApprovalComments] [varchar](1000) NULL,
	[UpdateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_TBL_AIMReturnApproval] PRIMARY KEY CLUSTERED 
(
	[RFSBarcode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_AIMReturnTasks]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_AIMReturnTasks](
	[Truck] [varchar](6) NOT NULL,
	[TaskReferenceNumber] [varchar](30) NOT NULL,
	[TaskReferenceLineNumber] [int] NOT NULL,
	[TaskDate] [date] NULL,
	[PartDivision] [char](3) NULL,
	[PartListSource] [char](3) NULL,
	[PartNumber] [varchar](24) NULL,
	[SKU] [varchar](30) NULL,
	[PartDescription] [varchar](20) NULL,
	[TaskQuantity] [int] NULL,
	[ReturnOrderNumber] [varchar](15) NULL,
	[RMANumber] [varchar](15) NULL,
	[District] [varchar](7) NULL,
	[Tech] [varchar](7) NULL,
	[ReturnReasonCode] [varchar](3) NULL,
	[BinNumber] [varchar](3) NULL,
	[FileName] [varchar](200) NULL,
	[RFSBarcode] [varchar](20) NULL,
	[TaskClosedDate] [date] NULL,
 CONSTRAINT [PK_TBL_AIMOpenReturnTasks] PRIMARY KEY CLUSTERED 
(
	[Truck] ASC,
	[TaskReferenceNumber] ASC,
	[TaskReferenceLineNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_AIMRGIDetail]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_AIMRGIDetail](
	[Region] [varchar](7) NULL,
	[District] [varchar](7) NOT NULL,
	[Tech] [nvarchar](7) NOT NULL,
	[FromUnit] [varchar](7) NULL,
	[ToUnit] [varchar](7) NULL,
	[ShipmentNumber] [varchar](30) NULL,
	[ShipStatus] [varchar](20) NULL,
	[SourceDate] [date] NOT NULL,
	[PartReceiptStatus] [varchar](15) NULL,
	[ReasonCode] [varchar](3) NULL,
	[Type] [varchar](3) NULL,
	[CoreVendor] [varchar](10) NULL,
	[CoreValueAmount] [decimal](8, 2) NULL,
	[Barcode] [varchar](10) NOT NULL,
	[PartDivision] [char](3) NULL,
	[PartListSource] [char](3) NULL,
	[PartNumber] [varchar](24) NULL,
	[SKU] [varchar](30) NOT NULL,
	[PartDescription] [varchar](15) NULL,
	[CostAmount] [decimal](8, 2) NULL,
	[SellPrice] [decimal](8, 2) NULL,
	[ReturnOrderNumber] [varchar](15) NOT NULL,
	[TrackingNumber] [varchar](50) NULL,
 CONSTRAINT [pk_AIMRGIDetail] PRIMARY KEY CLUSTERED 
(
	[District] ASC,
	[Tech] ASC,
	[SourceDate] ASC,
	[Barcode] ASC,
	[SKU] ASC,
	[ReturnOrderNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_AIMSSTUniBin]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_AIMSSTUniBin](
	[ServiceOrder] [varchar](8) NOT NULL,
	[UnitNumber] [varchar](7) NULL,
	[OrderType] [varchar](3) NULL,
	[ShipFromName] [varchar](55) NULL,
	[ShipFromAddress1] [varchar](35) NULL,
	[ShipFromAddress2] [varchar](35) NULL,
	[ShipFromCity] [varchar](35) NULL,
	[ShipFromState] [varchar](2) NULL,
	[ShipFromZip] [varchar](10) NULL,
	[ServiceOrderActionDate] [date] NULL,
	[Truck] [varchar](7) NULL,
	[PartSequenceNumber] [int] NOT NULL,
	[PartDivision] [char](3) NULL,
	[PartListSource] [char](3) NULL,
	[PartNumber] [varchar](24) NOT NULL,
	[PartDescription] [varchar](15) NULL,
	[SKU] [varchar](30) NOT NULL,
	[OrderLineNumber] [int] NULL,
	[OrderLineQuantity] [int] NULL,
	[ReasonCode] [varchar](3) NULL,
	[DispositionCode] [varchar](2) NULL,
	[BinNumber] [varchar](3) NULL,
	[UPCCode] [varchar](13) NULL,
	[PartDivisionReceived] [char](3) NULL,
	[PartListSourceReceived] [char](3) NULL,
	[PartNumberReceived] [varchar](24) NULL,
	[SKUReceived] [varchar](30) NULL,
	[PurchaseOrderNumber] [varchar](7) NULL,
	[TrackingNumber] [varchar](30) NULL,
	[RFSBarcode] [varchar](20) NULL,
	[WSDisplayCode] [varchar](15) NULL,
	[UnitType] [varchar](10) NULL,
	[CreateTimestamp] [datetime] NULL,
	[SourceDate] [date] NULL,
 CONSTRAINT [PK_TBL_AIMSSTUniBin] PRIMARY KEY CLUSTERED 
(
	[ServiceOrder] ASC,
	[PartSequenceNumber] ASC,
	[SKU] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_AIMTechInformation]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_AIMTechInformation](
	[District] [varchar](7) NULL,
	[Tech] [varchar](7) NULL,
	[TechEnterpriseID] [varchar](10) NOT NULL,
	[TechLastName] [varchar](50) NULL,
	[TechFirstName] [varchar](50) NULL,
	[TechMobileNumber] [varchar](10) NULL,
	[DeminF1] [varchar](1) NULL,
	[Status] [varchar](1) NULL,
	[PrimaryAddress1] [varchar](50) NULL,
	[PrimaryAddress2] [varchar](50) NULL,
	[PrimaryCity] [varchar](50) NULL,
	[PrimaryState] [varchar](2) NULL,
	[PrimaryZip] [varchar](9) NULL,
	[ReassortAddress1] [varchar](50) NULL,
	[ReassortAddress2] [varchar](50) NULL,
	[ReassortCity] [varchar](50) NULL,
	[ReassortState] [varchar](2) NULL,
	[ReassortZip] [varchar](9) NULL,
	[ReturnAddress1] [varchar](50) NULL,
	[ReturnAddress2] [varchar](50) NULL,
	[ReturnCity] [varchar](50) NULL,
	[ReturnState] [varchar](2) NULL,
	[ReturnZip] [varchar](9) NULL,
	[AlternateAddress1] [varchar](50) NULL,
	[AlternateAddress2] [varchar](50) NULL,
	[AlternateCity] [varchar](50) NULL,
	[AlternateState] [varchar](2) NULL,
	[AlternateZip] [varchar](9) NULL,
	[ShippingSchedduleSun] [varchar](1) NULL,
	[ShippingSchedduleMon] [varchar](1) NULL,
	[ShippingSchedduleTue] [varchar](1) NULL,
	[ShippingSchedduleWed] [varchar](1) NULL,
	[ShippingSchedduleThu] [varchar](1) NULL,
	[ShippingSchedduleFri] [varchar](1) NULL,
	[ShippingSchedduleSat] [varchar](1) NULL,
	[PDCNumber] [varchar](7) NULL,
	[ManagerEnterpriseID] [varchar](10) NULL,
	[ManagerName] [varchar](100) NULL,
	[EmailAddress] [varchar](250) NULL,
 CONSTRAINT [PK_TBL_AIMTechInformation] PRIMARY KEY CLUSTERED 
(
	[TechEnterpriseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_AIMTechPartTooLarge]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[TBL_AIMTechPartTooLarge](
	[District] [varchar](7) NULL,
	[Truck] [varchar](6) NULL,
	[Tech] [varchar](7) NULL,
	[EnterpriseID] [varchar](8) NULL,
	[ServiceOrder] [varchar](8) NULL,
	[PartLineNumber] [int] NULL,
	[Quantity] [int] NULL,
	[Barcode] [varchar](30) NULL,
	[RMANumber] [varchar](20) NOT NULL,
	[PartDivision] [char](3) NULL,
	[PartListSource] [char](3) NULL,
	[PartNumber] [char](24) NULL,
	[SKU] [varchar](30) NULL,
	[OrderType] [varchar](3) NULL,
	[OrderNumber] [varchar](12) NULL,
	[OrderDate] [date] NULL,
	[ShipFromName] [varchar](55) NULL,
	[ShipFromAddress1] [varchar](35) NULL,
	[ShipFromAddress2] [varchar](35) NULL,
	[ShipFromAddress3] [varchar](35) NULL,
	[ShipFromCity] [varchar](35) NULL,
	[ShipFromState] [varchar](2) NULL,
	[ShipFromZipCode] [varchar](10) NULL,
	[ShipFromCountry] [varchar](20) NULL,
	[RequestReturnDate] [date] NULL,
	[PartReturnType] [varchar](3) NULL,
	[ReturnReasonCode] [varchar](3) NULL,
	[ModifyDate] [datetime] NOT NULL,
 CONSTRAINT [pk_TechPartTooLarge] PRIMARY KEY CLUSTERED 
(
	[RMANumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_AIMTechToTechTransfer]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_AIMTechToTechTransfer](
	[UniqueID] [varchar](30) NOT NULL,
	[PartDivision] [char](3) NULL,
	[PartListSource] [char](3) NULL,
	[PartNumber] [varchar](24) NULL,
	[SKU] [varchar](30) NOT NULL,
	[PartDescription] [varchar](20) NULL,
	[PartQuantity] [int] NULL,
	[ServiceOrder] [varchar](8) NULL,
	[UserID] [varchar](12) NULL,
	[FromDistrict] [varchar](7) NOT NULL,
	[FromTruck] [varchar](7) NOT NULL,
	[FromTech] [varchar](7) NOT NULL,
	[FromBinNumber] [varchar](5) NULL,
	[ToDistrict] [varchar](7) NULL,
	[ToTruck] [varchar](7) NULL,
	[ToTech] [varchar](7) NULL,
	[ToBinNumber] [varchar](5) NULL,
	[TransferType] [varchar](3) NOT NULL,
	[TransferDate] [date] NOT NULL,
	[TransferTime] [time](7) NOT NULL,
	[ModifyDate] [date] NULL,
 CONSTRAINT [PK_TBL_AIMTechToTechTransfer] PRIMARY KEY CLUSTERED 
(
	[UniqueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_AIMTruck]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_AIMTruck](
	[District] [varchar](7) NOT NULL,
	[Truck] [varchar](7) NOT NULL,
	[Tech] [varchar](7) NULL,
	[TechEnterpriseID] [varchar](8) NULL,
	[OwnerEnterpriseID] [varchar](8) NULL,
	[Deleted] [char](1) NOT NULL,
 CONSTRAINT [PK_TBL_AIMTruck] PRIMARY KEY CLUSTERED 
(
	[District] ASC,
	[Truck] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_AIMTruckBinInventory]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_AIMTruckBinInventory](
	[ExtractDate] [date] NOT NULL,
	[District] [varchar](7) NOT NULL,
	[Truck] [varchar](7) NOT NULL,
	[Tech] [varchar](7) NULL,
	[TechEnterpriseID] [varchar](8) NULL,
	[PartDivision] [char](3) NOT NULL,
	[PartListSource] [char](3) NOT NULL,
	[PartNumber] [varchar](24) NOT NULL,
	[PartDescription] [varchar](15) NULL,
	[SKU] [varchar](30) NOT NULL,
	[SellPrice] [decimal](8, 2) NULL,
	[CostAmount] [decimal](8, 2) NULL,
	[TruckBin] [varchar](6) NOT NULL,
	[TruckBinQuantity] [int] NULL,
	[TruckStockAddDate] [date] NULL,
	[TruckStockChangeDate] [date] NULL,
	[SourceDate] [datetime] NULL,
 CONSTRAINT [PK_TBL_AIMTruckBinInventory] PRIMARY KEY CLUSTERED 
(
	[District] ASC,
	[Truck] ASC,
	[SKU] ASC,
	[TruckBin] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_AIMTruckInventory]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_AIMTruckInventory](
	[ExtractDate] [date] NOT NULL,
	[District] [varchar](7) NOT NULL,
	[Truck] [varchar](7) NOT NULL,
	[Tech] [varchar](7) NULL,
	[TechEnterpriseID] [varchar](8) NULL,
	[PartDivision] [char](3) NOT NULL,
	[PartListSource] [char](3) NOT NULL,
	[PartNumber] [varchar](24) NOT NULL,
	[SKU] [varchar](30) NOT NULL,
	[PartDescription] [varchar](15) NULL,
	[CostAmount] [decimal](8, 2) NULL,
	[SellPrice] [decimal](8, 2) NULL,
	[OnHandQuantity] [int] NULL,
	[TruckBin] [varchar](3) NULL,
	[AllocatedQuantity] [int] NULL,
	[BackorderedQuantity] [int] NULL,
	[OnOrderQuantity] [int] NULL,
	[TruckStockAddDate] [date] NULL,
	[TruckStockChangeDate] [date] NULL,
	[DataSource] [varchar](3) NULL,
 CONSTRAINT [PK_TBL_AIMTruckInventory] PRIMARY KEY CLUSTERED 
(
	[District] ASC,
	[Truck] ASC,
	[SKU] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_AIMUsePartApproval]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_AIMUsePartApproval](
	[UniqueID] [int] NOT NULL,
	[District] [varchar](7) NOT NULL,
	[Unit] [varchar](7) NULL,
	[Tech] [varchar](8) NOT NULL,
	[PartDivision] [char](3) NULL,
	[PartListSource] [char](3) NULL,
	[PartNumber] [varchar](24) NULL,
	[SKU] [varchar](30) NOT NULL,
	[CostAmount] [decimal](9, 2) NULL,
	[ServiceOrder] [varchar](8) NULL,
	[BinQuantity] [int] NULL,
	[BinNumber] [varchar](6) NULL,
	[ReasonCode] [varchar](2) NULL,
	[CreateDate] [datetime] NOT NULL,
	[ApprovalUser] [varchar](8) NULL,
	[ApprovalStatus] [varchar](15) NULL,
	[ApprovalComments] [varchar](500) NULL,
	[UpdateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_TBL_AIMUsePartApproval] PRIMARY KEY CLUSTERED 
(
	[UniqueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_FiscalCalendar]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_FiscalCalendar](
	[AccountingMonthName] [char](9) NULL,
	[CalendarDate] [date] NULL,
	[AccountingDate] [date] NOT NULL,
	[DayofWeek] [float] NULL,
	[DayofWeekName] [char](10) NULL,
	[DayofWeekNameShort] [char](3) NULL,
	[AccountingWeekStartDate] [date] NULL,
	[AccountingWeekEndDate] [date] NULL,
	[AccountingMonthStartDate] [date] NULL,
	[AccountingMonthEndDate] [date] NULL,
	[CalendarMonthStartDate] [date] NULL,
	[AccountingYear] [float] NULL,
	[AccountingWeek] [float] NULL,
	[AccountingYearWeek] [float] NULL,
	[AccountingMonth] [float] NULL,
	[AccountingYearMonth] [float] NULL,
	[AccountingQuarter] [float] NULL,
	[AccountingWeekInMonth] [float] NULL,
	[AccountingWeekInQuarter] [float] NULL,
	[TotalWeeksInMonth] [float] NULL,
	[CalendarYear] [float] NULL,
	[CalendarMonth] [float] NULL,
 CONSTRAINT [PK_TBL_FiscalCalendar] PRIMARY KEY CLUSTERED 
(
	[AccountingDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_HSSCPDCOTCCIInventory]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_HSSCPDCOTCCIInventory](
	[Channel] [varchar](3) NOT NULL,
	[Unit] [varchar](5) NOT NULL,
	[UnitName] [varchar](35) NOT NULL,
	[UnitType] [char](1) NOT NULL,
	[SVGPart] [varchar](32) NOT NULL,
	[SKU] [varchar](30) NOT NULL,
	[PartDivision] [varchar](3) NOT NULL,
	[PartListSource] [varchar](3) NOT NULL,
	[PartNumber] [varchar](24) NOT NULL,
	[PartDescription] [varchar](15) NOT NULL,
	[PartCost] [decimal](7, 2) NOT NULL,
	[PartSellPrice] [decimal](8, 2) NOT NULL,
	[SON] [varchar](4) NULL,
	[IsALSItem] [varchar](5) NULL,
	[DateAdded] [date] NULL,
	[OnHandNewQuantity] [int] NOT NULL,
	[OnOrderQuantity] [int] NOT NULL,
	[BackOrderQuantity] [int] NULL,
	[AllocatedQuantity] [int] NOT NULL,
	[NetAvailableQuantity] [int] NULL,
	[RolledUpNetAvail] [int] NULL,
	[MovementClass] [varchar](8) NULL,
	[MinimumStockQuantity] [int] NULL,
	[SL] [int] NOT NULL,
	[ROP] [int] NOT NULL,
	[EOQ] [int] NOT NULL,
	[StockMax] [int] NOT NULL,
	[AnnualDemandQuantity] [int] NULL,
	[StartDatePlanPart] [date] NULL,
	[DNPCode] [varchar](3) NULL,
	[LT] [int] NOT NULL,
	[InventoryPlanQuantity] [int] NULL,
	[AvailSt] [varchar](10) NULL,
	[LawsonProcure] [varchar](6) NULL,
	[SLDS] [varchar](3) NOT NULL,
	[ForcastMethod] [varchar](16) NULL,
	[PotentialOrdersThisWeek] [decimal](7, 2) NULL,
	[PotentialOrdersNextWeek] [decimal](7, 2) NULL,
 CONSTRAINT [PK_TBL_HSSCPDCOTCCIInventory] PRIMARY KEY CLUSTERED 
(
	[Unit] ASC,
	[SKU] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_Location]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Location](
	[LocationID] [varchar](30) NOT NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[TBL_Location] ADD [LocationType] [varchar](7) NOT NULL
ALTER TABLE [dbo].[TBL_Location] ADD [LocationName] [varchar](100) NOT NULL
ALTER TABLE [dbo].[TBL_Location] ADD [AddressPartOne] [varchar](50) NULL
ALTER TABLE [dbo].[TBL_Location] ADD [AddressPartTwo] [varchar](50) NULL
ALTER TABLE [dbo].[TBL_Location] ADD [City] [varchar](50) NULL
ALTER TABLE [dbo].[TBL_Location] ADD [State] [varchar](2) NULL
ALTER TABLE [dbo].[TBL_Location] ADD [Zip] [varchar](5) NULL
ALTER TABLE [dbo].[TBL_Location] ADD [ZipPlusFour] [varchar](4) NULL
ALTER TABLE [dbo].[TBL_Location] ADD [Latitude] [decimal](12, 9) NULL
ALTER TABLE [dbo].[TBL_Location] ADD [Longitude] [decimal](12, 9) NULL
ALTER TABLE [dbo].[TBL_Location] ADD [Phone] [varchar](10) NULL
ALTER TABLE [dbo].[TBL_Location] ADD [AlternatePhone] [varchar](10) NULL
ALTER TABLE [dbo].[TBL_Location] ADD [AlternatePhoneExtension] [varchar](10) NULL
ALTER TABLE [dbo].[TBL_Location] ADD [Fax] [varchar](10) NULL
ALTER TABLE [dbo].[TBL_Location] ADD [ChargeAccount] [varchar](15) NULL
ALTER TABLE [dbo].[TBL_Location] ADD [PickupCapable] [int] NOT NULL
ALTER TABLE [dbo].[TBL_Location] ADD [ShipCapable] [int] NOT NULL
 CONSTRAINT [PK_TBL_Location] PRIMARY KEY CLUSTERED 
(
	[LocationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_LocationInventory]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_LocationInventory](
	[LocationID] [varchar](30) NOT NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[TBL_LocationInventory] ADD [SKU] [varchar](30) NOT NULL
ALTER TABLE [dbo].[TBL_LocationInventory] ADD [OnHandQuantity] [int] NULL
ALTER TABLE [dbo].[TBL_LocationInventory] ADD [OnHandLocation] [varchar](3) NULL
ALTER TABLE [dbo].[TBL_LocationInventory] ADD [AllocatedQuantity] [int] NULL
ALTER TABLE [dbo].[TBL_LocationInventory] ADD [NetOnHandQuantity] [int] NULL
ALTER TABLE [dbo].[TBL_LocationInventory] ADD [BackorderedQuantity] [int] NULL
ALTER TABLE [dbo].[TBL_LocationInventory] ADD [OnOrderQuantity] [int] NULL
ALTER TABLE [dbo].[TBL_LocationInventory] ADD [PartCost] [decimal](7, 2) NOT NULL
ALTER TABLE [dbo].[TBL_LocationInventory] ADD [PartSellPrice] [decimal](7, 2) NOT NULL
 CONSTRAINT [PK_TBL_LocationInventory] PRIMARY KEY CLUSTERED 
(
	[LocationID] ASC,
	[SKU] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_Metric205Data]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Metric205Data](
	[UniqueID] [varchar](11) NOT NULL,
	[AccountingYearMonth] [decimal](8, 2) NOT NULL,
	[Period] [varchar](5) NOT NULL,
	[CallCount] [decimal](28, 9) NOT NULL,
 CONSTRAINT [PK_TBL_Metric205Data] PRIMARY KEY CLUSTERED 
(
	[UniqueID] ASC,
	[AccountingYearMonth] ASC,
	[Period] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_NPNPartOrder]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_NPNPartOrder](
	[PartOrder] [varchar](7) NOT NULL,
	[PartOrderDate] [date] NOT NULL,
	[UnitOrderingPart] [char](7) NOT NULL,
	[PartOrderType] [char](3) NOT NULL,
	[PartOrderStatus] [char](2) NOT NULL,
	[PartOrderStatusDate] [date] NOT NULL,
	[UnitToBeInvoiced] [char](7) NOT NULL,
	[UnitCreatingPartOrder] [char](7) NOT NULL,
	[NPSID] [char](7) NOT NULL,
	[TotalPartLines] [decimal](11, 2) NOT NULL,
	[OrderDiscount] [decimal](11, 2) NOT NULL,
	[OrderSpecialDiscount] [decimal](5, 2) NOT NULL,
	[OrderShipping] [decimal](5, 2) NOT NULL,
	[OrderTax] [decimal](11, 2) NOT NULL,
	[OrderDiscountPercent] [decimal](3, 2) NOT NULL,
	[TaxMethodUsed] [char](1) NOT NULL,
	[OverrideShipmentCode] [char](1) NOT NULL,
	[ServiceUnit] [char](7) NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[LastActionTimestamp] [datetime] NOT NULL,
 CONSTRAINT [PK_TBL_NPNPartOrder] PRIMARY KEY CLUSTERED 
(
	[PartOrder] ASC,
	[PartOrderDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_NPNPartOrderLine]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_NPNPartOrderLine](
	[PartOrder] [varchar](7) NOT NULL,
	[PartOrderDate] [date] NOT NULL,
	[PartOrderLine] [smallint] NOT NULL,
	[SKU] [varchar](30) NOT NULL,
	[PartQuantity] [int] NOT NULL,
	[PartModel] [varchar](24) NOT NULL,
	[PartSellPrice] [decimal](8, 2) NOT NULL,
	[PartDescription] [varchar](12) NOT NULL,
	[PartCoverage] [varchar](2) NOT NULL,
	[PartShippingMethod] [char](1) NOT NULL,
	[PartShippingDestination] [char](1) NOT NULL,
	[PartOrderStatus] [char](2) NOT NULL,
	[PartOrderStatusDate] [date] NOT NULL,
	[PartLocation] [varchar](5) NOT NULL,
	[ShipDate] [date] NOT NULL,
	[PartSequence] [smallint] NOT NULL,
	[PurchaseRequisitionOrder] [varchar](7) NOT NULL,
	[PurchaseRequisitionOrderType] [char](1) NOT NULL,
	[LastActionTimestamp] [datetime] NOT NULL,
 CONSTRAINT [PK_TBL_NPNPartOrderLine] PRIMARY KEY CLUSTERED 
(
	[PartOrder] ASC,
	[PartOrderDate] ASC,
	[PartOrderLine] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_Part]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Part](
	[PartSKU] [varchar](30) NOT NULL,
	[PartDivision] [char](3) NOT NULL,
	[PartListSource] [char](3) NOT NULL,
	[PartNumber] [varchar](24) NOT NULL,
	[PartDescription] [varchar](20) NOT NULL,
	[PartSourceOrderNumber] [int] NOT NULL,
	[PartAvailability] [char](1) NOT NULL,
	[PartCost] [decimal](7, 2) NOT NULL,
	[PartSellPrice] [decimal](7, 2) NOT NULL,
	[PartPriceModifyDate] [date] NOT NULL,
	[HazardousMaterialCatalogCode] [char](1) NOT NULL,
	[PartImageURL] [varchar](200) NULL,
	[PartAddDate] [date] NOT NULL,
 CONSTRAINT [PK_TBL_Part] PRIMARY KEY CLUSTERED 
(
	[PartSKU] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_Part_QA]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Part_QA](
	[PartSKU] [varchar](30) NOT NULL,
	[PartDivision] [char](3) NOT NULL,
	[PartListSource] [char](3) NOT NULL,
	[PartNumber] [varchar](24) NOT NULL,
	[PartDescription] [varchar](20) NOT NULL,
	[PartSourceOrderNumber] [int] NOT NULL,
	[PartAvailability] [char](1) NOT NULL,
	[PartCost] [decimal](7, 2) NOT NULL,
	[PartSellPrice] [decimal](7, 2) NOT NULL,
	[PartPriceModifyDate] [date] NOT NULL,
	[HazardousMaterialCatalogCode] [char](1) NOT NULL,
	[PartImageURL] [varchar](200) NULL,
	[PartAddDate] [date] NOT NULL,
 CONSTRAINT [PK_TBL_Part_QA] PRIMARY KEY CLUSTERED 
(
	[PartSKU] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_PartAvailabilityCodes]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_PartAvailabilityCodes](
	[avail_cd] [char](1) NOT NULL,
	[alpha_cd] [char](3) NULL,
	[avail_ds] [varchar](25) NULL,
 CONSTRAINT [PK_TBL_PartAvailabilityCodes] PRIMARY KEY CLUSTERED 
(
	[avail_cd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_PartShippingSpecifications]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_PartShippingSpecifications](
	[SKU] [varchar](30) NOT NULL,
	[ShippingWeight] [float] NOT NULL,
	[ShippingLength] [float] NOT NULL,
	[ShippingWidth] [float] NOT NULL,
	[ShippingHeight] [float] NOT NULL,
 CONSTRAINT [PK_TBL_PartShippingSpecifications] PRIMARY KEY CLUSTERED 
(
	[SKU] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_PartSub]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_PartSub](
	[PartSKU] [varchar](30) NOT NULL,
	[SubPartSKU] [varchar](30) NOT NULL,
	[SubPartWayCode] [varchar](1) NOT NULL,
	[SubPartLevel] [int] NOT NULL,
 CONSTRAINT [PK_TBL_PartSub] PRIMARY KEY CLUSTERED 
(
	[PartSKU] ASC,
	[SubPartSKU] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_PISR]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_PISR](
	[District] [varchar](7) NOT NULL,
	[Truck] [varchar](7) NOT NULL,
	[Tech] [varchar](7) NULL,
	[TotalTruckBinQuantity] [decimal](14, 0) NULL,
	[TotalCostAmount] [decimal](14, 2) NULL,
	[TotalSellPrice] [decimal](14, 2) NULL,
 CONSTRAINT [PK_TBL_PISR] PRIMARY KEY CLUSTERED 
(
	[District] ASC,
	[Truck] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ShipmentTrackingUPS]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ShipmentTrackingUPS](
	[TrackingNumber] [varchar](30) NOT NULL,
	[PackageReferenceNumber] [varchar](50) NULL,
	[ShipmentReferenceNumber] [varchar](50) NULL,
	[UnitNumber] [varchar](10) NULL,
	[AccountNumber] [varchar](10) NULL,
	[PartReferenceNumber] [varchar](10) NULL,
	[ManifestDate] [varchar](10) NULL,
	[OriginDate] [varchar](10) NULL,
	[GenericDate] [varchar](10) NULL,
	[ExceptionDate] [varchar](10) NULL,
	[ExceptionTime] [varchar](10) NULL,
	[ExceptionReasonCode] [varchar](5) NULL,
	[ExceptionReasonDescription] [varchar](200) NULL,
	[DeliveryDate] [varchar](10) NULL,
	[DeliveryTime] [varchar](10) NULL,
	[ScheduledDeliveryDate] [varchar](10) NULL,
	[RescheduledDeliveryDate] [varchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[TrackingNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_SSTReceiveTasks]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_SSTReceiveTasks](
	[TaskReferenceNumber] [varchar](30) NOT NULL,
	[TaskReferenceLineNumber] [int] NOT NULL,
	[TaskType] [varchar](1) NULL,
	[TaskCreateDate] [datetime] NULL,
	[TaskReferenceLineNumber2] [int] NULL,
	[PartDivision] [char](3) NULL,
	[PartListSource] [char](3) NULL,
	[PartNumber] [varchar](24) NULL,
	[SKU] [varchar](30) NOT NULL,
	[UPCCode] [varchar](13) NULL,
	[ItemDescription] [varchar](24) NULL,
	[BinNumber] [varchar](3) NULL,
	[PartQuantity] [int] NULL,
	[ServiceOrder] [varchar](8) NULL,
	[OrderLineNumber] [int] NULL,
	[ReturnUnitNumber] [varchar](7) NULL,
	[ReturnAddress1] [varchar](30) NULL,
	[ReturnAddress2] [varchar](30) NULL,
	[ReturnCity1] [varchar](30) NULL,
	[ReturnCity2] [varchar](30) NULL,
	[ReturnState] [varchar](2) NULL,
	[ReturnZipCode] [varchar](10) NULL,
	[RecountFlag] [varchar](1) NULL,
	[RFSBarCode] [varchar](20) NULL,
	[TaskStatus] [varchar](1) NULL,
	[CreationDate2] [date] NULL,
	[EXPDQuantity] [int] NULL,
	[Truck] [varchar](6) NULL,
	[TrackingNumber] [varchar](30) NULL,
	[EnterpriseID] [varchar](8) NULL,
	[Tech] [varchar](7) NULL,
	[Unit] [varchar](7) NULL,
	[TaskClosedDate] [datetime] NULL,
	[SourceDate] [date] NULL,
 CONSTRAINT [PK_TBL_SSTReceiveTasks] PRIMARY KEY CLUSTERED 
(
	[TaskReferenceNumber] ASC,
	[TaskReferenceLineNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_SSTReturnTasks]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_SSTReturnTasks](
	[TaskReferenceNumber] [varchar](30) NOT NULL,
	[TaskReferenceLineNumber] [int] NOT NULL,
	[TaskType] [varchar](1) NULL,
	[TaskCreateDate] [datetime] NULL,
	[TaskReferenceLineNumber2] [int] NULL,
	[PartDivision] [char](3) NULL,
	[PartListSource] [char](3) NULL,
	[PartNumber] [varchar](24) NULL,
	[SKU] [varchar](30) NULL,
	[UPCCode] [varchar](13) NULL,
	[ItemDescription] [varchar](24) NULL,
	[BinNumber] [varchar](3) NULL,
	[PartQuantity] [int] NULL,
	[ReturnOrderNumber] [varchar](15) NULL,
	[OrderLineNumber] [int] NULL,
	[ReturnUnitNumber] [varchar](7) NULL,
	[ReturnAddress1] [varchar](30) NULL,
	[ReturnAddress2] [varchar](30) NULL,
	[ReturnCity1] [varchar](30) NULL,
	[ReturnCity2] [varchar](30) NULL,
	[ReturnState] [varchar](2) NULL,
	[ReturnZipCode] [varchar](10) NULL,
	[ReturnReasonCode] [varchar](3) NULL,
	[RecountFlag] [varchar](1) NULL,
	[RFSBarCode] [varchar](20) NULL,
	[Status] [varchar](1) NULL,
	[CreationDate2] [date] NULL,
	[EXPDQuantity] [int] NULL,
	[Truck] [varchar](6) NULL,
	[TrackingNumber] [varchar](30) NULL,
	[EnterpriseID] [varchar](8) NULL,
	[Tech] [varchar](7) NULL,
	[Unit] [varchar](7) NULL,
	[SourceDate] [date] NULL,
	[TaskClosedDate] [datetime] NULL,
 CONSTRAINT [PK_TBL_SSTReturnTasks] PRIMARY KEY CLUSTERED 
(
	[TaskReferenceNumber] ASC,
	[TaskReferenceLineNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_SupplyChainPartMaster]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_SupplyChainPartMaster](
	[PartSKU] [varchar](30) NOT NULL,
	[PartDivision] [char](3) NOT NULL,
	[PartListSource] [char](3) NOT NULL,
	[PartNumber] [varchar](24) NOT NULL,
	[PartDescription] [varchar](20) NOT NULL,
	[PartSourceOrderNumber] [int] NOT NULL,
	[PartAvailability] [char](1) NOT NULL,
	[PartSellPrice] [decimal](7, 2) NOT NULL,
	[PartCost] [decimal](7, 2) NOT NULL,
	[PartPriceModifyDate] [date] NOT NULL,
	[HazardousMaterialCatalogCode] [char](1) NOT NULL,
	[PartAddDate] [date] NOT NULL,
	[SubPartSKU] [varchar](30) NOT NULL,
	[SubPartDivision] [char](3) NOT NULL,
	[SubPartListSource] [char](3) NOT NULL,
	[SubPart] [varchar](24) NOT NULL,
	[SubPartWayCode] [varchar](1) NOT NULL,
 CONSTRAINT [PK_TBL_SupplyChainPartMaster] PRIMARY KEY CLUSTERED 
(
	[PartSKU] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[testend$]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[testend$](
	[BarcodeSKU] [nvarchar](255) NULL,
	[UpdateTo] [nvarchar](255) NOT NULL,
	[F3] [nvarchar](255) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[updatespace$]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[updatespace$](
	[BarcodePart] [nvarchar](255) NULL,
	[UpdateTo] [nvarchar](255) NOT NULL,
	[F3] [nvarchar](255) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Virtual_Close_2016$]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Virtual_Close_2016$](
	[Barcode #] [nvarchar](255) NULL,
	[RMA #] [nvarchar](255) NULL,
	[ITEM] [nvarchar](255) NULL,
	[Tracking #] [nvarchar](255) NULL,
	[Truck #] [nvarchar](255) NULL,
	[District #] [nvarchar](255) NULL,
	[DOMS Order #] [nvarchar](255) NULL,
	[Destination] [nvarchar](255) NULL,
	[Close Date] [nvarchar](255) NULL
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[VIEW_AIMApprovals]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO











CREATE VIEW [dbo].[VIEW_AIMApprovals]
AS
Select
			ReportingRegion as Region
			,District
			,ManagerFourEnterpriseID
			,ManagerFourFullName
			,ManagerFourJobTitle
			,ManagerThreeEnterpriseID
			,ManagerThreeFullName
			,ManagerThreeJobTitle
			,ManagertwoEnterpriseID
			,ManagertwoFullName
			,ManagertwoJobTitle
			,ManagerJobTitle
			,ManagerEnterpriseID
			,ManagerFullName                         
			,EMP.EnterpriseID
			,FullName
			,ApprovalUser
			,Tech
			,ServiceOrder
			,OldSKU                          
			,ApprovalStatus
			,PartReturnQuantity
			,BinNumber
			,ApprovalComments
			,TransactionDate
			,ReturnApproval
			,UsePartApproval
			,truck
			,RMA
			,RMALine
			,OldPartDescription
			,NewSku
			,NewPartDescription
			,UpdatedCostAmount
			,UpdatedSellAmount
			,RFSBarcode
			,PartReturnReasonCode
			,PartReturnStatus
			,NewPartReasonCode
			,PartDivision
			,PartListSource
			,PartNumber
			,ReasonCode
			,[UpdateDate] 
 
from
(Select 
       a.[District], 
       a.[Tech],
       a.[OldSKU], 
	   a.CostAmount,
       Left([ServiceOrder], 8) as ServiceOrder,
       a.[PartReturnQuantity], 
       a.[BinNumber] ,
       a.[TransactionDate] ,
       Left([ApprovalUser], 8) as ApprovalUser, 
       Left([ApprovalStatus], 15) as ApprovalStatus,  
       Left([ApprovalComments], 500) as ApprovalComments,

       1 AS ReturnApproval,
       NULL AS UsePartApproval,
	   Truck,
       RMA,
       RMALine,
	   OldPartDescription,
	   NewSku,
	   NewPartDescription,
	   UpdatedCostAmount,
	   UpdatedSellAmount,
	   RFSBarcode,
	   PartReturnReasonCode,
	   PartReturnStatus,
	   NewPartReasonCode,
	   NULL AS unit,
	   NULL AS PartDivision,
	   NULL AS PartListSource,
	   NULL AS PartNumber,
	   NULL AS ReasonCode,
	   [UpdateDate] 
       From Parts.dbo.TBL_AIMReturnApproval A with (nolock) where ServiceOrder <> '' and ApprovalStatus <> 'Pending'
 
       Union 
 
Select 
       [District], 
       [Tech], 
       [SKU],
	   CostAmount, 
       [ServiceOrder], 
       [BinQuantity], 
       Left ([BinNumber], 3) as BinNumber,  
       Cast([CreateDate]as date) as CreateDate , 
       [ApprovalUser],  
       [ApprovalStatus],
       [ApprovalComments],      
	   NULL AS ReturnApproval,
       1 AS UsePartApproval,
	   NULL AS Truck,
       NULL AS RMA,
       NULL AS RMALine,
	   NULL AS OldPartDescription,
	   NULL AS NewSku,
	   NULL AS NewPartDescription,
	   NULL AS UpdatedCostAmount,
	   NULL AS UpdatedSellAmount,
	   NULL AS RFSBarcode,
	   NULL AS PartReturnReasonCode,
	   NULL AS PartReturnStatus,
	   NULL AS NewPartReasonCode,
	   unit,
	   PartDivision,
	   PartListSource,
	   PartNumber,
	   ReasonCode,
	   [UpdateDate]
       From Parts.dbo.TBL_AIMUsePartApproval B with (NOLOCK) where ServiceOrder <> '' and ApprovalStatus <> 'Pending' ) as id
       LEFT JOIN
                (SELECT
                           EnterpriseID
                           ,FullName
                           ,ManagerEnterpriseID
                           ,ManagerFullName
                           ,ManagerJobTitle
                           ,ManagertwoEnterpriseID
                           ,ManagertwoFullName
                           ,ManagertwoJobTitle
                           ,ManagerThreeEnterpriseID
                           ,ManagerThreeFullName
                           ,ManagerThreeJobTitle
                           ,ManagerFourEnterpriseID
                           ,ManagerFourFullName
                           ,ManagerFourJobTitle
                           ,ManagerFiveEnterpriseID
                           ,ManagerFiveFullName
                           ,ManagerFiveJobTitle
                     FROM
                           Employee.dbo.TBL_EmployeeHierarchy with (nolock))  EMP
       ON
              ID.Approvaluser = EMP.EnterpriseID
 
       LEFT JOIN
                (SELECT
                           unit
                           ,ReportingDistrict
                           ,ReportingTerritory
                           ,ReportingRegion
                     FROM
                           Employee.dbo.TBL_UnitCrossReference with (nolock))  Unit
       ON
              ID.District = UNIT.Unit

GO
/****** Object:  View [dbo].[VIEW_AIMDistrictInventory]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO













CREATE VIEW [dbo].[VIEW_AIMDistrictInventory]
AS

SELECT
	Region
	,RegionName
	,RVPEnterpriseID
	,RVPFullName
	,TFDEnterpriseID
	,TFDFullName
	,Territory
	,TerritoryName
	,Unit
	,UnitName
	,District
	,DSMEnterpriseID
	,DSMFullName
	,TMEnterpriseID
	,TMFullName
	,EnterpriseID
	,NPSID
	,FullName
	,Tech
	,Truck
	,RFSBarcode
	,RGI
	,PartDivision
	,PartListSource
	,PartNumber
	,SKU
	,PartDescription
	,Quantity
	,CostAmount
	,BinNumber 
	,ASNNumber 
	,CoreFlag 
	,UPSTrackingNumber
	,ReturnStatusDate 
	,DroppedDistrictDate 
	,ReceivedDistrictDate 
	,StatusPlacedDate 
	,SG01BinAddDate 
	,SG01ShippedOutDate 
	,ReceiveBinPlacedDate 
	,PDCShippedDate 
	,WISTransDate 
	,WISApprovedUser 
	,WISApprovedDate 
	,RejectedUser 
	,RejectedDate 
	,ModifyDate 
FROM
	Parts.dbo.TBL_AIMDistrictInventory DI WITH (NOLOCK) 
LEFT JOIN
      (SELECT
            EnterpriseID
            ,NPSID
            ,FullName
            ,TMEnterpriseID
            ,TMFullName
            ,DSMEnterpriseID
            ,DSMFullName
            ,Unit
            ,UnitName
            ,TFDEnterpriseID
            ,TFDFullName
            ,Territory
			,TerritoryName
            ,Region
            ,RegionName
            ,RVPEnterpriseID
            ,RVPFullName
      FROM
            Employee.dbo.VIEW_EmployeeHierarchyIHTechRegion) EMP
ON
      DI.District = EMP.Unit
      AND
      DI.Tech = EMP.NPSID










GO
/****** Object:  View [dbo].[VIEW_AIMEndToEndUPS]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[VIEW_AIMEndToEndUPS]
AS
SELECT 
	COALESCE(EMP.Region, UNIT.Region, 'Unknown') AS Region	
	,COALESCE(EMP.RegionName, UNIT.RegionName, 'Unknown') AS RegionName	
	,COALESCE(EMP.RVPEnterpriseID, UNIT.RVPEnterpriseID,  'Unknown') AS RVPEnterpriseID	
	,COALESCE(EMP.RVPFullName, UNIT.RVPFullName, 'Unknown') AS RVPFullName	
	,COALESCE(EMP.TFDEnterpriseID, UNIT.TFDEnterpriseID,  'Unknown') AS TFDEnterpriseID	
	,COALESCE(EMP.TFDFullName, UNIT.TFDFullName, 'Unknown') AS TFDFullName	
	,COALESCE(EMP.Territory, UNIT.Territory, 'Unknown') AS Territory	
	,COALESCE(EMP.TerritoryName, UNIT.TerritoryName, 'Unknown') AS TerritoryName	
	,COALESCE(EMP.Unit, UNIT.Unit, 'Unknown') AS Unit	
	,COALESCE(EMP.UnitName, UNIT.UnitName, 'Unknown') AS UnitName	
	,District
	,COALESCE(DSMEnterpriseID, 'Unknown') AS DSMEnterpriseID	
	,COALESCE(DSMFullName, 'Unknown') AS DSMFullName	
	,COALESCE(TMEnterpriseID, 'Unknown') AS TMEnterpriseID	
	,COALESCE(TMFullName,  'Unknown') AS TMFullName	
	,COALESCE(EMP.EnterpriseID,  'Unknown') AS EnterpriseID	
	,COALESCE(EMP.NPSID, 'Unknown') AS NPSID	
	,COALESCE(EMP.FullName, 'Unknown') AS FullName	
	,Tech
	,Truck
	,Barcode
	,RMANumber
	,RGINumber
	,UPSTrackingNumber
	,UPSFlag
	,PartDivision
	,PartListSource
	,PartNumber
	,ENDTOEND.SKU
	,PartDescription
	,OrderQuantity
	,CostAmount
	,ShippingWeight
	,ShippingLength * ShippingWidth * ShippingHeight as ShippingVolume
	,CASE WHEN ShippingLength * ShippingWidth * ShippingHeight > 1296 then 'Large' else '' end as PartSize
	,ShipToPDC
	,ReturnScanDate
	,ReturnShipDate
	,ReturnPickupDate
	,ReturnDeliveryDate
	,PDCPullFlag
	,PDCRecordCloseDate
	,CASE -- Requested by Bryan Evers on 1/9/2017
		WHEN [Status] = 'Missing' THEN 'Closed RMA'
		WHEN [Status] = 'Wrong Part' THEN 'WP Upchain'
	ELSE
		[Status]
	END AS [Status]
	,CASE -- Requested by Bryan Evers on 7/20/2017
		WHEN Reason = 'RSE' THEN 'Dispositioned as E&O'
	ELSE
		Reason
	END AS Reason
	,ExpectedSKU
	,Core
	,UserID
	,CASE -- Requested by Bryan Evers on 1/10/2017
		WHEN UPSFlag = 'NON UPS' AND ShippingStatus = 'Tech Shipped' THEN 'District Shipped'
		WHEN UPSFlag = 'NON UPS' AND [Status] = 'In PDC' AND ShippingStatus = 'Invalid shipment No' THEN 'District Shipped'
	ELSE
		ShippingStatus
	END AS ShippingStatus
	,LastModifyDate
FROM
	Parts.dbo.TBL_AIMEndToEndUPS ENDTOEND WITH (NOLOCK)
	LEFT JOIN Parts.dbo.TBL_PartShippingSpecifications as PSHIP WITH (NOLOCK) ON  ENDTOEND.SKU = PSHIP.SKU	 
	LEFT JOIN Employee.dbo.TBL_EmployeeHierarchyIHTechRegion as EMP WITH (NOLOCK)on ENDTOEND.District = EMP.Unit AND ENDTOEND.Tech = EMP.NPSID
	LEFT JOIN Employee.dbo.TBL_EmployeeHierarchyIHUnitRegion as UNIT WITH (NOLOCK) ON ENDTOEND.District = UNIT.Unit 


GO
/****** Object:  View [dbo].[VIEW_AIMPartTooLarge]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO















CREATE VIEW [dbo].[VIEW_AIMPartTooLarge]
AS

	SELECT
		Region
		,RegionName
		,RVPEnterpriseID
		,RVPFullName
		,TFDEnterpriseID
		,TFDFullName
		,Territory
		,TerritoryName
		,Unit
		,UnitName
		,DSMEnterpriseID
		,DSMFullName
		,TMEnterpriseID
		,TMFullName
		,EMP.EnterpriseID 
		,NPSID
		,FullName
		,[District]
		,[Truck]
		,[Tech]
		,[ServiceOrder]
		,[PartLineNumber]
		,[Quantity]
		,[Barcode]
		,[RMANumber]
		,[PartDivision]
		,[PartListSource]
		,[PartNumber]
		,AIMLARGE.[SKU]
		,PartDescription
		,[OrderType]
		,[OrderNumber]
		,[OrderDate]
		,[ShipFromName]
		,[ShipFromAddress1]
		,[ShipFromAddress2]
		,[ShipFromAddress3]
		,[ShipFromCity]
		,[ShipFromState]
		,[ShipFromZipCode]
		,[ShipFromCountry]
		,[RequestReturnDate]
		,[PartReturnType]
		,[ReturnReasonCode]
		,ShippingWeight
		,ShippingLength
		,ShippingWidth
		,ShippingHeight
		,round(ShippingVolume,2) as ShippingVolume
		,Case when Compliant is null then 'Y' else Compliant end as Compliant
		,[ModifyDate]
	FROM
		Parts.dbo.TBL_AIMTechPartTooLarge AIMLARGE
	LEFT JOIN
		(SELECT
			SKU
			,ShippingWeight
			,ShippingLength
			,ShippingWidth
			,ShippingHeight
			,(ShippingLength * .08333) * (ShippingWidth * .08333) * (ShippingHeight* .08333) As ShippingVolume
            ,Case when ((ShippingLength * .08333) * (ShippingWidth * .08333) * (ShippingHeight* .08333)) >= 2 then 'Y' else 'N' end as Compliant
		FROM
			Parts.dbo.TBL_PartShippingSpecifications) SIZE
	ON
		AIMLARGE.SKU = SIZE.SKU
	LEFT JOIN
		  (SELECT
		  PartSKU
		  ,PartDescription
		FROM
			Parts.dbo.TBL_Part) PART
	ON
		AIMLARGE.SKU = PART.PartSKU  
	LEFT JOIN
		  (SELECT
				EnterpriseID
				,NPSID
				,FullName
				,TMEnterpriseID
				,TMFullName
				,DSMEnterpriseID
				,DSMFullName
				,Unit
				,UnitName
				,TFDEnterpriseID
				,TFDFullName
				,Territory
				,TerritoryName
				,Region
				,RegionName
				,RVPEnterpriseID
				,RVPFullName
			FROM
				Employee.dbo.VIEW_EmployeeHierarchyIHTechRegion) EMP
	ON
		AIMLARGE.District = EMP.Unit
		AND
		AIMLARGE.EnterpriseID = EMP.EnterpriseID





GO
/****** Object:  View [dbo].[VIEW_AIMReceiveTasks]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO













CREATE VIEW [dbo].[VIEW_AIMReceiveTasks]
AS

SELECT 		
	COALESCE(EMP.Region, UNIT.Region, HVEMP.Region, HVUNIT.Region, 'Unknown') AS Region	
	,COALESCE(EMP.RegionName, UNIT.RegionName,HVEMP.RegionName, HVUNIT.RegionName, 'Unknown') AS RegionName	
	,COALESCE(EMP.RVPEnterpriseID, UNIT.RVPEnterpriseID, HVEMP.RVPDirEnterpriseID, HVUNIT.RVPDirEnterpriseID, 'Unknown') AS RVPEnterpriseID	
	,COALESCE(EMP.RVPFullName, UNIT.RVPFullName, HVEMP.RVPDirFullName, HVUNIT.RVPDirFullName, 'Unknown') AS RVPFullName	
	,COALESCE(EMP.TFDEnterpriseID, UNIT.TFDEnterpriseID, HVEMP.TFDEnterpriseID, HVUNIT.TFDEnterpriseID, 'Unknown') AS TFDEnterpriseID	
	,COALESCE(EMP.TFDFullName, UNIT.TFDFullName, HVEMP.TFDFullName, HVUNIT.TFDFullName, 'Unknown') AS TFDFullName	
	,COALESCE(EMP.Territory, UNIT.Territory, HVEMP.Territory, HVUNIT.Territory, 'Unknown') AS Territory	
	,COALESCE(EMP.TerritoryName, UNIT.TerritoryName, HVEMP.TerritoryName, HVUNIT.TerritoryName,'Unknown') AS TerritoryName	
	,COALESCE(EMP.Unit, UNIT.Unit, HVEMP.Unit, HVUNIT.Unit, 'Unknown') AS Unit	
	,COALESCE(EMP.UnitName, UNIT.UnitName, HVEMP.UnitName, HVUNIT.UnitName,'Unknown') AS UnitName	
	,District	
	,COALESCE(DSMEnterpriseID, DSMRTMEnterpriseID, 'Unknown') AS DSMEnterpriseID	
	,COALESCE(DSMFullName, DSMRTMFullName, 'Unknown') AS DSMFullName	
	,COALESCE(TMEnterpriseID, TMDTMEnterpriseID, 'Unknown') AS TMEnterpriseID	
	,COALESCE(TMFullName, TMDTMFullName, 'Unknown') AS TMFullName	
	,COALESCE(EMP.EnterpriseID, HVEMP.EnterpriseID, 'Unknown') AS EnterpriseID	
	,COALESCE(EMP.NPSID, HVEMP.NPSID, 'Unknown') AS NPSID	
	,COALESCE(EMP.FullName, HVEMP.FullName, 'Unknown') AS FullName	
	,Tech	
	,Truck	
	,TaskReferenceNumber	
	,TaskReferenceLineNumber	
	,TaskDate	
	,DeliveryDate	
	,PartDivision	
	,PartListSource	
	,PartNumber	
	,SKU	
	,PartDescription	
	,TaskQuantity	
	,CompletedQuantity	
	,TaskStatus	
	,TrackingNumber	
	,ServiceOrder	
	,BinNumber	
	,[FileName]	
	,TaskClosedDate	
	,CASE WHEN TaskClosedDate IS NULL THEN Parts.dbo.getworkingdays(DeliveryDate, GETDATE())-1 ELSE Parts.dbo.getworkingdays(DeliveryDate, TaskClosedDate) -1  END AS DaysSinceDelivered	
FROM		
	Parts.dbo.TBL_AIMReceiveTasks RECEIVETASKS WITH (NOLOCK)	
LEFT JOIN		
      (SELECT		
            EnterpriseID		
            ,NPSID		
            ,FullName		
            ,TMEnterpriseID		
            ,TMFullName		
            ,DSMEnterpriseID		
            ,DSMFullName		
            ,Unit		
            ,UnitName		
            ,TFDEnterpriseID		
            ,TFDFullName		
            ,Territory		
            ,TerritoryName		
            ,Region		
            ,RegionName		
            ,RVPEnterpriseID		
            ,RVPFullName		
      FROM		
            Employee.dbo.VIEW_EmployeeHierarchyIHTechRegion) EMP		
ON		
      RECEIVETASKS.District = EMP.Unit		
      AND		
      RECEIVETASKS.Tech = EMP.NPSID		
LEFT JOIN		
	(SELECT	
		Unit
		,UnitName
		,TFDEnterpriseID
		,TFDFullName
		,Territory
		,TerritoryName
		,Region
		,RegionName
		,RVPEnterpriseID
		,RVPFullName
	FROM	
		Employee.dbo.VIEW_EmployeeHierarchyIHUnitRegion) UNIT
ON		
	RECEIVETASKS.District = UNIT.Unit	
LEFT JOIN		
      (SELECT		
            EnterpriseID		
            ,NPSID		
            ,FullName		
            ,TMDTMEnterpriseID		
            ,TMDTMFullName		
            ,DSMRTMEnterpriseID		
            ,DSMRTMFullName		
            ,Unit		
            ,UnitName		
            ,TFDEnterpriseID		
            ,TFDFullName		
            ,Territory		
            ,TerritoryName		
            ,Region		
            ,RegionName		
            ,RVPDirEnterpriseID		
            ,RVPDirFullName		
      FROM		
            Employee.dbo.VIEW_EmployeeHierarchyIHHVACTechRegion) HVEMP		
ON		
      RECEIVETASKS.District = HVEMP.Unit		
      AND		
      RECEIVETASKS.Tech = HVEMP.NPSID		
LEFT JOIN		
	(SELECT	
		Unit
		,UnitName
		,TFDEnterpriseID
		,TFDFullName
		,Territory
		,TerritoryName
		,Region
		,RegionName
		,RVPDirEnterpriseID
		,RVPDirFullName
	FROM	
		Employee.dbo.VIEW_EmployeeHierarchyIHHVACUnitRegion) HVUNIT
ON		
	RECEIVETASKS.District = HVUNIT.Unit










GO
/****** Object:  View [dbo].[VIEW_AIMReturnApproval]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO












CREATE VIEW [dbo].[VIEW_AIMReturnApproval]
AS

SELECT 		
	COALESCE(EMP.Region, UNIT.Region, HVEMP.Region, HVUNIT.Region, 'Unknown') AS Region	
	,COALESCE(EMP.RegionName, UNIT.RegionName,HVEMP.RegionName, HVUNIT.RegionName, 'Unknown') AS RegionName	
	,COALESCE(EMP.RVPEnterpriseID, UNIT.RVPEnterpriseID, HVEMP.RVPDirEnterpriseID, HVUNIT.RVPDirEnterpriseID, 'Unknown') AS RVPEnterpriseID	
	,COALESCE(EMP.RVPFullName, UNIT.RVPFullName, HVEMP.RVPDirFullName, HVUNIT.RVPDirFullName, 'Unknown') AS RVPFullName	
	,COALESCE(EMP.TFDEnterpriseID, UNIT.TFDEnterpriseID, HVEMP.TFDEnterpriseID, HVUNIT.TFDEnterpriseID, 'Unknown') AS TFDEnterpriseID	
	,COALESCE(EMP.TFDFullName, UNIT.TFDFullName, HVEMP.TFDFullName, HVUNIT.TFDFullName, 'Unknown') AS TFDFullName	
	,COALESCE(EMP.Territory, UNIT.Territory, HVEMP.Territory, HVUNIT.Territory, 'Unknown') AS Territory	
	,COALESCE(EMP.TerritoryName, UNIT.TerritoryName, HVEMP.TerritoryName, HVUNIT.TerritoryName,'Unknown') AS TerritoryName	
	,COALESCE(EMP.Unit, UNIT.Unit, HVEMP.Unit, HVUNIT.Unit, 'Unknown') AS Unit	
	,COALESCE(EMP.UnitName, UNIT.UnitName, HVEMP.UnitName, HVUNIT.UnitName,'Unknown') AS UnitName	
	,District	
	,COALESCE(DSMEnterpriseID, DSMRTMEnterpriseID, 'Unknown') AS DSMEnterpriseID	
	,COALESCE(DSMFullName, DSMRTMFullName, 'Unknown') AS DSMFullName	
	,COALESCE(TMEnterpriseID, TMDTMEnterpriseID, 'Unknown') AS TMEnterpriseID	
	,COALESCE(TMFullName, TMDTMFullName, 'Unknown') AS TMFullName	
	,COALESCE(EMP.EnterpriseID, HVEMP.EnterpriseID, 'Unknown') AS EnterpriseID	
	,COALESCE(EMP.NPSID, HVEMP.NPSID, 'Unknown') AS NPSID	
	,COALESCE(EMP.FullName, HVEMP.FullName, 'Unknown') AS FullName	
	,Tech
	,Truck
	,RMA
	,RMALine
	,OldSKU
	,OldPartDescription
	,NewSKU
	,NewPartDescription
	,CostAmount
	,UpdatedCostAmount
	,UpdatedSellAmount
	,RFSBarcode
	,ServiceOrder
	,PartReturnQuantity
	,PartReturnReasonCode
	,PartReturnStatus
	,NewPartReasonCode
	,BinNumber
	,TransactionDate
	,ApprovalUser
	,ApprovalStatus
	,ApprovalComments
	,UpdateDate
FROM		
	Parts.dbo.TBL_AIMReturnApproval RETURNAPPROVAL WITH (NOLOCK)
	
LEFT JOIN		
      (SELECT		
            EnterpriseID		
            ,NPSID		
            ,FullName		
            ,TMEnterpriseID		
            ,TMFullName		
            ,DSMEnterpriseID		
            ,DSMFullName		
            ,Unit		
            ,UnitName		
            ,TFDEnterpriseID		
            ,TFDFullName		
            ,Territory		
            ,TerritoryName		
            ,Region		
            ,RegionName		
            ,RVPEnterpriseID		
            ,RVPFullName		
      FROM		
            Employee.dbo.VIEW_EmployeeHierarchyIHTechRegion) EMP		
ON		
      RETURNAPPROVAL.District = EMP.Unit		
      AND		
      RETURNAPPROVAL.Tech = EMP.NPSID		
LEFT JOIN		
	(SELECT	
		Unit
		,UnitName
		,TFDEnterpriseID
		,TFDFullName
		,Territory
		,TerritoryName
		,Region
		,RegionName
		,RVPEnterpriseID
		,RVPFullName
	FROM	
		Employee.dbo.VIEW_EmployeeHierarchyIHUnitRegion) UNIT
ON		
	RETURNAPPROVAL.District = UNIT.Unit	
LEFT JOIN		
      (SELECT		
            EnterpriseID		
            ,NPSID		
            ,FullName		
            ,TMDTMEnterpriseID		
            ,TMDTMFullName		
            ,DSMRTMEnterpriseID		
            ,DSMRTMFullName		
            ,Unit		
            ,UnitName		
            ,TFDEnterpriseID		
            ,TFDFullName		
            ,Territory		
            ,TerritoryName		
            ,Region		
            ,RegionName		
            ,RVPDirEnterpriseID		
            ,RVPDirFullName		
      FROM		
            Employee.dbo.VIEW_EmployeeHierarchyIHHVACTechRegion) HVEMP		
ON		
      RETURNAPPROVAL.District = HVEMP.Unit		
      AND		
      RETURNAPPROVAL.Tech = HVEMP.NPSID		
LEFT JOIN		
	(SELECT	
		Unit
		,UnitName
		,TFDEnterpriseID
		,TFDFullName
		,Territory
		,TerritoryName
		,Region
		,RegionName
		,RVPDirEnterpriseID
		,RVPDirFullName
	FROM	
		Employee.dbo.VIEW_EmployeeHierarchyIHHVACUnitRegion) HVUNIT
ON		
	RETURNAPPROVAL.District = HVUNIT.Unit



GO
/****** Object:  View [dbo].[VIEW_AIMReturnTasks]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO












CREATE VIEW [dbo].[VIEW_AIMReturnTasks]
AS

SELECT 		
	COALESCE(EMP.Region, UNIT.Region, HVEMP.Region, HVUNIT.Region, 'Unknown') AS Region	
	,COALESCE(EMP.RegionName, UNIT.RegionName,HVEMP.RegionName, HVUNIT.RegionName, 'Unknown') AS RegionName	
	,COALESCE(EMP.RVPEnterpriseID, UNIT.RVPEnterpriseID, HVEMP.RVPDirEnterpriseID, HVUNIT.RVPDirEnterpriseID, 'Unknown') AS RVPEnterpriseID	
	,COALESCE(EMP.RVPFullName, UNIT.RVPFullName, HVEMP.RVPDirFullName, HVUNIT.RVPDirFullName, 'Unknown') AS RVPFullName	
	,COALESCE(EMP.TFDEnterpriseID, UNIT.TFDEnterpriseID, HVEMP.TFDEnterpriseID, HVUNIT.TFDEnterpriseID, 'Unknown') AS TFDEnterpriseID	
	,COALESCE(EMP.TFDFullName, UNIT.TFDFullName, HVEMP.TFDFullName, HVUNIT.TFDFullName, 'Unknown') AS TFDFullName	
	,COALESCE(EMP.Territory, UNIT.Territory, HVEMP.Territory, HVUNIT.Territory, 'Unknown') AS Territory	
	,COALESCE(EMP.TerritoryName, UNIT.TerritoryName, HVEMP.TerritoryName, HVUNIT.TerritoryName,'Unknown') AS TerritoryName	
	,COALESCE(EMP.Unit, UNIT.Unit, HVEMP.Unit, HVUNIT.Unit, 'Unknown') AS Unit	
	,COALESCE(EMP.UnitName, UNIT.UnitName, HVEMP.UnitName, HVUNIT.UnitName,'Unknown') AS UnitName	
	,District	
	,COALESCE(DSMEnterpriseID, DSMRTMEnterpriseID, 'Unknown') AS DSMEnterpriseID	
	,COALESCE(DSMFullName, DSMRTMFullName, 'Unknown') AS DSMFullName	
	,COALESCE(TMEnterpriseID, TMDTMEnterpriseID, 'Unknown') AS TMEnterpriseID	
	,COALESCE(TMFullName, TMDTMFullName, 'Unknown') AS TMFullName	
	,COALESCE(EMP.EnterpriseID, HVEMP.EnterpriseID, 'Unknown') AS EnterpriseID	
	,COALESCE(EMP.NPSID, HVEMP.NPSID, 'Unknown') AS NPSID	
	,COALESCE(EMP.FullName, HVEMP.FullName, 'Unknown') AS FullName	
	,Tech	
	,Truck	
	,TaskReferenceNumber	
	,TaskReferenceLineNumber	
	,TaskDate	
	,PartDivision	
	,PartListSource	
	,PartNumber	
	,SKU	
	,PartDescription	
	,TaskQuantity	
	,ReturnOrderNumber	
	,RMANumber	
	,ReturnReasonCode	
	,BinNumber	
	,[FileName]	
	,RFSBarcode	
	,TaskClosedDate	
	,CASE WHEN TaskClosedDate IS NULL THEN Parts.dbo.getworkingdays(TaskDate, GETDATE())-1 ELSE Parts.dbo.getworkingdays(TaskDate, TaskClosedDate) -1  END AS DaysSinceTask	
FROM		
	Parts.dbo.TBL_AIMReturnTasks RETURNTASKS WITH (NOLOCK)	
LEFT JOIN		
      (SELECT		
            EnterpriseID		
            ,NPSID		
            ,FullName		
            ,TMEnterpriseID		
            ,TMFullName		
            ,DSMEnterpriseID		
            ,DSMFullName		
            ,Unit		
            ,UnitName		
            ,TFDEnterpriseID		
            ,TFDFullName		
            ,Territory		
            ,TerritoryName		
            ,Region		
            ,RegionName		
            ,RVPEnterpriseID		
            ,RVPFullName		
      FROM		
            Employee.dbo.VIEW_EmployeeHierarchyIHTechRegion) EMP		
ON		
      RETURNTASKS.District = EMP.Unit		
      AND		
      RETURNTASKS.Tech = EMP.NPSID		
LEFT JOIN		
	(SELECT	
		Unit
		,UnitName
		,TFDEnterpriseID
		,TFDFullName
		,Territory
		,TerritoryName
		,Region
		,RegionName
		,RVPEnterpriseID
		,RVPFullName
	FROM	
		Employee.dbo.VIEW_EmployeeHierarchyIHUnitRegion) UNIT
ON		
	RETURNTASKS.District = UNIT.Unit	
LEFT JOIN		
      (SELECT		
            EnterpriseID		
            ,NPSID		
            ,FullName		
            ,TMDTMEnterpriseID		
            ,TMDTMFullName		
            ,DSMRTMEnterpriseID		
            ,DSMRTMFullName		
            ,Unit		
            ,UnitName		
            ,TFDEnterpriseID		
            ,TFDFullName		
            ,Territory		
            ,TerritoryName		
            ,Region		
            ,RegionName		
            ,RVPDirEnterpriseID		
            ,RVPDirFullName		
      FROM		
            Employee.dbo.VIEW_EmployeeHierarchyIHHVACTechRegion) HVEMP		
ON		
      RETURNTASKS.District = HVEMP.Unit		
      AND		
      RETURNTASKS.Tech = HVEMP.NPSID		
LEFT JOIN		
	(SELECT	
		Unit
		,UnitName
		,TFDEnterpriseID
		,TFDFullName
		,Territory
		,TerritoryName
		,Region
		,RegionName
		,RVPDirEnterpriseID
		,RVPDirFullName
	FROM	
		Employee.dbo.VIEW_EmployeeHierarchyIHHVACUnitRegion) HVUNIT
ON		
	RETURNTASKS.District = HVUNIT.Unit	










GO
/****** Object:  View [dbo].[VIEW_AIMTruckBinInventory]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[VIEW_AIMTruckBinInventory]
AS

Select 
     COALESCE(EMP.Region, UNIT.Region, HVEMP.Region, HVUNIT.Region, 'Unknown') AS Region	
	,COALESCE(EMP.RegionName, UNIT.RegionName,HVEMP.RegionName, HVUNIT.RegionName, 'Unknown') AS RegionName	
	,COALESCE(EMP.RVPEnterpriseID, UNIT.RVPEnterpriseID, HVEMP.RVPDirEnterpriseID, HVUNIT.RVPDirEnterpriseID, 'Unknown') AS RVPEnterpriseID	
	,COALESCE(EMP.RVPFullName, UNIT.RVPFullName, HVEMP.RVPDirFullName, HVUNIT.RVPDirFullName, 'Unknown') AS RVPFullName	
	,COALESCE(EMP.TFDEnterpriseID, UNIT.TFDEnterpriseID, HVEMP.TFDEnterpriseID, HVUNIT.TFDEnterpriseID, 'Unknown') AS TFDEnterpriseID	
	,COALESCE(EMP.TFDFullName, UNIT.TFDFullName, HVEMP.TFDFullName, HVUNIT.TFDFullName, 'Unknown') AS TFDFullName	
	,COALESCE(EMP.Territory, UNIT.Territory, HVEMP.Territory, HVUNIT.Territory, 'Unknown') AS Territory	
	,COALESCE(EMP.TerritoryName, UNIT.TerritoryName, HVEMP.TerritoryName, HVUNIT.TerritoryName,'Unknown') AS TerritoryName	
	,COALESCE(EMP.Unit, UNIT.Unit, HVEMP.Unit, HVUNIT.Unit, 'Unknown') AS Unit	
	,COALESCE(EMP.UnitName, UNIT.UnitName, HVEMP.UnitName, HVUNIT.UnitName,'Unknown') AS UnitName	
	,Bin.District AS District	
	,COALESCE(DSMEnterpriseID, DSMRTMEnterpriseID, 'Unknown') AS DSMEnterpriseID	
	,COALESCE(DSMFullName, DSMRTMFullName, 'Unknown') AS DSMFullName	
	,COALESCE(TMEnterpriseID, TMDTMEnterpriseID, 'Unknown') AS TMEnterpriseID	
	,COALESCE(TMFullName, TMDTMFullName, 'Unknown') AS TMFullName	
	,COALESCE(EMP.EnterpriseID, HVEMP.EnterpriseID, 'Unknown') AS EnterpriseID	
	,COALESCE(EMP.NPSID, HVEMP.NPSID, 'Unknown') AS NPSID	
	,COALESCE(EMP.FullName, HVEMP.FullName, 'Unknown') AS FullName	 
	,[Truck]
	,[Tech] 
	,[TechEnterpriseID] 
	,[PartDivision] 
	,[PartListSource] 
	,[PartNumber] 
	,[PartDescription]
	,[SKU] 
	,[SellPrice] 
	,[CostAmount]
	,[TruckBin] 
	,[TruckBinQuantity] 
	,[TruckStockAddDate] 
	,[TruckStockChangeDate] 
	,[SourceDate]
	,[ExtractDate]
	FROM parts.dbo.TBL_AIMTruckBinInventory BIN WITH (NOLOCK)
LEFT JOIN		
      (SELECT		
            EnterpriseID		
            ,NPSID		
            ,FullName		
            ,TMEnterpriseID		
            ,TMFullName		
            ,DSMEnterpriseID		
            ,DSMFullName		
            ,Unit		
            ,UnitName		
            ,TFDEnterpriseID		
            ,TFDFullName		
            ,Territory		
            ,TerritoryName		
            ,Region		
            ,RegionName		
            ,RVPEnterpriseID		
            ,RVPFullName		
      FROM		
            Employee.dbo.VIEW_EmployeeHierarchyIHTechRegion) EMP		
ON		
	BIN.District = EMP.Unit		
	AND		
	BIN.Tech = EMP.NPSID		
LEFT JOIN		
	(SELECT	
		Unit
		,UnitName
		,TFDEnterpriseID
		,TFDFullName
		,Territory
		,TerritoryName
		,Region
		,RegionName
		,RVPEnterpriseID
		,RVPFullName
	FROM	
		Employee.dbo.VIEW_EmployeeHierarchyIHUnitRegion) UNIT
ON		
	BIN.District = UNIT.Unit	
LEFT JOIN		
      (SELECT		
            EnterpriseID		
            ,NPSID		
            ,FullName		
            ,TMDTMEnterpriseID		
            ,TMDTMFullName		
            ,DSMRTMEnterpriseID		
            ,DSMRTMFullName		
            ,Unit		
            ,UnitName		
            ,TFDEnterpriseID		
            ,TFDFullName		
            ,Territory		
            ,TerritoryName		
            ,Region		
            ,RegionName		
            ,RVPDirEnterpriseID		
            ,RVPDirFullName		
      FROM		
            Employee.dbo.VIEW_EmployeeHierarchyIHHVACTechRegion) HVEMP		
ON		
      BIN.District = HVEMP.Unit		
      AND		
      BIN.Tech = HVEMP.NPSID		
LEFT JOIN		
	(SELECT	
		Unit
		,UnitName
		,TFDEnterpriseID
		,TFDFullName
		,Territory
		,TerritoryName
		,Region
		,RegionName
		,RVPDirEnterpriseID
		,RVPDirFullName
	FROM	
		Employee.dbo.VIEW_EmployeeHierarchyIHHVACUnitRegion) HVUNIT
ON		
	BIN.District = HVUNIT.Unit	


GO
/****** Object:  View [dbo].[VIEW_Inventory]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







CREATE VIEW [dbo].[VIEW_Inventory]
AS

SELECT 
	LOC.LocationID
	,LocationType
	,LocationName
	,AddressPartOne
	,AddressPartTwo
	,City
	,[State]
	,Zip
	,ZipPlusFour
	,Latitude
	,Longitude
	,geography::STGeomFromText('POINT(' + CAST([Longitude] AS VARCHAR(20)) + ' ' + 
                    CAST([Latitude] AS VARCHAR(20)) + ')', 4326) AS Location
	,Phone
	,AlternatePhone
	,AlternatePhoneExtension
	,Fax
	,ChargeAccount
	,PickupCapable
	,ShipCapable
	,PartDivision
	,PartListSource
	,PartNumber
	,SKU
	,OnHandQuantity
	,OnHandLocation
	,AllocatedQuantity
	,NetOnHandQuantity
	,BackorderedQuantity
	,OnOrderQuantity
	,PartCost
	,PartSellPrice
FROM
	Parts.dbo.TBL_Location LOC WITH (NOLOCK)
INNER JOIN
	(SELECT
		LocationID
		,LEFT(SKU, 3) AS PartDivision
		,SUBSTRING(SKU, 4, 3) AS PartListSource
		,RIGHT(SKU, LEN(SKU) - 6) AS PartNumber
		,SKU
		,OnHandQuantity
		,OnHandLocation
		,AllocatedQuantity
		,NetOnHandQuantity
		,BackorderedQuantity
		,OnOrderQuantity
		,PartCost
		,PartSellPrice
	FROM
		Parts.dbo.TBL_LocationInventory WITH (NOLOCK)) INV
ON
	LOC.LocationID = INV.LocationID




GO
/****** Object:  View [dbo].[VIEW_InventoryTech]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








CREATE VIEW [dbo].[VIEW_InventoryTech]
AS

SELECT 
	Region
	,RegionName
	,RVPEnterpriseID
	,RVPFullName
	,TFDEnterpriseID
	,TFDFullName
	,Territory
	,TerritoryName
	,Unit
	,UnitName
	,DSMEnterpriseID
	,DSMFullName
	,TMEnterpriseID
	,TMFullName
	,EnterpriseID
	,NPSID
	,FullName
	,LOC.LocationID
	,LocationType
	,LocationName
	,AddressPartOne
	,AddressPartTwo
	,City
	,[State]
	,Zip
	,ZipPlusFour
	,Latitude
	,Longitude
	,geography::STGeomFromText('POINT(' + CAST([Longitude] AS VARCHAR(20)) + ' ' + 
                    CAST([Latitude] AS VARCHAR(20)) + ')', 4326) AS Location
	,Phone
	,AlternatePhone
	,AlternatePhoneExtension
	,Fax
	,ChargeAccount
	,PickupCapable
	,ShipCapable
	,PartDivision
	,PartListSource
	,PartNumber
	,SKU
	,OnHandQuantity
	,OnHandLocation
	,AllocatedQuantity
	,NetOnHandQuantity
	,BackorderedQuantity
	,OnOrderQuantity
	,PartCost
	,PartSellPrice
FROM
	Parts.dbo.TBL_Location LOC WITH (NOLOCK)
INNER JOIN
	(SELECT
		LocationID
		,LEFT(SKU, 3) AS PartDivision
		,SUBSTRING(SKU, 4, 3) AS PartListSource
		,RIGHT(SKU, LEN(SKU) - 6) AS PartNumber
		,SKU
		,OnHandQuantity
		,OnHandLocation
		,AllocatedQuantity
		,NetOnHandQuantity
		,BackorderedQuantity
		,OnOrderQuantity
		,PartCost
		,PartSellPrice
	FROM
		Parts.dbo.TBL_LocationInventory WITH (NOLOCK)) INV
ON
	LOC.LocationID = INV.LocationID
LEFT JOIN
      (SELECT
            EnterpriseID
            ,NPSID
            ,FullName
            ,TMEnterpriseID
            ,TMFullName
            ,DSMEnterpriseID
            ,DSMFullName
            ,Unit
            ,UnitName
            ,TFDEnterpriseID
            ,TFDFullName
            ,Territory
            ,TerritoryName
            ,Region
            ,RegionName
            ,RVPEnterpriseID
            ,RVPFullName
      FROM
            Employee.dbo.VIEW_EmployeeHierarchyIHTechRegion) EMP
ON
      LOC.LocationID = EMP.EnterpriseID
WHERE
	LocationType = 'TECH'


GO
/****** Object:  View [dbo].[VIEW_InventoryTruck]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO











CREATE VIEW [dbo].[VIEW_InventoryTruck]
AS

SELECT 
	Region
	,RegionName
	,RVPEnterpriseID
	,RVPFullName
	,TFDEnterpriseID
	,TFDFullName
	,Territory
	,TerritoryName
	,Unit
	,UnitName
	,DSMEnterpriseID
	,DSMFullName
    ,TMEnterpriseID
	,TMFullName
    ,FullName
    ,NPSID
	,TechEnterpriseID
	,Truck as TruckID
	,PartDivision
	,PartListSource
	,PartNumber
	,SKU
	,PartDescription
	,OnHandQuantity
	,AllocatedQuantity
	,OnHandQuantity - AllocatedQuantity as NetOnHandQuantity
	,BackorderedQuantity
	,OnOrderQuantity
	,TruckBin
	,CostAmount
	,SellPrice
FROM
	Parts.dbo.TBL_AIMTruckInventory INV WITH (NOLOCK)
LEFT JOIN
      (SELECT
			EnterpriseID
			,NPSID
			,FullName
			,TMEnterpriseID
			,TMFullName
			,DSMEnterpriseID
			,DSMFullName
			,Unit
			,UnitName
			,TFDEnterpriseID
			,TFDFullName
			,Territory
			,TerritoryName
			,Region
			,RegionName
			,RVPEnterpriseID
			,RVPFullName
		FROM
			Employee.dbo.VIEW_EmployeeHierarchyIHTechRegion) TECH
ON
     INV.TechEnterpriseID = TECH.EnterpriseID






GO
/****** Object:  View [dbo].[VIEW_Location]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







CREATE VIEW [dbo].[VIEW_Location]
AS

SELECT 
	LOC.LocationID
	,LocationType
	,LocationName
	,AddressPartOne
	,AddressPartTwo
	,City
	,[State]
	,Zip
	,ZipPlusFour
	,Latitude
	,Longitude
	,geography::STGeomFromText('POINT(' + CAST([Longitude] AS VARCHAR(20)) + ' ' + 
                    CAST([Latitude] AS VARCHAR(20)) + ')', 4326) AS Location
	,Phone
	,AlternatePhone
	,AlternatePhoneExtension
	,Fax
	,ChargeAccount
	,PickupCapable
	,ShipCapable
FROM
	Parts.dbo.TBL_Location LOC WITH (NOLOCK)




GO
/****** Object:  View [dbo].[VIEW_Metric205Data]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO












CREATE VIEW [dbo].[VIEW_Metric205Data]
AS

SELECT 
	UniqueID
	,md.AccountingYearMonth
	,Period
	,CallCount
	,accountingMonthstartdate
	,accountingMonthenddate
FROM
	Parts.dbo.TBL_Metric205Data MD WITH (NOLOCK)
INNER JOIN
	(SELECT distinct
	  accountingMonthstartdate
	 ,accountingMonthenddate
	 ,AccountingYearMonth
	FROM
		Parts.dbo.TBL_FiscalCalendar WITH (NOLOCK)) FC
ON
	MD.AccountingYearMonth = FC.AccountingYearMonth







GO
/****** Object:  View [dbo].[VIEW_Part]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE VIEW [dbo].[VIEW_Part]
AS

SELECT
	PartSKU
	,PartDivision
	,PartListSource
	,PartNumber
	,PartDescription
	,PartSourceOrderNumber
--	,PartAvailability
	,c.avail_ds AS PartAvailability
	,PartCost
	,PartSellPrice
	,PartPriceModifyDate
	,HazardousMaterialCatalogCode
	,PartImageURL
	,PartAddDate
	,ISNULL(ShippingWeight, '') AS ShippingWeight
	,ISNULL(ShippingLength, '') AS ShippingLength
	,ISNULL(ShippingWidth, '') AS ShippingWidth
	,ISNULL(ShippingHeight, '') AS ShippingHeight
FROM
	Parts.dbo.TBL_Part PART WITH (NOLOCK)
LEFT JOIN
	(	SELECT
			SKU
			,ShippingWeight
			,ShippingLength
			,ShippingWidth
			,ShippingHeight
		FROM
			Parts.dbo.TBL_PartShippingSpecifications WITH (NOLOCK)
	) PARTSHIP
	ON PART.PartSKU = PARTSHIP.SKU
/* Remove this if new values are causing an error*/
LEFT JOIN
	Parts.dbo.TBL_PartAvailabilityCodes c
	ON PART.PartAvailability = c.avail_cd
/*
*/



GO
/****** Object:  View [dbo].[VIEW_PartSub]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE VIEW [dbo].[VIEW_PartSub]
AS

SELECT
	PartSKU
	,SubPartSKU
	,SubPartWayCode
	,SubPartLevel
FROM
	Parts.dbo.TBL_PartSub WITH (NOLOCK)


GO
/****** Object:  View [dbo].[VIEW_PartsUsage]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [dbo].[VIEW_PartsUsage]
AS

SELECT 
	Region
	,RegionName
	,RVPEnterpriseID
	,RVPFullName
	,TFDEnterpriseID
	,TFDFullName
	,Territory
	,TerritoryName
	,Unit
	,UnitName
	,DSMEnterpriseID
	,DSMFullName 
	,TMEnterpriseID
	,TMFullName
	,EnterpriseID
	,NPSID
	,FullName
	,ServiceOrder 
	,PartSequence 
	,PartDivision 
	,PartListSource 
	,Part
	,PartDivision + PartListSource + Part as SKU 
	,PartDescription 
	,PartType 
	,PartQuantity 
	,PartQuantityUsed 
	,Coverage 
	,PartActualSellPrice 
	,PartStatus 
	,PartStatusDate 
	,PartExtendedWarrantySold 
	,PartPromotion 
	,PartOrderDate 
	,EmployeeIdentification 
	,PartQuantityOrdered 
	,PartQuantityReceived 
	,PartQuantityReceivedDate 
	,PartInstalledDate 
	,PartStockBin 
	,TruckBin 
	,PartDestinationBin 
	,PartPriceEach 
	,PartCostEach 
	,PartPremiumPriceEach
	,PartTrackingInquiryCode
	,MerchandiseComponentType 
	,LastActionTimestamp 
	
FROM ServiceOrder.dbo.VIEW_NPSServiceOrderParts  PARTS WITH (NOLOCK)
LEFT JOIN
      (SELECT
            Region
			,RegionName
			,RVPEnterpriseID
			,RVPFullName
			,TFDEnterpriseID
			,TFDFullName
			,Territory
			,TerritoryName
			,Unit
			,UnitName
			,DSMEnterpriseID
			,DSMFullName 
			,TMEnterpriseID
			,TMFullName
			,EnterpriseID
			,NPSID
			,FullName

      FROM
            Employee.dbo.VIEW_EmployeeHierarchyIHTechRegion) EMP 
ON
      PARTS.ServiceUnit = EMP.Unit  
	  AND
	  PARTS.EmployeeIdentification = EMP.NPSID 






GO
/****** Object:  View [dbo].[VIEW_PISR]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

















CREATE VIEW [dbo].[VIEW_PISR]
AS

SELECT 		
	COALESCE(EMP.Region, UNIT.Region, HVEMP.Region, HVUNIT.Region, 'Unknown') AS Region	
	,COALESCE(EMP.RegionName, UNIT.RegionName,HVEMP.RegionName, HVUNIT.RegionName, 'Unknown') AS RegionName	
	,COALESCE(EMP.RVPEnterpriseID, UNIT.RVPEnterpriseID, HVEMP.RVPDirEnterpriseID, HVUNIT.RVPDirEnterpriseID, 'Unknown') AS RVPEnterpriseID	
	,COALESCE(EMP.RVPFullName, UNIT.RVPFullName, HVEMP.RVPDirFullName, HVUNIT.RVPDirFullName, 'Unknown') AS RVPFullName	
	,COALESCE(EMP.TFDEnterpriseID, UNIT.TFDEnterpriseID, HVEMP.TFDEnterpriseID, HVUNIT.TFDEnterpriseID, 'Unknown') AS TFDEnterpriseID	
	,COALESCE(EMP.TFDFullName, UNIT.TFDFullName, HVEMP.TFDFullName, HVUNIT.TFDFullName, 'Unknown') AS TFDFullName	
	,COALESCE(EMP.Territory, UNIT.Territory, HVEMP.Territory, HVUNIT.Territory, 'Unknown') AS Territory	
	,COALESCE(EMP.TerritoryName, UNIT.TerritoryName, HVEMP.TerritoryName, HVUNIT.TerritoryName,'Unknown') AS TerritoryName	
	,District AS Unit	
	,COALESCE(EMP.UnitName, UNIT.UnitName, HVEMP.UnitName, HVUNIT.UnitName,'Unknown') AS UnitName	
	,District	
	,COALESCE(DSMEnterpriseID, DSMRTMEnterpriseID, 'Unknown') AS DSMEnterpriseID	
	,COALESCE(DSMFullName, DSMRTMFullName, 'Unknown') AS DSMFullName	
	,COALESCE(TMEnterpriseID, TMDTMEnterpriseID, 'Unknown') AS TMEnterpriseID	
	,COALESCE(TMFullName, TMDTMFullName, 'Unknown') AS TMFullName	
	,COALESCE(EMP.EnterpriseID, HVEMP.EnterpriseID, 'Unknown') AS EnterpriseID	
	,COALESCE(EMP.NPSID, HVEMP.NPSID, 'Unknown') AS NPSID	
	,COALESCE(EMP.FullName, HVEMP.FullName, 'Unknown') AS FullName	
	,Tech	
	,Truck	
	,Case when [TotalTruckBinQuantity] is NULL then 0 else TotalTruckBinQuantity end as TotalTruckBinQuantity
    ,Case when [TotalCostAmount] is NULL then 0 else TotalCostAmount end as TotalCostAmount
    ,Case when [TotalSellPrice] is NULL then 0 else TotalSellPrice end as TotalSellPrice

FROM		
	Parts.dbo.TBL_PISR INV WITH (NOLOCK)	
LEFT JOIN		
      (SELECT		
            EnterpriseID		
            ,NPSID		
            ,FullName		
            ,TMEnterpriseID		
            ,TMFullName		
            ,DSMEnterpriseID		
            ,DSMFullName		
            ,Unit		
            ,UnitName		
            ,TFDEnterpriseID		
            ,TFDFullName		
            ,Territory		
            ,TerritoryName		
            ,Region		
            ,RegionName		
            ,RVPEnterpriseID		
            ,RVPFullName		
      FROM		
            Employee.dbo.VIEW_EmployeeHierarchyIHTechRegion) EMP		
ON		
      INV.District = EMP.Unit		
      AND		
      INV.Tech = EMP.NPSID		
LEFT JOIN		
	(SELECT	
		Unit
		,UnitName
		,TFDEnterpriseID
		,TFDFullName
		,Territory
		,TerritoryName
		,Region
		,RegionName
		,RVPEnterpriseID
		,RVPFullName
	FROM	
		Employee.dbo.VIEW_EmployeeHierarchyIHUnitRegion) UNIT
ON		
	INV.District = UNIT.Unit	
LEFT JOIN		
      (SELECT		
            EnterpriseID		
            ,NPSID		
            ,FullName		
            ,TMDTMEnterpriseID		
            ,TMDTMFullName		
            ,DSMRTMEnterpriseID		
            ,DSMRTMFullName		
            ,Unit		
            ,UnitName		
            ,TFDEnterpriseID		
            ,TFDFullName		
            ,Territory		
            ,CASE WHEN (Unit BETWEEN '0009000' AND '0009999') THEN 'HVAC' END AS TerritoryName --added with request by B. Evers to place HVAC in Territory Name		
            ,Region		
            ,RegionName		
            ,RVPDirEnterpriseID		
            ,RVPDirFullName		
      FROM		
            Employee.dbo.VIEW_EmployeeHierarchyIHHVACTechRegion) HVEMP		
ON		
      INV.District = HVEMP.Unit		
      AND		
      INV.Tech = HVEMP.NPSID		
LEFT JOIN		
	(SELECT	
		Unit
		,UnitName
		,TFDEnterpriseID
		,TFDFullName
		,Territory
		,CASE WHEN (Unit BETWEEN '0009000' AND '0009999') THEN 'HVAC' END AS TerritoryName --added with request by B. Evers to place HVAC in Territory Name	
		,Region
		,RegionName
		,RVPDirEnterpriseID
		,RVPDirFullName
	FROM	
		Employee.dbo.VIEW_EmployeeHierarchyIHHVACUnitRegion) HVUNIT
ON		
	INV.District = HVUNIT.Unit














GO
/****** Object:  View [dbo].[VIEW_TruckManagement]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO













CREATE VIEW [dbo].[VIEW_TruckManagement]
AS

SELECT 		
	COALESCE(EMP.Region, UNIT.Region, HVEMP.Region, HVUNIT.Region, 'Unknown') AS Region	
	,COALESCE(EMP.RegionName, UNIT.RegionName,HVEMP.RegionName, HVUNIT.RegionName, 'Unknown') AS RegionName	
	,COALESCE(EMP.RVPEnterpriseID, UNIT.RVPEnterpriseID, HVEMP.RVPDirEnterpriseID, HVUNIT.RVPDirEnterpriseID, 'Unknown') AS RVPEnterpriseID	
	,COALESCE(EMP.RVPFullName, UNIT.RVPFullName, HVEMP.RVPDirFullName, HVUNIT.RVPDirFullName, 'Unknown') AS RVPFullName	
	,COALESCE(EMP.TFDEnterpriseID, UNIT.TFDEnterpriseID, HVEMP.TFDEnterpriseID, HVUNIT.TFDEnterpriseID, 'Unknown') AS TFDEnterpriseID	
	,COALESCE(EMP.TFDFullName, UNIT.TFDFullName, HVEMP.TFDFullName, HVUNIT.TFDFullName, 'Unknown') AS TFDFullName	
	,COALESCE(EMP.Territory, UNIT.Territory, HVEMP.Territory, HVUNIT.Territory, 'Unknown') AS Territory	
	,COALESCE(EMP.TerritoryName, UNIT.TerritoryName, HVEMP.TerritoryName, HVUNIT.TerritoryName,'Unknown') AS TerritoryName	
	,COALESCE(EMP.Unit, UNIT.Unit, HVEMP.Unit, HVUNIT.Unit, 'Unknown') AS Unit	
	,COALESCE(EMP.UnitName, UNIT.UnitName, HVEMP.UnitName, HVUNIT.UnitName,'Unknown') AS UnitName	
	,TRUCK.District	
	,COALESCE(DSMEnterpriseID, DSMRTMEnterpriseID, 'Unknown') AS DSMEnterpriseID	
	,COALESCE(DSMFullName, DSMRTMFullName, 'Unknown') AS DSMFullName	
	,COALESCE(TMEnterpriseID, TMDTMEnterpriseID, 'Unknown') AS TMEnterpriseID	
	,COALESCE(TMFullName, TMDTMFullName, 'Unknown') AS TMFullName	
	,COALESCE(EMP.EnterpriseID, HVEMP.EnterpriseID, 'Unknown') AS EnterpriseID	
	,COALESCE(EMP.NPSID, HVEMP.NPSID, 'Unknown') AS NPSID	
	,COALESCE(EMP.FullName, HVEMP.FullName, 'Unknown') AS FullName	
    ,TRUCK.Truck
    ,TRUCK.Tech
    ,TRUCK.TechEnterpriseID
    ,OwnerEnterpriseID
	,Deleted
	,TruckBin
	,TruckBinQuantity

FROM		
	Parts.dbo.TBL_AIMTruck TRUCK WITH (NOLOCK)
LEFT JOIN
      (SELECT
			District
			,Truck
			,Tech
			,TechEnterpriseID
			,TruckBin
			,TruckBinQuantity	

      FROM		
            Parts.dbo.TBL_AIMTruckBinInventory) BIN
ON		
      TRUCK.District = BIN.District	
      AND		
      TRUCK.Truck = BIN.Truck
	 	
LEFT JOIN		
      (SELECT		
            EnterpriseID		
            ,NPSID		
            ,FullName		
            ,TMEnterpriseID		
            ,TMFullName		
            ,DSMEnterpriseID		
            ,DSMFullName		
            ,Unit		
            ,UnitName		
            ,TFDEnterpriseID		
            ,TFDFullName		
            ,Territory		
            ,TerritoryName		
            ,Region		
            ,RegionName		
            ,RVPEnterpriseID		
            ,RVPFullName		
      FROM		
            Employee.dbo.VIEW_EmployeeHierarchyIHTechRegion) EMP		
ON		
      TRUCK.District = EMP.Unit		
      AND		
      TRUCK.Tech = EMP.NPSID		
LEFT JOIN		
	(SELECT	
		Unit
		,UnitName
		,TFDEnterpriseID
		,TFDFullName
		,Territory
		,TerritoryName
		,Region
		,RegionName
		,RVPEnterpriseID
		,RVPFullName
	FROM	
		Employee.dbo.VIEW_EmployeeHierarchyIHUnitRegion) UNIT
ON		
	TRUCK.District = UNIT.Unit	
LEFT JOIN		
      (SELECT		
            EnterpriseID		
            ,NPSID		
            ,FullName		
            ,TMDTMEnterpriseID		
            ,TMDTMFullName		
            ,DSMRTMEnterpriseID		
            ,DSMRTMFullName		
            ,Unit		
            ,UnitName		
            ,TFDEnterpriseID		
            ,TFDFullName		
            ,Territory		
            ,TerritoryName		
            ,Region		
            ,RegionName		
            ,RVPDirEnterpriseID		
            ,RVPDirFullName		
      FROM		
            Employee.dbo.VIEW_EmployeeHierarchyIHHVACTechRegion) HVEMP		
ON		
      TRUCK.District = HVEMP.Unit		
      AND		
      TRUCK.Tech = HVEMP.NPSID		
LEFT JOIN		
	(SELECT	
		Unit
		,UnitName
		,TFDEnterpriseID
		,TFDFullName
		,Territory
		,TerritoryName
		,Region
		,RegionName
		,RVPDirEnterpriseID
		,RVPDirFullName
	FROM	
		Employee.dbo.VIEW_EmployeeHierarchyIHHVACUnitRegion) HVUNIT
ON		
	TRUCK.District = HVUNIT.Unit
WHERE Deleted = 0












GO
/****** Object:  View [dbo].[VIEW_Trucks]    Script Date: 9/18/2019 8:02:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[VIEW_Trucks]
AS
SELECT DISTINCT
	District
	,Truck
FROM
	Parts.dbo.TBL_AIMTruck WITH (NOLOCK) 
WHERE
	Deleted = '0'
UNION
SELECT DISTINCT 
	District
	,Truck
FROM 
	Parts.dbo.TBL_AIMTruckBinInventory WITH (NOLOCK) 







GO
/****** Object:  Index [IX_TBL_AIMEndToEndUPS_Parts_EndToEndShipmentID]    Script Date: 9/18/2019 8:02:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_AIMEndToEndUPS_Parts_EndToEndShipmentID] ON [dbo].[TBL_AIMEndToEndUPS_Parts]
(
	[EndToEndShipmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UIX_TBL_AIMEndToEndUPS_Parts_SKU_EndToEndShipmentID]    Script Date: 9/18/2019 8:02:18 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UIX_TBL_AIMEndToEndUPS_Parts_SKU_EndToEndShipmentID] ON [dbo].[TBL_AIMEndToEndUPS_Parts]
(
	[SKU] ASC,
	[EndToEndShipmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TBL_AIMEndToEndUPS_Shipment_UPSTrackingNumberID]    Script Date: 9/18/2019 8:02:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_AIMEndToEndUPS_Shipment_UPSTrackingNumberID] ON [dbo].[TBL_AIMEndToEndUPS_Shipment]
(
	[UPSTrackingNumberID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UIX_TBL_AIMEndToEndUPS_Shipment_RMANumber_UPSTrackingNumberID]    Script Date: 9/18/2019 8:02:18 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UIX_TBL_AIMEndToEndUPS_Shipment_RMANumber_UPSTrackingNumberID] ON [dbo].[TBL_AIMEndToEndUPS_Shipment]
(
	[RMANumber] ASC,
	[UPSTrackingNumberID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UIX_TBL_AIMEndToEndUPS_UPSTrackingNumber_LastModifyDate]    Script Date: 9/18/2019 8:02:18 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UIX_TBL_AIMEndToEndUPS_UPSTrackingNumber_LastModifyDate] ON [dbo].[TBL_AIMEndToEndUPS_UPSTrackingNumber]
(
	[UPSTrackingNumber] ASC,
	[LastModifyDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IDX_TBL_AIMReceiveTasks_District]    Script Date: 9/18/2019 8:02:18 PM ******/
CREATE NONCLUSTERED INDEX [IDX_TBL_AIMReceiveTasks_District] ON [dbo].[TBL_AIMReceiveTasks]
(
	[District] ASC
)
INCLUDE ( 	[Tech]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IDX_TBL_Location_LocationType]    Script Date: 9/18/2019 8:02:18 PM ******/
CREATE NONCLUSTERED INDEX [IDX_TBL_Location_LocationType] ON [dbo].[TBL_Location]
(
	[LocationType] ASC
)
INCLUDE ( 	[LocationID],
	[LocationName],
	[AddressPartOne],
	[City],
	[State],
	[Zip],
	[Latitude],
	[Longitude],
	[Phone]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_NPNPartOrder_ServiceUnit_ServiceOrder]    Script Date: 9/18/2019 8:02:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_NPNPartOrder_ServiceUnit_ServiceOrder] ON [dbo].[TBL_NPNPartOrder]
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC,
	[PartOrderStatus] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_Part_Div_Source_Part]    Script Date: 9/18/2019 8:02:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_Part_Div_Source_Part] ON [dbo].[TBL_Part]
(
	[PartDivision] ASC,
	[PartListSource] ASC,
	[PartNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_TBL_Part_QA_Part_Div_Source]    Script Date: 9/18/2019 8:02:18 PM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_Part_QA_Part_Div_Source] ON [dbo].[TBL_Part_QA]
(
	[PartDivision] ASC,
	[PartListSource] ASC,
	[PartNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
USE [master]
GO
ALTER DATABASE [Parts] SET  READ_WRITE 
GO
